<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
if (App::environment('production')) {
    URL::forceScheme('https');
}
Route::get('/auth/social/{provider}', 'SocialAuthController@providerRedirect');
Route::get('/auth/{provider}/callback', 'SocialAuthController@providerRedirectCallback');
//Site internet
Route::get('/', function () {
    return view('front.home-site');
});
Route::get('/accueil', function () {
    return view('front.home-site');
});
Route::get('/a_propos', function () {
    return view('front.home-site');
});
Route::get('/programmes', function () {
    return view('front.home-site');
});
Route::get('/catalogue_photos', function () {
    return view('front.home-site');
});
Route::get('/login', function () {
    return view('home-bp');
});
Route::get('/ticket', function () {
    return view('home-bp');
});
Route::get('/password', function () {
    return view('home-bp');
});
Route::get('/register', function () {
    return view('home-bp');
});
Route::get('/pricing', function () {
    return view('home-bp');
});
Route::get('/formations', function () {
    return view('front.formations');
    // return "Hello";
});
Route::get('/inscription', function () {
    return view('front.inscription');
});
Route::get('/formation/{name}', function () {
    return view('front.formations');
});

Route::get('/bp/home', function () {
    return view('home-bp');
});

Route::get('/bp/summary', function () {
    return view('home-bp');
});
Route::get('/bp/admin/dashboard', function () {
    return view('home-bp');
});
Route::get('/bp/admin/concours/creer', function () {
    return view('home-bp');
});
Route::get('/bp/admin/concours/liste', function () {
    return view('home-bp');
});
Route::get('/bp/admin/concours/participants', function () {
    return view('home-bp');
});
Route::get('/bp/admin/programmes/emploi-de-temps', function () {
    return view('home-bp');
});
Route::get('/bp/admin/concours/projets', function () {
    return view('home-bp');
});
Route::get('/bp/admin/evaluation/projets', function () {
    return view('home-bp');
});
Route::get('/bp/soumission/projets', function () {
    return view('home-bp');
});
Route::get('/concours/preview/{id}', function () {
    return view('home-bp');
});
Route::get('/programme/register/{id}', function () {
    return view('home-bp');
});
Route::get('/programs/all/{id}', function ($id) {
    
    $url = "https://www.programmes.cyberschoolgabon.com/#/ateliers/{$id}";
    return redirect($url);
});

Route::get('/concours/login/{id}', function () {
    return view('home-bp');
});
Route::get('/bp/admin/evaluation/{id}', function () {
    return view('home-bp');
});
Route::get('/bp/admin/parametres/membres/utilisateur', function () {
    return view('home-bp');
});
Route::get('/bp/admin/parametres/membres/role', function () {
    return view('home-bp');
});
Route::get('/bp/superadmin/dashboard', function () {
    return view('home-bp');
});

Route::get('/bp/superadmin/projets', function () {
    return view('home-bp');
});
Route::get('/bp/superadmin/utilisateurs', function () {
    return view('home-bp');
});
Route::get('/bp/superadmin/plan-comptable', function () {
    return view('home-bp');
});


Route::get('/bp/summary/{id}', function () {
    return view('home-bp');
});

Route::get('/bp/summary/{id}/print', function () {
    return view('home-bp');
});

Route::get('/bp/executive-summary', function () {
    return view('home-bp');
});
Route::get('/register', function () {
    return view('home-bp');
});
Route::get('/bp/projets', function () {
    return view('home-bp');
});
Route::get('/bp/bpeconomique', function () {
    return view('home-bp');
});
Route::get('/bp/profile/user', function () {
    return view('home-bp');
});
Route::get('/bp/profile/evaluateur', function () {
    return view('home-bp');
});

// route pour le rattrapage des nouveaux champs de la bdd
Route::view('/scaffold', 'home-bp');

//ROUTES EVALUATEURS
Route::get('/bp/evaluateur/projets/{id}', function () {
    return view('home-bp');
});
Route::get('/bp/evaluateur/projets', function () {
    return view('home-bp');
});

// SUIVI ROUTES
Route::group(['prefix' => '/bp/suivi'], function () {
    Route::view('/recettes', 'home-suivi');
    Route::view('/depenses', 'home-suivi');
    Route::view('/soldes', 'home-suivi');
    Route::view('/produits', 'home-suivi');
    Route::view('/vendeurs', 'home-suivi');
    Route::view('/clients', 'home-suivi');
    Route::view('/equipe/membre', 'home-suivi');
    Route::view('/acteurs/roles', 'home-suivi');
    Route::view('/type-client', 'home-suivi');
    Route::view('/categories', 'home-suivi');
    Route::view('/pays', 'home-suivi');
    Route::view('/banques', 'home-suivi');
    Route::view('/taches', 'home-suivi');
    Route::view('/taches/workflow', 'home-suivi');
    Route::view('/kanban', 'home-suivi');
    Route::view('/board', 'home-suivi');
    Route::view('/acteurs/membres', 'home-suivi');
    Route::view('/', 'home-suivi');
    Route::view('/home', 'home-suivi');
    Route::view('/objectif', 'home-suivi');
    Route::view('/subprojet', 'home-suivi');
    Route::view('/devis', 'home-suivi');
    Route::view('/devis/model/{id}', 'home-suivi');
    Route::view('/devis/model/nouveau', 'home-suivi');
    Route::view('/devis/statistiques', 'home-suivi');
    Route::view('/devis/operations', 'home-suivi');
    Route::view('/devis/operation', 'home-suivi');
    Route::view('/devis/{index}', 'home-suivi');
    Route::view('/parametres/formulaire', 'home-suivi');
    Route::view('/parametres/fiscalite', 'home-suivi');
    Route::view('/dashboard', 'home-suivi');
    // FORMULAIRES
    Route::view('/parametres/formulaires/recettes', 'home-suivi');
    Route::view('/parametres/formulaires/depenses', 'home-suivi');
    Route::view('/parametres/formulaires/clients', 'home-suivi');
    // Suivi du bpfinancier
    Route::view('/execbp', 'home-suivi');
    Route::view('/execbp/execution', 'home-suivi');
    Route::view('/execbp/comparaison', 'home-suivi');
    // Route::view('/bpfinancier/', 'home-suivi');
});
// ERP ROUTES
Route::group(['prefix' => '/bp/erp'], function () {
    Route::view('/home', 'home-erp');
    Route::view('/tiers/dashboard', 'home-erp');

    Route::view('/tiers/clients', 'home-erp');
    Route::view('/tiers/fournisseurs', 'home-erp');
    Route::view('/tiers/personnels', 'home-erp');
    Route::view('/tiers/autres', 'home-erp');

    Route::view('/articles/dashboard', 'home-erp');
    Route::view('/articles/article-vente', 'home-erp');
    Route::view('/articles/article-achat', 'home-erp');

    Route::view('/ventes/dashboard', 'home-erp');
    Route::view('/ventes/facturation', 'home-erp');
    Route::view('/ventes/reglement', 'home-erp');

    Route::view('/achat/dashboard', 'home-erp');
});

// SUIVI ROUTES racine /acteurs
Route::group(['prefix' => '/bp/suivi/acteurs'], function () {
    Route::view('/', 'home-suivi');
    Route::view('/clients', 'home-suivi');
    Route::view('/vendeurs', 'home-suivi');
    Route::view('/fournisseurs', 'home-suivi');
    Route::view('/partenaires', 'home-suivi');
    Route::view('/equipes', 'home-suivi');
});

// SUIVI ROUTES racine /fisc
Route::group(['prefix' => '/bp/suivi/fisc'], function () {
    Route::view('/', 'home-suivi');
    Route::view('/banques', 'home-suivi');
});

// MONITEUR ROUTES
Route::group(['prefix' => '/bp/moniteur'], function () {
    Route::view('/', 'home-suivi');
});

// FINANCIER ROUTES
Route::group(['prefix' => 'bp/bpfinancier'], function () {
    Route::view('/investissement/besoins', 'home-financier');
    Route::view('/', 'home-financier');
    Route::view('/provisionTable', 'home-financier');
    // Route::view('/{id}', 'home-financier');
    // Route::view('/{id}', 'home-financier');
    Route::view('/investissement/ressources', 'home-financier');
    Route::view('/exploitation/charges', 'home-financier');
    Route::view('/exploitation/produits', 'home-financier');
    Route::view('/recapitulatif', 'home-financier');
    Route::view('/plan-tresorerie', 'home-financier');
    Route::view('/plan-financement', 'home-financier');
    Route::view('/compte-resultats', 'home-financier');
    Route::view('/solde-intermediaire-gestion', 'home-financier');
    Route::view('/seuil-rentabilite', 'home-financier');
    Route::view('/besoin-fonds-roulement', 'home-financier');
    Route::view('/echeancier-prets-bancaires', 'home-financier');
    Route::view('/bilan-previsionnel', 'home-financier');
    Route::view('/amortissements', 'home-financier');
});

// ECONOMIQUE ROUTES racine /economique
Route::group(['prefix' => '/bp/bpeconomique'], function () {
    Route::view('/', 'home-bp');
});

// ADMIN ROUTES
Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
    Route::view('/', 'back.home');
    Route::view('/accueil', 'back.home');
    Route::view('/utilisateurs', 'back.users.users');
    Route::view('/produits/types', 'back.produits.type');
    Route::view('/produits/nouveau', 'back.produits.produit');
    Route::view('/zones', 'back.zones.zone');
    Route::view('/zones/entreprises', 'back.zones.entreprise');
    Route::view('/faq', 'back.faq.faq');
    Route::view('/commandes/liste', 'back.orders.list');
    Route::view('/attribution/projet', 'back.summary.projet');
    Route::view('/projets/list', 'back.summary.projetTrie');
    Route::view('/projets/evalues', 'back.summary.trie');
    Route::view('/parametres/concours', 'back.parametres.parametre');
    Route::view('/parametres/branches', 'back.parametres.branche');
    Route::view('/parametres/activites', 'back.parametres.activite');
    Route::view('/formations', 'back.formations.formations');
});

// Redifined authenticated routes
Route::get('admin/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/admin/login', 'Auth\LoginController@login');
Route::post('admin/logout', 'Auth\LoginController@logout')->name('logout');
