/* eslint-disable no-undef */
// How to use, run `node SetdefaultTvaParameter.js --idproject <projectId>`

console.info('This script is used to set default tva parameter in the whole bpFinancier collection');

const firebase = require("firebase");
const defaultParamTva = require('./resources/assets/js/views/bp/financier/default.tva.json');

const config = ({
    apiKey: "AIzaSyAtLbJIn6Rk7xXdj81bIyBCc-KoBt3y9Hs",
    authDomain: "cyberschool-ab785.firebaseapp.com",
    databaseURL: "https://cyberschool-ab785.firebaseio.com",
    projectId: "cyberschool-ab785",
    storageBucket: "cyberschool-ab785.appspot.com",
    messagingSenderId: "107562228204",
    appId: "1:107562228204:web:29cb3cd4f35fe30bbcc8c9",
    measurementId: "G-H7E9VRGRNV"

});
firebase.initializeApp(config);  

firebase.firestore().settings({
    cacheSizeBytes: firebase.firestore.CACHE_SIZE_UNLIMITED
});

firebase.firestore().collection('financier').get().then((bpfinancier) => {
    let counter = 0;
    
    bpfinancier.forEach((financier) => {
        counter++;
        
        if(financier.data()['projet'] != null) {
            console.log('= = = = = = = = = = = = = = = = = =');
            console.log('Sélèction du projet ', financier.data().projet);
            // console.log(financier.id, " => ", financier.data());
            // recupere les anciens parametres
            console.log(`${counter}. Parametres financier du projet ${financier.data().projet}: `);
            console.log(financier.data().parametres);        
            if(financier.data().parametres){
                // rajout de la dva par defaut
                let newParams = Object.assign(financier.data().parametres, {tva:defaultParamTva});
                console.log('Nouveau parametrage: ', newParams);

                financier.ref.update({
                    parametres: newParams
                }).then( () => {
                    console.log(`Default Tva Parameters successfully updated for project ${counter}:`,financier.data().projet);
                    console.log('= = = = = = = = = = = = = = = = = =');
                });
            } else {
                // get the old bpfinancier
                let oldFinancier = financier.data();
                oldFinancier['parametres'] = { currency: null, chargePatronale: { dirigeant: 4, employe: 2 },tva: defaultParamTva };
                financier.ref.update(oldFinancier).then(() => {
                    console.log(`Default Financier Parameters successfully updated for project ${counter}:`,financier.data().projet);
                    console.log('= = = = = = = = = = = = = = = = = =');
                });
            }
        }
    });
});
