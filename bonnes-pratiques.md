 # Quelques bonnes pratiques en <business code/>

## 0. A la base de tout

* Faire un pull de la branche intégration avant démarrage de travaux
* Créer une branche parallèle pour le développement de nouvelles fonctionnalités

## 1. Répartition de fonctions

* Pour la réutilisabilité des fonctions écrites, veuillez les contextualiser: 
  - locale: dans un composant
  - sous-module: dans une mixins (avec ou sans données)
  - globale: dans un helper (export function <name>)
* Pour optimization des instanciations de firebase, nous avons défini les globales qui suivent: 
''' 
Vue.prototype.$firestore = firebase.firestore();
Vue.prototype.$firestorage = firebase.storage();
'''
Ces dernieres sont accessibles via this.$firestore et this.$firestorage afin d'éviter re-importer
firebase à tout azimut et libérer de la mémoire ...

## 2. Composition de code

* Toutes les parties de code {html + css + js} répétées devraient être converties en composant vue.js
* Prévoir des classes / données par défaut pour toute opération en cas de panne de connexion internet
* Variables 

## 3. Commentaires de code
* Ecrire les fonctions en camelCase
* Ecrire les classes css en snake-case

## 4. Suivi de code
 * Placer une url des pages WIP sur l'application de Business Plan, pour vérifier afterwork
 * Estimer la durée de votre tâche
 * Revisiter vos cartes Trello au moins chaque deux jours
 * Etiquettez vos taches backend\frontend, summary\financier\formation\suivi
 * Creez une checklist pour La Definition Of Done pour decrire ce que le testeur doit verifier dans l'appli et en relation avec votre tache
 * Placer un seul label par tache
 * Ne jamais démarrer une tache sans lui attribuer un label et un acteur
 * Eviter de cumuler plus de deux sous-taches dans une checklist
 * Toujours placer la deadline du sprint sur une tache
 * Supprimer les logs après utilisation