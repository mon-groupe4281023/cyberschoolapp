/* eslint-disable no-undef */
let mix = require('laravel-mix');
require('laravel-mix-bundle-analyzer');

let LiveReloadPlugin = require('webpack-livereload-plugin');
const path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

var plugin = 'resources/assets/plugins/';
try {
    // Une erreur peut se produire
    if (mix.isWatching()) {
        mix.js('resources/assets/js/app.js', 'public/js/app.js')
            .combine([
                plugin + 'jquery/jquery.min.js',
                plugin + 'popper/popper.min.js',
                plugin + 'slimscroll/jquery.slimscroll.js',
                plugin + 'waves/waves.js',
                plugin + 'sidebarmenu.js',
                plugin + 'sticky-kit/sticky-kit.min.js',
                'resources/assets/js/custom.js',
                'public/js/app.js',
            ], 'public/js/bundle.min.js')
            .sass('resources/assets/sass/style.scss', 'public/css', {
                implementation: require('sass')
            })
            .sass('resources/assets/sass/financier.scss', 'public/css', {
                implementation: require('sass')
            });

        mix.bundleAnalyzer({
            openAnalyzer: false,
            analyzerMode: 'static'
        });

    } else {
        mix.js('resources/assets/js/app.js', 'public/js/app.js')
            .combine([
                plugin + 'jquery/jquery.min.js',
                plugin + 'popper/popper.min.js',
                plugin + 'slimscroll/jquery.slimscroll.js',
                plugin + 'waves/waves.js',
                plugin + 'sidebarmenu.js',
                plugin + 'sticky-kit/sticky-kit.min.js',
                'resources/assets/js/custom.js',
                'public/js/app.js',
            ], 'public/js/bundle.min.js')
            .sass('resources/assets/sass/style.scss', 'public/css', {
                implementation: require('sass')
            })
            .sass('resources/assets/sass/financier.scss', 'public/css', {
                implementation: require('sass')
            });
    }
} catch (e) {
    // On gère le cas où on a une exception
    mix.js('resources/assets/js/app.js', 'public/js/app.js')
        .combine([
            plugin + 'jquery/jquery.min.js',
            plugin + 'bootstrap/bootstrap.min.js',
            plugin + 'slimscroll/jquery.slimscroll.js',
            plugin + 'waves/waves.js',
            plugin + 'sidebarmenu.js',
            plugin + 'sticky-kit/sticky-kit.min.js',
            'resources/assets/js/custom.js',
            'public/js/app.js',
        ], 'public/js/bundle.min.js')
        .sass('resources/assets/sass/style.scss', 'public/css', {
            implementation: require('sass')
        })
        .sass('resources/assets/sass/financier.scss', 'public/css', {
            implementation: require('sass')
        });
} finally {
    let config = {
        output: {
            publicPath: '/',
            chunkFilename: 'js/chunks/[name].[chunkhash].js'
        },
        resolve: {
            alias: {
                '@Models': path.resolve(__dirname, 'resources/assets/js/models'),
                '@Mixins': path.resolve(__dirname, 'resources/js/mixins'),
                '@Financier': path.resolve(__dirname, 'resources/assets/js/views/bp/financier'),
                '@Layouts': path.resolve(__dirname, 'resources/js/views/layouts')
            }
        },
        plugins: [
            new LiveReloadPlugin(),
        ],
        node: {
            child_process: 'empty',
            fs: 'empty'
        },
        // devtool: process.env.NODE_ENV === 'production' ? 'none' : 'inline-source-map'
        // devtool: process.env.NODE_ENV === 'production' ? 'none' : 'inline-cheap-source-map'
        devtool: process.env.NODE_ENV === 'production' ? 'none' : 'cheap-module-source-map'
    };

    mix.webpackConfig(config);
}
