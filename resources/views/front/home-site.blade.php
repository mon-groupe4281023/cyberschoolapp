@extends('layouts.master-site')
@section('css')
    <!-- Plugins CSS -->
  
    <link rel="stylesheet" href="{{asset('site/css/revslider/settings.css')}}">
    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{asset('site/css/styles.css')}}">
        <!-- Custom CSS -->
    {{-- <link rel="stylesheet" href="{{asset('site/css/customizer/pages.css')}}"> --}}
    <!-- IE Styles-->
    {{-- <link rel='stylesheet' href="{{asset('site/css/ie/ie.css')}}"> --}}
  
   
@endsection
@section('js')
        <!-- All Elements -->
		<script src="{{asset('site/js/jquery.appear.js')}}"></script>
        <!-- Liste des Sevices -->
		
        <!-- Slide top and bottom page -->
		<script src="{{asset('site/js/main.js')}}"></script>
      
        <!-- Slide bottom -->
		<script src="{{asset('site/js/jquery.touchwipe.min.js')}}"></script>
		<script src="{{asset('site/js/jquery.carouFredSel-6.2.1-packed.js')}}"></script>
        <!-- Slide top -->
        {{-- <script src="{{asset('site/js/revolution/jquery.themepunch.revolution.min.js')}}"></script>
        <script src="{{asset('site/js/revolution/jquery.themepunch.tools.min.js')}}"></script> --}}
@endsection