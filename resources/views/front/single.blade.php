@extends('layouts.master-site')
@section('css')
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="{{asset('site/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('site/css/animate.css')}}">
    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{asset('site/css/styles.css')}}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{asset('site/css/customizer/pages.css')}}">
    <!-- IE Styles-->
    <link rel='stylesheet' href="{{asset('site/css/ie/ie.css')}}">
@endsection
@section('js')
    <script src="{{asset('site/js/main.js')}}"></script>
@endsection
