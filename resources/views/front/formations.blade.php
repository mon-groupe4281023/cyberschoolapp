@extends('layouts.master-site')
@section('css')
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="{{asset('site/css/bootstrap.css')}}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{asset('site/css/customizer/pages.css')}}">
    <link rel="stylesheet" href="{{asset('site/css/animate.css')}}">
    <!-- IE Styles-->
    <link rel='stylesheet' href="{{asset('site/css/ie/ie.css')}}">
    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{asset('site/css/style.css')}}">
@endsection
@section('js')
        <!-- All Elements -->
		<script src="{{asset('site/js/jquery.appear.js')}}"></script>
        <script src="{{asset('site/js/main.js')}}"></script>
@endsection