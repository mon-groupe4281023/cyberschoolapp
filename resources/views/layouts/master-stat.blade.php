<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Business-Plan-stat</title>
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        
        <!-- Hotjar implement -->
        <script>
            (function(h,o,t,j,a,r){
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid:2719996,hjsv:6};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
            })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
        </script>

        <!-- Bootstrap Core CSS -->
        <link href="{{ asset('bp/css/pages/login-register-lock.css')}}" rel="stylesheet">
        <link href="{{asset('bp/assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <!-- This page CSS -->
        <!-- chartist CSS -->
        <link href="{{asset('bp/assets/plugins/morrisjs/morris.css')}}" rel="stylesheet">
        <link href="{{asset('bp/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css')}}" rel="stylesheet">
        <!--c3 CSS -->
        <link href="{{asset('bp/assets/plugins/c3-master/c3.min.css')}}" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="{{asset('bp/css/style_perso.css')}}" rel="stylesheet">
        <link href="{{asset('bp/css/style.css')}}" rel="stylesheet">
        <link href="{{asset('bp/colors/blue.css')}}" rel="stylesheet">
        <link href="{{asset('bp/css/introjs.css')}}" id="theme"  rel="stylesheet">
        <!-- Dashboard 1 Page CSS -->
        <link href="{{asset('bp/css/pages/dashboard3.css')}}" rel="stylesheet">
        <!-- You can change the theme colors from here -->
        <link href="{{asset('bp/css/colors/blue.css')}}" id="theme" rel="stylesheet">
       
    </head>
    <body class="fix-header fix-sidebar card-no-border">

        <div class="preloader">
            <div class="loader">
                <div class="loader__figure"></div>
                <p class="loader__label">Chargement...</p>
            </div>
        </div>

        <div id="root">
            <router-view></router-view>
        </div>

        <!-- JQUERY -->
        <script src="{{asset('bp/assets/plugins/jquery/jquery.min.js')}}"></script>
        <script src="{{asset('js/bundle.min.js')}}"></script>
        <!-- Bootstrap popper Core JavaScript -->
        <script src="{{asset('bp/assets/plugins/bootstrap/js/popper.min.js')}}"></script>
        <script src="{{asset('bp/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
        <!-- slimscrollbar scrollbar JavaScript -->
        <script src="{{asset('bp/js/perfect-scrollbar.jquery.min.js')}}"></script>
        <!--Wave Effects -->
        <script src="{{asset('bp/js/waves.js')}}"></script>
        <script src="{{asset('bp/js/sidebarmenu.js')}}"></script>
        <script src="{{asset('bp/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
        <script src="{{asset('bp/assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
        <!--Custom JavaScript -->
        <script src="{{asset('bp/js/custom.min.js')}}"></script>

        <script src="{{asset('bp/assets/plugins/raphael/raphael-min.js')}}"></script>
        <script src="{{asset('bp/assets/plugins/morrisjs/morris.js')}}"></script>
       
        <script src="{{asset('bp/js/morris-data.js')}}"></script>
        
       <!--summary js -->
        <!--Menu sidebar -->
       
       
        

        <!--<script src="//code.tidio.co/xmdqzekyf7kzhfufwxersjvqanjxyuxb.js"></script>-->
    </body>
</html>