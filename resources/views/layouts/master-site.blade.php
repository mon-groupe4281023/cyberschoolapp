<!DOCTYPE html>
<html lang="en" translate="no">
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <!-- Tell the browser to be responsive to screen width -->
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="">
		<meta name="author" content="">
		<link rel="shortcut icon" href="{{ asset('/images/favicon.ico') }}" type="image/x-icon">
	    <title>Cyberschool</title>
	    <meta name="csrf-token" content="{{ csrf_token() }}" />
		<!-- Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
		<!-- Font Icon -->
		<link rel="stylesheet" href="{{ asset('fonts/material-icon/css/material-design-iconic-font.min.css')}}">
		<!-- Main css -->
		<link href="{{ asset('css/app.css') }}">
		<link rel="shortcut icon" href="{{asset('favicon.ico')}}">
		{{-- <link href="{{ asset('/site/css/styles.css') }}" rel="stylesheet"> --}}
		<link href="{{ asset('site/css/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
		{{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous"> --}}

		
        <!-- Hotjar implement -->
        <script>
            (function(h,o,t,j,a,r){
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid:2719996,hjsv:6};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
            })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
        </script>

		@yield('css')
	</head>
	<body class="fix-header fix-sidebar card-no-border">
	    <div id="root">
	        <router-view></router-view>
	    </div>
		<script src="{{asset('/js/bundle.min.js')}}"></script>
		<script src="{{asset('bp/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

		@yield('js')
		
		<script>
			$(document).on('click', '.dropdown-menu', function (e) {
			  e.stopPropagation();
			});
		</script>
	</body>
</html>