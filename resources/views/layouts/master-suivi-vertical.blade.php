<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Business-Plan</title>
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <!-- Bootstrap Core CSS -->
        <link href="{{ asset('suivi/assets/plugins/calendar/dist/fullcalendar.css')}}" rel="stylesheet"/>
        <link href="{{asset('suivi/assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('suivi/assets/plugins/jasny/bootstrap-fileupload.min.css')}}" rel="stylesheet">
        <!-- <link href="{{asset('suivi/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet"> -->
        <!-- <link href="{{asset('suivi/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet"> -->

        <link href="{{asset('suivi/assets/plugins/datatables/media/css/dataTables.bootstrap4.css')}}" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="{{asset('bp/css/style_perso.css')}}" rel="stylesheet">
        <link href="{{asset('suivi/css/style.css')}}" rel="stylesheet">
        <link href="{{asset('suivi/assets/plugins/Magnific-Popup-master/dist/magnific-popup.css')}}" rel="stylesheet">
        <!-- You can change the theme colors from here -->
        <link href="{{asset('bp/css/pages/user-card.css')}}" rel="stylesheet">
        <link href="{{asset('suivi/css/colors/blue.css')}}" id="theme" rel="stylesheet">
        <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
              rel="stylesheet"> -->


    </head>
    <body class="fix-header fix-sidebar card-no-border">

        <div class="preloader">
            <div class="loader">
                <div class="loader__figure"></div>
                <p class="loader__label">Chargement...</p>
            </div>
        </div>

        <div id="root">
            <router-view></router-view>
        </div>
        
        <script src="{{ asset('suivi/assets/plugins/jquery/jquery.min.js')}}"></script>
        
        <!-- Bootstrap popper Core JavaScript -->
        <script src="{{asset('suivi/assets/plugins/popper/popper.min.js')}}"></script>
        
        <script src="{{asset('suivi/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
        <!-- slimscrollbar scrollbar JavaScript -->
        <script src="{{asset('suivi/js/perfect-scrollbar.jquery.min.js')}}"></script>
        <!--Wave Effects -->
        <script src="{{asset('suivi/js/waves.js')}}"></script>
        <!--Custom JavaScript -->
        <script src="{{asset('suivi/js/sidebarmenu.js')}}"></script>
        <!-- Sweet-Alert  -->
        <script src="{{asset('bp/assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
  
        <script src="{{asset('suivi/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
        <script src="{{asset('suivi/assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
        <script src="{{asset('suivi/js/custom.min.js')}}"></script>
        
        <!--Custom JavaScript -->
        <!-- script src="{{asset('suivi/js/custom.min.js')}}"></script -->
        <script src="{{asset('js/bundle.min.js')}}"></script>

        <script src="{{ asset('suivi/assets/plugins/calendar/jquery-ui.min.js')}}"></script>
        <script src="{{ asset('suivi/assets/plugins/moment/moment.js')}}"></script>
        <script src="{{ asset('suivi/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{ asset('suivi/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js')}}"></script>
        <!-- {{-- <script src="{{ asset('suivi/assets/plugins/jasny/bootstrap-fileupload.js')}}"></script> --}} -->

<!-- {{--         <script src="{{ asset('suivi/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script> -->
        <script src="{{ asset('suivi/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>

        <script src="{{ asset('suivi/assets/plugins/datatables/datatables.min.js')}}"></script> --}}
    </body>
</html>