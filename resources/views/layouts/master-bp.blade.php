<!DOCTYPE html>
<html lang="en" translate="no">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Business-Plan</title>
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <!-- Hotjar implement -->
        <script>
            (function(h,o,t,j,a,r){
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid:2719996,hjsv:6};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
            })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
        </script>

        <!-- Bootstrap Core CSS -->
        <link href="{{ asset('bp/css/pages/login-register-lock.css')}}" rel="stylesheet">
        <link href="{{asset('bp/assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <!-- This page CSS -->
        <!-- chartist CSS -->
        <link href="{{asset('bp/assets/plugins/chartist-js/dist/chartist.min.css')}}" rel="stylesheet">
        <link href="{{asset('bp/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css')}}" rel="stylesheet">
        <!--c3 CSS -->
        <link href="{{asset('bp/assets/plugins/c3-master/c3.min.css')}}" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="{{asset('bp/css/style_perso.css')}}" rel="stylesheet">
        <link href="{{asset('bp/css/style.css')}}" rel="stylesheet">
        <link href="{{asset('bp/css/introjs.css')}}" rel="stylesheet">
        <!-- Dashboard 1 Page CSS -->
        <link href="{{asset('bp/css/pages/dashboard3.css')}}" rel="stylesheet">
      
        <!-- SUMMARY -->
        <link href="{{asset('bp/assets/plugins/wizard/steps.css')}}" rel="stylesheet" />
        <link href="{{ asset('bp/assets/plugins/summernote/dist/summernote-bs4.css')}}" rel="stylesheet"/>
        <link href="{{ asset('bp/assets/plugins/datatables/media/css/dataTables.bootstrap4.css')}}" rel="stylesheet"/>
        <link href="{{ asset('bp/assets/plugins/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css')}}" rel="stylesheet"/>
        <link href="{{ asset('bp/css/pages/card-page.css')}}" rel="stylesheet"/>
        <link href="{{ asset('bp/css/pages/stylish-tooltip.css')}}" rel="stylesheet"/>
        <link href="{{ asset('bp/css/pages/pricing-page.css')}}" rel="stylesheet"/> 
        {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet">       --}}

    </head>
    <body class="fix-header fix-sidebar card-no-border">

        <div class="preloader">
            <div class="loader">
                <div class="loader__figure"></div>
                <p class="loader__label">Chargement...</p>
            </div>
        </div>

        <div id="root">
            <router-view></router-view>
        </div>

        <!--[if (!IE)|(gt IE 8)]><!-->
        <script src="{{asset('site/js/jquery-3.0.0.min.js')}}"></script>
        <!--<![endif]-->
        <script src="{{asset('js/bundle.min.js')}}"></script>
        <!--[if lte IE 8]>
        <script src="{{asset('site/js/jquery-1.9.1.min.js')}}"></script>
        <![endif]-->

        <!-- Bootstrap popper Core JavaScript -->
        <script src="{{asset('bp/assets/plugins/bootstrap/js/popper.min.js')}}"></script>
        <script src="{{asset('bp/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
        <!-- slimscrollbar scrollbar JavaScript -->
        <script src="{{asset('bp/js/perfect-scrollbar.jquery.min.js')}}"></script>
        <!--Wave Effects -->
        <script src="{{asset('bp/js/waves.js')}}"></script>
        <!-- <script src="{{ asset('bp/assets/plugins/Material/material-icons.min.js')}}"></script> -->
        <!--Custom JavaScript -->
        <script src="{{asset('bp/js/custom.min.js')}}"></script>
        <!--Intro Js -->
        <script src="{{asset('bp/js/intro.js')}}"></script>


        <!-- This page plugins -->
        <!--sparkline JavaScript -->
        <script src="{{asset('bp/assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
        <!--morris JavaScript -->
        <script src="{{asset('bp/assets/plugins/chartist-js/dist/chartist.min.js')}}"></script>
        <script src="{{asset('bp/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js')}}"></script>

    <!--summary js -->
        <!--Menu sidebar -->
        <script src="{{asset('bp/js/sidebarmenu.js')}}"></script>
        <script src="{{asset('bp/assets/plugins/wizard/jquery.validate.min.js')}}"></script>
        <!-- Sweet-Alert  -->
        <script src="{{asset('bp/assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
        {{-- <script src="{{asset('bp/assets/plugins/wizard/steps.js')}}"></script> --}}
        <script src="{{asset('bp/assets/plugins/summernote/dist/summernote-bs4.min.js')}}"></script>

        <!-- DATATABLES -->
        <script src="{{ asset('bp/assets/plugins/datatables/datatables.min.js')}}"></script>
        <script src="{{ asset('bp/assets/plugins/jquery-datatables-editable/jquery.dataTables.js')}}"></script>
        <script src="{{ asset('bp/assets/plugins/datatables/media/js/dataTables.bootstrap.js')}}"></script>
        <script scr="{{ asset('bp/assets/plugins/tiny-editable/mindmup-editabletable.js')}}"></script>
        
        <script scr="{{ asset('bp/assets/plugins/calendar/jquery-ui.min.js')}}"></script>
        <script scr="{{ asset('bp/assets/plugins/moment/moment.js')}}"></script>
        <script scr="{{ asset('bp/assets/plugins/calendar/dist/fullcalendar.min.js')}}"></script>
        <script scr="{{ asset('bp/assets/plugins/calendar/dist/cal-init.js')}}"></script>

        <script>
            /*$(document).ready(function() {
                $('#myTable').DataTable({
                    language: {
                        // processing:     "Traitement en cours...",
                        "processing": "<div></div><div></div><div></div><div></div><div></div>",
                        search:         "Rechercher&nbsp;:",
                        lengthMenu:    " Affichage par page _MENU_",
                        info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                        infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                        infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                        infoPostFix:    "",
                        loadingRecords: "Chargement en cours...",
                        zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                        emptyTable:     "Aucune donnée disponible dans le tableau",
                        paginate: {
                            "first":      "Premier",
                            "previous":   "Précédent",
                            "next":       "Suivant",
                            "last":       "Dernier"
                        },
                        aria: {
                            sortAscending:  ": activer pour trier la colonne par ordre croissant",
                            sortDescending: ": activer pour trier la colonne par ordre décroissant"
                        }
                    },
                });
            });*/
        </script>

        <!--<script src="//code.tidio.co/xmdqzekyf7kzhfufwxersjvqanjxyuxb.js"></script>-->
    </body>
</html>