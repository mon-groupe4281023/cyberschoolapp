@extends('app')
@section('title')
    <title>RV admin | Secteur</title>
@endsection
@section('bodyClass')
    class="hold-transition skin-red sidebar-mini"
@endsection
@section('header')
    @include('back.header')
@endsection
@section('sidebar')
    @include('back.sidebar')
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Branche du secteur d'activité
            <small>Liste</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Accueil</a></li>
            <li class="active"> Branche du secteur d'activité</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Branche du secteur d'activité</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                        class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="box-body">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="name">Secteur d'activité :</label>
                                        <input type="hidden" class="form-control" id="inputId" name="inputId">
                                        <select class="form-control" name="secteur" id="secteur">
                                            <option value="1" >Secteur primaire</option>
                                            <option value="2" >Secteur secondaire</option>
                                            <option value="3" >Secteur tertiaire</option>
                                            <option value="4" >Secteur quaternaire</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Branche :</label>
                                        <input type="text" class="form-control" name="branche" id="branche" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Code branche :</label>
                                        <input type="text" class="form-control" name="code_branche" id="code_branche" required>
                                    </div>
                                    <div class="box-footer">
                                        <button type="submit" name="btnSaveBranch" id="btnSaveBranch" class="btn btn-primary">Enregister
                                        </button>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <table id="example" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Secteur</th>
                                            <th>Branche</th>
                                            <th>Code branche</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>Secteur</th>
                                            <th>Branche</th>
                                            <th>Code branche</th>
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('../js/back/branches.js') }}"></script>
@endsection