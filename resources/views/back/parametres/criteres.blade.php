@extends('app')
@section('title')
    <title>RV admin | concours</title>
@endsection
@section('bodyClass')
    class="hold-transition skin-red sidebar-mini"
@endsection
@section('header')
    @include('back.header')
@endsection
@section('sidebar')
    @include('back.sidebar')
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Critères du concours
            <small>Liste</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Accueil</a></li>
            <li class="active"> Critères du concours</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Critères du concours</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                        class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="box-body">
                                <div class="col-sm-4">
                                    <form id="formCritere">
                                        <div class="form-group">
                                            <label for="name">Partie :</label>
                                            <input type="hidden" class="form-control" id="paramId" name="paramId">
                                            <select class="form-control" name="partie" id="partie">
                                                <option value="projet" >Présenation du projet</option>
                                                <option value="porteur" >Présenation du porteur</option>
                                                <option value="produit" >Présenation du produit/service</option>
                                                <option value="bmc" >Business Model Canvas</option>
                                                <option value="chiffre" >Chiffres clés vente-charge</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Critères :</label>
                                            <input type="text" class="form-control tagsinput" data-role="tagsinput" required>
                                        </div>
                                        <div class="box-footer">
                                            <button type="submit" name="btnAssignCritere" id="btnAssignCritere" class="btn btn-primary">Enregister
                                            </button>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-sm-8">
                                    <table id="example" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Partie</th>
                                            <th>Critères</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>Partie</th>
                                            <th>Critères</th>
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('../js/back/parametre.js') }}"></script>
@endsection