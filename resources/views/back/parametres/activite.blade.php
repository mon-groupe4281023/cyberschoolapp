@extends('app')
@section('title')
    <title>RV admin | Activite</title>
@endsection
@section('bodyClass')
    class="hold-transition skin-red sidebar-mini"
@endsection
@section('header')
    @include('back.header')
@endsection
@section('sidebar')
    @include('back.sidebar')
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Activités
            <small>Liste</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Accueil</a></li>
            <li class="active"> Activités</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Activités</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                        class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="box-body">
                                <div class="col-sm-4">
                                    <input type="hidden" class="form-control" id="inputId" name="inputId">
                                    <div class="form-group">
                                        <label for="name">Branche d'activité :</label>
                                        <select class="form-control select2" style="width: 100%;" name="branche" id="branche">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Activité :</label>
                                        <input type="text" class="form-control" name="activite" id="activite" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Code branche :</label>
                                        <input type="text" class="form-control" name="code_activite" id="code_activite" required>
                                    </div>
                                    <div class="box-footer">
                                        <button type="submit" name="btnSaveActivite" id="btnSaveActivite" class="btn btn-primary">Enregister
                                        </button>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <table id="example" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Activité</th>
                                            <th>Branche d'activité</th>
                                            <th>Code</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>Activité</th>
                                            <th>Branche d'activité</th>
                                            <th>Code</th>
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('../js/back/activites.js') }}"></script>
@endsection