@extends('app')
@section('bodyClass')
    class="hold-transition skin-red sidebar-mini"
@endsection
@section('header')
    @include('back.header')
@endsection
@section('sidebar')
    @include('back.sidebar')
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Utilisateurs
            <small>Nouveau</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Accueil</a></li>
            <li class="active">Utilisateurs</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Nouvel utilisateur</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                        class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="box-body">
                                <form role="form" id="formUsers">
                                    <div class="row">
                                        <div class="box-body">
                                            <div class="col-sm-8">
                                                <div class="form-group">
                                                    <label for="name">Nom (s) :</label>
                                                    <input type="text" class="form-control" id="name" name="name" placeholder="Nom" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="prenom">Prénom (s) :</label>
                                                    <input type="text" class="form-control" id="prenom" name="prenom" placeholder="Prenom">
                                                </div>
                                                <div class="form-group">
                                                    <label for="prenom">Sexe :</label>
                                                    <select class="form-control" name="sexe" id="sexe">
                                                        <option value="M" >Masculin</option>
                                                        <option value="F" >Féminin</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="Email1">Date de naissance :</label>
                                                    <input type="text" class="form-control datepicker" name="dateNaiss" id="dateNaiss">
                                                </div>
                                                <div class="form-group">
                                                    <label for="tel">Téléphone :</label>
                                                    <input type="tel" class="form-control" id="tel" name="tel" placeholder="Téléphone" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="Email1">Email</label>
                                                    <input type="email" class="form-control" autocomplete="off" name="email1" id="email1" placeholder="Email">
                                                </div>
                                                <div class="form-group">
                                                    <label for="password">Mot de passe</label>
                                                    <input type="password" class="form-control" autocomplete="off" name="password" id="password" placeholder="Mot de passe">
                                                </div>
                                                <div class="form-group">
                                                    <label for="Profil">Profil</label>
                                                    <select class="form-control" name="profil" id="profil">
                                                        <option value="1" >Admin</option>
                                                        <option value="2" >Evaluateur</option>
                                                        <option value="3" >User</option>
                                                        <option value="4" >Moniteur</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="fileupload fileupload-new pull-right" data-provides="fileupload">
                                                    <div class="fileupload-new thumbnail" style="width:190px; height: 170px;"><img src="{{asset('back/dist/img/user_x.png')}}" alt=""style="max-width: 190px; max-height: 170px; line-height: 10px;" /></div>
                                                    <div class="fileupload-preview fileupload-exists thumbnail" style="width:190px; height: 170px;max-width: 190px; max-height: 170px; line-height: 10px;"></div>
                                                    <div style="text-align:center;">
										<span class="btn btn-file btn-primary"><span class="fileupload-new">Choisir une autre photo</span>
										<span class="fileupload-exists">Changer</span><input type="file" name="photo_profil" id="photo_profil"/></span>
                                                        <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Supprimer</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <button type="submit" name="btnSave" class="btn btn-primary">Enregister</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Liste des utilisateurs</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                        class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="box-body">
                                <div class="col-sm-12">
                                    <table id="example" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Nom(s)</th>
                                            <th>Prénom(s)</th>
                                            <th>Profil</th>
                                            <th>Téléphone</th>
                                            <th>Email</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>Nom(s)</th>
                                            <th>Prénom(s)</th>
                                            <th>Profil</th>
                                            <th>Téléphone</th>
                                            <th>Email</th>
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalEditUser">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Modification Utilisateur</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label for="name">Nom (s) :</label>
                                        <input type="text" class="form-control" id="nameM" name="name" placeholder="Nom" required>
                                        <input type="hidden" class="form-control" id="idUser" name="idUser">
                                    </div>
                                    <div class="form-group">
                                        <label for="prenom">Prénom (s) :</label>
                                        <input type="text" class="form-control" id="prenomM" name="prenom" placeholder="Prenom">
                                    </div>
                                    <div class="form-group">
                                        <label for="prenom">Sexe :</label>
                                        <select class="form-control" name="sexe" id="sexeM">
                                            <option value="M" >Masculin</option>
                                            <option value="F" >Féminin</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="Email1">Date de naissance :</label>
                                        <input type="text" class="form-control datepicker" name="dateNaiss" id="dateNaissM">
                                    </div>
                                    <div class="form-group">
                                        <label for="tel">Téléphone :</label>
                                        <input type="tel" class="form-control" id="telM" name="tel" placeholder="Téléphone" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="Email1">Email</label>
                                        <input type="email" class="form-control" name="email1" id="emailM" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <label for="paswword">Mot dde passe :</label>
                                        <input type="paswword" class="form-control" id="paswwordM" name="paswword" placeholder="Mot de passe" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="Profil">Profil</label>
                                        <select class="form-control" name="profil" id="profilM">
                                            <option value="1" >Admin</option>
                                            <option value="2" >Evaluateur</option>
                                            <option value="3" >User</option>
                                            <option value="4" >Moniteur</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <input type="hidden" class="form-control" id="name" name="">
                                    <div class="fileupload fileupload-new pull-right" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width:190px; height: 170px;">
                                            <img id="userPhotoLoaded" src="{{asset('back/dist/img/user_x.png')}}" alt="" style="max-width: 190px; max-height: 170px; line-height: 10px;" />
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" style="width:190px; height: 170px;max-width: 190px; max-height: 170px; line-height: 10px;"></div>
                                        <div style="text-align:center;">
										<span class="btn btn-file btn-primary"><span class="fileupload-new">Choisir une autre photo</span>
										<span class="fileupload-exists">Changer</span>
                                            <input type="file" name="photo_profil" id="photo_profil"/>
                                        </span>
                                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Supprimer</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="modalBtnEditUser" class="btn btn-primary">Enregistrer</button>
                            <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Fermer</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('../js/back/users.js') }}"></script>
@endsection