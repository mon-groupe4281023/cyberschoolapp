
@extends('app')
@section('bodyClass')
    class="hold-transition skin-red sidebar-mini"
@endsection
@section('header')
    @include('back.header')
@endsection
@section('sidebar')
    @include('back.sidebar')
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Tableau de bord
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
            <li class="active">Tabelau de bord</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3 id="projet"></h3>

                        <p>Projets</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                <a href="{{url('/admin/projets/list')}}" class="small-box-footer">Plus de détail <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3 id="projetEvalue"><sup style="font-size:20px"></sup></h3>

                        <p>Projet Evalués</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="{{url('/admin/projets/evalues')}}" class="small-box-footer">Plus de détail <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3 id="utilisateurs"></h3>

                        <p>Utilisateurs</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                <a href="{{url('/admin/utilisateurs')}}" class="small-box-footer">Plus de détail <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3 id="evaluateurs"></h3>
                        <p>Evaluateurs</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="#" class="small-box-footer">Plus de détail <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
            </section>
        </div>
    </section>
@endsection
@section('footer')
    <footer class="main-footer">
        <strong>Artisé par <a target='_blank' href="https://www.cyberschoolgabon.com">CYBERSCHOOL-ENTREPRENEURIAT</a>.</strong> Tous
        droits reservés.
    </footer>
@endsection
@section('script')
    <script type="text/javascript" src="{{asset('../js/back/dashboard.js') }}"></script>
@endsection