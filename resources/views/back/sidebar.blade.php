<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{session('admin')[0]['photoURLAdmin']}}" class="img-circle" alt="{{session('admin')[0]['displayNameAdmin']}}"/>
            </div>
            <div class="pull-left info">
                <p>{{session('admin')[0]['displayNameAdmin']}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> En ligne</a>
            </div>
        </div>
        {{--<form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
            </div>
        </form>--}}
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">TABLEAU DE BORD</li>
            <li class="">
                <a href="{{url('admin/accueil')}}">
                    <i class="fa fa-dashboard"></i> <span>Tableau de bord</span>
                </a>
            </li>
            <li class="header">ADMINISTRATION</li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cog"></i> <span>Paramétrages</span>
                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{url('/admin/parametres/concours')}}"><i class="fa fa-circle-o"></i> Concours</a>
                    </li>
                    <li>
                        <a href="{{url('/admin/parametres/branches')}}"><i class="fa fa-circle-o"></i> Branches</a>
                    </li>
                    <li>
                        <a href="{{url('/admin/parametres/activites')}}"><i class="fa fa-circle-o"></i> Activités</a>
                    </li>
                    {{-- <li><a href="{{url('/admin/concours/criteres/{id}')}}"><i class="fa fa-circle-o"></i> Critères</a>
                    </li> --}}
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-shopping-cart"></i>
                    <span>Projets</span>
                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{url('/admin/attribution/projet')}}"><i class="fa fa-circle-o"></i> Attribuer un projet</a></li>
                    <li><a href="{{url('/admin/projets/list')}}"><i class="fa fa-circle-o"></i> liste</a></li>
                    <li><a href="{{url('/admin/projets/evalues')}}"><i class="fa fa-circle-o"></i> Liste projets évalués</a></li>
                </ul>
            </li>
           
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user"></i> <span>Utilisateurs</span>
                    <span class="pull-right-container">
                                     <i class="fa fa-angle-left pull-right"></i>
                                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{url('/admin/utilisateurs')}}"><i class="fa fa-circle-o"></i> Nouveau </a></li>
                </ul>
            </li>
        
            {{-- <li class="treeview">
                <a href="#">
                    <i class="fa fa-question"></i> <span>FAQ</span>
                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{url('/admin/faq')}}"><i class="fa fa-circle-o"></i> Nouvelle question</a>
                    </li>
                </ul>
            </li> --}}
            </li>
        
        </ul>
    </section>
</aside>