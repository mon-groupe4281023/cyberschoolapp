@extends('app')
@section('title')
    <title>RV admin | Commandes</title>
@endsection
@section('bodyClass')
    class="hold-transition skin-red sidebar-mini"
@endsection
@section('header')
    @include('back.header')
@endsection
@section('sidebar')
    @include('back.sidebar')
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Commandes
            <small>Liste</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Accueil</a></li>
            <li class="active">Commandes</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Liste des commandes</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                        class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="box-body">
                                <div class="col-sm-12">
                                    <table id="example" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Code</th>
                                            <th>Prix</th>
                                            <th>Téléphone</th>
                                            <th>Zone</th>
                                            <th>Entreprise</th>
                                            <th>Paiement</th>
                                            <th>Livraison</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>Code</th>
                                            <th>Prix</th>
                                            <th>Téléphone</th>
                                            <th>Zone</th>
                                            <th>Entreprise</th>
                                            <th>Paiement</th>
                                            <th>Livraison</th>
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalDetail">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Détail de la commande</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="exampled" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Libellé</th>
                                            <th>Prix</th>
                                            <th>Quantité</th>
                                            <th>Prix total</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>Libellé</th>
                                            <th>Prix</th>
                                            <th>Quantité</th>
                                            <th>Prix total</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="modalBtnEditPrdt" class="btn btn-primary">Enregistrer</button>
                            <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Fermer</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('../js/orders.js') }}"></script>
@endsection