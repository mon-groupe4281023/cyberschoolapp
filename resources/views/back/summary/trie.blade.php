@extends('app')
@section('title')
    <title>BP Cyberschool | Summary</title>
@endsection
@section('bodyClass')
    class="hold-transition skin-red sidebar-mini"
@endsection
@section('header')
    @include('back.header')
@endsection
@section('sidebar')
    @include('back.sidebar')
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Projets
            <small>Liste</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Accueil</a></li>
            <li class="active">Projets</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Liste des projets triés</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                        class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="box-body">
                                <div class="col-sm-12">
                                    <div id="grid">
                                    </div>
                                    <div class="" style="margin-top: 20px" id="jqxlistbox">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <input type="button" value="Exporter en Excel" id='excelExport' class="btn btn-success"/>
                            <input type="button" value="Exporter en PDF" id='pdfExport' class="btn btn-danger"/>
                        
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalEditProjet">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Modification Produit</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="name">Numero :</label>
                                        <input type="hidden" class="form-control" id="id" name="id" required>
                                        <input type="text" class="form-control" id="numero" name="numero" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Titre :</label>
                                        <input type="text" class="form-control" id="titre" name="titre"
                                               placeholder="titre" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Forme juridique :</label>
                                        <input type="text" class="form-control" id="formJuridique" name="formJuridique" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Choisir un évaluateur :</label>
                                        <select class="form-control" name="evaluateur" id="evaluateur">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="modalBtnAssignEval" class="btn btn-primary">Assigner</button>
                            <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Fermer</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type="text/javascript">
        function generatedata() {
            // prepare the data
            var data = new Array();
            var dat = new Array();
            var i = 0;
            var j = 0;
            firestore.collection('summary').get().then((results) => {
                results.forEach((sum) => {
                    console.log(sum);
                    /* firestore.collection('evaluation')
                    .where('summary', '==', firestore.collection('summary').doc(sum.id))
                    .get().then((docSnaps) => {
                        docSnaps.forEach((doc) => {
                            var projet = {};
                            projet.identifiant = sum.data().identifiant;
                            projet.titre = sum.data().titre;
                            projet.sexe = sum.data().porteur.sexe;
                            projet.projet = doc.data().projet.note1;
                            projet.porteur = doc.data().porteur.note2;
                            projet.produit = doc.data().produit.note3;
                            projet.bmc = doc.data().bmc.note4;
                            projet.chiffre = doc.data().chiffre.note5;
                            projet.moyenne = doc.data().note_global;
                            console.log('-----------');
                            console.log(doc.data());
                            data[i] = projet;
                        });
                    }); */
                });
            });
            
            return data;  
        };
        function getData()
        {
            var summary = {};
            var evaluations = {};
            var data = new Array();

            firestore.collection('summary').get().then((results) => {
                results.forEach((doc) => {
                    summary[doc.id] = doc.data();
                });
                    sum = firestore.collection('evaluation');
                    sum.get().then((docSnaps) => {
                        var arrObj = [];
                        docSnaps.forEach((doc) => {
                            evaluations = doc.data();
                            var projet = {};
                            var id = evaluations.summary.id;
                            projet.titre = summary[id].titre;
                            projet.description = summary[id].descrSynthetique;
                            projet.identifiant = summary[id].identifiant;
                            projet.formJuridique = summary[id].formJuridique;
                            projet.statutActivite = summary[id].statutActivite;
                            projet.heureTravaille = summary[id].heureTravaille;
                            projet.nom = summary[id].porteur.nomPorteur;
                            projet.sexe = summary[id].porteur.sexe;
                            projet.age = summary[id].porteur.age;
                            projet.niveauPorteur = summary[id].porteur.niveauPorteur;
                            projet.projet = evaluations.projet.note1;
                            projet.porteur = evaluations.porteur.note2;
                            projet.produit = evaluations.produit.note3;
                            projet.bmc = evaluations.bmc.note4;
                            projet.chiffre = evaluations.chiffre.note5;
                            projet.moyenne = evaluations.note_global;
                            if(evaluations.concours){
                                projet.concours=evaluations.concours;
        
                            }
                            else{
                                projet.concours="FERP";
                            };
                            arrObj.push(projet);
                    });
                    data= arrObj;
                    var source =
                    {
                        localdata: data,
                        datafields:
                        [
                            { name: 'identifiant', type: 'string' },
                            { name: 'titre', type: 'string' },
                            { name: 'formJuridique', type: 'string' },
                            { name: 'statutActivite', type: 'string' },
                            { name: 'heureTravaille', type: 'string' },
                            { name: 'nom', type: 'string' },
                            { name: 'sexe', type: 'string' },
                            { name: 'age', type: 'string' },
                            { name: 'niveauPorteur', type: 'string' },
                            { name: 'projet', type: 'number' },
                            { name: 'porteur', type: 'number' },
                            { name: 'produit', type: 'number' },
                            { name: 'bmc', type: 'number' },
                            { name: 'chiffre', type: 'number' },
                            { name: 'moyenne', type: 'number' },
                            { name: 'description', type: 'string' },
                            { name: 'concours', type: 'string' }
                            
                        ],
                        datatype: "array"
                    };
                    var adapter = new $.jqx.dataAdapter(source);
                    $("#grid").jqxGrid(
                    {
                        // width: 1050,
                        width: '100%',
                        height: 'auto',
                        source: adapter,
                        filterable: true,
                        sortable: true,
                        pageable: true,
                        autoheight: true,
                        localization: getLocalization('fr'),
                        pagesizeoptions: ['10', '20', '30', '40', '50'],
                        autoshowfiltericon: true,
                        columns: [
                        { text: 'Identifiant', datafield: 'identifiant', width: '10%',  hidden: true},
                        { text: 'Projet', datafield: 'titre', width: '20%' },
                        { text: 'Forme juridique', datafield: 'formJuridique', width: '10%',  hidden: true },
                        { text: 'Situation actuelle', datafield: 'statutActivite', width: '10%' },
                        { text: 'Temps de travaille', datafield: 'heureTravaille', width: '10%',  hidden: true },
                        { text: 'Nom Porteur', datafield: 'nom', width: '20%' },
                        { text: 'Sexe Porteur', datafield: 'sexe', width: '10%' },
                        { text: 'Age Porteur', datafield: 'age', width: '10%' },
                        { text: 'Niveau Porteur', datafield: 'niveauPorteur', width: '10%',  hidden: true },
                        { text: 'Note Projet', datafield: 'projet', width: '7%', cellsalign: 'center'},
                        { text: 'Note Porteur', datafield: 'porteur', width: '7%', cellsalign: 'center' },
                        { text: 'Note Produit', datafield: 'produit', width: '7%', cellsalign: 'center'},
                        { text: 'Note BMC', datafield: 'bmc', width: '5%', cellsalign: 'center'},
                        { text: 'Note Chiffres clés', datafield: 'chiffre', width: '7%', cellsalign: 'center'},
                        { text: 'Moyenne', datafield: 'moyenne', width: '7%', cellsalign: 'center'},
                        { text: 'Description', datafield: 'description', width: '20%' },
                        { text: 'Concours', datafield: 'concours', width: '20%' },
                        ]
                    });
                });
            });
            // return data;
        };
        $(document).ready(function () {
            getData();

            var addfilter = function () {
                var filtergroup = new $.jqx.filter();

                // apply the filters.
                $("#grid").jqxGrid('applyfilters');
            }

            var listSource = [
                { label: 'Identifiant', value: 'identifiant', checked: false }, 
                { label: 'Projet', value: 'titre', checked: true }, 
                { label: 'Description', value: 'description', checked: false }, 
                { label: 'Forme Juridique', value: 'formJuridique', checked: false }, 
                { label: 'Situation actuelle', value: 'statutActivite', checked: false }, 
                { label: 'Heure travaillée', value: 'heureTravaille', checked: false},
                { label: 'Porteur', value: 'nom', checked: true},
                { label: 'Sexe', value: 'sexe', checked: true},
                { label: 'Age', value: 'age', checked: false},
                { label: 'Niveau', value: 'niveauPorteur', checked: false},
                { label: 'Note projet', value: 'projet', checked: true},
                { label: 'Note porteur', value: 'porteur', checked: true},
                { label: 'Note produit', value: 'produit', checked: true},
                { label: 'Note BMC', value: 'bmc', checked: true},
                { label: 'Note chiffre', value: 'chiffre', checked: true},
                { label: 'Moyenne', value: 'moyenne', checked: true},
                { label: 'Concours', value: 'concours', checked: true},
            ];

            $("#jqxlistbox").jqxListBox({ source: listSource, width: 200, height: 200,  checkboxes: true });
            $("#jqxlistbox").on('checkChange', function (event) {
                $("#grid").jqxGrid('beginupdate');
                if (event.args.checked) {
                    $("#grid").jqxGrid('showcolumn', event.args.value);
                }
                else {
                    $("#grid").jqxGrid('hidecolumn', event.args.value);
                }
                $("#grid").jqxGrid('endupdate');
            });

            // button d'export
            $("#excelExport").jqxButton();
            $("#pdfExport").jqxButton();

            $("#excelExport").click(function () {
                $("#grid").jqxGrid('exportdata', 'xls', 'jqxGrid');           
            });
        
            $("#pdfExport").click(function () {
                $("#grid").jqxGrid('exportdata', 'pdf', 'jqxGrid');
            });


            $('#grid').jqxPanel({ width: 1050, height: 80});

            $("#grid").on("filter", function (event) {
                $("#events").jqxPanel('clearcontent');
                var filterinfo = $("#grid").jqxGrid('getfilterinformation');

                var eventData = "Triggered 'filter' event";
                for (i = 0; i < filterinfo.length; i++) {
                    var eventData = "Filter Column: " + filterinfo[i].filtercolumntext;
                    $('#events').jqxPanel('prepend', '<div style="margin-top: 5px;">' + eventData + '</div>');
                }
            });

            $('#clearfilteringbutton').jqxButton({ height: 25});
            $('#filterbackground').jqxCheckBox({ checked: true, height: 25});
            $('#filtericons').jqxCheckBox({ checked: false, height: 25});
            // clear the filtering.
            $('#clearfilteringbutton').click(function () {
                $("#grid").jqxGrid('clearfilters');
            });
            // show/hide filter background
            $('#filterbackground').on('change', function (event) {
                $("#grid").jqxGrid({ showfiltercolumnbackground: event.args.checked });
            });
            // show/hide filter icons
            $('#filtericons').on('change', function (event) {
                $("#grid").jqxGrid({ autoshowfiltericon: !event.args.checked });
            });
        });
    </script>
@endsection