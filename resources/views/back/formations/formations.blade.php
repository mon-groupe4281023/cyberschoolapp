@extends('app')
@section('title')
    <title>Cyberschool | Formation</title>
@endsection
@section('bodyClass')
    class="hold-transition skin-red sidebar-mini"
@endsection
@section('header')
    @include('back.header')
@endsection
@section('sidebar')
    @include('back.sidebar')
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Formations
            <small>Liste</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Accueil</a></li>
            <li class="active">Toutes les Formations</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Liste des Formations</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                        class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="box-body">
                                <div class="col-sm-12">
                                    <div id="grid">
                                    </div>
                                    <div class="form-inline-checkbox" style="margin-top: 20px" id="jqxlistbox">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <input type="button" value="Exporter en Excel" id='excelExport' class="btn btn-success"/>
                            <input type="button" value="Exporter en PDF" id='pdfExport' class="btn btn-danger"/>
                        
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalEditProjet">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Modification Produit</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="name">Numero :</label>
                                        <input type="hidden" class="form-control" id="id" name="id" required>
                                        <input type="text" class="form-control" id="numero" name="numero" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Titre :</label>
                                        <input type="text" class="form-control" id="titre" name="titre"
                                               placeholder="titre" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Forme juridique :</label>
                                        <input type="text" class="form-control" id="formJuridique" name="formJuridique" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Choisir un évaluateur :</label>
                                        <select class="form-control" name="evaluateur" id="evaluateur">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="modalBtnAssignEval" class="btn btn-primary">Assigner</button>
                            <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Fermer</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type="text/javascript">
        function generatedata() {
            // prepare the data
            var data = new Array();
            var dat = new Array();
            var i = 0;
            var j = 0;
            firestore.collection('eleves').get().then((results) => {
                results.forEach((eleve) => {
                    let inscris = {};
                    inscris.id = eleve.id;
                    inscris.nom = eleve.data().nom;
                    inscris.prenom = eleve.data().prenom;
                    inscris.phone = eleve.data().phone;
                    inscris.sexe = eleve.data().sexe;
                    inscris.formation = eleve.data().formation;
                    inscris.horaire = eleve.data().horaire;

                    data[i] = inscris;
                });
            });
            
            return data;  
        };
        function getData()
        {
            var summary = {};
            var evaluations = {};
            var data = new Array();

            firestore.collection('eleves').get().then((results) => {
                var arrObj = [];
                results.forEach((doc) => {
                    let inscrits = {};
                    let id = doc.id;
                    inscrits.nom = doc.data().nom;
                    inscrits.prenom = doc.data().prenom;
                    inscrits.phone = doc.data().phone;
                    inscrits.sexe = doc.data().sexe;
                    inscrits.formation = doc.data().formation;
                    inscrits.horaire = doc.data().horaire;
                    arrObj.push(inscrits);
                });
                    data= arrObj;
                    var source =
                    {
                        localdata: data,
                        datafields:
                        [
                            { name: 'nom', type: 'string' },
                            { name: 'prenom', type: 'string' },
                            { name: 'phone', type: 'string' },
                            { name: 'sexe', type: 'string' },
                            { name: 'formation', type: 'string' },
                            { name: 'horaire', type: 'string' }
                        ],
                        datatype: "array"
                    };
                    var adapter = new $.jqx.dataAdapter(source);
                    $("#grid").jqxGrid(
                    {
                        // width: 1050,
                        width: '100%',
                        height: 'auto',
                        source: adapter,
                        filterable: true,
                        sortable: true,
                        pageable: true,
                        autoheight: true,
                        localization: getLocalization('fr'),
                        autoshowfiltericon: true,
                        columns: [
                            { text: 'Nom', datafield: 'nom', width: '20%'},
                            { text: 'Prenom', datafield: 'prenom', width: '20%' },
                            { text: 'Phone', datafield: 'phone', width: '20%' },
                            { text: 'Sexe', datafield: 'sexe', width: '10%'},
                            { text: 'Formation', datafield: 'formation', width: '20%'},
                            { text: 'Horaire', datafield: 'horaire', width: '11%'}
                        ]
                    });
                });
        };
        $(document).ready(function () {
            getData();

            var addfilter = function () {
                var filtergroup = new $.jqx.filter();

                // apply the filters.
                $("#grid").jqxGrid('applyfilters');
            }

            var listSource = [ 
                { label: 'Nom', value: 'nom', checked: true }, 
                { label: 'Prenom', value: 'prenom', checked: true }, 
                { label: 'Phone', value: 'phone', checked: true }, 
                { label: 'Sexe', value: 'sexe', checked: true},
                { label: 'Formation', value: 'formation', checked: true},
                { label: 'Horaire', value: 'horaire', checked: true},
            ];

            $("#jqxlistbox").jqxListBox({ source: listSource, width: 200, height: 200,  checkboxes: true });
            $("#jqxlistbox").on('checkChange', function (event) {
                $("#grid").jqxGrid('beginupdate');
                if (event.args.checked) {
                    $("#grid").jqxGrid('showcolumn', event.args.value);
                }
                else {
                    $("#grid").jqxGrid('hidecolumn', event.args.value);
                }
                $("#grid").jqxGrid('endupdate');
            });

            // button d'export
            $("#excelExport").jqxButton();
            $("#pdfExport").jqxButton();

            $("#excelExport").click(function () {
                $("#grid").jqxGrid('exportdata', 'xls', 'jqxGrid');           
            });
        
            $("#pdfExport").click(function () {
                $("#grid").jqxGrid('exportdata', 'pdf', 'jqxGrid');
            });


            $('#grid').jqxPanel({ width: 1050, height: 80});

            $("#grid").on("filter", function (event) {
                $("#events").jqxPanel('clearcontent');
                var filterinfo = $("#grid").jqxGrid('getfilterinformation');

                var eventData = "Triggered 'filter' event";
                for (i = 0; i < filterinfo.length; i++) {
                    var eventData = "Filter Column: " + filterinfo[i].filtercolumntext;
                    $('#events').jqxPanel('prepend', '<div style="margin-top: 5px;">' + eventData + '</div>');
                }
            });

            $('#clearfilteringbutton').jqxButton({ height: 25});
            $('#filterbackground').jqxCheckBox({ checked: true, height: 25});
            $('#filtericons').jqxCheckBox({ checked: false, height: 25});
            // clear the filtering.
            $('#clearfilteringbutton').click(function () {
                $("#grid").jqxGrid('clearfilters');
            });
            // show/hide filter background
            $('#filterbackground').on('change', function (event) {
                $("#grid").jqxGrid({ showfiltercolumnbackground: event.args.checked });
            });
            // show/hide filter icons
            $('#filtericons').on('change', function (event) {
                $("#grid").jqxGrid({ autoshowfiltericon: !event.args.checked });
            });
        });
    </script>
@endsection