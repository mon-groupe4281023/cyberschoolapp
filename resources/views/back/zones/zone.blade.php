@extends('app')
@section('title')
    <title>RV admin | zones</title>
@endsection
@section('bodyClass')
    class="hold-transition skin-red sidebar-mini"
@endsection
@section('header')
    @include('back.header')
@endsection
@section('sidebar')
    @include('back.sidebar')
@endsection
@section('content')
    <section class="content-header">
        <h1>
            zones
            <small>Liste</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Accueil</a></li>
            <li class="active">zones</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Nouvelle zone</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                        class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="box-body">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="name">Libellé :</label>
                                        <input type="hidden" class="form-control" id="inputId" name="inputId">
                                        <input type="text" class="form-control" id="libelle" name="libelle"
                                               placeholder="libelle" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="prenom">Description :</label>
                                        <textarea class="form-control" rows="5" id="descr" name="descr"
                                                  placeholder="description"></textarea>
                                    </div>
                                    <div class="box-footer">
                                        <button type="submit" name="btnSave" id="btnSaveZone" class="btn btn-primary">Enregister
                                        </button>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <table id="example" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Désignation</th>
                                            <th>Description</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>Désignation</th>
                                            <th>Description</th>
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('../js/zone.js') }}"></script>
@endsection