@extends('app')
@section('title')
    <title>RV admin | Entreprises</title>
@endsection
@section('bodyClass')
    class="hold-transition skin-red sidebar-mini"
@endsection
@section('header')
    @include('back.header')
@endsection
@section('sidebar')
    @include('back.sidebar')
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Entreprise
            <small>Liste</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Accueil</a></li>
            <li><a href="{{url('/admin/zones')}}"><i class="fa fa-calculator"></i> Zone</a></li>
            <li class="active">Entreprise</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Nouvelle entreprise</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                        class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="box-body">
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label for="name">Libelle :</label>
                                        <input type="text" class="form-control" id="libelle" name="libelle"
                                               placeholder="libelle" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Téléphone :</label>
                                        <input type="text" class="form-control" id="tel" name="tel"
                                               placeholder="Téléphone" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Zone :</label>
                                        <select class="form-control" name="zoneLivr" id="zoneLivr">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Latitude :</label>
                                        <input type="text" class="form-control" id="latitude" name="latitude"
                                               placeholder="Latitude" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Longitude :</label>
                                        <input type="text" class="form-control" id="longitude" name="longitude"
                                               placeholder="Longitude" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="prenom">Description :</label>
                                        <textarea class="form-control" rows="5" id="descr" name="descr"
                                                  placeholder="description"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="fileupload fileupload-new pull-right" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width:190px; height: 170px;"><img
                                                    src="{{asset('images/no_image.jpg')}}" alt=""
                                                    style="max-width: 190px; max-height: 170px; line-height: 10px;"/>
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail"
                                             style="width:190px; height: 170px;max-width: 190px; max-height: 170px; line-height: 10px;"></div>
                                        <div style="text-align:center;">
										<span class="btn btn-file btn-primary"><span class="fileupload-new">Choisir une autre image</span>
										<span class="fileupload-exists">Changer</span><input type="file" id="imageEntr"
                                                                                             name="photo_profil"/></span>
                                            <a href="#" class="btn btn-danger fileupload-exists"
                                               data-dismiss="fileupload">Supprimer</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" name="btnSave" id="btnSaveEnt" class="btn btn-primary">Enregister
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Liste des produits</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                        class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="box-body">
                                <div class="col-sm-12">
                                    <table id="example" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Libellé</th>
                                            <th>Zone</th>
                                            <th>Téléphone</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>Libellé</th>
                                            <th>Zone</th>
                                            <th>Téléphone</th>
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>
                        <div class="box-footer">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalEditEntr">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Modification Entreprise</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label for="name">Libelle :</label>
                                        <input type="hidden" class="form-control" id="idEntr" name="libelle">
                                        <input type="text" class="form-control" id="libelleM" name="libelle"
                                               placeholder="libelle" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Téléphone :</label>
                                        <input type="text" class="form-control" id="telM" name="tel"
                                               placeholder="Téléphone" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Zone :</label>
                                        <select class="form-control" name="zoneLivrM" id="zoneLivrM">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Latitude :</label>
                                        <input type="text" class="form-control" id="latitudeM" name="latitude"
                                               placeholder="Latitude" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Longitude :</label>
                                        <input type="text" class="form-control" id="longitudeM" name="longitude"
                                               placeholder="Longitude" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="prenom">Description :</label>
                                        <textarea class="form-control" rows="5" id="descrM" name="descr"
                                                  placeholder="description"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <input type="hidden" class="form-control" id="name" name="">
                                    <div class="fileupload fileupload-new pull-right" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width:190px; height: 170px;"><img id="imgLoaded"
                                                    src="{{asset('images/no_image.jpg')}}" alt="Pas d'image"
                                                    style="max-width: 190px; max-height: 170px; line-height: 10px;"/>
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail"
                                             style="width:190px; height: 170px;max-width: 190px; max-height: 170px; line-height: 10px;"></div>
                                        <div style="text-align:center;">
										<span class="btn btn-file btn-primary"><span class="fileupload-new">Choisir une autre image</span>
										<span class="fileupload-exists">Changer</span><input type="file" id="imageEntrM"
                                                                                             name="photo_profil"/></span>
                                            <a href="#" class="btn btn-danger fileupload-exists"
                                               data-dismiss="fileupload">Supprimer</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="modalBtnEditEntreprise" class="btn btn-primary">Enregistrer</button>
                            <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Fermer</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('../js/entreprise.js') }}"></script>
@endsection