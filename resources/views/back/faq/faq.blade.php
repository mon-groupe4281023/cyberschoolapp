@extends('app')
@section('title')
    <title>RV admin | zones</title>
@endsection
@section('bodyClass')
    class="hold-transition skin-red sidebar-mini"
@endsection
@section('header')
    @include('back.header')
@endsection
@section('sidebar')
    @include('back.sidebar')
@endsection
@section('content')
    <section class="content-header">
        <h1>
            zones
            <small>Liste</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Accueil</a></li>
            <li class="active">FAQ</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Nouveau</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                        class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="box-body">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="name">Question :</label>
                                        <input type="hidden" class="form-control" id="faqId" name="faqId">
                                        <input type="text" class="form-control" id="question" name="question"
                                               placeholder="Question" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="prenom">Réponse :</label>
                                        <textarea class="form-control" rows="7" id="rep" name="rep"
                                                  placeholder="Réponse"></textarea>
                                    </div>
                                    <div class="box-footer">
                                        <button type="submit" name="btnSave" id="btnSaveFaq" class="btn btn-primary">Enregister
                                        </button>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <table id="example" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Question</th>
                                            <th>Réponse</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>Question</th>
                                            <th>Réponse</th>
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('../js/faq.js') }}"></script>
@endsection