<header class="main-header">
<a href="{{url('/')}}" class="logo" target="_blank">
    <img class="logo img-responsive" src="{{asset('back/dist/img/whitelogo.png')}}"/>
        <span class="logo-mini"><b>RV</b></span>
        <span class="logo-lg"><b>RV</b>RESTAURANT</span>
    </a>
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-envelope-o"></i>
                        <span class="label label-success">1</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">Vous avez 1 message(s)</li>
                        <li>
                            <ul class="menu">
                                <li>
                                    <a href="#">
                                        <div class="pull-left">
                                            <img src="{{session('admin')[0]['photoURLAdmin']}}" class="img-circle"
                                                 alt="{{session('admin')[0]['displayNameAdmin']}}"/>
                                        </div>
                                        <h4>
                                            Support Technique
                                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                        </h4>
                                        <p>Why not buy a new awesome theme?</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer"><a href="#">Voir tous les Messages</a></li>
                    </ul>
                </li>
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-warning">0</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">Vous avez 0 notification(s)</li>
                        <li>
                            <ul class="menu">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer"><a href="#">Tout voir</a></li>
                    </ul>
                </li>
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{session('admin')[0]['displayNameAdmin'] == null ? asset('back/dist/img/server.jpeg'): session('admin')[0]['photoURLAdmin']}}" 
                        class="user-image" alt="{{session('admin')[0]['displayNameAdmin']}}"/>
                        <span class="hidden-xs">{{session('admin')[0]['displayNameAdmin']}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <img src="{{session('admin')[0]['displayNameAdmin'] == null ? asset('back/dist/img/server.jpeg'): session('admin')[0]['photoURLAdmin']}}" 
                            class="img-circle" alt="{{session('admin')[0]['displayNameAdmin'] == null ? asset('back/dist/img/server.jpeg'): session('admin')[0]['displayNameAdmin']}}"/>

                            <p>
                                <?php 
                                    $user = session('admin');
                                    echo $user[0]['displayNameAdmin'];
                                ?>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Profil</a>
                            </div>
                            <div class="pull-right">
                                <button id="btnLogOut" class="btn btn-default btn-flat">Déconnexion</button>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>