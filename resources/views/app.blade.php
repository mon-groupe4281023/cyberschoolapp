<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    @yield('title')
<!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('back/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('back/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('../node_modules/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('../node_modules/fontawesome-free/css/solid.min.css')}}">
    <link rel="stylesheet" href="{{asset('../node_modules/fontawesome-free/css/regular.min.css')}}">
    <link rel="stylesheet" href="{{asset('../node_modules/fontawesome-free/css/fontawesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('back/bower_components/Ionicons/css/ionicons.min.css')}}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('back/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('back/dist/css/AdminLTE.min.css')}}">
    <link rel="stylesheet" href="{{asset('../assets/sass/app.scss')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('back/dist/css/skins/skin-red.css')}}">

    <link rel="icon" type="image/x-icon" href="{{ asset('front/img/favicon/favicon.png') }}">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{asset('back/bower_components/morris.js/morris.css')}}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{asset('back/bower_components/jvectormap/jquery-jvectormap.css')}}">
    <!-- Date Picker -->
    <link rel="stylesheet"
          href="{{asset('back/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
    <!-- Select2 -->
    <link rel="stylesheet"
          href="{{asset('back/bower_components/select2/dist/css/select2.min.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{asset('back/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{asset('back/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
    <!-- bootstrap tagsinput -->
    <link rel="stylesheet" href="{{asset('back/plugins/tagsinput/bootstrap-tagsinput.css')}}">

    <link href="{{asset('back/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css')}}" rel="stylesheet" media="All" />
    <link href="{{asset('back/plugins/pnotify/pnotify.custom.min.css')}}" rel="stylesheet" media="All" />
    <!-- Jsgrid CSS -->
    <link rel="stylesheet" href="{{asset('back/plugins/jqxgrid/jqx.light.css')}}">
    <link rel="stylesheet" href="{{asset('back/plugins/jqxgrid/jqx.base.css')}}">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body @yield('bodyClass')>
<div class="wrapper">
    @yield('header')
    @yield('sidebar')
    {{--<div id="appBack"></div>--}}
    <div class="content-wrapper">
        @yield('content')
    </div>
    <footer class="main-footer">
        <strong>Artisé par <a target='_blank' href="https://www.cyberschoolgabon.com">CYBERSCHOOL-ENTREPRENEURIAT</a>.</strong> Tous
        droits reservés.
    </footer>
</div>

<script src="https://www.gstatic.com/firebasejs/5.9.3/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.9.3/firebase-firestore.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.9.3/firebase-storage.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.9.3/firebase-auth.js"></script>
<script type="text/javascript" src="{{asset('js/back/layout-app.js') }}"></script>


<!-- jQuery 3 -->
<script src="{{asset('back/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('back/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('back/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Select2 -->
<script src="{{asset('back/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- Morris.js charts -->
<script src="{{asset('back/bower_components/raphael/raphael.min.js') }}"></script>
<script src="{{asset('back/bower_components/morris.js/morris.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{asset('back/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('back/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('back/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('back/bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('back/bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{asset('back/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{asset('back/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('back/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('back/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('back/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('back/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('back/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('back/dist/js/adminlte.min.js')}}"></script>

<script src="{{asset('back/plugins/bootstrap-fileupload/bootstrap-fileupload.js')}}"></script>
<script src="{{asset('back/plugins/pnotify/pnotify.custom.min.js')}}"></script>
<script src="{{asset('back/plugins/typeahead/typeahead.bundle.js')}}"></script>
<script src="{{asset('back/plugins/tagsinput/bootstrap-tagsinput.js')}}"></script>


<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('back/dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('back/dist/js/demo.js')}}"></script>
<!-- Jqxgrid js -->
<script src="{{asset('back/plugins/jqxgrid/jqxcore.js')}}"></script>
<script src="{{asset('back/plugins/jqxgrid/jqxdata.js')}}"></script>
<script src="{{asset('back/plugins/jqxgrid/jqxbuttons.js')}}"></script>
<script src="{{asset('back/plugins/jqxgrid/jqxscrollbar.js')}}"></script>
<script src="{{asset('back/plugins/jqxgrid/jqxlistbox.js')}}"></script>
<script src="{{asset('back/plugins/jqxgrid/jqxdropdownlist.js')}}"></script>
<script src="{{asset('back/plugins/jqxgrid/jqxmenu.js')}}"></script>
{{-- <script src="{{asset('back/plugins/jqxgrid/jqx-all.js')}}"></script> --}}
<script src="{{asset('back/plugins/jqxgrid/jqxgrid.js')}}"></script>
<script src="{{asset('back/plugins/jqxgrid/jqxgrid.filter.js')}}"></script>
<script src="{{asset('back/plugins/jqxgrid/jqxgrid.sort.js')}}"></script>
<script src="{{asset('back/plugins/jqxgrid/jqxgrid.selection.js')}}"></script>
<script src="{{asset('back/plugins/jqxgrid/jqxgrid.pager.js')}}"></script>
<script src="{{asset('back/plugins/jqxgrid/jqxdata.export.js')}}"></script>
<script src="{{asset('back/plugins/jqxgrid/jqxgrid.export.js')}}"></script>
<script src="{{asset('back/plugins/jqxgrid/jqxpanel.js')}}"></script>
<script src="{{asset('back/plugins/jqxgrid/globalize.js')}}"></script>
<script src="{{asset('back/plugins/jqxgrid/localization.js')}}"></script>
<script src="{{asset('back/plugins/jqxgrid/demos.js')}}"></script>

<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy',
            endDate: '<?php echo date_format(date_sub(new DateTime(),date_interval_create_from_date_string('18 years')),'d/m/Y');?>'
        });
        $('.datepicker2').datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy'
        });
        $('.tagsinput').tagsinput({
            typeaheadjs: {
                name: 'citynames',
                displayKey: 'name',
                valueKey: 'name',
                // source: citynames.ttAdapter()
            }
        });
    })
</script>

<script>
    $(function () {
        $('#example1').DataTable();
        $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false,
            'language'     : {
                url : '{{asset("general/datatable-fr_2.json")}}'
            }
        });
        //Initialize Select2 Elements
        $('.select2').select2()
            
        })
</script>

<!--Initialisation de pnotify-->
<script>
    $(document).ready(function () {
        PNotify.prototype.options.styling = "bootstrap3";
        PNotify.prototype.options.styling = "fontawesome";
    })
</script>


<script>
    $(document).ready(function () {
        $('#btnLogOut').click(function (e) {
            e.preventDefault();

            auth.signOut().then(function ()
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'POST', // Type of response and matches what we said in the route
                    url: '/admin/logout', // This is the url we gave in the route
                    success: function(response) {
                        window.location.href = "/admin";
                    }
                });
            }).catch(function (error)
            {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = '';
                // [START_EXCLUDE]
                switch (errorCode)
                {
                    case 'auth/wrong-password' :
                        errorMessage = 'Mot de passe incorrecte';
                        break;

                    case 'auth/invalid-email' :
                        errorMessage = 'Mail invalide';
                        break;

                    case 'auth/user-disabled' :
                        errorMessage = 'Compte supprimé ou desactivé. Contactez l\'administrateur';
                        break;

                    case 'auth/user-not-found' :
                        errorMessage = 'Aucun compte lié à ce mail';
                        break;
                    default:
                        errorMessage = 'Mot de passe ou mail incorrect';

                }
                $('#loginError').text(errorMessage);
                $('#alertLogin').fadeIn().fadeOut(5000);
                // console.log(errorCode);
            });
        });
        initFirebase();
    });
</script>

@yield('script')
</body>
</html>
