@extends('app')

@section('content')
    <section class="content-header">
        <h1>
            Erreur 404
        </h1>
      
    </section>

    <section class="content">
        <div class="error-page">
            <h2 class="headline text-yellow"> 404</h2>

            <div class="error-content">
                <h3><i class="fa fa-warning text-yellow"></i> Oops! Page non retrouvée.</h3>

                <p>
                    Vous tentez d'acceder à une page inexistante.
                    Cliquez ici <a href="{{url('/accueil')}}">pour retouner à l'accueil</a>
                </p>

                <form class="search-form">
                    <div class="input-group">
                        <input type="text" name="search" class="form-control" placeholder="Rechercher...">

                        <div class="input-group-btn">
                            <button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.input-group -->
                </form>
            </div>
            <!-- /.error-content -->
        </div>
        <!-- /.error-page -->
    </section>
@endsection