@extends('layouts.app')

@section('bodyClass')
    class="hold-transition login-page"
@endsection
@section('content')
    <div class="login-box">
        <div class="login-box-body" style="">
            <div class="login-logo">
                <a href="{{url('/')}}">
                    <img src="{{asset('back/dist/img/logo.png')}}" class="img-responsive" alt="logo rv"/>
                </a>
            </div>
            <p class="login-box-msg">Renseignez vos identifiants de connexion</p>
            <div id="alertLogin" style="display: none; border-radius: 3px!important;" class="message alert-message alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <p style="font-size:14px; font-style: italic; text-align: center" id="loginError"></p>
            </div>
            <form action="#" method="post" id="formLogin">
                {{ csrf_field() }}
                <div class="form-group has-feedback">
                    <input type="email" id="email" class="form-control" placeholder="Email"/>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" id="password" class="form-control" placeholder="Mot de passe"/>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    <span style="color: #E91E63; font-style: italic;" id="passwordError"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                    </div>
                    <div class="col-xs-4">
                        <button type="submit" id="btnConnect" class="btn btn-primary btn-block btn-flat">Connexion</button>
                    </div>
                </div>
            </form>

            <a href="#">Mot de passe oublié?</a><br/>

        </div>
    </div>
@endsection
@section('script-page')
    <script type="text/javascript" src="{{asset('../js/back/back.js') }}"></script>
    <script type="text/javascript">
        /**
         * Handles the sign in button press.
         */
        /*function SignIn() {
            var email = document.getElementById('email').value;
            var password = document.getElementById('password').value;
            let passTxt = document.getElementById('passwordError');
            let spanError = document.getElementById('loginError');
            let myDiv = document.getElementById('alertLogin');

            // Sign in with email and pass.
            // [START authwithemail]
            firebase.auth().signInWithEmailAndPassword(email, password).then(function ()
            {
                window.location.pathname = '/admin';
            }).catch(function (error)
            {
                // Handle Errors here.
                var errorCode = error.code;
                console.log(errorCode);
                var errorMessage = '';
                // [START_EXCLUDE]
                switch (errorCode)
                {
                    case 'auth/wrong-password' :
                        errorMessage = 'Mot de passe incorrecte';
                        break;

                    case 'auth/invalid-email' :
                        errorMessage = 'Mail invalide';
                        break;

                    case 'auth/user-disabled' :
                        errorMessage = 'Compte supprimé ou desactivé. Contactez l\'administrateur';
                        break;

                    case 'auth/user-not-found' :
                        errorMessage = 'Aucun compte lié à ce mail';
                        break;
                    default:
                        errorMessage = 'Mot de passe ou mail incorrect';

                }
                spanError.innerHTML = errorMessage;
                setTimeout(function () {
                    myDiv.style.display = 'block';
                }, 5000);

            });
        }*/


        /**
         * initApp handles setting up UI event listeners and registering Firebase auth listeners:
         *  - firebase.auth().onAuthStateChanged: This listener is called when the user is signed in or
         *    out, and that is where we update the UI.
         */
       /* function initApp() {
            // Listening for auth state changes.
            // [START authstatelistener]
            firebase.auth().onAuthStateChanged(function (user) {
                if (user)
                {
                    // User is signed in.
                    var displayName = user.displayName;
                    var email = user.email;
                    var emailVerified = user.emailVerified;
                    var photoURL = user.photoURL;
                    var isAnonymous = user.isAnonymous;
                    var uid = user.uid;
                    var providerData = user.providerData;
                }
            });
            // [END authstatelistener]
            document.getElementById('btnConnect').addEventListener('click', SignIn, false);
//            document.getElementById('quickstart-sign-up').addEventListener('click', handleSignUp, false);
        }

        window.onload = function () {
            initApp();
        };*/
    </script>
@endsection
