@extends('layouts.app')

@section('bodyClass')
    class="hold-transition login-page"
@endsection
@section('content')
    <div class="login-box">
        <div class="login-logo">
            <a href="#">
                <img src="{{asset('back/dist/img/logo.png')}}" alt="logo rv"/>
            </a>
        </div>
        <div class="login-box-body" style="">
            <p class="login-box-msg">Renseignez votre mail</p>
            <span style="color: #E91E63; font-style: italic;" id="loginError"></span>
            <form action="#" method="post">
                <div class="form-group has-feedback">
                    <input type="email" id="email" class="form-control" placeholder="Email"/>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        {{--<div class="checkbox icheck">
                            <label>
                                <input type="checkbox"/> Se souvenir de moi
                            </label>
                        </div>--}}
                    </div>
                    <div class="col-xs-4">
                        <button type="button" onclick="SignIn()" class="btn btn-danger btn-block btn-flat"></button>
                    </div>
                </div>
            </form>

            <a href="#">Mot de passe oublié?</a><br/>

        </div>
    </div>
@endsection
@section('script-page')
    <script type="text/javascript">

        function sendPasswordReset() {
            var email = document.getElementById('email').value;
            // [START sendpasswordemail]
            firebase.auth().sendPasswordResetEmail(email).then(function () {
                // Password Reset Email Sent!
                // [START_EXCLUDE]
                alert('Password Reset Email Sent!');
                // [END_EXCLUDE]
            }).catch(function (error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                // [START_EXCLUDE]
                if (errorCode == 'auth/invalid-email') {
                    alert(errorMessage);
                } else if (errorCode == 'auth/user-not-found') {
                    alert(errorMessage);
                }
                console.log(error);
                // [END_EXCLUDE]
            });
            // [END sendpasswordemail];
        }

        /**
         * initApp handles setting up UI event listeners and registering Firebase auth listeners:
         *  - firebase.auth().onAuthStateChanged: This listener is called when the user is signed in or
         *    out, and that is where we update the UI.
         */
        function initApp() {
            // Listening for auth state changes.
            // [START authstatelistener]
            firebase.auth().onAuthStateChanged(function (user) {
                // [START_EXCLUDE silent]
                document.getElementById('quickstart-verify-email').disabled = true;
                // [END_EXCLUDE]
                if (user) {
                    // User is signed in.
                    var displayName = user.displayName;
                    var email = user.email;
                    var emailVerified = user.emailVerified;
                    var photoURL = user.photoURL;
                    var isAnonymous = user.isAnonymous;
                    var uid = user.uid;
                    var providerData = user.providerData;
                    // [START_EXCLUDE]
                    document.getElementById('quickstart-sign-in-status').textContent = 'Signed in';
                    document.getElementById('quickstart-sign-in').textContent = 'Sign out';
                    document.getElementById('quickstart-account-details').textContent = JSON.stringify(user, null, '  ');
                    if (!emailVerified) {
                        document.getElementById('quickstart-verify-email').disabled = false;
                    }
                    // [END_EXCLUDE]
                } else {
                    // User is signed out.
                    // [START_EXCLUDE]
                    document.getElementById('quickstart-sign-in-status').textContent = 'Signed out';
                    document.getElementById('quickstart-sign-in').textContent = 'Sign in';
                    document.getElementById('quickstart-account-details').textContent = 'null';
                    // [END_EXCLUDE]
                }
                // [START_EXCLUDE silent]
                document.getElementById('quickstart-sign-in').disabled = false;
                // [END_EXCLUDE]
            });
            // [END authstatelistener]
            document.getElementById('quickstart-sign-in').addEventListener('click', toggleSignIn, false);
            document.getElementById('quickstart-sign-up').addEventListener('click', handleSignUp, false);
        }

        window.onload = function () {
            initApp();
        };
    </script>
@endsection
