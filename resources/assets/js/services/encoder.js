
export default class hasher {
    constructor() {
        let hash = require('object-hash')
        return hash
    }
    static test (hashCode, jsonObject){
        let encoding = this.hash(jsonObject);
        return hashCode === encoding;
    }
    static equal(objectA, objectB) {
        let hashA = this.hash(objectA);
        let hashB = this.hash(objectB);
        return hashA === hashB;
    }
}