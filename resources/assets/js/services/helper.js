
import {isNullOrUndefined, isArray } from 'util';


let isCadeau = (montantPret, taux, duree, precision) => {
   
    if(duree == 0 || montantPret==0 ){
        
        return 0;
    }
    return montantPret*(1+taux/100);
    
};
/**
 * @returns rounded value
 * @param {decimal} value - the value to ruond 
 * @param {int} decimals - the precision behind the comma
 */
export function arround(value, decimals) {
    if(value==0) {
        return 0
    } else {
        return Math.round(Number(Math.round(value+'e'+decimals)+decimals))
}
    }
        

// fonction vérifiant si un objet est vide
export function isEmpty(obj) {
    for(var key in obj) {
        if(Object.prototype.hasOwnProperty.call(obj, key))
            return false;
    }
    return true;
}

// supprime des espaces entre les mots d'une phrase
export function deleteSpace (chaine) {
    let space = '     ';
    while (space.length > 1) {

        while (chaine.includes(space)) {
            chaine = chaine.replace(space, ' ')
        }
        space = space.substring(1);
    }
    return chaine;
}

// formate un nombre en mettant des espaces entre les milliers ex: 2000 => 2 000
export function format (ch) {
    if (ch.length === 0) {
        return ''
    }
    let chh = [];
    let elt = [];
    ch = ch + '';
    let tabs = ch.split('');
    if (ch.length <= 3)
        return ch;
    while ((tabs.length - 1) % 3 !== 0)
        elt.push(tabs.shift());
    tabs.forEach((item, index, tab) => {
        chh.push(item);
        if (index % 3 === 0 && index !== tab.length - 1)
            chh.push(' ');
    });
    return elt.join('') + chh.join('')
}

export function HT(ttc, tva) {
    let result = (ttc=== undefined || ttc === null) ? 0 : Math.max(ttc, 0);
    if(tva === undefined || tva === null){
        return result
    } else {
        return  result * (1-tva);
    }
}

/**
 * calcule la valeur HT d'un champ au sein d'un objet et arrondis la valeur
 * @param {object} target 
 * @param {string/object} field 
 * @param {integer} tva 
 * @param {integer} precision
 */
export function convertFieldToHT(target, field, tva, precision) {
    if(target === undefined || target === null){
        return {}
    } else {
        if(field === undefined || field === null){
            return target
        } else {
            let result = {};
            if(precision === undefined || precision === null) precision = 0;
            for (let property in target) {
                if(Array.isArray(field)){
                    result[property] = (field.includes(property)) ? arround(HT(target[property], tva), precision) : target[property]
                } else {
                    result[property] = (property == field) ? arround(HT(target[field], tva), precision) : target[property]
                }
            }
            return result;
        }
    }
}

export function TTC(ht, tva) {
    let result = (ht === undefined || ht === null) ? 0 : Math.max(ht, 0);
    if(tva === undefined || tva === null){
        return result
    } else {
        return  result * (1+tva);
    }
}
/**
 * function calculate mensualite d'un credit
 */
export function mensualiteCredit(tauxNominalMensuel, capital, duree) {
    if (duree != 0 && tauxNominalMensuel != 0){
        return (capital  * (1+tauxNominalMensuel)) / duree;
        // return (capital/duree) * (1+ (tauxNominalMensuel * (duree-moisDurant+1)));
    }else {
        return 0
    }
}

export function calculRemboursementHT(ressource, tva, precision=0) {
    let ressourceHT = {'duree':ressource.duree, 'taux':ressource.taux,'montantPret':ressource.montantPret};
    return calculRemboursement(ressourceHT, precision);
}

/**
 * recalculer les remboursements par la methode de Mr Simplice
 */
export function calculRemboursement(ressource, precision =0) {
    let result =  {
        annee1:{montant:0, interet:0},
        annee2:{montant:0, interet:0},
        annee3:{montant:0, interet:0},
        annee4:{montant:0, interet:0}
    };
    if(ressource){
        let montantTotal = isCadeau(ressource.montantPret, ressource.taux, ressource.duree);
        let mensualite = (ressource.duree > 0) ? montantTotal / ressource.duree : 0;
        let interet = (montantTotal > 0) ? arround(montantTotal - ressource.montantPret, precision) : 0; // interet annuel
        console.log("Interets", interet);
        //  let interet = (montantTotal > 0) ? arround(montantTotal , precision) : 0; // interet annuel
        switch(true){
          case (ressource.duree <= 12 && ressource.duree > 0):
              result.annee1.montant = arround(montantTotal, precision);
              result.annee1.interet = arround(interet, precision);
          break;
          case (ressource.duree > 12 && ressource.duree <= 24):
          // le nombre de mois restants apres la 1ere anneee
              result.annee1.montant = arround(montantTotal *  12 / ressource.duree, precision);
              result.annee1.interet = arround(interet *  12 / ressource.duree, precision);
              result.annee2.montant = arround(mensualite * (ressource.duree-12), precision);
              result.annee2.interet = arround(interet * (ressource.duree - 12) / ressource.duree, precision);
          break;
          case (ressource.duree > 24 && ressource.duree <= 36):
              result.annee1.montant = arround(montantTotal * 12 / ressource.duree, precision);
              result.annee1.interet = arround(interet * 12 / ressource.duree, precision);
              result.annee2.montant = arround(mensualite * 12, precision);
              result.annee2.interet = arround(interet * 12 / ressource.duree, precision);
              result.annee3.montant = arround(mensualite * (ressource.duree - 24), precision);
              result.annee3.interet = arround(interet * (ressource.duree - 24) / ressource.duree, precision);
          break;
          case (ressource.duree > 36):
            result.annee1.montant = arround(montantTotal * 12 / ressource.duree, precision);
            result.annee1.interet = arround(interet * 12 / ressource.duree, precision);
            result.annee2.montant = arround((montantTotal-result.annee1.montant) * 12 / (ressource.duree-12), precision);
            result.annee2.interet = arround(interet * 12 / ressource.duree, precision);
            result.annee3.montant = arround((montantTotal-(result.annee1.montant+result.annee2.montant)) * 12/(ressource.duree - 24), precision);
            result.annee4.montant = arround((montantTotal-(result.annee1.montant+result.annee2.montant+result.annee3.montant)) * 12/(ressource.duree -36), precision);
            result.annee4.interet = arround(interet * (ressource.duree - 24)/ ressource.duree, precision);
          break;
        }
        return result;
    }
    return result;
}

/**
 *
 */
export function versement( moisDurant, options = {capital:0, taux:0, duree:0}) {
    if(options.capital > 0 && moisDurant <= options.duree && options.duree > 0){
        return mensualiteCredit(options.taux, options.capital, options.duree);
        // return (options.capital/options.duree) * (1+ (options.taux * (options.duree-moisDurant+1)));
    }
    return 0;
}

/**
 *
 */
export function cumulVersementAnnuel(annee, options = {capital:0, taux:0, duree:0}){
    let sum = 0, last = 12 * annee;
    for(var i=0; i<12; i++) {
        sum += versement(last-i, options);
    }
    return sum;
    // return (capital * tauxNominalMensuel) / (1 - ( (1+tauxNominalMensuel)** -duree) );
}

/**
 * JS method for testing if an array has value
 */
export function checkValue(value) {
    if (Array.isArray(value) && value.length) {
        return true;
    } else {
        return false;
    }
}

// Function transforming value
export function formatMontant(nbr) {
    if (!nbr) return 0;
    var nombre = ''+nbr;
    var retour = '';
    var count=0;
    for(var i=nombre.length-1 ; i>=0 ; i--) {
        if(count!==0 && count % 3 === 0)
            retour = nombre[i]+' '+retour ;
        else retour = nombre[i]+retour ; count++; }
    // alert('nb : '+nbr+' => '+retour);
    return retour;
}

/**
 *
 * @param {'prix', 'quantite', 'amortissement'} item
 * @param number precision
 */
export function echeancierRounded(item, precision = 0){
    if( Object.prototype.hasOwnProperty.call(item, "prix") && Object.prototype.hasOwnProperty.call(item, 'quantite') && Object.prototype.hasOwnProperty.call(item, 'amortissement')){
        if(item.amortissement > 0){
            return arround(item.quantite * item.prix / item.amortissement, precision);
        }
        return 0
    }
    return 0;
}

/**
 *
 * @param {le montant du truc} amount
 * @param {annees d'amortissement} amortissement
 */
export function amountPerYear(amount, amortissement){
    if(amortissement > 0){
        return amount / amortissement;
    }
    return 0
}

export function calculAmortissementImmoCorporelle(itemImmoCorp, precision = 0){
    let result =  { annee1: 0, annee2:0, annee3:0, annee4:0 };
    if(itemImmoCorp){
        switch(true){
            case (itemImmoCorp.amortissement <= 1 && itemImmoCorp.amortissement > 0):
                result.annee1 = arround(itemImmoCorp.quantite * itemImmoCorp.prix / itemImmoCorp.amortissement, precision);
            break;
            case itemImmoCorp.amortissement > 1 && itemImmoCorp.amortissement <= 2:
                result.annee1 = arround((itemImmoCorp.quantite * itemImmoCorp.prix / itemImmoCorp.amortissement), precision);
                result.annee2 = arround((itemImmoCorp.quantite * itemImmoCorp.prix / itemImmoCorp.amortissement), precision);
            break;
            case (itemImmoCorp.amortissement > 2 && itemImmoCorp.amortissement <= 3):
                result.annee1 = arround((itemImmoCorp.quantite * itemImmoCorp.prix  / itemImmoCorp.amortissement), precision);
                result.annee2 = arround((itemImmoCorp.quantite * itemImmoCorp.prix  / itemImmoCorp.amortissement), precision);
                result.annee3 = arround((itemImmoCorp.quantite * itemImmoCorp.prix  / itemImmoCorp.amortissement), precision);
            break;
            case (itemImmoCorp.amortissement > 3):
                result.annee1 = arround((itemImmoCorp.quantite * itemImmoCorp.prix / itemImmoCorp.amortissement), precision);
                result.annee2 = arround((itemImmoCorp.quantite * itemImmoCorp.prix / itemImmoCorp.amortissement), precision);
                result.annee3 = arround((itemImmoCorp.quantite * itemImmoCorp.prix / itemImmoCorp.amortissement), precision);
                result.annee4 = arround((itemImmoCorp.quantite * itemImmoCorp.prix / itemImmoCorp.amortissement), precision);
            break;
        }
        return result;
    }
    return result;
}

export function signedDiff(a,b) {
    let max = Math.max(Math.abs(a), Math.abs(b));
    let min = Math.min(Math.abs(a), Math.abs(b));
    let sign = ( max == Math.abs(a) ) ? Math.sign(a) : Math.sign(b);
    return sign * (max - min);
}

/**
 * return a random 36 digit hexa id 
 */
export function makeId(max=10) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    for (var i = 0; i < max; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}


// export function doublonDelete(data){
//     let cache={};
//     let datafilter=data.filter((elem)=>{
//         return cache[elem.text]?0:cache[elem.text]=1;
//     });
//     return datafilter
// }


export default {
    methods: {
        /*
        logout(){
            return axios.post('/api/auth/logout').then(response =>  {
                localStorage.removeItem('auth_token');
                axios.defaults.headers.common['Authorization'] = null;
                toastr['success'](response.data.message);
            }).catch(error => {
                console.log(error);
            });
        },

        authUser(){
            return axios.get('/api/auth/user').then(response =>  {
                return response.data;
            }).catch(error => {
                return error.response.data;
            });
        },

        check(){
            return axios.post('/api/auth/check').then(response =>  {
                return !!response.data.authenticated;
            }).catch(error =>{
                return response.data.authenticated;
            });
        },

        getFilterURL(data){
            let url = '';
            $.each(data, function(key,value) {
                url += (value) ? '&'+key+'='+encodeURI(value) : '';
            });
            return url;
        },
        */
        formAssign(form, data){
            for (let key of Object.keys(form)) {
                if(key !== "originalData" && key !== "errors" && key !== "autoReset"){
                    form[key] = data[key];
                }
            }
            return form;
        },

        taskColor(value){
            let classes = ['progress-bar','progress-bar-striped'];
            if(value < 20)
                classes.push('bg-danger');
            else if(value < 50)
                classes.push('bg-warning');
            else if(value < 80)
                classes.push('bg-info');
            else
                classes.push('bg-success');
            return classes;
        },
        /*
        formatDate(date, locale){
            if(!date)
            return;
            if(!locale){locale = "en"};

            if(locale == "fr"){
                moment.lang('fr');
                return moment(date).format('DD MMMM YYYY');
            }else{
                moment.lang('en');
                return moment(date).format('MMMM Do YYYY');
            }

        },
        formatDateTime(date, locale){
            if(!date)
                return;
            if(!locale){locale = "en"};

            if(locale == "fr"){
                moment.lang('fr');
                return moment(date).format('DD MMMM YYYY H:mm');
            }else{
                moment.lang('en');
                return moment(date).format('MMMM Do YYYY h:mm a');
            }
            return moment(date).format();
        },
        */
        // fonction vérifiant si un objet est vide
        isEmpty(obj) {
            for(var key in obj) {
                if(Object.prototype.hasOwnProperty.call(obj, key))
                    return false;
            }
            return true;
        },// fonction vérifiant si un objet est vide

        dateFormat(date){
            if(date!=null && date!=undefined){
                var timestamp = date.seconds*1000;
                var date_not_formatted = new Date(timestamp);

                var formatted_string = date_not_formatted.getFullYear() + "-";

                if (date_not_formatted.getMonth() < 9) {
                formatted_string += "0";
                }
                formatted_string += (date_not_formatted.getMonth() + 1);
                formatted_string += "-";

                if(date_not_formatted.getDate() < 10) {
                formatted_string += "0";
                }
                formatted_string += date_not_formatted.getDate();

                return formatted_string;
            }
            
          
        },
       
    
        FilterbyDate(a, b) {
            // Use toUpperCase() to ignore character casing
            const dateA = a.createdAt.seconds;
            const dateB = b.createdAt.seconds;

            let comparison = 0;
            if (dateA > dateB) {
            comparison = 1;
            } else if (dateA < dateB) {
            comparison = -1;
            }
            return comparison;
        },
        FilterbyDateDesc(a, b) {
            // Use toUpperCase() to ignore character casing
            const dateA = a.createdAt.seconds;
            const dateB = b.createdAt.seconds;

            let comparison = 0;
            if (dateA < dateB) {
            comparison = 1;
            } else if (dateA > dateB) {
            comparison = -1;
            }
            return comparison;
        },

        dateFormat2(date){
            if(date!=null && date!=undefined){
                var timestamp = date.seconds*1000;
                var date_not_formatted = new Date(timestamp);

                var formatted_string = date_not_formatted.getDate()+ "/";
                if(date_not_formatted.getDate() < 10) {
                    formatted_string += "0";
                    }
                    if (date_not_formatted.getMonth() < 9) {
                        formatted_string += "0";
                        }
                        formatted_string += (date_not_formatted.getMonth() + 1);
                        formatted_string += "/";
        

                    formatted_string += date_not_formatted.getFullYear() ;

              
               

                return formatted_string;
            }
          
        },
        hasDefinedProp(obj, key) {
            return ( obj[key] != undefined && obj[key] != null);
        },
        stringsAreEqual(first,second) {
            if(typeof(first)== "string" && typeof(second) == "string") {
                return first.endsWith(second) && first.startsWith(second);
            }
            return false;
        },
        ucword(value){
            if(!value)
                return;

            return value.toLowerCase().replace(/\b[a-z]/g, function(value) {
                return value.toUpperCase();
            });
        },
        /**
         *
         */
        versement( moisDurant, options = {capital:0, taux:0, duree:0}) {
            if(options.capital > 0 && moisDurant <= options.duree && options.duree > 0){
                return this.mensualiteCredit(options.taux, options.capital, options.duree);
                // return (options.capital/options.duree) * (1+ (options.taux * (options.duree-moisDurant+1)));
            }
            return 0;
        },
        /**
         *
         */
        cumulVersementAnnuel(annee, options = {capital:0, taux:0, duree:0}){
            let sum = 0, last = 12 * annee;
            for(var i=0; i<12; i++) {
                sum +=this.versement(last-i, options);
            }
            return sum;
        // return (capital * tauxNominalMensuel) / (1 - ( (1+tauxNominalMensuel)** -duree) );
        },
        /**
         * function calculate mensualite d'un credit
         */
        mensualiteCredit(tauxNominalMensuel, capital, duree) {
        if (duree != 0 && tauxNominalMensuel != 0){
            return (capital  * (1+tauxNominalMensuel)) / duree;
            // return (capital/duree) * (1+ (tauxNominalMensuel * (duree-moisDurant+1)));
        }else {
            return 0
        }
    },

        //GENERATE ID
        getRandomString(length) {
            var s = '';
            do { s += Math.random().toString(36).substr(2); } while (s.length < length);
            s = s.substr(0, length);
      
            return s;
            },

            // sendEmail(){
            //     const sgMail = require('@sendgrid/mail');
            //     sgMail.setApiKey(process.env.SENDGRID_API_KEY);
            //     const msg = {
            //     to: 'max.cyberschool@gmail.com',
            //     from: 'app.cyberschool@gmail.com',
            //     subject: 'Sending with Twilio SendGrid is Fun',
            //     text: 'and easy to do anywhere, even with Node.js',
            //     html: '<strong>and easy to do anywhere, even with Node.js</strong>',
            //     };
            //     sgMail.send(msg);
            // }
          

    },
   
   
   

  



}
