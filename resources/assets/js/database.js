import Vue from 'vue'
import Vuex from 'vuex'
import VueFire from 'vuefire'
import firebase from 'firebase/app'
import 'firebase/firestore'
Vue.use(VueFire)
firebase.initializeApp({
    apiKey: "AIzaSyAtLbJIn6Rk7xXdj81bIyBCc-KoBt3y9Hs",
    authDomain: "cyberschool-ab785.firebaseapp.com",
    databaseURL: "https://cyberschool-ab785.firebaseio.com",
    projectId: "cyberschool-ab785",
    storageBucket: "cyberschool-ab785.appspot.com",
    messagingSenderId: "107562228204"

})
export const db = firebase.firestore()