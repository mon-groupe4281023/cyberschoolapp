/* eslint-disable no-undef */

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

 require('./bootstrap');
 import store from './store'
 import router from './routes'
 import firebase from 'firebase/app'
 import 'firebase/firestore'
 import VueIntro from 'vue-introjs'
 import regeneratorRuntime from "regenerator-runtime";
 // var admin = require("firebase-admin");
 Vue.use(VueIntro)
 import Vuelidate from 'vuelidate'
 Vue.use(Vuelidate)
 import "../sass/kanban.scss"
 // Init plugin
 import Loading from 'vue-loading-overlay'
 Vue.use(Loading, {
     isFullPage: false,
     canCancel: false,
     loader: "spinner",
 });
 
 
 // import Vue from 'vue';
 import vueKanban from 'vue-kanban'
 Vue.use(vueKanban)
 
 //vueElement
 //Vue worker
 import VueWorker from 'vue-worker'
 Vue.use(VueWorker)
 //table
 import ElementUI from 'element-ui'
 import locale from 'element-ui/lib/locale/lang/fr'
 import 'element-ui/lib/theme-chalk/index.css'
 import VueDataTables from 'vue-data-tables'
 import Vuesax from 'vuesax'
 Vue.use(Vuesax)
 import 'vuesax/dist/vuesax.css'
 import 'material-icons/iconfont/material-icons.css';
 Vue.use(ElementUI, { locale });
 Vue.use(VueDataTables)
 
 //EXPORT
 import excel from 'vue-excel-export'
 Vue.use(excel)
 
 
 //Custom Form Input
 
 import vueNumericCustom from './custominput/vueNumeric'
 import datePickerCustom from './custominput/datePicker'
 import fileUploadCustom from './custominput/fileUpload'
 import elSelectCustom from './custominput/elSelect'
 import elInputCustom from './custominput/elInput'
 
 Vue.component("field-vueNumericCustom", vueNumericCustom);
 Vue.component("field-datePickerCustom", datePickerCustom);
 Vue.component("field-fileUploadCustom", fileUploadCustom);
 Vue.component("field-elSelectCustom", elSelectCustom);
 Vue.component("field-elInputCustom", elInputCustom);
 import {NumberToLetter} from "./numToLetter"
 
 
 
 import VueMoment from 'vue-moment'
 import moment from 'moment-timezone'
 require('moment/locale/fr')
 
 Vue.use(VueMoment, {
     moment,
 })
 
 import CKEditor from '@ckeditor/ckeditor5-vue';
 Vue.use(CKEditor);
 
 import VueHtml2Canvas from 'vue-html2canvas';
 
 Vue.use(VueHtml2Canvas);
 

 Vue.filter('toLetter', function(value){
    return NumberToLetter(value)
 })
 Vue.filter('reduceParagraphLength', function (value) {
    if (!value || typeof (value) !== 'string') {
      return '';
    }
  
    const maxLength = 100;
    const plainText = value.replace(/<\/?[^>]+(>|$)/g, '');
    const length = plainText.length;
  
    if (length > maxLength) {
      return `${plainText.slice(0, maxLength)}...`;
    } else {
      return value;
    }
  });
  
 Vue.filter('normalized', function (value) {
     return (value.charAt(0).toUpperCase() + value.substring(1).toLowerCase())
 })
 
 Vue.filter('capitalized', function (value) {
     if (!value) return "";
     return value.toUpperCase()
 })
 
 Vue.filter('cyb_txt_split', function (value) {
     if (!value) return "détails";
     let res = "";
     let formatText = value.replace(/<\/?[^>]+(>|$)/g, "")
     let len = formatText.length;
     if (len > 14) {
         res = value.substr(0, 11);
         return res + "...";
     } else {
         return value;
     }
 })
 Vue.filter('cyb_txt_switch_split', function (value) {
    if (!value) return "détails";
    let res = "";
    let formatText = value.replace(/<\/?[^>]+(>|$)/g, "")
    let len = formatText.length;
    if (len > 20) {
        res = value.substr(0, 20);
        return res + "...";
    } else {
        return value;
    }
})
 Vue.filter('cyb_description_split', function (value) {
     if (!value) return "";
     let res = "";
     let formatText = value.replace(/<\/?[^>]+(>|$)/g, "")
     let len = formatText.length;
     if (len > 20) {
         res = value.substr(0, 20);
         return res + "...";
     } else {
         return value;
     }
 })
 Vue.filter('cyb_descParam_split', function (value) {
     if (!value) return "";
     let res = "";
     let formatText = value.replace(/<\/?[^>]+(>|$)/g, "")
     let len = formatText.length;
     if (len > 15) {
         res = value.substr(0, 30);
         return res + "...";
     } else {
         return value;
     }
 })
 Vue.filter('cyb_desc_split', function (value) {
     if (!value) return "";
     let res = "";
     let formatText = value.replace(/<\/?[^>]+(>|$)/g, "")
     let len = formatText.length;
     if (len > 36) {
         res = value.substr(0, 30);
         return res + "...";
     } else {
         return value;
     }
 })
 Vue.filter('cyb_date_split', function (value) {
     if (!value) return "";
     let res = "";
     let len = value.length;
     if (len > 12) {
         res = value.substr(0, 9);
         return res + "...";
     } else {
         return value;
     }
 })
 Vue.filter('cyb_title_plit', function (value) {
     if (!value) return "";
     let res = "";
     let len = value.length;
     if (len > 21) {
         res = value.substr(0, 15);
         return res + "...";
     } else {
         return value;
     }
 })
 Vue.filter('cyb_title_project_plit', function (value) {
     if (!value) return "";
     let res = "";
     let len = value.length;
     if (len > 21) {
         res = value.substr(0, 17);
         return res + "...";
     } else {
         return value;
     }
 })
 Vue.filter('cyb_title_param_plit', function (value) {
     if (!value) return "";
     let res = "";
     let len = value.length;
     if (len > 10) {
         res = value.substr(0, 15);
         return res + "...";
     } else {
         return value;
     }
 })
 Vue.filter('cyb_name_plit', function (value) {
     if (!value) return "";
     let res = "";
     let len = value.length;
     if (len > 21) {
         res = value.substr(0, 19);
         return res + "...";
     } else {
         return value;
     }
 })
 Vue.filter('cyb_denomination_split', function (value) {
     if (!value) return "";
     let res = "";
     let len = value.length;
     if (len > 100) {
         res = value.substr(0, 100);
         return res + "...";
     } else {
         return value;
     }
 })
 Vue.filter('cyb_userPicName_split', function (value) {
     if (!value) return "";
     let res = "";
     let len = value.length;
     if (len > 6) {
         res = value.substr(0, 6);
         return res + "...";
     } else {
         return value;
     }
 })
 
 Vue.filter('cyb_descCategorie_split', function (value) {
     if (!value) return "";
     let res = "";
     let len = value.length;
     if (len > 20) {
         res = value.substr(0, 20);
         return res + "...";
     } else {
         return value;
     }
 })
 const config = ({
     apiKey: "AIzaSyAtLbJIn6Rk7xXdj81bIyBCc-KoBt3y9Hs",
     authDomain: "cyberschool-ab785.firebaseapp.com",
     databaseURL: "https://cyberschool-ab785.firebaseio.com",
     projectId: "cyberschool-ab785",
     storageBucket: "cyberschool-ab785.appspot.com",
     messagingSenderId: "107562228204",
     appId: "1:107562228204:web:29cb3cd4f35fe30bbcc8c9",
     measurementId: "G-H7E9VRGRNV"
 
 });
 firebase.initializeApp(config);
 
 
 // admin.initializeApp(config);
 /* objet global de manipulation de fire Database */
 // const db = firebase.database();
 
 firebase.firestore().settings({
     cacheSizeBytes: firebase.firestore.CACHE_SIZE_UNLIMITED
 });
 
 // global variables for database access 
 Vue.prototype.$firestore = firebase.firestore();
 Vue.prototype.$firestorage = firebase.storage();
 
 const app = new Vue({
     el: '#root',
     store,
     router
 });
 