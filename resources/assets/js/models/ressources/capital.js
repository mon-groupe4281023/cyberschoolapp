import Ressource from './ressource'
export default class Capital extends Ressource {
    constructor({apporteur="apporteur", identite="identite", quantite=1, montant=0, group="", commentaire=""}){
        super()
        this.apporteur = apporteur
        this.identite = identite
        this.quantite = quantite
        this.montant = montant
        this.group = group
        this.commentaire = commentaire
    }
    total(){
        return this.quantite * this.montant;
    }
}