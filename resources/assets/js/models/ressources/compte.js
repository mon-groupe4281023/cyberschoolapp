import Ressource from './ressource'
export default class Compte extends Ressource {
    constructor({apporteur="apporteur", identite="identite", quantite=1, montant=0, commentaire=""}){
        super()
        this.identite = identite
        this.apporteur = apporteur
        this.quantite = quantite
        this.montant = montant
        this.commentaire = commentaire
    }
    total(){
        return this.quantite * this.montant;
    }
}