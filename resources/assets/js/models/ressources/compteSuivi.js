import Compte from './compte'
var hasher = require('object-hash')

export default class CompteSuivi extends Compte {
    constructor({apporteur="apporteur", identite="identite", quantite=1, montant=0, commentaire=""}){
        super({apporteur:apporteur, identite:identite, quantite:quantite, montant:montant, commentaire:commentaire})
        this.previsionnel = {apporteur:apporteur, identite:identite, quantite:quantite, montant:montant, commentaire:commentaire}
        this.hash = hasher(this.previsionnel)
        this.suivi = false
    }
    delta(){
        let previsionnel = new Compte(this.previsionnel)
        return previsionnel.total() - this.total();
    }
}