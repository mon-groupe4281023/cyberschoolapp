import Ressource from './ressource'
export default class Subvention extends Ressource {
    constructor({fournisseur="creancier", montantPret=1, duree=0, taux=0, group="", commentaire=""}){
        super()
        this.fournisseur = fournisseur
        this.montantPret = montantPret
        this.duree = duree
        this.taux = taux
        this.group = group
        this.commentaire = commentaire
    } 
    montantRemboursement(){
        return this.duree == 0 ? 0 : this.montantPret + (this.taux*this.montantPret / 100)
    }
    interet(){
        return (this.montantRemboursement() > 0) ? this.montantRemboursement() - this.montantPret : 0
    }
    remboursement(){
        let montantTotal = this.montantRemboursement();
        let interet = this.interet(); // interet annuel
        switch(true){
            case (this.duree <= 12 && this.duree > 0):
                return {
                    annee1:{montant:montantTotal, interet:interet},
                    annee2:{montant:0, interet:0},
                    annee3:{montant:0, interet:0},
                    annee4:{montant:0, interet:0}
                }
            case (this.duree > 12 && this.duree <= 24):
         // le nombre de mois restants apres la 1ere annee
                return {
                    annee1:{montant:montantTotal *  12 / this.duree, interet:interet *  12 / this.duree},
                    annee2:{montant:montantTotal * (this.duree - 12) / this.duree, interet:interet * (this.duree - 12) / this.duree},
                    annee3:{montant:0, interet:0},
                    annee4:{montant:0, interet:0}
                }
            case (this.duree > 24 && this.duree <= 36):
                return {
                    annee1:{montant: montantTotal * 12 / this.duree, interet: interet * 12 / this.duree},
                    annee2:{montant: montantTotal * 12 / this.duree, interet: interet * 12 / this.duree},
                    annee3:{montant: montantTotal * (this.duree - 24) / this.duree, interet: interet * (this.duree - 24) / this.duree},
                    annee4:{montant:0, interet:0}
                }
            case (this.duree > 36):
                return {
                    annee1:{montant: montantTotal * 12 / this.duree, interet: interet * 12 / this.duree},
                    annee2:{montant: montantTotal * 12 / this.duree, interet: interet * 12 / this.duree},
                    annee3:{montant: montantTotal * 12 / this.duree, interet: interet * 12 / this.duree},
                    annee4:{montant: montantTotal * (this.duree - 24)/ this.duree, interet: interet * (this.duree - 24)/ this.duree}
                }
            default:
                return {
                    annee1:{montant:0, interet:0},
                    annee2:{montant:0, interet:0},
                    annee3:{montant:0, interet:0},
                    annee4:{montant:0, interet:0}
                };
        }
    }
}