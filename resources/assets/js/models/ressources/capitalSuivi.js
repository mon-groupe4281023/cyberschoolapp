import Capital from './capital'
var hasher = require('object-hash')

export default class CapitalSuivi extends Capital {
    constructor({apporteur="apporteur", identite="identite", quantite=1, montant=0, group="", commentaire=""}){
        super({apporteur:apporteur, identite:identite, quantite:quantite, montant:montant, group:group, commentaire:commentaire})
        this.previsionnel = {apporteur:apporteur, identite:identite, quantite:quantite, montant:montant, group:group, commentaire:commentaire}
        this.hash = hasher(this.previsionnel)
        this.suivi = false
    }

    delta() {
        let previsionnel = new Capital(this.previsionnel)
        return previsionnel.total() - this.total()
    }
}