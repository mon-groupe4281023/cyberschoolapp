import Emprunt from './emprunt'
var hasher = require('object-hash')

export default class EmpruntSuivi extends Emprunt {
    constructor({creancier="creancier", montantPret=1, duree=0, taux=0, group="", commentaire=""}){
        super({creancier:creancier, montantPret:montantPret, duree:duree, taux:taux, group:group, commentaire: commentaire})
        this.previsionnel = {creancier:creancier, montantPret:montantPret, duree:duree, taux:taux, group:group, commentaire: commentaire}
        this.hash = hasher(this.previsionnel)
        this.suivi = false
    }
    delta(){
        let previsionnel = new Emprunt(this.previsionnel)        
        return previsionnel.total() - this.total();
    }
}