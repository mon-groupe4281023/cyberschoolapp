import Subvention from './subvention'
var hasher = require('object-hash')

export default class SubventionSuivi extends Subvention {
    constructor({fournisseur="creancier", montantPret=1, duree=0, taux=0, group="", commentaire=""}){
        super({fournisseur:fournisseur, montantPret:montantPret, duree:duree, taux:taux, group:group, commentaire:commentaire})
        this.previsionnel = {fournisseur:fournisseur, montantPret:montantPret, duree:duree, taux:taux, group:group, commentaire:commentaire}
        this.hash = hasher(this.previsionnel)
        this.suivi = false
    }
    delta(){
        return this.previsionnel.montantPret - this.montantPret;
    }
}