import ImmoIncorporelle from './immoIncorporelle'

export default class ImmoIncorporelleSuivi extends ImmoIncorporelle {
    constructor({titre="Titre", quantite=1, prix=0, commentaire=""}){
        super({titre:titre, quantite:quantite, prix:prix, commentaire:commentaire})
        this.oldQuantite = quantite
        this.oldPrix = prix
        this.suivi = false
    }
    
    delta(){
        return (this.oldPrix*this.oldQuantite) - this.total;
    }

}