import ImmoCorporelle from './immoCorporelle'

export default class ImmoCorporelleSuivi extends ImmoCorporelle {
    constructor({titre="Titre", quantite=1, prix=0, commentaire="", group="", amortissement=1}){
        super({titre: titre, quantite: quantite, prix: prix, commentaire: commentaire, group: group, amortissement: amortissement})
        this.oldQuantite = quantite
        this.oldPrix = prix
        this.suivi = false
    }
    delta(){
        return (this.oldQuantite*this.oldPrix) - this.total();
    }
}