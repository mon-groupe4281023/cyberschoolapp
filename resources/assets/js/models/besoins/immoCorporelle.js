export default class immoCorporelle {
    constructor({titre="Titre", quantite=1, prix=1, tva = 0, taxeable = false, commentaire="", group="", amortissement=0}){
        this.titre = titre;
        this.quantite = quantite;
        this.amortissement = amortissement;
        this.prix = prix;
        this.tva = tva;
        this.taxeable = taxeable;
        this.commentaire = commentaire;
        this.group = group;
        
        // controle de l'amortissement
        if(amortissement == 0) {
            if(group !== "") {
                switch (this.group) {
                    case 'mobilier' : this.amortissement = 3; break;
                    case 'voiture-autres' : this.amortissement = 5; break;                
                    default: this.amortissement = 2; break;
                    // default = materielinfo | materiel-machine-outil | batiment-local-espacevente
                }
            } else this.amortissement = 2;
        }
    }

    get prixTotalTTC(){
        return this.quantite * this.prix ;
    }

    get prixTotalHT(){
        return this.taxeable ?  (1 - (this.tva/100)) * this.quantite * this.prix : this.quantite * this.prix;
    }

    get calculAmortissements(){
        switch(true){
            case (this.amortissement <= 1 && this.amortissement > 0):
                return { annee1: this.quantite * this.prix / this.amortissement, annee2:0, annee3:0, annee4:0 };
            case this.amortissement > 1 && this.amortissement <= 2:
                return { 
                    annee1: this.quantite * this.prix / this.amortissement, 
                    annee2: this.quantite * this.prix / this.amortissement, 
                    annee3:0, 
                    annee4:0
                };
            case (this.amortissement > 2 && this.amortissement <= 3):
                return { 
                    annee1:this.quantite * this.prix  / this.amortissement, 
                    annee2:this.quantite * this.prix  / this.amortissement, 
                    annee3:this.quantite * this.prix  / this.amortissement, 
                    annee4:this.quantite * this.prix  / this.amortissement
                };
            case (this.amortissement > 3):
                return {
                    annee1:this.quantite * this.prix  / this.amortissement,
                    annee2:this.quantite * this.prix  / this.amortissement,
                    annee3:this.quantite * this.prix  / this.amortissement,
                    annee4:this.quantite * this.prix  / this.amortissement
                };
            default: return { annee1: 0, annee2:0, annee3:0, annee4:0 };
        }
    }

    get amortissementsUnitaires(){
        switch(true){
            case (this.amortissement <= 1 && this.amortissement > 0):
                return { annee1: this.prix / this.amortissement,
                    annee2:0, annee3:0, annee4:0 };
            case this.amortissement > 1 && this.amortissement <= 2:
                return { 
                    annee1: this.prix / this.amortissement, 
                    annee2: 2 * this.prix / this.amortissement, 
                    annee3:0, annee4:0 };
            case (this.amortissement > 2 && this.amortissement <= 3):
                return { 
                    annee1: this.prix / this.amortissement, 
                    annee2: 2 * this.prix / this.amortissement, 
                    annee3: 3 * this.prix / this.amortissement, 
                    annee4:0 };
            case (this.amortissement > 3):
                return { 
                    annee1: 1 * this.prix / this.amortissement, 
                    annee2: 2 * this.prix / this.amortissement, 
                    annee3: 3 * this.prix / this.amortissement, 
                    annee4: 4 * this.prix / this.amortissement };
            default: return { annee1: 0, annee2:0, annee3:0, annee4:0 }
        }
    }

    get montantAmortissement(){
        return this.amortissement > 0 ? (this.quantite * this.prix / this.amortissement) : 0;
    }

    toJSON() {
        return {
            "titre": this.titre, 
            "quantite": this.quantite, 
            "prix": this.prix, 
            "tva": this.tva, 
            "taxeable": this.taxeable, 
            "commentaire": this.commentaire, 
            "group": this.group, 
            "amortissement": this.amortissement
        };
    }
}