import ImmoFinanciere from './immoFinanciere'

export default class ImmoFinanciereSuivi extends ImmoFinanciere {
    constructor({titre="Titre", quantite=1, montant=0, commentaire="", group=""}){
        super({titre: titre, quantite: quantite, montant: montant, commentaire: commentaire, group: group })
        this.oldQuantite = quantite
        this.oldMontant = montant
        this.suivi = false
    }

    delta() {
        return (this.oldQuantite*this.oldMontant) - this.total()
    }
}