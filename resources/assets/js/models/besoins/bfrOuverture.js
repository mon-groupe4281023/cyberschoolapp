import Besoin from './besoin'
export default class BfrOuverture extends Besoin {
    constructor({titre="Titre", coutMensuel=0, pourcentage=0, commentaire="", group="", duree=0}){
        super()
        this.titre = titre
        this.coutMensuel = coutMensuel
        this.duree = duree
        this.pourcentage = pourcentage
        this.commentaire = commentaire
        this.group = group
    }
    total(){
        return this.coutMensuel * this.duree * this.pourcentage / 100;
    }
}