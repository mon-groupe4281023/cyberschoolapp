import BfrOuverture from './bfrOuverture'

export default class BfrOuvertureSuivi extends BfrOuverture {
    constructor({titre="Titre", coutMensuel=1, pourcentage=0, commentaire="", group="", duree=1}){
        super({titre: titre, coutMensuel: coutMensuel, pourcentage: pourcentage, commentaire: commentaire, group: group, duree: duree})
        this.oldCoutMensuel = coutMensuel
        this.oldPourcentage = pourcentage
        this.suivi = false
    }
    delta(){
        let previsionnel = new BfrOuverture(this.previsionnel)
        return previsionnel.total() - this.total();
    }
}