import Besoin from './besoin'
export default class ImmoFinanciere extends Besoin {
    constructor({titre="Titre", quantite=1, montant=0, commentaire="", group=""}){
        super()
        this.titre = titre
        this.quantite = quantite
        this.montant = montant
        this.commentaire = commentaire
        this.group = group
    }
    total(){
        return this.quantite * this.prix;
    }
}