export default class immoIncorporelle {

    constructor({titre = 'Titre', quantite = 1, prix = 1, amortissement = 2, tva = 0, taxeable = false, commentaire = ""}) {
        this.titre = titre;
        this.quantite = quantite;
        this.prix = prix;
        this.amortissement = amortissement;
        this.tva = tva;
        this.taxeable = taxeable;
        this.commentaire = commentaire;
    }

    toJSON() {
        return {
            "titre": this.titre,
            "quantite": this.quantite,
            "prix": this.prix,
            "amortissement": this.amortissement,
            "tva" : this.tva,
            "taxeable" : this.taxeable, 
            "commentaire": this.commentaire
        }
    }

    get amortissementAnnuel() {
        return this.amortissement > 0 ? (this.prix * this.quantite)/ this.amortissement : 0;
    }
    
    get prixTotalTTC(){
        return this.quantite * this.prix ;
    }

    get prixTotalHT(){
        return this.taxeable ?  (1 - (this.tva/100)) * this.quantite * this.prix : this.quantite * this.prix;
    }
}