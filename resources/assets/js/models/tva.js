export default class tva {
    constructor({active = false, alias = 'normale', label='Tva normale', value=18, group = "default"}) {
        this.active = active;
        this.alias = alias;
        this.label = label;
        this.value = value;
        this.group = group;
    }

    toJSON() {
        return {
            'active': this.active,
            'alias': this.alias,
            'label': this.label,
            'value': this.value,
            'group': this.group

        }
    }
}