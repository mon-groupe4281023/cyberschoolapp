import PdfModel from "../../../../pdf/PdfModel";

class PdfBilanPrevisionnel extends PdfModel{

    /**
     *
     * @param base
     * @param base.title {String} Titre du document
     * @param base.data {Object} Ensemble de données
     * @param options {Object} - Autres informations sur le Document
     */
    constructor (base = {}, options = {}) {
        super(base, options)
    }

    /**
     * Style par défaut appliquer à tt les éléments d'un PDF
     */
    defaultStyle() {
        return {
            fontSize: 11.2,
            bold: false,
            alignment: 'left',
            font: this.options['meta'].police
        }
    }

    docDefinition(docDefinition = {}) {
        this.getDefaultFooter();
        return super.docDefinition(docDefinition);
    }

    buildArray (element, columns) {
        return this.getBodyArrayByLine(
            element.items,
            columns,
            this.getOptionsCell({
                style: ['lefted', 'normal'],
                borderColor: ['#efefef', '#efefef', '#efefef', '#efefef'],
                border: [false, false, false, true]
            }, [{
                position: 1,
                style: {
                    style: ['righted', 'normal'],
                    borderColor: ['#efefef', '#efefef', '#efefef', '#efefef'],
                    border: [false, false, false, true],
                    type: {name: 'FCFA'}
                }
            }], 2)
        );
    }

    columnsHeader () {
        return {
            columns: [
                this.getColumn(
                    {content: 'ACTIF'},
                    {width: '50%', margin: [115, 15]}
                ),
                this.getColumn(
                    {content: 'PASSIF'},
                    {width: '50%',  margin: [112, 15]}
                )
            ], columnGap: 5
        }
    }

    columnsContent (firstColumn, secondColumn) {
        const widthArray = ['*', '*']
        const width = '*'
        let table = PdfModel.getArray(widthArray, firstColumn, {}, false)
        let columns = [{width, table}]
        table = PdfModel.getArray(widthArray, secondColumn, {}, false)
        columns.push({width, table})
        console.log(columns)
        return { columns: columns, columnGap: 5}
    }

    subColumnsHeader (title, price, first = false) {
        const result = [
            PdfModel.getCell(title.toUpperCase(), {
                style: ['lefted', 'bgColorLightGrey', 'subHeaderBold']
            }),
            PdfModel.getCell(price, { 
                style: ['righted', 'bgColorLightGrey'], type: {name: 'FCFA'}
            })
        ];
        return (first) ? result : [result]
    }

    content() {
        return [
            {
                columns: [
             
                    ...(this.options['meta'].userLogo!=''?   [{
                        width: '30%',
                        stack: [
                            {
                                image:this.options['meta'].userLogo,
                                alignment: 'right',
                                height: 100,
                                width: 100
                            }
                        ],
                        margin:[5,-22,0,0]
                    }] : [] ),
                    ...(this.options['meta'].logoEntreprise && this.options['meta'].logoEntreprise!=''?   [{
                        width: '30%',
                        stack: [
                            {
                                image:this.options['meta'].logoEntreprise,
                                alignment: 'right',
                                height: 100,
                                width: 100
                            }
                        ],
                        margin:[0,0,0,0]
                    }] : [] ),
                ],
                columnGap: 4,
            },
            PdfModel.getCell('BILAN PREVISIONNEL HT', {
                style: ['bigHeader']
            }),
            PdfModel.getCell('Projet: ' + this.options.projet, {
                style: ['title'],
                margin: [0,30,0, 0]
            }),
            /**
             * Actif, Passif
             */
            this.columnsHeader(),
            /**
             * ACTIFS IMMOBILISÉS, Capitaux propres
            */
           this.columnsContent([[
                    PdfModel.getCell('ACTIFS IMMOBILISÉS', {
                        style: ['lefted', 'bgColorGrey', 'header']
                    }),
                    PdfModel.getCell(this.data.headerTotaux.immobilises, {
                        style: ['righted', 'bgColorGrey', 'header'],
                        type: {name: 'FCFA'}
                    })
                ]], [[
                    PdfModel.getCell('CAPITAUX PROPRES', {
                        style: ['lefted', 'bgColorGrey', 'header']
                    }),
                    PdfModel.getCell(this.data.headerTotaux.capitauxPropres, {
                        style: ['righted', 'bgColorGrey', 'header'],
                        type: {name: 'FCFA'}
                    })
            ]])
            /*{
                columns: [
                    {
                        width: '*',
                        table: PdfModel.getArray(['*', '*'], [
                            [
                                PdfModel.getCell('ACTIFS IMMOBILISÉS', {
                                    style: ['lefted', 'bgColorGrey', 'header']
                                }),
                                PdfModel.getCell(this.data.headerTotaux.immobilises, {
                                    style: ['righted', 'bgColorGrey', 'header'],
                                    type: {name: 'FCFA'}
                                })
                            ]
                        ])
                    },
                    {
                        width: '*',
                        table: PdfModel.getArray(['*', '*'], [
                            [
                                PdfModel.getCell('CAPITAUX PROPRES', {
                                    style: ['lefted', 'bgColorGrey', 'header']
                                }),
                                PdfModel.getCell(this.data.headerTotaux.capitauxPropres, {
                                    style: ['righted', 'bgColorGrey', 'header'],
                                    type: {name: 'FCFA'}
                                })
                            ]
                        ])
                    }
                ], columnGap: 5
            },
            /**
             * Elements ACTIFS IMMOBILISÉS and Elements Capitaux propres
             
            {
                columns: [
                    /**
                     * Elements ACTIFS IMMOBILISÉS
                     
                    {
                        width: '*',
                        table: PdfModel.getArray(['65%', '*'],
                            [
                                this.subColumnsHeader(
                                    'Immobilisations incorporelles',
                                    this.data.dataSource.immoIncorporelle.result, true
                                )
                            ].concat(this.buildArray(this.data.dataSource.immoIncorporelle, ['titre', 'prix'])).concat(
                                this.subColumnsHeader(
                                    'Immobilisations corporelles',
                                    this.data.dataSource.immoCorporelle.result
                                )
                            ).concat(this.buildArray(this.data.dataSource.immoIncorporelle, ['titre', 'prix'])).concat(
                                this.subColumnsHeader(
                                    'Immobilisations financières',
                                    this.data.dataSource.immoFinanciere.result
                                )
                            ).concat(this.buildArray(this.data.dataSource.immoFinanciere, ['titre', 'montant'])).concat(
                                this.subColumnsHeader(
                                    'Amortissements',
                                    this.data.dataSource.amortissementCorporelle.result
                                )
                            ).concat(this.buildArray(this.data.dataSource.amortissementCorporelle, ['titre', 'prix']))
                        )
                    },
                    /**
                     * Elements Capitaux propres
                     
                    {
                        width: '*',
                        table: PdfModel.getArray(['65%', '*'],
                            [
                                this.subColumnsHeader(
                                    'Capitaux numéraire et nature',
                                    this.data.dataSource.capital.result, true
                                )
                            ].concat(this.buildArray(this.data.dataSource.capital, ['apporteur', 'montant'])).concat(
                                this.subColumnsHeader(
                                    'Emprunts',
                                    this.data.dataSource.emprunt.result
                                )
                            ).concat(this.buildArray(this.data.dataSource.emprunt, ['titre', 'montant'])).concat(
                                this.subColumnsHeader(
                                    'Résultat net',
                                    this.data.resultatNet
                                )
                            )
                        )
                    }
                ], columnGap: 5
            },
            {text: '', pageBreak: 'before'},
            /**
             * Actif, Passif
             
            this.columnsHeader(),
            /**
             *  ACTIFS CIRCULANTS, Dettes
             
            {
                columns: [
                    {
                        width: '*',
                        table: PdfModel.getArray(['*', '*'], [
                            [
                                PdfModel.getCell('ACTIFS CIRCULANTS', {
                                    style: ['lefted', 'bgColorGrey', 'header']
                                }),
                                PdfModel.getCell(this.data.headerTotaux.circulants, {
                                    style: ['righted', 'bgColorGrey', 'header'],
                                    type: {name: 'FCFA'}
                                })
                            ]
                        ])
                    },
                    {
                        width: '*',
                        table: PdfModel.getArray(['*', '*'], [
                            [
                                PdfModel.getCell('DETTES', {
                                    style: ['lefted', 'bgColorGrey', 'header']
                                }),
                                PdfModel.getCell(this.data.headerTotaux.dettes, {
                                    style: ['righted', 'bgColorGrey', 'header'],
                                    type: {name: 'FCFA'}
                                })
                            ]
                        ])
                    }
                ], columnGap: 5
            },
            /**
             * Elements ACTIFS CIRCULANTS and Dettes
             
            {
                columns: [
                    {
                        width: '*',
                        table: PdfModel.getArray(['*', '*'],
                            [
                                this.subColumnsHeader(
                                    'Stocks en cours',
                                    this.data.dataSource.stockEnCours.result, true
                                )
                            ].concat(this.buildArray(this.data.dataSource.stockEnCours, ['titre', 'montant'])).concat(
                                this.subColumnsHeader(
                                    'Créance',
                                    this.data.dataSource.creance.result
                                )
                            ).concat(this.buildArray(this.data.dataSource.creance, ['titre', 'montant'])).concat(
                                this.subColumnsHeader(
                                    'Compte courant',
                                    this.data.dataSource.compteCourant.result
                                )
                            ).concat(this.buildArray(this.data.dataSource.compteCourant, ['apporteur', 'montant']))
                        )
                    },
                    {
                        width: '*',
                        table: PdfModel.getArray(['*', '*'],
                            [
                                this.subColumnsHeader(
                                    'Dettes commerciales',
                                    this.data.dataSource.detteFournisseur.result, true
                                )
                            ].concat(this.buildArray(this.data.dataSource.detteFournisseur, ['titre', 'montant'])).concat(
                                this.subColumnsHeader(
                                    'Autres créances',
                                    this.data.dataSource.detteDiverse.result
                                )
                            ).concat(this.buildArray(this.data.dataSource.detteDiverse, ['titre', 'montant']))

                        )
                    }
                ]
            }

            */
        ]
    }
}

export default PdfBilanPrevisionnel
