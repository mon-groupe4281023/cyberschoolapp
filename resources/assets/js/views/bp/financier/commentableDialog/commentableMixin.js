
export default {
    methods: {
        openModal: function (model) {
            console.log('from mixin: ', model);
            this.commented = model
            this.defaultComment = this.commented.commentaire
            this.showModal = true
        },
        closeComment() {
            this.showModal = false
            this.defaultComment = ''
        },
        intercom() {
            console.log(' saising something');
            this.commented.commentaire = this.defaultComment
        }
    },
    provide() { return {child: this}},
    data() {
        return {
            showModal: false,
            defaultComment: "",
            commented: {}
        }
    },
    components: {
    commentComponent: {
      inject: ['child'],
      template: `
        <el-dialog title="Saisie de commentaire" :visible.sync="child.showModal" @close="child.closeComment()">        
                <label>Saisissez votre commentaire</label>
                    <el-input type="textarea" v-model="child.defaultComment" autocomplete="off" @change="child.intercom()">
                    </el-input>
            <span slot="footer" class="dialog-footer">
                <el-button @click="child.showModal = false">Continuer</el-button>
            </span>
        </el-dialog>
    `,
    }
  }
}
