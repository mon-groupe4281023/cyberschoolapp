export default {
    "chart": {
        "zoomType": "xy"
    },
    "exporting": {
        "chartOptions": {
            "plotOptions": {
                "series": {
                    "dataLabels": {
                        "enabled": true
                    }
                }
            }
        },
        "fallbackToExportServer": false,
        "buttons": {
            "contextButton": {
                "menuItems": ["downloadPNG", "downloadSVG", "separator", "label"]
            }
        }
    },
    "navigation": {
        "buttonOptions": {
            "align": "left"
        }
    },
    "title": {
        "text": "Seuil de rentabilité HT"
    },
    "subtitle": {
        "text": "Source: Business Plan Financier"
    },
    "xAxis": [{
        "categories": ["Mois 1", "Mois 2", "Mois 3", "Mois 4", "Mois 5", "Mois 6", "Mois 7", "Mois 8", "Mois 9", "Mois 10", "Mois 11", "Mois 12",
            "Mois 13", "Mois 14", "Mois 15", "Mois 16", "Mois 17", "Mois 18", "Mois 19", "Mois 20", "Mois 21", "Mois 22", "Mois 23", "Mois 24",
             "Mois 25", "Mois 26","Mois 27", "Mois 28", "Mois 29", "Mois 30", "Mois 31", "Mois 32", "Mois 33", "Mois 34", "Mois 35", "Mois 36"],
        "crosshair": true
    }],
    "yAxis": [{
        "labels": {
            "formatter": function() { 
                // return this.value + " FCFA";
                let separator = JSON.parse(localStorage.getItem("currencySession"))?.['thousand-separator'] ?? " ";
                let chh = [];
                let elt = [];
                let tabs = this.value.toString().split("");
                if (this.value.toString().length <= 3) return this.value.toString();
                while ((tabs.length - 1) % 3 !== 0) elt.push(tabs.shift());
                tabs.forEach((item, index, tab) => {
                    chh.push(item);
                    if (index % 3 === 0 && index !== tab.length - 1) chh.push(separator);
                });
                return elt.join("") + chh.join("") + " FCFA";
            },
            "style": {
                "color": "black"
            }
        },
        "title": {
            "text": "Chiffres d'affaires HT",
            "style": {
                "color": "black"
            }
        }
    }],
    "tooltip": {
        "shared": true
    },
    "legend": {
        "layout": "vertical",
        "align": "left",
        "x": 120,
        "verticalAlign": "top",
        "y": 100,
        "floating": true,
        "backgroundColor": "rgba(255,255,255,0.25)"
    },
    "plotOptions": {
        "series": {
            "grouping": false,
            "borderWidth": 0
        }
    },
    "series": [{
        "name": "Chiffre d'Affaire cumulé HT",
        "type": "column",
        "data": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        "tooltip": {
            "valueSuffix": " FCFA"
        }        
    }, {
        "name": "Seuil de rentabilité",
        "type": "line",
        "data": [0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0],
        "tooltip": {
            "valueSuffix": " FCFA"
        },
        "color": "orange"
    },{
        "name": "Chiffre d'Affaire HT",
        "type": "column",
        "pointPlacement": -0.2,
        "data": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        "tooltip": {
            "valueSuffix": " FCFA"
        }        
    },{
        "color": "blue",
        "name": "Point Mort",
        "type": "column"
    }]
}