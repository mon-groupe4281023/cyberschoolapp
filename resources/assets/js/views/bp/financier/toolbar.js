function toolbar () {
  return {
    stacked: true,
    toolbar: {
      show: true,
      tools: {
        download: true,
        selection: true,
        zoom: true,
        zoomin: true,
        zoomout: true,
        pan: true,
        reset: true | '<img src="/static/icons/reset.png" width="20">',
        customIcons: []
      },
      autoSelected: 'zoom'
    },
    zoom: {
      enabled: true,
      type: 'x',
      autoScaleYaxis: false,
      zoomedArea: {
        fill: {
          color: '#90CAF9',
          opacity: 0.4
        },
        stroke: {
          color: '#0D47A1',
          opacity: 0.4,
          width: 1
        }
      }
    },
    animations: {
      enabled: true,
      easing: 'easeinout',
      speed: 800,
      animateGradually: {
        enabled: true,
        delay: 150
      },
      dynamicAnimation: {
        enabled: true,
        speed: 350
      }
    },
    selection: {
      enabled: true,
      type: 'x',
      fill: {
        color: '#24292e',
        opacity: 0.1
      },
      stroke: {
        width: 1,
        dashArray: 3,
        color: '#24292e',
        opacity: 0.4
      },
      xaxis: {
        min: undefined,
        max: undefined
      },
      yaxis: {
        min: undefined,
        max: undefined
      }
    }
  }
}

function dataLabel() {

  return {
    enabled: true,
        enabledOnSeries: undefined,
      textAnchor: 'middle',
          offsetX: 0,
          offsetY: 0,
          style: {
        fontSize: '14px',
            fontFamily: 'Helvetica, Arial, sans-serif',
            colors: undefined
      },
      dropShadow: {
        enabled: false,
            top: 1,
            left: 1,
            blur: 1,
            opacity: 0.45
      }
    }

}