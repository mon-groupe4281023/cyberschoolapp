/* 
type ApexChart = {
    width?: string | number
    height?: string | number
    type?:
      | 'line'
      | 'area'
      | 'bar'
      | 'histogram'
      | 'pie'
      | 'donut'
      | 'radialBar'
      | 'scatter'
      | 'bubble'
      | 'heatmap'
      | 'candlestick'
      | 'radar'
      | 'rangeBar'
    foreColor?: string
    fontFamily?: string
    background?: string
    offsetX?: number
    offsetY?: number
    dropShadow?: ApexDropShadow & {
      enabledOnSeries?: number[]
      color?: string | string[]
    }
    events?: {
      animationEnd?(chart: any, options?: any): void
      beforeMount?(chart: any, options?: any): void
      mounted?(chart: any, options?: any): void
      updated?(chart: any, options?: any): void
      mouseMove?(e: any, chart?: any, options?: any): void
      click?(e: any, chart?: any, options?: any): void
      legendClick?(chart: any, seriesIndex?: number, options?: any): void
      markerClick?(e: any, chart?: any, options?: any): void
      selection?(chart: any, options?: any): void
      dataPointSelection?(e: any, chart?: any, options?: any): void
      dataPointMouseEnter?(e: any, chart?: any, options?: any): void
      dataPointMouseLeave?(e: any, chart?: any, options?: any): void
      beforeZoom?(chart: any, options?: any): void
      zoomed?(chart: any, options?: any): void
      scrolled?(chart: any, options?: any): void
    }
    brush?: {
      enabled?: boolean
      autoScaleYaxis?: boolean
      target?: string
    }
    id?: string
    locales?: ApexLocale[]
    defaultLocale?: string
    parentHeightOffset?: number
    sparkline?: {
      enabled?: boolean
    }
    stacked?: boolean
    stackType?: 'normal' | '100%'
    toolbar?: {
      show?: boolean
      tools?: {
        download?: boolean | string
        selection?: boolean | string
        zoom?: boolean | string
        zoomin?: boolean | string
        zoomout?: boolean | string
        pan?: boolean | string
        reset?: boolean | string
      }
      autoSelected?: 'zoom' | 'selection' | 'pan'
    }
    zoom?: {
      enabled?: boolean
      type?: 'x' | 'y' | 'xy'
      autoScaleYaxis?: boolean
      zoomedArea?: {
        fill?: {
          color?: string
          opacity?: number
        }
        stroke?: {
          color?: string
          opacity?: number
          width?: number
        }
      }
    }
    selection?: {
      enabled?: boolean
      type?: string
      fill?: {
        color?: string
        opacity?: number
      }
      stroke?: {
        width?: number
        color?: string
        opacity?: number
        dashArray?: number
      }
      xaxis?: {
        min?: number
        max?: number
      }
      yaxis?: {
        min?: number
        max?: number
      }
    }
    animations?: {
      enabled?: boolean
      easing?: 'linear' | 'easein' | 'easeout' | 'easeinout'
      speed?: number
      animateGradually?: {
        enabled?: boolean
        delay?: number
      }
      dynamicAnimation?: {
        enabled?: boolean
        speed?: number
      }
    }
  }
  */