/* 
type ApexLegend = {
    show?: boolean
    showForSingleSeries?: boolean
    showForNullSeries?: boolean
    showForZeroSeries?: boolean
    floating?: boolean
    inverseOrder?: boolean
    position?: 'top' | 'right' | 'bottom' | 'left'
    horizontalAlign?: 'left' | 'center' | 'right'
    fontSize?: string
    fontFamily?: string
    width?: number
    height?: number
    offsetX?: number
    offsetY?: number
    formatter?(legendName: string, opts?: any): string
    tooltipHoverFormatter?(legendName: string, opts?: any): string
    textAnchor?: string
    labels?: {
      color?: string | string[]
      useSeriesColors?: boolean
    }
    markers?: {
      width?: number
      height?: number
      strokeColor?: string
      strokeWidth?: number
      fillColors?: string[]
      offsetX?: number
      offsetY?: number
      radius?: number
      customHTML?(): string
      onClick?(): void
    }
    itemMargin?: {
      horizontal?: number
      vertical?: number
    }
    containerMargin?: {
      left?: number
      top?: number
    }
    onItemClick?: {
      toggleDataSeries?: boolean
    }
    onItemHover?: {
      highlightDataSeries?: boolean
    }
} */

let ApexLegend = {
    show: true,
    showForSingleSeries: false,
    showForNullSeries: true,
    showForZeroSeries: true,
    position: 'bottom',
    horizontalAlign: 'center', 
    floating: false,
    fontSize: '14px',
    fontFamily: 'Helvetica, Arial',
    fontWeight: 400,
    formatter: undefined,
    inverseOrder: false,
    width: undefined,
    height: undefined,
    tooltipHoverFormatter: undefined,
    offsetX: 0,
    offsetY: 0,
    labels: {
        colors: undefined,
        useSeriesColors: false
    },
    markers: {
        width: 12,
        height: 12,
        strokeWidth: 0,
        strokeColor: '#fff',
        fillColors: undefined,
        radius: 12,
        customHTML: undefined,
        onClick: undefined,
        offsetX: 0,
        offsetY: 0
    },
    itemMargin: {
        horizontal: 5,
        vertical: 0
    },
    onItemClick: {
        toggleDataSeries: true
    },
    onItemHover: {
        highlightDataSeries: true
    }
};

export default ApexLegend