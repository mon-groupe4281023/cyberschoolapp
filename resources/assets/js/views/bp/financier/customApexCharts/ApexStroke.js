/* 
let ApexStroke = {
    show: false, 
    curve: 'smooth',  // | 'straight' | 'stepline'
    lineCap: 'butt',  // | 'square' | 'round'
    colors: [], // string[]
    width: null, // number | number[]
    dashArray: null, // number | number[]
};
*/
let ApexStroke = {
    show: true,
    curve: 'smooth',
    lineCap: 'butt',
    colors: undefined,
    width: 2,
    dashArray: 0,      
};

export default ApexStroke