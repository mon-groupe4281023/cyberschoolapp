import AnnotationLabel from './AnnotationLabel'

let PointAnnotations = {
    "x": 0,
    "y": 0,
    "yAxisIndex": 0,
    "seriesIndex": 0,
    "marker": {
      "size": 0,
      "fillColor": "",
      "strokeColor": "",
      "strokeWidth": 0,
      "shape": "",
      "offsetX": 0,
      "offsetY": 0,
      "radius": 0,
      "cssClass": "",
    },
    "label": AnnotationLabel
}

export default PointAnnotations