/*
import ApexDropShadow from './ApexDropShadow'

let ApexXAxis = {
    type: 'numeric', //  | 'category' | 'datetime'
    categories: [], // string[] | number[]
    labels: {
      show: true,
      rotate: 0,
      rotateAlways: true,
      hideOverlappingLabels: true,
      showDuplicates: true,
      trim: true,
      minHeight: 0,
      maxHeight: 0,
      style: {
        colors: "", // | string[]
        fontSize: "",
        fontFamily: "",
        cssClass: "",
      },
      offsetX: 0,
      offsetY: 0,
      format: "",
      // formatter?(value: string, timestamp: number): string
      datetimeFormatter: {
        year: "",
        month: "",
        day: "",
        hour: "",
        minute: "",
      }
    },
    axisBorder: {
      show: true,
      color: "",
      offsetX: 0,
      offsetY: 0,
      strokeWidth: 0,
    },
    axisTicks: {
      show: true,
      borderType: "",
      color: "",
      height: 0,
      offsetX: 0,
      offsetY: 0,
    },
    tickAmount: 0, // | 'dataPoints'
    min: 0,
    max: 0,
    range: 0,
    floating: true,
    position: "",
    title: {
      text: "",
      offsetX: 0,
      offsetY: 0,
      style: {
        color: "",
        fontFamily: "",
        fontSize: "",
        cssClass: "",
      }
    },
    crosshairs: {
      show: true,
      width: 0, // | string
      position: "",
      opacity: 0,
      stroke: {
        color: "",
        width: 0,
        dashArray: 0,
      },
      fill: {
        type: "",
        color: "",
        gradient: {
          colorFrom: "",
          colorTo: "",
          stops: [],
          opacityFrom: 0,
          opacityTo: 0,
        }
      },
      dropShadow: ApexDropShadow)
    },
    tooltip: {
      enabled: true,
      offsetY: 0,
      // formatter?(value: string, opts: object): string
      style: {
        fontSize: "",
        fontFamily: "",
      }
    }
};
*/

let ApexXAxis = {
  type: 'category',
  categories: [],
  labels: {
      show: true,
      rotate: -45,
      rotateAlways: false,
      hideOverlappingLabels: true,
      showDuplicates: false,
      trim: false,
      minHeight: undefined,
      maxHeight: 120,
      style: {
          colors: [],
          fontSize: '12px',
          fontFamily: 'Helvetica, Arial, sans-serif',
          fontWeight: 400,
          cssClass: 'apexcharts-xaxis-label',
      },
      offsetX: 0,
      offsetY: 0,
      format: undefined,
      formatter: undefined,
      datetimeUTC: true,
      datetimeFormatter: {
          year: 'yyyy',
          month: "MMM 'yy",
          day: 'dd MMM',
          hour: 'HH:mm',
      },
  },
  axisBorder: {
      show: true,
      color: '#78909C',
      height: 1,
      width: '100%',
      offsetX: 0,
      offsetY: 0
  },
  axisTicks: {
      show: true,
      borderType: 'solid',
      color: '#78909C',
      height: 6,
      offsetX: 0,
      offsetY: 0
  },
  tickAmount: undefined,
  tickPlacement: 'between',
  min: undefined,
  max: undefined,
  range: undefined,
  floating: false,
  position: 'bottom',
  title: {
      text: undefined,
      offsetX: 0,
      offsetY: 0,
      style: {
          color: undefined,
          fontSize: '12px',
          fontFamily: 'Helvetica, Arial, sans-serif',
          fontWeight: 600,
          cssClass: 'apexcharts-xaxis-title',
      },
  },
  crosshairs: {
      show: true,
      width: 1,
      position: 'back',
      opacity: 0.9,        
      stroke: {
          color: '#b6b6b6',
          width: 0,
          dashArray: 0,
      },
      fill: {
          type: 'solid',
          color: '#B1B9C4',
          gradient: {
              colorFrom: '#D8E3F0',
              colorTo: '#BED1E6',
              stops: [0, 100],
              opacityFrom: 0.4,
              opacityTo: 0.5,
          },
      },
      dropShadow: {
          enabled: false,
          top: 0,
          left: 0,
          blur: 1,
          opacity: 0.4,
      },
  },
  tooltip: {
      enabled: true,
      formatter: undefined,
      offsetY: 0,
      style: {
        fontSize: 0,
        fontFamily: 0,
      },
  },
};

export default ApexXAxis;
  