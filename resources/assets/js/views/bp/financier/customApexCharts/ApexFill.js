/* 
type ApexFill = {
    colors?: any[]
    opacity?: number
    type?: string
    gradient?: {
      shade?: string
      type?: string
      shadeIntensity?: number
      gradientToColors?: string[]
      inverseColors?: boolean
      opacityFrom?: number
      opacityTo?: number
      stops?: number[]
    }
    image?: {
      src?: string[]
      width?: number
      height?: number
    }
    pattern?: {
      style?: string
      width?: number
      height?: number
      strokeWidth?: number
    }
}
*/

let ApexFill = {
    colors: undefined,
    opacity: 0.9,
    type: 'solid',
    gradient: {
        shade: 'dark',
        type: "horizontal",
        shadeIntensity: 0.5,
        gradientToColors: undefined,
        inverseColors: true,
        opacityFrom: 1,
        opacityTo: 1,
        stops: [0, 50, 100],
        colorStops: []
    },
    image: {
        src: [],
        width: undefined,
        height: undefined
    },
    pattern: {
        style: 'verticalLines',
        width: 6,
        height: 6,
        strokeWidth: 2,
    },
};

export default ApexFill