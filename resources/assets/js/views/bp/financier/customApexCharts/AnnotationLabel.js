import AnnotationStyle from './AnnotationStyle'

let AnnotationLabel = {
    "borderColor": "",
    "borderWidth": 0,
    "text": "",
    "textAnchor": "",
    "offsetX": 0,
    "offsetY": 0,
    "style": AnnotationStyle,
    "position": "",
    "orientation": ""
};

export default AnnotationLabel;