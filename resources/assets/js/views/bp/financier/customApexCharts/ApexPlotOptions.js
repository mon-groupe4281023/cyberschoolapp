import ApexDropShadow from './ApexDropShadow'

let ApexPlotOptions = {
    bar: {
      horizontal: false,
      endingShape: 'flat', // | 'rounded'
      columnWidth: "",
      barHeight: "",
      distributed: false,
      colors: {
        ranges: [{
          from: 0,
          to: 0,
          color: "",
        }],
        backgroundBarColors: [],
        backgroundBarOpacity: 0,
      },
      dataLabels: {
        maxItems: 0,
        hideOverflowingLabels: false,
        position: "",
        orientation: 'horizontal', // | 'vertical'
      }
    },
    bubble: {
      minBubbleRadius: 0,
      maxBubbleRadius: 0,
    },
    candlestick: {
      colors: {
        upward: "",
        downward: "",
      },
      wick: {
        useFillColor: false,
      }
    },
    heatmap: {
      radius: 0,
      enableShades: false,
      shadeIntensity: 0,
      reverseNegativeShade: false,
      distributed: false,
      colorScale: {
        ranges: [{
            from: 0,
            to: 0,
            color: "",
            name: "",
          }],
        inverse: false,
        min: 0,
        max: 0,
      }
    },
    pie: {
      size: 0,
      customScale: 0,
      offsetX: 0,
      offsetY: 0,
      expandOnClick: false,
      dataLabels: {
        offset: 0,
        minAngleToShowLabel: 0,
      },
      donut: {
        size: "",
        background: "",
        labels: {
          show: false,
          name: {
            show: false,
            fontSize: "",
            fontFamily: "",
            color: "",
            offsetY: 0,
          },
          value: {
            show: false,
            fontSize: "",
            fontFamily: "",
            color: "",
            offsetY: 0,
            formatter: null //formatter?(val: ""): "",
          },
          total: {
            show: false,
            showAlways: false,
            label: "",
            color: "",
            formatter: null // formatter?(w: any): "",
          }
        }
      }
    },
    radar: {
      size: 0,
      offsetX: 0,
      offsetY: 0,
      polygons: {
        strokeColor: "", // string | string[]
        connectorColors: "", // string | string[]
        fill: {
          colors: [],
        }
      }
    },
    radialBar: {
      size: 0,
      inverseOrder: false,
      startAngle: 0,
      endAngle: 0,
      offsetX: 0,
      offsetY: 0,
      hollow: {
        margin: 0,
        size: "",
        background: "",
        image: "",
        width: 0,
        height: 0,
        offsetX: 0,
        offsetY: 0,
        clipped: false,
        position: 'front', //| 'back'
        dropShadow: ApexDropShadow,
      },
      track: {
        show: false,
        startAngle: 0,
        endAngle: 0,
        background: "",
        strokeWidth: "",
        opacity: 0,
        margin: 0,
        dropShadow: ApexDropShadow,
      },
      dataLabels: {
        show: false,
        name: {
          show: false,
          fontFamily: "",
          fontSize: "",
          color: "",
          offsetY: 0,
        },
        value: {
          show: false,
          fontFamily: "",
          fontSize: "",
          color: "",
          offsetY: 0,
          formatter: null // formatter?(val: 0): "",
        },
        total: {
          show: false,
          label: "",
          color: "",
          formatter: null // formatter?(opts: any): ""
        }
      }
    }
};

export default ApexPlotOptions;