/* 
let ApexMarkers = {
    size: 0,
    colors: "", // string[]
    strokeColor: "", // string | string[]
    strokeWidth: 0, // | number[]
    strokeOpacity: 0, // | number[]
    fillOpacity: 0, // | number[]
    discrete: [], // ApexDiscretePoint[]
    shape: 'circle', // | 'square' | string[]
    radius: 0,
    offsetX: 0,
    offsetY: 0,
    onClick: null, // void()
    onDblClick: null, // void()
    hover: {
      size: 0,
      sizeOffset: 0,
    }
};
*/
let ApexMarkers = {
  size: 0,
  colors: undefined,
  strokeColors: '#fff',
  strokeWidth: 2,
  strokeOpacity: 0.9,
  strokeDashArray: 0,
  fillOpacity: 1,
  discrete: [],
  shape: "circle",
  radius: 2,
  offsetX: 0,
  offsetY: 0,
  onClick: undefined,
  onDblClick: undefined,
  showNullDataPoints: true,
  hover: {
    size: undefined,
    sizeOffset: 3
  }
};

export default ApexMarkers;