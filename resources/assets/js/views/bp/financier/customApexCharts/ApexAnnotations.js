/*
import YAxisAnnotations from './YAxisAnnotations';
import XAxisAnnotations from './XAxisAnnotations';
import PointAnnotations from './PointAnnotations';
 let ApexAnnotations = {
    "position": "",
    "yaxis": YAxisAnnotations,
    "xaxis": XAxisAnnotations,
    "points": PointAnnotations
}; */

let ApexAnnotations = {
  position: 'front' ,
  yaxis: [{
      y: 0,
      y2: null,
      strokeDashArray: 1,
      borderColor: '#c2c2c2',
      fillColor: '#c2c2c2',
      opacity: 0.3,
      offsetX: 0,
      offsetY: -3,
      yAxisIndex: 0,
      label: {
          borderColor: '#c2c2c2',
          borderWidth: 1,
          text: undefined,
          textAnchor: 'end',
          position: 'right',
          offsetX: 0,
          offsetY: 0,
          style: {
              background: '#fff',
              color: '#777',
              fontSize: '12px',
              fontWeight: 400,
              fontFamily: undefined,
              cssClass: 'apexcharts-yaxis-annotation-label',
              padding: {
                left: 5,
                right: 5,
                top: 0,
                bottom: 2,
              }
          },
      },
  }],
  xaxis: [{
    x: 0,
    x2: null,
    strokeDashArray: 1,
    borderColor: '#c2c2c2',
    fillColor: '#c2c2c2',
    opacity: 0.3,
    offsetX: 0,
    offsetY: 0,
    label: {
        borderColor: '#c2c2c2',
        borderWidth: 1,
        text: undefined,
        textAnchor: 'middle',
        position: 'top',
        orientation: 'vertical',
        offsetX: 0,
        offsetY: 0,
        style: {
            background: '#fff',
            color: '#777',
            fontSize: '12px',
            fontWeight: 400,
            fontFamily: undefined,
            cssClass: 'apexcharts-xaxis-annotation-label',
        },
    },
  }],
  points: [{
    x: 0,
    y: null,
    yAxisIndex: 0,
    seriesIndex: 0,
    marker: {
      size: 0,
      fillColor: "#fff",
      strokeColor: "#333",
      strokeWidth: 3,
      shape: "circle",
      radius: 2,
      OffsetX: 0,
      OffsetY: 0,
      cssClass: '',
    },
    label: {
        borderColor: '#c2c2c2',
        borderWidth: 1,
        text: undefined,
        textAnchor: 'middle',
        offsetX: 0,
        offsetY: -15,
        style: {
            background: '#fff',
            color: '#777',
            fontSize: '12px',
            fontWeight: 400,
            fontFamily: undefined,
            cssClass: 'apexcharts-point-annotation-label',
            padding: {
              left: 5,
              right: 5,
              top: 0,
              bottom: 2,
            }
        },
    },
    image: {
      path: undefined,
      width: 20,
      height: 20,
      offsetX: 0,
      offsetY: 0,
    }
  }],
  
  texts: [{
    x: 0,
    y: 0,
    text: '',
    textAnchor: 'start',
    foreColor: undefined,
    fontSize: '13px',
    fontFamily: undefined,
    fontWeight: 400,
    appendTo: '.apexcharts-annotations',
    backgroundColor: 'transparent',
    borderColor: '#c2c2c2',
    borderRadius: 0,
    borderWidth: 0,
    paddingLeft: 4,
    paddingRight: 4,
    paddingTop: 2,
    paddingBottom: 2,
  }],

  shapes: [{
    x: 0,
    y: 0,
    type: 'rect',
    width: '100%',
    height: 50,
    appendTo: '.apexcharts-annotations',
    backgroundColor: '#fff',
    opacity: 1,
    borderColor: '#c2c2c2',
    borderRadius: 4,
    borderWidth: 0,
  }],
  images: [{
    path: '',
    x: 0,
    y: 0,
    width: 20,
    height: 20,
    appendTo: '.apexcharts-annotations'
  }],
};

export default ApexAnnotations