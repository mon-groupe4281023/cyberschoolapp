let ApexDiscretePoint = {
    seriesIndex: 0,
    dataPointIndex: 0,
    fillColor: "",
    strokeColor: "",
    size: 0,
};

export default ApexDiscretePoint;