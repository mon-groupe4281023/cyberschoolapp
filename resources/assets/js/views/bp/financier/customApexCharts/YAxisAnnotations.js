import {AnnotationLabel} from './AnnotationLabel'

let YAxisAnnotations = {
    "y": 0,
    "y2": 0,
    "strokeDashArray": 0,
    "fillColor": "",
    "borderColor": "",
    "opacity": 0,
    "offsetX": 0,
    "offsetY": 0,
    "yAxisIndex": 0,
    "label": AnnotationLabel
};

export default YAxisAnnotations