
/* let ApexYAxis = {
  show: true,
  showAlways: true,
  seriesName: "",
  opposite: true,
  reversed: true,
    logarithmic: true,
    tickAmount: 0,
    forceNiceScale: true,
    min: 0,
    max: 0,
    floating: true,
    decimalsInFloat: 0,
    labels: {
      show: true,
      minWidth: 0,
      maxWidth: 0,
      offsetX: 0,
      offsetY: 0,
      rotate: 0,
      align: 'left', // | 'center' | 'right'
      padding: 0,
      style: {
        color: "",
        fontSize: "",
        fontFamily: "",
        cssClass: "",
      },
      // formatter?(val: number, opts?: any): string
    },
    axisBorder: {
      show: true,
      color: "",
      offsetX: 0,
      offsetY: 0,
    },
    axisTicks: {
      show: true,
      color: "",
      width: 0,
      offsetX: 0,
      offsetY: 0,
    },
    title: {
      text: "",
      rotate: 0,
      offsetX: 0,
      offsetY: 0,
      style: {
        color: "",
        fontSize: "",
        fontFamily: "",
        cssClass: "",
      }
    },
    crosshairs: {
      show: true,
      position: "",
      stroke: {
        color: "",
        width: 0,
        dashArray: 0,
      }
    },
    tooltip: {
      enabled: true,
      offsetX: 0,
    }
  };
  */

  let ApexYAxis = {
    show: true,
    showAlways: true,
    showForNullSeries: true,
    seriesName: undefined,
    opposite: false,
    reversed: false,
    logarithmic: false,
    tickAmount: 6,
    min: 6,
    max: 6,
    forceNiceScale: false,
    floating: false,
    decimalsInFloat: undefined,
    labels: {
        show: true,
        align: 'right',
        minWidth: 0,
        maxWidth: 160,
        style: {
            colors: [],
            fontSize: '12px',
            fontFamily: 'Helvetica, Arial, sans-serif',
            fontWeight: 400,
            cssClass: 'apexcharts-yaxis-label',
        },
        offsetX: 0,
        offsetY: 0,
        rotate: 0,
        formatter: (value) => { return value },
    },
    axisBorder: {
        show: true,
        color: '#78909C',
        offsetX: 0,
        offsetY: 0
    },
    axisTicks: {
        show: true,
        borderType: 'solid',
        color: '#78909C',
        width: 6,
        offsetX: 0,
        offsetY: 0
    },
    title: {
        text: undefined,
        rotate: -90,
        offsetX: 0,
        offsetY: 0,
        style: {
            color: undefined,
            fontSize: '12px',
            fontFamily: 'Helvetica, Arial, sans-serif',
            fontWeight: 600,
            cssClass: 'apexcharts-yaxis-title',
        },
    },
    crosshairs: {
        show: true,
        position: 'back',
        stroke: {
            color: '#b6b6b6',
            width: 1,
            dashArray: 0,
        },
    },
    tooltip: {
        enabled: true,
        offsetX: 0,
    },
    
};

export default ApexYAxis;
  
  