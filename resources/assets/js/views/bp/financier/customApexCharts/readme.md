##  Props
Here are the basic props you can pass to the the `` component


| Prop                                                       | Description                                                  | Type               | Default   |
| ---------------------------------------------------------- | ------------------------------------------------------------ | ------------------ | --------- |
| [type](https://apexcharts.com/docs/options/chart/type)     | The chart type which is a mandatory prop to specify          | String             | ‘line’    |
| [series](https://apexcharts.com/docs/options/series)       | The data which you want to display in the chart              | Array              | undefined |
| [width](https://apexcharts.com/docs/options/chart/width)   | Width of the chart                                           | String \|\| Number | ‘100%’    |
| [height](https://apexcharts.com/docs/options/chart/height) | Height of the chart                                          | String \|\| Number | ‘auto’    |
| [options](https://apexcharts.com/docs/options)             | All the optional configurat° of the chart goes in this prop  | Object             | {}        |