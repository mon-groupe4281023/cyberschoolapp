/*
type ApexDataLabels = {
    enabled?: boolean
    enabledOnSeries?: undefined | boolean
    textAnchor?: 'start' | 'middle' | 'end'
    offsetX?: number
    offsetY?: number
    style?: {
      fontSize?: string
      fontFamily?: string
      colors?: string[]
    }
    dropShadow?: ApexDropShadow
    formatter?(val: number, opts?: any): string
};
 */

 let ApexDataLabels =  {
    enabled: true,
    enabledOnSeries: undefined,
    formatter: function (val, opts) {
        return val
    },
    textAnchor: 'middle',
    distributed: false,
    offsetX: 0,
    offsetY: 0,
    style: {
        fontSize: '14px',
        fontFamily: 'Helvetica, Arial, sans-serif',
        fontWeight: 'bold',
        colors: undefined
    },
    background: {
      enabled: true,
      foreColor: '#fff',
      padding: 4,
      borderRadius: 2,
      borderWidth: 1,
      borderColor: '#fff',
      opacity: 0.9,
      dropShadow: {
        enabled: false,
        top: 1,
        left: 1,
        blur: 1,
        color: '#000',
        opacity: 0.45
      }
    },
    dropShadow: {
        enabled: false,
        top: 1,
        left: 1,
        blur: 1,
        color: '#000',
        opacity: 0.45
    }
};

export default ApexDataLabels