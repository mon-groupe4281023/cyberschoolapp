let ApexDropShadow = {
    enabled: false,
    top: 0,
    left: 0,
    blur: 0,
    opacity: 0
};

export default ApexDropShadow;