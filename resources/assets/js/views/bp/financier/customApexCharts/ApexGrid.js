/*
type ApexGrid = {
    show?: boolean
    borderColor?: string
    strokeDashArray?: number
    position?: 'front' | 'back'
    xaxis?: {
      lines?: {
        show?: boolean
        offsetX?: number
        offsetY?: number
      }
    }
    yaxis?: {
      lines?: {
        show?: boolean
        offsetX?: number
        offsetY?: number
      }
    }
    row?: {
      colors?: string[]
      opacity?: number
    }
    column?: {
      colors?: string[]
      opacity?: number
    }
    padding?: {
      top?: number
      right?: number
      bottom?: number
      left?: number
    }
} */

let ApexGrid = {
    show: true,
    borderColor: '#90A4AE',
    strokeDashArray: 0,
    position: 'back',
    xaxis: {
        lines: {
            show: false
        }
    },   
    yaxis: {
        lines: {
            show: false
        }
    },  
    row: {
        colors: undefined,
        opacity: 0.5
    },  
    column: {
        colors: undefined,
        opacity: 0.5
    },  
    padding: {
        top: 0,
        right: 0,
        bottom: 0,
        left: 0
    },  
};

export default ApexGrid