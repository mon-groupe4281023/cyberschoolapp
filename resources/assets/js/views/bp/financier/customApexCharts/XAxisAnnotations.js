import AnnotationLabel from './AnnotaionLabel'

let XAxisAnnotations = {
    x: 0,
    x2: 0,
    strokeDashArray: 0,
    fillColor: "",
    borderColor: "",
    opacity: 0,
    offsetX: 0,
    offsetY: 0,
    label: AnnotationLabel
};

export default XAxisAnnotations