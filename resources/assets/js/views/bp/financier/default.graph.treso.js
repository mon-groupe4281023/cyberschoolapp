export default {
    "chart": {
        "zoomType": "xy"
    },
    "exporting": {
        "chartOptions": {
            "plotOptions": {
                "series": {
                    "dataLabels": {
                        "enabled": true
                    }
                }
            }
        },
        "fallbackToExportServer": false,
        "buttons": {
            "contextButton": {
                "menuItems": ["downloadPNG", "downloadSVG", "separator", "label"]
            }
        }
    },
    "navigation": {
        "buttonOptions": {
            "align": "left"
        }
    },
    "title": {
        "text": "Plan de trésorerie"
    },
    "subtitle": {
        "text": "Source: Business Plan Financier"
    },
    "xAxis": [{
        "categories": ["initial","Mois1", "Mois2", "Mois3", "Mois4", "Mois5", "Mois6", "Mois7", "Mois8", "Mois9", "Mois10", "Mois11", "Mois12"],
        "crosshair": true
    }],
    "yAxis": [{
        "labels": {
            "formatter": function() { 
                // return this.value + " FCFA";
                let separator = JSON.parse(localStorage.getItem("currencySession"))?.['thousand-separator'] ?? " ";
                let chh = [];
                let elt = [];
                let tabs = this.value.toString().split("");
                if (this.value.toString().length <= 3) return this.value.toString();
                while ((tabs.length - 1) % 3 !== 0) elt.push(tabs.shift());
                tabs.forEach((item, index, tab) => {
                    chh.push(item);
                    if (index % 3 === 0 && index !== tab.length - 1) chh.push(separator);
                });
                return elt.join("") + chh.join("") + " FCFA";
            },
            "style": {
                "color": "black"
            }
        },
        "title": {
            "text": "Solde cumulé",
            "style": {
                "color": "black"
            }
        }
    }, {
        "title": {
            "text": "Solde de fin de mois",
            "style": {
                "color": "#14a4d9"
            }
        },
        "labels": {
            "formatter": function() { 
                // return this.value + " FCFA";
                let separator = JSON.parse(localStorage.getItem("currencySession"))?.['thousand-separator'] ?? " ";
                let chh = [];
                let elt = [];
                let tabs = this.value.toString().split("");
                if (this.value.toString().length <= 3) return this.value.toString();
                while ((tabs.length - 1) % 3 !== 0) elt.push(tabs.shift());
                tabs.forEach((item, index, tab) => {
                    chh.push(item);
                    if (index % 3 === 0 && index !== tab.length - 1) chh.push(separator);
                });
                return elt.join("") + chh.join("") + " FCFA";
            },
            "style": {
                "color":"#14a4d9"
            }
        },
        "opposite": true
    }],
    "tooltip": {
        "shared": true
    },
    "legend": {
        "layout": "vertical",
        "align": "left",
        "x": 120,
        "verticalAlign": "top",
        "y": 100,
        "floating": true,
        "backgroundColor": "rgba(255,255,255,0.25)"
    },
    "series": [{
        "name": "Solde cumulé",
        "type": "column",
        "data": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        "tooltip": {
            "valueSuffix": " FCFA"
        },
        "color": 'black'   
    }, {
        "name": "Solde de fin de mois",
        "type": "spline",
        "data": [0, 0,0,0,0,0,0,0,0,0,0,0,0],
        "tooltip": {
            "valueSuffix": " FCFA"
        },
        "color": "#14a4d9"
    }]
}