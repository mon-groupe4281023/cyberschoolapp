    import PaysModel from '../../../../store/collection/pays'
    import VilleModel from '../../../../store/collection/ville'
    
    export default {
        data() {
            return {
                tableData: [],
                tableTitles: [],
                tableProps: {
                    border: true,
                    stripe: true,
                    
                    defaultSort: {
                        prop: 'flow_no',
                        order: 'descending'
                    }
                },
            }
        },
        methods: {
            deletefirestore(id) {
                this.ref.doc(id).delete().then(function () {
                }).catch((error) => {
                    loader.hide();
                    Swal.fire({
                        type: 'error',
                        title: 'Oops...',
                        text: 'Une erreur est survenue!',
                    })
                });
            },
            handleDelete(index, row) {
                Swal.fire({
                    title: 'Êtes-vous sûr?',
                    text: "Cette action est irréversible!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: 'j\'annule',
                    confirmButtonText: 'Oui, très sûr!'
                }).then((result) => {
                    if (result.value) {
                        this.tableData = []; //on reset le data pour éviter les doublons
                        this.deletefirestore(row.id);
                    }
                })
            }
        },
        mounted () {
            let Pays = new PaysModel('pays')
            let Ville = new VilleModel('ville')
            this.ref.
            where('projectId', '==', this.projetId).
            onSnapshot((querySnapshot) => {
                querySnapshot.forEach(async (doc) => {
                    let obj = doc.data();
                    obj.id = doc.id;
                    obj.nomPrenom = doc.data().nom+' '+doc.data().prenom
                    obj.paysName = await Pays.getData(doc.data().pays.id, 'nom_fr', 'pays')
                    obj.villeName = await Ville.getData(doc.data().ville.id, 'libelle', 'ville')
                    this.tableData.push(obj);
                });
                this.loading = false;
            });
        }
    };