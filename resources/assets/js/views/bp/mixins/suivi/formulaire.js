import firebase from 'firebase'

export default {
    data(){
        return {   
            provinceRef: firebase.firestore().collection('province'),
            paysRef: firebase.firestore().collection('pays'),
            villeRef: firebase.firestore().collection('ville'),
            pays: [],
            provinces:[],
            villes:[],
            customToolbar: [
                [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
                ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
                [{ 'list': 'ordered'}, { 'list': 'bullet' }],
                [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
                ['clean']                                         // remove formatting button
            ],
        }
    },
    async  created(){
        this.provinceRef.orderBy("code", "asc").get().then((querySnapshot)=>{
            this.province=[];
            querySnapshot.forEach((doc)=> {
                this.provinces.push({
                'libelle':doc.data().libelle,
                'code':doc.data().code,
                'id':doc.id,
                'ref':doc.ref,
                })
            })
        });
        this.paysRef.orderBy("alpha2", "asc").get().then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                let obj = {};
                obj.id = doc.id;
                obj.ref = doc.ref;
                obj.nom_fr = doc.data().nom_fr;
                this.pays.push(obj);
            });
        });

        this.villeRef.orderBy("libelle", "asc").get().then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                let obj = {};
                obj.id = doc.ref;
                obj.libelle = doc.data().libelle;
                this.villes.push(obj);
            });
        });
        // this.$loading().close();
    },
};