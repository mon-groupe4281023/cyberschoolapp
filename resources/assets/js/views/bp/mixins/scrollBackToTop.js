
export default {
    methods: {
        scrollBackToTop() {
            // on va grimper jusqu'au niveau du haut de la page
            document.body.scrollTop = 30; // For Safari
            document.documentElement.scrollTop = 30; /* Chrome, Firefox, IE and Opera places the overflow at the html level, unless else is specified.
            Therefore, we use the documentElement property for these browsers
            */
        }
    }
}
