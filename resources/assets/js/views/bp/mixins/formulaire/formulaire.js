// assets/js/views/bp/mixins/financier/produits
import Helpers from "../../../../services/helper";
import currenciesMaskMixin from "../currenciesMask";
import firebase from 'firebase'

export default {
  mixins: [Helpers, currenciesMaskMixin],
  computed: {},
  data() {
    return {
      paramsTypeRecetteRef: firebase.firestore().collection('params').doc(JSON.parse(localStorage.getItem('userSession')).projet).collection('typeRecette'),
      paramsTypeDepenseRef: firebase.firestore().collection('params').doc(JSON.parse(localStorage.getItem('userSession')).projet).collection('typeDepense'),
      qte: {}
    };
  },

  methods: {
    addInput(input) {
      let obj = input;
      obj.forEach(content => {
        this.schema.produits.active.groups[0].fields.push(content);
      });
    },
    addTypeRecette(model){
      this.paramsTypeRecetteRef.get().then((querySnapshot)=>{
        let i =0
        querySnapshot.forEach((doc)=>{
         
          if(model.toLowerCase()==doc.data().recette.toLowerCase()){
            i++
          }

        })
        if(i==0){
          let obj ={
            'recette': model
         }
         this.paramsTypeRecetteRef.add(obj)
        }
      })
      
    },
    addTypeDepense(model){
      this.paramsTypeDepenseRef.get().then((querySnapshot)=>{
        let i =0
        querySnapshot.forEach((doc)=>{
         
          if(model.toLowerCase()==doc.data().depense.toLowerCase()){
            i++
          }

        })
        if(i==0){
          let obj ={
            'depense': model
         }
         this.paramsTypeDepenseRef.add(obj)
        }
      })
      
    },

    // INPUT FORM GENERATOR UPDATE
    onModelUpdated(model, schema) {
     
      console.log('shema',schema);
      console.log('model',model);
      
      //$event.stopPropagation();
      //let dateinput=model.split('/');
      let str = schema.split(".");
      let schemaSplit = str[0] + "." + str[1];
      // console.log(schemaSplit)
      //SAVE MODEL VALUE IN OBJ
      if(str[0] !== "recettes"){
         this.model[schema]=model;
      }
    
      if (str[0] == "recettes") {
        var montantFacture = this.model.recettes.montantFacture;
        //RECETTE JOURNALIERE
        if(model.at(-1)==='recetteJournaliere'){
          this.content={
            Num:{
              type:'num',
              value:1
            },
            libelle:'RECETTE JOURNALIERE',
            qte:{
              type:'qte',
              value:1
            },
            totalPayer:{
              type:'montant',
              value:0,
              typeinput:'total'
            },
            remise:{
              type:'montant',
              value:0,
              typeinput:'remise',
              define: 'none'
            },
            montantRecu:{
              type:'montant',
              value:0,
              typeinput:"recu"
            },
            datePercu:{
              type:'date',
              value:new Date()
            },
            resteAPayer:{
              type:'montant',
              value:0,
              typeinput:"reste",
              define: 'none'
            },
            dateReste:{
              type:'date',
              value:new Date()
            },
          };
          this.TableContent.push(this.content);  

        }
        else{
        switch (schemaSplit) {
          case "recettes.libelle":
            this.model.recettes.prixVenteUnitaire = 0;
            this.prix = {};
            this.qte = {};
            this.total = {};
            this.montantRecu = {};
            this.montantRemise={};
            this.resteApayer = {};
            this.resteApayer = {};
            this.date={};


            var produitSelect = model.slice(-1)[0];
            
            //Find product content
            let produit = this.model.produits.find(
              item => item.nom == produitSelect
            );
            this.content={
              Num:{
                type:'num',
                value:1
              },
              libelle:produitSelect,
              qte:{
                type:'qte',
                value:1
              },
              totalPayer:{
                type:'montant',
                value:produit.prixVenteUnitaire,
                typeinput:'total'
              },
              remise:{
                type:'montant',
                value:produit.remise,
                typeinput:'remise'
              },
              montantRecu:{
                type:'montant',
                value:0,
                typeinput:"recu"
              },
              datePercu:{
                type:'date',
                value:new Date()
              },
              resteAPayer:{
                type:'montant',
                value:0,
                typeinput:"reste"
              },
              dateReste:{
                type:'date',
                value:new Date()
              },
            };
            this.TableContent.push(this.content);  
            this.getTotalPrixProduits();
            this.getTotalMontantRecu();
            this.getTotalResteAPayer();
            //console.log(this.produitsTab);
            this.prix = {
              type: "vueNumericCustom",
              label: "Prix unitaire " + produitSelect.toLowerCase(),
              model:
                "recettes." +
                produitSelect .toLowerCase().trim().replace(/\s/g, "_"),  
              featured: true,
              required: true,
              enabled: true,
              readonly: true,
              disabled: false,
              styleClasses: "prixunitaire"
            },
            this.total = {
              type: "vueNumericCustom",
              label: "Total " + produitSelect.toLowerCase(),
              model:
                "recettes.total" +
                produitSelect .toLowerCase().trim().replace(/\s/g, "_"),  
              featured: true,
              required: true,
              enabled: true,
              readonly: true,
              disabled: false,
              styleClasses: "total"
            },
            this.montantRecu = {
              type: "vueNumericCustom",
              label: "Montant reçu " + produitSelect.toLowerCase(),
              model:
                "recettes.montant." +
                produitSelect.toLowerCase().trim().replace(/\s/g, "_"),  
              featured: true,
              required: true,
              enabled: true,
              readonly: false,
              disabled: false,
              styleClasses: "montantrecu"
            },
            this.resteApayer = {
              type: "vueNumericCustom",
              label: "Reste à payer " + produitSelect.toLowerCase(),
              model:
                "recettes.resteApayer" +
                produitSelect .toLowerCase().trim().replace(/\s/g, "_"),  
              featured: true,
              required: true,
              enabled: true,
              readonly: true,
              disabled: false,
              styleClasses: "resteapayer"
            },
            this.date={
            
              type: "datePickerCustom",
              label: "Date",
              model:
              "recettes.dates." +
              produitSelect
                .toLowerCase()
                .trim()
                .replace(/\s/g, "_"),
              featured: true,
              styleClasses: 'datepickercustom',
              required: true,
              enabled:true,
              obj:'produits'
                                            
                                             
            },

            this.qte = {
              type: "input",
              inputType: "number",
              label: "Qte",
              model:
                "recettes.qte." +
                produitSelect
                  .toLowerCase()
                  .trim()
                  .replace(/\s/g, "_"),
              featured: false,
              required: true,
              styleClasses: "qte",
              className: true,
              enabled: true
            };
           /*
            this.schema.produits.active.groups[0].fields.splice(
              3,
              0,
            this.prix,this.qte, this.total,this.montantRecu,this.resteApayer,this.date
            );
            */
            this.model.recettes.qte[
              "" +
                produitSelect
                  .toLowerCase()
                  .trim()
                  .replace(/\s/g, "_") +
                ""
            ] = 1;
            this.model.recettes.produit = produit;
            this.model.recettes[produitSelect.toLowerCase()] =
              produit.prixVenteUnitaire;
              this.model.recettes.montant[produitSelect.toLowerCase()] = 0;
            this.model.recettes.montantRecu = 0 + this.model.recettes.montantRecu;
             
            this.model.recettes.image = produit.image.url;
            this.model.recettes.montantFacture =
            this.model.recettes.produit.prixVenteUnitaire *
            this.model.recettes.qte[
                  "" +
                    produitSelect
                      .toLowerCase()
                      .trim()
                      .replace(/\s/g, "_") +
                    ""
                ] +
              this.model.recettes.montantFacture;

            break;

          case "recettes.montantRecu":
            let valueinput = model;
            this.model.recettes.montantRecu = valueinput;
            this.model.recettes.resteMontant =
              this.model.recettes.montantFacture -
              this.model.recettes.montantRecu;
            // console.log(produit);

            break;
            case "recettes.remise":
             
              this.model.recettes.remise=  model;
              this.model.recettes.resteMontant =
                this.model.recettes.montantFacture -
                this.model.recettes.montantRecu -
                this.model.recettes.remise
                ;
              // console.log(produit);
    
              break;
          case "recettes.dates":
            let str = schema.split(".");
            let dateinput = model.split("/");
            let dateFormat =
              dateinput[2] + "-" + dateinput[1] + "-" + dateinput[0];
            //console.log(dateFormat)
            // console.log(schema)

            this.model[str[0]][str[1]][str[2]] = new Date(dateFormat);
           
            // console.log(this.model[str[0]][str[1]][str[2]])

            break;
          case "recettes.fichier":
          //  model.forEach((item)=>{
             
          //  })
          this.$refs.fichier.$children[0].$refs.child.fileList.push(model)
            // console.log("Data",model)
           
             
            break;
          //CLIENT
          case "client.nom":
            this.model.clients.forEach(client => {
              console.log(model);
              if (client.nom == model) {
                console.log(client.prenom)
                this.model.client.prenom = client.prenom;
                this.model.client.telephone1 = client.tel1;
                if(client.mailcontact !== undefined){
                  this.model.client.email = client.mailcontact;
                }else{
                  this.model.client.email=''
                }
                
              }
            });

            break;
            case "vendeur.nom":
              // this.model.client.prenom = "Hello World";
              this.model.vendeurs.forEach(vendeur => {
                if (vendeur.nom == model) {
                  this.model.vendeur.prenom = vendeur.prenom;
                  this.model.vendeur.telephone1 = vendeur.tel1;
                 
                }
              });

              break;
            case "recettes.type":
                if(this.$route.params.dataToUpdate){
                  this.model.recettes.type=this.$route.params.dataToUpdate.type
                  this.addTypeRecette(this.$route.params.dataToUpdate.type)
                }
                else{
                  this.model.recettes.type=model
                  this.addTypeRecette(model)
                 

                }
    
                break;

          default:
            break;
        }
      }
      }

      //QUANTITE RECETTE
      if (str[0]=="recettes" && str[1] == "qte") {
        if (model > 0) {
          //Rcherche des références du produit et mise à jour des champs
          let produit = this.model.produits.find(
            item =>
              item.nom.toLowerCase().replace(" ", "") == str[2].replace("_", "")
          );
          //console.log(str[2].replace('_', ''));

          this.model.recettes.montantFacture =
            produit.prixVenteUnitaire * this.model[str[0]][str[1]][str[2]] +
            montantFacture -
            produit.prixVenteUnitaire;
          this.model.recettes.resteMontant =
            this.model.recettes.montantFacture -
            this.model.recettes.montantRecu;
        }
      }
      if (str[0]=="recettes" && str[1] == "montant"  ) {
        this.model[str[0]][str[1]][str[2]]=model;
      }
      if (str[0] == "client") {
        this.model.clients.forEach(client => {
          switch (schemaSplit) {
            case "client.nom":
              this.model.client.prenom = "";
              this.model.client.email = "";
              // this.model.client.email = "";
              if (client.nom == model) {
                // console.log(client.prenom)

                this.model.client.prenom = client.prenom;
                this.model.client.telephone1 = client.tel1;
                if(client.email=== undefined){
                  this.model.client.email ="";
                }else{
                  this.model.client.email = client.email;
                  
                }
              
                // this.model.recettes.prixVenteUnitaire= produit.prixVenteUnitaire;

                // loader.hide();
                //console.log(produit);
              }
              break;

            default:
              break;
          }
        });
      }

      if (str[0] == "vendeur") {
        this.model.vendeurs.forEach(vendeur => {
          switch (schemaSplit) {
            case "vendeur.nom":
              this.model.vendeur.prenom = "";
              this.model.vendeur.tel1 = "";
              this.model.vendeur.email = "";
              if (vendeur.nom == model) {
                this.model.vendeur.prenom = vendeur.prenom;
                this.model.vendeur.telephone1 = vendeur.tel1;
                this.model.vendeur.email = vendeur.mail;
                // this.model.recettes.prixVenteUnitaire= produit.prixVenteUnitaire;
                //console.log(produit);
              }
              break;

            default:
              break;
          }
        });
      }

      if (str[0] == "depenses") {
        switch (schemaSplit) {
          //DEPENSES

          case "depenses.prixVenteUnitaire":
            this.model.depenses.prixVenteUnitaire = model;
            this.model.depenses.montant =
              this.model.depenses.prixVenteUnitaire *
              this.model.depenses.quantite;

            break;
          case "depenses.quantite":
            if (model > 0) {
              this.model.depenses.montant =
                this.model.depenses.prixVenteUnitaire *
                this.model.depenses.quantite;
              // console.log(produit);
            }

            break;

          case "depenses.dates":
            let str2 = schema.split(".");
            let dateinput2 = model.split("/");
            let dateFormat2 =
              dateinput2[2] + "-" + dateinput2[1] + "-" + dateinput2[0];
            //console.log(dateFormat)
            // console.log(schema)

            this.model[str2[0]][str2[1]][str2[2]] = new Date(dateFormat2);
            // console.log(this.model[str[0]][str[1]][str[2]])

            break;

          case "depenses.type":
            if (model == "Achat") {
              this.achat = true;
              //console.log(this.achat);
            } else {
              this.achat = false;
            }
            this.addTypeDepense(model)

            break;
          //CLIENT
          case "fournisseur.nom":
            this.fournisseurs.forEach(fournisseur => {
              if (fournisseur.nom == model) {
                this.model.fournisseur.telepone1 = fournisseur.tel1;
                this.model.fournisseur.telephone2 = fournisseur.tel2;
                this.model.fournisseur.email = fournisseur.mail;
              }
            });

            break;
          //CLIENT
          case "depenses.fichier":
          

            break;

          default:
            break;
        }
        //
      }

      if (str[0] == "fournisseur") {
        this.fournisseurs.forEach(fournisseur => {
          switch (schemaSplit) {
            case "fournisseur.nom":
              this.model.fournisseur.telephone1 = "";
              this.model.fournisseur.email = "";
              if (fournisseur.nom == model) {
                let loader = Vue.$loading.show();
                this.model.fournisseur.telephone1 = fournisseur.tel1;
                this.model.fournisseur.email = fournisseur.mail;
                // this.model.recettes.prixVenteUnitaire= produit.prixVenteUnitaire;

                loader.hide();
                //console.log(produit);
              }
              break;

            default:
              break;
          }
        });
      }
      // if(this.$route.params.dataToUpdate){
      //   this.model.recettes.type=this.$route.params.dataToUpdate.type
      //   console.log("Last Call",this.model.recettes.type,)
        

      // }
    },
    
    //Activer et désactiver les champs
    onVisibleChange: function(obj, index) {
      console.log(obj.obj);
      let section = obj.obj;

      if (obj.enabled) {
        this.schema[section].inactive.groups[0].fields.splice(index, 1);
        this.schema[section].active.groups[0].fields.push(obj);
      } else {
        // alert(index);
        this.schema[section].active.groups[0].fields.splice(index, 1);
        this.schema[section].inactive.groups[0].fields.push(obj);
      }
      //alert(size);

      //this.schema.texte.fields.push(obj);
      //this.index=size
      // let size=this.schema.texte.fields.length;
      /* this.clientRef.doc(clientId).get().then((doc) => {
                this.recettes.client = doc.data();
                loader.hide();
            }); */
    },
    getTotalPrixProduits(){
      this.totalPrixProduits=0;
        this.TableContent.forEach((item)=> {
          this.totalPrixProduits+=item.totalPayer.value
        });
      
    },
    getTotalMontantRecu(){
      this.totalMontantRecu=0;
        this.TableContent.forEach((item)=> {
          this.totalMontantRecu+=item.montantRecu.value
        });
      
      
    },
    getTotalResteAPayer(){
      this.totalResteAPayer=0;
        this.TableContent.forEach((item)=> {
          this.totalResteAPayer+=item.resteAPayer.value
        });
      },
    //CALCUL TOTAL RECETTE
    totalRecetteCalendrier(){
      this.recetteDetails.produits.forEach((item)=> {
      this.totaRecetteCalendrier+=item.totalPayer.value
      });
  },
    deleteItem(oldValue, model) {
      // console.log("Suppression", oldValue);

      // for (var i = 0; i < oldValue.length; i++) {
      //   if (newValue.indexOf(oldValue[i])===-1) {
      //     var valeurChamp = oldValue[i];
      //   }
      // }
      //console.log(valeurChamp);
      let indexcontent=this.TableContent.indexOf( this.TableContent.find(
        item => item.libelle == model
      ));
      this.TableContent.splice(indexcontent, 1);
      this.getTotalPrixProduits();
      this.getTotalMontantRecu();
      this.getTotalResteAPayer();



    },
    calculTotalPrix(index){
      let objProd=this.TableContent[index];
      //console.log(ObjProd);
      let qte=objProd.qte.value;
      let produit = this.model.produits.find(
        item => item.nom == objProd.libelle
      );
      let prix=produit.prixVenteUnitaire;
      objProd.totalPayer.value=qte*prix;
      objProd.resteAPayer.value=objProd.totalPayer.value-objProd.montantRecu.value;
      this.getTotalPrixProduits();
      this.getTotalMontantRecu();
      this.getTotalResteAPayer();
    },
    calculMontantReste(index){
      let objProd=this.TableContent[index];
      console.log(objProd);
      //objProd.resteAPayer.value=0;
      objProd.resteAPayer.value=objProd.totalPayer.value-objProd.remise.value-objProd.montantRecu.value;
      this.getTotalPrixProduits();
      this.getTotalMontantRecu();
      this.getTotalResteAPayer();
      //console.log('Hello')

    },
    // calculResteTotalRemise(index){
    //   let objProd=this.TableContent[index];
    //   objProd.totalPayer.value=objProd.totalPayer.value-objProd.remise.value;
    //   this.getTotalPrixProduits();
    //   this.getTotalMontantRecu();
    //   this.getTotalResteAPayer();
    //   //console.log('Hello')

    // },
    handleAddVersement(){
      this.content={
        lieu:{
          type:"texte",
          value:''
        },
        numeroCompte:{
          type:"texte",
          value:''
        },
        montant:{
          type:'montant',
          value:this.totalMontantRecu
        },
        datedepot:{
          type:'date',
          value:new Date()
        },
        operateur:{
          type:"texte",
          value:JSON.parse(localStorage.getItem('userSession')).displayName,
        },
       
      };
      this.Tableversement.push(this.content);

    },

    removeRawVersement(index){
      this.Tableversement.splice(index, 1);
      this.getTotalPrixProduits();
      this.getTotalMontantRecu();
      this.getTotalResteAPayer();
     
    },
    removeRawProduit(index,item){
      this.TableContent.splice(index, 1);
      let content=item;
     // console.log(item.libelle);
      let indexcontent=this.model.recettes.libelle.indexOf(this.model.recettes.libelle.find(
        item=>item==content.libelle
      ));
      this.model.recettes.libelle.splice(indexcontent);
      this.getTotalPrixProduits();
      this.getTotalMontantRecu();
      this.getTotalResteAPayer();


    }

  }
};
