import firebase from 'firebase'

export default {
    data() {
        return {
            currentCurrencyMask: JSON.parse(localStorage.getItem("currencySession")),
            currencyMaskConfig: []
        }
    },
    methods: {
        // `this` points to the vm instance
        applyCurrencyMask: function (symbol) {
            firebase.firestore().collection('monnaies').doc(symbol).get()
                .then( (maskConfigDocument) => {
                    if( maskConfigDocument.empty){
                        this.currentCurrencyMask = {
                            "id": "xaf",
                            "currency": "FCFA",
                            "currency-symbol-position": "suffix",
                            ":max":9999999999999999,
                            ":min":0,
                            ":minus": false,
                            "empty-value":0,
                            "output-type": "Number",
                            ":precision":0,
                            "thousand-separator": "  ",
                            "decimal-separator" : null,
                            "icon-flag": "flag-icon-ga",
                            "literal": "xaf"
                        };
                        localStorage.setItem("currencySession", JSON.stringify(this.currentCurrencyMask));
                    }
                    else{
                        this.currentCurrencyMask = {
                            "id": maskConfigDocument.id,
                            "currency": maskConfigDocument.get('currency'),
                            "currency-symbol-position": maskConfigDocument.get('currency-symbol-position'),
                            ":max": maskConfigDocument.get('max'),
                            ":min": maskConfigDocument.get('min'),
                            ":minus": maskConfigDocument.get('minus'),
                            "empty-value": maskConfigDocument.get('empty-value'),
                            "output-type": maskConfigDocument.get('output-type'),
                            ":precision": maskConfigDocument.get('precision'),
                            "thousand-separator": (maskConfigDocument.get("thousand-separator") == '\\s') ? " " : maskConfigDocument.get("thousand-separator"),
                            "decimal-separator" : (maskConfigDocument.get("decimal-separator") == '\\s') ? " " : maskConfigDocument.get("decimal-separator"),
                            "icon-flag": maskConfigDocument.get("icon-flag"),
                            "literal": maskConfigDocument.get('literal'),
                        };
                        localStorage.setItem("currencySession", JSON.stringify(this.currentCurrencyMask));
                    }
                });
        }
    },
    created:function() {

        firebase.firestore().collection('monnaies').get()
            .then( (maskConfigCollection) => {
                if( maskConfigCollection.empty){
                    this.currencyMaskConfig.push( JSON.parse(localStorage.getItem("currencySession")) );
                }
                else{
                    maskConfigCollection.forEach( (doc) => {
                        this.currencyMaskConfig.push({
                            "id": doc.id,
                            "currency": doc.get('currency'),
                            "currency-symbol-position": doc.get('currency-symbol-position'),
                            ":max": doc.get('max'),
                            ":min": doc.get('min'),
                            ":minus": doc.get('minus'),
                            "empty-value": doc.get('empty-value'),
                            "output-type": doc.get('output-type'),
                            ":precision": doc.get('precision'),
                            "thousand-separator": (doc.get("thousand-separator") == '\\s') ? " " : doc.get("thousand-separator"),
                            "decimal-separator" : (doc.get("decimal-separator") == '\\s') ? " " : doc.get("decimal-separator"),
                            "icon-flag": doc.get("icon-flag"),
                            "literal": doc.get('literal'),
                        });
                    });
                };
            })
    }
}
