// bp/mixins/financier/besoins.js
import {arround, amountPerYear, HT} from '../../../../services/helper'
import currenciesMaskMixin from '../currenciesMask'
export const currentCurrencyMask = JSON.parse(localStorage.getItem("currencySession"));

export function prixImmoTotalTTC(immo) {
    return immo.taxeable
      ? parseInt(immo.quantite ) * immo.prix
      : arround(
          (1 + immo.tva / 100) * parseInt(immo.quantite) * immo.prix,
          currentCurrencyMask[":precision"]
        );
  }
export function  prixImmoTotalHT(immo) {    
  
    return immo.taxeable
      ? arround(
          (1 - immo.tva / 100) * parseInt(immo.quantite)* immo.prix,
          currentCurrencyMask[":precision"]
        )
      :parseInt(immo.quantite ) * immo.prix;
  }
export function totalPrixIncorporel(item){
    return item.prix * item.quantite
}

export function totalPrixCorporel(item){
    return item.prix * item.quantite
}

export function amortissementLineaire(item){
    return item.amortissement > 0 ?
        totalPrixCorporel(item) / item.amortissement : 0
}

export function totalMontantFinancier(item){
    return item.montant * item.quantite
}

export function totalCoutBfrOuverture(item){
    return item['coutMensuel'] * item.duree * item.pourcentage / 100;
}

export function totalCoutBfrExploitation(item){
    return item.annee1 + item.annee2 + item.annee3 
}

export function calculAmortissementImmoCorporelle(itemImmoCorp){
    let result =  { 'annee1': 0, 'annee2':0, 'annee3':0, 'annee4':0 };
    if(itemImmoCorp){
        switch(true){
            case (itemImmoCorp.amortissement <= 1 && itemImmoCorp.amortissement > 0):
                result.annee1 = arround( this.prixImmoTotalHT(itemImmoCorp) / itemImmoCorp.amortissement, this.currentCurrencyMask[':precision']);
            break;
            case itemImmoCorp.amortissement > 1 && itemImmoCorp.amortissement <= 2:
                result.annee1 = arround(this.prixImmoTotalHT(itemImmoCorp) / itemImmoCorp.amortissement, this.currentCurrencyMask[':precision']);
                result.annee2 = arround(this.prixImmoTotalHT(itemImmoCorp) / itemImmoCorp.amortissement, this.currentCurrencyMask[':precision']);
            break;
            case (itemImmoCorp.amortissement > 2 && itemImmoCorp.amortissement <= 3):
                result.annee1 = arround(this.prixImmoTotalHT(itemImmoCorp)  / itemImmoCorp.amortissement, this.currentCurrencyMask[':precision']);
                result.annee2 = arround(this.prixImmoTotalHT(itemImmoCorp)  / itemImmoCorp.amortissement, this.currentCurrencyMask[':precision']);
                result.annee3 = arround(this.prixImmoTotalHT(itemImmoCorp)  / itemImmoCorp.amortissement, this.currentCurrencyMask[':precision']);
            break;
            case (itemImmoCorp.amortissement > 3):
                result.annee1 = arround(this.prixImmoTotalHT(itemImmoCorp) / itemImmoCorp.amortissement, this.currentCurrencyMask[':precision']);
                result.annee2 = arround(this.prixImmoTotalHT(itemImmoCorp) / itemImmoCorp.amortissement, this.currentCurrencyMask[':precision']);
                result.annee3 = arround(this.prixImmoTotalHT(itemImmoCorp) / itemImmoCorp.amortissement, this.currentCurrencyMask[':precision']);
                result.annee4 = arround(this.prixImmoTotalHT(itemImmoCorp) / itemImmoCorp.amortissement, this.currentCurrencyMask[':precision']);
                break;
            }
        return result;
    }
    return result;
}

export function calculAmortissementUnitaireImmoCorporelle(itemImmoCorp){
    let result =  { annee1: 0, annee2:0, annee3:0, annee4:0 };
    if(itemImmoCorp){
        switch(true){
            case (itemImmoCorp.amortissement <= 1 && itemImmoCorp.amortissement > 0):
                result.annee1 = arround(this.prixImmoTotalHT(itemImmoCorp.prix) / (itemImmoCorp.quantite * itemImmoCorp.amortissement), this.currentCurrencyMask[':precision']);
            break;
            case itemImmoCorp.amortissement > 1 && itemImmoCorp.amortissement <= 2:
                result.annee1 = arround(this.prixImmoTotalHT(itemImmoCorp.prix) / (itemImmoCorp.quantite * itemImmoCorp.amortissement), this.currentCurrencyMask[':precision']);
                result.annee2 = arround(2 * this.prixImmoTotalHT(itemImmoCorp.prix) / (itemImmoCorp.quantite * itemImmoCorp.amortissement), this.currentCurrencyMask[':precision']);
            break;
            case (itemImmoCorp.amortissement > 2 && itemImmoCorp.amortissement <= 3):
                result.annee1 = arround((itemImmoCorp.prix  / itemImmoCorp.amortissement), this.currentCurrencyMask[':precision']);
                result.annee2 = arround(2 * this.prixImmoTotalHT(itemImmoCorp.prix)  /(itemImmoCorp.quantite * itemImmoCorp.amortissement), this.currentCurrencyMask[':precision']);
                result.annee3 = arround(3 * this.prixImmoTotalHT(itemImmoCorp.prix)  /(itemImmoCorp.quantite * itemImmoCorp.amortissement), this.currentCurrencyMask[':precision']);
            break;
            case (itemImmoCorp.amortissement > 3):
                result.annee1 = arround( this.prixImmoTotalHT(itemImmoCorp.prix) / (itemImmoCorp.quantite * itemImmoCorp.amortissement), this.currentCurrencyMask[':precision']);
                result.annee2 = arround(2 * this.prixImmoTotalHT(itemImmoCorp.prix) / (itemImmoCorp.quantite * itemImmoCorp.amortissement), this.currentCurrencyMask[':precision']);
                result.annee3 = arround(3 * this.prixImmoTotalHT(itemImmoCorp.prix) / (itemImmoCorp.quantite * itemImmoCorp.amortissement), this.currentCurrencyMask[':precision']);
                result.annee4 = arround( this.prixImmoTotalHT(itemImmoCorp.prix) / (itemImmoCorp.quantite * itemImmoCorp.amortissement), this.currentCurrencyMask[':precision']);
            break;
        }
        return result;
    }
    return result;
}

export function calculAmortissementImmoCorporelleHT(itemImmoCorp, tva){
    let result =  { 'annee1': 0, 'annee2':0, 'annee3':0, 'annee4':0 };
    if(itemImmoCorp){
        switch(true){
            case (itemImmoCorp.amortissement <= 1 && itemImmoCorp.amortissement > 0):
                result.annee1 = arround(this.prixImmoTotalHT(itemImmoCorp) / itemImmoCorp.amortissement, tva, this.currentCurrencyMask[':precision']);
            break;
            case itemImmoCorp.amortissement > 1 && itemImmoCorp.amortissement <= 2:
                result.annee1 = arround(this.prixImmoTotalHT(itemImmoCorp) / itemImmoCorp.amortissement, tva, this.currentCurrencyMask[':precision']);
                result.annee2 = arround(this.prixImmoTotalHT(itemImmoCorp) / itemImmoCorp.amortissement, tva, this.currentCurrencyMask[':precision']);
            break;
            case (itemImmoCorp.amortissement > 2 && itemImmoCorp.amortissement <= 3):
                result.annee1 = arround(this.prixImmoTotalHT(itemImmoCorp) / itemImmoCorp.amortissement, tva, this.currentCurrencyMask[':precision']);
                result.annee2 = arround(this.prixImmoTotalHT(itemImmoCorp) / itemImmoCorp.amortissement, tva, this.currentCurrencyMask[':precision']);
                result.annee3 = arround(this.prixImmoTotalHT(itemImmoCorp) / itemImmoCorp.amortissement, tva, this.currentCurrencyMask[':precision']);
            break;
            case (itemImmoCorp.amortissement > 3):
                result.annee1 = arround(this.prixImmoTotalHT(itemImmoCorp) / itemImmoCorp.amortissement, tva, this.currentCurrencyMask[':precision']);
                result.annee2 = arround(this.prixImmoTotalHT(itemImmoCorp) / itemImmoCorp.amortissement, tva, this.currentCurrencyMask[':precision']);
                result.annee3 = arround(this.prixImmoTotalHT(itemImmoCorp) / itemImmoCorp.amortissement, tva, this.currentCurrencyMask[':precision']);
                result.annee4 = arround(this.prixImmoTotalHT(itemImmoCorp) / itemImmoCorp.amortissement, tva, this.currentCurrencyMask[':precision']);
            break;
        }
        return result;
    }
    return result;
}

export function calculTotalItemBfrOuverture(ouverture) {
    if(ouverture){
      return arround((ouverture['coutMensuel'] * ouverture.duree * ouverture.pourcentage / 100), this.currentCurrencyMask[':precision']);
    }
    return 0;
}

export function calculTotalItemBfrOuvertureHT(ouverture, tva) {
    if(ouverture){
      return arround(( HT(ouverture['coutMensuel'], tva) * ouverture.duree * ouverture.pourcentage / 100), this.currentCurrencyMask[':precision']);
    }
    return 0;
}
export function  calculTotalImmoIncorporelle(immoIncorporelle){
 let prix = 0; let echeancier = 0 ; let prixHT = 0;
 if(immoIncorporelle){
     immoIncorporelle.forEach( (item) =>  {
         prix = prix + this.prixImmoTotalTTC(item);
         prixHT = prixHT + this.prixImmoTotalHT(item);
        echeancier+= item.amortissement == 0 ? 0 : (this.prixImmoTotalHT(item) / item.amortissement);
     });
 }
 this.totalImmoIncorporelle.prix= arround(prix, currentCurrencyMask[':precision']);
 this.totalImmoIncorporelle.prixHT = arround(prixHT, currentCurrencyMask[':precision']);
 this.totalImmoIncorporelle.echeancier = arround(echeancier, currentCurrencyMask[':precision']);
 return this.totalImmoIncorporelle;
}
export function calculSubTotalImmoCorpMatInfo(immoCorpMatInfo){
    let sumPrix = 0; let sumEcheancier = 0; let sumPrixHT = 0;
    if(immoCorpMatInfo){
        immoCorpMatInfo.forEach( (item) =>  {
            sumEcheancier = sumEcheancier + amountPerYear(this.prixImmoTotalHT(item), item.amortissement);
            sumPrix = sumPrix + this.prixImmoTotalTTC(item);
            sumPrixHT = sumPrixHT + this.prixImmoTotalHT(item);
        });
    }
    this.subTotalImmoCorpMatInfo.echeancier = arround(sumEcheancier, currentCurrencyMask[':precision']);
    this.subTotalImmoCorpMatInfo.prix = arround(sumPrix, currentCurrencyMask[':precision']);
    this.subTotalImmoCorpMatInfo.prixHT = arround(sumPrixHT, currentCurrencyMask[':precision']);
    this.calculTotalImmoCorporelle();
    return this.subTotalImmoCorpMatInfo;
}
export function calculTotalImmoCorporelle(){
    totalImmoCorporelle.echeancier  = arround(subTotalImmoCorpMobilier.echeancier
        + subTotalImmoCorpMatMachOutil.echeancier
        + subTotalImmoCorpBatLocEspaceVente.echeancier
        + subTotalImmoCorpVoitAutre.echeancier
        + subTotalImmoCorpMatInfo.echeancier, currentCurrencyMask[':precision']) ;
    
    totalImmoCorporelle.prix  = arround(subTotalImmoCorpMobilier.prix
        + subTotalImmoCorpMatMachOutil.prix
        + subTotalImmoCorpBatLocEspaceVente.prix
        + subTotalImmoCorpVoitAutre.prix
        + subTotalImmoCorpMatInfo.prix, currentCurrencyMask[':precision']) ;
    
    totalImmoCorporelle.prixHT  = arround(subTotalImmoCorpMobilier.prixHT
        + subTotalImmoCorpMatMachOutil.prixHT
        + subTotalImmoCorpBatLocEspaceVente.prixHT
        + subTotalImmoCorpVoitAutre.prixHT
        + subTotalImmoCorpMatInfo.prixHT, currentCurrencyMask[':precision']) ;

}
export const /* immobilisation incorporelle */ totalImmoIncorporelle =  {
    prix:0, 
    prixHT:0,
    echeancier:0
}

export const  totalAmortisIncorporelleAnnuel = {
    annee1:0,  annee2:0,  annee3:0, annee4: 0
}

export const totalAmortisCorporelleAnnuel =  {
    annee1:0,  annee2:0,  annee3:0, annee4: 0
}

export const subTotalBfrDetFour = {
    annee1:0,  annee2:0,  annee3:0
}
export const subTotalBfrCreanceCli = {
    annee1:0, annee2:0, annee3:0
}
export const subTotalBfrStock =  {
    annee1:0, annee2:0, annee3:0
}
export const totalBfrExploitation ={
    annee1:0, annee2:0, annee3:0
}
export const totalBfrOuverture = 0
export const subTotalImmoFinCaution = 0
export const subTotalImmoFinDepGarantie = 0
export const subTotalImmoFinAutre = 0
export const totalImmoFinanciere = 0
export const totalImmoCorporelle = {
    prix:0,
    prixHT:0,
    echeancier:0
}
export const subTotalAmortisAnnuelImmoCorpVoitAutre = {
    annee1:0,  annee2:0,  annee3:0, annee4: 0
}
export const  subTotalImmoCorpVoitAutre = {
    prix:0,
    prixHT:0,
    echeancier:0
}
export const  subTotalAmortisAnnuelImmoCorpBatLocEspaceVente = {
    annee1:0,  annee2:0,  annee3:0, annee4: 0
}
export const  subTotalImmoCorpBatLocEspaceVente = {
    prix:0,
    prixHT:0,
    echeancier:0
}
export const  subTotalAmortisAnnuelImmoCorpMatMachOutil = {
    annee1:0,  annee2:0,  annee3:0, annee4: 0
}
export const  subTotalImmoCorpMatMachOutil = {
    prix:0,
    prixHT:0,
    echeancier:0
}
export const subTotalAmortisAnnuelImmoCorpMobilier = {
    annee1:0,  annee2:0,  annee3:0, annee4: 0
}
export const  subTotalImmoCorpMobilier = {
    prix:0,
    prixHT:0,
    echeancier:0
}
export const  subTotalAmortisAnnuelImmoCorpMatInfo = {
    annee1:0,  annee2:0,  annee3:0, annee4: 0
}
export const  subTotalImmoCorpMatInfo = {
    prix:0,
    echeancier:0,
    prixHT:0
}


export default {
    mixins: [currenciesMaskMixin],
    data() {
        return {
        
            
            /* immobilisation corporelle*/
            subTotalImmoCorpMatInfo: subTotalImmoCorpMatInfo,
            subTotalAmortisAnnuelImmoCorpMatInfo: subTotalAmortisAnnuelImmoCorpMatInfo,
            subTotalImmoCorpMobilier: subTotalImmoCorpMobilier,
            subTotalAmortisAnnuelImmoCorpMobilier: subTotalAmortisAnnuelImmoCorpMobilier,
            subTotalImmoCorpMatMachOutil:subTotalImmoCorpMatMachOutil,
            subTotalAmortisAnnuelImmoCorpMatMachOutil: subTotalAmortisAnnuelImmoCorpMatMachOutil,
            subTotalImmoCorpBatLocEspaceVente: subTotalImmoCorpBatLocEspaceVente,
            subTotalAmortisAnnuelImmoCorpBatLocEspaceVente:subTotalAmortisAnnuelImmoCorpBatLocEspaceVente,
            subTotalImmoCorpVoitAutre: subTotalImmoCorpVoitAutre,
            subTotalAmortisAnnuelImmoCorpVoitAutre: subTotalAmortisAnnuelImmoCorpVoitAutre,
            totalImmoCorporelle:totalImmoCorporelle,
            /* immobilisation financiere*/
            subTotalImmoFinCaution: subTotalImmoFinCaution,
            subTotalImmoFinDepGarantie: subTotalImmoFinDepGarantie,
            subTotalImmoFinAutre: subTotalImmoFinAutre ,
            totalImmoFinanciere: totalImmoFinanciere,
            /* BFR */
            totalBfrOuverture:totalBfrOuverture,
            totalBfrExploitation:totalBfrExploitation,
            subTotalBfrStock:subTotalBfrStock,
            subTotalBfrCreanceCli:subTotalBfrCreanceCli,
            subTotalBfrDetFour:subTotalBfrDetFour,
            totalAmortisCorporelleAnnuel: totalAmortisCorporelleAnnuel,
            totalAmortisIncorporelleAnnuel: totalAmortisIncorporelleAnnuel,
            totalImmoIncorporelle: totalImmoIncorporelle
        }
    },
  methods: {
    totalPrixIncorporel,
    totalPrixCorporel,
    totalMontantFinancier,
    totalCoutBfrOuverture,
    prixImmoTotalTTC,
    prixImmoTotalHT,
    totalCoutBfrExploitation,
    calculTotalImmoIncorporelle,
    calculAmortissementImmoCorporelleHT,
    calculTotalImmoCorporelle,
    amortissementAnnuel(immo) {
        return immo.amortissement == 0
          ? 0
          : arround(
              this.prixImmoTotalHT(immo) / immo.amortissement,
              this.currentCurrencyMask[":precision"]
            );
      },
    arround(value, decimals) {
        return arround(value, decimals);
    },
 
    calculTotalImmoIncorporelleHT(immoIncorporelle, tva){  
    let prix, echeancier = 0;
      if(immoIncorporelle){
          immoIncorporelle.forEach( (item) =>  {
                prix = prix + this.prixImmoTotalHT(item);
                echeancier+= item.amortissement == 0 ? 0 : (item.prix * item.quantite / item.amortissement);
          });
      }
      this.totalImmoIncorporelle.prixHT = arround(prix, this.currentCurrencyMask[':precision']);
      this.totalImmoIncorporelle.echeancier = arround(echeancier, this.currentCurrencyMask[':precision']);
      return this.totalImmoIncorporelle 
    },
    calculSubTotalImmoCorpMatInfo,
    // calculSubTotalImmoCorpMatInfo(immoCorpMatInfo){
    //     let sumPrix = 0; let sumEcheancier = 0; let sumPrixHT = 0;
    //     if(immoCorpMatInfo){
    //         immoCorpMatInfo.forEach( (item) =>  {
    //             sumEcheancier = sumEcheancier + amountPerYear(this.prixImmoTotalHT(item), item.amortissement);
    //             sumPrix = sumPrix + (item.quantite * item.prix);
    //             sumPrixHT = sumPrixHT + this.prixImmoTotalHT(item);
    //         });
    //     }
    //     this.subTotalImmoCorpMatInfo.echeancier = arround(sumEcheancier, this.currentCurrencyMask[':precision']);
    //     this.subTotalImmoCorpMatInfo.prix = arround(sumPrix, this.currentCurrencyMask[':precision']);
    //     this.subTotalImmoCorpMatInfo.prixHT = arround(sumPrixHT, this.currentCurrencyMask[':precision']);
    //     this.calculTotalImmoCorporelle();
    // },
    calculSubTotalImmoCorpMobilier:function(immoCorpMobilier){
        let sumPrix = 0; let sumEcheancier = 0; let sumPrixHT = 0;
        if(immoCorpMobilier){
        immoCorpMobilier.forEach( (item) =>  {
                sumEcheancier = sumEcheancier + amountPerYear(this.prixImmoTotalHT(item), item.amortissement);
                sumPrix = sumPrix + this.prixImmoTotalTTC(item);
                sumPrixHT = sumPrixHT + this.prixImmoTotalHT(item);
            });
        }
        this.subTotalImmoCorpMobilier.echeancier = arround(sumEcheancier, this.currentCurrencyMask[':precision']);
        this.subTotalImmoCorpMobilier.prix = arround(sumPrix, this.currentCurrencyMask[':precision']);
        this.subTotalImmoCorpMobilier.prixHT = arround(sumPrixHT, this.currentCurrencyMask[':precision']);

        this.calculTotalImmoCorporelle();
    },
    calculSubTotalImmoCorpMatMachOutil: function (immoCorpMatMachOutil){
        let sumPrix = 0; let sumEcheancier = 0; let sumPrixHT = 0;
        if(immoCorpMatMachOutil){
            immoCorpMatMachOutil.forEach( (item) =>  {
                sumEcheancier = sumEcheancier + amountPerYear(this.prixImmoTotalHT(item),item.amortissement);
                sumPrix = sumPrix + this.prixImmoTotalTTC(item);
                sumPrixHT = sumPrixHT + this.prixImmoTotalHT(item);
            });
        }
        this.subTotalImmoCorpMatMachOutil.echeancier = arround(sumEcheancier, this.currentCurrencyMask[':precision']);
        this.subTotalImmoCorpMatMachOutil.prix = arround(sumPrix, this.currentCurrencyMask[':precision']);
        this.subTotalImmoCorpMatMachOutil.prixHT = arround(sumPrixHT, this.currentCurrencyMask[':precision']);
        this.calculTotalImmoCorporelle();
    },
    calculSubTotalImmoCorpBatLocEspaceVente: function(immoCorpBatLocEspaceVente) {
        let sumPrix = 0; let sumEcheancier = 0; let sumPrixHT = 0;
        if(immoCorpBatLocEspaceVente){
            immoCorpBatLocEspaceVente.forEach( (item) =>  {
                sumEcheancier = sumEcheancier + amountPerYear(prixImmoTotalHT(item),item.amortissement);
                sumPrix = sumPrix + prixImmoTotalTTC(item);
                sumPrixHT = sumPrixHT + prixImmoTotalHT(item);
            });
        }
        this.subTotalImmoCorpBatLocEspaceVente.echeancier = arround(sumEcheancier, this.currentCurrencyMask[':precision']);
        this.subTotalImmoCorpBatLocEspaceVente.prix = arround(sumPrix, this.currentCurrencyMask[':precision']);
        this.subTotalImmoCorpBatLocEspaceVente.prixHT = arround(sumPrixHT, this.currentCurrencyMask[':precision']);
        this.calculTotalImmoCorporelle();
    },
    calculSubTotalImmoCorpVoitAutre: function(immoCorpVoitAutre) {
        let sumPrix = 0; let sumEcheancier = 0; let sumPrixHT = 0;
        if(immoCorpVoitAutre){
            immoCorpVoitAutre.forEach( (item) =>  {
                sumEcheancier = sumEcheancier + amountPerYear(this.prixImmoTotalHT(item), item.amortissement);
                sumPrix = sumPrix +this.prixImmoTotalTTC(item);
                sumPrixHT = sumPrixHT + this.prixImmoTotalHT(item);
            });
        }
        this.subTotalImmoCorpVoitAutre.echeancier = arround(sumEcheancier, this.currentCurrencyMask[':precision']);
        this.subTotalImmoCorpVoitAutre.prix = arround(sumPrix, this.currentCurrencyMask[':precision']);
        this.subTotalImmoCorpVoitAutre.prixHT = arround(sumPrixHT, this.currentCurrencyMask[':precision']);
        this.calculTotalImmoCorporelle();
    },
    calculAmortissementImmoCorporelle,
    calculAmortissementUnitaireImmoCorporelle,
    calculTotalAmortissementAnnuelImmoIncorp: function(immoIncorp){
        let result = { annee1:0, annee2:0, annee3:0, annee4:0};
        if(immoIncorp){
            immoIncorp.forEach( (item) =>  {
                result.annee1 = arround(result.annee1 + this.calculAmortissementImmoCorporelle(item).annee1, this.currentCurrencyMask[':precision']);
                result.annee2 = arround(result.annee2 + this.calculAmortissementImmoCorporelle(item).annee2, this.currentCurrencyMask[':precision']);
                result.annee3 = arround(result.annee3 + this.calculAmortissementImmoCorporelle(item).annee3, this.currentCurrencyMask[':precision']);
                result.annee4 = arround(result.annee4 + this.calculAmortissementImmoCorporelle(item).annee4, this.currentCurrencyMask[':precision']);
            });
        }
        this.totalAmortisIncorporelleAnnuel = result;
    },
    calculTotalAmortissementAnnuelImmoCorp: function(){
        this.totalAmortisCorporelleAnnuel = { annee1:0, annee2:0, annee3:0, annee4:0};

        this.totalAmortisCorporelleAnnuel.annee1 = arround(this.subTotalAmortisAnnuelImmoCorpMatInfo.annee1 +
            this.subTotalAmortisAnnuelImmoCorpMobilier.annee1 + this.subTotalAmortisAnnuelImmoCorpBatLocEspaceVente.annee1
            + this.subTotalAmortisAnnuelImmoCorpVoitAutre.annee1 + this.subTotalAmortisAnnuelImmoCorpMatMachOutil.annee1, this.currentCurrencyMask[':precision']) ;

        this.totalAmortisCorporelleAnnuel.annee2 = arround(this.subTotalAmortisAnnuelImmoCorpMatInfo.annee2 +
          this.subTotalAmortisAnnuelImmoCorpMobilier.annee2 + this.subTotalAmortisAnnuelImmoCorpBatLocEspaceVente.annee2
          + this.subTotalAmortisAnnuelImmoCorpVoitAutre.annee2 + this.subTotalAmortisAnnuelImmoCorpMatMachOutil.annee2, this.currentCurrencyMask[':precision']);

          this.totalAmortisCorporelleAnnuel.annee3 = arround(this.subTotalAmortisAnnuelImmoCorpMatInfo.annee3 +
            this.subTotalAmortisAnnuelImmoCorpMobilier.annee3 + this.subTotalAmortisAnnuelImmoCorpBatLocEspaceVente.annee3
            + this.subTotalAmortisAnnuelImmoCorpVoitAutre.annee3 + this.subTotalAmortisAnnuelImmoCorpMatMachOutil.annee3, this.currentCurrencyMask[':precision']);

          this.totalAmortisCorporelleAnnuel.annee4 = arround(this.subTotalAmortisAnnuelImmoCorpMatInfo.annee4 +
            this.subTotalAmortisAnnuelImmoCorpMobilier.annee4 + this.subTotalAmortisAnnuelImmoCorpBatLocEspaceVente.annee4
            + this.subTotalAmortisAnnuelImmoCorpVoitAutre.annee4 + this.subTotalAmortisAnnuelImmoCorpMatMachOutil.annee4, this.currentCurrencyMask[':precision']);
    },
    calculTotalAmortisAnnuelImmoCorpMatInfo: function(immoCorpMatInfo){
        let result = { annee1:0, annee2:0, annee3:0, annee4:0};
        if(immoCorpMatInfo){
            immoCorpMatInfo.forEach( (item) =>  {
                result.annee1 = arround(result.annee1 + this.calculAmortissementImmoCorporelle(item).annee1, this.currentCurrencyMask[':precision']);
                result.annee2 = arround(result.annee2 + this.calculAmortissementImmoCorporelle(item).annee2, this.currentCurrencyMask[':precision']);
                result.annee3 = arround(result.annee3 + this.calculAmortissementImmoCorporelle(item).annee3, this.currentCurrencyMask[':precision']);
                result.annee4 = arround(result.annee4 + this.calculAmortissementImmoCorporelle(item).annee4, this.currentCurrencyMask[':precision']);
            });
        }
        this.subTotalAmortisAnnuelImmoCorpMatInfo = result;
        this.calculTotalAmortissementAnnuelImmoCorp();
    },
    calculTotalAmortisAnnuelImmoCorpMobilier: function(immoCorpMobilier){
        let result = { annee1:0, annee2:0, annee3:0, annee4:0};
        if(immoCorpMobilier){
            immoCorpMobilier.forEach( (item) =>  {
                result.annee1 = arround(result.annee1 + this.calculAmortissementImmoCorporelle(item).annee1, this.currentCurrencyMask[':precision']);
                result.annee2 = arround(result.annee2 + this.calculAmortissementImmoCorporelle(item).annee2, this.currentCurrencyMask[':precision']);
                result.annee3 = arround(result.annee3 + this.calculAmortissementImmoCorporelle(item).annee3, this.currentCurrencyMask[':precision']);
                result.annee4 = arround(result.annee4 + this.calculAmortissementImmoCorporelle(item).annee4, this.currentCurrencyMask[':precision']);
            });
        }
        this.subTotalAmortisAnnuelImmoCorpMobilier = result;
        this.calculTotalAmortissementAnnuelImmoCorp();
    },
    calculTotalAmortisAnnuelImmoCorpMatMachOutil: function(immoCorpMatMachOutil){
        let result = { annee1:0, annee2:0, annee3:0, annee4:0};
        if(immoCorpMatMachOutil){
            immoCorpMatMachOutil.forEach( (item) =>  {
                result.annee1 = arround(result.annee1 + this.calculAmortissementImmoCorporelle(item).annee1, this.currentCurrencyMask[':precision']);
                result.annee2 = arround(result.annee2 + this.calculAmortissementImmoCorporelle(item).annee2, this.currentCurrencyMask[':precision']);
                result.annee3 = arround(result.annee3 + this.calculAmortissementImmoCorporelle(item).annee3, this.currentCurrencyMask[':precision']);
                result.annee4 = arround(result.annee4 + this.calculAmortissementImmoCorporelle(item).annee4, this.currentCurrencyMask[':precision']);
            });
        }
        this.subTotalAmortisAnnuelImmoCorpMatMachOutil = result;
        this.calculTotalAmortissementAnnuelImmoCorp();
    },
    calculTotalAmortisAnnuelImmoCorpBatLocEspaceVente: function(immoCorpBatLocEspaceVente){
        let result = { annee1:0, annee2:0, annee3:0, annee4:0};
        if(immoCorpBatLocEspaceVente){
            immoCorpBatLocEspaceVente.forEach( (item) =>  {
                result.annee1 = arround(result.annee1 + this.calculAmortissementImmoCorporelle(item).annee1, this.currentCurrencyMask[':precision']);
                result.annee2 = arround(result.annee2 + this.calculAmortissementImmoCorporelle(item).annee2, this.currentCurrencyMask[':precision']);
                result.annee3 = arround(result.annee3 + this.calculAmortissementImmoCorporelle(item).annee3, this.currentCurrencyMask[':precision']);
                result.annee4 = arround(result.annee4 + this.calculAmortissementImmoCorporelle(item).annee4, this.currentCurrencyMask[':precision']);
            });
        }
        this.subTotalAmortisAnnuelImmoCorpBatLocEspaceVente = result;
        this.calculTotalAmortissementAnnuelImmoCorp();
    },
    calculTotalAmortisAnnuelImmoCorpVoitAutre: function(immoCorpVoitAutre){
        let result = { annee1:0, annee2:0, annee3:0, annee4:0};
        if(immoCorpVoitAutre){
            immoCorpVoitAutre.forEach( (item) =>  {
                result.annee1 = arround(result.annee1 + this.calculAmortissementImmoCorporelle(item).annee1, this.currentCurrencyMask[':precision']);
                result.annee2 = arround(result.annee2 + this.calculAmortissementImmoCorporelle(item).annee2, this.currentCurrencyMask[':precision']);
                result.annee3 = arround(result.annee3 + this.calculAmortissementImmoCorporelle(item).annee3, this.currentCurrencyMask[':precision']);
                result.annee4 = arround(result.annee4 + this.calculAmortissementImmoCorporelle(item).annee4, this.currentCurrencyMask[':precision']);
            });
        }
        this.subTotalAmortisAnnuelImmoCorpVoitAutre = result;
        this.calculTotalAmortissementAnnuelImmoCorp();
    },
    calculSubTotalImmoFinCaution: function(immoFinCaution) {
        let sum = 0;
        if(immoFinCaution){
            immoFinCaution.forEach( (item) =>  {
                sum = sum + (item.quantite * item.montant);
            });
        }
        this.subTotalImmoFinCaution = sum;
        this.calculTotalImmoFinanciere();
    },
    calculSubTotalImmoFinDepGarantie: function(immoFinDepGarantie) {
        let sum = 0;
        if(immoFinDepGarantie){
            immoFinDepGarantie.forEach( (item) =>  {
                sum = sum + (item.quantite * item.montant);
            });
        }
        this.subTotalImmoFinDepGarantie = arround(sum, this.currentCurrencyMask[':precision']);
        this.calculTotalImmoFinanciere();
    },
    calculSubTotalImmoFinAutre: function(immoFinAutre) {
        let sum = 0;
        if(immoFinAutre){
            immoFinAutre.forEach( (item) =>  {
                sum = sum + (item.quantite * item.montant);
            });
        }
        this.subTotalImmoFinAutre = arround(sum, this.currentCurrencyMask[':precision']);
        this.calculTotalImmoFinanciere();
    },
    calculTotalImmoFinanciere: function(){
        
        this.totalImmoFinanciere  = arround(this.subTotalImmoFinCaution
            + this.subTotalImmoFinDepGarantie
            + this.subTotalImmoFinAutre, this.currentCurrencyMask[':precision']) ;
    },
    calculTotalBfrOuverture: function(bfrOuverture) {
        let sum = 0;
        if(bfrOuverture){
            bfrOuverture.forEach( (item) =>  {
                sum = sum + (item['coutMensuel'] * item.duree * item.pourcentage / 100);
            });
        }
        this.totalBfrOuverture = arround(sum, this.currentCurrencyMask[':precision']);
    },
    calculTotalBfrOuvertureHT: function(bfrOuverture, tva) {
        let sum = 0;
        if(bfrOuverture){
            bfrOuverture.forEach( (item) =>  {
                sum = sum + (HT(item['coutMensuel'], tva) * item.duree * item.pourcentage / 100);
            });
        }
        this.totalBfrOuverture = arround(sum, this.currentCurrencyMask[':precision']);
    },
    calculTotalItemBfrOuverture,
    calculTotalItemBfrOuvertureHT,
    calculSubTotalBfrStockAnnee1: function(bfrStock) {
        let sum = 0;
        if(bfrStock){
            bfrStock.forEach( (item) =>  {
                sum = sum +  item.annee1 ;
            });
        }
        this.subTotalBfrStock.annee1 = arround(sum, this.currentCurrencyMask[':precision']);
        this.calculTotalBfrExploitationAnnee1();
    },
    calculSubTotalBfrStockAnnee1HT: function(bfrStock, tva) {
        let sum = 0;
        if(bfrStock){
            bfrStock.forEach( (item) =>  {
                sum = sum +  HT(item.annee1, tva) ;
            });
        }
        this.subTotalBfrStock.annee1 = arround(sum, this.currentCurrencyMask[':precision']);
        this.calculTotalBfrExploitationAnnee1();
    },
    calculSubTotalBfrStockAnnee2: function(bfrStock) {
        let sum = 0;
        if(bfrStock){
            bfrStock.forEach( (item) =>  {
                sum = sum +  item.annee2 ;
            });
        }
        this.subTotalBfrStock.annee2 = arround(sum, this.currentCurrencyMask[':precision']);
        this.calculTotalBfrExploitationAnnee2();
    },
    calculSubTotalBfrStockAnnee2HT: function(bfrStock, tva) {
        let sum = 0;
        if(bfrStock){
            bfrStock.forEach( (item) =>  {
                sum = sum +  HT(item.annee2, tva) ;
            });
        }
        this.subTotalBfrStock.annee2 = arround(sum, this.currentCurrencyMask[':precision']);
        this.calculTotalBfrExploitationAnnee2();
    },
    calculSubTotalBfrStockAnnee3: function(bfrStock) {
        let sum = 0;
        if(bfrStock){
          bfrStock.forEach( (item) =>  {
              sum = sum +  item.annee3 ;
          });
        }
        this.subTotalBfrStock.annee3 = arround(sum, this.currentCurrencyMask[':precision']);
        this.calculTotalBfrExploitationAnnee3();
    },
    calculSubTotalBfrStockAnnee3HT: function(bfrStock, tva) {
        let sum = 0;
        if(bfrStock){
          bfrStock.forEach( (item) =>  {
              sum = sum +  HT(item.annee3, tva) ;
          });
        }
        this.subTotalBfrStock.annee3 = arround(sum, this.currentCurrencyMask[':precision']);
        this.calculTotalBfrExploitationAnnee3();
    },
    calculSubTotalBfrCreanceCliAnnee1: function(creancesClient) {
        let sum = 0;
        if(creancesClient){
            creancesClient.forEach( (item) =>  {
                sum = sum +  item.annee1 ;
            });
        }
        this.subTotalBfrCreanceCli.annee1 = arround(sum, this.currentCurrencyMask[':precision']);
        this.calculTotalBfrExploitationAnnee1();
    },
    calculSubTotalBfrCreanceCliAnnee1HT: function(creancesClient, tva) {
        let sum = 0;
        if(creancesClient){
            creancesClient.forEach( (item) =>  {
                sum = sum +  HT(item.annee1, tva) ;
            });
        }
        this.subTotalBfrCreanceCli.annee1 = arround(sum, this.currentCurrencyMask[':precision']);
        this.calculTotalBfrExploitationAnnee1();
    },
    calculSubTotalBfrCreanceCliAnnee2: function(creancesClient) {
        let sum = 0;
        if(creancesClient){
            creancesClient.forEach( (item) =>  {
                sum = sum +  item.annee2 ;
            });
        }
        this.subTotalBfrCreanceCli.annee2 = sum;
        this.calculTotalBfrExploitationAnnee2();
    },
    /**
     * 
     * @param {*} creancesClient 
     * @param {*} tva 
     */
    calculSubTotalBfrCreanceCliAnnee2HT: function(creancesClient, tva) {
        let sum = 0;
        if(creancesClient){
            creancesClient.forEach( (item) =>  {
                sum = sum +  HT(item.annee2, tva) ;
            });
        }
        this.subTotalBfrCreanceCli.annee2 = sum;
        this.calculTotalBfrExploitationAnnee2();
    },
    /**
     * 
     * @param {array} creancesClient 
     */
    calculSubTotalBfrCreanceCliAnnee3: function(creancesClient) {
        let sum = 0;
        if(creancesClient){
            creancesClient.forEach( (item) =>  {
                sum = sum +  item.annee3 ;
            });
        }
        this.subTotalBfrCreanceCli.annee3 = arround(sum, this.currentCurrencyMask[':precision']);
        this.calculTotalBfrExploitationAnnee3();
    },
    calculSubTotalBfrCreanceCliAnnee3HT: function(creancesClient, tva) {
        let sum = 0;
        if(creancesClient){
            creancesClient.forEach( (item) =>  {
                sum = sum +  HT(item.annee3, tva) ;
            });
        }
        this.subTotalBfrCreanceCli.annee3 = arround(sum, this.currentCurrencyMask[':precision']);
        this.calculTotalBfrExploitationAnnee3();
    },
    calculSubTotalBfrDetFourAnnee1: function(dettesFournisseur) {
        let sum = 0;
        if(dettesFournisseur){
            dettesFournisseur.forEach( (item) =>  {
                sum = sum + item.annee1 ;
            });
        }
        this.subTotalBfrDetFour.annee1 = arround(sum, this.currentCurrencyMask[':precision']);
        this.calculTotalBfrExploitationAnnee1();
    },
    calculSubTotalBfrDetFourAnnee1HT: function(dettesFournisseur, tva) {
        let sum = 0;
        if(dettesFournisseur){
            dettesFournisseur.forEach( (item) =>  {
                sum = sum +  HT(item.annee1, tva) ;
            });
        }
        this.subTotalBfrDetFour.annee1 = arround(sum, this.currentCurrencyMask[':precision']);
        this.calculTotalBfrExploitationAnnee1();
    },
    calculSubTotalBfrDetFourAnnee2: function(dettesFournisseur) {
        let sum = 0;
        if(dettesFournisseur){
            dettesFournisseur.forEach( (item) =>  {
                sum = sum +  item.annee2 ;
            });
        }
        this.subTotalBfrDetFour.annee2 = arround(sum, this.currentCurrencyMask[':precision']);
        this.calculTotalBfrExploitationAnnee2();
    },
    calculSubTotalBfrDetFourAnnee2HT: function(dettesFournisseur, tva) {
        let sum = 0;
        if(dettesFournisseur){
            dettesFournisseur.forEach( (item) =>  {
                sum = sum +  HT(item.annee2, tva) ;
            });
        }
        this.subTotalBfrDetFour.annee2 = arround(sum, this.currentCurrencyMask[':precision']);
        this.calculTotalBfrExploitationAnnee2();
    },
    calculSubTotalBfrDetFourAnnee3: function(dettesFournisseur) {
        let sum = 0;
        if(dettesFournisseur){
            dettesFournisseur.forEach( (item) =>  {
                sum = sum +  item.annee3 ;
            });
        }
        this.subTotalBfrDetFour.annee3 = arround(sum, this.currentCurrencyMask[':precision']);
        this.calculTotalBfrExploitationAnnee3();
    },
    calculSubTotalBfrDetFourAnnee3HT: function(dettesFournisseur, tva) {
        let sum = 0;
        if(dettesFournisseur){
            dettesFournisseur.forEach( (item) =>  {
                sum = sum + HT(item.annee3, tva) ;
            });
        }
        this.subTotalBfrDetFour.annee3 = arround(sum, this.currentCurrencyMask[':precision']);
        this.calculTotalBfrExploitationAnnee3();
    },
    calculTotalBfrExploitationAnnee1: function() {
        this.totalBfrExploitation.annee1 = arround(this.subTotalBfrStock.annee1 +
        this.subTotalBfrCreanceCli.annee1 - this.subTotalBfrDetFour.annee1, this.currentCurrencyMask[':precision']);
    },
    calculTotalBfrExploitationAnnee2: function() {
        this.totalBfrExploitation.annee2 = arround(this.subTotalBfrStock.annee2 +
        this.subTotalBfrCreanceCli.annee2 - this.subTotalBfrDetFour.annee2, this.currentCurrencyMask[':precision']);
    },
    calculTotalBfrExploitationAnnee3: function() {
        this.totalBfrExploitation.annee3 = arround(this.subTotalBfrStock.annee3 +
        this.subTotalBfrCreanceCli.annee3 - this.subTotalBfrDetFour.annee3, this.currentCurrencyMask[':precision']);
    },
    echeancierAmortissement(amount, years) {
        return amountPerYear(amount, years);
    },
    /**
     * calcule la repartition par annee d'un montant
     * @param {Integer} amount 
     * @param {Integer} years 
     */
    echeancierFinancier(amount, years) {
        return amountPerYear(amount, years);
    },
    
    formatBesoinsToJSON({ immoIncorporelle=[], immoCorporelle={ materielinfo: [ ], mobilier: [ ], 'materiel-machine-outil': [ ], 'batiment-local-espacevente': [ ], 'voiture-autres': [ ] }, immoFinanciere={ cautions : [ ], depots: [ ], autres:[ ] }, bfr={ bfrOuverture:[ ], stock:[ ], creanceClient:[ ], dettes:[ ] }}) {
        return {
                "immoIncorporelle":immoIncorporelle.map( item => JSON.parse(JSON.stringify(item))),
                "immoCorporelle":{
                    "materielinfo": immoCorporelle['materielinfo'].map( item => JSON.parse(JSON.stringify(item))),
                    "mobilier": immoCorporelle['mobilier'].map( item => JSON.parse(JSON.stringify(item))),
                    "materiel-machine-outil": immoCorporelle['materiel-machine-outil'].map( item => JSON.parse(JSON.stringify(item))),
                    "batiment-local-espacevente": immoCorporelle['batiment-local-espacevente'].map( item => JSON.parse(JSON.stringify(item))),
                    "voiture-autres": immoCorporelle['voiture-autres'].map( item => JSON.parse(JSON.stringify(item)))
                },
                "immoFinanciere": immoFinanciere,
                "bfr": bfr
        }
    }
  },
  computed:{
      variationBfr: {
        get: function(){
            return {
                'annee1': arround(this.totalBfrExploitation.annee1 - this.totalBfrOuverture, this.currentCurrencyMask[':precision']),
                'annee2': arround(this.totalBfrExploitation.annee2 - this.totalBfrExploitation.annee1, this.currentCurrencyMask[':precision']),
                'annee3': arround(this.totalBfrExploitation.annee3 - this.totalBfrExploitation.annee2 , this.currentCurrencyMask[':precision'])
            };
        }
    },
  },
};
