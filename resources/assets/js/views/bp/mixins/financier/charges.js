// assets/js/views/bp/mixins/financier/charges
import {arround, HT, isEmpty} from "../../../../services/helper.js"
import currenciesMaskMixin from '../currenciesMask'
import { isNullOrUndefined } from 'util'
import defaultChargePatronale from '../../financier/default.chargePatronale.json'

export default {
    mixins: [currenciesMaskMixin],
    data() {
        return {
            /* totuax et sous-totaux pour achat de matiere premiere */
            totalAchatMpDepVariable:{
                annee1:0,
                annee2:0,
                annee3:0,
                depensesMensuellesLancement: {
                    "01":{ "ttc": 0, "ht":0 },
                    "02":{ "ttc": 0, "ht":0 },
                    "03":{ "ttc": 0, "ht":0 },
                    "04":{ "ttc": 0, "ht":0 },
                    "05":{ "ttc": 0, "ht":0 },
                    "06":{ "ttc": 0, "ht":0 },
                    "07":{ "ttc": 0, "ht":0 },
                    "08":{ "ttc": 0, "ht":0 },
                    "09":{ "ttc": 0, "ht":0 },
                    "10":{ "ttc": 0, "ht":0 },
                    "11":{ "ttc": 0, "ht":0 },
                    "12":{ "ttc": 0, "ht":0 }
                },
                depensesLancement: {
                    ht: 0,
                    ttc: 0
                }
            },
            totalAchatMpCharVariable:{
                annee1:0,
                annee2:0,
                annee3:0
            },
            /* Service exterieurs */
            totalServExtFrais:{
                annee1:0,
                annee2:0,
                annee3:0,
                depensesMensuellesLancement: {
                    "01": 0,
                    "02": 0,
                    "03": 0,
                    "04": 0,
                    "05": 0,
                    "06": 0,
                    "07": 0,
                    "08": 0,
                    "09": 0,
                    "10": 0,
                    "11": 0,
                    "12": 0
                },
                depensesLancement: 0
            },
            /* Taxes et impots */
            subTotalTaxImpImpot:{
                annee1:0,
                annee2:0,
                annee3:0
            },
            subTotalTaxImpMairie:{
                annee1:0,
                annee2:0,
                annee3:0
            },
            subTotalFraisSalarie:{
                annee1:0,
                annee2:0,
                annee3:0,
                effectif:0,
            },
            subTotalFraisDirigeant:{
                annee1:0,
                annee2:0,
                annee3:0,
                effectif:0,
            },
            subTotalFraisStagiaire:{
                annee1:0,
                annee2:0,
                annee3:0,
                effectif:0,
            },
            subTotalFraisCharge:{
                annee1:0,
                annee2:0,
                annee3:0
            },
            totalFraisPersonnel:{
                annee1:0,
                annee2:0,
                annee3:0,
                effectif:0,
                accroissement1:0,
                accroissement2:0
            },
            paramChargePatronaleDirigeant:0,
            paramChargePatronaleEmploye:0,
            paramChargePatronaleEmployeCss:0,

        }
    },
    created(){
        /** initialisation des charges variables dns la mixin 
         * on aura juste besoin de mettre a jour depuis sur les etats-f
        */
        this.paramChargePatronaleEmploye = defaultChargePatronale.employe;
        this.paramChargePatronaleEmployeCss = defaultChargePatronale.css;
        this.paramChargePatronaleDirigeant = defaultChargePatronale.dirigeant;
    },
    methods: {
        /**
         * 
         * @param {Array} ventesMensuelles 
         * @return {int}
         */
        totalAchatHTChargeProduitAnnuelle: function(ventesMensuelles){
            let result = 0;
            if(!isEmpty(ventesMensuelles)) {
                for(let property in ventesMensuelles) {
                    result += ventesMensuelles[property].ht;
                }
            }
            return result;
        },
        /**
         * 
         * @param {Array} ventesMensuelles 
         * @return {int}
         */
        totalAchatTTCChargeProduitAnnuelle: function(ventesMensuelles){
            let result = 0;
            if(!isEmpty(ventesMensuelles)) {
                for(let property in ventesMensuelles) {
                    result += ventesMensuelles[property].ttc;
                }
            }
            return result
        },
        /**
         * 
         * @param {Array} ventesMensuelles 
         * @param {string} month
         * @return {int}
         */
        calculTvaChargeProduitMensuelle: function(ventesMensuelles, month) {
            let result = 0;            
            if(!isEmpty(ventesMensuelles)) {
                result = (isEmpty(ventesMensuelles[month])) ? 0 : ventesMensuelles[month].ttc - ventesMensuelles[month].ht;
            }
            return result
        },
        /**
         * 
         * @param {Array} ventesMensuelles 
         * @return {int}
         */
        calculTvaChargeProduitAnnuelle: function(ventesMensuelles) {
            let result = 0;
            if(!isEmpty(ventesMensuelles)) {
                for(let property in ventesMensuelles) {
                    result += ventesMensuelles[property].ttc - ventesMensuelles[property].ht;
                }
            }
            return result
        },
        accroissementPrevisionnelAnnee3:function(item, section, collection){
            item.annee3 = arround(( 1 + (item.accroissement2 / 100) )* item.annee2, this.currentCurrencyMask[':precision']) ;
            switch(section){
                case "achat.depense":
                    this.calculTotalAchatMpDepVariable(collection);
                break;
                case "achat.charge":
                    this.calculTotalAchatMpCharVariable(collection);
                break;
                case "service.frais":
                    this.calculTotalServExtFrais(collection);
                break;
                case "taxe.impot":
                    this.calculSubTotalTaxImpImpot(collection);
                break;
                case "taxe.mairie":
                    this.calculSubTotalTaxeImpMairie(collection);
                break;
                case "frais.salarie":
                    this.calculSubTotalFraisSalarie(collection);
                break;
                case "frais.dirigeant":
                    this.calculSubTotalFraisDirigeant(collection);
                break;
                case "frais.stagiaire":
                    this.calculSubTotalFraisStagiaire(collection);
                break;
            }
        },
        /* Mise a jours des depenses previsionnelles pour l'anne2*/
        accroissementPrevisionnelAnnee2: function(item, section, collection){
            item.annee2 = arround(( 1 + (item.accroissement1 / 100) ) * item.annee1, this.currentCurrencyMask[':precision']) ;
            this.accroissementPrevisionnelAnnee3(item, section, collection);
            //this.capitalPrevisionnelAccruAnnee3(item);
        },
        /* calcul le capital investit lors de la premiere annee */
        totalInitial:function(item, section, collection){
            item.annee1 = arround(item.quantite * item.prix, this.currentCurrencyMask[':precision']);
            this.accroissementPrevisionnelAnnee2(item, section, collection);
            //this.capitalPrevisionnelAccruAnnee2(item);
        },
        calculTotalAchatMpDepVariableHT(depensesVariablesMP, tva) {
            let sumAnnee1 = 0; let sumAnnee2 = 0; let sumAnnee3 = 0;
            for ( let mois in this.totalAchatMpDepVariable.depensesMensuellesLancement){
                this.totalAchatMpDepVariable.depensesMensuellesLancement[mois].ttc = 0;
                this.totalAchatMpDepVariable.depensesMensuellesLancement[mois].ht = 0;
            }
            this.totalAchatMpDepVariable.depensesLancement.ht = 0;
            this.totalAchatMpDepVariable.depensesLancement.ttc = 0;
            // TODO: check that it is not empty
            if(depensesVariablesMP){
                depensesVariablesMP.forEach( (item) =>  {
                    sumAnnee1 = sumAnnee1 + HT(item.annee1, tva);
                    sumAnnee2 = sumAnnee2 + HT(item.annee2, tva);
                    sumAnnee3 = sumAnnee3 + HT(item.annee3, tva);
                    for ( let mois in this.totalAchatMpDepVariable.depensesMensuellesLancement) {
                        this.totalAchatMpDepVariable.depensesMensuellesLancement[mois].ttc = arround(
                            this.totalAchatMpDepVariable.depensesMensuellesLancement[mois].ttc + item.ventes[mois].ttc, this.currentCurrencyMask[':precision']);

                        this.totalAchatMpDepVariable.depensesMensuellesLancement[mois].ht = arround(
                            this.totalAchatMpDepVariable.depensesMensuellesLancement[mois].ht + item.ventes[mois].ht, this.currentCurrencyMask[':precision']);

                        this.totalAchatMpDepVariable.depensesLancement.ht = arround(
                            this.totalAchatMpDepVariable.depensesLancement.ht + item.ventes[mois].ht, this.currentCurrencyMask[':precision']);

                        this.totalAchatMpDepVariable.depensesLancement.ttc = arround(
                            this.totalAchatMpDepVariable.depensesLancement.ttc + item.ventes[mois].ttc, this.currentCurrencyMask[':precision']);
                    }
                });
            }
            this.totalAchatMpDepVariable.annee1 = arround(sumAnnee1, this.currentCurrencyMask[':precision']);
            this.totalAchatMpDepVariable.annee2 = arround(sumAnnee2, this.currentCurrencyMask[':precision']);
            this.totalAchatMpDepVariable.annee3 = arround(sumAnnee3, this.currentCurrencyMask[':precision']);
        },
        calculTotalAchatMpCharVariableHT(chargesVariablesMP, tva) {
            let sumAnnee1 = 0; let sumAnnee2 = 0; let sumAnnee3 = 0;
            if(chargesVariablesMP){
                chargesVariablesMP.forEach( (item) =>  {
                    sumAnnee1 = sumAnnee1 + HT(item.annee1, tva);
                    sumAnnee2 = sumAnnee2 + HT(item.annee2, tva);
                    sumAnnee3 = sumAnnee3 + HT(item.annee3, tva);
                });
            }
            this.totalAchatMpCharVariable.annee1 = arround(sumAnnee1, this.currentCurrencyMask[':precision']);
            this.totalAchatMpCharVariable.annee2 = arround(sumAnnee2, this.currentCurrencyMask[':precision']);
            this.totalAchatMpCharVariable.annee3 = arround(sumAnnee3, this.currentCurrencyMask[':precision']);
        },
        calculTotalAchatMpDepVariable(depensesVariablesMP) {
            let sumAnnee1 = 0; let sumAnnee2 = 0; let sumAnnee3 = 0;
            for ( let mois in this.totalAchatMpDepVariable.depensesMensuellesLancement){
                this.totalAchatMpDepVariable.depensesMensuellesLancement[mois].ttc = 0;
                this.totalAchatMpDepVariable.depensesMensuellesLancement[mois].ht = 0;
            }
            this.totalAchatMpDepVariable.depensesLancement.ht = 0;
            this.totalAchatMpDepVariable.depensesLancement.ttc = 0;
            // TODO: check that it is not empty
            if(depensesVariablesMP){
                depensesVariablesMP.forEach( (item) =>  {
                    sumAnnee1 = sumAnnee1 + item.annee1;
                    sumAnnee2 = sumAnnee2 + item.annee2;
                    sumAnnee3 = sumAnnee3 + item.annee3;
                    for ( let mois in this.totalAchatMpDepVariable.depensesMensuellesLancement) {
                        this.totalAchatMpDepVariable.depensesMensuellesLancement[mois].ttc = arround(
                            this.totalAchatMpDepVariable.depensesMensuellesLancement[mois].ttc + item.ventes[mois].ttc, this.currentCurrencyMask[':precision']);

                        this.totalAchatMpDepVariable.depensesMensuellesLancement[mois].ht = arround(
                            this.totalAchatMpDepVariable.depensesMensuellesLancement[mois].ht + item.ventes[mois].ht, this.currentCurrencyMask[':precision']);

                        this.totalAchatMpDepVariable.depensesLancement.ht = arround(
                            this.totalAchatMpDepVariable.depensesLancement.ht + item.ventes[mois].ht, this.currentCurrencyMask[':precision']);

                        this.totalAchatMpDepVariable.depensesLancement.ttc = arround(
                            this.totalAchatMpDepVariable.depensesLancement.ttc + item.ventes[mois].ttc, this.currentCurrencyMask[':precision']);
                    }
                });
            }
            this.totalAchatMpDepVariable.annee1 = arround(sumAnnee1, this.currentCurrencyMask[':precision']);
            this.totalAchatMpDepVariable.annee2 = arround(sumAnnee2, this.currentCurrencyMask[':precision']);
            this.totalAchatMpDepVariable.annee3 = arround(sumAnnee3, this.currentCurrencyMask[':precision']);
        },
        calculTotalAchatMpCharVariable(chargesVariablesMP) {
            let sumAnnee1 = 0; let sumAnnee2 = 0; let sumAnnee3 = 0;
            if(chargesVariablesMP){
                chargesVariablesMP.forEach( (item) =>  {
                    sumAnnee1 = sumAnnee1 + item.annee1;
                    sumAnnee2 = sumAnnee2 + item.annee2;
                    sumAnnee3 = sumAnnee3 + item.annee3;
                });
            }
            this.totalAchatMpCharVariable.annee1 = arround(sumAnnee1, this.currentCurrencyMask[':precision']);
            this.totalAchatMpCharVariable.annee2 = arround(sumAnnee2, this.currentCurrencyMask[':precision']);
            this.totalAchatMpCharVariable.annee3 = arround(sumAnnee3, this.currentCurrencyMask[':precision']);
        },
        calculTotalServExtFrais(servicesExterieurs) {
            let sumAnnee1 = 0; let sumAnnee2 = 0; let sumAnnee3 = 0;
            for(let mois in this.totalServExtFrais.depensesMensuellesLancement) {
                this.totalServExtFrais.depensesMensuellesLancement[mois] = 0;
            }
            if(servicesExterieurs){
                servicesExterieurs.forEach( (item) =>  {
                    sumAnnee1 = sumAnnee1 + item.annee1;
                    sumAnnee2 = sumAnnee2 + item.annee2;
                    sumAnnee3 = sumAnnee3 + item.annee3;
                    for(let mois in this.totalServExtFrais.depensesMensuellesLancement) {

                        this.totalServExtFrais.depensesMensuellesLancement[mois] = arround(
                                this.totalServExtFrais.depensesMensuellesLancement[mois] 
                                + this.calculServiceExterieurDuMois(item, Number(mois)),
                            this.currentCurrencyMask[':precision']);

                        this.totalServExtFrais.depensesLancement = arround(
                                this.totalServExtFrais.depensesLancement 
                                + this.calculServiceExterieurDuMois(item, Number(mois)),
                            this.currentCurrencyMask[':precision']);
                    }
                });
            }
            this.totalServExtFrais.annee1 = arround(sumAnnee1, this.currentCurrencyMask[':precision']);
            this.totalServExtFrais.annee2 = arround(sumAnnee2, this.currentCurrencyMask[':precision']);
            this.totalServExtFrais.annee3 = arround(sumAnnee3, this.currentCurrencyMask[':precision']);
        },
        calculServiceExterieurDuMois(service, moisDurant) {
            if(moisDurant <= service.quantite){
                return  service.prix ;
            }
            return 0;
        },
        calculTotalServExtFraisHT(servicesExterieurs, tva) {
            let sumAnnee1 = 0; let sumAnnee2 = 0; let sumAnnee3 = 0;
            for(let mois in this.totalServExtFrais.depensesMensuellesLancement) {
                this.totalServExtFrais.depensesMensuellesLancement[mois] = 0;
            }
            if(servicesExterieurs){
                servicesExterieurs.forEach( (item) =>  {
                    sumAnnee1 = sumAnnee1 + HT(item.annee1, tva);
                    sumAnnee2 = sumAnnee2 + HT(item.annee2, tva);
                    sumAnnee3 = sumAnnee3 + HT(item.annee3, tva);
                    for(let mois in this.totalServExtFrais.depensesMensuellesLancement) {

                        this.totalServExtFrais.depensesMensuellesLancement[mois] = arround(
                                this.totalServExtFrais.depensesMensuellesLancement[mois] 
                                + this.calculServiceExterieurDuMoisHT(item, Number(mois), tva),
                            this.currentCurrencyMask[':precision']);

                        this.totalServExtFrais.depensesLancement = arround(
                                this.totalServExtFrais.depensesLancement 
                                + this.calculServiceExterieurDuMoisHT(item, Number(mois), tva),
                            this.currentCurrencyMask[':precision']);
                    }
                });
            }
            this.totalServExtFrais.annee1 = arround(sumAnnee1, this.currentCurrencyMask[':precision']);
            this.totalServExtFrais.annee2 = arround(sumAnnee2, this.currentCurrencyMask[':precision']);
            this.totalServExtFrais.annee3 = arround(sumAnnee3, this.currentCurrencyMask[':precision']);
        },
        calculServiceExterieurDuMoisHT(service, moisDurant, tva) {
            if(moisDurant <= service.quantite){
                return  HT(service.prix, tva) ;
            }
            return 0;
        },
        calculSubTotalTaxeImpMairie(taxesImpotsMairie) {
            let sumAnnee1 = 0; let sumAnnee2 = 0; let sumAnnee3 = 0;
            if(taxesImpotsMairie){
                taxesImpotsMairie.forEach( (item) =>  {
                    sumAnnee1 = sumAnnee1 + item.annee1;
                    sumAnnee2 = sumAnnee2 + item.annee2;
                    sumAnnee3 = sumAnnee3 + item.annee3;
                });
            }
            this.subTotalTaxImpMairie.annee1 = arround(sumAnnee1, this.currentCurrencyMask[':precision']);
            this.subTotalTaxImpMairie.annee2 = arround(sumAnnee2, this.currentCurrencyMask[':precision']);
            this.subTotalTaxImpMairie.annee3 = arround(sumAnnee3, this.currentCurrencyMask[':precision']);
        },
        calculSubTotalTaxImpImpot(taxesImpotsImpots) {
            let sumAnnee1 = 0; let sumAnnee2 = 0; let sumAnnee3 = 0;
            if(taxesImpotsImpots){
                taxesImpotsImpots.forEach( (item) =>  {
                    sumAnnee1 = sumAnnee1 + item.annee1;
                    sumAnnee2 = sumAnnee2 + item.annee2;
                    sumAnnee3 = sumAnnee3 + item.annee3;
                });
            }
            this.subTotalTaxImpImpot.annee1 = arround(sumAnnee1, this.currentCurrencyMask[':precision']);
            this.subTotalTaxImpImpot.annee2 = arround(sumAnnee2, this.currentCurrencyMask[':precision']);
            this.subTotalTaxImpImpot.annee3 = arround(sumAnnee3, this.currentCurrencyMask[':precision']);
        },
        // calculs hors taxe pour la mairie
        calculSubTotalTaxeImpMairieHT(taxesImpotsMairie, tva) {
            let sumAnnee1 = 0; let sumAnnee2 = 0; let sumAnnee3 = 0;
            if(taxesImpotsMairie){
                taxesImpotsMairie.forEach( (item) =>  {
                    sumAnnee1 = sumAnnee1 + HT(item.annee1, tva);
                    sumAnnee2 = sumAnnee2 + HT(item.annee2, tva);
                    sumAnnee3 = sumAnnee3 + HT(item.annee3, tva);
                });
            }
            this.subTotalTaxImpMairie.annee1 = arround(sumAnnee1, this.currentCurrencyMask[':precision']);
            this.subTotalTaxImpMairie.annee2 = arround(sumAnnee2, this.currentCurrencyMask[':precision']);
            this.subTotalTaxImpMairie.annee3 = arround(sumAnnee3, this.currentCurrencyMask[':precision']);
        },
        calculSubTotalTaxImpImpotHT(taxesImpotsImpots, tva) {
            let sumAnnee1 = 0; let sumAnnee2 = 0; let sumAnnee3 = 0;
            if(taxesImpotsImpots){
                taxesImpotsImpots.forEach( (item) =>  {
                    sumAnnee1 = sumAnnee1 + HT(item.annee1, tva);
                    sumAnnee2 = sumAnnee2 + HT(item.annee2, tva);
                    sumAnnee3 = sumAnnee3 + HT(item.annee3, tva);
                });
            }
            this.subTotalTaxImpImpot.annee1 = arround(sumAnnee1, this.currentCurrencyMask[':precision']);
            this.subTotalTaxImpImpot.annee2 = arround(sumAnnee2, this.currentCurrencyMask[':precision']);
            this.subTotalTaxImpImpot.annee3 = arround(sumAnnee3, this.currentCurrencyMask[':precision']);
        },
        /* calcul le salaire total sur la premiere annee en fonction des mois ouvrables */
        salaireInitial:function(item, section, collection){
            if(item.group == 'charges-sociales') {
                item.annee1 = arround(( item.taux * item.effectif * item.ouvrable * item.salaire ) / 100, this.currentCurrencyMask[':precision']);
            }else{
                item.annee1 = arround(item.effectif * item.salaire * item.ouvrable, this.currentCurrencyMask[':precision']);
            }
            this.accroissementPrevisionnelAnnee2(item, section, collection);
        },
        calculTotalFraisPersonnelMensuel(arrayPersonnel, moisDurant){
            let sum = 0;
            arrayPersonnel.forEach( (personnel)=> {
                sum += this.calculSalaireDuMois(personnel, moisDurant);
            });
            return arround(sum, this.currentCurrencyMask[':precision']);
        },
        calculChargeSocialeMensuelle(arrayPersonnel, partCharge, moisDurant){
            return arround( this.calculTotalFraisPersonnelMensuel(arrayPersonnel, moisDurant) * partCharge / 100, this.currentCurrencyMask[':precision']) ;
        },
        calculSubTotalFraisSalarie(personnelSalaries) {
            let sumAnnee1 = 0; let sumAnnee2 = 0; let sumAnnee3 = 0; let effectif = 0;
            if(personnelSalaries){
                personnelSalaries.forEach( (item) =>  {
                    sumAnnee1 = sumAnnee1 + item.annee1;
                    sumAnnee2 = sumAnnee2 + item.annee2;
                    sumAnnee3 = sumAnnee3 + item.annee3;
                    effectif += item.effectif;
                });
            }
            this.subTotalFraisSalarie.effectif = effectif;
            this.subTotalFraisSalarie.annee1 = arround(sumAnnee1, this.currentCurrencyMask[':precision']);
            this.subTotalFraisSalarie.annee2 = arround(sumAnnee2, this.currentCurrencyMask[':precision']);
            this.subTotalFraisSalarie.annee3 = arround(sumAnnee3, this.currentCurrencyMask[':precision']);
            this.calculSubTotalFraisCharge();
        },
        calculSubTotalFraisStagiaire(personnelStagiaires) {
            let sumAnnee1 = 0; let sumAnnee2 = 0; let sumAnnee3 = 0; let effectif = 0;
            if(personnelStagiaires){
                personnelStagiaires.forEach( (item) =>  {
                    sumAnnee1 = sumAnnee1 + item.annee1;
                    sumAnnee2 = sumAnnee2 + item.annee2;
                    sumAnnee3 = sumAnnee3 + item.annee3;
                    effectif += item.effectif;
                });
            }
            this.subTotalFraisStagiaire.effectif = effectif;
            this.subTotalFraisStagiaire.annee1 = arround(sumAnnee1, this.currentCurrencyMask[':precision']);
            this.subTotalFraisStagiaire.annee2 = arround(sumAnnee2, this.currentCurrencyMask[':precision']);
            this.subTotalFraisStagiaire.annee3 = arround(sumAnnee3, this.currentCurrencyMask[':precision']);
            this.calculSubTotalFraisCharge();
        },
        calculSubTotalFraisDirigeant(personnelDirigeants) {
            let sumAnnee1 = 0; let sumAnnee2 = 0; let sumAnnee3 = 0; let effectif = 0;
            if(personnelDirigeants){
                personnelDirigeants.forEach( (item) =>  {
                    sumAnnee1 = sumAnnee1 + item.annee1;
                    sumAnnee2 = sumAnnee2 + item.annee2;
                    sumAnnee3 = sumAnnee3 + item.annee3;
                    effectif += item.effectif;
                });
            }
            this.subTotalFraisDirigeant.effectif = effectif;
            this.subTotalFraisDirigeant.annee1 = arround(sumAnnee1, this.currentCurrencyMask[':precision']);
            this.subTotalFraisDirigeant.annee2 = arround(sumAnnee2, this.currentCurrencyMask[':precision']);
            this.subTotalFraisDirigeant.annee3 = arround(sumAnnee3, this.currentCurrencyMask[':precision']);
            this.calculSubTotalFraisCharge();
        },
        calculSalaireDuMois(employer, moisDurant) {
            if(moisDurant <= employer.ouvrable){
                return  arround(employer.salaire * employer.effectif, this.currentCurrencyMask[':precision']);
            }
            return 0;
        },
        // alculs des frais hors taxe
        calculSubTotalFraisSalarieHT(personnelSalaries, tva) {
            let sumAnnee1 = 0; let sumAnnee2 = 0; let sumAnnee3 = 0; let effectif = 0;
            if(personnelSalaries){
                personnelSalaries.forEach( (item) =>  {
                    sumAnnee1 = sumAnnee1 + HT(item.annee1, tva);
                    sumAnnee2 = sumAnnee2 + HT(item.annee2, tva);
                    sumAnnee3 = sumAnnee3 + HT(item.annee3, tva);
                    effectif += item.effectif;
                });
            }
            this.subTotalFraisSalarie.effectif = effectif;
            this.subTotalFraisSalarie.annee1 = arround(sumAnnee1, this.currentCurrencyMask[':precision']);
            this.subTotalFraisSalarie.annee2 = arround(sumAnnee2, this.currentCurrencyMask[':precision']);
            this.subTotalFraisSalarie.annee3 = arround(sumAnnee3, this.currentCurrencyMask[':precision']);
            this.calculSubTotalFraisCharge();
        },
        calculSubTotalFraisStagiaireHT(personnelStagiaires, tva) {
            let sumAnnee1 = 0; let sumAnnee2 = 0; let sumAnnee3 = 0; let effectif = 0;
            if(personnelStagiaires){
                personnelStagiaires.forEach( (item) =>  {
                    sumAnnee1 = sumAnnee1 + HT(item.annee1, tva);
                    sumAnnee2 = sumAnnee2 + HT(item.annee2, tva);
                    sumAnnee3 = sumAnnee3 + HT(item.annee3, tva);
                    effectif += item.effectif;
                });
            }
            this.subTotalFraisStagiaire.effectif = effectif;
            this.subTotalFraisStagiaire.annee1 = arround(sumAnnee1, this.currentCurrencyMask[':precision']);
            this.subTotalFraisStagiaire.annee2 = arround(sumAnnee2, this.currentCurrencyMask[':precision']);
            this.subTotalFraisStagiaire.annee3 = arround(sumAnnee3, this.currentCurrencyMask[':precision']);
            this.calculSubTotalFraisCharge();
        },
        calculSubTotalFraisDirigeantHT(personnelDirigeants, tva) {
            let sumAnnee1 = 0; let sumAnnee2 = 0; let sumAnnee3 = 0; let effectif = 0;
            if(personnelDirigeants){
                personnelDirigeants.forEach( (item) =>  {
                    sumAnnee1 = sumAnnee1 + HT(item.annee1, tva);
                    sumAnnee2 = sumAnnee2 + HT(item.annee2, tva);
                    sumAnnee3 = sumAnnee3 + HT(item.annee3, tva);
                    effectif += item.effectif;
                });
            }
            this.subTotalFraisDirigeant.effectif = effectif;
            this.subTotalFraisDirigeant.annee1 = arround(sumAnnee1, this.currentCurrencyMask[':precision']);
            this.subTotalFraisDirigeant.annee2 = arround(sumAnnee2, this.currentCurrencyMask[':precision']);
            this.subTotalFraisDirigeant.annee3 = arround(sumAnnee3, this.currentCurrencyMask[':precision']);
            this.calculSubTotalFraisCharge();
        },
        calculSalaireDuMoisHT(employer, moisDurant, tva) {
            if(moisDurant <= employer.ouvrable){
                return  arround(HT(employer.salaire, tva) * employer.effectif, this.currentCurrencyMask[':precision']);
            }
            return 0;
        },
        // cumul des charges sociales
        calculSubTotalFraisCharge() {
            this.subTotalFraisCharge.annee1 = arround(this.chargeSocialeDirigeant.annee1 + this.chargeSocialeEmploye.annee1, this.currentCurrencyMask[':precision']);
            this.subTotalFraisCharge.annee2 = arround(this.chargeSocialeDirigeant.annee2 + this.chargeSocialeEmploye.annee2, this.currentCurrencyMask[':precision']);
            this.subTotalFraisCharge.annee3 = arround(this.chargeSocialeDirigeant.annee3 + this.chargeSocialeEmploye.annee3, this.currentCurrencyMask[':precision']);
            this.calculTotalFraisPersonnel();
        },
        calculTotalFraisPersonnel() {
            this.totalFraisPersonnel.effectif = this.subTotalFraisStagiaire.effectif + this.subTotalFraisSalarie.effectif + this.subTotalFraisDirigeant.effectif;
            
            this.totalFraisPersonnel.annee1 = arround(this.subTotalFraisSalarie.annee1
                + this.subTotalFraisStagiaire.annee1 + this.subTotalFraisCharge.annee1
                + this.subTotalFraisDirigeant.annee1, this.currentCurrencyMask[':precision']);

            this.totalFraisPersonnel.annee2 = arround(this.subTotalFraisSalarie.annee2
                + this.subTotalFraisStagiaire.annee2 + this.subTotalFraisCharge.annee2
                + this.subTotalFraisDirigeant.annee2, this.currentCurrencyMask[':precision']);

            this.totalFraisPersonnel.accroissement1 = arround((this.totalFraisPersonnel.annee1 == 0) ?
                0 : ((this.totalFraisPersonnel.annee2 /this.totalFraisPersonnel.annee1) )  -1, 2);

            this.totalFraisPersonnel.annee3 = arround(this.subTotalFraisSalarie.annee3
                + this.subTotalFraisStagiaire.annee3 + this.subTotalFraisCharge.annee3
                + this.subTotalFraisDirigeant.annee3, this.currentCurrencyMask[':precision']);

            this.totalFraisPersonnel.accroissement2 = arround((this.totalFraisPersonnel.annee2 == 0) ?
                0 : ((this.totalFraisPersonnel.annee3 / this.totalFraisPersonnel.annee2) ) - 1, 2);
        },
        calculEffectifPersonnel(personnels){
            let sum = 0;
            if(personnels){
                personnels.forEach(function(personnel) {
                    sum = sum + personnel.effectif;
                });
            }
            return sum;
        },
        calculEffectifTotal() {
            this.totalFraisPersonnel.effectif =
                this.subTotalFraisSalarie.effectif + this.subTotalFraisDirigeant.effectif +
                this.subTotalFraisStagiaire.effectif;
        },
        // emptyTotauxCharges() {
        //     /* totuax et sous-totaux pour achat de matiere premiere */
        //     this.totalAchatMpDepVariable = {
        //         annee1:0,
        //         annee2:0,
        //         annee3:0,
        //         depensesMensuellesLancement: {
        //             "01":{ "ttc": 0, "ht":0 },
        //             "02":{ "ttc": 0, "ht":0 },
        //             "03":{ "ttc": 0, "ht":0 },
        //             "04":{ "ttc": 0, "ht":0 },
        //             "05":{ "ttc": 0, "ht":0 },
        //             "06":{ "ttc": 0, "ht":0 },
        //             "07":{ "ttc": 0, "ht":0 },
        //             "08":{ "ttc": 0, "ht":0 },
        //             "09":{ "ttc": 0, "ht":0 },
        //             "10":{ "ttc": 0, "ht":0 },
        //             "11":{ "ttc": 0, "ht":0 },
        //             "12":{ "ttc": 0, "ht":0 }
        //         },
        //         depensesLancement: {
        //             ht: 0,
        //             ttc: 0
        //         }
        //     },
        //     this.totalAchatMpCharVariable = {
        //         annee1:0,
        //         annee2:0,
        //         annee3:0
        //     },
        //     /* Service exterieurs */
        //     this.totalServExtFrais = {
        //         annee1:0,
        //         annee2:0,
        //         annee3:0,
        //         depensesMensuellesLancement: {
        //             "01": 0,
        //             "02": 0,
        //             "03": 0,
        //             "04": 0,
        //             "05": 0,
        //             "06": 0,
        //             "07": 0,
        //             "08": 0,
        //             "09": 0,
        //             "10": 0,
        //             "11": 0,
        //             "12": 0
        //         },
        //         depensesLancement: 0
        //     },
        //     /* Taxes et impots */
        //     this.subTotalTaxImpImpot = {
        //         annee1:0,
        //         annee2:0,
        //         annee3:0
        //     },
        //     this.subTotalTaxImpMairie = {
        //         annee1:0,
        //         annee2:0,
        //         annee3:0
        //     },
        //     this.subTotalFraisSalarie = {
        //         annee1:0,
        //         annee2:0,
        //         annee3:0,
        //         effectif:0,
        //     },
        //     this.subTotalFraisDirigeant = {
        //         annee1:0,
        //         annee2:0,
        //         annee3:0,
        //         effectif:0,
        //     },
        //     this.subTotalFraisStagiaire = {
        //         annee1:0,
        //         annee2:0,
        //         annee3:0,
        //         effectif:0,
        //     },
        //     this.subTotalFraisCharge = {
        //         annee1:0,
        //         annee2:0,
        //         annee3:0
        //     },
        //     this.totalFraisPersonnel = {
        //         annee1:0,
        //         annee2:0,
        //         annee3:0,
        //         effectif:0,
        //         accroissement1:0,
        //         accroissement2:0
        //     }
        // }
    },
    computed: {
        chargeSocialeDirigeant: {
            get: function() {
                let result = {
                    titre: "Les Dirigeants",
                    taux:this.paramChargePatronaleDirigeant,
                    effectif: this.subTotalFraisDirigeant.effectif,
                    annee1: 0,
                    annee2: 0,
                    annee3: 0,
                    accroissement1: 0,
                    accroissement2: 0
                };
                result.annee1 = arround(result.taux * this.subTotalFraisDirigeant.annee1 / 100, this.currentCurrencyMask[':precision']);
                result.annee2 = arround(result.taux * this.subTotalFraisDirigeant.annee2 / 100, this.currentCurrencyMask[':precision']);
                // acroissement des charges sociales sur l'annee1
                if(result.annee1 > 0) {
                    result.accroissement1  = arround((result.annee2 / result.annee1) -1, 2);
                }else{
                    result.accroissement1 = 0
                }
                result.annee3 = arround(result.taux * this.subTotalFraisDirigeant.annee3 / 100, this.currentCurrencyMask[':precision']);
                // acroissement des charges sociales sur l'annee2
                if(result.annee2 > 0) {
                    result.accroissement2  = arround((result.annee3 /result.annee2) -1, 2);
                }else {
                    result.accroissement2 = 0
                }
                return result;

            },
            set: function (taux) {
                this.paramChargePatronaleDirigeant = isNullOrUndefined(taux) ? this.paramChargePatronaleDirigeant : taux
            }
        },
        chargeSocialeEmploye: {
            get: function () {
                let result = {
                    titre: "Les autres salaires",
                    taux:this.paramChargePatronaleEmploye,
                    effectif: this.subTotalFraisSalarie.effectif + this.subTotalFraisStagiaire.effectif,
                    annee1: 0,
                    annee2: 0,
                    annee3: 0,
                    accroissement1: 0,
                    accroissement2: 0
                };
                result.annee1 = arround(result.taux * (this.subTotalFraisSalarie.annee1 + this.subTotalFraisStagiaire.annee1) / 100, this.currentCurrencyMask[':precision']);
                result.annee2 = arround(result.taux * (this.subTotalFraisSalarie.annee2 + this.subTotalFraisStagiaire.annee2) / 100, this.currentCurrencyMask[':precision']);
                // acroissement des charges sociales sur l'annee1
                if(result.annee1 > 0) {
                    result.accroissement1  = arround((result.annee2 - result.annee1) * 100 / result.annee1, this.currentCurrencyMask[':precision']);
                }else{
                    result.accroissement1 = 0
                }
                result.annee3 = arround(result.taux * (this.subTotalFraisSalarie.annee3 + this.subTotalFraisStagiaire.annee3) / 100, this.currentCurrencyMask[':precision']);
                // acroissement des charges sociales sur l'annee2
                if(result.annee2 > 0) {
                    result.accroissement2  = arround((result.annee3 - result.annee2) * 100 / result.annee2, this.currentCurrencyMask[':precision']);
                }else {
                    result.accroissement2 = 0
                }
                return result;
            },
            set: function (taux) {
                this.paramChargePatronaleEmploye = isNullOrUndefined(taux) ? this.paramChargePatronaleEmploye : taux
            }
        },
        chargeSocialeEmployeCss: {
            get: function () {
                let result = {
                    titre: "CSS",
                    taux:this.paramChargePatronaleEmployeCss,
                    effectif: this.totalFraisPersonnel.effectif - this.subTotalFraisDirigeant.effectif,
                    annee1: 0,
                    annee2: 0,
                    annee3: 0,
                    accroissement1: 0,
                    accroissement2: 0
                };
                result.annee1 = arround(result.taux * (this.subTotalFraisSalarie.annee1 + this.subTotalFraisStagiaire.annee1) / 100, this.currentCurrencyMask[':precision']);
                result.annee2 = arround(result.taux * (this.subTotalFraisSalarie.annee2 + this.subTotalFraisStagiaire.annee2) / 100, this.currentCurrencyMask[':precision']);
                // acroissement des charges sociales sur l'annee1
                if(result.annee1 > 0) {
                    result.accroissement1  = arround((result.annee2 - result.annee1) * 100 / result.annee1, this.currentCurrencyMask[':precision']);
                }else{
                    result.accroissement1 = 0
                }
                result.annee3 = arround(result.taux * (this.subTotalFraisSalarie.annee3 + this.subTotalFraisStagiaire.annee3) / 100, this.currentCurrencyMask[':precision']);
                // acroissement des charges sociales sur l'annee2
                if(result.annee2 > 0) {
                    result.accroissement2  = arround((result.annee3 - result.annee2) * 100 / result.annee2, this.currentCurrencyMask[':precision']);
                }else {
                    result.accroissement2 = 0
                }
                return result;
            },
            set: function (taux) {
                this.paramChargePatronaleEmployeCss = isNullOrUndefined(taux) ? this.paramChargePatronaleEmployeCss : taux
            }
        },
        
        totalTaxeImpot() {
            let result = {annee1:0, annee2:0, annee3:0 };
            result.annee1 = arround(this.subTotalTaxImpMairie.annee1 + this.subTotalTaxImpImpot.annee1, this.currentCurrencyMask[':precision']);
            result.annee2 = arround(this.subTotalTaxImpMairie.annee2 + this.subTotalTaxImpImpot.annee2, this.currentCurrencyMask[':precision']);
            result.annee3 = arround(this.subTotalTaxImpMairie.annee3 + this.subTotalTaxImpImpot.annee3, this.currentCurrencyMask[':precision']);
            return result;
        },
        totalAchat() {
            let result = {
                annee1:0,
                annee2:0,
                annee3:0
            };
            result.annee1 = arround(this.totalAchatMpDepVariable.annee1 + this.totalAchatMpCharVariable.annee1, this.currentCurrencyMask[':precision']);
            result.annee2 = arround(this.totalAchatMpDepVariable.annee2 + this.totalAchatMpCharVariable.annee2, this.currentCurrencyMask[':precision']);
            result.annee3 = arround(this.totalAchatMpDepVariable.annee3 + this.totalAchatMpCharVariable.annee3, this.currentCurrencyMask[':precision']);
            return result;
        }
    }
}
