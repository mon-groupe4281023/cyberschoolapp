// assets/js/views/bp/mixins/financier/produits
import {arround, isEmpty, HT} from '../../../../services/helper'
import currenciesMaskMixin from '../currenciesMask'
import {has, hasIn} from 'lodash/core'

export const subTotalProduitsFinanciers = {
    "annee1": 0,
    "annee2": 0,
    "annee3": 0
};

export default {
    mixins: [currenciesMaskMixin],
    data() {
        return {
            totalVolumeVenteAnnuel:{
                "jours":0,
                "dimanches":0,
                "samedis":0,
                "feries":0,
                "ouvrables":0
            },
            // TODO:  calculer les jours des mois en fonction des annees bisextiles ou bien les renseigner en parametre
            totalChiffreAffaireHT: {
                "annee1":{
                    "accroissement":0,
                    "montant":0,
                    "volumeVente": {
                        "01": 0,
                        "02": 0,
                        "03": 0,
                        "04": 0,
                        "05": 0,
                        "06": 0,
                        "07": 0,
                        "08": 0,
                        "09": 0,
                        "10": 0,
                        "11": 0,
                        "12": 0
                    }
                },
                "annee2":{
                    "accroissement":0,
                    "montant":0,
                    "volumeVente": {
                        "01": 0,
                        "02": 0,
                        "03": 0,
                        "04": 0,
                        "05": 0,
                        "06": 0,
                        "07": 0,
                        "08": 0,
                        "09": 0,
                        "10": 0,
                        "11": 0,
                        "12": 0
                    }
                },
                "annee3":{
                    "montant":0,
                    "volumeVente": {
                        "01": 0,
                        "02": 0,
                        "03": 0,
                        "04": 0,
                        "05": 0,
                        "06": 0,
                        "07": 0,
                        "08": 0,
                        "09": 0,
                        "10": 0,
                        "11": 0,
                        "12": 0
                    }
                }
            },
            totalTva: {
                "annee1":{
                    "accroissement":0,
                    "montant":0,
                    "volumeVente": {
                        "01": 0,
                        "02": 0,
                        "03": 0,
                        "04": 0,
                        "05": 0,
                        "06": 0,
                        "07": 0,
                        "08": 0,
                        "09": 0,
                        "10": 0,
                        "11": 0,
                        "12": 0
                    }
                },
                "annee2":{
                    "accroissement":0,
                    "montant":0,
                    "volumeVente": {
                        "01": 0,
                        "02": 0,
                        "03": 0,
                        "04": 0,
                        "05": 0,
                        "06": 0,
                        "07": 0,
                        "08": 0,
                        "09": 0,
                        "10": 0,
                        "11": 0,
                        "12": 0
                    }
                },
                "annee3":{
                    "montant":0,
                    "volumeVente": {
                        "01": 0,
                        "02": 0,
                        "03": 0,
                        "04": 0,
                        "05": 0,
                        "06": 0,
                        "07": 0,
                        "08": 0,
                        "09": 0,
                        "10": 0,
                        "11": 0,
                        "12": 0
                    }
                }
            },
            totalChiffreAffaire: {
                "annee1":{
                    "accroissement":2,
                    "montant":0,
                    "quantiteVendue":0 ,
                    "volumeVente": {
                        "01": 0,
                        "02": 0,
                        "03": 0,
                        "04": 0,
                        "05": 0,
                        "06": 0,
                        "07": 0,
                        "08": 0,
                        "09": 0,
                        "10": 0,
                        "11": 0,
                        "12": 0
                    }
                },
                "annee2":{
                    "accroissement":2,
                    "montant":0,
                    "quantiteVendue":0,
                    "volumeVente": {
                        "01": 0,
                        "02": 0,
                        "03": 0,
                        "04": 0,
                        "05": 0,
                        "06": 0,
                        "07": 0,
                        "08": 0,
                        "09": 0,
                        "10": 0,
                        "11": 0,
                        "12": 0
                    }
                },
                "annee3":{
                    "montant":0,
                    "quantiteVendue":0,
                    "volumeVente": {
                        "01": 0,
                        "02": 0,
                        "03": 0,
                        "04": 0,
                        "05": 0,
                        "06": 0,
                        "07": 0,
                        "08": 0,
                        "09": 0,
                        "10": 0,
                        "11": 0,
                        "12": 0
                    }
                }
            },
            ventesMensuellesGroupees: {},
            subTotalProduitsFinanciers: subTotalProduitsFinanciers,
            subTotalVentesExceptionnelles: {
                "annee1": 0,
                "annee2": 0,
                "annee3": 0
            },
            subTotalAchatsExceptionnels: {
                "annee1": 0,
                "annee2": 0,
                "annee3": 0
            }
        }
    },
    methods: {
        calculTvaProduit: function(produit){
            return  this.calculPrixProduit(produit, 'ttc') - this.calculPrixProduit(produit, 'ht');
        },
        calculPrixProduit: function(produit, mode){
            if(mode) { 
                if(mode.toUpperCase() === 'TTC') {
                    return produit.prixIncludeTaxe ? produit.prixVenteUnitaire : arround(produit.prixVenteUnitaire * ( 1 + (produit.tva.value / 100)), this.currentCurrencyMask[':precision']);
                }
                else if(mode.toUpperCase() === 'HT'){
                    return produit.prixIncludeTaxe ?  arround( (produit.prixVenteUnitaire * (100 - produit.tva.value) / 100), this.currentCurrencyMask[':precision']) : produit.prixVenteUnitaire;
                    // return produit.prixIncludeTaxe ?  arround( (produit.prixVenteUnitaire * 100 / (100 + produit.tva.value) ), this.currentCurrencyMask[':precision']) : produit.prixVenteUnitaire;
                }
            }else return 0;
        },
        //DONE: update
        convertProductToCharge(product){
            let result = {};
            let prixAchat = this.calculPrixProduit(product,'ttc') * product?.chargeVariable / 100 ?? 0;
            if(product?.chargeVariable > 0){
                //TODO: changer la formule annee1.montant => annee1.ttc
                result["titre"] = product?.nom ?? '';
                result["quantite"] = product?.chiffreAffaire?.annee1?.quantiteVendue ?? 0;
                result["prix"] = prixAchat;
                result["annee1"] = arround(product?.chiffreAffaire?.annee1?.quantiteVendue * prixAchat, this.currentCurrencyMask[':precision']) ?? 0;
                result["commentaire"] = "";
                result["accroissement1"] = product?.chiffreAffaire?.annee1?.accroissement ?? 0;
                result["annee2"] = arround(product?.chiffreAffaire?.annee2?.ttc * product?.chargeVariable / 100, this.currentCurrencyMask[':precision']) ?? 0;
                result["accroissement2"] = product?.chiffreAffaire?.annee2?.accroissement ?? 0;
                result["annee3"] = arround(product?.chiffreAffaire?.annee3?.ttc * product?.chargeVariable / 100, this.currentCurrencyMask[':precision']) ?? 0;
                result["group"] = "depenses-produits";
                result["ventes"] = {"01":{}, "02":{}, "03":{}, "04":{}, "05":{}, "06":{}, "07":{}, "08":{}, "09":{}, "10":{}, "11":{}, "12":{}};
                
                for(const moisOuvrable in product.ventes) {
                    result['ventes'][moisOuvrable]['venteMensuelle'] = product?.ventes[moisOuvrable]?.venteMensuelle ?? 0;
                    result['ventes'][moisOuvrable]['ttc'] = arround(product?.ventes[moisOuvrable]?.venteMensuelle * prixAchat, this.currentCurrencyMask[':precision']) ?? 0;
                    result['ventes'][moisOuvrable]['ht'] = arround(product?.ventes[moisOuvrable]?.venteMensuelle * this.calculPrixProduit(product,'ht') * product?.chargeVariable / 100, this.currentCurrencyMask[':precision']) ?? 0;
                }
                return result;
            }                    
        },
        calculCumulVenteProduitAnnuel(item, collectionProduit = null){
            item.chiffreAffaire.annee1.quantiteVendue = arround(item.ventes['01'].venteMensuelle + item.ventes['02'].venteMensuelle +
                item.ventes['03'].venteMensuelle + item.ventes['04'].venteMensuelle +
                item.ventes['05'].venteMensuelle + item.ventes['06'].venteMensuelle +
                item.ventes['07'].venteMensuelle + item.ventes['08'].venteMensuelle +
                item.ventes['09'].venteMensuelle + item.ventes['10'].venteMensuelle +
                item.ventes['11'].venteMensuelle + item.ventes['12'].venteMensuelle, this.currentCurrencyMask[':precision']);
            // calcul du chiffre d'affaires ttc annuel en annee 1
            item.chiffreAffaire.annee1.ttc = arround(item.chiffreAffaire.annee1.quantiteVendue
                *  this.calculPrixProduit(item, 'ttc'), this.currentCurrencyMask[':precision']);
            // calcul du chiffre d'affaires ht annuel en annee 1
            item.chiffreAffaire.annee1.ht = arround(item.chiffreAffaire.annee1.quantiteVendue
                *  this.calculPrixProduit(item, 'ht'), this.currentCurrencyMask[':precision']);
            // calcul de tva sur chiffre d'affaires annuel en annee 1
            item.chiffreAffaire.annee1.tva = arround(item.chiffreAffaire.annee1.quantiteVendue
                *  this.calculTvaProduit(item), this.currentCurrencyMask[':precision']);
                // augmentation du chiffre d'affaires de l'anne 1 (du au quantites)
            this.calculTotalChiffreAffaireAnnee1(collectionProduit);
            // calcul du chiffre d'affaires annuel en annee 2 et annee3 par ricochet
            this.updateMontantChiffreAffaireAnnee2(item, collectionProduit);
        },
        // this function is unused and uncomplete
        searchReferentProductInProductService(articleCollection,productName){
            if(articleCollection){
                articleCollection.forEach( (produitReference) => {
                    if(produitReference.nom == productName){
                        return produitReference;
                    }
                });
            }
              return null;
        },
        // operations in chiffre affaire
        calculTotalChiffreAffaireAnnee1(collectionProduit){
            let sum= 0, ht = 0, tva = 0,
            chiffreMensuelProduit = {
                "01": {"ttc":0, "ht":0,"tva":0},
                "02": {"ttc":0, "ht":0,"tva":0},
                "03": {"ttc":0, "ht":0,"tva":0},
                "04": {"ttc":0, "ht":0,"tva":0},
                "05": {"ttc":0, "ht":0,"tva":0},
                "06": {"ttc":0, "ht":0,"tva":0},
                "07": {"ttc":0, "ht":0,"tva":0},
                "08": {"ttc":0, "ht":0,"tva":0},
                "09": {"ttc":0, "ht":0,"tva":0},
                "10": {"ttc":0, "ht":0,"tva":0},
                "11": {"ttc":0, "ht":0,"tva":0},
                "12": {"ttc":0, "ht":0,"tva":0}
            };
            if(collectionProduit){
                collectionProduit.forEach( (produit) => {
                    sum = arround(sum + produit.chiffreAffaire.annee1.ttc, this.currentCurrencyMask[':precision']);
                    tva = arround(tva + produit.chiffreAffaire.annee1.tva, this.currentCurrencyMask[':precision']);
                    ht = arround(ht + produit.chiffreAffaire.annee1.ht, this.currentCurrencyMask[':precision']);
                    for(let property in produit.ventes){
                        chiffreMensuelProduit[property].ttc += produit.ventes[property].ttc;
                        chiffreMensuelProduit[property].tva += produit.ventes[property].tva;
                        chiffreMensuelProduit[property].ht += produit.ventes[property].ht;
                    }
                });
            }
            this.totalChiffreAffaire.annee1.montant = sum;
            for(let key in this.totalChiffreAffaire.annee1.volumeVente) {
                this.totalChiffreAffaire.annee1.volumeVente[key] = arround(chiffreMensuelProduit[key].ttc, this.currentCurrencyMask[':precision']);
            }
            this.totalChiffreAffaireHT.annee1.montant = ht;
            for(let key in this.totalChiffreAffaireHT.annee1.volumeVente) {
                this.totalChiffreAffaireHT.annee1.volumeVente[key] = arround(chiffreMensuelProduit[key].ht, this.currentCurrencyMask[':precision']);
            }
            this.totalTva.annee1.montant = tva;
            for(let key in this.totalTva.annee1.volumeVente) {
                this.totalTva.annee1.volumeVente[key] = arround(chiffreMensuelProduit[key].tva, this.currentCurrencyMask[':precision']);
            }
        },
        calculTotalChiffreAffaireAnnee2(collectionProduit = null){
            let sum= 0, ht = 0, tva = 0,
            chiffreMensuelProduit = {
                "01": {"ttc":0, "ht":0,"tva":0},
                "02": {"ttc":0, "ht":0,"tva":0},
                "03": {"ttc":0, "ht":0,"tva":0},
                "04": {"ttc":0, "ht":0,"tva":0},
                "05": {"ttc":0, "ht":0,"tva":0},
                "06": {"ttc":0, "ht":0,"tva":0},
                "07": {"ttc":0, "ht":0,"tva":0},
                "08": {"ttc":0, "ht":0,"tva":0},
                "09": {"ttc":0, "ht":0,"tva":0},
                "10": {"ttc":0, "ht":0,"tva":0},
                "11": {"ttc":0, "ht":0,"tva":0},
                "12": {"ttc":0, "ht":0,"tva":0}
            };
            if(collectionProduit){
                collectionProduit.forEach( (produit) => {
                    sum = arround(sum + produit.chiffreAffaire.annee2.ttc, this.currentCurrencyMask[':precision']);
                    tva = arround(tva + produit.chiffreAffaire.annee2.tva, this.currentCurrencyMask[':precision']);
                    ht = arround(ht + produit.chiffreAffaire.annee2.ht, this.currentCurrencyMask[':precision']);
                    for(let property in produit.ventes){
                        chiffreMensuelProduit[property].ttc += produit.ventes[property].ttc * (100+produit.chiffreAffaire.annee1.accroissement) / 100;
                        chiffreMensuelProduit[property].tva += produit.ventes[property].tva * (100+produit.chiffreAffaire.annee1.accroissement) / 100;
                        chiffreMensuelProduit[property].ht += produit.ventes[property].ht *(100+ produit.chiffreAffaire.annee1.accroissement) / 100;
                    }
                });
            }
            
            this.totalChiffreAffaire.annee2.montant = sum;
            this.totalTva.annee2.montant = tva;
            this.totalChiffreAffaireHT.annee2.montant = ht;
            // accroissement chiffre affaire ttc
            if(this.totalChiffreAffaire.annee1.montant > 0 && sum > 0){
                this.totalChiffreAffaire.annee1.accroissement = arround((sum - this.totalChiffreAffaire.annee1.montant)
                * 100 / this.totalChiffreAffaire.annee1.montant, 2);
            }else{
                this.totalChiffreAffaire.annee1.accroissement = 0;
            }
            for(let key in this.totalChiffreAffaire.annee2.volumeVente) {
                this.totalChiffreAffaire.annee2.volumeVente[key] = arround(chiffreMensuelProduit[key].ttc, this.currentCurrencyMask[':precision']);
            }

            // accroissement chiffre affaire ht
            if(this.totalChiffreAffaireHT.annee1.montant > 0 && ht > 0){
                this.totalChiffreAffaireHT.annee1.accroissement = arround((ht - this.totalChiffreAffaireHT.annee1.montant)
                * 100 / this.totalChiffreAffaireHT.annee1.montant, 2);
            }else{
                this.totalChiffreAffaireHT.annee1.accroissement = 0;
            }
            for(let key in this.totalChiffreAffaireHT.annee2.volumeVente) {
                this.totalChiffreAffaireHT.annee2.volumeVente[key] = arround(chiffreMensuelProduit[key].ht, this.currentCurrencyMask[':precision']);
            }

            // accroissement tva
            if(this.totalTva.annee1.montant > 0 && tva > 0){
                this.totalTva.annee1.accroissement = arround((tva - this.totalTva.annee1.montant)
                * 100 / this.totalTva.annee1.montant, 2);
            }else{
                this.totalTva.annee1.accroissement = 0;
            }
            for(let key in this.totalTva.annee2.volumeVente) {
                this.totalTva.annee2.volumeVente[key] = arround(chiffreMensuelProduit[key].tva, this.currentCurrencyMask[':precision']);
            }

        },
        calculTotalChiffreAffaireAnnee3(collectionProduit = null){
            let sum= 0, ht = 0, tva = 0,
            chiffreMensuelProduit = {
                "01": {"ttc":0, "ht":0,"tva":0},
                "02": {"ttc":0, "ht":0,"tva":0},
                "03": {"ttc":0, "ht":0,"tva":0},
                "04": {"ttc":0, "ht":0,"tva":0},
                "05": {"ttc":0, "ht":0,"tva":0},
                "06": {"ttc":0, "ht":0,"tva":0},
                "07": {"ttc":0, "ht":0,"tva":0},
                "08": {"ttc":0, "ht":0,"tva":0},
                "09": {"ttc":0, "ht":0,"tva":0},
                "10": {"ttc":0, "ht":0,"tva":0},
                "11": {"ttc":0, "ht":0,"tva":0},
                "12": {"ttc":0, "ht":0,"tva":0}
            };

            if(collectionProduit){
                collectionProduit.forEach( (produit) => {
                    sum = arround(sum + produit.chiffreAffaire.annee3.ttc, this.currentCurrencyMask[':precision']);
                    tva = arround(tva + produit.chiffreAffaire.annee3.tva, this.currentCurrencyMask[':precision']);
                    ht = arround(ht + produit.chiffreAffaire.annee3.ht, this.currentCurrencyMask[':precision']);
                    for(let property in produit.ventes){
                        chiffreMensuelProduit[property].ttc += ((100+produit.chiffreAffaire.annee2.accroissement)/100) * (produit.ventes[property].ttc * (100+produit.chiffreAffaire.annee1.accroissement) / 100);
                        chiffreMensuelProduit[property].tva += ((100+produit.chiffreAffaire.annee2.accroissement)/100) * (produit.ventes[property].tva * (100+produit.chiffreAffaire.annee1.accroissement) / 100);
                        chiffreMensuelProduit[property].ht += ((100+ produit.chiffreAffaire.annee2.accroissement)/100) * (produit.ventes[property].ht *(100+ produit.chiffreAffaire.annee1.accroissement) / 100);
                    }
                })
            }
            this.totalChiffreAffaire.annee3.montant = sum;
            this.totalTva.annee3.montant = tva;
            this.totalChiffreAffaireHT.annee3.montant = ht;
            // accroissement chiffre affaire ttc
            if(this.totalChiffreAffaire.annee2.montant > 0 && sum > 0){
                this.totalChiffreAffaire.annee2.accroissement = arround((sum - this.totalChiffreAffaire.annee2.montant)
                * 100 / this.totalChiffreAffaire.annee2.montant, 2);
            }else{
                this.totalChiffreAffaire.annee2.accroissement = 0;
            }
            for(let key in this.totalChiffreAffaire.annee3.volumeVente) {
                this.totalChiffreAffaire.annee3.volumeVente[key] = arround(chiffreMensuelProduit[key].ttc, this.currentCurrencyMask[':precision']);
            }

            // accroissement chiffre affaire ht
            if(this.totalChiffreAffaireHT.annee2.montant > 0 && ht > 0){
                this.totalChiffreAffaireHT.annee2.accroissement = arround((ht - this.totalChiffreAffaireHT.annee2.montant)
                * 100 / this.totalChiffreAffaireHT.annee2.montant, 2);
            }else{
                this.totalChiffreAffaireHT.annee2.accroissement = 0;
            }
            for(let key in this.totalChiffreAffaireHT.annee3.volumeVente) {
                this.totalChiffreAffaireHT.annee3.volumeVente[key] = arround(chiffreMensuelProduit[key].ht, this.currentCurrencyMask[':precision']);
            }

            // accroissement tva
            if(this.totalTva.annee2.montant > 0 && tva > 0){
                this.totalTva.annee2.accroissement = arround((tva - this.totalTva.annee2.montant)
                * 100 / this.totalTva.annee2.montant, 2);
            }else{
                this.totalTva.annee2.accroissement = 0;
            }
            for(let key in this.totalTva.annee3.volumeVente) {
                this.totalTva.annee3.volumeVente[key] = arround(chiffreMensuelProduit[key].tva, this.currentCurrencyMask[':precision']);
            }
        },
        updateMontantChiffreAffaireAnnee2(produit, collectionProduit=null){
            // le ttc
            produit.chiffreAffaire.annee2.ttc = arround(produit.chiffreAffaire.annee1.ttc +
                    (produit.chiffreAffaire.annee1.ttc * produit.chiffreAffaire.annee1.accroissement / 100)
                , this.currentCurrencyMask[':precision']);
            // le tva
            produit.chiffreAffaire.annee2.tva = arround(produit.chiffreAffaire.annee1.tva +
                    (produit.chiffreAffaire.annee1.tva * produit.chiffreAffaire.annee1.accroissement / 100)
                , this.currentCurrencyMask[':precision']);
            // la ht
            produit.chiffreAffaire.annee2.ht = arround(produit.chiffreAffaire.annee1.ht +
                    (produit.chiffreAffaire.annee1.ht * produit.chiffreAffaire.annee1.accroissement / 100)
                , this.currentCurrencyMask[':precision']);
            
            this.updateMontantChiffreAffaireAnnee3(produit, collectionProduit);
            this.calculTotalChiffreAffaireAnnee2(collectionProduit);
        },
        updateMontantChiffreAffaireAnnee3(produit, collectionProduit = null){
            // the ttc 
            produit.chiffreAffaire.annee3.ttc = arround(produit.chiffreAffaire.annee2.ttc +
                    (produit.chiffreAffaire.annee2.ttc * produit.chiffreAffaire.annee2.accroissement / 100)
                , this.currentCurrencyMask[':precision']);
            // the ht
            produit.chiffreAffaire.annee3.ht = arround(produit.chiffreAffaire.annee2.ht +
                    (produit.chiffreAffaire.annee2.ht * produit.chiffreAffaire.annee2.accroissement / 100)
                , this.currentCurrencyMask[':precision']);
            // tva
            produit.chiffreAffaire.annee3.tva = arround(produit.chiffreAffaire.annee2.tva +
                    (produit.chiffreAffaire.annee2.tva * produit.chiffreAffaire.annee2.accroissement / 100)
                , this.currentCurrencyMask[':precision']);

            this.calculTotalChiffreAffaireAnnee3(collectionProduit);
        },
        // calculer le chiffre d'affaires d'un mois
        calculChiffreAffaireMensuelArticle(produit, monthOrder, taxe){
            let sum = 0;
            if(!isEmpty(monthOrder)){
                if(has(produit,'ventes')){
                    if(!isEmpty(produit.ventes)){
                        if(taxe.toUpperCase() === 'TTC' || taxe.toUpperCase() === 'HT'){
                            sum = this.calculPrixProduit(produit, taxe) * produit.ventes[monthOrder].venteMensuelle;
                        }
                    }
                }
            }
            return arround(sum, this.currentCurrencyMask[':precision']);
        },
        // calculer le chiffre d'affaires d'un mois
        calculSubTotalCAMensuel(collectionProduit, monthOrder, taxe){
            let sum = 0;
            if(!isEmpty(monthOrder)){
                collectionProduit.forEach( produit => {
                    if(has(produit,'ventes')){
                        if(!isEmpty(produit.ventes)){
                            if(taxe.toUpperCase() === 'TTC' || taxe.toUpperCase() === 'HT'){
                                sum = this.calculPrixProduit(produit, taxe) * produit.ventes[monthOrder].venteMensuelle;
                            }
                        }
                    }
                })
            }
            return arround(sum, this.currentCurrencyMask[':precision']);
        },
        // calculer la tva generee d'un mois
        calculTvaMensuel(produit, monthOrder){
            let sum = 0;
            if(!isEmpty(monthOrder)){
                if(has(produit,'ventes')){
                    if(!isEmpty(produit.ventes)){
                        sum = this.calculTvaProduit(produit) * produit.ventes[monthOrder].venteMensuelle;
                    }
                }
            }
            
            return arround(sum, this.currentCurrencyMask[':precision']);
        },
        calculSubTotalProduitsFinanciers: function(produitsFinanciers) {
            let result = {
                "annee1": 0,
                "annee2": 0,
                "annee3": 0,
            }
            if(! isEmpty(produitsFinanciers)){
                produitsFinanciers.forEach( item => {
                    result.annee1 += item.annee1;
                    result.annee2 += item.annee2;
                    result.annee3 += item.annee3;
                })
            }
            this.subTotalProduitsFinanciers = result;
        },
        calculSubTotalVentesExceptionnelles: function(ventesExceptionnelles) {
            let result = {
                "annee1": 0,
                "annee2": 0,
                "annee3": 0,
            }
            if(! isEmpty(ventesExceptionnelles)){
                ventesExceptionnelles.forEach( vente => {
                    result.annee1 = arround(result.annee1 + vente.annee1, this.currentCurrencyMask[':precision']);
                    result.annee2 = arround(result.annee2 + vente.annee2, this.currentCurrencyMask[':precision']);
                    result.annee3 = arround(result.annee3 + vente.annee3, this.currentCurrencyMask[':precision']);
                })
            }
            this.subTotalVentesExceptionnelles = result;
        },
        calculSubTotalAchatsExceptionnels: function(achatsExceptionnels) {
            let result = {
                "annee1": 0,
                "annee2": 0,
                "annee3": 0
            }
            if(! isEmpty(achatsExceptionnels)){
                achatsExceptionnels.forEach( achat => {
                    result.annee1 = arround(result.annee1 + achat.annee1, this.currentCurrencyMask[':precision']);
                    result.annee2 = arround(result.annee2 + achat.annee2, this.currentCurrencyMask[':precision']);
                    result.annee3 = arround(result.annee3 + achat.annee3, this.currentCurrencyMask[':precision']);
                })
            }
            this.subTotalAchatsExceptionnels = result;
        },
        calculSubTotalVentesExceptionnellesHT: function(ventesExceptionnelles, tva) {
            let result = {
                "annee1": 0,
                "annee2": 0,
                "annee3": 0,
            }
            if(! isEmpty(ventesExceptionnelles)){
                ventesExceptionnelles.forEach( vente => {
                    result.annee1 = arround(result.annee1 + HT(vente.annee1, tva), this.currentCurrencyMask[':precision']);
                    result.annee2 = arround(result.annee2 + HT(vente.annee2, tva), this.currentCurrencyMask[':precision']);
                    result.annee3 = arround(result.annee3 + HT(vente.annee3, tva), this.currentCurrencyMask[':precision']);
                })
            }
            this.subTotalVentesExceptionnelles = result;
        },
        calculSubTotalAchatsExceptionnelsHT: function(achatsExceptionnels, tva) {
            let result = {
                "annee1": 0,
                "annee2": 0,
                "annee3": 0
            }
            if(! isEmpty(achatsExceptionnels)){
                achatsExceptionnels.forEach( achat => {
                    result.annee1 = arround(result.annee1 + HT(achat.annee1, tva), this.currentCurrencyMask[':precision']);
                    result.annee2 = arround(result.annee2 + HT(achat.annee2, tva), this.currentCurrencyMask[':precision']);
                    result.annee3 = arround(result.annee3 + HT(achat.annee3, tva), this.currentCurrencyMask[':precision']);
                })
            }
            this.subTotalAchatsExceptionnels = result;
        },
        /* calcul le montant annuel d'un achat/vente exceptionnel(lle) */
        resultatInitial:function(item, section, collection){
            item.annee1 = arround(item.quantite * item.prix, this.currentCurrencyMask[':precision']);
            this.resultatPrevisionnelAnnee2(item, section, collection);
            //this.capitalPrevisionnelAccruAnnee2(item);
        },
        /* Mise a jours des depenses previsionnelles pour l'anne2*/
        resultatPrevisionnelAnnee2: function(item, section, collection){
            item.annee2 = arround(( 1 + (item.accroissement1 / 100) ) * item.annee1, this.currentCurrencyMask[':precision']);
            this.resultatPrevisionnelAnnee3(item, section, collection);
            //this.capitalPrevisionnelAccruAnnee3(item);
        },
        resultatPrevisionnelAnnee3:function(item, section, collection){
            item.annee3 = arround(( 1 + (item.accroissement2 / 100) )* item.annee2, this.currentCurrencyMask[':precision']);
        },
        // emptyTotauxProduits() {
        //     this.totalVolumeVenteAnnuel = {
        //         "jours":0,
        //         "dimanches":0,
        //         "samedis":0,
        //         "feries":0,
        //         "ouvrables":0
        //     },
        //     // TODO:  calculer les jours des mois en fonction des annees bisextiles ou bien les renseigner en parametre
        //     this.totalChiffreAffaireHT =  {
        //         "annee1":{
        //             "accroissement":0,
        //             "montant":0,
        //             "volumeVente": {
        //                 "01": 0,
        //                 "02": 0,
        //                 "03": 0,
        //                 "04": 0,
        //                 "05": 0,
        //                 "06": 0,
        //                 "07": 0,
        //                 "08": 0,
        //                 "09": 0,
        //                 "10": 0,
        //                 "11": 0,
        //                 "12": 0
        //             }
        //         },
        //         "annee2":{
        //             "accroissement":0,
        //             "montant":0,
        //             "volumeVente": {
        //                 "01": 0,
        //                 "02": 0,
        //                 "03": 0,
        //                 "04": 0,
        //                 "05": 0,
        //                 "06": 0,
        //                 "07": 0,
        //                 "08": 0,
        //                 "09": 0,
        //                 "10": 0,
        //                 "11": 0,
        //                 "12": 0
        //             }
        //         },
        //         "annee3":{
        //             "montant":0,
        //             "volumeVente": {
        //                 "01": 0,
        //                 "02": 0,
        //                 "03": 0,
        //                 "04": 0,
        //                 "05": 0,
        //                 "06": 0,
        //                 "07": 0,
        //                 "08": 0,
        //                 "09": 0,
        //                 "10": 0,
        //                 "11": 0,
        //                 "12": 0
        //             }
        //         }
        //     },
        //     this.totalTva =  {
        //         "annee1":{
        //             "accroissement":0,
        //             "montant":0,
        //             "volumeVente": {
        //                 "01": 0,
        //                 "02": 0,
        //                 "03": 0,
        //                 "04": 0,
        //                 "05": 0,
        //                 "06": 0,
        //                 "07": 0,
        //                 "08": 0,
        //                 "09": 0,
        //                 "10": 0,
        //                 "11": 0,
        //                 "12": 0
        //             }
        //         },
        //         "annee2":{
        //             "accroissement":0,
        //             "montant":0,
        //             "volumeVente": {
        //                 "01": 0,
        //                 "02": 0,
        //                 "03": 0,
        //                 "04": 0,
        //                 "05": 0,
        //                 "06": 0,
        //                 "07": 0,
        //                 "08": 0,
        //                 "09": 0,
        //                 "10": 0,
        //                 "11": 0,
        //                 "12": 0
        //             }
        //         },
        //         "annee3":{
        //             "montant":0,
        //             "volumeVente": {
        //                 "01": 0,
        //                 "02": 0,
        //                 "03": 0,
        //                 "04": 0,
        //                 "05": 0,
        //                 "06": 0,
        //                 "07": 0,
        //                 "08": 0,
        //                 "09": 0,
        //                 "10": 0,
        //                 "11": 0,
        //                 "12": 0
        //             }
        //         }
        //     },
        //     this.totalChiffreAffaire =  {
        //         "annee1":{
        //             "accroissement":2,
        //             "montant":0,
        //             "quantiteVendue":0 ,
        //             "volumeVente": {
        //                 "01": 0,
        //                 "02": 0,
        //                 "03": 0,
        //                 "04": 0,
        //                 "05": 0,
        //                 "06": 0,
        //                 "07": 0,
        //                 "08": 0,
        //                 "09": 0,
        //                 "10": 0,
        //                 "11": 0,
        //                 "12": 0
        //             }
        //         },
        //         "annee2":{
        //             "accroissement":2,
        //             "montant":0,
        //             "quantiteVendue":0,
        //             "volumeVente": {
        //                 "01": 0,
        //                 "02": 0,
        //                 "03": 0,
        //                 "04": 0,
        //                 "05": 0,
        //                 "06": 0,
        //                 "07": 0,
        //                 "08": 0,
        //                 "09": 0,
        //                 "10": 0,
        //                 "11": 0,
        //                 "12": 0
        //             }
        //         },
        //         "annee3":{
        //             "montant":0,
        //             "quantiteVendue":0,
        //             "volumeVente": {
        //                 "01": 0,
        //                 "02": 0,
        //                 "03": 0,
        //                 "04": 0,
        //                 "05": 0,
        //                 "06": 0,
        //                 "07": 0,
        //                 "08": 0,
        //                 "09": 0,
        //                 "10": 0,
        //                 "11": 0,
        //                 "12": 0
        //             }
        //         }
        //     },
        //     this.ventesMensuellesGroupees =  {},
        //     this.subTotalVentesExceptionnelles =  {
        //         "annee1": 0,
        //         "annee2": 0,
        //         "annee3": 0
        //     },
        //     this.subTotalAchatsExceptionnels =  {
        //         "annee1": 0,
        //         "annee2": 0,
        //         "annee3": 0
        //     }
        // }
    },
    computed: {
        totalSoldeExceptionnel: {
            get: function() {
                let annee1 = arround(this.subTotalVentesExceptionnelles.annee1 - this.subTotalAchatsExceptionnels.annee1, this.currentCurrencyMask[':precision']);
                let annee2 = arround(this.subTotalVentesExceptionnelles.annee2 - this.subTotalAchatsExceptionnels.annee2, this.currentCurrencyMask[':precision']);
                let annee3 = arround(this.subTotalVentesExceptionnelles.annee3 - this.subTotalAchatsExceptionnels.annee3, this.currentCurrencyMask[':precision']);                

                let result = { "annee1": {"montant":annee1}, "annee2": {"montant":annee2}, "annee3": {"montant":annee3} };
                
                result.annee1['accroissement'] = (result.annee1.montant == 0) ? 0 : arround((result.annee2.montant - result.annee1.montant) *100 / result.annee1.montant, this.currentCurrencyMask[':precision']);
                result.annee2['accroissement'] = (result.annee2.montant == 0) ? 0 : arround((result.annee3.montant - result.annee2.montant) *100 / result.annee2.montant, this.currentCurrencyMask[':precision']);

                return result;
            }
        }
    }
}
