class Tresor {

    /**
     * Create a new Tresor instance.
     *
     * @param {object} data
     */
    constructor(data, titre){

        this.originalData = {"initial":0,"mois1":0,"mois2":0,"mois3":0,"mois4":0,"mois5":0,"mois6":0,"mois7":0,"mois8":0,"mois9":0,"mois10":0,"mois11":0,"mois12":0,"total":0}
        if(data !== undefined){
            for(let property in this.originalData) {
                this.originalData[property] = data.hasOwnProperty(property) ? data[property] : 0;
            }
        }
        this.titre = titre !== undefined ? titre : "indefini";
        this['hasChildren'] = false;
        
    }


    /**
     * Fetch all relevant data for the Tresor.
     */
    data(tag) {
        let result = {};

        for (let property in this.originalData) {
            result[property] = this.originalData[property];
        }
        // data['total'] = this.originalData.reduce(this.reducer);
        if(tag!==undefined) {result['tag'] = tag}
        result['titre'] = this.titre;
        if(this.originalData.hasOwnProperty("children")){
            if(this.hasChildren == true) { 
                //recursive call for child data extraction
                result['children'] = this.originalData.children.map( child => child.data() );
            }
        }        
        return result;
    }


    /**
     * Reset the Tresor fields.
     */
    addChildren(child) {
        if(!this.hasOwnProperty("hasChildren")) {
            this['hasChildren'] = true;
            this.originalData['children'] = new Array();
        }else if(this.hasChildren == false){
            this.originalData['children'] = new Array();
            this.hasChildren = true;
        }
        this.originalData.children.push(child)
    }


    /**
     * Sum all elements in an array
     * .
     * @param {array} data
     */
    reducer(accumulator, currentValue) { return accumulator + currentValue};
    
    /**
     * Reset the form fields.
     */
    reset() {
        for (let field in this.originalData) {
            this[field] = '';
        }
    }

    /**
     * get the value of  field
     */
    get(property) {
        return this.originalData[property]        
    }
    
    /**
     * set the value of  field
     */
    set(property, value) {
        if(this.originalData.hasOwnProperty(property) && value!= null) {
            this.originalData[property] = value;     
        }
    }
    
    /**
     * increment the value of  field
     */
    increment(property, value) {
        if(this.originalData.hasOwnProperty(property) && value!= null) {
            this.originalData[property] += value;     
        }
    }

}

export default Tresor;