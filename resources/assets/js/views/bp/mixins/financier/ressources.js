// assets/js/views/bp/mixins/financier/resssources
import {arround, mensualiteCredit, HT} from '../../../../services/helper'
import currenciesMaskMixin from '../currenciesMask'

export default {
    mixins: [currenciesMaskMixin],
    data() {
        return {
            /* Apport en capital */
            subTotalCapAppNumeraire:0,
            subTotalCapAppNature:0,
            /* Comptes Courants Associes */
            totalCompteAssocie:0,
            /* Emprunt bancaires */
            subTotalEmpruntBanque:{
                montant:0,
                echeancier:0,
                remboursement:0
            },
            subTotalRemboursAnnuelEmpruntBancaire: {
                annee1:{montant:0, interet:0},
                annee2:{montant:0, interet:0},
                annee3:{montant:0, interet:0},
                annee4:{montant:0, interet:0}
            },
            subTotalEmpruntMicroFin:{
                montant:0,
                echeancier:0,
                remboursement:0
            },
            subTotalRemboursAnnuelEmpruntMicroFin: {
                annee1:{montant:0, interet:0},
                annee2:{montant:0, interet:0},
                annee3:{montant:0, interet:0},
                annee4:{montant:0, interet:0}
            },
            /* Subventions aides institutions */
            subTotalAidBanque:{
                montant:0,
                echeancier:0,
                remboursement:0
            },
            subTotalAidInstitution:{
                montant:0,
                echeancier:0,
                remboursement:0
            },
            subTotalAidAutre:{
                montant:0,
                echeancier:0,
                remboursement:0
            },
            // remboursement des subventions
            subTotalRemboursAnnuelSubvBancaire: {
                annee1:{montant:0, interet:0},
                annee2:{montant:0, interet:0},
                annee3:{montant:0, interet:0},
                annee4:{montant:0, interet:0}
            },
            subTotalRemboursAnnuelSubvInsti: {
              annee1:{montant:0, interet:0},
              annee2:{montant:0, interet:0},
              annee3:{montant:0, interet:0},
              annee4:{montant:0, interet:0}
            },
            subTotalRemboursAnnuelSubvAutre: {
              annee1:{montant:0, interet:0},
              annee2:{montant:0, interet:0},
              annee3:{montant:0, interet:0},
              annee4:{montant:0, interet:0}
            }
        }
    },
    methods: {
        arround(value, decimals) {
            return arround(value, decimals);
        },
        calculSubTotalCapAppNumeraire(capitauxNumeraires){
            let sum = 0;
            if(capitauxNumeraires){
                capitauxNumeraires.forEach( (item) =>  {
                    sum = sum + (item.quantite * item.montant);
                });
            }
            this.subTotalCapAppNumeraire = arround(sum, this.currentCurrencyMask[':precision']);
        },
        calculSubTotalCapAppNumeraireHT(capitauxNumeraires, tva){
            let sum = 0;
            if(capitauxNumeraires){
                capitauxNumeraires.forEach( (item) =>  {
                    sum = sum + (item.quantite * HT(item.montant, tva=0));
                });
            }
            this.subTotalCapAppNumeraire = arround(sum, this.currentCurrencyMask[':precision']);
        },
        calculSubTotalCapAppNature(capitauxNatures){
            let sum = 0;
            if(capitauxNatures){
                capitauxNatures.forEach( (item) =>  {
                    sum = sum + (item.quantite * item.montant);
                });
            }
            this.subTotalCapAppNature = arround(sum, this.currentCurrencyMask[':precision']);
        },
        calculSubTotalCapAppNatureHT(capitauxNatures, tva){
            let sum = 0;
            if(capitauxNatures){
                capitauxNatures.forEach( (item) =>  {
                    sum = sum + (item.quantite * HT(item.montant, tva=0));
                });
            }
            this.subTotalCapAppNature = arround(sum, this.currentCurrencyMask[':precision']);
        },
        calculRemboursementHT: function(ressource, tva) {
           
            // let ressourceHT = {'duree':ressource.duree, 'taux':ressource.taux,'montantPret':HT(ressource.montantPret, tva)};
             let ressourceHT = {'duree':ressource.duree, 'taux':ressource.taux,'montantPret':ressource.montantPret};
            return this.calculRemboursement(ressourceHT);
        },
        "calculRemboursement"(ressource){
          // TODO: recalculer les remboursements par la methode de Mr Simplice
            let result =  {
                annee1:{montant:0, interet:0},
                annee2:{montant:0, interet:0},
                annee3:{montant:0, interet:0},
                annee4:{montant:0, interet:0}
            };
            if(ressource){
            
                let montantTotal = this.isCadeau(ressource.montantPret, ressource.taux, ressource.duree);
                let mensualite = (ressource.duree > 0) ? montantTotal / ressource.duree : 0;
                let interet = (montantTotal > 0) ? arround(montantTotal - ressource.montantPret, this.currentCurrencyMask[':precision']) : 0; // interet annuel
                //  let interet = (montantTotal > 0) ? arround(montantTotal , this.currentCurrencyMask[':precision']) : 0; // interet annuel
                switch(true){
                  case (ressource.duree <= 12 && ressource.duree > 0):
                      result.annee1.montant = arround(montantTotal, this.currentCurrencyMask[':precision']);
                      result.annee1.interet = arround(interet, this.currentCurrencyMask[':precision']);
                  break;
                  case (ressource.duree > 12 && ressource.duree <= 24):
                  // le nombre de mois restants apres la 1ere anneee
                      result.annee1.montant = arround(montantTotal *  12 / ressource.duree, this.currentCurrencyMask[':precision']);
                      result.annee1.interet = arround(interet *  12 / ressource.duree, this.currentCurrencyMask[':precision']);
                      result.annee2.montant = arround(mensualite * (ressource.duree-12), this.currentCurrencyMask[':precision']);
                      result.annee2.interet = arround(interet * (ressource.duree - 12) / ressource.duree, this.currentCurrencyMask[':precision']);
                  break;
                  case (ressource.duree > 24 && ressource.duree <= 36):
                      result.annee1.montant = arround(montantTotal * 12 / ressource.duree, this.currentCurrencyMask[':precision']);
                      result.annee1.interet = arround(interet * 12 / ressource.duree, this.currentCurrencyMask[':precision']);
                      result.annee2.montant = arround(mensualite * 12, this.currentCurrencyMask[':precision']);
                      result.annee2.interet = arround(interet * 12 / ressource.duree, this.currentCurrencyMask[':precision']);
                      result.annee3.montant = arround(mensualite * (ressource.duree - 24), this.currentCurrencyMask[':precision']);
                      result.annee3.interet = arround(interet * (ressource.duree - 24) / ressource.duree, this.currentCurrencyMask[':precision']);
                  break;
                  case (ressource.duree > 36):
                    result.annee1.montant = arround(montantTotal * 12 / ressource.duree, this.currentCurrencyMask[':precision']);
                    result.annee1.interet = arround(interet * 12 / ressource.duree, this.currentCurrencyMask[':precision']);
                    result.annee2.montant = arround((montantTotal-result.annee1.montant) * 12 / (ressource.duree-12), this.currentCurrencyMask[':precision']);
                    result.annee2.interet = arround(interet * 12 / ressource.duree, this.currentCurrencyMask[':precision']);
                    result.annee3.montant = arround((montantTotal-(result.annee1.montant+result.annee2.montant)) * 12/(ressource.duree - 24), this.currentCurrencyMask[':precision']);
                    result.annee4.montant = arround((montantTotal-(result.annee1.montant+result.annee2.montant+result.annee3.montant)) * 12/(ressource.duree -36), this.currentCurrencyMask[':precision']);
                    result.annee4.interet = arround(interet * (ressource.duree - 24)/ ressource.duree, this.currentCurrencyMask[':precision']);
                  break;
                }
                return result;
            }
            return result;
        },
        calculTotalCompteAssocie(comptes){
            let sum = 0;
            if(comptes){
                comptes.forEach((item) =>{
                    sum = sum + (item.quantite * item.montant);
                });
            }
            this.totalCompteAssocie = arround(sum, this.currentCurrencyMask[':precision']);
        },
        calculTotalCompteAssocieHT(comptes, tva){
            let sum = 0;
            if(comptes){
                comptes.forEach((item) =>{
                    sum = sum + (item.quantite * HT(item.montant, tva=0));
                });
            }
            this.totalCompteAssocie = arround(sum, this.currentCurrencyMask[':precision']);
        },
        calculSubTotalEmpruntBanque(empruntsBanque){
            let sumMontant = 0; let sumEcheancier = 0; let sumRemboursement = 0;
            if(empruntsBanque){
                empruntsBanque.forEach( (item) => {
                    sumMontant = sumMontant + item.montantPret;
                    sumRemboursement = sumRemboursement + item.montantPret + (item.montantPret * item.taux / 100);
                    if(item.duree > 0) {
                        sumEcheancier = sumEcheancier + mensualiteCredit(item.taux / 100, item.montantPret, item.duree) ;
                    }
                });
            }
            this.subTotalEmpruntBanque.montant = arround(sumMontant, this.currentCurrencyMask[':precision']);
            this.subTotalEmpruntBanque.echeancier = arround(sumEcheancier, this.currentCurrencyMask[':precision']);
            this.subTotalEmpruntBanque.remboursement = arround(sumRemboursement, this.currentCurrencyMask[':precision']);
        },
        calculTotalRemboursAnnuelEmpruntBanque: function(empruntsBanque){
            let result = {
                annee1:{montant:0, interet:0},
                annee2:{montant:0, interet:0},
                annee3:{montant:0, interet:0},
                annee4:{montant:0, interet:0}
            };
            if(empruntsBanque){
                empruntsBanque.forEach( (item) => {
                    result.annee1.montant = arround(result.annee1.montant + this.calculRemboursement(item).annee1.montant, this.currentCurrencyMask[':precision']);
                    result.annee1.interet = arround(result.annee1.interet + this.calculRemboursement(item).annee1.interet, this.currentCurrencyMask[':precision']);
                    result.annee2.montant = arround(result.annee2.montant + this.calculRemboursement(item).annee2.montant, this.currentCurrencyMask[':precision']);
                    result.annee2.interet = arround(result.annee2.interet + this.calculRemboursement(item).annee2.interet, this.currentCurrencyMask[':precision']);
                    result.annee3.montant = arround(result.annee3.montant + this.calculRemboursement(item).annee3.montant, this.currentCurrencyMask[':precision']);
                    result.annee3.interet = arround(result.annee3.interet + this.calculRemboursement(item).annee3.interet, this.currentCurrencyMask[':precision']);
                    result.annee4.montant = arround(result.annee4.montant + this.calculRemboursement(item).annee4.montant, this.currentCurrencyMask[':precision']);
                    result.annee4.interet = arround(result.annee4.interet + this.calculRemboursement(item).annee4.interet, this.currentCurrencyMask[':precision']);
                });
            }
            this.subTotalRemboursAnnuelEmpruntBancaire = result;
        },
        calculSubTotalEmpruntBanqueHT(empruntsBanque, tva=0){
            let sumMontant = 0; let sumEcheancier = 0; let sumRemboursement = 0;
            if(empruntsBanque){
                empruntsBanque.forEach( (item) => {
                    sumMontant = sumMontant + HT(item.montantPret, tva=0);
                    sumRemboursement = sumRemboursement + HT(item.montantPret, tva=0) + ( HT(item.montantPret, tva=0) * item.taux / 100);
                    if(item.duree > 0) {
                        sumEcheancier = sumEcheancier + mensualiteCredit(item.taux / 100, HT(item.montantPret, tva=0), item.duree) ;
                    }
                });
            }
            this.subTotalEmpruntBanque.montant = arround(sumMontant, this.currentCurrencyMask[':precision']);
            this.subTotalEmpruntBanque.echeancier = arround(sumEcheancier, this.currentCurrencyMask[':precision']);
            this.subTotalEmpruntBanque.remboursement = arround(sumRemboursement, this.currentCurrencyMask[':precision']);
        },
        calculTotalRemboursAnnuelEmpruntBanqueHT: function(empruntsBanque, tva=0){
            let result = {
                annee1:{montant:0, interet:0},
                annee2:{montant:0, interet:0},
                annee3:{montant:0, interet:0},
                annee4:{montant:0, interet:0}
            };
            if(empruntsBanque){
                empruntsBanque.forEach( (item) => {
                    result.annee1.montant = arround(result.annee1.montant + this.calculRemboursementHT(item, tva).annee1.montant, this.currentCurrencyMask[':precision']);
                    result.annee1.interet = arround(result.annee1.interet + this.calculRemboursementHT(item, tva).annee1.interet, this.currentCurrencyMask[':precision']);
                    result.annee2.montant = arround(result.annee2.montant + this.calculRemboursementHT(item, tva).annee2.montant, this.currentCurrencyMask[':precision']);
                    result.annee2.interet = arround(result.annee2.interet + this.calculRemboursementHT(item, tva).annee2.interet, this.currentCurrencyMask[':precision']);
                    result.annee3.montant = arround(result.annee3.montant + this.calculRemboursementHT(item, tva).annee3.montant, this.currentCurrencyMask[':precision']);
                    result.annee3.interet = arround(result.annee3.interet + this.calculRemboursementHT(item, tva).annee3.interet, this.currentCurrencyMask[':precision']);
                    result.annee4.montant = arround(result.annee4.montant + this.calculRemboursementHT(item, tva).annee4.montant, this.currentCurrencyMask[':precision']);
                    result.annee4.interet = arround(result.annee4.interet + this.calculRemboursementHT(item, tva).annee4.interet, this.currentCurrencyMask[':precision']);
                });
            }
            this.subTotalRemboursAnnuelEmpruntBancaire = result;
        },
        calculSubTotalEmpruntMicroFin (empruntsMicroFinance){
            let sumMontant = 0; let sumEcheancier = 0; let sumRemboursement = 0;
            if(empruntsMicroFinance){
                empruntsMicroFinance.forEach( (item) => {
                    sumMontant = sumMontant + item.montantPret;
                    sumRemboursement = sumRemboursement + item.montantPret + (item.montantPret * item.taux / 100);
                    if(item.duree > 0) {
                        sumEcheancier = sumEcheancier + mensualiteCredit( item.taux / 100, item.montantPret, item.duree);
                    }
                });
            }
            this.subTotalEmpruntMicroFin.montant = arround(sumMontant, this.currentCurrencyMask[':precision']);
            this.subTotalEmpruntMicroFin.echeancier = arround(sumEcheancier, this.currentCurrencyMask[':precision']);
            this.subTotalEmpruntMicroFin.remboursement = arround(sumRemboursement, this.currentCurrencyMask[':precision']);
        },
        calculTotalRemboursAnnuelEmpruntMicroFin: function(empruntsMicroFinance){
            let result = {
                annee1:{montant:0, interet:0},
                annee2:{montant:0, interet:0},
                annee3:{montant:0, interet:0},
                annee4:{montant:0, interet:0}
            };
            if(empruntsMicroFinance){
                empruntsMicroFinance.forEach( (item) => {
                    result.annee1.montant = arround(result.annee1.montant + this.calculRemboursement(item).annee1.montant, this.currentCurrencyMask[':precision']);
                    result.annee1.interet = arround(result.annee1.interet + this.calculRemboursement(item).annee1.interet, this.currentCurrencyMask[':precision']);
                    result.annee2.montant = arround(result.annee2.montant + this.calculRemboursement(item).annee2.montant, this.currentCurrencyMask[':precision']);
                    result.annee2.interet = arround(result.annee2.interet + this.calculRemboursement(item).annee2.interet, this.currentCurrencyMask[':precision']);
                    result.annee3.montant = arround(result.annee3.montant + this.calculRemboursement(item).annee3.montant, this.currentCurrencyMask[':precision']);
                    result.annee3.interet = arround(result.annee3.interet + this.calculRemboursement(item).annee3.interet, this.currentCurrencyMask[':precision']);
                    result.annee4.montant = arround(result.annee4.montant + this.calculRemboursement(item).annee4.montant, this.currentCurrencyMask[':precision']);
                    result.annee4.interet = arround(result.annee4.interet + this.calculRemboursement(item).annee4.interet, this.currentCurrencyMask[':precision']);
                });
            }
            this.subTotalRemboursAnnuelEmpruntMicroFin = result;
        },
        calculSubTotalEmpruntMicroFinHT (empruntsMicroFinance, tva){
            let sumMontant = 0; let sumEcheancier = 0; let sumRemboursement = 0;
            if(empruntsMicroFinance){
                empruntsMicroFinance.forEach( (item) => {
                    sumMontant = sumMontant + HT(item.montantPret, tva=0);
                    sumRemboursement = sumRemboursement + HT(item.montantPret, tva=0) + (HT(item.montantPret, tva=0) * item.taux / 100);
                    if(item.duree > 0) {
                        sumEcheancier = sumEcheancier + mensualiteCredit( item.taux / 100, HT(item.montantPret, tva=0), item.duree);
                    }
                });
            }
            this.subTotalEmpruntMicroFin.montant = arround(sumMontant, this.currentCurrencyMask[':precision']);
            this.subTotalEmpruntMicroFin.echeancier = arround(sumEcheancier, this.currentCurrencyMask[':precision']);
            this.subTotalEmpruntMicroFin.remboursement = arround(sumRemboursement, this.currentCurrencyMask[':precision']);
        },
        calculTotalRemboursAnnuelEmpruntMicroFinHT: function(empruntsMicroFinance, tva){
            let result = {
                annee1:{montant:0, interet:0},
                annee2:{montant:0, interet:0},
                annee3:{montant:0, interet:0},
                annee4:{montant:0, interet:0}
            };
            if(empruntsMicroFinance){
                empruntsMicroFinance.forEach( (item) => {
                    result.annee1.montant = arround(result.annee1.montant + this.calculRemboursementHT(item, tva).annee1.montant, this.currentCurrencyMask[':precision']);
                    result.annee1.interet = arround(result.annee1.interet + this.calculRemboursementHT(item, tva).annee1.interet, this.currentCurrencyMask[':precision']);
                    result.annee2.montant = arround(result.annee2.montant + this.calculRemboursementHT(item, tva).annee2.montant, this.currentCurrencyMask[':precision']);
                    result.annee2.interet = arround(result.annee2.interet + this.calculRemboursementHT(item, tva).annee2.interet, this.currentCurrencyMask[':precision']);
                    result.annee3.montant = arround(result.annee3.montant + this.calculRemboursementHT(item, tva).annee3.montant, this.currentCurrencyMask[':precision']);
                    result.annee3.interet = arround(result.annee3.interet + this.calculRemboursementHT(item, tva).annee3.interet, this.currentCurrencyMask[':precision']);
                    result.annee4.montant = arround(result.annee4.montant + this.calculRemboursementHT(item, tva).annee4.montant, this.currentCurrencyMask[':precision']);
                    result.annee4.interet = arround(result.annee4.interet + this.calculRemboursementHT(item, tva).annee4.interet, this.currentCurrencyMask[':precision']);
                });
            }
            this.subTotalRemboursAnnuelEmpruntMicroFin = result;
        },
        /*
        calculTotalRemboursementAnnuelEmprunt: function(){
            this.totalRemboursementAnnuelEmprunt.annee1 = this.subTotalRemboursAnnuelEmpruntBancaire.annee1 +
                this.subTotalRemboursAnnuelEmpruntMicroFin.annee1 ;

            this.totalRemboursementAnnuelEmprunt.annee2 = this.subTotalRemboursAnnuelEmpruntBancaire.annee2 +
              this.subTotalRemboursAnnuelEmpruntMicroFin.annee2 ;

            this.totalRemboursementAnnuelEmprunt.annee3 = this.subTotalRemboursAnnuelEmpruntBancaire.annee3 +
              this.subTotalRemboursAnnuelEmpruntMicroFin.annee3;

            this.totalRemboursementAnnuelEmprunt.annee4 = this.subTotalRemboursAnnuelEmpruntBancaire.annee4 +
              this.subTotalRemboursAnnuelEmpruntMicroFin.annee4 ;
        }, */
        calculSubTotalAidBanque (subventionsAidesBancaires){
            let sumMontant = 0; let sumEcheancier = 0; let sumRemboursement = 0;
            if(subventionsAidesBancaires){
                subventionsAidesBancaires.forEach( (item) => {
                    if(item.duree == 0) { item.taux == 0}
                    if(item.taux != 0) { item.duree == 1}
                    sumMontant = sumMontant +  item.montantPret ;
                    if(item.duree > 0) {
                        sumEcheancier = sumEcheancier + mensualiteCredit(item.taux / 100, item.montantPret, item.duree);
                    }
                    sumRemboursement = sumRemboursement + this.isCadeau(item.montantPret, item.taux, item.duree);
                });
            }
            this.subTotalAidBanque.montant = arround(sumMontant, this.currentCurrencyMask[':precision']);
            this.subTotalAidBanque.echeancier = arround(sumEcheancier, this.currentCurrencyMask[':precision']);
            this.subTotalAidBanque.remboursement = arround(sumRemboursement, this.currentCurrencyMask[':precision']);
        },
        calculSubTotalAidBanqueHT (subventionsAidesBancaires, tva){
            let sumMontant = 0; let sumEcheancier = 0; let sumRemboursement = 0;
            if(subventionsAidesBancaires){
                subventionsAidesBancaires.forEach( (item) => {
                    if(item.duree == 0) { item.taux == 0}
                    if(item.taux != 0) { item.duree == 1}
                    sumMontant = sumMontant + HT(item.montantPret, tva) ;
                    if(item.duree > 0) {
                        sumEcheancier = sumEcheancier + mensualiteCredit(item.taux / 100, HT(item.montantPret, tva=0), item.duree);
                    }
                    sumRemboursement = sumRemboursement + this.isCadeau( HT(item.montantPret, tva=0), item.taux, item.duree);
                });
            }
            this.subTotalAidBanque.montant = arround(sumMontant, this.currentCurrencyMask[':precision']);
            this.subTotalAidBanque.echeancier = arround(sumEcheancier, this.currentCurrencyMask[':precision']);
            this.subTotalAidBanque.remboursement = arround(sumRemboursement, this.currentCurrencyMask[':precision']);
        },
        calculTotalRemboursAnnuelAidBanque: function(subventionsAidesBancaires){
            let result = {
              annee1:{montant:0, interet:0},
              annee2:{montant:0, interet:0},
              annee3:{montant:0, interet:0},
              annee4:{montant:0, interet:0}
            };
            if(subventionsAidesBancaires){
                subventionsAidesBancaires.forEach( (item) => {
                    result.annee1.montant = arround(result.annee1.montant + this.calculRemboursement(item).annee1.montant, this.currentCurrencyMask[':precision']);
                    result.annee1.interet = arround(result.annee1.interet + this.calculRemboursement(item).annee1.interet, this.currentCurrencyMask[':precision']);
                    result.annee2.montant = arround(result.annee2.montant + this.calculRemboursement(item).annee2.montant, this.currentCurrencyMask[':precision']);
                    result.annee2.interet = arround(result.annee2.interet + this.calculRemboursement(item).annee2.interet, this.currentCurrencyMask[':precision']);
                    result.annee3.montant = arround(result.annee3.montant + this.calculRemboursement(item).annee3.montant, this.currentCurrencyMask[':precision']);
                    result.annee3.interet = arround(result.annee3.interet + this.calculRemboursement(item).annee3.interet, this.currentCurrencyMask[':precision']);
                    result.annee4.montant = arround(result.annee4.montant + this.calculRemboursement(item).annee4.montant, this.currentCurrencyMask[':precision']);
                    result.annee4.interet = arround(result.annee4.interet + this.calculRemboursement(item).annee4.interet, this.currentCurrencyMask[':precision']);
                });
            }
            this.subTotalRemboursAnnuelSubvBancaire = result;
        },
        calculTotalRemboursAnnuelAidBanqueHT: function(subventionsAidesBancaires, tva){
            let result = {
              annee1:{montant:0, interet:0},
              annee2:{montant:0, interet:0},
              annee3:{montant:0, interet:0},
              annee4:{montant:0, interet:0}
            };
            if(subventionsAidesBancaires){
                subventionsAidesBancaires.forEach( (item) => {
                    result.annee1.montant = arround(result.annee1.montant + this.calculRemboursementHT(item, tva).annee1.montant, this.currentCurrencyMask[':precision']);
                    result.annee1.interet = arround(result.annee1.interet + this.calculRemboursementHT(item, tva).annee1.interet, this.currentCurrencyMask[':precision']);
                    result.annee2.montant = arround(result.annee2.montant + this.calculRemboursementHT(item, tva).annee2.montant, this.currentCurrencyMask[':precision']);
                    result.annee2.interet = arround(result.annee2.interet + this.calculRemboursementHT(item, tva).annee2.interet, this.currentCurrencyMask[':precision']);
                    result.annee3.montant = arround(result.annee3.montant + this.calculRemboursementHT(item, tva).annee3.montant, this.currentCurrencyMask[':precision']);
                    result.annee3.interet = arround(result.annee3.interet + this.calculRemboursementHT(item, tva).annee3.interet, this.currentCurrencyMask[':precision']);
                    result.annee4.montant = arround(result.annee4.montant + this.calculRemboursementHT(item, tva).annee4.montant, this.currentCurrencyMask[':precision']);
                    result.annee4.interet = arround(result.annee4.interet + this.calculRemboursementHT(item, tva).annee4.interet, this.currentCurrencyMask[':precision']);
                });
            }
            this.subTotalRemboursAnnuelSubvBancaire = result;
        },
        calculSubTotalAidInstitution (subventionsAidesInstitution){
            let sumMontant = 0; let sumEcheancier = 0; let sumRemboursement = 0;
            if(subventionsAidesInstitution){
                subventionsAidesInstitution.forEach( (item) => {
                    if(item.duree == 0) { item.taux == 0}
                    if(item.taux != 0) { item.duree == 1}
                    sumMontant = sumMontant + item.montantPret;
                    sumRemboursement = sumRemboursement + this.isCadeau(item.montantPret, item.taux, item.duree);
                    if(item.duree > 0) {
                        sumEcheancier = sumEcheancier + mensualiteCredit(item.taux / 100, item.montantPret, item.duree);
                    }
                });
            }
            this.subTotalAidInstitution.montant = arround(sumMontant, this.currentCurrencyMask[':precision']);
            this.subTotalAidInstitution.echeancier = arround(sumEcheancier, this.currentCurrencyMask[':precision']);
            this.subTotalAidInstitution.remboursement = arround(sumRemboursement, this.currentCurrencyMask[':precision']);
        },
        calculSubTotalAidInstitutionHT (subventionsAidesInstitution, tva=0){
            let sumMontant = 0; let sumEcheancier = 0; let sumRemboursement = 0;
            if(subventionsAidesInstitution){
                subventionsAidesInstitution.forEach( (item) => {
                    if(item.duree == 0) { item.taux == 0}
                    if(item.taux != 0) { item.duree == 1}
                    sumMontant = sumMontant + HT(item.montantPret, tva);
                    sumRemboursement = sumRemboursement + this.isCadeau( HT(item.montantPret, tva), item.taux, item.duree);
                    if(item.duree > 0) {
                        sumEcheancier = sumEcheancier + mensualiteCredit(item.taux / 100, HT(item.montantPret, tva), item.duree);
                    }
                });
            }
            this.subTotalAidInstitution.montant = arround(sumMontant, this.currentCurrencyMask[':precision']);
            this.subTotalAidInstitution.echeancier = arround(sumEcheancier, this.currentCurrencyMask[':precision']);
            this.subTotalAidInstitution.remboursement = arround(sumRemboursement, this.currentCurrencyMask[':precision']);
        },
        calculTotalRemboursAnnuelAidInstitution: function(subventionsAidesInstitutions){
            let result = {
              annee1:{montant:0, interet:0},
              annee2:{montant:0, interet:0},
              annee3:{montant:0, interet:0},
              annee4:{montant:0, interet:0}
            };
            if(subventionsAidesInstitutions){
                subventionsAidesInstitutions.forEach( (item) => {
                    result.annee1.montant = arround(result.annee1.montant + this.calculRemboursement(item).annee1.montant, this.currentCurrencyMask[':precision']);
                    result.annee1.interet = arround(result.annee1.interet + this.calculRemboursement(item).annee1.interet, this.currentCurrencyMask[':precision']);
                    result.annee2.montant = arround(result.annee2.montant + this.calculRemboursement(item).annee2.montant, this.currentCurrencyMask[':precision']);
                    result.annee2.interet = arround(result.annee2.interet + this.calculRemboursement(item).annee2.interet, this.currentCurrencyMask[':precision']);
                    result.annee3.montant = arround(result.annee3.montant + this.calculRemboursement(item).annee3.montant, this.currentCurrencyMask[':precision']);
                    result.annee3.interet = arround(result.annee3.interet + this.calculRemboursement(item).annee3.interet, this.currentCurrencyMask[':precision']);
                    result.annee4.montant = arround(result.annee4.montant + this.calculRemboursement(item).annee4.montant, this.currentCurrencyMask[':precision']);
                    result.annee4.interet = arround(result.annee4.interet + this.calculRemboursement(item).annee4.interet, this.currentCurrencyMask[':precision']);
                });
            }
            this.subTotalRemboursAnnuelSubvInsti = result;
        },
        calculTotalRemboursAnnuelAidInstitutionHT: function(subventionsAidesBancaires, tva){
            let result = {
              annee1:{montant:0, interet:0},
              annee2:{montant:0, interet:0},
              annee3:{montant:0, interet:0},
              annee4:{montant:0, interet:0}
            };
            if(subventionsAidesBancaires){
                subventionsAidesBancaires.forEach( (item) => {
                    result.annee1.montant = arround(result.annee1.montant + this.calculRemboursementHT(item, tva).annee1.montant, this.currentCurrencyMask[':precision']);
                    result.annee1.interet = arround(result.annee1.interet + this.calculRemboursementHT(item, tva).annee1.interet, this.currentCurrencyMask[':precision']);
                    result.annee2.montant = arround(result.annee2.montant + this.calculRemboursementHT(item, tva).annee2.montant, this.currentCurrencyMask[':precision']);
                    result.annee2.interet = arround(result.annee2.interet + this.calculRemboursementHT(item, tva).annee2.interet, this.currentCurrencyMask[':precision']);
                    result.annee3.montant = arround(result.annee3.montant + this.calculRemboursementHT(item, tva).annee3.montant, this.currentCurrencyMask[':precision']);
                    result.annee3.interet = arround(result.annee3.interet + this.calculRemboursementHT(item, tva).annee3.interet, this.currentCurrencyMask[':precision']);
                    result.annee4.montant = arround(result.annee4.montant + this.calculRemboursementHT(item, tva).annee4.montant, this.currentCurrencyMask[':precision']);
                    result.annee4.interet = arround(result.annee4.interet + this.calculRemboursementHT(item, tva).annee4.interet, this.currentCurrencyMask[':precision']);
                });
            }
            this.subTotalRemboursAnnuelSubvInsti = result;
        },
        // dis moi si je te rembourse ou pas
        isCadeau(montantPret, taux, duree) {
            if(duree == 0){
                return 0;
            }else {
               
                    // return arround( montantPret + (taux*montantPret / 100), this.currentCurrencyMask[':precision']);
                    // return arround(montantPret*taux/100, this.currentCurrencyMask[':precision']);
                    return arround(montantPret*(1+taux/100), this.currentCurrencyMask[':precision']);
              
            }
        },
        calculSubTotalAidAutre (autresSubventionsAides){
            let sumMontant = 0; let sumEcheancier = 0; let sumRemboursement = 0;
            if(autresSubventionsAides){
                autresSubventionsAides.forEach( (item) => {
                    if(item.duree == 0) { item.taux == 0}
                    if(item.taux != 0) { item.duree == 1}
                    sumMontant = sumMontant + item.montantPret ;
                    sumRemboursement = sumRemboursement + this.isCadeau(item.montantPret, item.taux, item.duree);
                    if(item.duree > 0) {
                        sumEcheancier = sumEcheancier + mensualiteCredit(item.taux/100, item.montantPret, item.duree);
                    }
                });
            }
            this.subTotalAidAutre.montant = arround(sumMontant, this.currentCurrencyMask[':precision']);
            this.subTotalAidAutre.echeancier = arround(sumEcheancier, this.currentCurrencyMask[':precision']);
            this.subTotalAidAutre.remboursement = arround(sumRemboursement, this.currentCurrencyMask[':precision']);
        },
        calculTotalRemboursAnnuelAidAutre: function(autresSubventionsAides){
            let result = {
                annee1:{montant:0, interet:0},
                annee2:{montant:0, interet:0},
                annee3:{montant:0, interet:0},
                annee4:{montant:0, interet:0}
            };
            if(autresSubventionsAides){
                autresSubventionsAides.forEach( (item) => {
                    result.annee1.montant = arround(result.annee1.montant + this.calculRemboursement(item).annee1.montant, this.currentCurrencyMask[':precision']);
                    result.annee1.interet = arround(result.annee1.interet + this.calculRemboursement(item).annee1.interet, this.currentCurrencyMask[':precision']);
                    result.annee2.montant = arround(result.annee2.montant + this.calculRemboursement(item).annee2.montant, this.currentCurrencyMask[':precision']);
                    result.annee2.interet = arround(result.annee2.interet + this.calculRemboursement(item).annee2.interet, this.currentCurrencyMask[':precision']);
                    result.annee3.montant = arround(result.annee3.montant + this.calculRemboursement(item).annee3.montant, this.currentCurrencyMask[':precision']);
                    result.annee3.interet = arround(result.annee3.interet + this.calculRemboursement(item).annee3.interet, this.currentCurrencyMask[':precision']);
                    result.annee4.montant = arround(result.annee4.montant + this.calculRemboursement(item).annee4.montant, this.currentCurrencyMask[':precision']);
                    result.annee4.interet = arround(result.annee4.interet + this.calculRemboursement(item).annee4.interet, this.currentCurrencyMask[':precision']);
                });
            }
            this.subTotalRemboursAnnuelSubvAutre = result;
        },
        calculSubTotalAidAutreHT (autresSubventionsAides, tva=0){
            let sumMontant = 0; let sumEcheancier = 0; let sumRemboursement = 0;
            if(autresSubventionsAides){
                autresSubventionsAides.forEach( (item) => {
                    if(item.duree == 0) { item.taux == 0}
                    if(item.taux != 0) { item.duree == 1}
                    sumMontant = sumMontant + HT(item.montantPret, tva) ;
                    sumRemboursement = sumRemboursement + this.isCadeau( HT(item.montantPret, tva), item.taux, item.duree);
                    if(item.duree > 0) {
                        sumEcheancier = sumEcheancier + mensualiteCredit(item.taux/100, HT(item.montantPret, tva), item.duree);
                    }
                });
            }
            this.subTotalAidAutre.montant = arround(sumMontant, this.currentCurrencyMask[':precision']);
            this.subTotalAidAutre.echeancier = arround(sumEcheancier, this.currentCurrencyMask[':precision']);
            this.subTotalAidAutre.remboursement = arround(sumRemboursement, this.currentCurrencyMask[':precision']);
        },
        calculTotalRemboursAnnuelAidAutreHT: function(autresSubventionsAides, tva){
            let result = {
                annee1:{montant:0, interet:0},
                annee2:{montant:0, interet:0},
                annee3:{montant:0, interet:0},
                annee4:{montant:0, interet:0}
            };
            if(autresSubventionsAides){
                autresSubventionsAides.forEach( (item) => {
                    result.annee1.montant = arround(result.annee1.montant + this.calculRemboursementHT(item, tva).annee1.montant, this.currentCurrencyMask[':precision']);
                    result.annee1.interet = arround(result.annee1.interet + this.calculRemboursementHT(item, tva).annee1.interet, this.currentCurrencyMask[':precision']);
                    result.annee2.montant = arround(result.annee2.montant + this.calculRemboursementHT(item, tva).annee2.montant, this.currentCurrencyMask[':precision']);
                    result.annee2.interet = arround(result.annee2.interet + this.calculRemboursementHT(item, tva).annee2.interet, this.currentCurrencyMask[':precision']);
                    result.annee3.montant = arround(result.annee3.montant + this.calculRemboursementHT(item, tva).annee3.montant, this.currentCurrencyMask[':precision']);
                    result.annee3.interet = arround(result.annee3.interet + this.calculRemboursementHT(item, tva).annee3.interet, this.currentCurrencyMask[':precision']);
                    result.annee4.montant = arround(result.annee4.montant + this.calculRemboursementHT(item, tva).annee4.montant, this.currentCurrencyMask[':precision']);
                    result.annee4.interet = arround(result.annee4.interet + this.calculRemboursementHT(item, tva).annee4.interet, this.currentCurrencyMask[':precision']);
                });
            }
            this.subTotalRemboursAnnuelSubvAutre = result;
        },
        // emptyTotauxRessources() {
        //     /* Apport en capital */
        //     this.subTotalCapAppNumeraire = 0,
        //     this.subTotalCapAppNature = 0,
        //     /* Comptes Courants Associes */
        //     this.totalCompteAssocie = 0,
        //     /* Emprunt bancaires */
        //     this.subTotalEmpruntBanque = {
        //         montant:0,
        //         echeancier:0,
        //         remboursement:0
        //     },
        //     this.subTotalRemboursAnnuelEmpruntBancaire =  {
        //         annee1:{montant:0, interet:0},
        //         annee2:{montant:0, interet:0},
        //         annee3:{montant:0, interet:0},
        //         annee4:{montant:0, interet:0}
        //     },
        //     this.subTotalEmpruntMicroFin = {
        //         montant:0,
        //         echeancier:0,
        //         remboursement:0
        //     },
        //     this.subTotalRemboursAnnuelEmpruntMicroFin =  {
        //         annee1:{montant:0, interet:0},
        //         annee2:{montant:0, interet:0},
        //         annee3:{montant:0, interet:0},
        //         annee4:{montant:0, interet:0}
        //     },
        //     /* Subventions aides institutions */
        //     this.subTotalAidBanque = {
        //         montant:0,
        //         echeancier:0,
        //         remboursement:0
        //     },
        //     this.subTotalAidInstitution = {
        //         montant:0,
        //         echeancier:0,
        //         remboursement:0
        //     },
        //     this.subTotalAidAutre = {
        //         montant:0,
        //         echeancier:0,
        //         remboursement:0
        //     },
        //     // remboursement des subventions
        //     this.subTotalRemboursAnnuelSubvBancaire =  {
        //         annee1:{montant:0, interet:0},
        //         annee2:{montant:0, interet:0},
        //         annee3:{montant:0, interet:0},
        //         annee4:{montant:0, interet:0}
        //     },
        //     this.subTotalRemboursAnnuelSubvInsti =  {
        //         annee1:{montant:0, interet:0},
        //         annee2:{montant:0, interet:0},
        //         annee3:{montant:0, interet:0},
        //         annee4:{montant:0, interet:0}
        //     },
        //     this.subTotalRemboursAnnuelSubvAutre =  {
        //         annee1:{montant:0, interet:0},
        //         annee2:{montant:0, interet:0},
        //         annee3:{montant:0, interet:0},
        //         annee4:{montant:0, interet:0}
        //     }
        // }
    },
    computed: {

        totalRemboursementAnnuelEmprunt: function (){
            let result = {
                annee1:{montant:0, interet:0},
                annee2:{montant:0, interet:0},
                annee3:{montant:0, interet:0},
                annee4:{montant:0, interet:0}
            };
            result.annee1.montant = arround(this.subTotalRemboursAnnuelEmpruntBancaire.annee1.montant +
                this.subTotalRemboursAnnuelEmpruntMicroFin.annee1.montant,this.currentCurrencyMask[':precision']) ;
            result.annee1.interet = arround(this.subTotalRemboursAnnuelEmpruntBancaire.annee1.interet +
                this.subTotalRemboursAnnuelEmpruntMicroFin.annee1.interet,this.currentCurrencyMask[':precision']) ;
            result.annee2.montant = arround(this.subTotalRemboursAnnuelEmpruntBancaire.annee2.montant +
              this.subTotalRemboursAnnuelEmpruntMicroFin.annee2.montant,this.currentCurrencyMask[':precision']) ;
            result.annee2.interet = arround(this.subTotalRemboursAnnuelEmpruntBancaire.annee2.interet +
              this.subTotalRemboursAnnuelEmpruntMicroFin.annee2.interet,this.currentCurrencyMask[':precision']) ;
            result.annee3.montant = arround(this.subTotalRemboursAnnuelEmpruntBancaire.annee3.montant +
              this.subTotalRemboursAnnuelEmpruntMicroFin.annee3.montant,this.currentCurrencyMask[':precision']);
            result.annee3.interet = arround(this.subTotalRemboursAnnuelEmpruntBancaire.annee3.interet +
              this.subTotalRemboursAnnuelEmpruntMicroFin.annee3.interet,this.currentCurrencyMask[':precision']);
            result.annee4.montant = arround(this.subTotalRemboursAnnuelEmpruntBancaire.annee4.montant +
              this.subTotalRemboursAnnuelEmpruntMicroFin.annee4.montant,this.currentCurrencyMask[':precision']) ;
            result.annee4.interet = arround(this.subTotalRemboursAnnuelEmpruntBancaire.annee4.interet +
              this.subTotalRemboursAnnuelEmpruntMicroFin.annee4.interet,this.currentCurrencyMask[':precision']) ;
            return result;
        },
        totalApportCapital(){
            return arround(this.subTotalCapAppNature + this.subTotalCapAppNumeraire,
              this.currentCurrencyMask[':precision']);
        },
        totalSubventionAide (){
            let remboursement = arround(this.subTotalAidBanque.remboursement
                + this.subTotalAidInstitution.remboursement + this.subTotalAidAutre.remboursement,
              this.currentCurrencyMask[':precision']);
            let montant = arround(this.subTotalAidBanque.montant
                + this.subTotalAidInstitution.montant + this.subTotalAidAutre.montant,
              this.currentCurrencyMask[':precision']);
            let echeancier = arround(this.subTotalAidBanque.echeancier
                + this.subTotalAidInstitution.echeancier + this.subTotalAidAutre.echeancier,
              this.currentCurrencyMask[':precision']);
            return { montant: montant, echeancier: echeancier, remboursement: remboursement};
        },
        totalEmpruntBancaire (){
            let remboursement = arround(this.subTotalEmpruntBanque.remboursement
                + this.subTotalEmpruntMicroFin.remboursement, this.currentCurrencyMask[':precision']);
            let montant =arround( this.subTotalEmpruntBanque.montant
                + this.subTotalEmpruntMicroFin.montant, this.currentCurrencyMask[':precision']);
            let echeancier = arround(this.subTotalEmpruntBanque.echeancier
                + this.subTotalEmpruntMicroFin.echeancier , this.currentCurrencyMask[':precision']);
            return { montant: montant, echeancier: echeancier, remboursement: remboursement};
        },
        totalRemboursementAnnuelSubvention(){
            let result = {
                annee1:{montant:0, interet:0},
                annee2:{montant:0, interet:0},
                annee3:{montant:0, interet:0},
                annee4:{montant:0, interet:0}
            }

            result.annee1.montant = arround(this.subTotalRemboursAnnuelSubvBancaire.annee1.montant +
                this.subTotalRemboursAnnuelSubvInsti.annee1.montant + this.subTotalRemboursAnnuelSubvAutre.annee1.montant,
              this.currentCurrencyMask[':precision']);
            result.annee1.interet = arround(this.subTotalRemboursAnnuelSubvBancaire.annee1.interet +
                this.subTotalRemboursAnnuelSubvInsti.annee1.interet + this.subTotalRemboursAnnuelSubvAutre.annee1.interet,
              this.currentCurrencyMask[':precision']);

            result.annee2.montant = arround(this.subTotalRemboursAnnuelSubvBancaire.annee2.montant +
              this.subTotalRemboursAnnuelSubvInsti.annee2.montant + this.subTotalRemboursAnnuelSubvAutre.annee2.montant ,
              this.currentCurrencyMask[':precision']);
            result.annee2.interet = arround(this.subTotalRemboursAnnuelSubvBancaire.annee2.interet +
              this.subTotalRemboursAnnuelSubvInsti.annee2.interet + this.subTotalRemboursAnnuelSubvAutre.annee2.interet ,
              this.currentCurrencyMask[':precision']);

            result.annee3.montant = arround(this.subTotalRemboursAnnuelSubvBancaire.annee3.montant +
              this.subTotalRemboursAnnuelSubvInsti.annee3.montant + this.subTotalRemboursAnnuelSubvAutre.annee3.montant,
              this.currentCurrencyMask[':precision']);
            result.annee3.interet = arround(this.subTotalRemboursAnnuelSubvBancaire.annee3.interet +
              this.subTotalRemboursAnnuelSubvInsti.annee3.interet + this.subTotalRemboursAnnuelSubvAutre.annee3.interet,
              this.currentCurrencyMask[':precision']);

            result.annee4.montant = arround(this.subTotalRemboursAnnuelSubvBancaire.annee4.montant +
              this.subTotalRemboursAnnuelSubvInsti.annee4.montant + this.subTotalRemboursAnnuelSubvAutre.annee4.montant ,
              this.currentCurrencyMask[':precision']);
            result.annee4.interet = arround(this.subTotalRemboursAnnuelSubvBancaire.annee4.interet +
              this.subTotalRemboursAnnuelSubvInsti.annee4.interet + this.subTotalRemboursAnnuelSubvAutre.annee4.interet ,
              this.currentCurrencyMask[':precision']);

            return result;
        },
    }
}
