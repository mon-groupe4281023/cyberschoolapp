
import firebase from 'firebase'
import { StarRating } from 'vue-rate-it'
export default {
    components: {
        StarRating
    },
    computed: {
        note() {
            return this.note1.rating1 + this.note1.rating2 + this.note1.rating3 + this.note1.rating4;
        },
    },
    data() {
        return {
            note1: {
                rating1: 0,
                rating2: 0,
                rating3: 0,
                rating4: 0,
            },
            mentionCritere1: '',
            mentionCritere2: '',
            mentionCritere3: '',
            mentionCritere4: '',
            starColor: ''
        }
    },
    methods: {
        setCritere1Mention(e) {
            switch (e) {
                case 1:
                    this.mentionCritere1 = 'Médiocre';
                    this.starColor = '#398bf7';
                    break;
                case 2:
                    this.mentionCritere1 = 'Insuffisant';
                    break;
                case 3:
                    this.mentionCritere1 = 'Assez-bien';
                    break;
                case 4:
                    this.mentionCritere1 = 'Bien';
                    break;
                case 5:
                    this.mentionCritere1 = 'Excellent';
                    break;
                default:
                    this.mentionCritere1 = '';
                    break;
            }
        },
        setCritere2Mention(e) {
            switch (e) {
                case 1:
                    this.mentionCritere2 = 'Médiocre';
                    break;
                case 2:
                    this.mentionCritere2 = 'Insuffisant';
                    break;
                case 3:
                    this.mentionCritere2 = 'Assez-bien';
                    break;
                case 4:
                    this.mentionCritere2 = 'Bien';
                    break;
                case 5:
                    this.mentionCritere2 = 'Excellent';
                    break;
                default:
                    this.mentionCritere2 = '';
                    break;
            }
        },
        setCritere3Mention(e) {
            switch (e) {
                case 1:
                    this.mentionCritere3 = 'Médiocre';
                    break;
                case 2:
                    this.mentionCritere3 = 'Insuffisant';
                    break;
                case 3:
                    this.mentionCritere3 = 'Assez-bien';
                    break;
                case 4:
                    this.mentionCritere3 = 'Bien';
                    break;
                case 5:
                    this.mentionCritere3 = 'Excellent';
                    break;
                default:
                    this.mentionCritere3 = '';
                    break;
            }
        },
        setCritere4Mention(e) {
            switch (e) {
                case 1:
                    this.mentionCritere4 = 'Médiocre';
                    break;
                case 2:
                    this.mentionCritere4 = 'Insuffisant';
                    break;
                case 3:
                    this.mentionCritere4 = 'Assez-bien';
                    break;
                case 4:
                    this.mentionCritere4 = 'Bien';
                    break;
                case 5:
                    this.mentionCritere4 = 'Excellent';
                    break;
                default:
                    this.mentionCritere4 = '';
                    break;
            }
        },
        getAuthUserFullName() {

            if (sessionStorage.getItem("userSession")) {

                let userInfo = JSON.parse(sessionStorage.getItem("userSession"));
                let datef = userInfo.firstTimeConnection;
                let date = (new Date(datef)).toLocaleString();
                //console.log(userInfo);
                let profileName = userInfo.displayName;
                return profileName
            }
        },
    }
}