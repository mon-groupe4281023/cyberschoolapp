

import Helpers from "../../../../services/helper";
export default{
mixin:[Helpers],
data () {
    return{
        etatDroitsAdmin:false,
        etatDroitsSuivi:false,
        etatDroitsFinancier:false,
        permissionIncubateurInit:[],
        permissionSuiviInit:[],
        permissionFinancierInit:[],

    }
},
methods:{

    swithStateDroitsAdmin(value){
        if(!value){
            this.permissionIncubateur.liste=[];
            this.permissionIncubateur.liste=this.permissionIncubateurInit   
        }
        else{
            this.fullscreenLoading=true
            this.ref.doc(this.idUserIndex).get()
            .then((doc)=>{
                if (doc.exists) {
                   
            if(doc.data().admin){
                this.permissionIncubateur.liste=[],
                doc.data().admin.forEach(item=>{
                this.permissionIncubateur.liste.push(item)
                })
                this.permissionIncubateur.quotaConcours= doc.data().quotaConcours
            }
                this.fullscreenLoading=false
                this.dialogVisible=true
                   
                } else {
                    console.log('No such document!');
                    this.fullscreenLoading=false;
                    this.dialogVisible=true
                   
                }
            }).catch(()=>{

            })  
        }
              

    },
    swithStateDroitsSuivi(value){
        if(!value){
            this.permissionSuivi.liste=[];
            this.permissionSuivi.liste=this.permissionSuiviInit 
        }
        else{
            this.fullscreenLoading=true
            this.ref.doc(this.idUserIndex).get()
            .then((doc)=>{
                if (doc.exists) {
                        
                    if(doc.data().suivi){
                        this.permissionSuiviInit=this.permissionSuivi.liste
                        this.etatDroitsSuivi=true
                        this.permissionSuivi.liste=[],
                        doc.data().suivi.forEach(item=>{
                        this.permissionSuivi.liste.push(item)
                        })
                    }
                        
                this.fullscreenLoading=false
                this.dialogVisible=true
                   
                } else {
                    console.log('No such document!');
                    this.fullscreenLoading=false;
                    this.dialogVisible=true
                   
                }
            }).catch(()=>{

            })  
        }
              

    },
    swithStateDroitsFinancier(value){
        if(!value){
            this.permissionBpFinancier.liste=[];
            this.permissionBpFinancier.liste=this.permissionFinancierInit   
        }
        else{
            this.fullscreenLoading=true
            this.ref.doc(this.idUserIndex).get()
            .then((doc)=>{
                if (doc.exists) {
                   
                    if(doc.data().bpFinancier){
                        this.permissionFinancierInit=this.permissionBpFinancier.liste
                        this.etatDroitsFinancier=true
                        this.permissionBpFinancier.liste=[],
                        doc.data().bpFinancier.forEach(item=>{
                            this.permissionBpFinancier.liste.push(item)
                        })
                    }

                this.fullscreenLoading=false
                this.dialogVisible=true
                   
                } else {
                    console.log('No such document!');
                    this.fullscreenLoading=false;
                    this.dialogVisible=true
                   
                }
            }).catch(()=>{

            })  
        }
              

    },

}

}
