
export default {
            // data(){
            //     return{
            //         progressAll:0
            //     }
            // },
    methods: {
        getSumSubActionAll(data){
            if(data) {
                let i=0;
                if(data.length){
                    data.forEach(element => {
               
                        i++
           
                });
                }else{
                    i++
                }
            
                return i
            }

         },
        getSumSubActionDone(data){
            if(data) {
                let i=0;
                if(data && data.length){
                    data.forEach(element => {
                        if(element.state){
                            i++
                        }
                    });

                }else{
                    if(data.etat=='3'){
                        i++
                    }
                }
           
                return i
            }

         },
        getSumActionDone(data){
            if(data) {
                let i=0;

                data.forEach(element => {
                    if(element.etat=='3'){
                        i++
                    }
                });
     
      
               return i
            }
  
        },
        getSumActionInProgress(data){
            if(data) {
                let i=0;
                data.forEach(element => {
                    if(element.etat=='2'){
                        i++
                    }
                });
                return i
            }

         },
        getSumActionInProgressAll(data){
            if(data) {
                let progressAll=0
                data.forEach(action => {
              
                    let percentInProgres=0
                    if(action.subActions){
                        action.subActions.forEach(item => {
                            if(item.state){
                        
                              percentInProgres+=item.percent
                      
                            }
                           
                          });
                    }
              
                    if(percentInProgres>0 & percentInProgres<100) {
                        progressAll +=percentInProgres
                    }           
              
                });
                return progressAll
            }

            },
            getSumActionsDoneAll(data){
                if(data) {
                    let doneAll=0
                    data.forEach(action => {
                        // let dateInit =new Date( this.dateFormat(action.dates[0]))
                        // let dateFinal= new Date( this.dateFormat(action.dates[1]))
                            
                        // let nbJoursTotal=Math.ceil(Math.abs(dateFinal-dateInit)/(1000*60*60*24));
                        let percentDone=0
                        if(action.subActions){
                            action.subActions.forEach(item => {
                                if(item.state){
                                //   let dateInitSub =new Date( this.dateFormat(item.dates[0]))
                                //   let dateFinalSub= new Date( this.dateFormat(item.dates[1]))
                                //   nbJours+=Math.ceil(Math.abs(dateFinalSub-dateInitSub)/(1000*60*60*24));
                                    percentDone+=item.percent
                          
                                }
                               
                              });
                        }
            
                        if(percentDone==100) {
                            doneAll+=percentDone
                        }           
                  
                    });
                    return doneAll
                }
 
                },
                getSumActionsNotStartedAll(data){
                    if(data) {
                        let notStartedAll=0
                        data.forEach(action => {
                            // let dateInit =new Date( this.dateFormat(action.dates[0]))
                            // let dateFinal= new Date( this.dateFormat(action.dates[1]))
                                    
                            // let nbJoursTotal=Math.ceil(Math.abs(dateFinal-dateInit)/(1000*60*60*24));
                             let percentNotStarted=0;
                            if(action.subActions){
                                action.subActions.forEach(item => {
                                    if(item.state){
                                        percentNotStarted+=item.percent
                                  
                                    }
                                       
                                  });
                            }
                            if(percentNotStarted==0) {
                                notStartedAll++
                            }           
                          
                        });
                        return notStartedAll
                    }
  
                    },
            getSumActionNotStarted(data){
                if(data) {
                    let i=0;
                    data.forEach(element => {
                        if(element.etat=='1'){
                            i++
                        }
                    });
                    return i
                }
     
                },
            percentActionsDone(data){
                if(data) {
                    let totalDone=this.getSumActionsDoneAll(data)
                    return Math.round(totalDone/data.length) 

                }

            },
            percentActionsInProgress(data){
                if(data) {
                    let totalInProgress=this.getSumActionInProgressAll(data)
                    return Math.round(totalInProgress/data.length) 
    
                }

            },
            percentActionsNotStarted(data){
                if(data) {
                    let totalNotStarted=this.getSumActionsNotStartedAll(data)
                    return Math.round((totalNotStarted*100)/data.length) 
        
                }

            },
          percentSubAction(action){
              if(action.subActions) {
                   // let dateInit =new Date( this.dateFormat(action.dates[0]))
                // let dateFinal= new Date( this.dateFormat(action.dates[1]))
            
                // let nbJoursTotal=Math.ceil(Math.abs(dateFinal-dateInit)/(1000*60*60*24));
                let percentSub=0
                if(action.subActions!=0){
                    action.subActions.forEach(item => {
                        if(item.state){
                          // let dateInitSub =new Date( this.dateFormat(item.dates[0]))
                          // let dateFinalSub= new Date( this.dateFormat(item.dates[1]))
                          // nbJours+=Math.ceil(Math.abs(dateFinalSub-dateInitSub)/(1000*60*60*24));
                          percentSub+=item.percent
          
                        }
               
                      });
                }else{
                    if(action.etat=='3'){
                        percentSub=100
                    }
                }
               
                return Math.round(percentSub)
              }
         
                },

                

          

    },
}