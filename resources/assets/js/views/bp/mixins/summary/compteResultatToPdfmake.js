import pdfMake from "pdfmake/build/pdfmake.min"
import pdfFonts from "pdfmake/build/vfs_fonts"
pdfMake.vfs = pdfFonts.pdfMake.vfs
let  fontsList = import("../../../../pdf/fonts")
import {calculAmortissementImmoCorporelle, calculRemboursement} from "../../../../services/helper"

let format = (ch) => {
    let chh = [];
    let elt = [];
    ch = ch + '';
    let tabs = ch.split('');
    if (ch.length <= 3)
        return ch;
    while ((tabs.length - 1) % 3 !== 0)
        elt.push(tabs.shift());
    tabs.forEach((item, index, tab) => {
        chh.push(item);
        if (index % 3 === 0 && index !== tab.length - 1)
            chh.push(' ');
    });
    return elt.join('') + chh.join('')
};
var money = '';
var precision = '';

export default {

    methods: {
        buildArrayYears: allYears => {

            let yearsHeader = [
                {text: 'Années d\'exercices', style:['bolded','subheader','lefted',], colSpan:5, border:[false,false,false,false]},{},{},
                {},{},
                {
                    text: allYears.annee1, style:['bolded','subheader','centered',], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: allYears.annee2, style:['bolded','subheader','centered',], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: allYears.annee3, style:['bolded','subheader','centered',], colSpan:2, border:[false,false,false,false]
                },{},
            ];
            let result = new Array(yearsHeader);
            return result;
        },
        buildArrayCAE: TTcae => {
            let cae = [
                {text: 'CHIFFRE D’AFFAIRE D\'EXPLOITATION', style:['bolded','subheader','lefted', 'backgroundGreen', 'whiteColor'], colSpan:5, border:[false,false,false,false]},{},{},
                {},{},
                {
                    text: format(parseInt(TTcae.annee1)) +' ' + money, style:['bolded','subheader','centered','backgroundGreen', 'whiteColor'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(TTcae.annee2)) +' ' + money, style:['bolded','subheader','centered','backgroundGreen', 'whiteColor'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(TTcae.annee3)) +' ' + money, style:['bolded','subheader','centered','backgroundGreen', 'whiteColor'], colSpan:2, border:[false,false,false,false]
                },{},
            ];
            let result = new Array(cae);
            return result;
        },
        buildArrayVentes:  (TTcae, articles)  => {
            let ventes = [
                {text: 'Vente d\'articles', style:['bolded','subheader','lefted', 'backgroundLightGreen'], colSpan:5, border:[false,false,false,false]},{},{},
                {},{},
                {
                    text: format(parseInt(TTcae.annee1)) +' ' + money, style:['bolded','subheader','centered', 'backgroundLightGreen'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(TTcae.annee2)) +' ' + money, style:['bolded','subheader','centered', 'backgroundLightGreen'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(TTcae.annee3)) +' ' + money, style:['bolded','subheader','centered', 'backgroundLightGreen'], colSpan:2, border:[false,false,false,false]
                },{},
            ];
            let result = new Array(ventes);
            for(let article of articles) {
                result.push([
                    {text: article.nom, style:['bolded','subheader','lefted'], colSpan:5, border:[false,false,false,false]},{},{},
                    {},{},
                    {
                        text: format(parseInt(article.chiffreAffaire.annee1.ht)) +' ' + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(parseInt(article.chiffreAffaire.annee2.ht)) +' ' + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(parseInt(article.chiffreAffaire.annee3.ht)) +' ' + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                ]);
            }
            return result;
        },
        buildArrayCE: TTcharges => {
            let cae = [
                {text: 'CHARGES D\'EXPLOITATION', style:['bolded','subheader','lefted', 'backgroundRed', 'whiteColor'], colSpan:5, border:[false,false,false,false]},{},{},
                {},{},
                {
                    text: format(parseInt(TTcharges.annee1)) +' ' + money, style:['bolded','subheader','centered','backgroundRed', 'whiteColor'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(TTcharges.annee2)) +' ' + money, style:['bolded','subheader','centered','backgroundRed', 'whiteColor'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(TTcharges.annee3)) +' ' + money, style:['bolded','subheader','centered','backgroundRed', 'whiteColor'], colSpan:2, border:[false,false,false,false]
                },{},
            ];
            let result = new Array(cae);
            return result;
        },
        buildArrayAchats: (TTachats, TTdepenses, depenses) => {
            let achatHeader =  [
                {text: 'Services extérieurs', style:['bolded','subheader','lefted', 'backgroundPink'], colSpan:5, border:[false,false,false,false]},{},{},
                {},{},
                {
                    text: format(parseInt(TTachats.annee1)) +' ' + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(TTachats.annee2)) +' ' + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(TTachats.annee3)) +' ' + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
                },{},
            ];
            let result = new Array(achatHeader);
            result.push([
                {text: 'Dépenses variables liées aux produits', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:5, border:[false,false,false,false]},{},{},
                {},{},
                {
                    text: format(parseInt(TTdepenses.annee1)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(TTdepenses.annee2)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(TTdepenses.annee3)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
            ]);
            for (let depense of depenses) {
                result.push([
                    {text: depense.titre, style:['bolded','subheader','lefted'], colSpan:5, border:[false,false,false,false]},{},{},
                    {},{},
                    {
                        text: format(parseInt(depense.annee1)) +' ' + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(parseInt(depense.annee2)) +' ' + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(parseInt(depense.annee3)) +' ' + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                ]);
            }
            return result;
        },
        buildArraySE: (TTse, services) => {
            let seHeader =  [
                {text: 'Achats', style:['bolded','subheader','lefted', 'backgroundPink'], colSpan:5, border:[false,false,false,false]},{},{},
                {},{},
                {
                    text: format(parseInt(TTse.annee1)) +' ' + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(TTse.annee2)) +' ' + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(TTse.annee3)) +' ' + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
                },{},
            ];
            let result = new Array(seHeader);

            for (let service of services) {
                result.push([
                    {text: service.titre, style:['bolded','subheader','lefted'], colSpan:5, border:[false,false,false,false]},{},{},
                    {},{},
                    {
                        text: format(parseInt(service.annee1)) +' ' + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(parseInt(service.annee2)) +' ' + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(parseInt(service.annee3)) +' ' + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                ]);
            }
            return result;
        },
        buildArrayImpotsTaxes: (TTtaxe, ssTT, taxes) => {
            let taxeHeader =  [
                {text: 'Impôts et Taxes', style:['bolded','subheader','lefted', 'backgroundPink'], colSpan:5, border:[false,false,false,false]},{},{},
                {},{},
                {
                    text: format(parseInt(TTtaxe.annee1)) +' ' + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(TTtaxe.annee2)) +' ' + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(TTtaxe.annee3)) +' ' + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
                },{},
            ];
            let result = new Array(taxeHeader);
            result.push([
                {text: 'Taxes aux Impôts', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:5, border:[false,false,false,false]},{},{},
                {},{},
                {
                    text: format(parseInt(ssTT['autre'].annee1)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(ssTT['autre'].annee2)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(ssTT['autre'].annee3)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
            ]);
            for (let autre of taxes['autre']) {
                result.push([
                    {text: autre.titre, style:['bolded','subheader','lefted'], colSpan:5, border:[false,false,false,false]},{},{},
                    {},{},
                    {
                        text: format(parseInt(autre.annee1)) +' ' + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(parseInt(autre.annee2)) +' ' + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(parseInt(autre.annee3)) +' ' + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                ]);
            }
            result.push([
                {text: 'Taxes de la Mairie', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:5, border:[false,false,false,false]},{},{},
                {},{},
                {
                    text: format(parseInt(ssTT['mairie'].annee1)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(ssTT['mairie'].annee2)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(ssTT['mairie'].annee3)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
            ]);
            for (let mairie of taxes['mairie']) {
                result.push([
                    {text: mairie.titre, style:['bolded','subheader','lefted'], colSpan:5, border:[false,false,false,false]},{},{},
                    {},{},
                    {
                        text: format(parseInt(mairie.annee1)) +' ' + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(parseInt(mairie.annee2)) +' ' + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(parseInt(mairie.annee3)) +' ' + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                ]);
            }
            return result;
        },
        buildArrayFP: (TTfp, ssTT, fps) => {
            let fpHeader =  [
                {text: 'Frais de personnel', style:['bolded','subheader','lefted', 'backgroundPink'], colSpan:5, border:[false,false,false,false]},{},{},
                {},{},
                {
                    text: format(parseInt(TTfp.annee1)) +' ' + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(TTfp.annee2)) +' ' + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(TTfp.annee3)) +' ' + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
                },{},
            ];
            let result = new Array(fpHeader);
            result.push([
                {text: 'Rémunération des salariés', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:5, border:[false,false,false,false]},{},{},
                {},{},
                {
                    text: format(parseInt(ssTT['salaire'].annee1)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(ssTT['salaire'].annee2)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(ssTT['salaire'].annee3)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
            ]);
            for (let salaire of fps['salaire']) {
                result.push([
                    {text: salaire.titre, style:['bolded','subheader','lefted'], colSpan:5, border:[false,false,false,false]},{},{},
                    {},{},
                    {
                        text: format(parseInt(salaire.annee1)) +' ' + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(parseInt(salaire.annee2)) +' ' + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(parseInt(salaire.annee3)) +' ' + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                ]);
            }
            result.push([
                {text: 'Rémunération des stagiaires', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:5, border:[false,false,false,false]},{},{},
                {},{},
                {
                    text: format(parseInt(ssTT['stagiaire'].annee1)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(ssTT['stagiaire'].annee2)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(ssTT['stagiaire'].annee3)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
            ]);
            for (let stagiaire of fps['stagiaire']) {
                result.push([
                    {text: stagiaire.titre, style:['bolded','subheader','lefted'], colSpan:5, border:[false,false,false,false]},{},{},
                    {},{},
                    {
                        text: format(parseInt(stagiaire.annee1)) +' ' + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(parseInt(stagiaire.annee2)) +' ' + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(parseInt(stagiaire.annee3)) +' ' + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                ]);
            }
            result.push([
                {text: 'Charges sociales des employés', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:5, border:[false,false,false,false]},{},{},
                {},{},
                {
                    text: format(parseInt(ssTT['chargeSocialeE'].annee1)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(ssTT['chargeSocialeE'].annee2)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(ssTT['chargeSocialeE'].annee3)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
            ]);
            result.push([
                {text: 'Rémunération des dirigeants', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:5, border:[false,false,false,false]},{},{},
                {},{},
                {
                    text: format(parseInt(ssTT['dirigeant'].annee1)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(ssTT['dirigeant'].annee2)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(ssTT['dirigeant'].annee3)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
            ]);
            for (let dirigeant of fps['dirigeant']) {
                result.push([
                    {text: dirigeant.titre, style:['bolded','subheader','lefted'], colSpan:5, border:[false,false,false,false]},{},{},
                    {},{},
                    {
                        text: format(parseInt(dirigeant.annee1)) +' ' + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(parseInt(dirigeant.annee2)) +' ' + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(parseInt(dirigeant.annee3)) +' ' + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                ]);
            }
            result.push([
                {text: 'Rémunération des dirigeants', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:5, border:[false,false,false,false]},{},{},
                {},{},
                {
                    text: format(parseInt(ssTT['chargeSocialeD'].annee1)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(ssTT['chargeSocialeD'].annee2)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(ssTT['chargeSocialeD'].annee3)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
            ]);
            return result;
        },
        buildArrayDA: (TTda, ssTT, da) => {
            let daHeader =  [
                {text: 'Dotation aux amortissements', style:['bolded','subheader','lefted', 'backgroundPink'], colSpan:5, border:[false,false,false,false]},{},{},
                {},{},
                {
                    text: format(parseInt(TTda.annee1)) +' ' + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(TTda.annee2)) +' ' + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(TTda.annee3)) +' ' + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
                },{},
            ];
            let result = new Array(daHeader);
            result.push([
                {text: 'Matériel informatique', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:5, border:[false,false,false,false]},{},{},
                {},{},
                {
                    text: format(parseInt(ssTT['informatique'].annee1)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(ssTT['informatique'].annee2)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(ssTT['informatique'].annee3)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
            ]);
            for (let informatique of da['informatique']) {
                result.push([
                    {text: informatique.titre, style:['bolded','subheader','lefted'], colSpan:5, border:[false,false,false,false]},{},{},
                    {},{},
                    {
                        text: format(calculAmortissementImmoCorporelle(informatique, precision).annee1) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(calculAmortissementImmoCorporelle(informatique, precision).annee2) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(calculAmortissementImmoCorporelle(informatique, precision).annee3) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                ]);
            }
            result.push([
                {text: 'Mobilier', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:5, border:[false,false,false,false]},{},{},
                {},{},
                {
                    text: format(parseInt(ssTT['mobilier'].annee1)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(ssTT['mobilier'].annee2)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(ssTT['mobilier'].annee3)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
            ]);
            for (let mobilier of da['mobilier']) {
                result.push([
                    {text: mobilier.titre, style:['bolded','subheader','lefted'], colSpan:5, border:[false,false,false,false]},{},{},
                    {},{},
                    {
                        text: format(calculAmortissementImmoCorporelle(mobilier, precision).annee1) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(calculAmortissementImmoCorporelle(mobilier, precision).annee2) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(calculAmortissementImmoCorporelle(mobilier, precision).annee3) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                ]);
            }
            result.push([
                {text: 'Materiel/Machine/Outils', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:5, border:[false,false,false,false]},{},{},
                {},{},
                {
                    text: format(parseInt(ssTT['materiel'].annee1)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(ssTT['materiel'].annee2)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(ssTT['materiel'].annee3)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
            ]);
            for (let materiel of da['materiel']) {
                result.push([
                    {text: materiel.titre, style:['bolded','subheader','lefted'], colSpan:5, border:[false,false,false,false]},{},{},
                    {},{},
                    {
                        text: format(calculAmortissementImmoCorporelle(materiel, precision).annee1) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(calculAmortissementImmoCorporelle(materiel, precision).annee2) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(calculAmortissementImmoCorporelle(materiel, precision).annee3) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                ]);
            }
            result.push([
                {text: 'Batiment/Local/Espace de vente', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:5, border:[false,false,false,false]},{},{},
                {},{},
                {
                    text: format(parseInt(ssTT['batiment'].annee1)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(ssTT['batiment'].annee2)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(ssTT['batiment'].annee3)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
            ]);
            for (let batiment of da['batiment']) {
                result.push([
                    {text: batiment.titre, style:['bolded','subheader','lefted'], colSpan:5, border:[false,false,false,false]},{},{},
                    {},{},
                    {
                        text: format(calculAmortissementImmoCorporelle(batiment, precision).annee1) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(calculAmortissementImmoCorporelle(batiment, precision).annee2) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(calculAmortissementImmoCorporelle(batiment, precision).annee3) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                ]);
            }
            result.push([
                {text: 'Moyen de roulement', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:5, border:[false,false,false,false]},{},{},
                {},{},
                {
                    text: format(parseInt(ssTT['bfr'].annee1)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(ssTT['bfr'].annee2)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(ssTT['bfr'].annee3)) +' ' + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
                },{},
            ]);
            for (let bfr of da['bfr']) {
                result.push([
                    {text: bfr.titre, style:['bolded','subheader','lefted'], colSpan:5, border:[false,false,false,false]},{},{},
                    {},{},
                    {
                        text: format(calculAmortissementImmoCorporelle(bfr, precision).annee1) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(calculAmortissementImmoCorporelle(bfr, precision).annee2) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(calculAmortissementImmoCorporelle(bfr, precision).annee3) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                ]);
            }
            return result;
        },
        buildArrayCF: (TTcf, cf) => {
            let daHeader =  [
                {text: 'Charges financières', style:['bolded','subheader','lefted', 'backgroundPink'], colSpan:5, border:[false,false,false,false]},{},{},
                {},{},
                {
                    text: format(parseInt(TTcf.annee1)) +' ' + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(TTcf.annee2)) +' ' + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(TTcf.annee3)) +' ' + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
                },{},
            ];
            let result = new Array(daHeader);
            //banque
            for (let banque of cf['banque']) {
                result.push([
                    {text: banque.creancier, style:['bolded','subheader','lefted'], colSpan:5, border:[false,false,false,false]},{},{},
                    {},{},
                    {
                        text: format(calculRemboursement(banque, precision).annee1.interet) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(calculRemboursement(banque, precision).annee2.interet) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(calculRemboursement(banque, precision).annee3.interet) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                ]);
            }
            //microfinance
            for (let microfinance of cf['microfinance']) {
                result.push([
                    {text: microfinance.creancier, style:['bolded','subheader','lefted'], colSpan:5, border:[false,false,false,false]},{},{},
                    {},{},
                    {
                        text: format(calculRemboursement(microfinance, precision).annee1.interet) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(calculRemboursement(microfinance, precision).annee2.interet) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(calculRemboursement(microfinance, precision).annee3.interet) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                ]);
            }
            //subvention Banque
            for (let subvention of cf['subventionB']) {
                result.push([
                    {text: subvention.fournisseur, style:['bolded','subheader','lefted'], colSpan:5, border:[false,false,false,false]},{},{},
                    {},{},
                    {
                        text: format(calculRemboursement(subvention, precision).annee1.interet) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(calculRemboursement(subvention, precision).annee2.interet) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(calculRemboursement(subvention, precision).annee3.interet) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                ]);
            }
            //subvention Institution
            for (let subvention of cf['subventionI']) {
                result.push([
                    {text: subvention.fournisseur, style:['bolded','subheader','lefted'], colSpan:5, border:[false,false,false,false]},{},{},
                    {},{},
                    {
                        text: format(calculRemboursement(subvention, precision).annee1.interet) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(calculRemboursement(subvention, precision).annee1.interet) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(calculRemboursement(subvention, precision).annee1.interet) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                ]);
            }
            //subvention Autre
            for (let subvention of cf['subventionA']) {
                result.push([
                    {text: subvention.fournisseur, style:['bolded','subheader','lefted'], colSpan:5, border:[false,false,false,false]},{},{},
                    {},{},
                    {
                        text: format(calculRemboursement(subvention, precision).annee1.interet) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(calculRemboursement(subvention, precision).annee1.interet) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(calculRemboursement(subvention, precision).annee1.interet) + ' '+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                    },{},
                ]);
            }
            return result;
        },
        buildArrayResultat: (item, name) => {

            let color1 = (item.annee1 < 0) ? 'backgroundRed' : 'backgroundGreen';
            let color2 = (item.annee2 < 0) ? 'backgroundRed' : 'backgroundGreen';
            let color3 = (item.annee3 < 0) ? 'backgroundRed' : 'backgroundGreen';
            let resHeader = [
                {text: name.toUpperCase(), style:['bolded','subheader','lefted', 'backgroundGrey', 'whiteColor'], colSpan:5, border:[false,false,false,false]},{},{},{},{},
                {
                    text: format(parseInt(item.annee1)) + ' ' + money, style:['bolded','subheader','centered', color1, 'whiteColor'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(item.annee2)) + ' ' + money, style:['bolded','subheader','centered', color2, 'whiteColor'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: format(parseInt(item.annee3)) + ' ' + money, style:['bolded','subheader','centered', color3, 'whiteColor'], colSpan:2, border:[false,false,false,false]
                },{},
            ];
            let result = new Array(resHeader);
            return result;
        },
        printPdfCompte(projet, years, totaux, elements, paramsMoney, meta, password) {
            // implementation du pdf
            money = paramsMoney['currency'];
            precision = paramsMoney['precision'];
            var docDefinition = {
                info: {
                    title: `${projet.projettitre.toUpperCase()} COMPTE DE RESULTATS`,
                    author: JSON.parse(localStorage.getItem("userSession")).displayName,
                    subject: '',
                    keywords: '',
                    creator: JSON.parse(localStorage.getItem("userSession")).displayName,
                    producer: 'CYBERSCHOOL ENTREPREUNEURIAT',
                },
                userPassword: password,
                ownerPassword: 'cbscodeteam',
                permissions: {
                    printing: 'highResolution', //'lowResolution'
                    modifying: false,
                    copying: false,
                    annotating: true,
                    fillingForms: true,
                    contentAccessibility: true,
                    documentAssembly: true
                },
                background:  function () {
                    return {
                        columns: [{
                            width: '*',
                            stack: [
                                {
                                    image: meta.userLogo,
                                    alignment: 'center',
                                    height: 350,
                                    width: 350,
                                    opacity: 0.08
                                }
                            ],
                            margin:[0, 170,0,0]
                        }]
                    }
                },
                pageOrientation: 'landscape',
                styles: this.styles,
                defaultStyle:   {
                    fontSize: 10,
                    bold: false,
                    alignment: 'justify',
                    font:meta.police?meta.police: "Poppins"
                },
                footer: {
                    columns: [
                        { text: meta.infoJuridiques && meta.infoJuridiques!=""?meta.infoJuridiques:  'CYBERSCHOOL ENTREPREUNEURIAT    RCCM : RG LBV 2018A46528   NIF :395648 A \n' +
                                'Siege Echangeur de Nzeng Ayong /site : www.cyberschoolgabon.com/Tel :061723638 / 062352105 / 066927701/\n' +
                                'Bp :20207/email : contact@cyberschoolgabon.com\n', margin: [90,0,0,0], style: ['small']}
                    ]
                },
                content: [
                    {
                        columns: [
                            ...(meta.userLogo!=''?   [{
                                width: '30%',
                                stack: [
                                    {
                                        image:meta.userLogo,
                                        alignment: 'right',
                                        height: 100,
                                        width: 100
                                    }
                                ],
                                margin:[45, -32,0,0]
                            }] : [] ),
                            ...(meta.logoEntreprise && meta.logoEntreprise!=''?   [{
                                width: '30%',
                                stack: [
                                    {
                                        image:meta.logoEntreprise,
                                        alignment: 'right',
                                        height: 100,
                                        width: 100
                                    }
                                ],
                                margin:[0,-32,55,0]
                            }] : [] ),
                          
                        ],
                        columnGap: 4,
                    },
                    {text:'Compte de Résultats prévisionnels sur 3 ans', style:['bolded', 'bigHeader'], alignment: 'center', margin: [50,15,0,22]},
                    {text:'Projet: '+ projet.projettitre, style: 'header', margin: [50,0,0,15]},
                    { text:'', margin: [0,0,0,25]},
                    {
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArrayYears(years),
                            layout:'noBorders'
                        },margin: [50,0,0,22]
                    },
                    {
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArrayCAE(totaux['cae']),
                            layout:'noBorders'
                        },margin: [50,0,0,22]
                    },
                    {
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArrayVentes(totaux['cae'], elements['articles']),
                            layout:'noBorders'
                        },margin: [50,0,0,22]
                    },
                    {
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArrayCE(totaux['charges']),
                            layout:'noBorders'
                        },margin: [50,0,0,22]
                    },
                    {
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArrayAchats(totaux['achats'], totaux['depensesVariables'], elements['depenses']),
                            layout:'noBorders'
                        },margin: [50,0,0,22]
                    },
                    {
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArraySE(totaux['se'], elements['services']),
                            layout:'noBorders'
                        },margin: [50,0,0,22]
                    },
                    {
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArrayImpotsTaxes(totaux['taxes'], totaux['sTTtaxes'], elements['taxes']),
                            layout:'noBorders'
                        },margin: [50,0,0,22]
                    },
                    {
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArrayFP(totaux['fp'], totaux['sTTfp'], elements['fp']),
                            layout:'noBorders'
                        },margin: [50,0,0,22]
                    },
                    {
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArrayDA(totaux['da'], totaux['sTTda'], elements['da']),
                            layout:'noBorders'
                        },margin: [50,0,0,22]
                    },
                    {
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArrayCF(totaux['cf'], elements['cf']),
                            layout:'noBorders'
                        },margin: [50,0,0,22]
                    },
                    {
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArrayResultat(elements['exploitattion'], 'Résultat d\'exploitation'),
                            layout:'noBorders'
                        },margin: [50,0,0,22]
                    },
                    {
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArrayResultat(elements['imprevus'], "Résultat Exceptionnel (Imprévus)"),
                            layout:'noBorders'
                        },margin: [50,0,0,22]
                    },
                    {
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArrayResultat(elements['courantImpot'], 'Résultat courant avant Impôts'),
                            layout:'noBorders'
                        },margin: [50,0,0,22]
                    },
                    {
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArrayResultat(elements['beneficeImpot'], "Impôt sur les bénéfices"),
                            layout:'noBorders'
                        },margin: [50,0,0,22]
                    },
                    {
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArrayResultat(elements['apresImpot'], "Résultat après impôt"),
                            layout:'noBorders'
                        },margin: [50,0,0,22]
                    },
                    {
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArrayResultat(elements['autofinance'], "Capacité d'autofinancement"),
                            layout:'noBorders'
                        },margin: [50,0,0,22]
                    },
                ]
            };
            fontsList.then((value)=>{
                pdfMake.fonts = value.fonts
                pdfMake.createPdf(docDefinition).download(projet.projettitre.toLowerCase() + '_compte_de_resultats_previsionnels.pdf');
            })
           
        }
    },
    data() {
        return {
            styles: {
                centered:{
                    alignment: 'center'
                },
                lefted:{
                    alignment: 'left'
                },
                righted:{
                    alignment: 'right'
                },
                header: {
                    fontSize: 15,
                    bold: true,
                    alignment: 'justify'
                },
                bigHeader: {
                    fontSize: 18,
                    decoration: 'underline',
                    bold: true,
                    alignment: 'justify'
                },
                subheader: {
                    fontSize: 12,
                    bold: true,
                    alignment: 'justify'
                },
                quote: {
                    italics: true,
                    alignment: 'justify'
                },
                small: {
                    fontSize: 6.5,
                    alignment: 'justify'
                },
                bigger: {
                    fontSize: 13,
                    italics: true,
                    alignment: 'justify'
                },
                tableHeader: {
                    alignment: 'left',
                    bold:true
                },
                bolded: {
                    bold: true
                },
                italiced: {
                    italics: true,
                },
                whiteColor: {
                    color: 'white'
                },
                blackColor: {
                    color: 'black'
                },
                redColor: {
                    color: '#fb3a3a'
                },
                blueColor: {
                    color:'#398bf7'
                },
                backgroundBlueRecap: {
                    fillColor: '#1e58b3'
                },
                backgroundBlueLight: {
                    fillColor: '#d7e1ec'
                },
                backgroundGreen: {
                    fillColor: '#00ba8b'
                },
                backgroundGrey:{
                    fillColor:'#99abb4'
                },
                backgroundRed: {
                    fillColor: '#f13c42'
                },
                backgroundPink: {
                    fillColor: '#f0d4d6'
                },
                backgroundGreyRecap: {
                    fillColor: '#f3f1f1'
                },
                backgroundDarkBlueRecap: {
                    fillColor: '#398bf7'
                },
                backgroundLightBlueRecap: {
                    fillColor: '#2bbbff'
                },
                backgroundLightGreen: {
                    fillColor: '#e1f2de'
                },
                backgroundTableauStandard: {
                    fillColor: '#398bf7'
                }

            }
        }
    }
}
