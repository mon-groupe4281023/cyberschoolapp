import pdfMake from "pdfmake/build/pdfmake.min"
import pdfFonts from "pdfmake/build/vfs_fonts"
import {format} from "../../../../services/helper"

var money = '';
var precision = '';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

export default {

    methods: {
        buildArrayYears: _ => {
            let yearsHeader = [
                {text: 'Années d\'exercices', style:['bolded','subheader','lefted',], colSpan:3, border:[false,false,false,false]},{},{},
                {
                    text: 'Durée', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: 'Année 1', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: 'Année 2', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: 'Année 3', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                },{},
            ];
            return new Array(yearsHeader);
        },
        buildArrayImmoCA: elements => {
            let result = [];
            elements.forEach(element => {
                let style = [];
                if (element.amortissement === '') {
                    style = ['backgroundBlueRecap','bolded','subheader', 'whiteColor'];
                } else {
                    style = ['backgroundBlueLight', 'downHeader']
                }
                result.push([
                    {text: element.titre, style: style.concat(['lefted']), colSpan:3, border:[false,false,false,false]},{},{},
                    {
                        text: element.amortissement, style: style.concat(['centered']), colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(element.annee1) + ' ' + money, style:style.concat(['righted']), colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(element.annee2) + ' ' + money, style:style.concat(['righted']), colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(element.annee3) + ' ' + money, style:style.concat(['righted']), colSpan:2, border:[false,false,false,false]
                    },{}
                ]);
                if (element.children) {
                    for (let child of element.children) {
                        result.push([
                            {text: child.titre, style:['lefted'], colSpan:3, border:[false,false,false,false], margin: [10,0,0,0]},{},{},
                            {text: child.amortissement + ' ans', style:['centered',], colSpan:2, border:[false,false,false,false]
                            },{},
                            {text:  format(child.annee1) + ' ' + money, style:['righted',], colSpan:2, border:[false,false,false,false]
                            },{},
                            {text:  format(child.annee2)  + ' ' + money, style:['righted',], colSpan:2, border:[false,false,false,false]
                            },{},
                            {text:  format(child.annee3)  + ' ' + money, style:['righted',], colSpan:2, border:[false,false,false,false]
                            },{}
                        ]);
                    }
                }
            });
            return result;
        },
        printPdfAmortissemnt(projet, paramsMoney, elements, images, password){
            // implementation du pdf
            money = paramsMoney['currency'];
            precision = paramsMoney['precision'];
            var docDefinition = {
                info: {
                    title: projet.projettitre.toUpperCase() +' AMORTISSEMENTS (HT) SUR 3 ANS',
                    author: JSON.parse(localStorage.getItem("userSession")).displayName,
                    subject: '',
                    keywords: '',
                    creator: JSON.parse(localStorage.getItem("userSession")).displayName,
                    producer: 'CYBERSCHOOL ENTREPREUNEURIAT',
                },
                userPassword: password,
                ownerPassword: 'cbscodeteam',
                permissions: {
                    printing: 'highResolution', //'lowResolution'
                    modifying: false,
                    copying: false,
                    annotating: true,
                    fillingForms: true,
                    contentAccessibility: true,
                    documentAssembly: true
                },
                background:  function () {
                    return {
                        columns: [{
                            width: '*',
                            stack: [
                                {
                                    image: images.logoBp,
                                    alignment: 'center',
                                    height: 290,
                                    width: 430,
                                    opacity: 0.08
                                }
                            ],
                            margin:[0, 170,0,0]
                        }]
                    }
                },
                pageOrientation: 'landscape',
                styles: this.styles,
                defaultStyle: this.defaultStyle,
                footer: {
                    columns: [
                        { text: 'CYBERSCHOOL ENTREPREUNEURIAT    RCCM : RG LBV 2018A46528   NIF :395648 A \n' +
                                'Siege Echangeur de Nzeng Ayong /site : www.cyberschoolgabon.com/Tel :061723638 / 062352105 / 066927701/\n' +
                                'Bp :20207/email : contact@cyberschoolgabon.com\n', margin: [90,0,0,0], style: ['small']}
                    ]
                },
                content: [
                    {
                        columns: [
                            {
                                width: '*',
                                stack: [
                                    {
                                        image: images.userLogo,
                                        alignment: 'left',
                                        height: 110,
                                        width: 110
                                    }
                                ],
                                margin:[45, -32,0,0]
                            },
                            {
                                width: '30%',
                                stack: [
                                    {
                                        image: images.logoBp,
                                        alignment: 'right',
                                        height: 100,
                                        width: 150
                                    }
                                ],
                                margin:[0,-32,55,0]
                            }
                        ],
                        columnGap: 4,
                    },
                    { text: 'Amortissements (HT) sur 3 ans',
                        style:['bolded', 'bigHeader'], alignment: 'center', margin: [50,30,0,22]},
                    {text:'Projet : '+ projet.projettitre, style: 'header',margin: [50,0,0,15]},
                    { text:'', margin: [0,0,0,25]},
                    {
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArrayYears(),
                            layout:'noBorders'
                        },margin: [50,0,0,22]
                    },
                    {
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArrayImmoCA(elements),
                            layout:'noBorders'
                        },margin: [50,0,0,22]
                    }
                ]
            };
            pdfMake.createPdf(docDefinition).download(projet.projettitre.toLowerCase() +'_amortissements.pdf');
        }
    },
    data() {
        return {
            styles: {
                centered:{
                    alignment: 'center'
                },
                lefted:{
                    alignment: 'left'
                },
                righted:{
                    alignment: 'right'
                },
                downHeader: {
                    fontSize: 10.8
                },
                bigHeader: {
                    fontSize: 18,
                    bold: true,
                    decoration: 'underline',
                    alignment: 'justify'
                },
                subheader: {
                    fontSize: 12,
                    alignment: 'justify'
                },
                quote: {
                    italics: true,
                    alignment: 'justify'
                },
                small: {
                    fontSize: 6.5,
                    alignment: 'justify'
                },
                bigger: {
                    fontSize: 13,
                    italics: true,
                    alignment: 'justify'
                },
                tableHeader: {
                    alignment: 'left',
                    bold:true
                },
                bolded: {
                    bold: true
                },
                italiced: {
                    italics: true,
                },
                whiteColor: {
                    color: 'white'
                },
                blackColor: {
                    color: 'black'
                },
                redColor: {
                    color: '#fb3a3a'
                },
                backgroundBlueRecap: {
                    fillColor: '#4c8caa'
                },
                backgroundBlueLight: {
                    fillColor: '#91bdd1'
                },
                backgroundGreen: {
                    fillColor: '#00ba8b'
                },
                backgroundGrey:{
                    fillColor:'#99abb4'
                },
                backgroundRed: {
                    fillColor: '#f13c42'
                },
                backgroundPink: {
                    fillColor: '#f0d4d6'
                },
                backgroundGreyRecap: {
                    fillColor: '#f3f1f1'
                },
                backgroundDarkBlueRecap: {
                    fillColor: '#398bf7'
                },
                backgroundLightBlueRecap: {
                    fillColor: '#2bbbff'
                },
                backgroundLightGreen: {
                    fillColor: '#e1f2de'
                },
                backgroundTableauStandard: {
                    fillColor: '#398bf7'
                }

            },
            defaultStyle:  {
                fontSize: 10,
                bold: false,
                alignment: 'justify'
            },
        }
    }
}
