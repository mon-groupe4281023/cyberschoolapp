// assets/js/views/bp/mixins/summary/functions
export default {
    methods: {
        // calcul du total de ressource
        calculTotalRessource(arrayRessources){
            let total = 0;
            arrayRessources.forEach(ressource => {
                total = total+ressource.cout;
            });
            return total;
        },
        // calcul du total de Besoin
        calculTotalBesoin(arrayBesoins){
            let total = 0;
            arrayBesoins.forEach(besoin => {
                total = total+besoin.cout;
            });
            return total;
        },
        // calcul du total des ventes
        calculTotalVente(arrayVente){
            let totalMensuel = 0;
            let totalAnnuel = 0;
            arrayVente.forEach(vente => {
                totalMensuel = totalMensuel + vente.ptotal;
                totalAnnuel = totalAnnuel + vente.ptotVenteAnnuel;
            });
            return { 'mensuel': totalMensuel, 'annuel': totalAnnuel}
        },
        // calcul du total des charges
        calculTotalCharge(arrayCharges){
            let totalMensuel = 0;
            let totalAnnuel = 0;
            arrayCharges.forEach(charge => {
                totalMensuel = totalMensuel + charge.ptotal;
                totalAnnuel = totalAnnuel + charge.ptotChargeAnnuel;
            });
            return { 'mensuel': totalMensuel, 'annuel': totalAnnuel}
        },
    },
}
