import pdfMake from "pdfmake/build/pdfmake.min"
import pdfFonts from "pdfmake/build/vfs_fonts"
import {format, arround} from "../../../../services/helper"

var money = '';
var precision = '';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

export default {

    methods: {
        buildArrayYears: _ => {
            let yearsHeader = [
                {text: 'Années d\'exercices', style:['bolded','subheader','lefted',], colSpan:4, border:[false,false,false,false]},{},{},{},
                {
                    text: 'Année 1', style:['bolded','subheader','centered',], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: 'Année 2', style:['bolded','subheader','centered',], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: 'Année 3', style:['bolded','subheader','centered',], colSpan:2, border:[false,false,false,false]
                },{},
            ];
            return new Array(yearsHeader);
        },
        buildArrayBfr: elements => {
            let result = [];
            elements.forEach(element => {
                result.push([
                    {text: element.titre, style:['bolded','subheader','lefted', 'backgroundPink', 'whiteColor'], colSpan:4, border:[false,false,false,false]},{},{},{},
                    {
                        text: format(arround(element.annee1, precision)) + ' ' + money, style:['bolded','subheader','righted', 'backgroundPink', 'whiteColor'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: (element.annee2 === '-') ? '-' : format(arround(element.annee2, precision)) + ' ' + money, style:['bolded','subheader',(element.annee2 === '-') ? 'centered' : 'righted', 'backgroundPink', 'whiteColor'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: (element.annee3 === '-') ? '-' : format(arround(element.annee3, precision)) + ' ' + money, style:['bolded','subheader', (element.annee2 === '-') ? 'centered' : 'righted', 'backgroundPink', 'whiteColor'], colSpan:2, border:[false,false,false,false]
                    },{},
                ]);
                if (element.children) {
                    for (let child of element.children) {
                        result.push([
                            {text: child.titre, style:[ (child.group) ? '' : 'bolded','lefted', (child.group) ? '' : 'backgroundPinkLigth'], colSpan:4, border:[false,false,false,false]},{},{},{},
                            {
                                text: format(arround(child.annee1, precision)) + ' ' + money, style:['bolded','righted', (child.group) ? '' : 'backgroundPinkLigth'], colSpan:2, border:[false,false,false,false]
                            },{},
                            {
                                text: (child.annee2 === '-') ? '-' : format(arround(child.annee2, precision)) + ' ' + money, style:['bolded',(child.annee2 === '-') ? 'centered' : 'righted', (child.group) ? '' : 'backgroundPinkLigth'], colSpan:2, border:[false,false,false,false]
                            },{},
                            {
                                text: (child.annee3 === '-') ? '-' : format(arround(child.annee3, precision)) + ' ' + money, style:['bolded', (child.annee2 === '-') ? 'centered' : 'righted', (child.group) ? '' : 'backgroundPinkLigth'], colSpan:2, border:[false,false,false,false]
                            },{},
                        ]);
                        if (child.children) {
                            /**
                             * sChild petit fils de element
                             */
                            for (let sChild of child.children) {
                                result.push([
                                    {text: sChild.titre, style:['lefted',], colSpan:4, border:[false,false,false,false]},{},{},{},
                                    {
                                        text: format(arround(sChild.annee1, precision)) + ' ' + money, style:['righted',], colSpan:2, border:[false,false,false,false]
                                    },{},
                                    {
                                        text: format(arround(sChild.annee2, precision)) + ' ' + money, style:['righted',], colSpan:2, border:[false,false,false,false]
                                    },{},
                                    {
                                        text: format(arround(sChild.annee3, precision)) + ' ' + money, style:['righted',], colSpan:2, border:[false,false,false,false]
                                    },{},
                                ]);
                            }
                        }
                    }
                }
            });
            return result;
        },
        printPdfBfr(projet, paramsMoney, elements, images, password){
            // implementation du pdf
            money = paramsMoney['currency'];
            precision = paramsMoney['precision'];
            var docDefinition = {
                info: {
                    title: projet.projettitre.toUpperCase() + ' BESOINS EN FONDS DE ROULEMENT (HT) SUR 3 ANS',
                    author: JSON.parse(localStorage.getItem("userSession")).displayName,
                    subject: '',
                    keywords: '',
                    creator: JSON.parse(localStorage.getItem("userSession")).displayName,
                    producer: 'CYBERSCHOOL ENTREPREUNEURIAT',
                },
                userPassword: password,
                ownerPassword: 'cbscodeteam',
                permissions: {
                    printing: 'highResolution', //'lowResolution'
                    modifying: false,
                    copying: false,
                    annotating: true,
                    fillingForms: true,
                    contentAccessibility: true,
                    documentAssembly: true
                },
                background:  function () {
                    return {
                        columns: [{
                            width: '*',
                            stack: [
                                {
                                    image: images.logoBp,
                                    alignment: 'center',
                                    height: 290,
                                    width: 430,
                                    opacity: 0.08
                                }
                            ],
                            margin:[0, 170,0,0]
                        }]
                    }
                },
                pageOrientation: 'landscape',
                styles: this.styles,
                defaultStyle: this.defaultStyle,
                footer: {
                    columns: [
                        { text: 'CYBERSCHOOL ENTREPREUNEURIAT    RCCM : RG LBV 2018A46528   NIF :395648 A \n' +
                                'Siege Echangeur de Nzeng Ayong /site : www.cyberschoolgabon.com/Tel :061723638 / 062352105 / 066927701/\n' +
                                'Bp :20207/email : contact@cyberschoolgabon.com\n', margin: [90,0,0,0], style: ['small']}
                    ]
                },
                content: [
                    {
                        columns: [
                            {
                                width: '*',
                                stack: [
                                    {
                                        image: images.userLogo,
                                        alignment: 'left',
                                        height: 110,
                                        width: 110
                                    }
                                ],
                                margin:[45, -32,0,0]
                            },
                            {
                                width: '30%',
                                stack: [
                                    {
                                        image: images.logoBp,
                                        alignment: 'right',
                                        height: 100,
                                        width: 150
                                    }
                                ],
                                margin:[0,-32,55,0]
                            }
                        ],
                        columnGap: 4,
                    },
                    { text: 'Besoins en fonds de roulement (HT) sur 3 ans',
                        style:['bolded', 'bigHeader'], alignment: 'center', margin: [50,30,0,22]},
                    {text:'Projet : '+ projet.projettitre, style: 'header',margin: [50,0,0,15]},
                    { text:'', margin: [0,0,0,25]},
                    {
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArrayYears(),
                            layout:'noBorders'
                        },margin: [50,0,0,22]
                    },
                    {
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArrayBfr(elements),
                            layout:'noBorders'
                        },margin: [50,0,0,22]
                    },
                ]
            };
            pdfMake.createPdf(docDefinition).download(projet.projettitre.toLowerCase() +'_besoins_fonds_roulement.pdf');
        }
    },
    data() {
        return {
            styles: {
                centered:{
                    alignment: 'center'
                },
                lefted:{
                    alignment: 'left'
                },
                righted:{
                    alignment: 'right'
                },
                header: {
                    fontSize: 15,
                    bold: true,
                    alignment: 'justify'
                },
                bigHeader: {
                    fontSize: 18,
                    bold: true,
                    decoration: 'underline',
                    alignment: 'center'
                },
                subheader: {
                    fontSize: 12,
                    bold: true,
                    alignment: 'justify'
                },
                quote: {
                    italics: true,
                    alignment: 'justify'
                },
                small: {
                    fontSize: 6.5,
                    alignment: 'justify'
                },
                bigger: {
                    fontSize: 13,
                    italics: true,
                    alignment: 'justify'
                },
                tableHeader: {
                    alignment: 'left',
                    bold:true
                },
                bolded: {
                    bold: true
                },
                italiced: {
                    italics: true,
                },
                whiteColor: {
                    color: 'white'
                },
                childColor: {
                    color: '#67757c'
                },
                blackColor: {
                    color: 'black'
                },
                redColor: {
                    color: '#fb3a3a'
                },
                blueColor: {
                    color:'#398bf7'
                },
                backgroundBlueRecap: {
                    fillColor: '#1e58b3'
                },
                backgroundBlueLight: {
                    fillColor: '#d7e1ec'
                },
                backgroundGreen: {
                    fillColor: '#00ba8b'
                },
                backgroundGrey:{
                    fillColor:'#99abb4'
                },
                backgroundPinkLigth: {
                    fillColor: '#f4b3cb'
                },
                backgroundPink: {
                    fillColor: '#d43d75'
                },
                backgroundGreyRecap: {
                    fillColor: '#f3f1f1'
                },
                backgroundDarkBlueRecap: {
                    fillColor: '#398bf7'
                },
                backgroundLightBlueRecap: {
                    fillColor: '#2bbbff'
                },
                backgroundLightGreen: {
                    fillColor: '#e1f2de'
                },
                backgroundTableauStandard: {
                    fillColor: '#398bf7'
                }

            },
            defaultStyle:  {
                fontSize: 10,
                bold: false,
                alignment: 'justify'
            },
        }
    }
}
