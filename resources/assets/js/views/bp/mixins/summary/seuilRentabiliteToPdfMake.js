import pdfMake from "pdfmake/build/pdfmake.min"
import {arround, format} from "../../../../services/helper"
import pdfFonts from "pdfmake/build/vfs_fonts"

var money = '';
var precision = '';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

let getDataType =  (annee, type) => {

    if (type === 'point') {
        annee  = format(arround(annee, precision)) + ' MOIS';
    } else if (type === 'tauxMargeV') {
        annee  = format(arround(annee, precision)) + ' %';
    } else {
        annee  = format(arround(annee, precision)) + ' ' + money;
    }
    return annee
};

export default {

    methods: {
        buildArrayYears: _ => {

            let yearsHeader = [
                {text: 'Années d\'exercices', style:['bolded','subheader','lefted',], colSpan:3, border:[false,false,false,false]},{},{},
                {
                    text: 'annee1', style:['bolded','subheader','centered',], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: 'annee2', style:['bolded','subheader','centered',], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: 'annee3', style:['bolded','subheader','centered',], colSpan:2, border:[false,false,false,false]
                },{},
            ];
            let result = new Array(yearsHeader);
            return result;
        },
        buildArrayRentabilite: rentabilites => {
            let result = [];
            for (let rentabilite in rentabilites) {

                result.push([
                    {text: rentabilites[rentabilite].titre, style:['bolded','subheader','lefted', 'backgroundPurple', 'whiteColor'], colSpan:3, border:[false,false,false,false]},{},{},
                    {
                        text: getDataType(rentabilites[rentabilite].annee1, rentabilite), style:['bolded','subheader','righted', 'backgroundPurple', 'whiteColor'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: (rentabilite !== 'point') ? getDataType(rentabilites[rentabilite].annee1, rentabilite) : '', style:['bolded','subheader','righted', 'backgroundPurple', 'whiteColor'], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: (rentabilite !== 'point') ? getDataType(rentabilites[rentabilite].annee1, rentabilite) : '', style:['bolded','subheader','righted', 'backgroundPurple', 'whiteColor'], colSpan:2, border:[false,false,false,false]
                    },{},
                ]);
                if (rentabilites[rentabilite].children) {
                    let childs = rentabilites[rentabilite].children;
                    for (let child in childs) {
                        result.push([
                            {text: childs[child].titre, style:['lefted',], colSpan:3, border:[false,false,false,false]},{},
                            {},{
                                text: format(arround(childs[child].annee1, precision)) + ' ' + money, style:['righted',], colSpan:2, border:[false,false,false,false]
                            },{},
                            {
                                text: format(arround(childs[child].annee2, precision)) + ' ' + money, style:['righted',], colSpan:2, border:[false,false,false,false]
                            },{},
                            {
                                text: format(arround(childs[child].annee3, precision)) + ' ' + money, style:['righted',], colSpan:2, border:[false,false,false,false]
                            },{},
                        ]);
                    }
                }
                result.push([
                    {text: '', margin: [0,0,0,4], colSpan:3, border:[false,false,false,false]},{},{},
                    {text: '', margin: [0,0,0,4], colSpan:2, border:[false,false,false,false]},{},
                    {text: '', margin: [0,0,0,4], colSpan:2, border:[false,false,false,false]},{},
                    {text: '', margin: [0,0,0,4], colSpan:2, border:[false,false,false,false]},{},
                ]);
            }

            return result;
        },
        printPdfSeuil(projet, rentabilite, params, images, password){
            // implementation du pdf
            money = params['currency'];
            precision = params['precision'];
            var docDefinition = {
                info: {
                    title: projet.projettitre.toUpperCase() + ' Seuil de Rentabilité',
                    author: JSON.parse(localStorage.getItem("userSession")).displayName,
                    subject: '',
                    keywords: '',
                    creator: JSON.parse(localStorage.getItem("userSession")).displayName,
                    producer: 'CYBERSCHOOL ENTREPREUNEURIAT',
                },
                userPassword: password,
                ownerPassword: 'cbscodeteam',
                permissions: {
                    printing: 'highResolution', //'lowResolution'
                    modifying: false,
                    copying: false,
                    annotating: true,
                    fillingForms: true,
                    contentAccessibility: true,
                    documentAssembly: true
                },
                background:  function () {
                    return {
                        columns: [{
                            width: '*',
                            stack: [
                                {
                                    image: images.logoBp,
                                    alignment: 'center',
                                    height: 290,
                                    width: 430,
                                    opacity: 0.08
                                }
                            ],
                            margin:[0, 170,0,0]
                        }]
                    }
                },
                pageOrientation: 'landscape',
                styles: this.styles,
                defaultStyle: this.defaultStyle,
                footer: {
                    columns: [
                        { text: 'CYBERSCHOOL ENTREPREUNEURIAT    RCCM : RG LBV 2018A46528   NIF :395648 A \n' +
                                'Siege Echangeur de Nzeng Ayong /site : www.cyberschoolgabon.com/Tel :061723638 / 062352105 / 066927701/\n' +
                                'Bp :20207/email : contact@cyberschoolgabon.com\n', margin: [55,0,0,0], style: ['small']}
                    ]
                },
                content: [
                    // en-tete du doc
                    {
                        columns: [
                            {
                                width: '*',
                                stack: [
                                    {
                                        image: images.userLogo,
                                        alignment: 'left',
                                        height: 110,
                                        width: 110
                                    }
                                ],
                                margin:[15, -32,0,0]
                            },
                            {
                                width: '30%',
                                stack: [
                                    {
                                        image: images.logoBp,
                                        alignment: 'right',
                                        height: 100,
                                        width: 150
                                    }
                                ],
                                margin:[0,-32, 0,0]
                            }
                        ],
                        columnGap: 4,
                    },
                    ,
                    { text: 'Seuil de rentabilité et point mort',
                        style:['bolded', 'bigHeader'], alignment: 'center', margin: [10,30,0,22]},
                    {text:'Projet : '+ projet.projettitre, style: 'header',margin: [10,0,0,15]},
                    { text:'', margin: [10,0,0,25]},
                    {
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArrayYears(),
                            layout:'noBorders'
                        },margin: [10,0,0,22]
                    },{
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArrayRentabilite(rentabilite),
                            layout:'noBorders'
                        },margin: [10,0,0,22]
                    },
                ]
            };
            pdfMake.createPdf(docDefinition).download(projet.projettitre.toLowerCase() +'_seuil_de_rentabilite.pdf');
        }
    },
    data() {
        return {
            styles: {
                centered:{
                    alignment: 'center'
                },
                righted:{
                    alignment: 'right'
                },
                header: {
                    fontSize: 15,
                    bold: true,
                    alignment: 'justify'
                },
                bigHeader: {
                    fontSize: 18,
                    bold: true,
                    decoration: 'underline',
                    alignment: 'justify'
                },
                subheader: {
                    fontSize: 12,
                    bold: true,
                },
                quote: {
                    italics: true,
                    alignment: 'justify'
                },
                small: {
                    fontSize: 6.5,
                    alignment: 'justify'
                },
                bigger: {
                    fontSize: 13,
                    italics: true,
                    alignment: 'justify'
                },
                tableHeader: {
                    alignment: 'left',
                    bold:true
                },
                bolded: {
                    bold: true
                },
                italiced: {
                    italics: true,
                },
                whiteColor: {
                    color: 'white'
                },
                blackColor: {
                    color: 'black'
                },
                redColor: {
                    color: '#fb3a3a'
                },
                blueColor: {
                    color:'#398bf7'
                },
                backgroundBlueRecap: {
                    fillColor: '#1e58b3'
                },
                backgroundBlueLight: {
                    fillColor: '#d7e1ec'
                },
                backgroundGreen: {
                    fillColor: '#00ba8b'
                },
                backgroundPurple:{
                    fillColor:'#875faf'
                },
                backgroundRed: {
                    fillColor: '#f13c42'
                },
                backgroundPink: {
                    fillColor: '#f0d4d6'
                },
                backgroundGreyRecap: {
                    fillColor: '#f3f1f1'
                },
                backgroundDarkBlueRecap: {
                    fillColor: '#398bf7'
                },
                backgroundLightBlueRecap: {
                    fillColor: '#2bbbff'
                },
                backgroundLibelleRecap: {
                    // fillColor: '#f3f1f1',
                    fillColor: '#ded9d9'
                },
                backgroundTableauStandard: {
                    fillColor: '#398bf7'
                }

            },
            defaultStyle:  {
                fontSize: 10,
                bold: false,
                alignment: 'justify'
            },
        }
    }
}
