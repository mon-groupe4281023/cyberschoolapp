import pdfMake from "pdfmake/build/pdfmake.min"
import pdfFonts from "pdfmake/build/vfs_fonts"
import {arround, format} from "../../../../services/helper"

pdfMake.vfs = pdfFonts.pdfMake.vfs;
var money = '';
var precision = '';

export default {

    methods: {
        buildArrayYears: years => {

            let yearsHeader = [
                {text: 'Années d\'exercices', style:['bolded','subheader','lefted',], colSpan:3, border:[false,false,false,false]},{},{},
                {
                    text: years.annee1, style:['bolded','subheader','centered',], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: years.annee2, style:['bolded','subheader','centered',], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: years.annee3, style:['bolded','subheader','centered',], colSpan:2, border:[false,false,false,false]
                },{},
            ];
            let result = new Array(yearsHeader);
            return result;
        },
        buildArraySoldeIG: soldes => {
            let result = [];
            for (let solde in soldes) {
                result.push([
                    {text: soldes[solde].titre, style:['bolded','subheader','lefted', 'backgroundBlueRecap', 'whiteColor'], colSpan:3, border:[false,false,false,false]},{},{},
                    {
                        text: format(arround(soldes[solde].annee1, precision)) + ' ' + money, style:['backgroundBlueRecap', 'whiteColor', 'bolded','subheader','righted',], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(arround(soldes[solde].annee2, precision)) + ' ' + money, style:['backgroundBlueRecap', 'whiteColor', 'bolded','subheader','righted',], colSpan:2, border:[false,false,false,false]
                    },{},
                    {
                        text: format(arround(soldes[solde].annee3, precision)) + ' ' + money, style:['backgroundBlueRecap', 'whiteColor', 'bolded','subheader','righted',], colSpan:2, border:[false,false,false,false]
                    },{},
                ]);
                let childs = soldes[solde].children;

                for (let child in childs) {
                    result.push([
                        {text: childs[child].titre, style:['lefted',], colSpan:3, border:[false,false,false,false]},{},
                        {},{
                            text: format(arround(childs[child].annee1, precision)) + ' ' + money, style:['righted',], colSpan:2, border:[false,false,false,false]
                        },{},
                        {
                            text: format(arround(childs[child].annee2, precision)) + ' ' + money, style:['righted',], colSpan:2, border:[false,false,false,false]
                        },{},
                        {
                            text: format(arround(childs[child].annee3, precision)) + ' ' + money, style:['righted',], colSpan:2, border:[false,false,false,false]
                        },{},
                    ]);
                }
            }
            return result;
        },
        printPdfSolde(projet, soldes, params, years, images, password){
            // implementation du pdf
            money = params['currency'];
            precision = params['precision'];
            var docDefinition = {
                info: {
                    title: projet.projettitre.toUpperCase() + ' SOLDE INTERMEDIAIRE DE GESTION',
                    author: JSON.parse(localStorage.getItem("userSession")).displayName,
                    subject: '',
                    keywords: '',
                    creator: JSON.parse(localStorage.getItem("userSession")).displayName,
                    producer: 'CYBERSCHOOL ENTREPREUNEURIAT',

                },
                userPassword: password,
                ownerPassword: 'cbscodeteam',
                permissions: {
                    printing: 'highResolution', //'lowResolution'
                    modifying: false,
                    copying: false,
                    annotating: true,
                    fillingForms: true,
                    contentAccessibility: true,
                    documentAssembly: true
                },
                background:  function (currentPage, pageSize) {
                    return {
                        columns: [{
                            width: '*',
                            stack: [
                                {
                                    image: images.logoBp,
                                    alignment: 'center',
                                    height: 290,
                                    width: 430,
                                    opacity: 0.08
                                }
                            ],
                            margin:[0, 170,0,0]
                        }]
                    }
                },
                pageOrientation: 'landscape',
                styles: this.styles,
                defaultStyle: this.defaultStyle,
                footer: {
                    columns: [
                        { text: 'CYBERSCHOOL ENTREPREUNEURIAT    RCCM : RG LBV 2018A46528   NIF :395648 A \n' +
                                'Siege Echangeur de Nzeng Ayong /site : www.cyberschoolgabon.com/Tel :061723638 / 062352105 / 066927701/\n' +
                                'Bp :20207/email : contact@cyberschoolgabon.com\n', margin: [55,0,0,0], style: ['small']}
                    ]
                },
                content: [
                    // en-tete du doc
                    {
                        columns: [
                            {
                                width: '*',
                                stack: [
                                    {
                                        image: images.userLogo,
                                        alignment: 'left',
                                        height: 110,
                                        width: 110
                                    }
                                ],
                                margin:[15, -32,0,0]
                            },
                            {
                                width: '30%',
                                stack: [
                                    {
                                        image: images.logoBp,
                                        alignment: 'right',
                                        height: 100,
                                        width: 150
                                    }
                                ],
                                margin:[0,-32, 0,0]
                            }
                        ],
                        columnGap: 4,
                    },
                    { text: 'Solde imtermédiaire de gestion',
                        style:['bolded', 'bigHeader'], alignment: 'center', margin: [10,30,0,22]},
                    {text:'Projet : '+ projet.projettitre, style: 'header',margin: [10,0,0,15]},
                    { text:'', margin: [0,0,0,25]},
                    {
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArrayYears(years),
                            layout:'noBorders'
                        },margin: [10,0,0,22]
                    },{
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArraySoldeIG(soldes),
                            layout:'noBorders'
                        },margin: [10,0,0,22]
                    },
                ]
            };
            pdfMake.createPdf(docDefinition).download(projet.projettitre.toLowerCase() +'_solde_intermediaire_gestion.pdf');
        }
    },
    data() {
        return {
            styles: {
                centered:{
                    alignment: 'center'
                },
                righted:{
                    alignment: 'right'
                },
                header: {
                    fontSize: 15,
                    bold: true,
                    alignment: 'justify'
                },
                bigHeader: {
                    fontSize: 18,
                    bold: true,
                    decoration: 'underline',
                    alignment: 'center'
                },
                subheader: {
                    fontSize: 12,
                    bold: true,
                },
                quote: {
                    italics: true,
                    alignment: 'justify'
                },
                small: {
                    fontSize: 6.5,
                    alignment: 'justify'
                },
                bigger: {
                    fontSize: 13,
                    italics: true,
                    alignment: 'justify'
                },
                tableHeader: {
                    alignment: 'left',
                    bold:true
                },
                bolded: {
                    bold: true
                },
                italiced: {
                    italics: true,
                },
                whiteColor: {
                    color: 'white'
                },
                backgroundBlueRecap: {
                    fillColor: '#36a2eb'
                },
            },
            defaultStyle:  {
                fontSize: 10,
                bold: false,
                alignment: 'justify'
            },
        }
    }
}

