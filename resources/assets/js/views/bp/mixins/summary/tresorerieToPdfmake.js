import pdfMake from "pdfmake/build/pdfmake.min"
import pdfFonts from "pdfmake/build/vfs_fonts"
import {arround, format, deleteSpace} from "../../../../services/helper"

var money = '';
var precision = '';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

export default {

    methods: {
        buildArrayYear: _ => {
            let yearsHeader = [
                {text: 'Annee 1', style:['bolded','light','lefted'], border:[false,false,false,false], colSpan:2},{},
                {text: 'INITIAL', style:['bolded','light','centered'], border:[false,false,false,false]},
                {text: 'JAN', style:['bolded','light','centered'], border:[false,false,false,false]},
                {text: 'FEV', style:['bolded','light','centered'], border:[false,false,false,false]},
                {text: 'MAR', style:['bolded','light','centered'], border:[false,false,false,false]},
                {text: 'AVR', style:['bolded','light','centered'], border:[false,false,false,false]},
                {text: 'MAI', style:['bolded','light','centered'], border:[false,false,false,false]},
                {text: 'JUN', style:['bolded','light','centered'], border:[false,false,false,false]},
            ];
            let result = new Array(yearsHeader);
            return result;
        },
        buildArrayYear1: _ => {
            let yearsHeader = [
                {text: 'Annee 1', style:['bolded','light','lefted'], border:[false,false,false,false], colSpan:2},{},
                {text: 'JUL', style:['bolded','light','centered'], border:[false,false,false,false]},
                {text: 'AOU', style:['bolded','light','centered'], border:[false,false,false,false]},
                {text: 'SEPT', style:['bolded','light','centered'], border:[false,false,false,false]},
                {text: 'OCT', style:['bolded','light','centered'], border:[false,false,false,false]},
                {text: 'NOV', style:['bolded','light','centered'], border:[false,false,false,false]},
                {text: 'DEC', style:['bolded','light','centered'], border:[false,false,false,false]},
                {text: 'TOTAL', style:['bolded','light','centered'], border:[false,false,false,false]},
            ];
            let result = new Array(yearsHeader);
            return result;
        },
        buildArrayTresoSem1: tresos => {
            let result;
            result = [];
            for (let treso of tresos) {
                let colorHeader = 'backgroundGrey';
                let color = '';
                let cc = '';
                let colorMFC = 'backgroundGrey';
                if (treso.titre === 'Décaissements' || treso.titre === 'Encaissements'){
                    color = cc = 'whiteColor';
                    colorMFC = (treso.titre === 'Décaissements') ? 'backgroundRed': 'backgroundGreen';
                    colorHeader = (treso.titre === 'Décaissements') ? 'backgroundRed': 'backgroundGreen';
                }
                if (treso.tag === 'resultat') {
                    colorMFC = '';
                    cc = 'whiteColor';
                }
                result.push([
                    {text: treso.titre, style:['bolded','light', colorHeader, 'lefted', color], border:[false,false,false,false], colSpan:2},{},
                    {text: format(arround(treso.initial, precision)) + ' ' + money, style:['bolded','light', (treso.initial<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', 'righted', cc, colorMFC], border:[false,false,false,false]},
                    {text: format(arround(treso.mois1, precision)) + ' ' + money, style:['bolded','light', (treso.mois1<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', colorMFC, 'righted', cc], border:[false,false,false,false]},
                    {text: format(arround(treso.mois2, precision)) + ' ' + money, style:['bolded','light', (treso.mois2<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', colorMFC, 'righted', cc], border:[false,false,false,false]},
                    {text: format(arround(treso.mois3, precision)) + ' ' + money, style:['bolded','light', (treso.mois3<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', colorMFC, 'righted', cc], border:[false,false,false,false]},
                    {text: format(arround(treso.mois4, precision)) + ' ' + money, style:['bolded','light',(treso.mois4<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', colorMFC, 'righted', cc], border:[false,false,false,false]},
                    {text: format(arround(treso.mois5, precision)) + ' ' + money, style:['bolded','light',(treso.mois5<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', colorMFC, 'righted', cc], border:[false,false,false,false]},
                    {text: format(arround(treso.mois6, precision)) + ' ' + money, style:['bolded','light',(treso.mois6<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', colorMFC, 'righted', cc], border:[false,false,false,false]},
                ]);
                if (treso.children) {
                    for (let child of treso.children) {
                        result.push([
                            {text: deleteSpace(child.titre), style:['light','lefted'], border:[false,false,false,true], colSpan:2, borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']}, {},
                            {text: format(arround(child.initial, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                            {text: format(arround(child.mois1, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                            {text: format(arround(child.mois2, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                            {text: format(arround(child.mois3, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                            {text: format(arround(child.mois4, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                            {text: format(arround(child.mois5, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                            {text: format(arround(child.mois6, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                        ]);
                        if (child.children) {
                            for (let item of child.children) {
                                result.push([
                                    {text: '- ' + deleteSpace(item.titre), margin: [10, 0, 0, 0], style:['light','lefted'], border:[false,false,false,true], colSpan:2, borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']}, {},
                                    {text: format(arround(item.initial, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                                    {text: format(arround(item.mois1, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                                    {text: format(arround(item.mois2, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                                    {text: format(arround(item.mois3, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                                    {text: format(arround(item.mois4, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                                    {text: format(arround(item.mois5, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                                    {text: format(arround(item.mois6, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                                ]);
                            }
                        }
                    }
                }
                result.push([
                    {text: '', margin: [0,0,0,4], border:[false,false,false,false], colSpan:2},{},
                    {text: '', margin: [0,0,0,4], border:[false,false,false,false]},
                    {text: '', margin: [0,0,0,4], border:[false,false,false,false]},
                    {text: '', margin: [0,0,0,4], border:[false,false,false,false]},
                    {text: '', margin: [0,0,0,4], border:[false,false,false,false]},
                    {text: '', margin: [0,0,0,4], border:[false,false,false,false]},
                    {text: '', margin: [0,0,0,4], border:[false,false,false,false]},
                    {text: '', margin: [0,0,0,4], border:[false,false,false,false]},
                ])
            }
            return result;
        },
        buildArrayTresoSem2: tresos => {
            let result;
            result = [];
            for (let treso of tresos) {
                let colorHeader = 'backgroundGrey';
                let color = '';
                let cc = '';
                let colorMFC = 'backgroundGrey';
                if (treso.titre === 'Décaissements' || treso.titre === 'Encaissements'){
                    colorHeader = (treso.titre == 'Décaissements') ? 'backgroundRed': 'backgroundGreen';
                    colorMFC = (treso.titre == 'Décaissements') ? 'backgroundRed': 'backgroundGreen';
                    color = cc = 'whiteColor';
                }
                if (treso.tag === 'resultat') {
                    colorMFC = '';
                    cc = 'whiteColor';
                }
                result.push([
                    {text: deleteSpace(treso.titre), style:['bolded','light', colorHeader,'lefted', color], border:[false,false,false,false], colSpan:2},{},
                    {text: format(arround(treso.mois7, precision)) + ' ' + money, style:['bolded','light',(treso.mois7<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', colorMFC,'righted', cc], border:[false,false,false,false]},
                    {text: format(arround(treso.mois8, precision)) + ' ' + money, style:['bolded','light',(treso.mois8<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', colorMFC,'righted', cc], border:[false,false,false,false]},
                    {text: format(arround(treso.mois9, precision)) + ' ' + money, style:['bolded','light', (treso.mois9<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', colorMFC,'righted', cc], border:[false,false,false,false]},
                    {text: format(arround(treso.mois10, precision)) + ' ' + money, style:['bolded','light',(treso.mois10<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', colorMFC,'righted', cc], border:[false,false,false,false]},
                    {text: format(arround(treso.mois11, precision)) + ' ' + money, style:['bolded','light',(treso.mois11<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', colorMFC,'righted', cc], border:[false,false,false,false]},
                    {text: format(arround(treso.mois12, precision)) + ' ' + money, style:['bolded','light',(treso.mois12<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', colorMFC,'righted', cc], border:[false,false,false,false]},
                    {text: format(arround(treso.total, precision)) + ' ' + money, style:['bolded','light',(treso.total<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', colorMFC,'righted', cc], border:[false,false,false,false]},
                ]);
                if (treso.children) {
                    let children = treso.children;
                    for (let child of children) {
                        result.push([
                            {text: deleteSpace(child.titre), style:['light','lefted'], border:[false,false,false,true], colSpan:2, borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},{},
                            {text: format(arround(child.mois7, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                            {text: format(arround(child.mois8, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                            {text: format(arround(child.mois9, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                            {text: format(arround(child.mois10, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                            {text: format(arround(child.mois11, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                            {text: format(arround(child.mois12, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                            {text: format(arround(child.total, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']}
                        ]);
                    }
                }
                result.push([
                    {text: '', margin: [0,0,0,15], border:[false,false,false,false], colSpan:2},{},
                    {text: '', margin: [0,0,0,15], border:[false,false,false,false]},
                    {text: '', margin: [0,0,0,15], border:[false,false,false,false]},
                    {text: '', margin: [0,0,0,15], border:[false,false,false,false]},
                    {text: '', margin: [0,0,0,15], border:[false,false,false,false]},
                    {text: '', margin: [0,0,0,15], border:[false,false,false,false]},
                    {text: '', margin: [0,0,0,15], border:[false,false,false,false]},
                    {text: '', margin: [0,0,0,15], border:[false,false,false,false]},
                ])
            }
            return result;
        },
        printPdfFinance(paramsMoney, projet, treso, images, password){
            // implementation du pdf
            money = paramsMoney['currency'];
            precision = paramsMoney['precision'];
            var docDefinition = {
                info: {
                    title: `${projet.projettitre.toUpperCase()} TRESORERIE DE LANCEMENT`,
                    author: JSON.parse(localStorage.getItem("userSession")).displayName,
                    subject: '',
                    keywords: '',
                    creator: JSON.parse(localStorage.getItem("userSession")).displayName,
                    producer: 'CYBERSCHOOL ENTREPREUNEURIAT',

                },
                userPassword: password,
                ownerPassword: 'cbscodeteam',
                permissions: {
                    printing: 'highResolution', //'lowResolution'
                    modifying: false,
                    copying: false,
                    annotating: true,
                    fillingForms: true,
                    contentAccessibility: true,
                    documentAssembly: true
                },
                background:  function () {
                    return {
                        columns: [{
                            width: '*',
                            stack: [
                                {
                                    image: images.logoBp,
                                    alignment: 'center',
                                    height: 290,
                                    width: 430,
                                    opacity: 0.08
                                }
                            ],
                            margin:[0, 170,0,0]
                        }]
                    }
                },
                pageOrientation: 'landscape',
                styles: this.styles,
                defaultStyle: this.defaultStyle,
                footer: {
                    columns: [
                        { text: 'CYBERSCHOOL ENTREPREUNEURIAT    RCCM : RG LBV 2018A46528   NIF :395648 A \n' +
                                'Siege Echangeur de Nzeng Ayong /site : www.cyberschoolgabon.com/Tel :061723638/062352105/066927701/\n'+
                                'Bp :20207/email : contact@cyberschoolgabon.com\n', margin: [90,0,0,0], style: ['small']
                        }
                    ]
                },
                content: [
                    // en-tete du doc
                    {
                        columns: [
                            {
                                width: '*',
                                stack: [
                                    {
                                        image: images.userLogo,
                                        alignment: 'left',
                                        height: 110,
                                        width: 110
                                    }
                                ],
                                margin:[0, -32,0,0]
                            },
                            {
                                width: '30%',
                                stack: [
                                    {
                                        image: images.logoBp,
                                        alignment: 'right',
                                        height: 100,
                                        width: 150
                                    }
                                ],
                                margin:[0,-32,0,0]
                            }
                        ],
                        columnGap: 4,
                    },
                    { text: 'Trésorerie de lancement',
                        style:['bolded', 'bigHeader'], alignment: 'center', margin: [0,30,0,22]},
                    {text:'Projet : '+ projet.projettitre, style: 'header',margin: [0,0,0,15]},
                    { text:'', margin: [0,0,0,25]},
                    {
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArrayYear(),
                            layout:'noBorders'
                        },margin: [2,0,0,22]
                    },
                    {
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArrayTresoSem1(treso),
                            layout:'noBorders'
                        },margin: [2,0,0,22]
                    },{text:'', pageBreak: 'before'},
                    {
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArrayYear1(),
                            layout:'noBorders'
                        },margin: [2,0,0,22]
                    },
                    {
                        table: {
                            headerRows: 0,
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            body: this.buildArrayTresoSem2(treso),
                            layout:'noBorders'
                        },margin: [2,0,0,22]
                    }
                ]
            };
            pdfMake.createPdf(docDefinition).download(projet.projettitre.toLowerCase() + '_tresorerie_de_lancement.pdf');
        }
    },
    data() {
        return {
            styles: {
                centered:{
                    alignment: 'center'
                },
                righted:{
                    alignment: 'right'
                },
                lefted:{
                    alignment: 'left'
                },
                header: {
                    fontSize: 15,
                    bold: true,
                    alignment: 'justify'
                },
                bigHeader: {
                    fontSize: 18,
                    bold: true,
                    decoration: 'underline',
                    alignment: 'justify'
                },
                subheader: {
                    fontSize: 12,
                    bold: true,
                    alignment: 'justify'
                },
                quote: {
                    italics: true,
                    alignment: 'justify'
                },
                small: {
                    fontSize: 6.5,
                    alignment: 'justify'
                },
                bigger: {
                    fontSize: 13,
                    italics: true,
                    alignment: 'justify'
                },
                light: {
                    fontSize: 9,
                    alignment: 'justify'
                },
                tableHeader: {
                    alignment: 'left',
                    bold:true
                },
                bolded: {
                    bold: true
                },
                italiced: {
                    italics: true,
                },
                whiteColor: {
                    color: 'white'
                },
                blackColor: {
                    color: 'black'
                },
                redColor: {
                    color: '#fb3a3a'
                },
                blueColor: {
                    color:'#398bf7'
                },
                backgroundBlueRecap: {
                    fillColor: '#1e58b3'
                },
                backgroundBlueLight: {
                    fillColor: '#d7e1ec'
                },
                backgroundGreen: {
                    fillColor: '#64ab59'
                },
                backgroundGrey:{
                    fillColor:'#99abb4'
                },
                backgroundRed: {
                    fillColor: '#f13c42'
                },
                backgroundPink: {
                    fillColor: '#f0d4d6'
                },
                backgroundGreyRecap: {
                    fillColor: '#f3f1f1'
                },
                backgroundDarkBlueRecap: {
                    fillColor: '#398bf7'
                },
                backgroundLightBlueRecap: {
                    fillColor: '#2bbbff'
                },
                backgroundLibelleRecap: {
                    // fillColor: '#f3f1f1',
                    fillColor: '#ded9d9'
                },
                backgroundTableauStandard: {
                    fillColor: '#398bf7'
                }

            },
            defaultStyle:  {
                fontSize: 10,
                bold: false,
                alignment: 'justify'
            },
        }
    }
}

