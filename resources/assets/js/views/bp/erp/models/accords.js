import FirebaseModel from '../../../../store/models/firebaseModelErp';

class Accord extends FirebaseModel {

    
    constructor(collection = 'accords', infos) {
        super(collection)
        this.data = {
            libelle : infos.libelle,
            color:infos.color
        }
    }
}
export default Accord


