import FirebaseModel from '../../../../store/models/firebaseModelErp';

class Salarier extends FirebaseModel {

    constructor(collection = 'salariers', infos) {
        super(collection)
        this.data = {
            code : infos.code,
            matricule : infos.matricule,
            nom : infos.libelle,
            prenom:infos.prenom,
            montant: infos.montant,
            mois: infos.mois
        }
    }
    
}
export default Salarier  
