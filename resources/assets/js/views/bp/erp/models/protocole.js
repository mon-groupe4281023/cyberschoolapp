import FirebaseModel from '../../../../store/models/firebaseModelErp';

class Protocole extends FirebaseModel {

    
    constructor(collection = 'protocoles', infos) {
        super(collection)
        this.data = {
            libelle : infos.libelle,
            color:infos.color,
            actions: infos.actions
        }
    }
}
export default Protocole