import FirebaseModel from '../../../../store/models/firebaseModelErp';

class Composant extends FirebaseModel {

    constructor(collection = 'composants', infos) {
        super(collection)
        this.data = {
            libelle : infos.libelle,
            color:infos.color
        }
    }
}
export default Composant  