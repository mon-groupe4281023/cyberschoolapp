import FirebaseModel from '../../../../store/models/firebaseModelErp';

class RisqueParams extends FirebaseModel {

    constructor(collection = 'risques', infos) {
        super(collection)
        this.data = {
            libelle : infos.libelle,
            color:infos.color,
            // createdAt: FirebaseModel.getCreatedAt()
        }
    }
    
}
export default RisqueParams  