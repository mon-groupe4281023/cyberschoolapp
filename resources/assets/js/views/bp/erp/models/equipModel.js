import FirebaseModel from '../../../../store/models/firebaseModelErp';

class EquipModel extends FirebaseModel {

    constructor(collection = 'equipes', infos) {
        super(collection)
        this.data = {
            libelle : infos.libelle,
            color:infos.color,
            // createdAt: FirebaseModel.getCreatedAt()
        }
    }
    
}
export default EquipModel  