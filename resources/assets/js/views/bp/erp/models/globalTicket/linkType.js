import FirebaseModel from '../../../../../store/models/firebaseModelErp';

class LinkType extends FirebaseModel {

    constructor(collection = 'link', data) {
        super(collection)
        this.data = {
            libelle: data.libelle,
            type:data.type,
        }
    }
}
export default LinkType  