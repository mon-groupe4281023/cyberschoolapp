import FirebaseModel from '../../../../../store/models/firebaseModelErp';

class TypeTicket extends FirebaseModel {

    
    constructor(collection = 'globalTypeTicket', data) {
        super(collection)
        this.data = {
            libelle : data.libelle,
            type:data.type
        }
    }
}
export default TypeTicket