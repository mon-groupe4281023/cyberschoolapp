import FirebaseModel from '../../../../store/models/firebaseModelErp';

class Provenance extends FirebaseModel {

    
    constructor(collection = 'provenances', infos) {
        super(collection)
        this.data = {
            libelle : infos.libelle,
            color:infos.color
        }
    }
}
export default Provenance