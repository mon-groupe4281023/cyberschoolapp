import FirebaseModel from '../../../../store/models/firebaseModelErp';

class Version extends FirebaseModel {

    
    constructor(collection = 'versions', infos) {
        super(collection)
        this.data = {
            libelle : infos.libelle,
            color: infos.color,
        }
    }
}
export default Version


