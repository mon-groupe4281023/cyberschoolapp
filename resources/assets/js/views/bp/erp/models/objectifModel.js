import FirebaseModel from '../../../../store/models/firebaseModelErp';

class ObjectifModel extends FirebaseModel {

    constructor(collection = 'objectifs', infos) {
        super(collection)
        this.data = {
            libelle : infos.libelle,
            color:infos.color,
            // createdAt: FirebaseModel.getCreatedAt()
        }
    }
    
}
export default ObjectifModel  