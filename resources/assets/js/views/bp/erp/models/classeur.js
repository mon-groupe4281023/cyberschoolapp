import FirebaseModel from '../../../../store/models/firebaseModelErp';

class Classeur extends FirebaseModel {

    
    constructor(collection = 'classeurs', infos) {
        super(collection)
        this.data = {
            libelle : infos.libelle,
            color:infos.color
        }
    }
}
export default Classeur


