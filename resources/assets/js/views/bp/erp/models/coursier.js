import FirebaseModel from '../../../../store/models/firebaseModelErp';

class Coursier extends FirebaseModel {

    constructor(collection = 'coursiers', infos) {
        super(collection)
        this.data = {
            nom : infos.libelle,
            prenom:infos.prenom,
            contact: infos.contact,
            email: infos.email
        }
    }
    
}
export default Coursier  
