import FirebaseModel from '../../../../store/models/firebaseModelErp';

class Sprint extends FirebaseModel {

    
    constructor(collection = 'sprints', infos) {
        super(collection)
        this.data = {
            libelle : infos.libelle,
            color: infos.color,
            dateDebut: infos.dateDebut,
            dateFin: infos.dateFin
        }
    }
}
export default Sprint


