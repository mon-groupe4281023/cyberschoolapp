import FirebaseModel from '../../../../store/models/firebaseModelErp';

class Priorite extends FirebaseModel {

    constructor(collection = 'priorites', infos) {
        super(collection)
        this.data = {
            libelle : infos.libelle,
            color:infos.color,
            // createdAt: FirebaseModel.getCreatedAt()
        }
    }
    
}
export default Priorite  