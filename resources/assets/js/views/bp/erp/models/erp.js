import FirebaseModel from '../../../../store/models/firebaseModelErp';
import { Notification } from "element-ui";
class ErpModel extends FirebaseModel {

    constructor(collection = 'erp') {
        super(collection)
        this.init = {}
    }

    async initialization(id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
 
    /*************************SET METHODS***************************/
    // Model Article Achat
    static setModelArticleAchat(document) {
        let article = {
            titre: true,
            prixTTC: true,
            prixHT: true,
            tva: true,
            gamme: true,
            description: true,
            poids: true,
            longueur: true,
            largeur: true,
            hauteur: true,
            surface: true,
            volume: true,
            client: true,
            stock: true,
            fournisseur: true,
            famille: true,
            type: true,
            nature: true,
            notes: true,
        }
        FirebaseModel.saveDocumentSubCollection('params', document, 'formArticleAchat', article)
    }
    // Model Article Vente
    static setModelArticleVente(document) {
        let vente = {
            titre: true,
            prixTTC: true,
            prixHT: true,
            tva: true,
            coutVariable: true,
            refCompt: true,
            gamme: true,
            description: true,
            poids: true,
            longueur: true,
            largeur: true,
            hauteur: true,
            surface: true,
            volume: true,
            client: true,
            stock: true,
            fournisseur: true,
            famille: true,
            type: true,
            nature: true,
            notes: true,
        }
        FirebaseModel.saveDocumentSubCollection('params', document, 'formArticleVente', vente)
    }
    // Model Client
    static setModelClient(document) {
        let client = {
            matricule: true,
            nom: true,
            prenom: true,
            typeContrat: true,
            email: true,
            poste: true,
            adresse: true,
            codePostal: true,
            ville: true,
            pays: true,
            quartier: true,
            arrondissement: true,
            region: true,
            province: true,
            mobileSure: true,
            tel1: true,
            tel2: true,
            emails: true,
            whatsapp: true,
            zoom: true,
            skype: true,
            note: true,
        }
        FirebaseModel.saveDocumentSubCollection('params', document, 'formRhClient', client)
    }
    // Model Fournisseur
    static setModelFournisseur(document) {
        let fournisseur = {
            raisonSociale: true,
            statut: true,
            assujettiTva: true,
            relation: true,
            mobileSure: true,
            refCompta: true,
            addresse: true,
            codePostal: true,
            ville: true,
            pays: true,
            quartier: true,
            arrondissement: true,
            region: true,
            province: true,
            tel1: true,
            tel2: true,
            email: true,
            whatsapp: true,
            zoom: true,
            skype: true,
            notes: true,
        }
        FirebaseModel.saveDocumentSubCollection('params', document, 'formRhFour', fournisseur)
    }
    /*************************END SET METHODS***************************/
    static updateNotification() {
        Notification.success({
            title: 'Success',
            message: 'L\'élémént a été modifié avec succès',
        });
    }
    static createNotification() {
        Notification.success({
            title: 'Success',
            message: 'L\'élémént a été ajouté avec succès',
        });
    }
    static generateMatricule(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() *
                charactersLength));
        }
        return result;
    }
}

export default ErpModel;