import FirebaseModel from '../../../../store/models/firebaseModelErp';

class CourrierSortant extends FirebaseModel {
    
    constructor(collection = 'sortant', infos) {
        super(collection)
        this.data = {
            libelle : infos.libelle,
            color:infos.color,
            createdAt: FirebaseModel.getCreatedAt()
        }
    }
}
export default CourrierSortant