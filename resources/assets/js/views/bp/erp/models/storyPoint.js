import FirebaseModel from '../../../../store/models/firebaseModelErp';

class StoryPoint extends FirebaseModel {

    constructor(collection = 'storyPoints', infos) {
        super(collection)
        this.data = {
            libelle : infos.libelle,
            color:infos.color,
            // createdAt: FirebaseModel.getCreatedAt()
        }
    }
    
}
export default StoryPoint  