import FirebaseModel from '../../../../store/models/firebaseModelErp';

class TypeTicketHeader extends FirebaseModel {

    
    constructor(collection = 'typeTicketHeaders', infos) {
        super(collection)
        this.data = {
            libelle : infos.libelle,
            color: infos.color,
        }
    }
}
export default TypeTicketHeader


