import FirebaseModel from '../../../../../store/models/firebaseModelErp';

class Reservation extends FirebaseModel {

    constructor(collection = 'planAction', data) {
        super(collection)
        this.data = {
            titre: data.titre,
            adminId:data.adminId,
            archive:false,
            dateDebut: data.dateDebut,
            dateFin: data.dateFin,
            heureDebut : data.heureDebut,
            heureFin: data.heureFin,
            description: data.description,
            dates: data.dates,
            etat: data.etat,
            index: data.index,
            etiquette: data.etiquette,
            refEtiquette: data.etiquette,
            priorite : data.priorite,
            activite: data.activite,
            refCategorie: data.activite,
            summaryId: data.summaryId,
            typeTicket: data.typeTicket,
            userId:     data.userId,
            articles: data.articles,
            protocole: data.protocole, 
            createdAt: FirebaseModel.getCreatedAt(), 
            version:'2.0'
        }
    }
}
export default Reservation  