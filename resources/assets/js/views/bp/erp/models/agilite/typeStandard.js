import FirebaseModel from '../../../../../store/models/firebaseModelErp';

class TypeStandard extends FirebaseModel {

    constructor(collection = 'planAction', data) {
        super(collection)
        if(!data.resultatsAttendus) {
            data.resultatsAttendus =''
        }
        this.data = {
            titre: data.titre,
            adminId:data.adminId,
            archive:false,
            description: data.description,
            dates: data.dates,
            etat: data.etat,
            etiquette: data.etiquette,
            refEtiquette: data.etiquette,
            resultatsAttendus: data.resultatsAttendus,
            priorite : data.priorite,
            activite: data.activite,
            refCategorie: data.activite,
            summaryId: data.summaryId,
            typeTicket: data.typeTicket,
            userId:     data.userId,
            sprint:     data.sprint,
            files: data.files,
            protocole: data.protocole,
            alertDate: data.alertDate,
            index: data.index,
            createdAt: FirebaseModel.getCreatedAt(),
            version:'2.0'
        }
    }
}
export default TypeStandard  