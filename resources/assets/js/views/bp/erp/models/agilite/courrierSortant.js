import FirebaseModel from '../../../../../store/models/firebaseModelErp';

class CourrierSortant extends FirebaseModel {

    constructor(collection = 'planAction', data) {
        super(collection)
        this.data = {
            titre: data.titre,
            adminId:data.adminId,
            archive:false,
            description: data.description,
            dates: data.dates,
            etat: data.etat,
            etiquette: data.etiquette,
            refEtiquette: data.etiquette,
            priorite : data.priorite,
            activite: data.activite,
            refCategorie: data.activite,
            summaryId: data.summaryId,
            typeTicket: data.typeTicket,
            objet: data.objet,
            files: data.files,
            userId:  data.userId,
            // dateDebutFin: data.dateDebutFin,
            version:'2.0',
            createdAt: FirebaseModel.getCreatedAt(),

        }
    }
}
export default CourrierSortant  