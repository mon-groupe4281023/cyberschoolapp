import FirebaseModel from '../../../../../store/models/firebaseModelErp';

class WorkSpace extends FirebaseModel {
    constructor(collection="summary", data) {
        super(collection)
        this.data = {
             titre: data.titre,
             description : data.description,
             responsable: data.responsable,
             categorie: data.type,
             createdAt:FirebaseModel.getCreatedAt(),
             type:"workspace",
             user:data.user
        }
    }
 
}
export default WorkSpace  