import FirebaseModel from '../../../../../store/models/firebaseModelErp';

class DemandeAchat extends FirebaseModel {

    constructor(collection = 'planAction', data) {
        super(collection)
        this.data = {
            titre: data.titre,
            adminId:data.adminId,
            archive:false,
            heureDebut : data.heureDebut,
            heureFin: data.heureFin,
            dateDebut: data.dateDebut,
            dateFin: data.dateFin,
            description: data.description,
            dates: data.dates,
            etat: data.etat,
            etiquette: data.etiquette,
            refEtiquette: data.etiquette,
            priorite : data.priorite,
            protocole : data.protocole,
            activite: data.activite,
            refCategorie: data.activite,
            summaryId: data.summaryId,
            typeTicket: data.typeTicket,
            userId:     data.userId,
            accord: data.accord,
            numeroCompte: data.numeroCompte,            
            salarier: data.salarier,
            numeroCompte: data.numeroCompte,
            mois: data.mois,
            createdAt: FirebaseModel.getCreatedAt(),
            version:'2.0'
        }
    }
}
export default DemandeAchat  