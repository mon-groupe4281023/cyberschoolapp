import FirebaseModel from '../../../../../store/models/firebaseModelErp';

class DemandeAchat extends FirebaseModel {

    constructor(collection = 'planAction', data) {
        super(collection)
        this.data = {
            titre: data.titre,
            adminId:data.adminId,
            archive:false,
            description: data.description,
            dates: data.dates,
            etat: data.etat,
            etiquette: data.etiquette,
            refEtiquette: data.etiquette,
            priorite : data.priorite,
            demandeRaison: data.demandeRaison,
            protocole : data.protocole,
            activite: data.activite,
            refCategorie: data.activite,
            summaryId: data.summaryId,
            articles: data.articles,
            typeTicket: data.typeTicket,
            userId:     data.userId,
            index: data.index,
            createdAt: FirebaseModel.getCreatedAt(),
            version:'2.0'
        }
    }
}
export default DemandeAchat  