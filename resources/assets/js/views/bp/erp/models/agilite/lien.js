import FirebaseModel from '../../../../../store/models/firebaseModelErp';

class Lien extends FirebaseModel {
    constructor(collection="links", data) {
        super(collection)
        this.data = {
             idType: data.idType,
             idTicket : data.idTicket,
             createdAt:FirebaseModel.getCreatedAt(),
        }
    }

}
export default Lien  