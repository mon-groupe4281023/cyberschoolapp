import FirebaseModel from '../../../../../store/models/firebaseModelErp';

class Ticket extends FirebaseModel {
    // constructor(collection="subTasks", data) {
    //     super(collection)
    //     this.data = {
    //          titre: data.titre,
    //          etat : data.etat,
    //          dates: data.dates,
    //          state: data.state,
    //          description:'',
    //          percent:data.percent,
    //          userId: data.userId
    //     }
    // }
    static updateDocumentSubSubCollectionField(collection, doc, subCollection, subSubDoc, state, etat) {
        return FirebaseModel.getCollection(collection)
        .doc(doc)
        .collection(subCollection)
        .doc(subSubDoc)
        .update({"state":state, "etat":etat})
    }
}
export default Ticket  