import FirebaseModel from '../../../../../store/models/firebaseModelErp';

class SubTask extends FirebaseModel {
    constructor(collection="subTasks", data) {
        super(collection)
       
            this.data = {
                titre: data.titre,
                etat : data.etat,
                dates: data.dates,
                state: data.state,
                description:'',
                percent:data.percent,
                commentaire:data.commentaire?data.commentaire:'',
                userId: data.userId,
                createdAt:FirebaseModel.getCreatedAt(),
           }


    }
    static updateDocumentSubSubCollectionField(collection, doc, subcollection, subSubDoc, subSubCollection, id,  state, etat) {
        return FirebaseModel.getCollection(collection)
        .doc(doc)
        .collection(subcollection)
        .doc(subSubDoc)
        .collection(subSubCollection)
        .doc(id)
        .update({"state":state, "etat":etat})
    }
}
export default SubTask  