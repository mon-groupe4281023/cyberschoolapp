import FirebaseModel from '../../../../../store/models/firebaseModelErp';

class TypeTicket extends FirebaseModel {

    constructor(collection = 'planAction', data) {
        super(collection)
        this.data = {
            titre: data.titre,
            admindId:data.admindId,
            archive:false,
            description: data.description,
            dates: data.dates,
            etat: data.etat,
            files: data.files,
            activite: data.activite,
            refCategorie: data.activite,
            summaryId: data.summaryId,
            typeTicket: data.typeTicket,
            userId:     data.userId

        }
    }
}
export default TypeTicket  