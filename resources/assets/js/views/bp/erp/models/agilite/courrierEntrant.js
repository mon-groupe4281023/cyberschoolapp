import FirebaseModel from '../../../../../store/models/firebaseModelErp';

class CourrierEntrant extends FirebaseModel {

    constructor(collection = 'planAction', data) {
        super(collection)
        this.data = {
            titre: data.titre,
            adminId:data.adminId,
            archive:false,
            description: data.description,
            dates: data.dates,
            dateArrive: data.dateArrive,
            dateCourrier: data.dateCourrier,
            etat: data.etat,
            etiquette: data.etiquette,
            refEtiquette: data.etiquette,
            priorite : data.priorite,
            classeur : data.classeur,
            provenance: data.provenance,
            // version: data.version,
            activite: data.activite,
            // periode: data.periode,
            refCategorie: data.activite,
            summaryId: data.summaryId,
            typeTicket: data.typeTicket,
            num:data.num,
            numEmet:data.numEmet,
            coursier:data.coursier,
            objet: data.objet,
            index: data.index,
            files: data.files,
            userId:  data.userId,
            createdAt: FirebaseModel.getCreatedAt(),
            version:'2.0'
        }
    }
}
export default CourrierEntrant  
