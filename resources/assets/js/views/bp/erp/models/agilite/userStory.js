import FirebaseModel from '../../../../../store/models/firebaseModelErp';

class UserStory extends FirebaseModel {

    constructor(collection = 'planAction', data) {
        super(collection)
        this.data = {
            titre: data.titre,
            adminId:data.adminId,
            archive:false,
            description: data.description,
            dates: data.dates,
            etat: data.etat,
            etiquette: data.etiquette,
            refEtiquette: data.etiquette,
            priorite : data.priorite,
            // risque
            risque: data.risque,
            objectif: data.objectif,
            equipe: data.equipe,
            bValue: data.bValue,
            storyPoint: data.storyPoint,
            enTantQue: data.enTantQue,
            monEspace: data.monEspace,
            afinDe: data.afinDe,
            independant: data.independant,
            negociable: data.negociable,
            viable: data.viable,
            estimable: data.estimable,
            petite: data.petite,
            testable: data.testable,
            alerteDate: data.alerteDate,
            sprint: data.sprint,
            activite: data.activite,
            periode: data.periode,
            refCategorie: data.activite,
            summaryId: data.summaryId,
            typeTicket: data.typeTicket,
            index: data.index,
            files: data.files,
            userId:  data.userId,
            createdAt: FirebaseModel.getCreatedAt(),
            version:'2.0'
        }
    }
}
export default UserStory  
