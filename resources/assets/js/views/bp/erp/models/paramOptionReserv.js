import FirebaseModel from '../../../../store/models/firebaseModelErp';

class ParamOptionReserv extends FirebaseModel {

    constructor(collection = 'salle', infos) {
        super(collection)
        this.data = {
            tableauTypeReservation: infos.tableauTypeReservation,
            titre: infos.titre,
            ref : infos.ref,
            articles : infos.articles,
            images: infos.images,
            dates:infos.dates,
            qte: infos.qte,
            prixUttc: infos.prixUttc,
            commentaires: infos.commentaires,
        }
    }
    
}
export default ParamOptionReserv
