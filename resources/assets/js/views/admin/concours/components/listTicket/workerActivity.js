importScripts("https://www.gstatic.com/firebasejs/8.3.1/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.3.1/firebase-auth.js");
importScripts("https://www.gstatic.com/firebasejs/8.3.1/firebase-firestore.js");

// importScripts("/resources/assets/js/services/helper.js")
// importScripts("/resources/assets/js/views/bp/mixins/admin/actionPlanNew.js")

// firebase configuration object
const firebaseConfig = {
  apiKey: "AIzaSyAtLbJIn6Rk7xXdj81bIyBCc-KoBt3y9Hs",
  authDomain: "cyberschool-ab785.firebaseapp.com",
  databaseURL: "https://cyberschool-ab785.firebaseio.com",
  projectId: "cyberschool-ab785",
  storageBucket: "cyberschool-ab785.appspot.com",
  messagingSenderId: "107562228204",
  appId: "1:107562228204:web:29cb3cd4f35fe30bbcc8c9",
  measurementId: "G-H7E9VRGRNV"

};
// initialize firebase
firebase.initializeApp(firebaseConfig);
function timeDifference(date1, date2) {
  let result = {};
  let difference = date1.getTime() - date2.getTime();
  return new Promise(resolve => {
                
      let daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;
     
      let hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;
  
      let minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;
  
      let secondsDifference = Math.floor(difference / 1000);
  
      result.daysDifference = daysDifference;
      result.hoursDifference = hoursDifference;
      result.minutesDifference = minutesDifference;
      result.secondsDifference = secondsDifference;

      resolve(result) ;

    });
  
 
}

function dateFormat(date){
  if(date!=null && date!=undefined){
      var timestamp = date.seconds*1000;
      var date_not_formatted = new Date(timestamp);

      var formatted_string = date_not_formatted.getFullYear() + "-";

      if (date_not_formatted.getMonth() < 9) {
      formatted_string += "0";
      }
      formatted_string += (date_not_formatted.getMonth() + 1);
      formatted_string += "-";

      if(date_not_formatted.getDate() < 10) {
      formatted_string += "0";
      }
      formatted_string += date_not_formatted.getDate();

      return formatted_string;
  }
          
        
}

onmessage = function(event) {
  let data = event.data
       firebase
        .firestore()
        .collection("tasks")
        .doc(data[2])
        .collection("activites")
        .doc(data[1])
        .collection("tickets")
        .get()
        .then((querySnapshot) => {
          querySnapshot.forEach(async (doc) => {
            if (doc.data().userId && doc.data().userId.id == data[0]) {
              let result = await timeDifference(
                new Date(dateFormat(doc.data().createdAt)),
                new Date()
              );
              if (result.daysDifference == 0 || result.daysDifference == -1) {
                // let resultData = doc.data();
                // resultData.id = doc.id;
                // resultData.ref = doc.ref;
                this.postMessage(doc.id)
              }
            } else {
              doc.ref
                .collection("subTasks")
                .get()
                .then((docs) => {
                  docs.forEach(async (doc) => {
                    if (
                      doc.data().userId &&
                      doc.data().userId.id == data[0]
                    ) {
                      let result = await timeDifference(
                        new Date(dateFormat(doc.data().createdAt)),
                        new Date()
                      );
                      if (
                        result.daysDifference == 0 ||
                        result.daysDifference == -1
                      ) {
                      // let resultData = doc.data();
                      //     resultData.id = doc.id;
                          // resultData.ref = doc.ref;
                          this.postMessage(doc.id)
                      }
                    }
                  });
                });
            }
          });
        })
        .catch((error) => {});
 
  }