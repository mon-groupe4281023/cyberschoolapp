importScripts("https://www.gstatic.com/firebasejs/8.3.1/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.3.1/firebase-auth.js");
importScripts("https://www.gstatic.com/firebasejs/8.3.1/firebase-firestore.js");

// importScripts("/resources/assets/js/services/helper.js")
// importScripts("/resources/assets/js/views/bp/mixins/admin/actionPlanNew.js")

// firebase configuration object
const firebaseConfig = {
  apiKey: "AIzaSyAtLbJIn6Rk7xXdj81bIyBCc-KoBt3y9Hs",
  authDomain: "cyberschool-ab785.firebaseapp.com",
  databaseURL: "https://cyberschool-ab785.firebaseio.com",
  projectId: "cyberschool-ab785",
  storageBucket: "cyberschool-ab785.appspot.com",
  messagingSenderId: "107562228204",
  appId: "1:107562228204:web:29cb3cd4f35fe30bbcc8c9",
  measurementId: "G-H7E9VRGRNV"

};
// initialize firebase
firebase.initializeApp(firebaseConfig);


onmessage =  function(event) {
  let data = event.data
//    await firebase
//   .firestore()
//   .collection("planAction")
//   .doc(this.idSummarySelected)
//   .collection(this.docId)
//   .where("etat", "==", "3")
//   .get()
//   .then((query) => {
//     return query.size;
//   });
// this.totalDone =
//   this.totalDone +
   firebase
    .firestore()
    .collection("tasks")
    .doc(data[0])
    .collection("activites")
    .doc(data[1])
    .collection("tickets")
    .where("etat", "==", "3")
    .get()
    .then((querySnapshot) => {
      this.postMessage(querySnapshot.size)
    });
 
  }