import {EcartQuantitePrix, EcartQuantiteMontant, EcartBfrOuverture, EcartBfrExploitation} from './percentage.js'

/**
 */
export function isProgressLevel(itemSuivi, level) {
    if(['low', 'mid', 'middle', 'full'].includes(level)){
        /* then magic starts
        switch(level.toLowerCase()) {
            case 'low': return  break;
            case 'mid': return  break;
            case 'full': return break;
            default: return false;
        } */
        switch(true){
            case (Object.hasOwnProperty(itemSuivi,'prix') && Object.hasOwnProperty(itemSuivi,'quantite')):
                switch(level.toLowerCase()) {
                    case 'low': return EcartQuantitePrix(itemSuivi) < 5 ? true : false; break;
                    case 'mid': return EcartQuantitePrix(itemSuivi) > 5 && EcartQuantitePrix(itemSuivi) < 100 ? true : false; break;
                    case 'full': return EcartQuantitePrix(itemSuivi) == 100 ? true : false; break;
                    default: return false;
                }
                break;
            case (Object.hasOwnProperty(itemSuivi,'montant') && Object.hasOwnProperty(itemSuivi,'quantite') ):
                switch(level.toLowerCase()) {
                    case 'low': return EcartQuantiteMontant(itemSuivi) < 5 ? true : false; break;
                    case 'mid': return EcartQuantiteMontant(itemSuivi) > 5 && EcartQuantiteMontant(itemSuivi) < 100 ? true : false; break;
                    case 'full': return EcartQuantiteMontant(itemSuivi) == 100 ? true : false; break;
                    default: return false;
                }
                break;
            case (Object.hasOwnProperty(itemSuivi,'coutMensuel') && Object.hasOwnProperty(itemSuivi,'pourcentage') ):
                switch(level.toLowerCase()) {
                    case 'low': return EcartBfrOuverture(itemSuivi) < 5 ? true : false; break;
                    case 'mid': return EcartBfrOuverture(itemSuivi) > 5 && EcartBfrOuverture(itemSuivi) < 100 ? true : false; break;
                    case 'full': return EcartBfrOuverture(itemSuivi) == 100 ? true : false; break;
                    default: return false;
                }
                break;
        }
    }
    
}

export const mixin = {
    methods: {
        isProgressLevel
    }
}