// import {arround} from '../../../../services/helper'
import currenciesMaskMixin from '../../../bp/mixins/currenciesMask'
import { isNullOrUndefined } from "util";

/***    BESOINS     **/
export let totalImmoIncorporellePrevisionnel = 0;

export function calculTotalImmoIncorporellePrevisionnel(immoIncorporelleSuivi) {
    if(!isNullOrUndefined(immoIncorporelleSuivi)) {
        immoIncorporelleSuivi.forEach( item => {
            totalImmoIncorporellePrevisionnel = totalImmoIncorporellePrevisionnel + totalPrixPrevisionnelIncorporel(item);
        })
    }
}

export let subTotalImmoCorpMatInfoPrevisionnel = {
    "echeancier": 0, "prix": 0
}

export let subTotalImmoCorpMobilierPrevisionnel = {
    "echeancier": 0, "prix": 0
}

export let subTotalImmoCorpMatMachOutilPrevisionnel = {
    "echeancier": 0, "prix": 0
}

export let subTotalImmoCorpBatLocEspaceVentePrevisionnel = {
    "echeancier": 0, "prix": 0
}

export let subTotalImmoCorpVoitAutrePrevisionnel = {
    "echeancier": 0, "prix": 0
}

export let totalImmoCorporellePrevisionnel = {
    "echeancier": 0, "prix": 0
}

export let subTotalImmoFinCautionPrevisionnel = 0;

export let subTotalImmoFinDepGarantiePrevisionnel = 0 ;

export let subTotalImmoFinAutrePrevisionnel = 0;

export let totalImmoFinancierePrevisionnel = 0;

export let totalBfrOuverturePrevisionnel = 0;

export let subTotalBfrStockPrevisionnel = {
    "annee1": 0,
    "annee2": 0,
    "annee3": 0,
}

export let subTotalBfrCreanceCliPrevisionnel = {
    "annee1": 0,
    "annee2": 0,
    "annee3": 0,
}

export let subTotalBfrDetFourPrevisionnel = {
    "annee1": 0,
    "annee2": 0,
    "annee3": 0,
}

export let totalBfrExploitationPrevisionnel = {
    "annee1": 0,
    "annee2": 0,
    "annee3": 0,
}

export function calculSubTotalImmoCorpMatInfoPrevisionnel(immoCorpMatInfoSuivi) {
    let sumPrix = 0; let sumEcheancier = 0;
    if(!isNullOrUndefined(immoCorpMatInfoSuivi)) {
        immoCorpMatInfoSuivi.forEach( (item) =>  {
            sumEcheancier = sumEcheancier + (item.oldAmortissement > 0 ? totalPrixPrevisionnelCorporel(item)/item.oldAmortissement : 0);
            sumPrix = sumPrix + totalPrixPrevisionnelCorporel(item);
        });        
    }
    subTotalImmoCorpMatInfoPrevisionnel.echeancier = sumEcheancier;
    subTotalImmoCorpMatInfoPrevisionnel.prix = sumPrix;
    calculTotalImmoCorporellePrevisionnel();
}

export function calculSubTotalImmoCorpMobilierPrevisionnel(immoCorpMobilierSuivi) {
    let sumPrix = 0; let sumEcheancier = 0;
    if(!isNullOrUndefined(immoCorpMobilierSuivi)) {
        immoCorpMobilierSuivi.forEach( item => {
            sumEcheancier = sumEcheancier + (item.oldAmortissement > 0 ? totalPrixPrevisionnelCorporel(item)/item.oldAmortissement : 0);
            sumPrix = sumPrix + totalPrixPrevisionnelCorporel(item);
        })
    }
    subTotalImmoCorpMobilierPrevisionnel.echeancier = sumEcheancier;
    subTotalImmoCorpMobilierPrevisionnel.prix = sumPrix;
    calculTotalImmoCorporellePrevisionnel();
}

export function calculSubTotalImmoCorpMatMachOutilPrevisionnel(immoCorpMatMachOutilSuivi) {
    let sumPrix = 0; let sumEcheancier = 0;
    if(!isNullOrUndefined(immoCorpMatMachOutilSuivi)){
        immoCorpMatMachOutilSuivi.forEach( item => {
            sumEcheancier = sumEcheancier + (item.oldAmortissement > 0 ? totalPrixPrevisionnelCorporel(item)/item.oldAmortissement : 0);
            sumPrix = sumPrix + totalPrixPrevisionnelCorporel(item);
        })
    }
    subTotalImmoCorpMatMachOutilPrevisionnel.echeancier = sumEcheancier;
    subTotalImmoCorpMatMachOutilPrevisionnel.prix = sumPrix;
    calculTotalImmoCorporellePrevisionnel();
}

export function calculSubTotalImmoCorpBatLocEspaceVentePrevisionnel(immoCorpBatLocEspaceVenteSuivi) {
    let sumPrix = 0; let sumEcheancier = 0;
    if(!isNullOrUndefined(immoCorpBatLocEspaceVenteSuivi)) {
        immoCorpBatLocEspaceVenteSuivi.forEach( item => {
            sumEcheancier = sumEcheancier + (item.oldAmortissement > 0 ? totalPrixPrevisionnelCorporel(item)/item.oldAmortissement : 0);
            sumPrix = sumPrix + totalPrixPrevisionnelCorporel(item);
        })
    }
    subTotalImmoCorpBatLocEspaceVentePrevisionnel.echeancier = sumEcheancier
    subTotalImmoCorpBatLocEspaceVentePrevisionnel.prix = sumPrix
    calculTotalImmoCorporellePrevisionnel();
}

export function calculSubTotalImmoCorpVoitAutrePrevisionnel(immoCorpVoitAutreSuivi) {
    let sumPrix = 0; let sumEcheancier = 0;
    if(!isNullOrUndefined(immoCorpVoitAutreSuivi)) {
        immoCorpVoitAutreSuivi.forEach( item => {
            sumEcheancier = sumEcheancier + (item.oldAmortissement > 0 ? totalPrixPrevisionnelCorporel(item)/item.oldAmortissement : 0);
            sumPrix = sumPrix + totalPrixPrevisionnelCorporel(item);
        })
    }
    subTotalImmoCorpVoitAutrePrevisionnel.echeancier = sumEcheancier;
    subTotalImmoCorpVoitAutrePrevisionnel.prix = sumPrix;
    calculTotalImmoCorporellePrevisionnel();
}

export function calculTotalImmoCorporellePrevisionnel(){
    totalImmoCorporellePrevisionnel.echeancier  = subTotalImmoCorpMobilierPrevisionnel.echeancier
        + subTotalImmoCorpMatMachOutilPrevisionnel.echeancier
        + subTotalImmoCorpBatLocEspaceVentePrevisionnel.echeancier
        + subTotalImmoCorpVoitAutrePrevisionnel.echeancier
        + subTotalImmoCorpMatInfoPrevisionnel.echeancier;
    totalImmoCorporellePrevisionnel.prix  =  subTotalImmoCorpMobilierPrevisionnel.prix
        + subTotalImmoCorpMatMachOutilPrevisionnel.prix
        + subTotalImmoCorpBatLocEspaceVentePrevisionnel.prix
        + subTotalImmoCorpVoitAutrePrevisionnel.prix
        + subTotalImmoCorpMatInfoPrevisionnel.prix;
}

export function calculSubTotalImmoFinCautionPrevisionnel(immoFinCautionSuivi) {
    let sum = 0;
    if(!isNullOrUndefined(immoFinCautionSuivi)) {
        immoFinCautionSuivi.forEach( item => {
            sum = sum + totalMontantPrevisionnelFinancier(item);
        })
    }
    subTotalImmoFinCautionPrevisionnel = sum;
    calculTotalImmoFinancierePrevisionnel();
}

export function calculSubTotalImmoFinDepGarantiePrevisionnel(immoFinDepGarantieSuivi) {
    let sum = 0;
    if(!isNullOrUndefined(immoFinDepGarantieSuivi)) {
        immoFinDepGarantieSuivi.forEach( item => {
            sum = sum + totalMontantPrevisionnelFinancier(item);
        })
    }
    subTotalImmoFinDepGarantiePrevisionnel = sum;
    calculTotalImmoFinancierePrevisionnel();
}

export function calculSubTotalImmoFinAutrePrevisionnel(immoFinAutreSuivi){
    let sum = 0;
    if(!isNullOrUndefined(immoFinAutreSuivi)) {
        immoFinAutreSuivi.forEach( item => {
            sum = sum + totalMontantPrevisionnelFinancier(item);
        })
    }
    subTotalImmoFinAutrePrevisionnel = sum;
    calculTotalImmoFinancierePrevisionnel();
}

export function calculTotalImmoFinancierePrevisionnel(){
    totalImmoFinancierePrevisionnel  = subTotalImmoFinCautionPrevisionnel
        + subTotalImmoFinDepGarantiePrevisionnel
        + subTotalImmoFinAutrePrevisionnel;
}

export function calculTotalBfrOuverturePrevisionnel(bfrOuvertureSuivi){
    let sum = 0;
    if(!isNullOrUndefined(bfrOuvertureSuivi)){
        bfrOuvertureSuivi.forEach(item => {
            sum = sum + totalCoutPrevisionnelBfrOuverture(item)
        })
    }
    totalBfrOuverturePrevisionnel = sum;    
}

export function calculSubTotalBfrStockAnnee1Previsionnel(bfrStockSuivi) {
    let sum = 0;
    if(!isNullOrUndefined(bfrStockSuivi)){
        bfrStockSuivi.forEach( item => {
            sum = sum +  item.oldAnnee1 ;
        })
    }
    subTotalBfrStockPrevisionnel.annee1 = sum;
    calculTotalBfrExploitationAnnee1Previsionnel();
}

export function calculSubTotalBfrStockAnnee2Previsionnel(bfrStockSuivi) {
    let sum = 0;
    if(!isNullOrUndefined(bfrStockSuivi)){
        bfrStockSuivi.forEach( item => {
            sum = sum +  item.oldAnnee2 ;
        })
    }
    subTotalBfrStockPrevisionnel.annee2 = sum;
    calculTotalBfrExploitationAnnee2Previsionnel();
}

export function calculSubTotalBfrStockAnnee3Previsionnel(bfrStockSuivi) {
    let sum = 0;
    if(!isNullOrUndefined(bfrStockSuivi)){
        bfrStockSuivi.forEach( item => {
            sum = sum +  item.oldAnnee3 ;
        })
    }
    subTotalBfrStockPrevisionnel.annee3 = sum;
    calculTotalBfrExploitationAnnee3Previsionnel();
}

export function calculSubTotalBfrCreanceCliAnnee1Previsionnel(creancesClientSuivi) {
    let sum = 0;
    if(!isNullOrUndefined(creancesClientSuivi)) {
        creancesClientSuivi.forEach( item => {
            sum = sum +  item.oldAnnee1 ;
        })
    }
    subTotalBfrCreanceCliPrevisionnel.annee1 = sum;
    calculTotalBfrExploitationAnnee1Previsionnel();
}

export function calculSubTotalBfrCreanceCliAnnee2Previsionnel(creancesClientSuivi) {
    let sum = 0;
    if(!isNullOrUndefined(creancesClientSuivi)) {
        creancesClientSuivi.forEach( item => {
            sum = sum +  item.oldAnnee2 ;
        })
    }
    subTotalBfrCreanceCliPrevisionnel.annee2 = sum;
    calculTotalBfrExploitationAnnee2Previsionnel();
}

export function calculSubTotalBfrCreanceCliAnnee3Previsionnel(creancesClientSuivi) {
    let sum = 0;
    if(!isNullOrUndefined(creancesClientSuivi)) {
        creancesClientSuivi.forEach( item => {
            sum = sum +  item.oldAnnee3 ;
        })
    }
    subTotalBfrCreanceCliPrevisionnel.annee3 = sum;
    calculTotalBfrExploitationAnnee3Previsionnel();
}

export function calculSubTotalBfrDetFourAnnee1Previsionnel(dettesFournisseurSuivi) {
    let sum = 0;
    if(!isNullOrUndefined(dettesFournisseurSuivi)) {
        dettesFournisseurSuivi.forEach( item => {
            sum = sum + item.oldAnnee1 ;
        })
    }
    subTotalBfrDetFourPrevisionnel.annee1 = sum;
    calculTotalBfrExploitationAnnee1Previsionnel();
}

export function calculSubTotalBfrDetFourAnnee2Previsionnel(dettesFournisseurSuivi) {
    let sum = 0;
    if(!isNullOrUndefined(dettesFournisseurSuivi)) {
        dettesFournisseurSuivi.forEach( item => {
            sum = sum + item.oldAnnee2 ;
        })
    }
    subTotalBfrDetFourPrevisionnel.annee2 = sum;
    calculTotalBfrExploitationAnnee2Previsionnel();
}

export function calculSubTotalBfrDetFourAnnee3Previsionnel(dettesFournisseurSuivi) {
    let sum = 0;
    if(!isNullOrUndefined(dettesFournisseurSuivi)) {
        dettesFournisseurSuivi.forEach( item => {
            sum = sum + item.oldAnnee3 ;
        })
    }
    subTotalBfrDetFourPrevisionnel.annee3 = sum;
    calculTotalBfrExploitationAnnee3Previsionnel();
}

export function calculTotalBfrExploitationAnnee1Previsionnel() {
    totalBfrExploitationPrevisionnel.annee1 = subTotalBfrStockPrevisionnel.annee1 +
    subTotalBfrCreanceCliPrevisionnel.annee1 - subTotalBfrDetFourPrevisionnel.annee1;
}

export function calculTotalBfrExploitationAnnee2Previsionnel() {
    totalBfrExploitationPrevisionnel.annee2 = subTotalBfrStockPrevisionnel.annee2 +
    subTotalBfrCreanceCliPrevisionnel.annee2 - subTotalBfrDetFourPrevisionnel.annee2;
}

export function calculTotalBfrExploitationAnnee3Previsionnel() {
    totalBfrExploitationPrevisionnel.annee3 = subTotalBfrStockPrevisionnel.annee3 +
    subTotalBfrCreanceCliPrevisionnel.annee3 - subTotalBfrDetFourPrevisionnel.annee3;
}

export function totalPrixPrevisionnelIncorporel(itemSuivi){
    return itemSuivi.oldPrix * itemSuivi.oldQuantite
}

export function totalPrixPrevisionnelCorporel(itemSuivi){
    return itemSuivi.oldPrix * itemSuivi.oldQuantite
}

export function amortissementLineairePrevisionnel(itemSuivi){
    return itemSuivi.oldAmortissement > 0 ?
        totalPrixPrevisionnelCorporel(itemSuivi) / itemSuivi.oldAmortissement : 0
}

export function totalMontantPrevisionnelFinancier(itemSuivi){
    return itemSuivi.oldMontant * itemSuivi.oldQuantite
}

export function totalCoutPrevisionnelBfrOuverture(itemSuivi){
    return itemSuivi['oldCoutMensuel'] * itemSuivi.oldDuree * itemSuivi.oldPourcentage / 100;
}

export function totalCoutPrevisionnelBfrExploitation(itemSuivi){
    return itemSuivi.oldAnnee1 + itemSuivi.oldAnnee2 + itemSuivi.oldAnnee3 
}

/***      RESSOURCES      ***/

export let totalCapitalPrevisionnel = 0;

export function calculTotalCapitalPrevisionnel() {
    totalCapitalPrevisionnel = subTotalCapAppNumerairePrevisionnel + subTotalCapAppNaturePrevisionnel;
}

export let subTotalCapAppNumerairePrevisionnel = 0;
export let subTotalCapAppNaturePrevisionnel = 0;

export function calculTotalCapitalNumerairePrevisionnel(capNumeraireSuivi) {
    let sum = 0;
    if(!isNullOrUndefined(capNumeraireSuivi)) {
        capNumeraireSuivi.forEach( item => {
            sum = sum + (item.montant * item.quantite)
        })
    }
    subTotalCapAppNumerairePrevisionnel = sum;
    calculTotalCapitalPrevisionnel();
}

export const mixin = {
    mixins: [currenciesMaskMixin],
    data() {
        return {
            totalImmoIncorporellePrevisionnel,
            subTotalImmoCorpMatInfoPrevisionnel,
            subTotalImmoCorpMatMachOutilPrevisionnel,
            subTotalImmoCorpBatLocEspaceVentePrevisionnel,
            subTotalImmoCorpMobilierPrevisionnel,
            subTotalImmoCorpVoitAutrePrevisionnel,
            totalImmoCorporellePrevisionnel,
            subTotalImmoFinDepGarantiePrevisionnel,
            subTotalImmoFinCautionPrevisionnel,
            subTotalImmoFinAutrePrevisionnel,
            totalImmoFinancierePrevisionnel,
            totalBfrOuverturePrevisionnel,
            subTotalBfrStockPrevisionnel,
            subTotalBfrCreanceCliPrevisionnel,
            subTotalBfrDetFourPrevisionnel,
            totalBfrExploitationPrevisionnel,

            subTotalCapAppNumerairePrevisionnel,
            subTotalCapAppNaturePrevisionnel,
            totalCapitalPrevisionnel
        }
    },
    methods: {
        totalPrixPrevisionnelIncorporel,
        totalPrixPrevisionnelCorporel,
        amortissementLineairePrevisionnel,
        totalMontantPrevisionnelFinancier,
        totalCoutPrevisionnelBfrOuverture,
        totalCoutPrevisionnelBfrExploitation,
        calculTotalImmoIncorporellePrevisionnel,
        calculSubTotalImmoCorpMatInfoPrevisionnel,
        calculSubTotalImmoCorpMobilierPrevisionnel,
        calculSubTotalImmoCorpMatMachOutilPrevisionnel,
        calculSubTotalImmoCorpBatLocEspaceVentePrevisionnel,
        calculSubTotalImmoCorpVoitAutrePrevisionnel,
        calculTotalImmoCorporellePrevisionnel,
        calculSubTotalImmoFinCautionPrevisionnel,
        calculSubTotalImmoFinDepGarantiePrevisionnel,
        calculSubTotalImmoFinAutrePrevisionnel,
        calculTotalImmoFinancierePrevisionnel,
        calculTotalBfrOuverturePrevisionnel,
        calculSubTotalBfrStockAnnee1Previsionnel,
        calculSubTotalBfrStockAnnee2Previsionnel,
        calculSubTotalBfrStockAnnee3Previsionnel,
        calculSubTotalBfrCreanceCliAnnee1Previsionnel,
        calculSubTotalBfrCreanceCliAnnee2Previsionnel,
        calculSubTotalBfrCreanceCliAnnee3Previsionnel,
        calculSubTotalBfrDetFourAnnee1Previsionnel,
        calculSubTotalBfrDetFourAnnee2Previsionnel,
        calculSubTotalBfrDetFourAnnee3Previsionnel,
        calculTotalBfrExploitationAnnee1Previsionnel,
        calculTotalBfrExploitationAnnee2Previsionnel,
        calculTotalBfrExploitationAnnee3Previsionnel,
    }
}
