import {arround} from '../../../../services/helper'

export function EcartQuantitePrix(itemSuivi) {
    let total = itemSuivi.prix * itemSuivi.quantite
    let previsionnel = itemSuivi.oldPrix * itemSuivi.oldQuantite
    return previsionnel > 0 ? arround(total*100/previsionnel, 1): 0
}

export function EcartQuantiteMontant(itemSuivi) {
    let total = itemSuivi.montant * itemSuivi.quantite
    let previsionnel = itemSuivi.oldMontant * itemSuivi.oldQuantite
    return previsionnel > 0 ? arround(total*100/previsionnel, 1): 0
}

export function EcartBfrOuverture(itemSuivi) {
    let total = (itemSuivi['coutMensuel'] * itemSuivi.duree * itemSuivi.pourcentage / 100)
    let previsionnel = (itemSuivi['oldCoutMensuel'] * itemSuivi.oldDuree * itemSuivi.oldPourcentage / 100)
    return previsionnel > 0 ? arround(total*100/previsionnel, 1): 0
}

export function EcartBfrExploitation(itemSuivi) {
    let ecartAnnee1 = itemSuivi.oldAnnee1 > 0 ? (itemSuivi.annee1*100/itemSuivi.oldAnnee1): 0
    let ecartAnnee2 = itemSuivi.oldAnnee2 > 0 ? (itemSuivi.annee2*100/itemSuivi.oldAnnee2): 0
    let ecartAnnee3 = itemSuivi.oldAnnee3 > 0 ? (itemSuivi.annee3*100/itemSuivi.oldAnnee3): 0
    return arround((ecartAnnee1 + ecartAnnee2 + ecartAnnee3) / 3, 1)
}


export const mixin = {
    methods: {
        EcartQuantitePrix,
        EcartQuantiteMontant,
        EcartBfrOuverture,
        EcartBfrExploitation,
    }
}