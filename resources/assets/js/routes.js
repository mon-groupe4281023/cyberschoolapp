import VueRouter from 'vue-router'
import helper from './services/helper'
import VueMultiGuard from 'vue-router-multiguard';
import Multiguard from 'vue-router-multiguard';
import firebase from 'firebase'
import { Message, Loading } from 'element-ui';


// let currentUser=firebase.auth().currentUser;
let routes = [
    {
        path: '/',
        component: () => import(/* webpackChunkName: "guest-page-site" */'./layouts/guest-page-site'),

        children: [
            {
                path: '/scaffold',
                name: 'scaffold',
                component: () => import(/* webpackChunkName: "bp.scaffold" */'./views/bp/scaffold'),
            },
            {
                path: '/accueil',
                name: 'accueil',
                component: () => import(/* webpackChunkName: "site.home" */'./views/site/home'),
                beforeEnter: (to, from, next) => {
                    if (to.meta.requiresAuth) {
                        next('/accueil')
                    }
                }
            },
            {
                path: '/',
                alias: '/accueil',
                component: () => import(/* webpackChunkName: "site.home" */'./views/site/home')
            },



        ]
    },
    {
        path: '/',
        alias: '/hello',
        component: () => import(/* webpackChunkName: "guest-page-site" */'./layouts/guest-page-site'),
        children: [
            {
                path: '/inscription',
                component: () => import(/* webpackChunkName: "formation.inscription" */'./views/site/formation/inscription')
            },
            {
                path: '/formations',
                component: () => import(/* webpackChunkName: "formation.index" */'./views/site/formation/index')
            },
            {
                path: '/a_propos',
                component: () => import(/* webpackChunkName: "formation.index" */'./views/site/a_propos')
            },
            {
                meta: { requiresAuth: false },
                path: '/programmes',
                name: "public-programmes",
                component: () => import(/* webpackChunkName: "auth.login" */'./views/site/programmes/programmes')
            },
            {
                path: '/catalogue_photos',
                component: () => import(/* webpackChunkName: "formation.index" */'./views/site//catalogue_photos')
            },
            {
                path: '/formation/:name',
                component: () => import(/* webpackChunkName: "formation.single" */'./views/site/formation/single'),
                props: true
            }
        ]
    },
    {

        path: '/',
        component: () => import(/* webpackChunkName: "default-page-bp" */'./layouts/default-page-bp'),

        children: [

            {
                meta: { requiresAuth: true, windowRedirectAfter: true, },
                path: '/bp/home',
                name: "homebp",
                component: () => import(/* webpackChunkName: "bp.home" */'./views/bp/home'),
                beforeEnter: VueMultiGuard([isLoggedAsParticipant, accessBpHome])

            },

            {
                path: '/blank',
                component: () => import(/* webpackChunkName: "pages.blank" */'./views/pages/blank')
            },
            {
                path: '/configuration',
                component: () => import(/* webpackChunkName: "configuration.configuration" */'./views/configuration/configuration')
            },
            {
                path: '/profile',
                component: () => import(/* webpackChunkName: "user.profile" */'./views/user/profile')
            },
            {
                path: '/task',
                component: () => import(/* webpackChunkName: "task.index" */'./views/task/index')
            },
            {
                path: '/task/:id/edit',
                component: () => import(/* webpackChunkName: "task.edit" */'./views/task/edit')
            },
            {
                path: '/user',
                component: () => import(/* webpackChunkName: "user.index" */'./views/user/index')
            },
        ]
    },
    {
        path: '/',
        component: () => import(/* webpackChunkName: "default2-page-bp" */'./layouts/default2-page-bp'),

        children: [

            {
                meta: { requiresAuth: true },
                path: '/bp/summary/concours',
                component: () => import(/* webpackChunkName: "bp.summary" */'./views/bp/summary'),
                beforeEnter: Multiguard([isLoggedAsParticipant, soummettreProjet]),
                name: 'summary-concours',
                beforeEnter: VueMultiGuard([isLoggedAsParticipant]),
                beforeRouteLeave: VueMultiGuard([accessSummary])

            },
            {
                path: '/bp/summary',
                meta: { requiresAuth: true },
                component: () => import(/* webpackChunkName: "bp.summary" */'./views/bp/summary'),
                name: 'summary',
                beforeEnter: VueMultiGuard([quotaProjets, accessSummary])
            },
            {
                meta: { requiresAuth: true },
                path: '/bp/summary/:id',
                component: () => import(/* webpackChunkName: "bp.singleSummary" */'./views/bp/summary'),
                name: 'singleSummary',
                beforeEnter: VueMultiGuard([accessSummary])
            },
            {
                path: "/bp/summary/:id/print",
                component: () => import(/* webpackChunkName: "bp.exportSummary" */"./views/bp/exportSummary"),
                props: true
            },
            {
                meta: { requiresAuth: true, windowRedirectAfter: true },
                path: '/bp/projets',
                component: () => import(/* webpackChunkName: "bp.selection-projet" */'./views/bp/selection-projet'),
                name: "projets",
                beforeEnter: VueMultiGuard([isLoggedAsParticipant, accessProjetsInterneListe])
            },
            {
                meta: { requiresAuth: true },
                path: '/bp/soumission/projets',
                component: () => import(/* webpackChunkName: "admin.concours.soumissionProjet" */'./views/admin/concours/soumissionProjet'),
                name: 'soumission-projet',
                beforeEnter: VueMultiGuard([isLoggedAsParticipant])

            },
            {
                meta: { requiresAuth: true },
                path: '/bp/profile/user',
                component: () => import(/* webpackChunkName: "bp.profile.user" */'./views/bp/profile/user'),
                beforeEnter: VueMultiGuard([isLoggedAsParticipant, accessLogoEntreprise])
            },

        ]
    },
    {
        path: '/', meta: { requiresAuth: true },
        component: () => import(/* webpackChunkName: "default-page-financier" */'./layouts/default-page-financier'),
        children: [
            {
                meta: { requiresAuth: true },
                path: "/bp/bpfinancier/seuil-rentabilite",
                component: () => import(/* webpackChunkName: "financier.seuilRentabilite" */"./views/bp/financier/seuilRentabilite"),
                name: 'seuilRentabilite',
                beforeEnter: Multiguard([isLoggedAsParticipant, accessSeuilRentabilite])
            },
            {
                meta: { requiresAuth: true },
                path: '/bp/bpfinancier',
                component: () => import(/* webpackChunkName: "financier.dashboard" */'./views/bp/financier/dashboard'),
                name: 'dashboardFinancier',
                beforeEnter: Multiguard([isLoggedAsParticipant, accessDashboardFinancier])
            },
            {
                meta: { requiresAuth: true },
                path: '/bp/bpfinancier/provisionTable',
                component: () => import(/* webpackChunkName: "financier.dashboard" */'./views/bp/financier/provisionTable'),
                name: 'provisionTable',
                beforeEnter: Multiguard([isLoggedAsParticipant, accessDashboardFinancier])
            },
            {
                meta: { requiresAuth: true },
                path: '/bp/bpfinancier/solde-intermediaire-gestion',
                component: () => import(/* webpackChunkName: "financier.soldeGestion" */'./views/bp/financier/soldeGestion'),
                name: 'soldeIntermediaireGestion',
                beforeEnter: Multiguard([isLoggedAsParticipant, accessSoldeIntermediaireGestion])
            },
            {
                meta: { requiresAuth: true },
                path: '/bp/bpfinancier/echeancier-prets-bancaires',
                component: () => import(/* webpackChunkName: "errors.construction" */'./views/errors/construction'),
                name: 'echeancierPretBancaire',
                props: {
                    title: 'Echéancier en cours de construction',
                    link: 'https://forms.gle/c49f9WbBYX4vBdaj7'
                },
                beforeEnter: Multiguard([isLoggedAsParticipant, accessEcheancierPretBancaire])
            },
            {
                meta: { requiresAuth: true },
                path: '/bp/bpfinancier/investissement/besoins',
                component: () => import(/* webpackChunkName: "financier.besoins" */'./views/bp/financier/besoins'),
                name: 'besoins',
                beforeEnter: Multiguard([isLoggedAsParticipant, accessBesoins])

            },
            {
                meta: { requiresAuth: true },
                path: '/bp/bpfinancier/investissement/ressources',
                component: () => import(/* webpackChunkName: "financier.ressources" */'./views/bp/financier/ressources'),
                name: 'ressources',
                beforeEnter: Multiguard([isLoggedAsParticipant, accessRessources])
            },
            {
                meta: { requiresAuth: true },
                path: '/bp/bpfinancier/exploitation/produits',
                component: () => import(/* webpackChunkName: "financier.produits" */'./views/bp/financier/produits'),
                name: 'produits',
                beforeEnter: Multiguard([isLoggedAsParticipant, accessProduits])
            },
            {
                meta: { requiresAuth: true },
                path: '/bp/bpfinancier/exploitation/charges',
                component: () => import(/* webpackChunkName: "financier.charges" */'./views/bp/financier/charges'),
                name: 'charges',
                beforeEnter: Multiguard([isLoggedAsParticipant, accessCharges])
            },
            {
                path: '/bp/bpfinancier/recapitulatif',
                component: () => import(/* webpackChunkName: "financier.recapitulatif" */'./views/bp/financier/recapitulatif'),
                name: 'recapitulatif'
            },
            {
                meta: { requiresAuth: true },
                path: "/bp/bpfinancier/plan-financement",
                component: () => import(/* webpackChunkName: "financier.planFinancement" */"./views/bp/financier/planFinancement"),
                name: 'planFinancement',
                beforeEnter: VueMultiGuard([isLoggedAsParticipant, accessplanFinancement])
            },
            {
                meta: { requiresAuth: true },
                path: "/bp/bpfinancier/compte-resultats",
                component: () => import(/* webpackChunkName: "financier.compteResultats" */"./views/bp/financier/compteResultats"),
                name: 'compteResultat',
                beforeEnter: VueMultiGuard([isLoggedAsParticipant, accessCompteResultat])
            },
            {
                meta: { requiresAuth: true },
                path: "/bp/bpfinancier/plan-tresorerie",
                component: () => import(/* webpackChunkName: "financier.planTresorerie" */"./views/bp/financier/planTresorerie"),
                name: 'planTresorerie',
                beforeEnter: VueMultiGuard([isLoggedAsParticipant, accessPlanTresorerie])
            },
            {
                meta: { requiresAuth: true },
                path: "/bp/bpfinancier/besoin-fonds-roulement",
                component: () => import(/* webpackChunkName: "financier.bfr" */'./views/bp/financier/bfr'),
                name: 'bfr',
                beforeEnter: VueMultiGuard([isLoggedAsParticipant, accessBesoinFondsRoulement])
            },
            {
                meta: { requiresAuth: true },
                path: "/bp/bpfinancier/bilan-previsionnel",
                component: () => import(/* webpackChunkName: "financier.bilanPrevisionnel" */'./views/bp/financier/bilanSyscohada'),
                name: 'bilanPrevisionnel',
                beforeEnter: VueMultiGuard([isLoggedAsParticipant, accessBilanPrevisionnel])
            },
            {
                meta: { requiresAuth: true },
                path: "/bp/bpfinancier/amortissements",
                component: () => import(/* webpackChunkName: "financier.amortissements" */'./views/bp/financier/amortissements'),
                name: 'amortissements',
                beforeEnter: VueMultiGuard([isLoggedAsParticipant, accessAmortissements])
            },

        ]
    },
    {
        path: '/bp/bpeconomique',
        component: () => import(/* webpackChunkName: "errors.construction" */'./views/errors/construction'),
        props: {
            title: 'Business Plan économique en cours de construction',
            link: 'https://forms.gle/c49f9WbBYX4vBdaj7'
        },
    },
    {
        path: '/',
        component: () => import(/* webpackChunkName: "default-suivi-page" */'./layouts/default-suivi-page'),
        children: [

            {
                meta: { requiresAuth: true, windowRedirectAfter: true },
                path: '/bp/suivi',

                component: () => import(/* webpackChunkName: "suivi.home" */'./views/suivi/home'),

                name: 'suivi-home'
            },
            {
                meta: { requiresAuth: true },
                path: '/bp/suivi/dashboard',
                component: () => import(/* webpackChunkName: "suivi.suivi" */'./views/suivi/finances/dashboard'),
                name: 'suivi-dashboard',
            },
            {
                meta: { requiresAuth: true },
                path: '/bp/suivi/recettes',
                component: () => import(/* webpackChunkName: "suivi.finances.recette" */'./views/suivi/finances/recette'),
                name: 'suivi-recettes',
                beforeEnter: Multiguard([isLoggedAsParticipant, accessRecette])
            },
            {
                meta: { requiresAuth: true },
                path: '/bp/suivi/depenses',
                component: () => import(/* webpackChunkName: "suivi.finances.depenses" */'./views/suivi/finances/depenses'),
                name: 'suivi-depenses',
                beforeEnter: Multiguard([isLoggedAsParticipant, accessDepense])
            },
            {
                meta: { requiresAuth: true },
                path: '/bp/suivi/soldes',
                component: () => import(/* webpackChunkName: "suivi.finances.soldes" */'./views/suivi/finances/soldes'),
                name: 'suivi-soldes',
                beforeEnter: Multiguard([isLoggedAsParticipant, accessSoldes])
            },
            {
                path: '/bp/suivi/devis',
                component: () => import(/* webpackChunkName: "suivi.devis.devis" */'./views/suivi/devis/devis'),
                name: 'Devis'
            },
            {
                path: '/bp/suivi/devis/model/:id',
                component: () => import(/* webpackChunkName: "suivi.devis.parametre-devis" */'./views/suivi/devis/parametre-devis'),
                name: 'model-devis'
            },
            {
                path: '/bp/suivi/devis/model/nouveau',
                component: () => import(/* webpackChunkName: "suivi.devis.parametre-devis" */'./views/suivi/devis/parametre-devis'),
                name: 'nouveau-model-devis'
            },
            {
                path: '/bp/suivi/devis/statistiques',
                component: () => import(/* webpackChunkName: "suivi.devis.statistiques" */'./views/suivi/devis/statistiques'),
                name: 'Statistiques'
            },
            {
                path: '/bp/suivi/devis/operations',
                component: () => import(/* webpackChunkName: "suivi.devis.operations" */'./views/suivi/devis/operations'),
                name: 'Operations'
            },
            {
                path: '/bp/suivi/devis/:id',
                component: () => import(/* webpackChunkName: "suivi.devis.formulaire-devis" */'./views/suivi/devis/formulaire-devis'),
                name: 'FormulaireDevis',
                props: true
            },
            {
                path: '/bp/suivi/taches',
                component: () => import(/* webpackChunkName: "suivi.taches.taches" */'./views/suivi/taches/taches'),
                name: 'suivi-taches'
            },
            {
                path: '/bp/suivi/taches/workflow',
                component: () => import(/* webpackChunkName: "suivi.taches.parametres.workflow" */'./views/suivi/taches/parametres/workflow'),
                name: 'taches-workflow'
            },
            {
                path: '/bp/suivi/kanban',
                component: () => import(/* webpackChunkName: "suivi.taches.kanban" */'./views/suivi/taches/kanban'),
                name: 'suivi-kanban'
            },
            {
                path: '/bp/suivi/board',
                component: () => import(/* webpackChunkName: "suivi.taches.board" */'./views/suivi/taches/board'),
                name: 'suivi-board'
            },
            {
                path: '/bp/suivi/produits',
                component: () => import(/* webpackChunkName: "suivi.parametres.produits" */'./views/suivi/parametres/produits'),
                name: 'suivi-produits'
            },
            {
                path: '/bp/suivi/vendeurs',
                component: () => import(/* webpackChunkName: "suivi.parametres.vendeurs" */'./views/suivi/parametres/vendeurs'),
                name: 'suivi-vendeurs'
            },
            {
                path: '/bp/suivi/clients',
                component: () => import(/* webpackChunkName: "suivi.parametres.clients" */'./views/suivi/parametres/clients'),
                name: 'suivi-clients'
            },
            {
                path: '/bp/suivi/pays',
                component: () => import(/* webpackChunkName: "suivi.parametres.pays" */'./views/suivi/parametres/pays'),
                name: 'suivi-pays'
            },
            {
                path: '/bp/suivi/categories',
                component: () => import(/* webpackChunkName: "suivi.parametres.categories" */'./views/suivi/parametres/categories'),
                name: 'suivi-categories'
            },
            {
                path: '/bp/suivi/fisc',
                component: () => import(/* webpackChunkName: "suivi.fisc.home" */'./views/suivi/fisc/home'),
                name: 'suivi-fisc'
            },
            {
                path: '/bp/suivi/fisc/banques',
                component: () => import(/* webpackChunkName: "suivi.fisc.banques" */'./views/suivi/fisc/banques'),
                name: 'suivi-fisc-banque'
            },
            {
                path: '/bp/suivi/acteurs',
                component: () => import(/* webpackChunkName: "suivi.acteurs.home" */'./views/suivi/acteurs/home'),
                name: 'suivi-acteurs'
            },
            {
                path: '/bp/suivi/acteurs/clients',
                component: () => import(/* webpackChunkName: "suivi.acteurs.clients" */'./views/suivi/acteurs/clients'),
                name: 'suivi-acteurs-clients'
            },
            {
                path: '/bp/suivi/acteurs/vendeurs',
                component: () => import(/* webpackChunkName: "suivi.acteurs.clients" */'./views/suivi/acteurs/vendeur'),
                name: 'suivi-acteurs-vendeurs'
            },
            {
                path: '/bp/suivi/acteurs/fournisseurs',
                component: () => import(/* webpackChunkName: "suivi.acteurs.fournisseurs" */'./views/suivi/acteurs/fournisseurs'),
                name: 'suivi-acteurs-fournisseurs'
            },
            {
                path: '/bp/suivi/acteurs/membres',
                component: () => import(/* webpackChunkName: "suivi.acteurs.membres" */'./views/suivi/acteurs/membres'),
                name: 'suivi-acteurs-membres'
            },
            {
                path: '/bp/suivi/acteurs/roles',
                component: () => import(/* webpackChunkName: "suivi.acteurs.roles" */'./views/suivi/acteurs/roles'),
                name: 'suivi-acteurs-roles'
            },
            {
                path: '/bp/suivi/parametres/formulaires/recettes',
                component: () => import(/* webpackChunkName: "suivi.parametres.formulaires.recettes" */'./views/suivi/parametres/formulaires/recettes'),
                name: 'suivi-parametres-formulaires-recettes'
            },
            {
                path: '/bp/suivi/parametres/formulaires/depenses',
                component: () => import(/* webpackChunkName: "suivi.parametres.formulaires.depenses" */'./views/suivi/parametres/formulaires/depenses'),
                name: 'suivi-parametres-formulaires-depenses'

            },
            {
                path: '/bp/suivi/parametres/formulaires/fiscalite',
                component: () => import(/* webpackChunkName: "errors.construction" */'./views/errors/construction'),
                props: {
                    title: 'Fiche d\'evaluation en cours de construction',
                    link: 'https://forms.gle/c49f9WbBYX4vBdaj7'
                },
                name: 'suivi-parametres-formulaires-fiscalite'
            },
            {
                path: '/bp/suivi/objectif',
                component: () => import(/* webpackChunkName: "errors.construction" */'./views/errors/construction'),
                props: {
                    title: 'Gestion des objectifs en cours de construction',
                    link: 'https://forms.gle/c49f9WbBYX4vBdaj7'
                },
                name: 'suivi-objectif'
            },
            {
                path: '/bp/suivi/parametres/formulaire',
                component: () => import(/* webpackChunkName: "errors.construction" */'./views/errors/construction'),
                props: {
                    title: 'Cette page est en cours de construction',
                    link: 'https://forms.gle/c49f9WbBYX4vBdaj7'
                },
            },
            {
                path: '/bp/suivi/parametres/fiscalite',
                component: () => import(/* webpackChunkName: "errors.construction" */'./views/errors/construction'),
                props: {
                    title: 'Cette page est en cours de construction',
                    link: 'https://forms.gle/c49f9WbBYX4vBdaj7'
                },
            },
            {
                path: '/bp/suivi/execbp',
                component: () => import(/* webpackChunkName: "suivi.execbp.index" */'./views/suivi/execbp/index.vue'),
                name: 'suivi-bp-financier'
            },
            {
                path: '/bp/suivi/execbp/execution',
                component: () => import(/* webpackChunkName: "suivi.execbp.execution" */'./views/suivi/execbp/execution.vue'),
                name: 'suivi-execution-financier'
            },
            {
                path: '/bp/suivi/execbp/comparaison',
                component: () => import(/* webpackChunkName: "suivi.execbp.comparaison" */'./views/suivi/execbp/comparaison.vue'),
                name: 'suivi-comparaison-financier'
            }
        ]
    },

    // {
    //     path: '/',
    //     component: () => import(/* webpackChunkName: "default-suivi-page" */'./layouts/default-erp-page'),
    //     children: [

    //         {
    //             meta: { requiresAuth: true, windowRedirectAfter: true },
    //             path: '/bp/erp/home',
    //             component: () => import(/* webpackChunkName: "suivi.home" */'./views/bp/erp/home'),
    //             name: 'erp-home'
    //         },
    //         {
    //             meta: { requiresAuth: true, windowRedirectAfter: true },
    //             path: '/bp/erp/tiers/dashboard',
    //             component: () => import(/* webpackChunkName: "suivi.home" */'./views/bp/erp/tiers/dashboard'),
    //             name: 'erp-tiers-dashboard'
    //         },
    //         {
    //             meta: { requiresAuth: true, windowRedirectAfter: true },
    //             path: '/bp/erp/tiers/clients',
    //             component: () => import(/* webpackChunkName: "suivi.home" */'./views/bp/erp/tiers/clients'),
    //             name: 'erp-tiers-clients'
    //         },
    //         {
    //             meta: { requiresAuth: true, windowRedirectAfter: true },
    //             path: '/bp/erp/tiers/fournisseurs',
    //             component: () => import(/* webpackChunkName: "suivi.home" */'./views/bp/erp/tiers/fournisseurs'),
    //             name: 'erp-tiers-fournisseurs'
    //         },
    //         {
    //             meta: { requiresAuth: true, windowRedirectAfter: true },
    //             path: '/bp/erp/tiers/personnels',
    //             component: () => import(/* webpackChunkName: "suivi.home" */'./views/bp/erp/tiers/personnels'),
    //             name: 'erp-tiers-personnels'
    //         },
    //         {
    //             meta: { requiresAuth: true, windowRedirectAfter: true },
    //             path: '/bp/erp/tiers/autres',
    //             component: () => import(/* webpackChunkName: "suivi.home" */'./views/bp/erp/tiers/autres'),
    //             name: 'erp-tiers-autres'
    //         },
    //         {
    //             meta: { requiresAuth: true, windowRedirectAfter: true },
    //             path: '/bp/erp/articles/dashboard',
    //             component: () => import(/* webpackChunkName: "suivi.home" */'./views/bp/erp/articles/dashboard'),
    //             name: 'erp-articles-dashboard'
    //         },
    //         {
    //             meta: { requiresAuth: true, windowRedirectAfter: true },
    //             path: '/bp/erp/articles/article-vente',
    //             component: () => import(/* webpackChunkName: "suivi.home" */'./views/bp/erp/articles/article-vente'),
    //             name: 'erp-articles-ventes'
    //         },
    //         {
    //             meta: { requiresAuth: true, windowRedirectAfter: true },
    //             path: '/bp/erp/articles/article-achat',
    //             component: () => import(/* webpackChunkName: "suivi.home" */'./views/bp/erp/articles/article-achat'),
    //             name: 'erp-articles-achats'
    //         },
    //         {
    //             meta: { requiresAuth: true, windowRedirectAfter: true },
    //             path: '/bp/erp/ventes/dashboard',
    //             component: () => import(/* webpackChunkName: "suivi.home" */'./views/bp/erp/ventes/dashboard'),
    //             name: 'erp-ventes-dashboard'
    //         },
    //         {
    //             meta: { requiresAuth: true, windowRedirectAfter: true },
    //             path: '/bp/erp/ventes/facturation',
    //             component: () => import(/* webpackChunkName: "suivi.home" */'./views/bp/erp/ventes/facturation'),
    //             name: 'erp-ventes-facturation'
    //         },
    //         {
    //             meta: { requiresAuth: true, windowRedirectAfter: true },
    //             path: '/bp/erp/ventes/reglement',
    //             component: () => import(/* webpackChunkName: "suivi.home" */'./views/bp/erp/ventes/reglement'),
    //             name: 'erp-ventes-reglement'
    //         },
    //         {
    //             meta: { requiresAuth: true, windowRedirectAfter: true },
    //             path: '/bp/erp/achat/dashboard',
    //             component: () => import(/* webpackChunkName: "suivi.home" */'./views/bp/erp/achat/dashboard'),
    //             name: 'erp-achat-dashboard'
    //         },
    //     ]
    // },

    {
        path: '/',
        component: () => import(/* webpackChunkName: "default-moniteur-page" */'./layouts/default-moniteur-page'),
        children: [

            {
                meta: { requiresAuth: true, windowRedirectAfter: true },
                path: '/bp/moniteur',
                component: () => import(/* webpackChunkName: "moniteur.moniteur" */'./views/suivi/moniteur/moniteur'),
                beforeEnter: (to, from, next) => {
                    if (to.meta.requiresAuth) {
                        if (localStorage.getItem("userSession")) {
                            let userInfo = JSON.parse(localStorage.getItem("userSession"))
                            if (userInfo.profile == 4) {
                                next()
                            }
                            else {
                                Message.warning({
                                    dangerouslyUseHTMLString: true,
                                    message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                })
                            }
                        } else {
                            Message.warning({
                                dangerouslyUseHTMLString: true,
                                message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                            })
                        }
                    }
                },
                name: 'moniteur'
            },
            /* {
                path: '/bp/suivi/:projet/recettes',
                component: require('./views/suivi/finances/recette'),
                name: 'suivi-recettes', props: true
            } */
        ]
    },
    {
        path: '/',
        beforeEnter: (to, from, next) => {
            // if (to.meta.requiresAuth) {
            if (localStorage.getItem("userSession")) {
                let userInfo = JSON.parse(localStorage.getItem("userSession"))
                if (userInfo.profile == 2) {
                    next()
                }
                else {
                    Message.warning({
                        dangerouslyUseHTMLString: true,
                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                    })
                }
            } else {
                Message.warning({
                    dangerouslyUseHTMLString: true,
                    message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                })
                // }
            }
        },
        component: () => import(/* webpackChunkName: "default-moniteur-page" */'./layouts/default-superadmin-page'),
        children: [

            {
                // meta: { requiresAuth: true, windowRedirectAfter: true },
                path: '/bp/superadmin/dashboard',
                component: () => import(/* webpackChunkName: "moniteur.moniteur" */'./views/superadmin/dashboard'),

                name: 'superadmin-dashboard'
            },
            {
                // meta: { requiresAuth: true, windowRedirectAfter: true },
                path: '/bp/superadmin/projets',
                component: () => import(/* webpackChunkName: "moniteur.moniteur" */'./views/superadmin/projets/projets'),

                name: 'superadmin-projets'
            },
            {
                // meta: { requiresAuth: true, windowRedirectAfter: true },
                path: '/bp/superadmin/utilisateurs',
                component: () => import(/* webpackChunkName: "moniteur.moniteur" */'./views/superadmin/utilisateurs/utilisateurs'),

                name: 'superadmin-utilisateurs'
            },
            {
                path: '/bp/superadmin/plan-comptable',
                component: () => import(/* webpackChunkName: "suivi.parametres.categories" */'./views/superadmin/plancomptable/plancomptable'),
                name: 'setting-plan-comptable-create'
            },
            {
                path: '/bp/superadmin/plan-comptable',
                component: () => import(/* webpackChunkName: "suivi.parametres.categories" */'./views/superadmin/plancomptable/plancomptable'),
                name: 'setting-plan-comptable-liste'
            },
            /* {
                path: '/bp/suivi/:projet/recettes',
                component: require('./views/suivi/finances/recette'),
                name: 'suivi-recettes', props: true
            } */
        ]
    },

    {
        path: '/',
        component: () => import(/* webpackChunkName: "guest-page" */'./layouts/guest-page'),

        children: [
            {
                meta: { requiresAuth: false },
                path: '/login',
                name: "login",
                component: () => import(/* webpackChunkName: "auth.login" */'./views/auth/login')
            },
            {
                meta: { requiresAuth: false },
                path: '/ticket',
                name: "login-ticket",
                component: () => import(/* webpackChunkName: "auth.login" */'./views/auth/loginTicket')
            },

            {
                path: '/programme/register/:id',
                name: "admin-program-register",
                component: () => import(/* webpackChunkName: "evaluateur.evaluateur" */'./views/admin/concours/previewAutreProgram'),
                beforeEnter: VueMultiGuard([quotaInscription]),
                props: true
            },
            {
                path: '/concours/preview/:id',
                name: "admin-previewConcours",
                component: () => import(/* webpackChunkName: "evaluateur.evaluateur" */'./views/admin/concours/previewConcours'),
                beforeEnter: VueMultiGuard([quotaInscription]),
                props: true
            },
            {
                path: '/programs/all/:id',
                name: "admin-allPrograms",
                component: () => import(/* webpackChunkName: "evaluateur.evaluateur" */'./views/admin/concours/allPrograms'),
                props: true
            },
            {
                path: '/concours/login/:id',
                name: "admin-concoursLogin",
                component: () => import(/* webpackChunkName: "evaluateur.evaluateur" */'./views/admin/concours/concoursLogin')
            },
            {
                path: '/password',
                component: () => import(/* webpackChunkName: "auth.password'" */'./views/auth/password')
            },
            {
                path: '/register',
                name: 'register',
                component: () => import(/* webpackChunkName: "auth.register" */'./views/auth/register')

            },
            {
                path: '/pricing',
                component: () => import(/* webpackChunkName: "auth.pricing" */'./views/auth/pricing')
            },
            {
                path: '/auth/:token/activate',
                component: () => import(/* webpackChunkName: "auth.activate" */'./views/auth/activate')
            },
            {
                path: '/password/reset/:token',
                component: () => import(/* webpackChunkName: "auth.reset" */'./views/auth/reset')
            },
            {
                path: '/auth/social',
                component: () => import(/* webpackChunkName: "auth.social-auth" */'./views/auth/social-auth')
            }

        ]
    },
    {
        path: '/',
        component: () => import(/* webpackChunkName: "default-page-evaluateur" */'./layouts/default-page-evaluateur'),

        children: [
            {
                meta: { requiresAuth: true },
                path: '/bp/evaluateur',
                name: "evaluateur",
                component: () => import(/* webpackChunkName: "evaluateur.evaluateur" */'./views/evaluateur/evaluateur'),
                beforeEnter: (to, from, next) => {
                    if (to.meta.requiresAuth) {
                        if (localStorage.getItem("userSession")) {
                            let userInfo = JSON.parse(localStorage.getItem("userSession"))
                            if (userInfo.profile == 2) {
                                next()
                            }
                            else {
                                Message.warning({
                                    dangerouslyUseHTMLString: true,
                                    message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                })
                            }

                        } else {
                            Message.warning({
                                dangerouslyUseHTMLString: true,
                                message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                            })
                        }


                    }
                }
            },
            {
                meta: { requiresAuth: true },
                path: '/bp/evaluateur/projets',
                name: "evaluateurprojets",
                component: () => import(/* webpackChunkName: "evaluateur.evaluateurprojets" */'./views/evaluateur/evaluateurprojets'),

            },
            {
                path: '/bp/evaluateur/projets/:id',
                name: "evaluateurprojet",
                component: () => import(/* webpackChunkName: "evaluateur.evaluateur" */'./views/evaluateur/evaluateur')
            },
            {
                path: '/bp/profile/evaluateur',
                component: () => import(/* webpackChunkName: "profile.userevaluateur" */'./views/bp/profile/userevaluateur')
            },
        ]
    },
    {
        path: '/',

        component: () => import(/* webpackChunkName: "default-moniteur-page" */'./layouts/default-admin-page'),
        children: [

            {
                meta: { requiresAuth: true },
                path: '/bp/admin/dashboard',
                name: "admin-dashboard",
                component: () => import(/* webpackChunkName: "evaluateur.evaluateur" */'./views/admin/dashboard'),
                beforeEnter: VueMultiGuard([isLoggedIn, accessDashboard])

            },
            {
                meta: { requiresAuth: true },
                path: '/bp/admin/concours/creer',
                name: "admin-concours-creer",
                component: () => import(/* webpackChunkName: "evaluateur.evaluateur" */'./views/admin/concours/concours'),
                beforeEnter: VueMultiGuard([isLoggedIn, accessCreer])
            },
            {
                meta: { requiresAuth: true },
                path: '/bp/admin/concours/liste',
                name: "admin-concours-liste",
                component: () => import(/* webpackChunkName: "evaluateur.evaluateur" */'./views/admin/concours/concours'),
                beforeEnter: VueMultiGuard([isLoggedIn, accessListe])
            },
            {
                meta: { requiresAuth: true },
                path: '/bp/admin/concours/participants',
                name: "admin-concours-participants",
                component: () => import(/* webpackChunkName: "evaluateur.evaluateur" */'./views/admin/concours/participants'),
                beforeEnter: VueMultiGuard([isLoggedIn, accessParticipants])
            },
            {
                meta: { requiresAuth: true },
                path: '/bp/admin/programmes/emploi-de-temps',
                name: "admin-programmes-emploi-de-temps",
                component: () => import(/* webpackChunkName: "evaluateur.evaluateur" */'./views/admin/concours/emploiDeTemps'),
                beforeEnter: VueMultiGuard([isLoggedIn])
            },
            {
                meta: { requiresAuth: true },
                path: '/bp/admin/concours/projets',
                name: "admin-concours-projets",
                component: () => import(/* webpackChunkName: "evaluateur.evaluateur" */'./views/admin/concours/projetsConcours'),
                beforeEnter: VueMultiGuard([isLoggedIn, accessProjetsConcours])
            },
            {
                meta: { requiresAuth: true },
                path: '/bp/admin/evaluation/projets',
                name: "admin-evaluation-projets",
                component: () => import(/* webpackChunkName: "evaluateur.evaluateur" */'./views/admin/evaluation/projetsList'),
                beforeEnter: VueMultiGuard([isLoggedIn, accessEvaluateurProjets])
            },
            {
                meta: { requiresAuth: true },
                path: '/bp/admin/evaluation/:id',
                name: "admin-evaluation-evaluationprojet",
                component: () => import(/* webpackChunkName: "evaluateur.evaluateur" */'./views/admin/evaluation/evaluationprojet'),
                beforeEnter: VueMultiGuard([isLoggedIn, accessEvaluateurProjets])
            },
            {
                meta: { requiresAuth: true },
                path: '/bp/admin/parametres/membres/utilisateur',
                name: "setting-members-user",
                component: () => import(/* webpackChunkName: "evaluateur.evaluateur" */'./views/admin/parametres/membres/utilisateur'),
                beforeEnter: VueMultiGuard([isLoggedIn, accessSettingUser])
            },
            {
                meta: { requiresAuth: true },
                path: '/bp/admin/parametres/membres/role',
                name: "setting-members-role",
                component: () => import(/* webpackChunkName: "evaluateur.evaluateur" */'./views/admin/parametres/membres/role'),
                beforeEnter: VueMultiGuard([isLoggedIn, accessSettingRole])
            },

        ]
    },

 
    {
        path: '*',
        component: () => import(/* webpackChunkName: "error-page" */'./layouts/error-page'),
        children: [
            {
                path: '*',
                component: () => import(/* webpackChunkName: "page-not-found" */'./views/errors/page-not-found')
            }
        ]
    }
];

const router = new VueRouter({
    routes,
    linkActiveClass: 'active',
    mode: 'history'
});

function isLoggedIn(to, from, next) {
    if (to.meta.requiresAuth) {
        let userInfo = JSON.parse(localStorage.getItem("userSession"))
        if (userInfo.profile == 4) {

            next()
        }
        else {
            // console.log('Bad')
            Message.warning({
                dangerouslyUseHTMLString: true,
                message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
            })
        }
    }
}

function accessDashboard(to, from, next) {
    let perm = JSON.parse(localStorage.getItem("userSession")).permissions.admin
    perm.forEach(item => {
        if (item.route == 'admin-dashboard') {
            if (item.checked) {

                next()
                // console.log(from)
            }
            else {
                // console.log('Bad')
                Message.warning({
                    dangerouslyUseHTMLString: true,
                    message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                })
            }

        }
    })

}
function accessCreer(to, from, next) {
    firebase.firestore().collection('users').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
        let obj = doc.data();
        if (obj.adminId) {
            Loading.service({ fullscreen: true });
            obj.roleId.get().then(roleUser => {
                let perm = roleUser.data().permissions.admin;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'admin-concours-creer') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    to.params.access = 'creer'
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })


            })
        }
        else {
            Loading.service({ fullscreen: true });
            firebase.firestore().collection('permissions').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
                let obj = doc.data()
                //  console.log('Mon Log',doc.data())
                let perm = doc.data().admin;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'admin-concours-creer') {
                                if (itemcontent.checked) {
                                    to.params.data = itemcontent.value
                                    to.params.access = 'creer'
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })

            })

        }


    })


}
function accessListe(to, from, next) {
    firebase.firestore().collection('users').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
        let obj = doc.data();
        if (obj.adminId) {
            Loading.service({ fullscreen: true });
            obj.roleId.get().then(roleUser => {
                let perm = roleUser.data().permissions.admin;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'admin-concours-liste') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    to.params.access = 'liste'
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })


            })
        }
        else {
            Loading.service({ fullscreen: true });
            firebase.firestore().collection('permissions').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
                let obj = doc.data()
                //  console.log('Mon Log',doc.data())
                let perm = doc.data().admin;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'admin-concours-liste') {
                                if (itemcontent.checked) {
                                    to.params.data = itemcontent.value
                                    to.params.access = 'liste'
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })

            })

        }


    })


}

function accessParticipants(to, from, next) {
    firebase.firestore().collection('users').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
        let obj = doc.data();
        if (obj.adminId) {
            Loading.service({ fullscreen: true });
            obj.roleId.get().then(roleUser => {
                let perm = roleUser.data().permissions.admin;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'admin-concours-participants') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })


            })
        }
        else {
            Loading.service({ fullscreen: true });
            firebase.firestore().collection('permissions').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
                let obj = doc.data()
                //  console.log('Mon Log',doc.data())
                let perm = doc.data().admin;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'admin-concours-participants') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })

            })

        }


    })


}

function accessProjetsConcours(to, from, next) {
    firebase.firestore().collection('users').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
        let obj = doc.data();
        if (obj.adminId) {
            Loading.service({ fullscreen: true });
            obj.roleId.get().then(roleUser => {
                let perm = roleUser.data().permissions.admin;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'admin-concours-projets') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })


            })
        }
        else {
            Loading.service({ fullscreen: true });
            firebase.firestore().collection('permissions').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
                let obj = doc.data()
                //  console.log('Mon Log',doc.data())
                let perm = doc.data().admin;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'admin-concours-projets') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })

            })

        }


    })
}



function accessEvaluateurProjets(to, from, next) {
    firebase.firestore().collection('users').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
        let obj = doc.data();
        if (obj.adminId) {
            Loading.service({ fullscreen: true });
            obj.roleId.get().then(roleUser => {
                let perm = roleUser.data().permissions.admin;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'admin-evaluation-projets') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })


            })
        }
        else {
            Loading.service({ fullscreen: true });
            firebase.firestore().collection('permissions').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
                let obj = doc.data()
                //  console.log('Mon Log',doc.data())
                let perm = doc.data().admin;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'admin-evaluation-projets') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })

            })

        }


    })
}
// function accessEvaluateurProjets(to,from,next){
//     let perm = JSON.parse(localStorage.getItem("userSession")).permissions.admin
//     perm.forEach(item=>{
//     if(item.haschildren==true){
//         //    console.log(item.haschildren)
//             item.content.forEach(itemcontent=>{
//                 if(itemcontent.route=='admin-evaluation-projets'){
//                     if(itemcontent.checked){


//                         next()
//                         // console.log(itemcontent.checked)
//                     }
//                     else{
//                         // console.log('Bad')
//                               Message.warning({
//                                     dangerouslyUseHTMLString: true,
//                                     message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
//                                 } )
//                     }

//                 }

//             })
//         }


//     })
// }



function accessProjetsInterne(to, from, next) {
    let perm = JSON.parse(localStorage.getItem("userSession")).permissions.admin
    perm.forEach(item => {
        if (item.haschildren == true) {
            //    console.log(item.haschildren)
            item.content.forEach(itemcontent => {
                if (itemcontent.route == 'admin-projets-créer') {
                    if (itemcontent.checked) {


                        next()
                        // console.log(itemcontent.checked)
                    }
                    else {
                        // console.log('Bad')
                        Message.warning({
                            dangerouslyUseHTMLString: true,
                            message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                        })
                    }

                }

            })
        }


    })
}

async function accessProjetsInterneListe(to, from, next) {

    let userSession = JSON.parse(localStorage.getItem('userSession'));
    userSession.projet = '';
    userSession.projettitre = '';
    //Admin perm
    localStorage.setItem('userSession', JSON.stringify(userSession));
    firebase.firestore().collection('users').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
        let obj = doc.data();
        if (obj.adminId) {

            obj.roleId.get().then(roleUser => {
                let perm = roleUser.data().permissions.admin;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'projets') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })


            })
        }
        else {
            Loading.service({ fullscreen: true });

            let userSession = JSON.parse(localStorage.getItem('userSession'));
            userSession.projet = '';
            userSession.projettitre = '';
            //Admin perm
            localStorage.setItem('userSession', JSON.stringify(userSession));
            firebase.firestore().collection('permissions').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
                let obj = doc.data()
                //  console.log('Mon Log',doc.data())
                let perm = doc.data().admin;
                perm.forEach(item => {
                    if (item.haschildren == true) {

                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'projets') {

                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })

            })

        }


    })
}

function accessSettingRole(to, from, next) {
    let userSession = JSON.parse(localStorage.getItem('userSession'));
    userSession.projet = '';
    userSession.projettitre = '';
    //Admin perm
    localStorage.setItem('userSession', JSON.stringify(userSession));
    firebase.firestore().collection('users').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
        let obj = doc.data();
        if (obj.adminId) {
            Loading.service({ fullscreen: true });
            obj.roleId.get().then(roleUser => {
                let perm = roleUser.data().permissions.admin;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'setting-members-role') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })


            })
        }
        else {
            Loading.service({ fullscreen: true });

            let userSession = JSON.parse(localStorage.getItem('userSession'));
            userSession.projet = '';
            userSession.projettitre = '';
            //Admin perm
            localStorage.setItem('userSession', JSON.stringify(userSession));
            firebase.firestore().collection('permissions').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
                let obj = doc.data()
                //  console.log('Mon Log',doc.data())
                let perm = doc.data().admin;
                perm.forEach(item => {
                    if (item.haschildren == true) {

                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'setting-members-role') {

                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })

            })

        }


    })
}

function accessSettingUser(to, from, next) {

    firebase.firestore().collection('users').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
        let obj = doc.data();
        if (obj.adminId) {
            Loading.service({ fullscreen: true });
            obj.roleId.get().then(roleUser => {
                let perm = roleUser.data().permissions.admin;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'setting-members-user') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })


            })
        }
        else {
            Loading.service({ fullscreen: true });
            firebase.firestore().collection('permissions').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
                let obj = doc.data()
                //  console.log('Mon Log',doc.data())
                let perm = doc.data().admin;
                perm.forEach(item => {
                    if (item.haschildren == true) {

                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'setting-members-user') {

                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })

            })

        }


    })
}
function accessLogoEntreprise(to, from, next) {
    Loading.service({ fullscreen: true });
    firebase.firestore().collection('users').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
        let obj = doc.data();
        if (!obj.adminId) {

            let accesslogo = {
                logoEntreprise: {
                    'value': true
                }
            }
            to.params.data = accesslogo
            to.params.user = obj
            next()
        }
        else {

            let accesslogo = {
                logoEntreprise: {
                    'value': false
                }
            }
            to.params.data = accesslogo

            next()
        }


    })
}

// function accessSettingUser(to,from,next){
//     let perm = JSON.parse(localStorage.getItem("userSession")).permissions.admin
//     perm.forEach(item=>{
//     if(item.haschildren==true){
//         //    console.log(item.haschildren)
//             item.content.forEach(itemcontent=>{
//                 if(itemcontent.route=='setting-members-user'){
//                     if(itemcontent.checked){


//                         next()
//                         // console.log(itemcontent.checked)
//                     }
//                     else{
//                         // console.log('Bad')
//                               Message.warning({
//                                     dangerouslyUseHTMLString: true,
//                                     message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
//                                 } )
//                     }

//                 }

//             })
//         }


//     })
// }

function isLoggedAsParticipant(to, from, next) {
    Loading.service({ fullscreen: true });
    if (to.meta.requiresAuth) {
        let userInfo = JSON.parse(localStorage.getItem("userSession"))
        if (userInfo.profile == '3' || userInfo.profile == '4') {

            next()
        }
        else {
            // console.log('Bad')
            Message.warning({
                dangerouslyUseHTMLString: true,
                message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
            })
        }
    }
}
function soummettreProjet(to, from, next) {
    if (to.meta.requiresAuth) {
        let perm = JSON.parse(localStorage.getItem("userSession"))
        if (perm.permissions.executiveSummary.state && perm.etatConcours) {

            next()
        }
        else {
            // console.log('Bad')
            Message.warning({
                dangerouslyUseHTMLString: true,
                message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
            })
        }
    }
}
router.beforeEach((to, from, next) => {
    Loading.service({ fullscreen: true });
    let matchedObject = from.matched.slice().reverse().find(r => r.meta && r.meta.hasOwnProperty('windowRedirectAfter'));
    // $loading({fullscreen:true})
    //  let loadingInstance=Loading.service({ fullscreen: true });
    if (matchedObject) {
        if (matchedObject.meta.windowRedirectAfter === true) {
            window.location.href = to.fullPath;
            return;
        }
    }
    if (to.meta.requiresAuth) {
        firebase.firestore().collection('users').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
            let obj = doc.data();
            let currentuser = firebase.auth().currentUser
            // console.log('Displayname',currentuser.displayName)
            obj.displayName = currentuser.displayName
            // console.log('Objet',obj)
            to.params.user = obj
            to.params.displayName = currentuser.displayName
            return next()
        })
    } else {
        return next()
    }



});
function accessDashboardFinancier(to, from, next) {
    firebase.firestore().collection('users').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
        let obj = doc.data();
        if (obj.adminId && !obj.idConcours) {
            Loading.service({ fullscreen: true });
            obj.roleId.get().then(roleUser => {
                let perm = roleUser.data().permissions.bpFinancier;
                console.log(perm)
                perm.forEach(item => {
                    if (item.haschildren == false) {

                        if (item.route == 'dashboardFinancier') {
                            if (item.checked) {

                                to.params.data = item.value
                                next()
                                // console.log(itemcontent.checked)
                            }
                            else {
                                // console.log('Bad')
                                Message.warning({
                                    dangerouslyUseHTMLString: true,
                                    message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                })

                            }

                        }


                    }


                })


            })
        }
        if (!obj.adminId || obj.idConcours) {
            // alert('Hello')
            Loading.service({ fullscreen: true });
            firebase.firestore().collection('permissions').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
                let obj = doc.data()
                //  console.log('Mon Log',doc.data())
                let perm = doc.data().bpFinancier;
                perm.forEach(item => {
                    if (item.route == 'dashboardFinancier') {
                        if (item.checked) {
                            to.params.data = item.value
                            next()
                            // console.log(itemcontent.checked)
                        }
                        else {
                            // console.log('Bad')
                            Message.warning({
                                dangerouslyUseHTMLString: true,
                                message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                            })

                        }

                    }





                })

            })

        }


    })


}
function accessplanFinancement(to, from, next) {
    firebase.firestore().collection('users').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
        let obj = doc.data();
        if (obj.adminId && !obj.idConcours) {
            Loading.service({ fullscreen: true });
            obj.roleId.get().then(roleUser => {
                let perm = roleUser.data().permissions.bpFinancier;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'planFinancement') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })


            })
        }
        if (!obj.adminId || obj.idConcours) {
            Loading.service({ fullscreen: true });
            firebase.firestore().collection('permissions').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
                let obj = doc.data()
                //  console.log('Mon Log',doc.data())
                let perm = doc.data().bpFinancier;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'planFinancement') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })

            })

        }


    })


}

// function accessplanFinancement(to,from,next){
//     if (to.meta.requiresAuth){
//         let perm = JSON.parse(localStorage.getItem("userSession")).permissions.bpFinancier
//         perm.forEach(item=>{
//             if(item.haschildren==true){
//                 //    console.log(item.haschildren)
//                     item.content.forEach(itemcontent=>{
//                         if(itemcontent.route=='planFinancement'){
//                             if(itemcontent.checked){


//                                 next()
//                                 // console.log(itemcontent.checked)
//                             }
//                             else{
//                                 // console.log('Bad')
//                                       Message.warning({
//                                     dangerouslyUseHTMLString: true,
//                                     message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
//                                 } )
//                             }

//                         }

//                     })
//                 }


//             })
//     }


// }


function accessRessources(to, from, next) {
    if (to.meta.requiresAuth) {
        let perm = JSON.parse(localStorage.getItem("userSession")).permissions.bpFinancier
        perm.forEach(item => {
            if (item.haschildren == true) {
                //    console.log(item.haschildren)
                item.content.forEach(itemcontent => {
                    if (itemcontent.route == 'ressources') {
                        if (itemcontent.checked) {


                            next()
                            // console.log(itemcontent.checked)
                        }
                        else {
                            // console.log('Bad')
                            Message.warning({
                                dangerouslyUseHTMLString: true,
                                message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                            })
                        }

                    }

                })
            }


        })
    }


}
function accessBesoins(to, from, next) {
    if (to.meta.requiresAuth) {
        let perm = JSON.parse(localStorage.getItem("userSession")).permissions.bpFinancier
        perm.forEach(item => {
            if (item.haschildren == true) {
                //    console.log(item.haschildren)
                item.content.forEach(itemcontent => {
                    if (itemcontent.route == 'besoins') {
                        if (itemcontent.checked) {


                            next()
                            // console.log(itemcontent.checked)
                        }
                        else {
                            // console.log('Bad')
                            Message.warning({
                                dangerouslyUseHTMLString: true,
                                message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                            })
                        }

                    }

                })
            }


        })
    }


}

function accessProduits(to, from, next) {
    if (to.meta.requiresAuth) {
        let perm = JSON.parse(localStorage.getItem("userSession")).permissions.bpFinancier
        perm.forEach(item => {
            if (item.haschildren == true) {
                //    console.log(item.haschildren)
                item.content.forEach(itemcontent => {
                    if (itemcontent.route == 'produits') {
                        if (itemcontent.checked) {


                            next()
                            // console.log(itemcontent.checked)
                        }
                        else {
                            // console.log('Bad')
                            Message.warning({
                                dangerouslyUseHTMLString: true,
                                message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                            })
                        }

                    }

                })
            }


        })
    }


}
function accessCharges(to, from, next) {
    if (to.meta.requiresAuth) {
        let perm = JSON.parse(localStorage.getItem("userSession")).permissions.bpFinancier
        perm.forEach(item => {
            if (item.haschildren == true) {
                //    console.log(item.haschildren)
                item.content.forEach(itemcontent => {
                    if (itemcontent.route == 'charges') {
                        if (itemcontent.checked) {


                            next()
                            // console.log(itemcontent.checked)
                        }
                        else {
                            // console.log('Bad')
                            Message.warning({
                                dangerouslyUseHTMLString: true,
                                message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                            })
                        }

                    }

                })
            }


        })
    }


}

function accessCompteResultat(to, from, next) {
    firebase.firestore().collection('users').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
        let obj = doc.data();
        if (obj.adminId && !obj.idConcours) {
            Loading.service({ fullscreen: true });
            obj.roleId.get().then(roleUser => {
                let perm = roleUser.data().permissions.bpFinancier;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'compteResultat') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })


            })
        }
        if (!obj.adminId || obj.idConcours) {
            Loading.service({ fullscreen: true });
            firebase.firestore().collection('permissions').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
                let obj = doc.data()
                //  console.log('Mon Log',doc.data())
                let perm = doc.data().bpFinancier;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'compteResultat') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })

            })

        }


    })


}

function accessPlanTresorerie(to, from, next) {
    firebase.firestore().collection('users').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
        let obj = doc.data();
        if (obj.adminId && !obj.idConcours) {
            Loading.service({ fullscreen: true });
            obj.roleId.get().then(roleUser => {
                let perm = roleUser.data().permissions.bpFinancier;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'planTresorerie') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })


            })
        }
        if (!obj.adminId || obj.idConcours) {
            Loading.service({ fullscreen: true });
            firebase.firestore().collection('permissions').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
                let obj = doc.data()
                //  console.log('Mon Log',doc.data())
                let perm = doc.data().bpFinancier;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'planTresorerie') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })

            })

        }


    })


}

async function quotaInscription(to, from, next) {
    let inscriptions = 0
    let loadingInstance1 = Loading.service({ fullscreen: true });
    firebase.firestore().collection('concours').doc(to.params.id).get().then((doc) => {
        let promiseUser = new Promise((resolve, reject) => {
            firebase.firestore().collection('users').where('idConcours', '==', firebase.firestore().collection('concours').doc(to.params.id)).get()
                .then((snapshot) => {
                    if (!snapshot.empty) {
                        snapshot.forEach(() => {
                            inscriptions++
                        })
                        resolve(inscriptions)
                    }
                    else {
                        reject(inscriptions)
                    }
                })
        })
        Promise.allSettled([promiseUser]).then(() => {
            let quota = doc.data().participants
            // console.log("Participants",inscriptions)
            console.log("Quota", quota)
            if (quota == inscriptions || inscriptions > quota) {
                Message.warning({
                    dangerouslyUseHTMLString: true,
                    message: '<strong>Le quota d\'inscriptions a été atteint </strong> ',
                })
                loadingInstance1.close();
                next(from.path)
            } else {
                next()
            }
        })

    })

}


function accessBesoinFondsRoulement(to, from, next) {
    firebase.firestore().collection('users').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
        let obj = doc.data();
        if (obj.adminId && !obj.idConcours) {
            Loading.service({ fullscreen: true });
            obj.roleId.get().then(roleUser => {
                let perm = roleUser.data().permissions.bpFinancier;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'bfr') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })


            })
        }
        if (!obj.adminId || obj.idConcours) {
            Loading.service({ fullscreen: true });
            firebase.firestore().collection('permissions').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
                let obj = doc.data()
                //  console.log('Mon Log',doc.data())
                let perm = doc.data().bpFinancier;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'bfr') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })

            })

        }


    })


}

function accessBilanPrevisionnel(to, from, next) {
    firebase.firestore().collection('users').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
        let obj = doc.data();
        if (obj.adminId && !obj.idConcours) {
            Loading.service({ fullscreen: true });
            obj.roleId.get().then(roleUser => {
                let perm = roleUser.data().permissions.bpFinancier;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'bilanPrevisionnel') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })


            })
        }
        if (!obj.adminId || obj.idConcours) {
            Loading.service({ fullscreen: true });
            firebase.firestore().collection('permissions').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
                let obj = doc.data()
                //  console.log('Mon Log',doc.data())
                let perm = doc.data().bpFinancier;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'bilanPrevisionnel') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })

            })

        }


    })


}

function accessAmortissements(to, from, next) {
    firebase.firestore().collection('users').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
        let obj = doc.data();
        if (obj.adminId && !obj.idConcours) {
            Loading.service({ fullscreen: true });
            obj.roleId.get().then(roleUser => {
                let perm = roleUser.data().permissions.bpFinancier;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'amortissements') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })


            })
        }
        if (!obj.adminId || obj.idConcours) {
            Loading.service({ fullscreen: true });
            firebase.firestore().collection('permissions').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
                let obj = doc.data()
                //  console.log('Mon Log',doc.data())
                let perm = doc.data().bpFinancier;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'amortissements') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })

            })

        }


    })


}


function accessEcheancierPretBancaire(to, from, next) {
    firebase.firestore().collection('users').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
        let obj = doc.data();
        if (obj.adminId && !obj.idConcours) {
            Loading.service({ fullscreen: true });
            obj.roleId.get().then(roleUser => {
                let perm = roleUser.data().permissions.bpFinancier;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'echeancierPretBancaire') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })


            })
        }
        if (!obj.adminId || obj.idConcours) {
            Loading.service({ fullscreen: true });
            firebase.firestore().collection('permissions').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
                let obj = doc.data()
                //  console.log('Mon Log',doc.data())
                let perm = doc.data().bpFinancier;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'echeancierPretBancaire') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })

            })

        }


    })


}

function accessSoldeIntermediaireGestion(to, from, next) {
    firebase.firestore().collection('users').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
        let obj = doc.data();
        if (obj.adminId && !obj.idConcours) {
            Loading.service({ fullscreen: true });
            obj.roleId.get().then(roleUser => {
                let perm = roleUser.data().permissions.bpFinancier;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'soldeIntermediaireGestion') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })


            })
        }
        if (!obj.adminId || obj.idConcours) {
            Loading.service({ fullscreen: true });
            firebase.firestore().collection('permissions').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
                let obj = doc.data()
                //  console.log('Mon Log',doc.data())
                let perm = doc.data().bpFinancier;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'soldeIntermediaireGestion') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })

            })

        }


    })


}

function accessSeuilRentabilite(to, from, next) {
    firebase.firestore().collection('users').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
        let obj = doc.data();
        if (obj.adminId && !obj.idConcours) {
            Loading.service({ fullscreen: true });
            obj.roleId.get().then(roleUser => {
                let perm = roleUser.data().permissions.bpFinancier;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'seuilRentabilite') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })


            })
        }
        if (!obj.adminId || obj.idConcours) {
            Loading.service({ fullscreen: true });
            firebase.firestore().collection('permissions').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
                let obj = doc.data()
                //  console.log('Mon Log',doc.data())
                let perm = doc.data().bpFinancier;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'seuilRentabilite') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })

            })

        }


    })


}

//PERMISSION SUIVI
function accessRecette(to, from, next) {
    firebase.firestore().collection('users').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
        let obj = doc.data();
        if (obj.adminId && !obj.idConcours) {
            Loading.service({ fullscreen: true });
            obj.roleId.get().then(roleUser => {
                let perm = roleUser.data().permissions.suivi;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'suivi-recettes') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })


            })
        }
        if (!obj.adminId || obj.idConcours) {
            Loading.service({ fullscreen: true });
            firebase.firestore().collection('permissions').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
                let obj = doc.data()
                console.log('Mon Log', doc.data())
                let perm = doc.data().suivi;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'suivi-recettes') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })

            })

        }


    })


}
function accessDepense(to, from, next) {
    firebase.firestore().collection('users').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
        let obj = doc.data();
        if (obj.adminId && !obj.idConcours) {
            Loading.service({ fullscreen: true });
            obj.roleId.get().then(roleUser => {
                let perm = roleUser.data().permissions.suivi;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'suivi-depenses') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })


            })
        }
        if (!obj.adminId || obj.idConcours) {
            Loading.service({ fullscreen: true });
            firebase.firestore().collection('permissions').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
                let obj = doc.data()
                console.log('Mon Log', doc.data())
                let perm = doc.data().suivi;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'suivi-depenses') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })

            })

        }


    })


}

function accessSoldes(to, from, next) {
    firebase.firestore().collection('users').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
        let obj = doc.data();
        if (obj.adminId && !obj.idConcours) {
            Loading.service({ fullscreen: true });
            obj.roleId.get().then(roleUser => {
                let perm = roleUser.data().permissions.suivi;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'suivi-soldes') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })


            })
        }
        if (!obj.adminId || obj.idConcours) {
            Loading.service({ fullscreen: true });
            firebase.firestore().collection('permissions').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
                let obj = doc.data()
                console.log('Mon Log', doc.data())
                let perm = doc.data().suivi;
                perm.forEach(item => {
                    if (item.haschildren == true) {
                        //    console.log(item.haschildren)
                        item.content.forEach(itemcontent => {
                            if (itemcontent.route == 'suivi-soldes') {
                                if (itemcontent.checked) {

                                    to.params.data = itemcontent.value
                                    next()
                                    // console.log(itemcontent.checked)
                                }
                                else {
                                    // console.log('Bad')
                                    Message.warning({
                                        dangerouslyUseHTMLString: true,
                                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                                    })

                                }

                            }

                        })
                    }


                })

            })

        }


    })


}

function accessBpHome(to, from, next) {
    firebase.firestore().collection('users').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
        let obj = doc.data();
        Loading.service({ fullscreen: true });
        if (obj.adminId) {
            if (obj.roleId) {

                obj.roleId.get().then(roleUser => {
                    let perm = roleUser.data().permissions;
                    to.params.data = perm
                    next()


                })
            }

        }
        if (obj.idConcours) {
            Loading.service({ fullscreen: true });
            firebase.firestore().collection('permissions').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
                let obj = doc.data()
                let perm = doc.data();
                if (perm.executiveSummary.state) {
                    to.params.data = perm
                    next()

                }
                else {
                    // next({path:from.path})
                    // console.log('route courante',this.$route.path)
                    Message.warning({
                        dangerouslyUseHTMLString: true,
                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                    })
                }

            })

        }
        if (!obj.adminId) {
            Loading.service({ fullscreen: true });
            firebase.firestore().collection('permissions').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
                let obj = doc.data()
                console.log('Mon Log', doc.data())
                let perm = doc.data();
                to.params.data = perm
                next()

            })

        }

    })


}
function accessSummary(to, from, next) {
    firebase.firestore().collection('users').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
        let obj = doc.data();
        // console.log('Log user',obj)
        if (obj.adminId && !obj.idConcours) {
            Loading.service({ fullscreen: true });
            if (obj.roleId) {
                obj.roleId.get().then(roleUser => {
                    let perm = roleUser.data().permissions;
                    if (perm.executiveSummary.state) {
                        to.params.data = perm
                        next()

                    }
                    else {
                        // console.log('route courante',this.$route.path)
                        Message.warning({
                            dangerouslyUseHTMLString: true,
                            message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                        })
                    }


                })
            }
        }

        if (!obj.adminId || obj.idConcours) {
            Loading.service({ fullscreen: true });
            firebase.firestore().collection('permissions').doc(JSON.parse(localStorage.getItem("userSession")).uid).get().then((doc) => {
                let obj = doc.data()
                let perm = doc.data();
                if (perm.executiveSummary.state) {
                    to.params.data = perm
                    next()

                }
                else {
                    // next({path:from.path})
                    // console.log('route courante',this.$route.path)
                    Message.warning({
                        dangerouslyUseHTMLString: true,
                        message: '<strong>Vous n\'avez pas les droits d\'accès à cette ressource</strong> ',
                    })
                }

            })
        }

    })


}
function quotaProjets(to, from, next) {
    if (to.meta.requiresAuth) {
        let quotaProjets = JSON.parse(localStorage.getItem("userSession")).permissions.quotaProjets
        firebase.firestore().collection('summary').where('user', '==', JSON.parse(localStorage.getItem("userSession")).uid).get().then((querySnapshot) => {

            //    let count=Object.keys(querySnapshot).length
            let count = 0
            querySnapshot.forEach(() => {
                //    console.log('data',doc.data())
                count++
            }
            )
            if (quotaProjets == count) {
                // next()
                // this.$notify.error({
                //     title: 'Error',
                //     message: 'Erreur'
                //     });
                // next(false)
                Message({
                    dangerouslyUseHTMLString: true,
                    message: '<strong>Vous avez atteint votre quota de création de projets</strong>',
                })
                //    console.log('Nombre de projets',count)

            }
            else {
                let userSession = JSON.parse(localStorage.getItem('userSession'));
                userSession.projet = '';
                userSession.projettitre = '';
                //Admin perm
                localStorage.setItem('userSession', JSON.stringify(userSession));
                next()

            }

        })

    }
}

function selectionProjets(to, from, next) {
    if (to.meta.requiresAuth) {

        let userSession = JSON.parse(localStorage.getItem('userSession'));
        userSession.projet = '';
        userSession.projettitre = '';
        //Admin perm
        localStorage.setItem('userSession', JSON.stringify(userSession));
        next()

    }
}
export default router;
