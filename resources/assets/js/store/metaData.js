import firebase from "firebase"

/**
 *
 * @param userId
 * @param otherImages
 * @returns {Promise<*>}
 */

export default async function getImage (userId, otherImages = false) {
    let userLogo = ''
    const userDoc = await firebase.firestore()
        .collection('users')
        .doc(userId)
        .get()
    const user = await userDoc.data()
    if (user.hasOwnProperty('logoPdf')) {
        userLogo = {
           'logo': user.logoPdf,
           'police': user.police?user.police:"",
           'infoJuridiques': user.infoJuridiques?user.infoJuridiques: "",
        } 
    } else {
        
        if((typeof user.adminId)=="object"){
            console.warn("Access with object")
              const adminDoc= await user.adminId
                .get()
               const userAdmin = await adminDoc.data()
               userLogo = {
                'logo': userAdmin.logoPdf,
                'police': userAdmin.police?userAdmin.police:"",
                'infoJuridiques': userAdmin.infoJuridiques?userAdmin.infoJuridiques: "",
          } 
            
        }else{

            console.log("user", user);
            const adminDoc = await firebase.firestore()
                .collection('users')
                .doc(user.adminId)
                .get();
           const  userAdmin =  await adminDoc.data()
           userLogo = {
               'logo': userAdmin.logoPdf,
               'police': userAdmin.police?userAdmin.police:"",
               'infoJuridiques': userAdmin.infoJuridiques?userAdmin.infoJuridiques: "",
         } 
        }
      
    }
    const imageRequest = await fetch('/bp/assets/images/imageBpPdf.json')
    const imageBpPdf = await imageRequest.json()
    return new Promise((resolve, reject) => {
        if (otherImages) {
            resolve({
                userLogo: `data:image/png;base64,${userLogo.logo}`,
                imageBpPdf: imageBpPdf,
                police:userLogo.police,
                infoJuridiques:userLogo.infoJuridiques
            })
        } else {
            resolve({
                userLogo: `data:image/png;base64,${userLogo.logo}`,
                logoBp: imageBpPdf.logoBp,
                police:userLogo.police,
                infoJuridiques:userLogo.infoJuridiques
            })
        }
    })
}