import FirebaseModel from "../models/firebaseModel"
import * as besoin from "../../views/bp/mixins/financier/besoins";

class PdfDataFormat {

 
   constructor(data) {
        this.data = data
        
   }


   pdfFormat() {
   const totalImmoInCorporelle =  besoin.calculTotalImmoIncorporelle(this.data.besoins.immoIncorporelle)
   const subTotalImmoCorpMatInfo   =  besoin.calculSubTotalImmoCorpMatInfo(this.data.besoins.immoCorporelle.materielinfo)
    // const totaux = {
    //     besoins: this.totalBesoins,
    //     ressources: this.totalRessources,
    //     immoI: 0,
    //     immoC: {
    //       totalInitial: 0,
    //       totalAnnee1: 0,
    //       totalAnnee2: 0,
    //       totalAnnee3: 0 ,
    //     },
    //     finance: this.totalImmoFinanciere,
    //     bfr: {
    //       initial: this.totalBfrOuverture,
    //       annee1: this.totalBfrExploitation.annee1,
    //       annee2: this.totalBfrExploitation.annee2,
    //       annee3: this.totalBfrExploitation.annee3,
    //     },
    //     capital: this.totalApportCapital,
    //     associes: this.totalCompteAssocie,
    //     emprunt: this.totalEmpruntBancaire.montant,
    //     subvention: this.totalSubventionAide,
    //     re: {
    //       annee1: this.totalRemboursements.annee1,
    //       annee2: this.totalRemboursements.annee2,
    //       annee3: this.totalRemboursements.annee3,
    //     },
    //     soldeE: {
    //       initial: this.soldeExercice.initial,
    //       annee1: this.soldeExercice.annee1,
    //       annee2: this.soldeExercice.annee2,
    //       annee3: this.soldeExercice.annee3,
    //     },
    //     soldeC: {
    //       initial: this.soldeCumule.initial,
    //       annee1: this.soldeCumule.annee1,
    //       annee2: this.soldeCumule.annee2,
    //       annee3: this.soldeCumule.annee3,
    //     },
    //   };
        return subTotalImmoCorpMatInfo;
    }
    totalBesoins() {
        let initial = 0;
        let annee1 = 0;
        let annee2 = 0;
        let annee3 = 0;
        let annee4 = 0;
        let sumBesoinsinit=
        this.totalImmoIncorporelle.prixHT +
        this.totalImmoCorporelle.prixHT +
        this.totalImmoFinanciere +
        this.totalBfrOuverture;
  
        let sumBesoinsannee1 =
          // this.totalAmortisCorporelleAnnuel.annee1 +
          this.totalBfrExploitation.annee1 +
          this.totalRemboursements.annee1;
  
        let sumBesoinsannee2 =
          // this.totalAmortisCorporelleAnnuel.annee2 +
          this.totalBfrExploitation.annee2 +
          this.totalRemboursements.annee2;
  
        let sumBesoinsannee3 =
          // this.totalAmortisCorporelleAnnuel.annee3 +
          this.totalBfrExploitation.annee3 +
          this.totalRemboursements.annee3;
  
        initial = initial + sumBesoinsinit;
        annee1 = annee1 + sumBesoinsannee1;
        annee2 = annee2 + sumBesoinsannee2;
  
        annee3 = annee3 + sumBesoinsannee3;
  
        return {
          initial: arround(initial, this.currentCurrencyMask[":precision"]),
          annee1: arround(annee1, this.currentCurrencyMask[":precision"]),
          annee2: arround(annee2, this.currentCurrencyMask[":precision"]),
          annee3: arround(annee3, this.currentCurrencyMask[":precision"]),
          totalTva: {
            initial: arround(
              sumBesoinsinit,
              this.currentCurrencyMask[":precision"]
            ),
            annee1: arround(
              sumBesoinsannee1,
              this.currentCurrencyMask[":precision"]
            ),
            annee2: arround(
              sumBesoinsannee2,
              this.currentCurrencyMask[":precision"]
            ),
            annee3: arround(
              sumBesoinsannee3,
              this.currentCurrencyMask[":precision"]
            ),
          },
        };
      }
      totalRessources() {
        let initial = 0;
        let annee1 = 0;
        let annee2 = 0;
        let annee3 = 0;
        let annee4 = 0;
        // total ressources au lancement du projet
        initial =
          initial +
          this.totalApportCapital +
          this.totalCompteAssocie +
          this.totalEmpruntBancaire.montant +
          this.totalSubventionAide.montant;
        annee1 = annee1 + this.capaciteAutofinancement.annee1;
        annee2 = annee2 + this.capaciteAutofinancement.annee2;
        annee3 = annee3 + this.capaciteAutofinancement.annee3;
        annee4 = annee4 + this.capaciteAutofinancement.annee4;
        return {
          initial: arround(initial, this.currentCurrencyMask[":precision"]),
          annee1: arround(annee1, this.currentCurrencyMask[":precision"]),
          annee2: arround(annee2, this.currentCurrencyMask[":precision"]),
          annee3: arround(annee3, this.currentCurrencyMask[":precision"]),
          annee4: arround(annee4, this.currentCurrencyMask[":precision"]),
        };
      }

}
export default PdfDataFormat
