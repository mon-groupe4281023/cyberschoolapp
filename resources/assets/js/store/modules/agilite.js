import firebase from "firebase";
const state = () => ({
    idSummarySelected:'',
    typeTicketSelected:'none',
    planActionRef: null,
    tasksRef:null,
    activites:[],
    listTypesTickets: [],
    listProjets : [],
    selectedActivite:"",
    listTickets: [],
    duplicate: false,
    codeProjet:'',
    couleur: '',
    task:{},
    ticketDialog: false,
    etatTask: 'none',
    mode:'filtres',
    typeRequest:'global',
    adminId:'',
    currentUId:'',
    displayName:'',
    priorite:'',
    accord:'',
    etiquette:'',
    listLinks: [],
    dialog :{
         typeTicket: false,
         listTask:false
    },
    userSession: {}
})
// getters
const getters = {
    getIdSummary:(state)=> state.idSummarySelected ,
    getDialogListTask: state => {
        return state.dialog.listTask;
    }
        
    
}

// actions
const actions = {}

// muatations
const mutations = {
    setSummary(state, data){
         state.idSummarySelected = data
    },
    setPlanActionRef(state, data){
        state.planActionRef = data
   },
   setTasksRef(state, ref){
    state.tasksRef = ref
    },
    setCouleur(state, couleur){
        state.couleur = couleur
    },
    setCurrentUId(state, currentUId){
        state.currentUId = currentUId
    },
    setPriorite(state, priorite){
        state.priorite = priorite
    },
    setAdminId(state, adminId){
        state.adminId = adminId
    },
    setListTickets(state, listTickets){
        state.listTickets = listTickets
    },
    //DisplayDialog
    displayDialog(state, payload){
        state.dialog[payload.var] = payload.display
   },
   setDisplayListTicket(state, display){
       state.dialog.listTask = display
   },
   setCodeProjet(state,codeProjet){
       state.codeProjet = codeProjet
   },
   setTask(state, task) {
        state.task= task
   },
   setMode(state, mode) {
    state.mode= mode
},
  setActivites(state, activites) {
    state.activites= activites
},
  setSelectedActivite(state, id) {
    state.selectedActivite= id
},
    setListProjets(state, projets) {
        state.listProjets = projets
    },
    setListTypesTickets(state, list) {
    state.listTypesTickets = list
    },
    setTypeTicketSelected(state, id) {
    state.typeTicketSelected = id
    },
    setEtatTask(state, etat) {
        state.etatTask = etat
    },
    setEtiquette(state, etiquette) {
        state.etiquette = etiquette
    },
    setAccord(state, accord) {
        state.accord = accord
    },
    setTicketDialog(state, display) {
        state.ticketDialog = display
    },
    setDuplicate(state, isDuplicate){
        state.duplicate = isDuplicate
    },
    setListLinks(state, link){
        state.listLinks.push(link)
    },
    setTypeRequest(state, request) {
        state.typeRequest = request
    },
    setDisplayName(state, displayName) {
        state.displayName = displayName
    },
    setUserSession(state, session) {
        state.userSession = session
    }
}


export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
  }