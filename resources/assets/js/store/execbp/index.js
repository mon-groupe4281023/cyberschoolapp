import Vuex from 'vuex'
// import modules
import besoin from './besoin'
// import ressource from './ressource'
import {isNullOrUndefined} from 'util'

const actions = {
  clone(context, financier) {
    console.log('action')
    // parcours des besoins
    if(!isNullOrUndefined(financier['besoins'])){
      // on va dans les immo incorporelle
      if(!isNullOrUndefined(financier['besoins']['immoIncorporelle'])){
        financier['besoins']['immoIncorporelle'].forEach( immo => {
          context.commit('besoin/addImmoIncorporelle', immo)
          console.log(immo)
        })
      }
      // on va dans les immo corporelle
      if(!isNullOrUndefined(financier['besoins']['immoCorporelle'])){
        financier['besoins']['immoCorporelle'].forEach( immo => {
          context.commit('besoin/addImmoCorporelle', immo, {root:true})
        })
      }
      // on va dans les immo financiere
      if(!isNullOrUndefined(financier['besoins']['immoFinanciere'])){
        financier['besoins']['immoFinanciere'].forEach( immo => {
          context.commit('besoin/addimmoFinanciere', immo, {root:true})
        })
      }
      // on va dans les bfr
      if(!isNullOrUndefined(financier['besoins']['bfr'])){
        // ouverture
        if(!isNullOrUndefined(financier['besoins']['bfr']['bfrOuverture'])){
          financier['besoins']['bfr']['bfrOuverture'].forEach( immo => {
            context.commit('besoin/addBfrOuverture', immo, {root:true})
          })
        }
        // stock
        if(!isNullOrUndefined(financier['besoins']['bfr']['stock'])){
          financier['besoins']['bfr']['stock'].forEach( immo => {
            context.commit('besoin/addBfrExploitation', immo, {root:true})
          })
        }
        // creances
        if(!isNullOrUndefined(financier['besoins']['bfr']['creanceClient'])){
          financier['besoins']['bfr']['creanceClient'].forEach( immo => {
            context.commit('besoin/addBfrExploitation', immo, {root:true})
          })
        }
        // dettes
        if(!isNullOrUndefined(financier['besoins']['bfr']['dettes'])){
          financier['besoins']['bfr']['dettes'].forEach( immo => {
            context.commit('besoin/addBfrExploitation', immo, {root:true})
          })
        }
      }

    } else console.log(financier)
    // parcoure des ressources

    
  }
}

const getters = {

}

const mutations = {

}

export default new Vuex.Store({
  modules: {
    besoin: besoin,
  },
  state: () => {
    return {
    }
  },
  mutations: mutations,
  actions: actions,
  getters: getters,
  namespaced: true
})
/*
export default new Vuex.Store({
  modules: {
    execbp: execbp,
  },
  namespaced: true
}) */
