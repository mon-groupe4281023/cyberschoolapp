import FirebaseModel from "../models/firebaseModel"
import firebase from 'firebase'
import { Message,Loading } from 'element-ui';

class GroupProgram extends FirebaseModel{

    constructor (collection = 'groupProgram',titre,state,programs,idIncubateur) {
        super(collection)
        this.data = {
            titre: titre,
            public: state,
            programs:programs,
            idIncubateur:idIncubateur
        }
        // this.init = {}
    }

    // async initialization (id) {
    //     this.init = Object.assign({}, this.data, this.getData(id))
    // }
    
    /**
     * 
     * @param {*} collection 
     * @param {*} adminId 
     */
 static  getAllDocumentByAdminId(collection,adminId){
          let results = []
         firebase.firestore()
        .collection(collection)
        .where('idIncubateur','==',adminId)
        .get()
        .then((querySnapshot) =>{
           querySnapshot.forEach((doc) =>{
            let imagesConcours = []
            doc.data().programs.forEach((concoursId)=>{
               this.getProgramImages(concoursId,'concours').then((doc)=>{
                imagesConcours.push(doc.data().logoConcours)
                });
            })
            // console.log(doc.id)
            results.push({
                titre:doc.data().titre,
                programs:doc.data().programs,
                idIncubateur:doc.data().idIncubateur,
                images:imagesConcours,
                id:doc.id
            }
               );
               
            })
         
        })
        return results
      

    }
    /**
     * 
     * @param {*} id 
     * @param {*} collection 
     */
  static  getProgramImages(id,collection){
        return firebase.firestore()
        .collection(collection)
        .doc(id)
        .get() 

    }
    /**
         * 
         * @param {*} collection 
         * @param {*} adminId 
         */
     static  getAllDocumentPublic(collection){
        
        let results = []
   
       Loading.service({ fullscreen: true });
       let promiseProg= new Promise((resolve,reject)=>{
        firebase.firestore()
        .collection(collection)
        .where('public','==',true)
        .get()
        .then((querySnapshot) =>{
            if(!querySnapshot.empty){
                querySnapshot.forEach((doc) =>{
                    let imagesConcours = []
                    doc.data().programs.forEach((concoursId)=>{
                       this.getProgramImages(concoursId,'concours').then((doc)=>{
                        imagesConcours.push(doc.data().logoConcours)
                        });
                    })
                    // console.log(doc.id)
                    results.push({
                        titre:doc.data().titre,
                        programs:doc.data().programs,
                        idIncubateur:doc.data().idIncubateur,
                        images:imagesConcours,
                        id:doc.id
                    }
                       );
                    })
                    resolve(results)
                    // console.log("Hello", results)
                    
     

            }else{
                    reject(results)
            }
          
        })
       })
       Promise.allSettled([promiseProg]).then((values)=>{
        console.log("Log1",results)
        return results
     })
     
  
     


  }
}

export default GroupProgram
