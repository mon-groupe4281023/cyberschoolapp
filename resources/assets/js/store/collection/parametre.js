import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Prametre
 */
class Prametre extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'parametre') {
        super(collection)
        this.data = {
            criteres: []
        }
        this.init = {}
    }

    /**
     * Initialisation des données d'un documents
     * @param id Id du document
     * @return {Promise<void>}
     */
    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Prametre
