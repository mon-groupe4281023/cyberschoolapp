import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Formulaires
 */
class Formulaires extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'formulaires') {
        super(collection)
        this.data = {
            form: '',
            locale: '',
            model: {},
            projectId: '',
            schema: {}
        }
        this.init = {}
    }

    /**
     * Initialisation des données d'un documents
     * @param id Id du document
     * @return {Promise<void>}
     */
    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Formulaires
