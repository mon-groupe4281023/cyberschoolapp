import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Monnaies
 */
class Monnaies extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'monnaies') {
        super(collection)
        this.data = {
            currency: '',
            "currency-symbol-position": '',
            "decimal-separator": '',
            "empty-value": '',
            literal: '',
            max: 0,
            min: 0,
            minus: 0,
            "output-type": '',
            precision: 0,
            "thousand-separator": ''
        }
        this.init = {}
    }

    /**
     * Initialisation des données d'un documents
     * @param id Id du document
     * @return {Promise<void>}
     */
    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Monnaies
