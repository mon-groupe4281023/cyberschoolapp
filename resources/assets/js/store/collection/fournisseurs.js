import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Fournisseurs
 */
class Fournisseurs extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'fournisseurs') {
        super(collection)
        this.data = {
            codepostal: '',
            createdAt: '',
            deletedAt: '',
            id: '',
            mail: '',
            nom: '',
            note: '',
            pays: '',
            projectId: '',
            quartier: '',
            tel1: '',
            tel2: '',
            type: '',
            updatedAt: '',
            ville: ''
        }
        this.init = {}
    }

    /**
     * Initialisation des données d'un documents
     * @param id Id du document
     * @return {Promise<void>}
     */
    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Fournisseurs
