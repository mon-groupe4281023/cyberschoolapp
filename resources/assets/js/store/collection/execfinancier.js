import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Execfinancier
 * @extends FirebaseModele
 */
class Execfinancier extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'execfinancier') {
        super(collection)
        this.data = {
            besoins: {
                bfr: {},
                immoCorporelle: {},
                immoFinanciere: {},
                immoIncorporelle: []
            },
            projet: '',
            ressources: {
                capital: {},
                comptes: [],
                emprumts: {},
                "subventions-aide" : {}
            },
            users: ''
        }
        this.init = {}
    }

    /**
     * Initialisation des données d'un documents
     * @param id Id du document
     * @return {Promise<void>}
     */
    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Execfinancier
