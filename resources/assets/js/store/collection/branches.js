import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Branches
 * @extends FirebaseModel
 */
class Branches extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'branches') {
        super(collection)
        this.data = {
            code: '',
            libelle: '',
            secteur: ''
        }
        this.init = {}
    }

    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Branches
