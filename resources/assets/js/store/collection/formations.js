import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Formations
 * @extends FirebaseModel
 */
class Formations extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'formations') {
        super(collection)
        this.data = {
            description: '',
            duree: '',
            libelle: '',
            pathImg: '',
            prix: ''
        }
        this.init = {}
    }

    /**
     * Initialisation des données d'un documents
     * @param id Id du document
     * @return {Promise<void>}
     */
    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Formations
