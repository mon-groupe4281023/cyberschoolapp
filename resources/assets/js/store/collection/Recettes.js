import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Recettes
 */
class Recettes extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'recettes') {
        super(collection)
        this.data = {
            banque: {},
            bureautique: 0,
            client: {},
            createdAt: '',
            dates: {},
            deletedAt: '',
            fichier: '',
            image: '',
            libelle: [],
            montantFacture: 0,
            montantRecu: 0,
            prixVenteUnitaire: 0,
            products: {},
            produit: {},
            projectId: '',
            qte: {},
            quantite: 0,
            recette: '',
            resteMontant: 0,
            type: '',
            updatedAt: '',
            url: '',
            vendeurId: '',
            versement: []
        }
        this.init = {}
    }

    /**
     * Initialisation des données d'un documents
     * @param id Id du document
     * @return {Promise<void>}
     */
    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Recettes
