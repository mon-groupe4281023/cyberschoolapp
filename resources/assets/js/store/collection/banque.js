import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Banque
 * @extends FirebaseModel
 */
class Banque extends FirebaseModel{

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'banque') {
        super(collection)
        this.data = {
            codepostal: '',
            createdAt: '',
            mail: '',
            nom: '',
            pays: '',
            projectId: '',
            guartier: '',
            tel1: '',
            tel2: '',
            ville: ''
        }
        this.init = {}
    }

    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Banque
