import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Board
 * @extends FirebaseModel
 */
class Board extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'board') {
        super(collection)
        this.data = {
            createdAt: '',
            desc: '',
            prejet: '',
            titre: ''
        }
        this.init = {}
    }

    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Board
