import FirebaseModel from "../models/firebaseModel"
import {prixImmoTotalHT} from "../../views/bp/mixins/financier/besoins";

/**
 * Collection Financier
 */
class Financier extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'financier') {
        super(collection)
        this.data = {
            besoins: {
                bfr: {},
                immoCorporelle: {},
                immoFinanciere: {},
                immoIncorporelle: []
            },
            charges: {},
            parametres: {},
            produits: {},
            projet: '',
            ressources: {
                capital: {},
                comptes: [],
                emprumts: {},
                "subventions-aide" : {}
            },
            users: ''
        }
    }

    /**
     * Initialisation des données d'un documents
     * @param id Id du document
     * @return {Promise<void>}
     */
    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }

    formatForPdf () {

    }
}

export default Financier
