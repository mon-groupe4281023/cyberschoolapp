import firebase from 'firebase'
class Fichier {
  /**
  * Constructor
  * @param dossoier {string} Nom de la collection
  */
  constructor (dossier) {
    this.dossier = dossier
  }

  metaData (file) {
    const metadata = {
      contentType: file.type
    }
    return metadata
  }

  // methodes
  saveToStorage (dossier, file) {
    const storage = firebase.storage().ref()
    const metadata = this.metaData(file)
    return storage.child(dossier + '/' + file.lastModified + file.name).put(file, metadata)
  }
  saveToStorageBase64 (dossier, base64File,fileName) {
    const storage = firebase.storage().ref()
    return storage.child(dossier + '/' + fileName).putString(base64File, 'base64')
  }
}
export default Fichier
