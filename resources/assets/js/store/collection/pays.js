import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Pays
 */
class Pays extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'messages') {
        super(collection)
        this.data = {
            alpha2: '',
            alpha3: '',
            code: '',
            nom_ang: '',
            nom_fr: ''
        }
        this.init = {}
    }

    /**
     * Initialisation des données d'un documents
     * @param id Id du document
     * @return {Promise<void>}
     */
    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Pays
