import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Depenses
 * @extends FirebaseModel
 */
class Depenses extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'depenses') {
        super(collection)
        this.data = {
            banque: {},
            createdAt: '',
            dates: {},
            deletedAt: '',
            description: '',
            libelle: '',
            montant: 0,
            prixVenteUnitaire: '',
            projectId: '',
            quantite: 0,
            type: '',
            updatedAt: '',
            url: '',
            vendeur: {}
        }
        this.init = {}
    }

    /**
     * Initialisation des données d'un documents
     * @param id Id du document
     * @return {Promise<void>}
     */
    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Depenses
