import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Evaluateur
 * @extends FirebaseModel
 */
class Evaluation extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'evaluation') {
        super(collection);
        this.data = {
            bmc: {},
            chiffre: {},
            evaluateur: '',
            note_global: '',
            porteur: {},
            produit: {},
            projet: {},
            summary: ''
        }
        this.init = {}
    }

    /**
     * Initialisation des données d'un documents
     * @param id Id du document
     * @return {Promise<void>}
     */
    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Evaluation
