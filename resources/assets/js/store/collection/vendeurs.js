import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Vendeurs
 */
class Vendeurs extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'vendeurs') {
        super(collection)
        this.data = {
            adr: '',
            age: '',
            codepostal: '',
            confirm: '',
            createdAt: '',
            deletedAt: '',
            mail: '',
            nom: '',
            note: '',
            pays: '',
            prenom: '',
            projectId: '',
            quartier: '',
            representant: '',
            tel1: '',
            tel2: '',
            type: '',
            updatedAt: ''
        }
        this.init = {}
    }

    /**
     * Initialisation des données d'un documents
     * @param id Id du document
     * @return {Promise<void>}
     */
    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Vendeurs
