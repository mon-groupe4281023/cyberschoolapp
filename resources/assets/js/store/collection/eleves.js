import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Eleves
 * @extends FirebaseModel
 */
class Eleves extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'eleves') {
        super(collection)
        this.data = {
            email: '',
            formation: '',
            horaire: '',
            nom: '',
            phone: '',
            prenom: '',
            sexe: ''
        }
        this.init = {}
    }

    /**
     * Initialisation des données d'un documents
     * @param id Id du document
     * @return {Promise<void>}
     */
    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Eleves
