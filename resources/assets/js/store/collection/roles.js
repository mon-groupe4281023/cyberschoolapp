import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Roles
 */
class Roles extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'roles') {
        super(collection)
        this.data = {
            createdAt: '',
            desc: '',
            projet: '',
            titre: ''
        }
        this.init = {}
    }

    /**
     * Initialisation des données d'un documents
     * @param id Id du document
     * @return {Promise<void>}
     */
    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Roles
