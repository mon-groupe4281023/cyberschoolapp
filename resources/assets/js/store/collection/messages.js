import FirebaseModel from "../models/firebaseModelErp"

/**
 * Collection Messages
 */
class Messages extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'messages', data) {
        super(collection)
        this.data = {
             titre: data.titre,
             content: data.content,
             userEmail: data.userEmail,
             email: data.isEmail
        }
       
    }

}

export default Messages
