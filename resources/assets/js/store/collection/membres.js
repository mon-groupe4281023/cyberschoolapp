import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Membres
 */
class Membres extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'membres') {
        super(collection)
        this.data = {
            adminId: '',
            userId: ''
        }
        this.init = {}
    }

    /**
     * Initialisation des données d'un documents
     * @param id Id du document
     * @return {Promise<void>}
     */
    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Membres
