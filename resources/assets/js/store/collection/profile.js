import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Profile
 */
class Profile extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'profile') {
        super(collection)
        this.data = {
            description: '',
            title: ''
        }
        this.init = {}
    }

    /**
     * Initialisation des données d'un documents
     * @param id Id du document
     * @return {Promise<void>}
     */
    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Profile
