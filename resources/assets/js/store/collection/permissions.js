import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Permissions
 */
class Permissions extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'permissions') {
        super(collection)
        this.data = {
            liste: [],
            role: ''
        }
        this.init = {}
    }

    /**
     * Initialisation des données d'un documents
     * @param id Id du document
     * @return {Promise<void>}
     */
    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Permissions
