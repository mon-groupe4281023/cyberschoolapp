import FirebaseModel from "../models/firebaseModel"

/**
 * Collection TypeClient
 */
class TypeClient extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'typeClient') {
        super(collection)
        this.data = {
            descr: '',
            libelle: ''
        }
        this.init = {}
    }

    /**
     * Initialisation des données d'un documents
     * @param id Id du document
     * @return {Promise<void>}
     */
    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default TypeClient
