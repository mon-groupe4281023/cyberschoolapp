import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Suivi
 */
class Suivi extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'suivi') {
        super(collection)
        this.data = {
            depenses: {},
            projectId: ''
        }
        this.init = {}
    }

    /**
     * Initialisation des données d'un documents
     * @param id Id du document
     * @return {Promise<void>}
     */
    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Suivi
