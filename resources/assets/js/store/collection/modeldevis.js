import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Modeldevis
 */
class Modeldevis extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'modeldevis') {
        super(collection)
        this.data = {
            clientInfo: [],
            date: '',
            entrepriseInfos: '',
            footerInfo: '',
            logo: '',
            projectId: '',
            signature: {},
            tableau1: '',
            tableau2: '',
            titre: '',
            extra:''
        }
        this.init = {}
    }

    /**
     * Initialisation des données d'un documents
     * @param id Id du document
     * @return {Promise<void>}
     */
    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Modeldevis
