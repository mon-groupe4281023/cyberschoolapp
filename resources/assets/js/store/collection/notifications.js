import FirebaseModel from "../models/firebaseModelErp"

/**
 * Collection Notifications
 */
class Notifications extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'notifications',data) {
        super(collection)
        this.data = {
            projectId: data.idProjet,
            action: data.action,
            state: data.state,
            actionType: data.actionType,
            actionText: data.actionText,
            adminId: data.adminId,
            userId:data.userId,
            actionState:data.actionState
        }
        // this.init = {}
    }

    
}

export default Notifications
