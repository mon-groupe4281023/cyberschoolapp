import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Ville
 */
class Ville extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'ville') {
        super(collection)
        this.data = {
            idProvince: '',
            libelle: ''
        }
        this.init = {}
    }

    /**
     * Initialisation des données d'un documents
     * @param id Id du document
     * @return {Promise<void>}
     */
    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Ville
