import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Activites
 * @extends FirebaseModel
 */
class Activites extends FirebaseModel{

    /**
     * Constructor
     * @param collection nom de la collection
     */
    constructor (collection = 'activites') {
        super(collection)
        this.data = {
            branche: '',
            code: '',
            libelle: ''
        }
        this.init = {}
    }

    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Activites
