import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Produits
 */
class Produits extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'produits') {
        super(collection)
        this.data = {
            chargeVariable: 0,
            delaisClient: 0,
            delaisFournisseur: {},
            description: '',
            id: '',
            image: {},
            nom: '',
            prixVenteUnitaire: 0,
            projectId: '',
            stockMatierePremiere: {},
            stockProduitFini: {},
            target: 0,
            updateAt: ''
        }
        this.init = {}
    }

    /**
     * Initialisation des données d'un documents
     * @param id Id du document
     * @return {Promise<void>}
     */
    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Produits
