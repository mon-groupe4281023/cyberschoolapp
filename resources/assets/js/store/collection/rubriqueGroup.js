import FirebaseModel from "../models/firebaseModel"
import firebase from 'firebase'
import { Message,Loading } from 'element-ui';

class RubriqueGroup extends FirebaseModel{

    constructor (collection = 'rubriqueGroup',titre,group,idUser) {
        super(collection)
        this.data = {
            titre: titre,
            group:group,
            idUser:idUser
        }
        // this.init = {}
    }

    // async initialization (id) {
    //     this.init = Object.assign({}, this.data, this.getData(id))
    // }
    
    /**
     * 
     * @param {*} collection 
     * @param {*} idSummary 
     */
 static  getAllRubriqueByProject(collection,idSummary,subCollection){
         
         firebase.firestore()
        .collection(collection)
        .doc(idSummary).collection(subCollection)
        .get()
    }
    static  getAllRubriqueTicket(collection,idSummary,subCollection){
        firebase.firestore()
       .collection(collection)
       .doc(idSummary).collection(subCollection)
       .get()
   }

 
 
}

export default RubriqueGroup
