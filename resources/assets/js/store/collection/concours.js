import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Concours
 * @extends FirebaseModel
 */
class Concours extends FirebaseModel {

    /**
     * Constructor nom de la collection
     * @param collection
     */
    constructor(collection = 'concours') {
        super(collection)
        this.data = {
            adminId: '',
            createdAt: '',
            criteresEvaluation: '',
            dateConcours: [],
            descriptionConcours: '',
            etatDuConcours: true,
            id: '',
            incubateur: '',
            libelleConcours: '',
            logoConcours: '',
            nbParticipantsInscris: [],
            participants: 0,
            quotasRestant: 0,
            wallpaper: ''
        }
        this.init = {}
    }

    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Concours
