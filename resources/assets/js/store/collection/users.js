import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Users
 */
class Users extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'users') {
        super(collection)
        this.data = {
            activated: false,
            adminId: '',
            dateNaissance: '',
            email: '',
            firstTimeConnection: '',
            first_name: '',
            fonction: '',
            last_name: '',
            password: '',
            profile: '',
            roleId: '',
            sexe: '',
            telephone: ''
        }
        this.init = {}
    }

    /**
     * Initialisation des données d'un documents
     * @param id Id du document
     * @return {Promise<void>}
     */
    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Users
