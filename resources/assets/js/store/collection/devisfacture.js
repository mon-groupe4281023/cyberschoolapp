import FirebaseModel from "../models/firebaseModel";

/**
 * Collection devisFacture
 * @extends FirebaseModel
 */
class Devisfacture extends FirebaseModel {
  /**
   * Constructor
   * @param collection Nom de la collection
   */
  constructor(collection = "devisfacture") {
    super(collection);
    this.data = {
      footer: {},
      idDevis: "",
      infosFacture: {},
      paymentsInfos: {
        date: "",
      },
      montants: {},
      produits: [],
      projectId: "",
    };
  }

  /**
   * Initialisation des données d'un documents
   * @param id Id du document
   * @return {Promise<void>}
   */
  async initialization(id) {
    let data = { exist: false };
    try {
      data = await this.getData(id);
    } catch (e) {
      console.log(e);
    }

    this.init = Object.assign({}, this.data, data);
  }
}

export default Devisfacture;
