import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Province
 */
class Province extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'province') {
        super(collection)
        this.data = {
            code: '',
            libelle: ''
        }
        this.init = {}
    }

    /**
     * Initialisation des données d'un documents
     * @param id Id du document
     * @return {Promise<void>}
     */
    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Province
