import Clients from "./clients"

/**
 * Collection Clients
 * @extends FirebaseModel
 */
class Entreprise extends Clients {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'clients') {
        super(collection)
        this.data = {
            siteurl: '',
            contact: {
                'nom' : '',
                'tel' : '',
                'email' : '',
            },
        }
        this.init = {}
    }

    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Entreprise
