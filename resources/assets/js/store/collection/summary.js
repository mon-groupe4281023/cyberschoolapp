import FirebaseModel from '../models/firebaseModel'
 
/**
 * Collection Summary
 * @extends FirebaseModel
 */
class Summary extends FirebaseModel{

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor (collection = 'summary') {
        super(collection)
        this.data = {
            activite: [], 
            adminId: '',
            associe: [],
            besoins: [],
            bmc: {},
            brancheActivite:'',
            charges: [],
            clients: [],
            concurrent: '',
            currency: '',
            descrOpportunite: '',
            descrSynthetique: '',
            etat: '',
            eval: '',
            formJuridique: '',
            heureTravaille: '',
            naissProjet: '',
            porteur: {},
            produits: [],
            ressources: [],
            secteurActivite: '',
            situationActu: '',
            statut: '',
            statutActivite: '',
            titre: '',
            user: '',
            ventes: [],
            visionCourt: [],
            visionLong: [],
            zone: '',
            logo:'',
            infosJuridiques:''
        }
        this.init = {}
    }

    /**
     *  Initiialisation des champs de la collection, modify init property
     * @param id {string} Id du document
     */
    async initialization (id) {

        let brancheActivite = []
        let activite = []
        try {
            brancheActivite = await this.getDataJoinField(id, 'brancheActivite', 'branches')
            activite = await this.getDataJoinField(id, 'activite', 'activites')
        } catch (e) {
            console.log(e)
        }
        const data = await this.getData(id)

        this.init = Object.assign({}, this.data, data, {
            activite,
            brancheActivite,
            totalBesoin: this.calculTotalBesoin(data.besoins),
            totalCharge: this.totauxChrg(data.charges),
            totalRessource: this.calculTotalRessource(data.ressources),
            totalVente: this.calculTotalVente(data.ventes)
        })
    }

    /**
     *  Calcul le total des ressources
     * @param arrayRessources {array} Listes des ressources
     * @return {number} Total du cout des ressources
     */
    calculTotalRessource(arrayRessources){
        let total = 0;
        if (arrayRessources.length > 0 && typeof (arrayRessources) === 'object') {
            arrayRessources.forEach(ressource => {
                total = total + ressource.cout;
            });
        }
        return total;
    }

    /**
     * Calcul le total des besoins
     * @param arrayBesoins {array} Liste des besoins
     * @return {number} Total du cout des besoins
     */
    calculTotalBesoin(arrayBesoins){
        let total = 0;
        if (arrayBesoins.length > 0 && typeof (arrayBesoins) === 'object') {
            arrayBesoins.forEach(besoin => {
                total = total+besoin.cout;
            });
        }
        return total;
    }

    /**
     * Calcul le total des ventes
     * @param arrayVente {array} Liste des ventes
     * @return {{mensuel: number, annuel: number}} Total du cout des ventes
     */
    calculTotalVente(arrayVente){
        let total = {
            mensuel: 0,
            annuel: 0
        }
        if (arrayVente.length > 0 && typeof (arrayVente) === 'object') {
            arrayVente.forEach(vente => {
                total.mensuel = total.mensuel + vente.ptotal;
                total.annuel = total.annuel + vente.ptotVenteAnnuel;
            });
        }
        return total
    }

    /**
     * Calcul le total des charges
     * @param arrayCharges {array} Liste des ventes
     * @return {{mensuel: number, annuel: number}} Total du cout des ventes
     */
    totauxChrg (arrayCharges) {
        let total = {
            mensuel: 0,
            annuel: 0
        }
        if (arrayCharges.length > 0 && typeof (arrayCharges) === 'object') {
            arrayCharges.forEach(charge => {
                total.annuel = total.annuel + charge.nombre_mois * charge.punitaire * charge.quantite;
                total.mensuel = total.mensuel + charge.punitaire * charge.quantite;
            });
        }
        return total
    }
}

export default Summary
