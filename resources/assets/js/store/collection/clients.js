import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Clients
 * @extends FirebaseModel
 */
class Clients extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'clients') {
        super(collection)
        this.data = {
            nom: '',
            tel1: '',
            tel2: '',
            email: '',
            bp:'',
            note: '',
            pays: '',
            ville: '',
            quartier: '',
            projectId: '',
            type: '',
            createdAt: '',
            deletedAt: '',
            updatedAt: '',
        }
        this.init = {}
    }

    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Clients
