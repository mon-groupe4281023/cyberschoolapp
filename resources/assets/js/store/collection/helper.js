export default {
    // calcul du total de ressource
    calculTotalRessource(arrayRessources){
        let total = 0;
        if (arrayRessources.length > 0 && typeof (arrayRessources) === 'object') {
            arrayRessources.forEach(ressource => {
                total = total + ressource.cout;
            });
        }
        return total;
    },
    // calcul du total de Besoin
    calculTotalBesoin(arrayBesoins){
        let total = 0;
        if (arrayBesoins.length > 0 && typeof (arrayBesoins) === 'object') {
            arrayBesoins.forEach(besoin => {
                total = total+besoin.cout;
            });
        }
        return total;
    },
    // calcul du total des ventes
    calculTotalVente(arrayVente){
        let totalMensuel = 0;
        let totalAnnuel = 0;
        if (arrayVente.length > 0 && typeof (arrayVente) === 'object') {
            arrayVente.forEach(vente => {
                totalMensuel = totalMensuel + vente.ptotal;
                totalAnnuel = totalAnnuel + vente.ptotVenteAnnuel;
            });
        }
        return { 'mensuel': totalMensuel, 'annuel': totalAnnuel}
    },
    // calcul du total des charges
    calculTotalCharge(arrayCharges){
        let totalMensuel = 0;
        let totalAnnuel = 0;
        if (arrayCharges.length > 0 && typeof (arrayCharges) === 'object') {
            arrayCharges.forEach(charge => {
                totalMensuel = totalMensuel + charge.ptotal;
                totalAnnuel = totalAnnuel + charge.ptotChargeAnnuel;
            });
        }
        return { 'mensuel': totalMensuel, 'annuel': totalAnnuel}
    },

    totauxChrg (arrayCharges) {

        let totalC = 0;
        let totalmois=0;
        let totalanne=0
        if (arrayCharges.length > 0 && typeof (arrayCharges) === 'object') {

            arrayCharges.forEach(charge => {
                totalanne = totalanne + charge.nombre_mois * charge.punitaire * charge.quantite;
                totalmois = totalmois + charge.punitaire * charge.quantite;
                totalC = totalC + charge.ptotChargeAnnuel;
            });
        }

        return { 'mensuel': totalmois, 'annuel': totalanne}
    }
}