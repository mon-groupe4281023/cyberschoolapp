import FirebaseModel from "../models/firebaseModel"

/**
 * Collection Tache
 */
class Tache extends FirebaseModel {

    /**
     * Constructor
     * @param collection Nom de la collection
     */
    constructor(collection = 'tache') {
        super(collection)
        this.data = {
            id: '',
            liste: [],
            status: ''
        }
        this.init = {}
    }

    /**
     * Initialisation des données d'un documents
     * @param id Id du document
     * @return {Promise<void>}
     */
    async initialization (id) {
        this.init = Object.assign({}, this.data, await this.getData(id))
    }
}

export default Tache
