import firebase from "firebase";

class FirebaseModel {
    /**
     * Constructor
     * @param collection {string} Nom de la collection
     */
    constructor(collection) {
        this.collection = collection;
    }


    /***************************ENREGISTREMENT*******************************/
    static getCreatedAt() {
        return firebase.firestore.FieldValue.serverTimestamp();
    }
    static saveDocument(collection, data) {
        data.createdAt = firebase.firestore.FieldValue.serverTimestamp();
        return firebase
            .firestore()
            .collection(collection)
            .add(data)
    }
    static saveDocumentWithId(collection,docId, subCol, subId, data) {
        return firebase
            .firestore()
            .collection(collection)
            .doc(docId)
            .collection(subCol)
            .doc(subId)
            .set({libelle: data}, {merge : true})
    }
     saveDocumentDynamic(collection, data) {
        data.createdAt = firebase.firestore.FieldValue.serverTimestamp();
        return firebase
            .firestore()
            .collection(collection)
            .add(data)
    }
 static async saveDocumentSubCollection(collection, doc, subCollection, data) {
        return firebase.firestore().collection(collection)
            .doc(doc)
            .collection(subCollection)
            .add(data)
    }
    
  saveDocumentSubCollectionDynamic(collection, doc, subCollection, data) {
      data.createdAt = firebase.firestore.FieldValue.serverTimestamp();
        return firebase.firestore().collection(collection)
            .doc(doc)
            .collection(subCollection)
            .add(data)
    }

static async saveDocumentSubCollection(collection, doc, subCollection, data) {
        return firebase.firestore().collection(collection)
            .doc(doc)
            .collection(subCollection)
            .add(data)
    }

 static  saveDocumentSubSubCollection(collection, doc, subCollection, subSubDoc, subSubCollection, data) {
    data.createdAt = firebase.firestore.FieldValue.serverTimestamp();    
    return firebase.firestore().collection(collection)
            .doc(doc)
            .collection(subCollection)
            .doc(subSubDoc)
            .collection(subSubCollection)
            .add(data)
    }
    
     saveDocumentSubSubCollectionDynamic(collection, doc, subCollection, subSubDoc, subSubCollection, data) {
        data.createdAt = firebase.firestore.FieldValue.serverTimestamp();
        return firebase.firestore().collection(collection)
            .doc(doc)
            .collection(subCollection)
            .doc(subSubDoc)
            .collection(subSubCollection)
            .add(data)
    }
    saveDocumentSubSubSubCollectionDynamic(collection, doc, subCollection, subSubDoc, subSubCollection,subSubSubDoc,subSubSubCollection, data) {
        data.createdAt = firebase.firestore.FieldValue.serverTimestamp();
        return firebase.firestore().collection(collection)
            .doc(doc)
            .collection(subCollection)
            .doc(subSubDoc)
            .collection(subSubCollection)
            .doc(subSubSubDoc)
            .collection(subSubSubCollection)
            .add(data)
    }

    /***************************END ENREGISTREMENT*******************************/


    /***************************MODIFICATION*******************************/
    static updateDocument(id, fieldUpdate, collection) {
        return FirebaseModel.getCollection(collection)
            .doc(id)
            .update(fieldUpdate);
    }

    static updateDocumentSubcollection(collection, document, subcollection, id, data) {
        data.updatedAt = firebase.firestore.FieldValue.serverTimestamp();
        return firebase.firestore().collection(collection).doc(document).collection(subcollection).doc(id).set(data)
    }
     updateDocumentSubcollectionDynamic(collection, document, subcollection, id, data) {
        data.updatedAt = firebase.firestore.FieldValue.serverTimestamp();
        return firebase.firestore().collection(collection).doc(document).collection(subcollection).doc(id).set(data)
    }
    static updateDocumentSubSubCollection(collection, doc, subcollection, subSubDoc, subSubCollection, id, data) {
        data.updatedAt = firebase.firestore.FieldValue.serverTimestamp();
        return firebase.firestore()
        .collection(collection)
        .doc(doc)
        .collection(subcollection)
        .doc(subSubDoc)
        .collection(subSubCollection)
        .doc(id)
        .set(data)
    }
     updateDocumentSubSubCollectionDynamic(collection, doc, subcollection, subSubDoc, subSubCollection, id, data) {
        data.updatedAt = firebase.firestore.FieldValue.serverTimestamp();
        return firebase.firestore()
        .collection(collection)
        .doc(doc)
        .collection(subcollection)
        .doc(subSubDoc)
        .collection(subSubCollection)
        .doc(id)
        .set(data)
    }
       /***************************METHODE SPECIFIQUE POURMETTRE A JOUR L'ETAT DES PARAMETRES*******************************/
    static updateDocumentSubSubCollectionField(collection, doc, subcollection, subSubDoc, subSubCollection, id,  value) {
        return firebase.firestore()
        .collection(collection)
        .doc(doc)
        .collection(subcollection)
        .doc(subSubDoc)
        .collection(subSubCollection)
        .doc(id)
        .update({default:value})
    }
    /***************************METHODE SPECIFIQUE POURMETTRE A JOUR L'ETAT DES PARAMETRES******************************/
    /***************************END MODIFICATION*******************************/



    /***************************RECUPERATION*******************************/
    static getCollection(collection) {
        return firebase.firestore().collection(collection);
    }
    static foreachItem(data) {
        const results = []
        data.then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                const data = doc.data()
                data.id = doc.id
                results.push(data)
            })
        })
        return results
    }
    static getDocument(id, collection) {
        return firebase
            .firestore()
            .collection(collection)
            .doc(id)
            .get();
    }
    static getAllDocument(collection) {
        return firebase
            .firestore()
            .collection(collection)
            .get();
    }
    getData(id, field = null, collection = null) {
        return new Promise((resolve, rejects) => {
            FirebaseModel.getDocument(id, collection ? collection : this.collection)
                .then((doc) => {
                    if (doc.exists) {
                        if (field) {
                            if (doc.data()[field]) resolve(doc.data()[field]);
                            else rejects("Field undefended " + field, doc.data());
                        } else resolve(doc.data());
                    } else rejects(doc);
                })
                .catch((error) => {
                    console.warn(`Données introuvables : ${error}`);
                });
        });
    }
    async getDataJoinField(id, field, collection, fieldRef = "libelle") {
        let collectionResult;
        const idRef = await this.getData(id, field);
        if (idRef && idRef.length > 0) {
            collectionResult =
                typeof idRef === "object"
                    ? await this.getDataManyDocument(idRef, fieldRef, collection)
                    : await this.getData(idRef, fieldRef, collection);
        }
        return new Promise((resolve, reject) => {
            if (collectionResult) {
                resolve(collectionResult);
            } else {
                reject(idRef);
            }
        });
    }
    static getDataManyDocument(idArray, field = null, collection) {
        let results = [];
        idArray.forEach(async (id) => {
            results.push(await this.getData(id, field, collection));
        });
        return new Promise((resolve) => {
            resolve(results);
        });
    }
    static getDocumentSubCollection(collection, document, subCollection) {
        return firebase.firestore()
            .collection(collection)
            .doc(document)
            .collection(subCollection)
            .orderBy("createdAt", "desc")
            .get()
    }
    static getAllDocumentSubCollection(collection, document, subCollection) {
        return firebase.firestore()
            .collection(collection)
            .doc(document)
            .collection(subCollection)
    }
    static getDocumentSubSubCollection(collection, doc, subCollection, subSubDoc, subSubCollection) {
        return firebase.firestore()
            .collection(collection)
            .doc(doc)
            .collection(subCollection)
            .doc(subSubDoc)
            .collection(subSubCollection)
            // .orderBy("createdAt","asc")
            .get()
    }
    static getDocumentSubSubCollectionSnap(collection, doc, subCollection, subSubDoc, subSubCollection) {
        return firebase.firestore()
            .collection(collection)
            .doc(doc)
            .collection(subCollection)
            .doc(subSubDoc)
            .collection(subSubCollection)
            // .orderBy("createdAt","asc")
    }
    static getDocumentSubSubCollectionOrder(collection, doc, subCollection, subSubDoc, subSubCollection) {
        return firebase.firestore()
            .collection(collection)
            .doc(doc)
            .collection(subCollection)
            .doc(subSubDoc)
            .collection(subSubCollection)
            .orderBy("createdAt","asc")
            .get()
    }
    static getOneDocumentSubSubCollection(collection, doc, subCollection, subSubDoc, subSubCollection, subSubSubDoc) {
        return firebase.firestore()
            .collection(collection)
            .doc(doc)
            .collection(subCollection)
            .doc(subSubDoc)
            .collection(subSubCollection)
            .doc(subSubSubDoc)
            .get()
    }
    static getDocumentSubSubCollectionDoc(collection, doc, subCollection, subSubDoc, subSubCollection) {
        return firebase.firestore()
            .collection(collection)
            .doc(doc)
            .collection(subCollection)
            .doc(subSubDoc)
            .collection(subSubCollection)
    }
    static getDocumentSubCollectionSnap(collection, document, subCollection) {
        return firebase.firestore()
            .collection(collection)
            .doc(document)
            .collection(subCollection)
    }
    static getDocumentSubCollectionSnapShot(collection, document, subCollection) {
        return new Promise((resolve) => {
            firebase.firestore()
                .collection(collection)
                .doc(document)
                .collection(subCollection)
                .onSnapshot((snapshot) => {
                    let items = []
                    if (!snapshot.empty) {
                        snapshot.forEach((doc) => {
                            const data = doc.data()
                            data.id = doc.id
                            items.push(data)
                        })
                        resolve(items)
                    } else {
                        resolve(items)
                    }
                })
        })
    }

    getDataWhere(field, value, init = {}, operator = "==", id = true) {
        return new Promise((resolve) => {
            FirebaseModel.getCollection(this.collection)
                .where(field, operator, value)
                .onSnapshot((query) => {
                    let results = [];
                    if (!query.empty) {
                        query.forEach((doc) => {
                            let data = doc.data();
                            if (id) data.id = doc.id;
                            results.push(Object.assign({}, init, data));
                        });
                        resolve(results);
                    } else resolve(results);
                });
        });
    }
    /***************************END RECUPERATION*******************************/

    /***************************VERIFCATION*******************************/
    static ifSubCollectionExist(collection, document, subcollection) {
        return new Promise((res) => {
            firebase.firestore().collection(collection).doc(document).collection(subcollection).get().then((reponse) => {
                res(reponse.size);
            })
        });
    }
    /***************************END VERIFCATION*******************************/

    /***************************DELETE***********************************/
    static deleteDocumentSubSubCollection(collection, doc, subCollection, subSubDoc, subSubCollection,doumentId) {
        return firebase.firestore()
        .collection(collection)
        .doc(doc)
        .collection(subCollection)
        .doc(subSubDoc)
        .collection(subSubCollection)
        .doc(doumentId)
        .delete();
    }
    static deleteDocumentSubcollection(collection, document, subcollection, doc) {
        return firebase.firestore()
        .collection(collection)
        .doc(document)
        .collection(subcollection)
        .doc(doc)
        .delete();
    }
        
    static deleteDocumentcollection(collection, document) {
        return firebase.firestore().collection(collection).doc(document).delete();
    }
    /***************************END DELETE*******************************/
     /***************************GROUP COLLECTION*******************************/

    // static getDocumentsCollectionGroup(collection) {
    //     return firebase.firestore().collectionGroup(collection)
    // }

}

export default FirebaseModel;
