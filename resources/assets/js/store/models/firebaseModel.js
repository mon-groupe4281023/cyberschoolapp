import firebase from "firebase";

class FirebaseModel {
    /**
     * Constructor
     * @param collection {string} Nom de la collection
     */
    constructor(collection) {
        this.collection = collection;
    }


    /***************************ENREGISTREMENT*******************************/
     saveDocument(collection, data) {
        data.createdAt = firebase.firestore.FieldValue.serverTimestamp();
        return firebase
            .firestore()
            .collection(collection)
            .add(data)
    }
    static async saveDocumentSubCollection(collection, doc, subCollection, data) {
        return firebase.firestore().collection(collection)
            .doc(doc)
            .collection(subCollection)
            .add(data)
    }
    /***************************END ENREGISTREMENT*******************************/


    /***************************MODIFICATION*******************************/
    static updateDocument(id, fieldUpdate, collection) {
        return FirebaseModel.getCollection(collection)
            .doc(id)
            .update(fieldUpdate);
    }

    static updateDocumentSubcollection(collection, document, subcollection, id, data) {
        return firebase.firestore().collection(collection).doc(document).collection(subcollection).doc(id).set(data)
    }
    /***************************END MODIFICATION*******************************/



    /***************************RECUPERATION*******************************/
    static getCollection(collection) {
        return firebase.firestore().collection(collection)
    }
    static foreachItem(data) {
        const results = []
        data.then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                const data = doc.data()
                data.id = doc.id
                results.push(data)
            })
        })
        return results
    }
    static getDocument(id, collection) {
        return firebase
            .firestore()
            .collection(collection)
            .doc(id)
            .get();
    }
    getData(id, field = null, collection = null) {
        return new Promise((resolve, rejects) => {
            FirebaseModel.getDocument(id, collection ? collection : this.collection)
                .then((doc) => {
                    if (doc.exists) {
                        if (field) {
                            if (doc.data()[field]) resolve(doc.data()[field]);
                            else rejects("Field undefended " + field, doc.data());
                        } else resolve(doc.data());
                    } else rejects(doc);
                })
                .catch((error) => {
                    console.warn(`Données introuvables : ${error}`);
                });
        });
    }
    async getDataJoinField(id, field, collection, fieldRef = "libelle") {
        let collectionResult;
        const idRef = await this.getData(id, field);
        if (idRef && idRef.length > 0) {
            collectionResult =
                typeof idRef === "object"
                    ? await this.getDataManyDocument(idRef, fieldRef, collection)
                    : await this.getData(idRef, fieldRef, collection);
        }
        return new Promise((resolve, reject) => {
            if (collectionResult) {
                resolve(collectionResult);
            } else {
                reject(idRef);
            }
        });
    }
    static getDataManyDocument(idArray, field = null, collection) {
        let results = [];
        idArray.forEach(async (id) => {
            results.push(await this.getData(id, field, collection));
        });
        return new Promise((resolve) => {
            resolve(results);
        });
    }
    static getDocumentSubCollection(collection, document, subCollection) {
        return firebase.firestore()
            .collection(collection)
            .doc(document)
            .collection(subCollection)
            .get()
    }

    static getDocumentSubCollectionSnapShot(collection, document, subCollection) {
        return new Promise((resolve) => {
            firebase.firestore()
                .collection(collection)
                .doc(document)
                .collection(subCollection)
                .onSnapshot((snapshot) => {
                    let items = []
                    if (!snapshot.empty) {
                        snapshot.forEach((doc) => {
                            const data = doc.data()
                            data.id = doc.id
                            items.push(data)
                        })
                        resolve(items)
                    } else {
                        resolve(items)
                    }
                })
        })
    }

          getDataWhere(field, value, init = {}, operator = "==", id = true) {
        return new Promise((resolve) => {
            FirebaseModel.getCollection(this.collection)
                .where(field, operator, value)
                .onSnapshot((query) => {
                    let results = [];
                    if (!query.empty) {
                        query.forEach((doc) => {
                            let data = doc.data();
                            if (id) data.id = doc.id;
                            results.push(Object.assign({}, init, data));
                        });
                        resolve(results);
                    } else resolve(results);
                });
        });
    }
    /***************************END RECUPERATION*******************************/

    /***************************VERIFCATION*******************************/
    static ifSubCollectionExist(collection, document, subcollection) {
        return new Promise((res) => {
            firebase.firestore().collection(collection).doc(document).collection(subcollection).get().then((reponse) => {
                res(reponse.size);
            })
        });
    }

    /***************************END VERIFCATION*******************************/

   static allItemWhere(collection,field,value, operator = "=="){
     return   FirebaseModel.getCollection(collection)
        .where(field, operator, value).get()
    }

    /***************************DELETE***********************************/
    static deleteDocumentSubcollection(collection, document, subcollection, doc) {
        return firebase.firestore()
            .collection(collection)
            .doc(document)
            .collection(subcollection)
            .doc(doc)
            .delete();
    }

    static deleteDocumentcollection(collection, document) {
        return firebase.firestore().collection(collection).doc(document).delete();
    }
    /***************************END DELETE*******************************/

    /*************************** UPLOAD *********************************/

    static uploadImage(reference, file) {
        // reference = /erp/clients/def_2.jpg
        return firebase.storage().ref(reference).put(file)
    }
    /***************************END UPLOAD*******************************/

    /***************************GET TYPE*******************************/
    static getDocumentSubcollectionWithCondition(collection, document, subcollection, field, value, operator = "==") {
        return firebase.firestore().collection(collection).doc(document).collection(subcollection).where(field, operator, value).get()
    }
}

export default FirebaseModel;
