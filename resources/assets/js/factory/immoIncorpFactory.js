/* eslint-disable no-undef */
let immoIncorpFactory = ( function() {
    let abstractVehicleFactory // instance;
    let types = {};

    function registerImmoIncorp( type, ImmoIncorp ) {
        var proto = ImmoIncorp.prototype;

        // only register classes that fulfill the vehicle contract
        if ( proto.drive && proto.breakDown ) {
            types[type] = ImmoIncorp;
        }   
    }

    function init() {
        let immoIncorp = require('../models/besoins/immoIncorporelle');
        registerImmoIncorp('financier',immoIncorp);

        let immoIncorpExec = require('../models/besoins/immoIncorporelle');
        registerImmoIncorp('execution',immoIncorpExec);
        
    }
} )

export default immoIncorpFactory;