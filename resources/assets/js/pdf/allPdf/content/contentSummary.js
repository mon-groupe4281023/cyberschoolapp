let money = 0;
let precision = 0 ;
const htmlToPdfmake = require("html-to-pdfmake");
let format = (ch) => {
    let chh = [];
    let elt = [];
    ch = ch + '';
    let tabs = ch.split('');
    if (ch.length <= 3)
        return ch;
    while ((tabs.length - 1) % 3 !== 0) {
        elt.push(tabs.shift())
    }
    tabs.forEach((item, index, tab) => {
        chh.push(item);
        if (index % 3 === 0 && index !== tab.length - 1) {
            chh.push(' ')
        }
    });
    return elt.join('') + chh.join('')
};
let dateFormat = (d) => {
    let chs = d + '';
    chs = chs.split('-');
    let date = new Date(chs[0], chs[1] - 1, chs[2]);
    return new Intl.DateTimeFormat('fr').format(date);
};
let secteurShow = (secteur) => {
    let nameSecteur;
    switch (secteur) {
        case '1':
            nameSecteur = 'Secteur Primaire';
            break;
        case '2':
            nameSecteur = 'Secteur Secondaire';
            break;
        case '3':
            nameSecteur = 'Secteur Tertiaire';
        break;
        default:
            nameSecteur = 'Secteur Quaternaire'
    }
    return nameSecteur;
};

export function buildArrayVisionLong (visionArray) {
    let header = [
        {text: 'Objectif(s)', style: ['bolded','whiteColor','backgroundTableauStandard','centered']},
        {text: 'Plan d\'action', style: ['bolded','whiteColor','backgroundTableauStandard','centered']},
        {text: 'Date début', style: ['bolded','whiteColor','backgroundTableauStandard','centered']},
        {text: 'Date fin', style: ['bolded','whiteColor','backgroundTableauStandard','centered']},
    ];
    let result = Array(header);
    if (visionArray) {

        visionArray.forEach(vArray => {

            result.push([
                {text: vArray.objectif, alignment: 'center', width:'auto'},
                {text: vArray.plan, alignment: 'center', width:'auto'},
                {text: (vArray.dateDebut) ? dateFormat(vArray.dateDebut) : ' ', alignment: 'center', width:'auto'},
                {text: (vArray.dateFin) ? dateFormat(vArray.dateFin) : ' ', alignment: 'center', width:'auto'},
            ]);
        })
    }
    return result;
}
 export function buildArrayVisionCourt (visionArray) {
    let header = [
        {text: 'Objectif(s)', style: ['bolded','whiteColor','backgroundTableauStandard','centered']},
        {text: 'Plan d\'action', style: ['bolded','whiteColor','backgroundTableauStandard','centered']},
        {text: 'Date début', style: ['bolded','whiteColor','backgroundTableauStandard','centered']},
        {text: 'Date fin', style: ['bolded','whiteColor','backgroundTableauStandard','centered']},
    ];
    let result = Array(header);
    if (visionArray) {

        visionArray.forEach(vArray => {

            result.push([
                {text: vArray.objectif, alignment: 'center', width:'auto'},
                {text: vArray.plan, alignment: 'center', width:'auto'},
                {text: (vArray.dateDebut) ? dateFormat(vArray.dateDebut) : ' ', alignment: 'center', width:'auto'},
                {text:  (vArray.dateFin) ? dateFormat(vArray.dateFin) : ' ', alignment: 'center', width:'auto'},
            ]);
        })
    }
    return result;

}
export function buildArrayProduits(produitArray){
    let header = [
        {text: 'Produits / services phares', style: ['bolded','whiteColor','backgroundTableauStandard','centered']},
        {text: 'Solutions (désagrément à éradiquer)', style: ['bolded','whiteColor','backgroundTableauStandard','centered']},
        {text: 'Valeur ajoutée apportées aux clients', style: ['bolded','whiteColor','backgroundTableauStandard','centered']}
    ];
    let result = Array(header);
    produitArray.forEach(function (produit) {
        result.push([
                {text: (produit.produit) ? produit.produit : ' ', alignment: 'center'},
                {text: (produit.solution) ? produit.solution: ' ', alignment: 'center'},
                {text: (produit.valeurAjoute) ? produit.valeurAjoute : ' ', alignment: 'center'}
        ]);
    });
    return result;
}
export function buildArrayClients (clientArray){
    let header = // en-tete du tableau descriptif du porteur de projet
    [
        {text: 'Aspiration de vos clients', style: ['bolded','whiteColor','backgroundTableauStandard','centered']},
        {text: 'Problèmes (qui gènent les clients)', style: ['bolded','whiteColor','backgroundTableauStandard','centered']},
        {text: 'Bénéfices(au-delà de leurs attentes)', style: ['bolded','whiteColor','backgroundTableauStandard','centered']}
    ];
    let result = Array(header);
    if(!clientArray){
        result.push([
            {text: '', alignment: 'center', width:'*'},
            {text: '', alignment: 'center'},
            {text: '', alignment: 'center'}
        ])
        return result
    };
    clientArray.forEach(function (client) {
        result.push([
            {text: client.aspiration, alignment: 'center', width:'auto'},
            {text: client.probleme, alignment: 'center', width:'auto'},
            {text: client.benefice, alignment: 'center', width:'auto'}
        ]);
    });
    return result;
}
export function buildArrayAssocies(associeArray){
    let header = // en-tete du tableau descriptif du porteur de projet
    [
        {text: 'Nom & Prénom', style: ['bolded','whiteColor','backgroundTableauStandard','centered']},
        {text: 'Genre', style: ['bolded','whiteColor','backgroundTableauStandard','centered']},
        {text: 'Age', style: ['bolded','whiteColor','backgroundTableauStandard','centered']},
        {text: 'Niveau d\'étude', style: ['bolded','whiteColor','backgroundTableauStandard','centered']},
        {text: 'Compétences', style: ['bolded','whiteColor','backgroundTableauStandard','centered']}
    ];
    let result = Array(header);
    if(!associeArray){
        result.push([
            {text: '', alignment: 'center', width:'*'},
            {text: '', alignment: 'center'},
            {text: '', alignment: 'center'},
            {text: '', alignment: 'center'},
            {text: '', alignment: 'center'}
        ]);
        return result
    };
    associeArray.forEach(function (associe) {
        result.push([
            {text: `${associe.nom} \n ${associe.nom}`, alignment: 'center'},
            {text: associe.sexe, alignment: 'center'},
            {text: associe.age, alignment: 'center'},
            {text: associe.niveau, alignment: 'center'},
            {text: associe.competence, alignment: 'center'}
        ]);
    });
    return result;
}
export function buildArrayRecap(totaux){
    // en-tete du sous-tableau des besoins
    let investHeader = [
        {text: 'INVESTISSEMENT', style:['whiteColor','bolded','subheader','centered','backgroundYellowRecap'], colSpan:6, border:[false,false,false,false]}, {}, {}, {}, {}, {}
    ];
    let result = new Array(investHeader);
    result.push([
        {text: 'BESOINS (investissement)', style:['whiteColor','bolded','backgroundRed','subheader'], colSpan:5, border:[false,false,false,false]}, {}, {}, {}, {},
        { text:'Coût', style:['bolded','whiteColor','backgroundRed', 'centered'], border:[false,false,false,false]}
    ]);
    // insertion des elements de besoins
    if(totaux.besoins.length > 0 && typeof (totaux.besoins) === 'object'){
        totaux.besoins.forEach(besoin => {
            result.push([
                {text: besoin.besoin, colSpan:4, border:[false,false,false,false]},{},{},{},
                { text: format(besoin.cout) + ' ' + money, style:['righted'], border:[false,false,false,false], colSpan:2}, {}
            ]);
        });
    }
    // ligne de sous-total besoins
    result.push([
        {text:'TOTAL DES BESOINS', style:['bolded','backgroundGreyRecap'], colSpan:4, border:[false,false,false,false]},{},{},{},
        {text: format(totaux.totalBesoin) + ' ' + money, style:['redColor','bolded','righted','backgroundGreyRecap'], border:[false,false,false,false], colSpan:2}, {}
    ]);
    result.push([{ text:'\n', colSpan:6, border:[false,false,false,false]}]);

    // en-tete du sous-tableau des ressources
    result.push([
        {text:'RESSOURCES (capital)', style:['whiteColor','backgroundGreenRecap', 'bolded','subheader'], colSpan:5, border:[false,false,false,false]}, {},  {}, {}, {}, { text:'Coût', style:['bolded','whiteColor','backgroundGreenRecap', 'centered'], border:[false,false,false,false]}
    ]);

    // insertions des elements de ressources
    if(totaux.ressources.length > 0 && typeof (totaux.ressources) === 'object'){
        totaux.ressources.forEach(ressource => {
            result.push([
                { text:ressource.besoin, colSpan:4, border:[false,false,false,false]}, {}, {},{},
                { text: format(ressource.cout) + ' ' + money, style:['righted'], border:[false,false,false,false], colSpan:2}, {}
            ]);
        });
    }
    // sous-total des ressources
    result.push([
        {text:'TOTAL DES RESSOURCES',colSpan:4, style:['bolded', 'backgroundGreyRecap'], border:[false,false,false,false]},{},{},{},
        {text: format(totaux.totalRessource) + ' ' + money , style:['redColor','bolded','righted','backgroundGreyRecap'], border:[false,false,false,false], colSpan:2}, {}
    ]);
    // solde cumulé
    let solde = totaux.totalRessource - totaux.totalBesoin
    result.push([
        {text: 'Solde cumulé', style: ['bolded',(solde < 0) ? 'backgroundRed': 'backgroundCumulRecap','subheader'], colSpan:4, border:[false,false,false,false]}, {}, {}, {},
        {text: format(solde) + ' ' + money, style: [(solde < 0) ? 'backgroundRed': 'backgroundCumulRecap','bolded','righted'], border:[false,false,false,false], colSpan:2}, {}
    ]);
    result.push([{ text:'\n', colSpan:6, border:[false,false,false,false]}]);

    // en-tete du sous-tableau des ventes
    result.push([
        {text: 'EXPLOITATION', style:['whiteColor','bolded','subheader','centered','backgroundDarkBlueRecap'], colSpan:6, border:[false,false,false,false]}, {}, {}, {}, {}, {}
    ]);
    result.push([
        {text: 'VENTES', style: ['whiteColor','bolded','backgroundLightBlueRecap','subheader'], border:[false,false,false,false]},
        {text: 'Qté', style: ['whiteColor','bolded','backgroundLightBlueRecap','subheader'], border:[false,false,false,false]},
        {text: 'PU', style: ['whiteColor','bolded','backgroundLightBlueRecap','subheader'], border:[false,false,false,false]},
        {text: 'Total/mois', style: ['whiteColor','bolded','backgroundLightBlueRecap','subheader'], border:[false,false,false,false]},
        {text: 'Nb. mois', style: ['whiteColor','bolded','backgroundLightBlueRecap','subheader'], border:[false,false,false,false], width: '15%'},
        {text: 'Année', style: ['whiteColor','bolded','backgroundLightBlueRecap','subheader'], border:[false,false,false,false]}
    ]);
    // insertion ds ventes
    if(totaux.ventes.length > 0 && typeof (totaux.ventes) === 'object'){
        totaux.ventes.forEach(vente => {
            result.push([
                { text: vente.besoin, border:[false,false,false,false]},
                { text: vente.quantite, style:['centered'], border:[false,false,false,false]},
                { text: format(vente.punitaire) + ' ' + money, style:['righted'], border:[false,false,false,false]},
                { text: format(vente.ptotal) + ' ' + money, style:['righted'], border:[false,false,false,false]},
                { text: vente.nombre_mois, style:['centered'], border:[false,false,false,false]},
                { text: format(vente.ptotVenteAnnuel) + ' ' + money, style:['righted'], border:[false,false,false,false]}
            ]);
        });
    }
    // sous-total des ventes
    result.push([
        {text: 'TOTAL DES VENTES', style: ['bolded', 'backgroundGreyRecap'], colSpan:5, border:[false,false,false,false]}, {}, {}, {}, {},
        {text: format(totaux.totalVente.annuel) + ' ' + money, style:['redColor','bolded','righted', 'backgroundGreyRecap'], border:[false,false,false,false]}
    ]);
    // MOYENNE VENTES MENSUELLES (TOTAL VENTE ANNUELLES/12)
    result.push([
        {text: 'MOYENNE VENTES MENSUELLES (TOTAL VENTE ANUELLES/12)',         style: [ 'backgroundGreyRecap'], colSpan:5, border:[false,false,false,false]}, {}, {}, {}, {},
        {text: format(parseInt(totaux.totalVente.annuel / 12)) + ' ' + money, style:['redColor','bolded','righted', 'backgroundGreyRecap'], border:[false,false,false,false]}
    ]);
    result.push([{ text:'\n', colSpan:6, border:[false,false,false,false] }]);

    // tableau des charges
    result.push([
        {text: 'CHARGES', style: ['whiteColor','bolded','backgroundLightBlueRecap','subheader'], border:[false,false,false,false]},
        {text: 'Qté', style: ['whiteColor','bolded','backgroundLightBlueRecap','subheader'], border:[false,false,false,false]},
        {text: 'PU', style: ['whiteColor','bolded','backgroundLightBlueRecap','subheader'], border:[false,false,false,false]},
        {text: 'Total/mois', style: ['whiteColor','bolded','backgroundLightBlueRecap','subheader'], border:[false,false,false,false]},
        {text: 'Nb. mois', style: ['whiteColor','bolded','backgroundLightBlueRecap','subheader'], border:[false,false,false,false], width: '15%'},
        {text: 'Année', style: ['whiteColor','bolded','backgroundLightBlueRecap','subheader'], border:[false,false,false,false]}
    ]);

    // insertion des charges
    if(totaux.charges.length > 0 && typeof (totaux.charges) === 'object'){
        totaux.charges.forEach(charge => {
            result.push([
                { text: charge.besoin, border:[false,false,false,false]},
                { text: charge.quantite, style:['centered'], border:[false,false,false,false]},
                { text: format(charge.punitaire) + ' ' + money, style:['righted'], border:[false,false,false,false]},
                { text: format(charge.ptotal) + ' ' + money, style:['righted'], border:[false,false,false,false]},
                { text: charge.nombre_mois, style:['centered'], border:[false,false,false,false]},
                { text: format(charge.ptotChargeAnnuel) + ' ' + money, style:['righted'], border:[false,false,false,false]}
            ]);
        });
    }
    //sous-totaux des charges
    result.push([
        {text:'TOTAL DES CHARGES', style:['bolded', 'backgroundGreyRecap'], colSpan:5, border:[false,false,false,false]}, {}, {}, {}, {},
        {text: format(totaux.totalCharge.annuel) + ' ' + money, style:['redColor','bolded','righted', 'backgroundGreyRecap'], border:[false,false,false,false]}
    ]);
    // MOYENNE VENTES MENSUELLES (TOTAL VENTE ANNUELLES/12)
    result.push([
        {text:'MOYENNE CHARGES MENSUELLES (TOTAL CHARGES ANNUELLES/12)',         style: [ 'backgroundGreyRecap'], colSpan:5, border:[false,false,false,false]}, {}, {}, {}, {},
        {text: format(parseInt(totaux.totalCharge.annuel / 12)) + ' ' + money, style:['redColor','bolded','righted', 'backgroundGreyRecap'], border:[false,false,false,false]}
    ]);
    // le fameux benefice net
    let beneficeMensuel = totaux.totalVente.mensuel - totaux.totalCharge.mensuel
    let beneficeAnnuel = totaux.totalVente.annuel - totaux.totalCharge.annuel
            
    result.push([
        {text: 'RESULTAT NET (CHIFFRE D\'AFFAIRES - CHARGES)', style: ['bolded',(beneficeMensuel < 0) ? 'backgroundRed': 'backgroundDarkBlueRecap','subheader', 'lefted', 'whiteColor'], colSpan:5, border:[false,false,false,false]},{},{}, {},{},
        { text: format(beneficeAnnuel) + ' ' + money, style:['bolded', 'whiteColor',(beneficeAnnuel < 0) ? 'backgroundRed': 'backgroundDarkBlueRecap','righted'], border:[false,false,false,false]}
    ]);
    return result;
}
/**
* Returns the text from a HTML string
*
* @param {html} String The html string
*/
export function stripHtml(html){
    /**
    * Elements de configuration du formattage de texte appliquer :
    * [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
    * ['bold', 'italic', 'underline', 'strike'],
    * toggled buttons
    * [{ 'list': 'ordered'}, { 'list': 'bullet' }],
    *
    stack: [
        // second column consists of paragraphs
        'paragraph A',
        'paragraph B',
        'these paragraphs will be rendered one below another inside the column'
    ]
    **/

    let result = { stack: [] };

    if(html) {
        html.replace('&nbsp;',"  ");
    };
    // Set the HTML content with the providen
    // temporalDivElement.innerHTML = html;
    // Retrieve the text property of the element (cross-browser support)
    // return temporalDivElement.textContent || temporalDivElement.innerText || "";
    return html;
}

export function bmcStripHtml (bmc) {
    let bmcStrip = {}
    for (const bmcProperty in bmc) {
        const bmcStripProp = this.stripHtml(bmc[bmcProperty])
        bmcStrip[bmcProperty] = bmcStripProp.replace(/<(?:.|\s)*?>/gi,"")
    }
    return bmcStrip
}

export function content(summaryDoc, metaData, password = '1234',paramsMoney){
    //money
     money = paramsMoney['currency']
     precision = paramsMoney['precision']
    //HTML FORMAT 
    var strdesc=htmlToPdfmake(summaryDoc.descrSynthetique ? summaryDoc.descrSynthetique : "");
    var stropp=htmlToPdfmake(summaryDoc.descrOpportunite ? summaryDoc.descrOpportunite : "");
    var strsitu=htmlToPdfmake(summaryDoc.situationActu ? summaryDoc.situationActu : "");
    var strnaiss=htmlToPdfmake(summaryDoc.naissProjet ? summaryDoc.naissProjet : "");
    var strzone=htmlToPdfmake(summaryDoc.zone ? summaryDoc.zone : "");
    var strconcu=htmlToPdfmake(summaryDoc.concurrent ? summaryDoc.concurrent : "");

    
    // implementation du pdf
    summaryDoc.bmc = this.bmcStripHtml(summaryDoc.bmc)
 
       let  result = [
            // en-tete du doc
         
            {text:'Résumé Opérationnel', pageOrientation: 'portrait',  style:'header', margin: [200,2,0,10]},
            // liste numérotée pour le summary
            {
                ol: [
                    // partie 1 : projet
                    [
                        {text:' Présentation du projet', style:'subheader', margin: [0,2,0,10]},
                        {
                            start:1,
                            ol: [
                                [
                                    {text:'Titre / Libellé du projet', style:['bolded'], margin:[0,5,0,4]},
                                    {text: summaryDoc.titre, margin:[0,0,0,8]}
                                ],
                                [
                                    {text:'Secteur d\'activité', style:['bolded'], margin:[0,5,0,4]},
                                    {text: secteurShow(summaryDoc.secteurActivite), margin:[0,0,0,8]}
                                ],
                                [
                                    {text:'Branche d\'activité', style:['bolded'], margin:[0,5,0,4]},
                                    {text: summaryDoc.brancheActivite, margin:[0,0,0,8]}
                                ],
                                [
                                    {text:'Activité', style:['bolded'], margin:[0,5,0,4]},
                                    {text: summaryDoc.activite[0], margin:[0,0,0,8]}
                                ],
                                [
                                    {text:'Activité génératrice de revenu :', style:['bolded'], margin:[0,5,0,4]},
                                    {text: summaryDoc.statutActivite ? summaryDoc.statutActivite : "", margin:[0,0,0,8]}
                                ],
                                [
                                    {text:'Temps de travail sur le projet :', style:['bolded'], margin:[0,5,0,4]},
                                    {text: summaryDoc.heureTravaille ? (summaryDoc.heureTravaille+ ' chaque jour') : "", margin:[0,0,0,8]}
                                ],
                                [
                                    {text:'Description synthétique du projet/Entreprise et sa forme juridique: ', style:['bolded'], margin:[0,5,0,4]},
                                    {text: summaryDoc.formJuridique ? 'Forme juridique: ' + summaryDoc.formJuridique : "", style:['bolded'], margin:[0,0,0,7]},
                                    ...(strdesc) 
                                    //{text: this.stripHtml(summaryDoc.descrSynthetique), margin:[0,0,0,8]}
                                ],
                                [
                                    {text:'Description de l\'opportunité (problème à resoudre) et l\'impact du projet :', style:['bolded'], margin:[0,5,0,4]},
                                    ...(stropp) 
                                    //{text: this.stripHtml(summaryDoc.descrOpportunite), margin:[0,0,0,8]}
                                ],
                                [
                                    {text:'Description de la situation actuelle du projet/Entreprise', style:['bolded'], margin:[0,5,0,4]},
                                    ...(strsitu) 
                                    //{text: this.stripHtml(summaryDoc.situationActu), margin:[0,0,0,4]}
                                ]
                            ]
                        },
                        { text:'', margin: [0,0,0,15] }
                    ],
                    // partie 2 : porteur de projet
                    [
                        {text:' Présentation du porteur de projet', style:'subheader', margin: [0,0,0,5]},
                        {
                            ol: [
                                [
                                    {text: 'Porteur de projet :', style:['bolded'], margin:[0,5,0,4]},
                                    {
                                        table: {
                                            headerRows: 1,
                                            widths:['*','*','*','*','*'],
                                            // en-tete du tableau descriptif du porteur de projet
                                            // dontBreakRows: true,
                                            // keepWithHeaderRows: 1,
                                            body: [
                                                [
                                                    {text: 'Nom & Prénom', style: ['bolded','whiteColor','backgroundTableauStandard','centered']},
                                                    {text: 'Genre', style: ['bolded','whiteColor','backgroundTableauStandard','centered']},
                                                    {text: 'Age', style: ['bolded','whiteColor','backgroundTableauStandard','centered']},
                                                    {text: 'Niveau d\'étude', style: ['bolded','whiteColor','backgroundTableauStandard','centered']},
                                                    {text: 'Compétences', style: ['bolded','whiteColor','backgroundTableauStandard','centered']}
                                                ],
                                                [
                                                    {text: `${summaryDoc.porteur.nomPorteur} \n ${summaryDoc.porteur.prenom}` ,alignment: 'center', width:'*'},
                                                    {text: summaryDoc.porteur.sexe ?  summaryDoc.porteur.sexe : "", alignment: 'center'},
                                                    {text: summaryDoc.porteur.age ? summaryDoc.porteur.age : "", alignment: 'center'},
                                                    {text: summaryDoc.porteur.niveauPorteur ? summaryDoc.porteur.niveauPorteur : "", alignment: 'center'},
                                                    {text: summaryDoc.porteur.competence ? summaryDoc.porteur.competence : "", alignment: 'center'}
                                                ]
                                            ],
                                        }
                                    },
                                    {text:'', style:['bolded'], margin:[0,5,0,4]},
                                    {
                                        table: {
                                            headerRows: 1,
                                            widths:['*','*','*','*','*'],
                                            // dontBreakRows: true,
                                            // keepWithHeaderRows: 1,
                                            body: [
                                                // en-tete du tableau descriptif du porteur de projet
                                                [
                                                    {text:'', border: [false, false, true, true],},
                                                    {text: 'Francais', style: ['bolded','whiteColor','backgroundTableauStandard','centered']},
                                                    {text: 'Anglais', style: ['bolded','whiteColor','backgroundTableauStandard','centered']},
                                                    {text: 'Espagnol', style: ['bolded','whiteColor','backgroundTableauStandard','centered']},
                                                    {text: 'Autre:', style: ['bolded','whiteColor','backgroundTableauStandard','centered']}
                                                ],
                                                [
                                                    {text: 'Lu', style:['whiteColor', 'backgroundTableauStandard']},
                                                    {text: (summaryDoc.porteur.langue.Francais.lu) ? summaryDoc.porteur.langue.Francais.lu : "non" , alignment: 'center'},
                                                    {text: (summaryDoc.porteur.langue.Anglais.lu ? summaryDoc.porteur.langue.Anglais.lu : "non") , alignment: 'center'},
                                                    {text: (summaryDoc.porteur.langue.Espagnole.lu ? summaryDoc.porteur.langue.Espagnole.lu : "non") , alignment: 'center'},
                                                    {text: (summaryDoc.porteur.langue.Autre.lu ? summaryDoc.porteur.langue.Autre.lu : "non") , alignment: 'center'},
                                                ],
                                                [
                                                    {text: 'Parlé', style:['whiteColor', 'backgroundTableauStandard']},
                                                    {text: (summaryDoc.porteur.langue.Francais.parlé ? summaryDoc.porteur.langue.Francais.parlé : "non") , alignment: 'center'},
                                                    {text: (summaryDoc.porteur.langue.Anglais.parlé ? summaryDoc.porteur.langue.Anglais.parlé : "non") , alignment: 'center'},
                                                    {text: (summaryDoc.porteur.langue.Espagnole.parlé ? summaryDoc.porteur.langue.Espagnole.parlé : "non") , alignment: 'center'},
                                                    {text: (summaryDoc.porteur.langue.Autre.parlé ? summaryDoc.porteur.langue.Autre.parlé : "non") , alignment: 'center'},
                                                ],
                                                [
                                                    {text: 'Ecrit', style:['whiteColor', 'backgroundTableauStandard']},
                                                    {text: (summaryDoc.porteur.langue.Francais.écrit ? summaryDoc.porteur.langue.Francais.écrit : "non") , alignment: 'center'},
                                                    {text: (summaryDoc.porteur.langue.Anglais.écrit ? summaryDoc.porteur.langue.Anglais.écrit : "non") , alignment: 'center'},
                                                    {text: (summaryDoc.porteur.langue.Espagnole.écrit ? summaryDoc.porteur.langue.Espagnole.écrit : "non") , alignment: 'center'},
                                                    {text: (summaryDoc.porteur.langue.Autre.écrit ? summaryDoc.porteur.langue.Autre.écrit : "non") , alignment: 'center'},
                                                ]
                                            ],

                                        }
                                    },

                                    {text:'', style:['bolded'], margin:[0,0,0,10]}
                                ],
                                [
                                    {text: 'Associés :', margin:[0,5,0,5], style:['bolded']},
                                    {
                                        table: {
                                            headerRows: 1,
                                            widths:['*','*','*','*','*'],
                                            // keepWithHeaderRows: 1,
                                            body: this.buildArrayAssocies(summaryDoc.associe)
                                        }
                                    },
                                    {text:'', style:['bolded'], margin:[0,0,0,10]}
                                ],
                                [
                                    {text:'Naissance du projet / idée :', style:['bolded'], margin: [0,5,0,0]},
                                    ...(strnaiss) 
                                    //{text:this.stripHtml(summaryDoc.naissProjet), margin: [0,0,0,10]}
                                ],
                                [
                                    {text:'Vision et plan d\'action à court et moyen terme (entre 6 mois et 1an) :', style:['bolded'], margin:[0,5,0,5]},
                                    {
                                        table: {
                                            headerRows: 1,
                                            widths:['*','*','*', '*'],
                                            // dontBreakRows: true,
                                            // keepWithHeaderRows: 1,
                                            body: this.buildArrayVisionCourt(summaryDoc.visionCourt)
                                        }
                                    },
                                    {text:'', style:['bolded'], margin:[0,0,0,10]}
                                ],
                                [
                                    { text:'Vision et plan d\'action à long terme (1 à 3ans) :', style:['bolded'], margin: [0,5,0,5]},
                                    {
                                        table: {
                                            headerRows: 1,
                                            widths:['*','*','*', '*'],
                                            // dontBreakRows: true,
                                            // keepWithHeaderRows: 1,
                                            body: this.buildArrayVisionLong(summaryDoc.visionLong)
                                        }
                                    },
                                    {text:'', style:['bolded'], margin:[0,0,0,10]}
                                ]
                            ]
                        },
                        { text:'', margin:[0,0,0,15]}
                    ],
                    // partie 3 : produit et service
                    [
                        {text:' Présentation du produit/service', style:'subheader', margin: [0,0,0,10]},
                        {
                            ol: [
                                [
                                    {text: 'Produits :', style:['bolded'], margin:[0,5,0,4]},
                                    {
                                        table: {
                                            headerRows: 1,
                                            widths:['*','*','*'],
                                            // dontBreakRows: true,
                                            // keepWithHeaderRows: 1,
                                            body: this.buildArrayProduits(summaryDoc.produits)
                                        }
                                    },
                                    {text:'', style:['bolded'], margin:[0,0,0,10]}
                                ],
                                [
                                    {text: 'Clients :', style:['bolded'], margin:[0,5,0,4]},
                                    {
                                        table: {
                                            headerRows: 1,
                                            widths:['*','*','*'],
                                            // dontBreakRows: true,
                                            // keepWithHeaderRows: 1,
                                            body: this.buildArrayClients(summaryDoc.clients)
                                        }
                                    },
                                    {text:'', style:['bolded'], margin:[0,0,0,10]}
                                ],
                                [
                                    {text:'Concurrents directs :', style:['bolded'], margin:[0,5,0,0]},
                                   ...(strconcu)
                                    //{ text: this.stripHtml(summaryDoc.concurrent), margin:[0,2,0,10]}
                                ],
                                [
                                    { text:'Zones de ventes et part de marché :', style:['bolded'], margin: [0,5,0,0]},
                                   ...(strzone)
                                    //{text: this.stripHtml(summaryDoc.zone), margin:[0,2,0,10]}
                                ]
                            ]
                        },
                        { text:'', margin: [0,0,0,15] }
                    ],
                            
                    // partie 4 : Business Lean Model Canvas
                    [
                        {text:' Business Model Canvas', style:'subheader', margin: [0,0,0,10], pageOrientation: 'landscape', pageBreak: 'before'},

                        {
                            style: 'tableExample',
                            color: '#444',
                            table: {
                                widths: ['20%', '20%', '10%', '10%', '20%', '20%'],
                                heights:[170,170,160],
                                headerRows: 0,
                                // keepWithHeaderRows: 1,
                                body: [
                                    [
                                        // Partenaires clefs
                                        {
                                            stack: [
                                                {
                                                    image: metaData.imageBpPdf.handshake,
                                                    alignment: 'left',
                                                    width: 20,
                                                    height: 20,
                                                    margin: [0, 0, 0, 0]
                                                },
                                                { width: 'auto', text:' Partenaires clés', style:['bolded', 'colorIconBmc'], alignment:'left', margin:[28,-15,0,0] },
                                                { text: summaryDoc.bmc.partenaire ? ' \n' + summaryDoc.bmc.partenaire : "", alignment:'left'}
                                            ],
                                            rowSpan:2
                                        },
                                        // Activites clefs
                                        {
                                            stack: [
                                                {
                                                    image: metaData.imageBpPdf.shovel,
                                                    alignment:'left',
                                                    width: 20,
                                                    height: 20,
                                                    margin: [0, 0, 0, 0]
                                                },
                                                {text:' Activitiés clés', style:['bolded', 'colorIconBmc'], alignment: 'left',margin:[28,-15,0,0]},
                                                {text: summaryDoc.bmc.activite ? '\n' + summaryDoc.bmc.activite : "", alignment:'left'}
                                            ]
                                        },
                                        // Proposition de valeur
                                        {
                                            stack: [
                                                {
                                                    image: metaData.imageBpPdf.anniversary,
                                                    alignment:'left',
                                                    width: 20,
                                                    height: 20,
                                                    margin: [0, 0, 0, 0]
                                                },
                                                {text:' Proposition de valeur', style:['bolded', 'colorIconBmc'], alignment: 'left',margin:[28,-15,0,0]},
                                                {text: summaryDoc.bmc.proposition_valeur ? "\n" + summaryDoc.bmc.proposition_valeur : "", alignment:'left'}
                                            ],
                                            colSpan: 2, rowSpan:2},
                                        { },
                                        // Relation client
                                        {
                                            stack: [
                                                {
                                                    image: metaData.imageBpPdf.heart,
                                                    alignment:'left',
                                                    width: 20,
                                                    height: 20,
                                                    margin: [0, 0, 0, 0]
                                                },
                                                { text:' Relation Clients', style:['bolded', 'colorIconBmc'],alignment: 'left',margin:[28,-15,0,0]},
                                                { text:summaryDoc.bmc.relation ? "\n" + summaryDoc.bmc.relation : "", alignment:'left'}
                                            ]
                                        },
                                        // Segment Client
                                        {
                                            stack: [
                                                {
                                                    image: metaData.imageBpPdf.users,
                                                    alignment:'left',
                                                    width: 20,
                                                    height: 20,
                                                    margin: [0, 0, 0, 0]
                                                },
                                                { text:' Segments Clients', style:['bolded', 'colorIconBmc'],alignment: 'left',margin:[28,-15,0,0] },
                                                { text: summaryDoc.bmc.segment_client ? "\n" + summaryDoc.bmc.segment_client : "", alignment:'left'}
                                            ],
                                            rowSpan:2
                                        }
                                    ],
                                    [
                                        { },
                                        // Ressources clefs
                                        {
                                            stack: [
                                                {
                                                    image: metaData.imageBpPdf.database,
                                                    alignment:'left',
                                                    width: 20,
                                                    height: 20,
                                                    margin: [0, 0, 0, 0]
                                                },
                                                { text:' Ressources Clés', style:['bolded', 'colorIconBmc'], alignment: 'left',margin:[28,-15,0,0] },
                                                { text: summaryDoc.bmc.ressource ? "\n" + summaryDoc.bmc.ressource : "", alignment:'left'}
                                            ]
                                        },
                                        { },
                                        { },
                                        // Canaux de distribution
                                        {
                                            stack: [
                                                {
                                                    image: metaData.imageBpPdf.truck,
                                                    alignment:'left',
                                                    width: 20,
                                                    height: 20,
                                                    margin: [0, 0, 0, 0]
                                                },
                                                {text:' Canaux de distribution', style:['bolded', 'colorIconBmc'], alignment: 'left',margin:[28,-15,0,0]},
                                                {text: summaryDoc.bmc.canaux ? "\n" + summaryDoc.bmc.canaux : "" , alignment:'left'}
                                            ]
                                        },
                                        { }
                                    ],
                                    [
                                        // Structure de couts
                                        {
                                            colSpan: 3, rowSpan: 1,
                                            stack: [
                                                {
                                                    image: metaData.imageBpPdf.cash,
                                                    alignment:'left',
                                                    width: 20,
                                                    height:20,
                                                    margin: [0, 0, 0, 0]
                                                },
                                                { text:'Structure des coûts', style:['bolded', 'colorIconBmc'], alignment:'left',margin:[28,-15,0,0] },
                                                { text: summaryDoc.bmc.structure_cout ? "\n" + summaryDoc.bmc.structure_cout : "" , alignment:'left'}
                                            ]
                                        }, {}, {},
                                        // Sources de revenus
                                        {
                                            colSpan: 3, rowSpan: 1,
                                            stack: [
                                                {
                                                    image: metaData.imageBpPdf.card,
                                                    alignment:'left',
                                                    width: 20,
                                                    height:20,
                                                    margin: [0, 0, 0, 0]
                                                },
                                                { text:'Sources de Revenus', style:['bolded', 'colorIconBmc'], alignment:'left',margin:[28,-15,0,0] },
                                                { text: summaryDoc.bmc.source_revenu ? "\n" + summaryDoc.bmc.source_revenu : "" , alignment:'left'}
                                            ]
                                        }, {}, {}
                                    ]
                                ]
                            }
                        },
                        { text:'', margin: [0,0,0,15] }
                    ],
                            
                    // partie 5 : Chiffres: vetes et charges
                    // [
                    //     {text:' Chiffres clés Vente-Charge', style:'subheader', margin: [0,0,0,10], pageOrientation: 'portrait', pageBreak: 'before'},
                    //     [
                    //         {
                    //             table: {
                    //                 headerRows: 0,
                    //                 widths: ['*', 'auto', 'auto', 'auto', 'auto', 'auto'],
                    //                 body: this.buildArrayRecap(summaryDoc),
                    //                 layout:'noBorders'
                    //             },
                    //             // layout: 'lightHorizontalLines'
                    //         },
                    //     ],
                    //     { text:'', margin: [0,0,0,15] }
                    // ]
                ]
            }
    ]
    
   return result
         
   }
