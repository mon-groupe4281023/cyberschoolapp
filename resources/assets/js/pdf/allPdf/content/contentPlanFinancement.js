import PdfModel from '../../PdfModel'
import {calculRemboursement} from '../../../services/helper'
const arrayColumns = ['9%.11', '9.11%', '9%.11', '9%', '9%', '9%', '9%', '9.11%', '9.11%', '9.11%', '9.11%']
let money = 0;
export function buildArrayYears (allYears) {

    let yearsHeader = [
        {text: 'Années d\'exercices', style:['bolded','subheader','lefted',], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: 'Initial', style:['bolded','subheader','centered',], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: allYears.annee1, style:['bolded','subheader','centered',], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: allYears.annee2, style:['bolded','subheader','centered',], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: allYears.annee3, style:['bolded','subheader','centered',], colSpan:2, border:[false,false,false,false]
        },{},
    ];
    let result = new Array(yearsHeader);
    return result;
}
export function buildArrayAllBesoins(totaux, money) {
    let besoinsHeader = [
        {text: 'Besoins', style:['whiteColor', 'bolded','subheader','lefted', 'backgroundRed'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: PdfModel.contentFormat(totaux.besoins.initial) + money, style:['whiteColor', 'bolded','subheader','centered', 'backgroundRed'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: PdfModel.contentFormat(totaux.besoins.annee1) + money, style:['whiteColor', 'bolded','subheader','centered', 'backgroundRed'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(totaux.besoins.annee2) + money, style:['whiteColor', 'bolded','subheader','centered', 'backgroundRed'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(totaux.besoins.annee3) + money, style:['whiteColor', 'bolded','subheader','centered', 'backgroundRed'], colSpan:2, border:[false,false,false,false]
        },{},
    ];
    let result = new Array(besoinsHeader);
    return result;
}
export function buildArrayImmoI(immos, total, sTotaux,  money) {

    let immoHeader = [
        {text: 'Immobilisations Incorporelles', style:['bolded','subheader','lefted', 'backgroundPink'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: PdfModel.contentFormat(total) + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text:'-', style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
        },{},
    ];
    let result = new Array(immoHeader);

    if (immos) {
        immos.forEach(immo => {
            result.push([
                {text: immo.titre, style:['bolded','subheader','lefted'], colSpan:3, border:[false,false,false,false]},{},{},
                {
                    text: PdfModel.contentFormat(immo.prix) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]

                },{},
                {
                    text:'-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                },{},
                {
                    text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
                },{},
            ]);
        });
    }

    return result;
}
export function buildArrayImmoC(immos, total, sTotaux, money) {
    let immoHeader = [
        {text: 'Immobilisations Corporelles', style:['bolded','subheader','lefted', 'backgroundPink'], colSpan:3, border:[false,false,false,false]},{},{},
        {text: PdfModel.contentFormat(total.totalInitial) + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]},{},
        {text: '-', style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]},{},
        {text: '-', style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]},{},
        {text: '-', style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]},{},
    ];
    let result = new Array(immoHeader);
    let matInfos = immos['materielinfo'];
    let mobiliers = immos['mobilier'];
    let materiels = immos['materiel-machine-outil'];
    let batiments = immos['batiment-local-espacevente'];
    let voitures = immos['voiture-autres'];
    //Materiel Informatique
    result.push([
        {text: 'Matériel informatique', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:3, border:[false,false,false,false]},{},{},
        {text: PdfModel.contentFormat(sTotaux['materielInfo'].initial) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]},{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
    ]);
    //Liste Materiel Informatique
    for (let index in matInfos) {
        result.push([
            {text: matInfos[index].titre, style:['bolded','subheader','lefted'], colSpan:3, border:[false,false,false,false]},{},{},
            {text: PdfModel.contentFormat(matInfos[index].prix * matInfos[index].quantite) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]},{},
            {text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]},{},
            {text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]},{},
            {text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]},{},
        ]);
    }
    //Mobilier
    result.push([
        {text: 'Mobilier', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: PdfModel.contentFormat(sTotaux['mobilier'].initial) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
    ]);
    //Liste Mobilier
    for (let index in mobiliers) {
        result.push([
            {text: mobiliers[index].titre, style:['bolded','subheader','lefted'], colSpan:3, border:[false,false,false,false]},{},{},
            {text: PdfModel.contentFormat(mobiliers[index].prix * mobiliers[index].quantite) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]},{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
        ]);
    }
    //materiel-machine-outil
    result.push([
        {text: 'Materiels/Machines/Outils', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: PdfModel.contentFormat(sTotaux['materiel'].initial) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
    ]);
    //Liste Materiel-Machien-Outil
    for (let index in materiels) {
        result.push([
            {text: materiels[index].titre, style:['bolded','subheader','lefted'], colSpan:3, border:[false,false,false,false]},{},{},
            {
                text: PdfModel.contentFormat(materiels[index].prix * materiels[index].quantite) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]

            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
        ]);
    }
    //batiment-local-espacevente
    result.push([
        {text: 'Bâtiment/Local/Espace de vente', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: PdfModel.contentFormat(sTotaux['batiment'].initial) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
    ]);
    //Liste Batiment Local espace vente
    for (let index in batiments) {
        result.push([
            {text: batiments[index].titre, style:['bolded','subheader','lefted'], colSpan:3, border:[false,false,false,false]},{},{},
            {
                text: PdfModel.contentFormat(batiments[index].prix * batiments[index].quantite) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]

            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
        ]);
    }
    //voiture-autres
    result.push([
        {text: 'Moyen de roulement', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: PdfModel.contentFormat(sTotaux['voiture'].initial) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
    ]);
    //Liste Voitures Autres
    for (let index in voitures) {
        result.push([
            {text: voitures[index].titre, style:['bolded','subheader','lefted'], colSpan:3, border:[false,false,false,false]},{},{},
            {
                text: PdfModel.contentFormat(voitures[index].prix * voitures[index].quantite) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]

            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
        ]);
    }
    return result;
}
export function buildArrayFinance(finance, total, sTotaux, money) {
    let immoFinanceHeader = [
        {text: 'Immobilisations Financières', style:['bolded','subheader','lefted', 'backgroundPink'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: PdfModel.contentFormat(total) + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text:'-', style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
        },{},
    ];
    let result = new Array(immoFinanceHeader);
    let cautions = finance['cautions'];
    let depots = finance['depots'];
    let autres = finance['autres'];
    //Caution
    result.push([
        {text: 'Cautions', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: PdfModel.contentFormat(sTotaux['caution']) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
    ]);
    //Liste des cautions
    for (let index in cautions) {
        result.push([
            {text: cautions[index].titre, style:['bolded','subheader','lefted'], colSpan:3, border:[false,false,false,false]},{},{},
            {
                text: PdfModel.contentFormat(cautions[index].montant * cautions[index].quantite) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]

            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
        ]);
    }
    //depots
    result.push([
        {text: 'Dépots de garantie', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: PdfModel.contentFormat(sTotaux['depot']) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
    ]);
    for (let index in depots) {
        result.push([
            {text: depots[index].titre, style:['bolded','subheader','lefted'], colSpan:3, border:[false,false,false,false]},{},{},
            {
                text: PdfModel.contentFormat(depots[index].montant * depots[index].quantite) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]

            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
        ]);
    }
    //autres
    result.push([
        {text: 'Autres', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: PdfModel.contentFormat(sTotaux['autre']) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
    ]);
    //Liste autres
    for (let index in autres) {
        result.push([
            {text: autres[index].titre, style:['bolded','subheader','lefted'], colSpan:3, border:[false,false,false,false]},{},{},
            {
                text: PdfModel.contentFormat(autres[index].montant * autres[index].quantite) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]

            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
        ]);
    }
    return result;
}
export function buildArrayRE (ressources, total, sTotaux) {
    let reHeader = [
        {text: 'Remboursements des emprunts', style:['bolded','subheader','lefted', 'backgroundPink'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: PdfModel.contentFormat(total.annee1) + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(total.annee2) + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(total.annee3) + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
        },{},
    ];
    let result = new Array(reHeader);
    let banques = ressources['emprunts']['banque'];
    let microfinances = ressources['emprunts']['microfinance'];
    let banqueSub = ressources['subventions']['banque'];
    let institutionSub = ressources['subventions']['institution'];
    let autresSub = ressources['subventions']['autres'];
    //
    result.push([
        {text: 'Remboursements aux banques', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: PdfModel.contentFormat(sTotaux['reBanque']['annee1'].montant) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(sTotaux['reBanque']['annee2'].montant) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text:  PdfModel.contentFormat(sTotaux['reBanque']['annee3'].montant) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
    ]);
    for (let index in banques) {
        result.push([
            {text: banques[index].creancier, style:['bolded','subheader','lefted'], colSpan:3, border:[false,false,false,false]},{},{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]

            },{},
            {
                text: PdfModel.contentFormat(calculRemboursement(banques[index]).annee1.montant) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: PdfModel.contentFormat(calculRemboursement(banques[index]).annee2.montant) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: PdfModel.contentFormat(calculRemboursement(banques[index]).annee3.montant) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
        ]);
    }
    //
    result.push([
        {text: 'Remboursements aux microfinances', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: PdfModel.contentFormat(sTotaux['reMicrofinance'].annee1.montant) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(sTotaux['reMicrofinance'].annee2.montant) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(sTotaux['reMicrofinance'].annee3.montant) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
    ]);
    for (let index in microfinances) {
        result.push([
            {text: microfinances[index].creancier, style:['bolded','subheader','lefted'], colSpan:3, border:[false,false,false,false]},{},{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]

            },{},
            {
                text: PdfModel.contentFormat(calculRemboursement(microfinances[index]).annee1.montant) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: PdfModel.contentFormat(calculRemboursement(microfinances[index]).annee2.montant) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: PdfModel.contentFormat(calculRemboursement(microfinances[index]).annee3.montant) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
        ]);
    }
    //
    result.push([
        {text: 'Remboursement: subventions des banques', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: PdfModel.contentFormat(sTotaux['reSubventionB'].annee1.montant) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(sTotaux['reSubventionB'].annee2.montant) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(sTotaux['reSubventionB'].annee3.montant) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
    ]);
    for (let index in banqueSub) {
        result.push([
            {text: banqueSub[index].fournisseur, style:['bolded','subheader','lefted'], colSpan:3, border:[false,false,false,false]},{},{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]

            },{},
            {
                text: PdfModel.contentFormat(calculRemboursement(banqueSub[index]).annee1.montant) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: PdfModel.contentFormat(calculRemboursement(banqueSub[index]).annee2.montant) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: PdfModel.contentFormat(calculRemboursement(banqueSub[index]).annee3.montant) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
        ]);
    }
    //
    result.push([
        {text: 'Remboursement: subventions des institutions', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: PdfModel.contentFormat(sTotaux['reSubventionI'].annee1.montant) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(sTotaux['reSubventionI'].annee2.montant) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(sTotaux['reSubventionI'].annee3.montant) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
    ]);
    for (let index in institutionSub) {
        result.push([
            {text: institutionSub[index].fournisseur, style:['bolded','subheader','lefted'], colSpan:3, border:[false,false,false,false]},{},{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]

            },{},
            {
                text: PdfModel.contentFormat(calculRemboursement(institutionSub[index]).annee1.montant) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: PdfModel.contentFormat(calculRemboursement(institutionSub[index]).annee2.montant) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: PdfModel.contentFormat(calculRemboursement(institutionSub[index]).annee3.montant) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
        ]);
    }
    //
    result.push([
        {text: 'Remboursement: subventions autres', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(sTotaux['reSubventionA'].annee1.montant) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(sTotaux['reSubventionA'].annee2.montant) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(sTotaux['reSubventionA'].annee3.montant) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
    ]);
    for (let index in autresSub) {
        result.push([
            {text: autresSub[index].fournisseur, style:['bolded','subheader','lefted'], colSpan:3, border:[false,false,false,false]},{},{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]

            },{},
            {
                text: PdfModel.contentFormat(calculRemboursement(autresSub[index]).annee1.montant) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: PdfModel.contentFormat(calculRemboursement(autresSub[index]).annee2.montant) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: PdfModel.contentFormat(calculRemboursement(autresSub[index]).annee3.montant) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
        ]);
    }
    return result;
}
export function buildArrayBfr(bfr, total, sTotaux) {
    let bfrHeader = [
        {text: 'Besoins en Fonds de Roulement', style:['bolded','subheader','lefted', 'backgroundPink'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: PdfModel.contentFormat(total.initial) + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: PdfModel.contentFormat(total.annee1) + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(total.annee2) + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(total.annee3) + money, style:['bolded','subheader','centered', 'backgroundPink'], colSpan:2, border:[false,false,false,false]
        },{},
    ];
    let result = new Array(bfrHeader);
    let bfrOuvreture = bfr['bfrOuverture'];
    let creanceClient = bfr['creanceClient'];
    //BFR d'ouverture
    result.push([
        {text: 'BFR d\'ouverture', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: PdfModel.contentFormat(sTotaux['ouverture']) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
    ]);
    //Liste BFR Ouverture
    for (let index in bfrOuvreture) {
        result.push([
            {text: bfrOuvreture[index].titre, style:['bolded','subheader','lefted'], colSpan:3, border:[false,false,false,false]},{},{},
            {
                text: PdfModel.contentFormat((bfrOuvreture[index].coutMensuel * bfrOuvreture[index].duree * bfrOuvreture[index].pourcentage)  /100 )+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]

            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
        ]);
    }
    //BFR d'exploitation
    result.push([
        {text: 'BFR d\'exploitation', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: PdfModel.contentFormat(sTotaux['exploitation'].annee1) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(sTotaux['exploitation'].annee2) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(sTotaux['exploitation'].annee3) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
    ]);
    //Liste Bfr Exploitation
    result.push([
        {text: 'Stock', style:['bolded','subheader','lefted'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: PdfModel.contentFormat(sTotaux['stock'].annee1) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(sTotaux['stock'].annee2) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(sTotaux['stock'].annee3) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
        },{},
    ]);
    result.push([
        {text: 'Créances clients', style:['bolded','subheader','lefted'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: PdfModel.contentFormat(sTotaux['creance'].annee1) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(sTotaux['creance'].annee2) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(sTotaux['creance'].annee3) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
        },{},
    ]);
    result.push([
        {text: 'Dettes aux fournisseurs', style:['bolded','subheader','lefted'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: PdfModel.contentFormat(sTotaux['fournisseur'].annee1) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(sTotaux['fournisseur'].annee2) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(sTotaux['fournisseur'].annee3) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
        },{},
    ]);
    result.push([
        {text: 'Variation du BFR', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: PdfModel.contentFormat(sTotaux['variationBfr'].annee1) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(sTotaux['variationBfr'].annee2) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(sTotaux['variationBfr'].annee3) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
    ]);
    return result;
}
export function buildArrayAllTva(totaux) {
    let besoinsHeader = [
        {text: 'TVA', style:['whiteColor', 'bolded','subheader','lefted', 'backgroundRed'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: PdfModel.contentFormat(totaux.besoins.totalTva.initial) + money, style:['whiteColor', 'bolded','subheader','centered', 'backgroundRed'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: PdfModel.contentFormat(totaux.besoins.totalTva.annee1) + money, style:['whiteColor', 'bolded','subheader','centered', 'backgroundRed'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(totaux.besoins.totalTva.annee2) + money, style:['whiteColor', 'bolded','subheader','centered', 'backgroundRed'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(totaux.besoins.totalTva.annee3) + money, style:['whiteColor', 'bolded','subheader','centered', 'backgroundRed'], colSpan:2, border:[false,false,false,false]
        },{},
    ];
    let result = new Array(besoinsHeader);
    return result;
}
export function buildArrayRessources (totaux) {
    let ressourcesHeader = [
        {text: 'Ressources', style:['whiteColor', 'bolded','subheader','lefted', 'backgroundBlueRecap'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: PdfModel.contentFormat(totaux.ressources.initial) + money, style:['whiteColor', 'bolded','subheader','centered', 'backgroundBlueRecap'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: PdfModel.contentFormat(totaux.ressources.annee1) + money, style:['whiteColor', 'bolded','subheader','centered', 'backgroundBlueRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(totaux.ressources.annee2) + money, style:['whiteColor', 'bolded','subheader','centered', 'backgroundBlueRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(parseInt(totaux.ressources.annee3)) + money, style:['whiteColor', 'bolded','subheader','centered', 'backgroundBlueRecap'], colSpan:2, border:[false,false,false,false]
        },{},
    ];
    let result = new Array(ressourcesHeader);
    return result;
}
export function buildArrayCapital (capital, total, sTotaux) {
    let capitalHeader = [
        {text: 'Capital', style:['bolded','subheader','lefted', 'backgroundBlueLight'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: PdfModel.contentFormat(total['capital']) + money, style:['bolded','subheader','centered', 'backgroundBlueLight'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundBlueLight'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundBlueLight'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundBlueLight'], colSpan:2, border:[false,false,false,false]
        },{},
    ];
    let numeraire = capital['numeraire'];
    let nature = capital['nature'];
    let result = new Array(capitalHeader);
    //Apports numéraires
    result.push([
        {text: 'Apports numéraires', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: PdfModel.contentFormat(sTotaux['numeraire']) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
    ]);
    for (let index in numeraire) {
        result.push([
            {text: numeraire[index].identite, style:['bolded','subheader','lefted'], colSpan:3, border:[false,false,false,false]},{},{},
            {
                text: PdfModel.contentFormat(numeraire[index].montant * numeraire[index].quantite)+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
        ]);
    }
    //Apports en nature
    result.push([
        {text: 'Apports en nature', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: PdfModel.contentFormat(sTotaux['nature']) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
    ]);
    for (let index in nature) {
        result.push([
            {text: nature[index].identite, style:['bolded','subheader','lefted'], colSpan:3, border:[false,false,false,false]},{},{},
            {
                text: PdfModel.contentFormat(nature[index].montant * nature[index].quantite)+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]

            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
        ]);
    }
    return result;
}
export function buildArrayCompteAssocies (comptes, total) {
    let compteAssocies = [
        {text: 'Comptes courants d\'associés', style:['bolded','subheader','lefted', 'backgroundBlueLight'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: PdfModel.contentFormat(total['associes']) + money, style:['bolded','subheader','centered', 'backgroundBlueLight'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundBlueLight'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundBlueLight'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundBlueLight'], colSpan:2, border:[false,false,false,false]
        },{},
    ];
    let result = new Array(compteAssocies);
    result.push([
        {text: 'Apports numéraires', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: PdfModel.contentFormat(total['associes']) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
    ]);
    for (let index in comptes) {
        result.push([
            {text: comptes[index].identite, style:['bolded','subheader','lefted'], colSpan:3, border:[false,false,false,false]},{},{},
            {
                text: PdfModel.contentFormat(comptes[index].montant * comptes[index].quantite)+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]

            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
        ]);
    }
    return result;
}
export function buildArrayEmpruntBancaire (emprunts, total) {
    let empruntHeader = [
        {text: 'Emprunts', style:['bolded','subheader','lefted', 'backgroundBlueLight'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: PdfModel.contentFormat(total) + money, style:['bolded','subheader','centered', 'backgroundBlueLight'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundBlueLight'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundBlueLight'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundBlueLight'], colSpan:2, border:[false,false,false,false]
        },{},
    ];
    let result = new Array(empruntHeader);
    let banque = emprunts['banque'];
    let microfinance = emprunts['microfinance'];
    result.push([
        {text: 'Emprunts aux banques', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
    ]);
    for (let index in banque) {
        result.push([
            {text: banque[index].creancier, style:['bolded','subheader','lefted'], colSpan:3, border:[false,false,false,false]},{},{},
            {
                text: PdfModel.contentFormat(banque[index].montantPret) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]

            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
        ]);
    }
    result.push([
        {text: ' Emprunts aux microfinances', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text:   '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]},{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
    ]);
    for (let index in microfinance) {
        result.push([
            {text: microfinance[index].creancier, style:['bolded','subheader','lefted'], colSpan:3, border:[false,false,false,false]},{},{},
            {
                text: PdfModel.contentFormat(microfinance[index].montantPret)+ money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]

            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
        ]);
    }
    return result;
}
export function buildArraySubvention (subventions, total, sTotaux) {
    let subventionHeader = [
        {text: 'Subventions', style:['bolded','subheader','lefted', 'backgroundBlueLight'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: PdfModel.contentFormat(total['subvention'].montant) + money, style:['bolded','subheader','centered', 'backgroundBlueLight'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundBlueLight'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundBlueLight'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundBlueLight'], colSpan:2, border:[false,false,false,false]
        },{},
    ];
    let result = new Array(subventionHeader);
    let banques = subventions['banque'];
    let institutions = subventions['institution'];
    let autres = subventions['autres'];
    result.push([
        {text: 'Subventions des banques', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: PdfModel.contentFormat(sTotaux['subvention']['banque'].montant) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
    ]);
    for (let index in banques) {
        result.push([
            {text: banques[index].fournisseur, style:['bolded','subheader','lefted'], colSpan:3, border:[false,false,false,false]},{},{},
            {
                text: PdfModel.contentFormat(banques[index].montantPret) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
        ]);
    }
    result.push([
        {text: 'Subventions des institutions', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: PdfModel.contentFormat(sTotaux['subvention']['institution'].montant) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
    ]);
    for (let index in institutions) {
        result.push([
            {text: institutions[index].fournisseur, style:['bolded','subheader','lefted'], colSpan:3, border:[false,false,false,false]},{},{},
            {
                text: PdfModel.contentFormat(institutions[index].montantPret) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
        ]);
    }
    result.push([
        {text: 'Subventions autres', style:['bolded','subheader','lefted', 'backgroundGreyRecap'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: PdfModel.contentFormat(sTotaux['subvention']['autre'].montant) + money, style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: '-', style:['bolded','subheader','centered', 'backgroundGreyRecap'], colSpan:2, border:[false,false,false,false]
        },{},
    ]);
    for (let index in autres) {
        result.push([
            {text: autres[index].fournisseur, style:['bolded','subheader','lefted'], colSpan:3, border:[false,false,false,false]},{},{},
            {
                text: PdfModel.contentFormat(autres[index].montantPret) + money, style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
            {
                text: '-', style:['bolded','subheader','centered'], colSpan:2, border:[false,false,false,false]
            },{},
        ]);
    }
    return result;
}
export function buildArraySoldeExercice (solde) {
    let color = (solde.initial < 0) ? 'backgroundRed' : 'backgroundGreen';
    let color1 = (solde.annee1 < 0) ? 'backgroundRed' : 'backgroundGreen';
    let color2 = (solde.annee2 < 0) ? 'backgroundRed' : 'backgroundGreen';
    let color3 = (solde.annee3 < 0) ? 'backgroundRed' : 'backgroundGreen';
    let soldeHeader = [
        {text: 'SOLDE D\'EXERCICE', style:['bolded','subheader','lefted', 'backgroundGrey', 'whiteColor'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: PdfModel.contentFormat(parseInt(solde.initial)) + money, style:['bolded','subheader','centered', color, 'whiteColor'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: PdfModel.contentFormat(parseInt(solde.annee1)) + money, style:['bolded','subheader','centered', color1, 'whiteColor'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(parseInt(solde.annee2)) + money, style:['bolded','subheader','centered', color2, 'whiteColor'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(parseInt(solde.annee3)) + money, style:['bolded','subheader','centered', color3, 'whiteColor'], colSpan:2, border:[false,false,false,false]
        },{},
    ];
    let result = new Array(soldeHeader);
    return result;
}
export function buildArraySoldeCumule (solde) {
    let color = (solde.initial < 0) ? 'backgroundRed' : 'backgroundGreen';
    let color1 = (solde.annee1 < 0) ? 'backgroundRed' : 'backgroundGreen';
    let color2 = (solde.annee2 < 0) ? 'backgroundRed' : 'backgroundGreen';
    let color3 = (solde.annee3 < 0) ? 'backgroundRed' : 'backgroundGreen';
    let soldeHeader = [
        {text: 'SOLDE CUMULE', style:['bolded','subheader','lefted', 'backgroundGrey', 'whiteColor'], colSpan:3, border:[false,false,false,false]},{},{},
        {
            text: PdfModel.contentFormat(parseInt(solde.initial)) + money, style:['bolded','subheader','centered', color, 'whiteColor'], colSpan:2, border:[false,false,false,false]

        },{},
        {
            text: PdfModel.contentFormat(parseInt(solde.annee1)) + money, style:['bolded','subheader','centered', color1, 'whiteColor'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(parseInt(solde.annee2)) + money, style:['bolded','subheader','centered', color2, 'whiteColor'], colSpan:2, border:[false,false,false,false]
        },{},
        {
            text: PdfModel.contentFormat(parseInt(solde.annee3)) + money, style:['bolded','subheader','centered', color3, 'whiteColor'], colSpan:2, border:[false,false,false,false]
        },{},
    ];
    let result = new Array(soldeHeader);
    return result;
}
export function addContent(totaux, sTotaux, projet, years, besoins, ressources,money) {
    money = money
    let result= [
        { text: 'Plan de financement prévisionnel sur 3 ans',PageOrientation: 'landscape', pageBreak: 'before',
                    style:['bolded', 'header'], alignment: 'center', margin: [30,30,0,22]},

        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayYears(years),
                layout:'noBorders'
            },margin: [30,0,0,22]
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayAllBesoins(totaux),
                layout:'noBorders'
            },margin: [30,0,0,22]
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body:buildArrayImmoI(besoins.immoI, totaux.immoI, sTotaux),
                layout:'noBorders'
            },margin: [30,0,0,22]
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayImmoC(besoins.immoC, totaux.immoC, sTotaux),
                layout:'noBorders'
            },margin: [30,0,0,40]
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayFinance(besoins.finance, totaux.finance, sTotaux),
                layout:'noBorders'
            },margin: [30,0,0,22]
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayRE(ressources, totaux.re, sTotaux),
                layout:'noBorders'
            },margin: [30,0,0,22]
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayBfr(besoins.bfr, totaux.bfr, sTotaux),
                layout:'noBorders'
            },margin: [30,0,0,22]
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayAllTva(totaux),
                layout:'noBorders'
            },margin: [30,0,0,22]
        },
        {text:'', pageBreak: 'before'},
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayRessources(totaux),
                layout:'noBorders'
            },margin: [30,0,0,22]
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayCapital(ressources['capital'], totaux, sTotaux),
                layout:'noBorders'
            },margin: [30,0,0,22]
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayCompteAssocies(ressources['comptes'], totaux),
                layout:'noBorders'
            },margin: [30,0,0,22]
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayEmpruntBancaire(ressources['emprunts'], totaux.emprunt),
                layout:'noBorders'
            },margin: [30,0,0,22]
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArraySubvention(ressources['subventions'], totaux, sTotaux),
                layout:'noBorders'
            },margin: [30,0,0,22]
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArraySoldeExercice(totaux['soldeE']),
                layout:'noBorders'
            },margin: [30,0,0,22]
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArraySoldeCumule(totaux['soldeC']),
                layout:'noBorders'
            },margin: [30,0,0,22]
        },

    ]
    return result
}
