import {buildArrayYears, buildArraySubvention, buildArraySoldeExercice, buildArraySoldeCumule, buildArrayRessources, buildArrayRE, buildArrayImmoI, buildArrayImmoC, buildArrayFinance, buildArrayEmpruntBancaire, buildArrayCompteAssocies, buildArrayCapital, buildArrayBfr, buildArrayAllTva, buildArrayAllBesoins} from './contentPlanFinancement'
const arrayColumns = ['9%.11', '9.11%', '9%.11', '9%', '9%', '9%', '9%', '9.11%', '9.11%', '9.11%', '9.11%']
var money = '';
var precision = '';

export function content (totaux, sTotaux, projet, years, besoins, ressources,paramsMoney, meta) {
    money = paramsMoney['currency']
    precision = paramsMoney['precision']
    return [
        // en-tete du doc
        {
            columns: [
             
                ...(meta.userLogo!=''?   [{
                    width: '30%',
                    stack: [
                        {
                            image:meta.userLogo,
                            alignment: 'right',
                            height: 100,
                            width: 100
                        }
                    ],
                    margin:[5,-22,0,0]
                }] : [] ),
                ...(meta.logoEntreprise && meta.logoEntreprise!=''?   [{
                    width: '30%',
                    stack: [
                        {
                            image:meta.logoEntreprise,
                            alignment: 'right',
                            height: 100,
                            width: 100
                        }
                    ],
                    margin:[0,0,0,0]
                }] : [] ),
            ],
            columnGap: 4,
        },
        { text: 'Plan de financement prévisionnel sur 3 ans',
                    style:['bolded', 'bigHeader'], alignment: 'center', margin: [30,30,0,22]},
        {text:'Projet : '+ projet.projettitre, style: 'header',margin: [30,0,0,15]},
        { text:'', margin: [0,0,0,25]},
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayYears(years),
                layout:'noBorders'
            },margin: [30,0,0,22]
        },
        // {
        //     table: {
        //         headerRows: 0,
        //         widths: arrayColumns,
        //         body: buildArrayAllBesoins(totaux),
        //         layout:'noBorders'
        //     },margin: [30,0,0,22]
        // },
        // {
        //     table: {
        //         headerRows: 0,
        //         widths: arrayColumns,
        //         body: buildArrayImmoI(besoins.immoI, totaux.immoI, sTotaux),
        //         layout:'noBorders'
        //     },margin: [30,0,0,22]
        // },
        // {
        //     table: {
        //         headerRows: 0,
        //         widths: arrayColumns,
        //         body: buildArrayImmoC(besoins.immoC, totaux.immoC, sTotaux),
        //         layout:'noBorders'
        //     },margin: [30,0,0,40]
        // },
        // {
        //     table: {
        //         headerRows: 0,
        //         widths: arrayColumns,
        //         body: buildArrayFinance(besoins.finance, totaux.finance, sTotaux),
        //         layout:'noBorders'
        //     },margin: [30,0,0,22]
        // },
        // {
        //     table: {
        //         headerRows: 0,
        //         widths: arrayColumns,
        //         body: buildArrayRE(ressources, totaux.re, sTotaux),
        //         layout:'noBorders'
        //     },margin: [30,0,0,22]
        // },
        // {
        //     table: {
        //         headerRows: 0,
        //         widths: arrayColumns,
        //         body: buildArrayBfr(besoins.bfr, totaux.bfr, sTotaux),
        //         layout:'noBorders'
        //     },margin: [30,0,0,22]
        // },
        // {
        //     table: {
        //         headerRows: 0,
        //         widths: arrayColumns,
        //         body: buildArrayAllTva(totaux),
        //         layout:'noBorders'
        //     },margin: [30,0,0,22]
        // },
        // {text:'', pageBreak: 'before'},
        // {
        //     table: {
        //         headerRows: 0,
        //         widths: arrayColumns,
        //         body: buildArrayRessources(totaux),
        //         layout:'noBorders'
        //     },margin: [30,0,0,22]
        // },
        // {
        //     table: {
        //         headerRows: 0,
        //         widths: arrayColumns,
        //         body: buildArrayCapital(ressources['capital'], totaux, sTotaux),
        //         layout:'noBorders'
        //     },margin: [30,0,0,22]
        // },
        // {
        //     table: {
        //         headerRows: 0,
        //         widths: arrayColumns,
        //         body: buildArrayCompteAssocies(ressources['comptes'], totaux),
        //         layout:'noBorders'
        //     },margin: [30,0,0,22]
        // },
        // {
        //     table: {
        //         headerRows: 0,
        //         widths: arrayColumns,
        //         body: buildArrayEmpruntBancaire(ressources['emprunts'], totaux.emprunt),
        //         layout:'noBorders'
        //     },margin: [30,0,0,22]
        // },
        // {
        //     table: {
        //         headerRows: 0,
        //         widths: arrayColumns,
        //         body: buildArraySubvention(ressources['subventions'], totaux, sTotaux),
        //         layout:'noBorders'
        //     },margin: [30,0,0,22]
        // },
        // {
        //     table: {
        //         headerRows: 0,
        //         widths: arrayColumns,
        //         body: buildArraySoldeExercice(totaux['soldeE']),
        //         layout:'noBorders'
        //     },margin: [30,0,0,22]
        // },
        // {
        //     table: {
        //         headerRows: 0,
        //         widths: arrayColumns,
        //         body: buildArraySoldeCumule(totaux['soldeC']),
        //         layout:'noBorders'
        //     },margin: [30,0,0,22]
        // },
    ]
}