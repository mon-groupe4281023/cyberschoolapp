import PdfModel from '../PdfModel'
import * as contentPf from '../financier/planFinancement/content'
import * as contentCr from '../financier/compteResultat/content'
import * as contentS from './content/contentSummary'
import * as contentBr from '../financier/bilanPrevisionnel/content'
import doc from '../docDefinitionModel'




 let money= 0;
 let precision = 0;


class allPdf extends PdfModel {

    /**
     *
     * @param base
     * @param base.title {String} Titre du document
     * @param base.data {Object} Ensemble de données
     * @param options {Object} - Autres informations sur le Document
    */
    constructor (base = {}, options = {}) {
        super(base, options)
    }

    docDefinition () {
        const docDefinition = {
            pageOrientation: 'portrait',
            footer: (currentPage) => { return doc.footerFinancier(currentPage, this.options['meta'].infoJuridiques)},
            background: () => { return doc.backgroundBp(this.options['meta'].userLogo)},
            userPassword: this.options['password'],
            ownerPassword: 'cbscodeteam',
            permissions: {
                printing: 'highResolution', //'lowResolution'
                modifying: false,
                copying: false,
                annotating: true,
                fillingForms: true,
                contentAccessibility: true,
                documentAssembly: true
            },
            pageMargins: [ 20, 30, 60, 30 ]
        }
        return super.docDefinition(docDefinition);
    }

    defaultStyle () {
        return {
            fontSize: 10,
            bold: false,
            alignment: 'justify',
            font: this.options['meta'].police
        }
    }

    styles () {
        return Object.assign({}, super.styles(), {
            centered:{
                alignment: 'center'
            },
            righted:{
                alignment: 'right'
            },
            lefted: {
                alignment: 'left'
            },
            header: {
                fontSize: 15,
                bold: true,
                alignment: 'left'
            },
            bigHeader: {
                fontSize: 18,
                bold: true,
                decoration: 'underline',
                alignment: 'justify'
            },
            subheader: {
                fontSize: 12,
                bold: true,
                alignment: 'justify'
            },
            quote: {
                italics: true,
                alignment: 'justify'
            },
            small: {
                fontSize: 6.5,
                alignment: 'justify'
            },
            bigger: {
                fontSize: 13,
                italics: true,
                alignment: 'justify'
            },
            tableHeader: {
                alignment: 'left',
                bold:true
            },
            bolded: {
                bold: true
            },
            italiced: {
                italics: true,
            },
            whiteColor: {
                color: 'white'
            },
            blackColor: {
                color: 'black'
            },
            redColor: {
                color: '#fb3a3a'
            },
            blueColor: {
                color:'#398bf7'
            },
            backgroundBlueRecap: {
                fillColor: '#1e58b3'
            },
            backgroundBlueLight: {
                fillColor: '#d7e1ec'
            },
            backgroundGreen: {
                fillColor: '#00ba8b'
            },
            backgroundGrey:{
                fillColor:'#99abb4'
            },
            backgroundRed: {
                fillColor: '#f13c42'
            },
            backgroundPink: {
                fillColor: '#f0d4d6'
            },
            backgroundGreyRecap: {
                fillColor: '#f3f1f1'
            },
            backgroundDarkBlueRecap: {
                fillColor: '#398bf7'
            },
            backgroundLightBlueRecap: {
                fillColor: '#2bbbff'
            },
            backgroundLibelleRecap: {
                // fillColor: '#f3f1f1',
                fillColor: '#ded9d9'
            },
            backgroundTableauStandard: {
                fillColor: '#398bf7'
            }
        })
    }



content () {
    return content(
            this.data.totaux, this.data.sTotaux, this.data.projet, this.data.years,
            this.data.besoins, this.data.ressources,this.options.paramsMoney, 
            this.options.meta,this.options.password,
            this.data.summaryDoc, this.data.totauxCr, this.data.elementsCr,
            this.data.resultatNet, this.data.dettesFinancieres, this.data.totauxBilan, this.data.besoinsBilan, this.data.ressourcesBilan, this.options.downloadOptions
        );
        
           
    }
}
export function content (totaux, sTotaux, projet, years, besoins, ressources,paramsMoney, meta,password, summaryDoc, totauxCr,elementsCr,resultatNet, dettesFinancieres, totauxBilan, besoinsBilan, ressourcesBilan, downloadOptions ) {
    money = paramsMoney['currency']
    precision = paramsMoney['precision']

 console.log("Options",downloadOptions);
   let summary = downloadOptions.find((item)=>item.value=='summary');
   let planFinancement = downloadOptions.find((item)=>item.value=='plan-de-financement');
   let compteResultat = downloadOptions.find((item)=>item.value=='compte-de-resultat');
   let Bilan = downloadOptions.find((item)=>item.value=='bilan');
   console.log("Summary",summary);

    return [
        // en-tete du doc
        {
            columns: [
             
                ...(meta.userLogo!=''?   [{
                    width: '30%',
                    stack: [
                        {
                            image:meta.userLogo,
                            alignment: 'right',
                            height: 100,
                            width: 100
                        }
                    ],
                    margin:[5,-22,0,0]
                }] : [] ),
                ...(meta.logoEntreprise && meta.logoEntreprise!=''?   [{
                    width: '30%',
                    stack: [
                        {
                            image:meta.logoEntreprise,
                            alignment: 'right',
                            height: 100,
                            width: 100
                        }
                    ],
                    margin:[0,-22,0,0]
                }] : [] ),
            ],
            columnGap: 4,
        },
        {text:'BUSINESS PLAN', pageOrientation: 'portrait',  style:'bigHeader', margin: [200,2,0,10]},
        ...(summary.visible?   [
            contentS.content(summaryDoc,meta,password,paramsMoney) ] : [] ),
        ...(planFinancement.visible?   [
             contentPf.content(totaux, sTotaux, projet, years, besoins, ressources,paramsMoney,meta,'all') ] : [] ), 
        ...(compteResultat.visible?   [
            contentCr.content(projet,years,totauxCr,elementsCr,paramsMoney,meta,'all') ] : [] ),
         ...(Bilan.visible?   [
            contentBr.content(meta, projet.projetTitle, resultatNet, dettesFinancieres, besoinsBilan, ressourcesBilan, totauxBilan,'all') ] : [] ),        
       
   
    ]
}
export default allPdf
