export let fonts = {
    Helvetica: {
      normal: "HelveticaNeueMed.ttf",
      bold: "HelveticaNeueBd.ttf",
      semi_bold: "HelveticaNeueMed.ttf",
      italics: "HelveticaNeueIt.ttf",
      bolditalics: "HelveticaNeueBd.ttf",
      light:"HelveticaNeueLt.ttf"
    },
    Roboto: {
        normal: 'Roboto-Regular.ttf',
        bold: 'Roboto-Medium.ttf',
        italics: 'Roboto-Italic.ttf',
        bolditalics: 'Roboto-MediumItalic.ttf'
      },
    Poppins: {
        normal: "Poppins-Regular.ttf",
        bold: "Poppins-Bold.ttf",
        bold1: "Poppins-SemiBold.ttf",
        italics: "Poppins-Italic.ttf",
        bolditalics: "Poppins-MediumItalic.ttf"
    },
      OpenSans: {
        normal: "OpenSans-Regular.ttf",
        bold: "OpenSans-Bold.ttf",
        bold1: "OpenSans-SemiBold.ttf",
        italics: "OpenSans-Italic.ttf",
        bolditalics: "OpenSans-MediumItalic.ttf"
      }
                  
  };
