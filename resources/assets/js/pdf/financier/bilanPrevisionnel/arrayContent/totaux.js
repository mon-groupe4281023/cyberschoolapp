import { table } from "../../../toolBuilder"
import PdfModel from '../../../PdfModel'
import {currentCurrencyMask} from '../../../../views/bp/mixins/financier/besoins'
var money = currentCurrencyMask['currency'];
var precision = currentCurrencyMask[":precision"];

export default function buildTotaux (totaux) {

    let body = []
    const widths = ['*', '*', '*', '*', '*']
    body = [
        [
            {text: 'TOTAL ACTIF', style: ['backgroundDarkBlueRecap', 'whiteColor', 'bolded'], colSpan: 2 }, {}, {text: PdfModel.contentFormat(totaux.totauxActif.totalActifGeneral.net)+' ' + money, style:['bolded','subheader','righted'], colSpan: 3}, {}, {}
        ],
        [
            {text: 'TOTAL PASSIF', style: ['backgroundRed', 'whiteColor', 'bolded'], colSpan: 2 }, {}, {text: PdfModel.contentFormat(totaux.totauxPassif.totalPassif)+' ' + money, style:['bolded','subheader','righted'], colSpan: 3}, {}, {}
        ],
    ]
    
    return table(body, widths)
}