import { table } from "../../../toolBuilder"
import PdfModel from '../../../PdfModel'
import {currentCurrencyMask} from '../../../../views/bp/mixins/financier/besoins'
var money = currentCurrencyMask['currency'];
var precision = currentCurrencyMask[":precision"];

export function buildArrayCapital(capitaux) {
    let result = []
   Object.entries(capitaux).forEach(sectionCapital => {
        sectionCapital.forEach(capitaux => {
        if( (typeof capitaux) =='object') {
            capitaux.forEach(capital => {
                result.push( [
                    {text: capital.apporteur, colSpan: 2, border: [true, false, false, false]},  {}, {text: PdfModel.contentFormat(capital.montant * capital.quantite) +' ' + money, style:['righted'], colSpan: 3}, {}, {}
                ],)
            
                });
            }
        });
        
    });
    return result

}
export function buildArrayDettesFinancieres(dettesFinancieres) {
    let result = []
    dettesFinancieres.forEach(emprunt => {
        result.push( [
            {text:  emprunt.creancier == null
                ? emprunt.fournisseur
                : emprunt.creancier, colSpan: 2, border: [true, false, false, false]},  {}, {text: PdfModel.contentFormat(emprunt.remboursement.annee1.montant) +' ' + money, style:['righted'], colSpan: 3}, {}, {}
        ],)
        
        });

    return result

}
export function buildArraySubVention(subventions) {
    let result = []
   Object.entries(subventions).forEach(sectionSubvention => {
 
    sectionSubvention.forEach(subventions => {
       if ((typeof subventions) == 'object')  {
        subventions.forEach(subvention => {
            result.push( [
                {text: subvention.fournisseur, colSpan: 2, border: [true, false, false, false]},  {}, {text: PdfModel.contentFormat(subvention.montantPret) +' ' + money, style:['righted'], colSpan: 3}, {}, {}
            ],)
        
            });
       }
        });
        
    });
    return result

}
export function buildArraypassifCirculant(dettes) {
    let result = []
    dettes.forEach(dette => {
        result.push( [
            {text:  dette.titre, colSpan: 2, border: [true, false, false, false]},  {}, {text: PdfModel.contentFormat(dette.annee1) +' ' + money, style:['righted'], colSpan: 3}, {}, {}
        ],)
        
        });

    return result

}
export function buildPassif (resultatNet,dettesFinancieres,besoins, resources, totaux) {

    let body = []
    const widths = ['*', '*', '*', '*', '*']
    body = [
        [
            {text: 'Passif (Avant répartition)', style: ['backgroundRed', 'whiteColor'], alignment: 'center', colSpan: 2}, {},
            {text: 'N', style: ['backgroundGreyRecap'], alignment: 'center', colSpan: 3}, {}, {}
        ],
        [
            {text: 'PASSIF IMMOBILISE', style: ['redColor'], colSpan: 2, border: [true, false, false, false]}, {}, {text: '', colSpan: 3}, {}, {}
        ],
        [
            {text: 'Capital', style:['bolded', 'redColor'], colSpan: 2, border: [true, false, false, false]},  {}, {text: PdfModel.contentFormat(totaux.totalApportCapital) +' ' + money, style:['bolded','subheader','righted'], colSpan: 3}, {}, {},
          
        ],
        ...buildArrayCapital(resources.capital),
        [
            {text: 'Résultat net de l\'exercice (bénéfice + ou perte -) ', style:['bolded', 'redColor'], colSpan: 2, border: [true, false, false, false]}, {}, {text: PdfModel.contentFormat(totaux.resultatNet.annee1) +' ' + money, style:['bolded','subheader','righted'], colSpan: 3}, {}, {},
 
        ],
        [
            {text: 'Subventions et aides', style:['bolded', 'redColor'], colSpan: 2, border: [true, false, false, false]}, {},
             {text: PdfModel.contentFormat(totaux.totalSubventionAide.montant) +' ' + money, style:['bolded','subheader','righted'], colSpan: 3}, {}, {},
 
        ],
        ...buildArraySubVention(resources['subventions-aides']),
        [
            {text: 'TOTAL CAPITAUX PROPRES ', colSpan: 2,  style: ['bolded', 'backgroundGreyRecap'],border: [true, false, false, true]}, {}, 
            {text: PdfModel.contentFormat(totaux.subTotalCapitauxPropresAvecResultatNet) +' ' + money, style:['bolded','backgroundGreyRecap','subheader','righted'], colSpan: 3}, {}, {}
        ],
        [
            {text: 'DETTES FINANCIERES ET RESSOURCES ASSIMILEES', style:['bolded', 'redColor'], colSpan: 2, border: [true, false, false, false]}, {}, {text: '', colSpan: 3}, {}, {}
        ],
        ...buildArrayDettesFinancieres(dettesFinancieres),
        [
            {text: 'TOTAL DETTES FINANCIERES', colSpan: 2,  style: ['bolded', 'backgroundGreyRecap'],border: [true, false, false, true]}, {}, 
            {  text:PdfModel.contentFormat(totaux.subTotalDettesFinancieres) +' ' + money, style:['bolded','subheader','backgroundGreyRecap','righted'],colSpan: 3}, {}, {}
        ],
        [
            {text: ' ', colSpan: 2,}, {}, {text: ' ', colSpan: 3}, {}, {},
        ],
        [
            {text: 'TOTAL RESSOURCES STABLES', colSpan: 2,  style: ['bolded', 'backgroundGreyRecap']}, {}, {text: PdfModel.contentFormat(totaux.subTotalRessourcesStables) +' ' + money, style:['bolded','subheader','righted'], colSpan: 3}, {}, {}
        ],
        [
            {text: 'PASSIF CIRCULANT', style:['bolded', 'redColor'], colSpan: 2, border: [true, false, false, true]}, {}, {text: '', colSpan: 3}, {}, {},
      
        ],
        ...buildArraypassifCirculant(besoins.bfr.dettes),
        [
            {text: 'TOTAL PASSIF CIRCULANT', colSpan: 2,  style: ['bolded', 'backgroundGreyRecap'],border: [true, false, false, true]}, {}, 
            {text: PdfModel.contentFormat(totaux.subTotalPassifCirculant) +' ' + money, style:['bolded','backgroundGreyRecap','subheader','righted'], colSpan: 3}, {}, {}
        ],
        [
            {text: 'TRESORERIE-PASSIF', style:['bolded', 'redColor'], colSpan: 2, border: [true, false, false, false]}, {}, {text: '', colSpan: 3}, {}, {}
        ],
        [
            {text: 'Trésorerie passive de l\'année en cours', colSpan: 2, border: [true, false, false, false]},  {}, {text: PdfModel.contentFormat(totaux.subTotalTresoreriePassif) +' ' + money, style:['bolded','subheader','righted'], colSpan: 3}, {}, {}
        ],
        [
            {text: 'TOTAL TRESORERIE PASSIF', colSpan: 2,  style: ['bolded', 'backgroundGreyRecap']}, {}, 
            {text: PdfModel.contentFormat(totaux.subTotalTresoreriePassif) +' ' + money, style:['bolded', 'backgroundGreyRecap','subheader','righted'], colSpan: 3}, {}, {}
        ],
        [
            {text: 'TOTAL GENERAL', colSpan: 2, style: ['backgroundRed', 'whiteColor', 'bolded']}, {}, {text: PdfModel.contentFormat(totaux.totalPassif) +' ' + money, style:['bolded','subheader','righted'], colSpan: 3}, {}, {}
        ],
    ]

    return table(body, widths)
 
}