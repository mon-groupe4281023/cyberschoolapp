export default function buildHeader (meta, projetTitle, mode='only') {
    return [
        {
            columns: [
                ...(meta.userLogo!='' && mode=='only'?   [{
                    width: '30%',
                    stack: [
                        {
                            image:meta.userLogo,
                            alignment: 'left',
                            height: 100,
                            width: 100
                        }
                    ],
                    margin:[0,0,0,0]
                }] : [] ),
                ...(meta.logoEntreprise && meta.logoEntreprise!=''  && mode=='only' ?   [{
                    width: '30%',
                    stack: [
                        {
                            image:meta.logoEntreprise,
                            alignment: 'right',
                            height: 100,
                            width: 100
                        }
                    ],
                    margin:[0,0,0,0]
                }] : [] ),
            ],
        },
        {text: 'Bilan prévisionnel HT', style:['bolded', 'bigHeader'], alignment: 'center', margin: [30,30,0,22]},
        ...(mode=="only"?   [
        {text:'Projet: '+ projetTitle, style: 'header', margin: [0,0,0,15]}, { text:'', margin: [0,0,0,15]}] : [] ),
    ]
}