import { table } from "../../../toolBuilder"
import PdfModel from "../../../PdfModel"
import { prixImmoTotalHT, currentCurrencyMask } from '../../../../views/bp/mixins/financier/besoins'
var money = currentCurrencyMask['currency'];
var precision = currentCurrencyMask[":precision"];
export function buildArrayImmoInc(immoInc) {
    let result = [];
    immoInc.forEach((immo) => {
        immo.priHT = prixImmoTotalHT(immo);
        immo.montantAm = immo.amortissement > 0 ? immo.priHT / immo.amortissement : 0;
        result.push([
            { text: immo.titre, colSpan: 2, border: [true, false, false, false] }, {},
            { text: PdfModel.contentFormat(immo.priHT) + ' ' + money, alignment: 'right' },
            { text: immo.amortissement > 0 ? PdfModel.contentFormat(immo.montantAm) + ' ' + money : '-', alignment: 'right' },
            { text: PdfModel.contentFormat(immo.priHT - immo.montantAm) + ' ' + money, alignment: 'right' }
        ]
        )
    });

    return result
}
export function buildArrayImmoCor(immos) {
    let result = []
    for (let section in immos) {
        console.log("immos", immos[section]);
        immos[section].forEach((item) => {
            item.priHT = prixImmoTotalHT(item);
            item.montantAm = item.amortissement > 0 ? item.priHT / item.amortissement : 0;
            result.push([
                { text: item.titre, colSpan: 2, border: [true, false, false, false] }, {},
                { text: PdfModel.contentFormat(item.priHT) + ' ' + money, alignment: 'right' },
                { text: item.amortissement > 0 ? PdfModel.contentFormat(item.montantAm) + ' ' + money : '-', alignment: 'right' },
                { text: PdfModel.contentFormat(item.priHT - item.montantAm) + ' ' + money, alignment: 'right' }
            ]
            )
        })

    }
    return result
}
export function buildArrayImmoF(immos) {
    let result = []
    for (let section in immos) {
        immos[section].forEach((item) => {
            result.push([
                { text: item.titre, colSpan: 2, border: [true, false, false, false] }, {},
                { text: PdfModel.contentFormat(item.montant * item.quantite) + ' ' + money, alignment: 'right' },
                { text: '-', alignment: 'right' },
                { text: PdfModel.contentFormat(item.montant * item.quantite) + ' ' + money, alignment: 'right' }
            ]
            )
        })

    }
    return result
}

export function buildArrayStock(stocks) {
    let result = [];
    stocks.forEach((item) => {
        result.push([
            { text: item.titre, colSpan: 2, border: [true, false, false, false] }, {},
            { text: PdfModel.contentFormat(item.annee1) + ' ' + money, alignment: 'right' },
            { text: '-', alignment: 'right' },
            { text: PdfModel.contentFormat(item.annee1) + ' ' + money, alignment: 'right' }
        ])
    })
    return result
}
export function buildArrayCreanceC(creances) {
    let result = [];
    creances.forEach((item) => {
        result.push([
            { text: item.titre, colSpan: 2, border: [true, false, false, false] }, {},
            { text: PdfModel.contentFormat(item.annee1) + ' ' + money, alignment: 'right' },
            { text: '-', alignment: 'right' },
            { text: PdfModel.contentFormat(item.annee1) + ' ' + money, alignment: 'right' }
        ])
    })
    return result
}



export function buildActif(totaux, besoins) {

    const widths = ['*', '*', '*', '*', '*']
    const body = [
        [
            { text: ' \nActif', style: ['backgroundDarkBlueRecap', 'whiteColor'], alignment: 'center', colSpan: 2, rowSpan: 2 }, {},
            { text: 'Exercice N', style: ['backgroundGreyRecap'], alignment: 'center', colSpan: 3 }, {}, {}
        ],
        [
            {}, {},
            { text: 'Brut', style: ['backgroundGreyRecap'], alignment: 'center' },
            { text: 'Amort Prov', style: ['backgroundGreyRecap'], alignment: 'center' },
            { text: 'Net', style: ['backgroundGreyRecap'], alignment: 'center' },
        ],
        [
            { text: 'ACTIF IMMOBILISE', style: ['blueColor'], colSpan: 2, border: [true, false, false, false] }, {}, { text: '', colSpan: 3 }, {}, {}
        ],
        [
            { text: 'Immobilisations incorporelles', style: ['blueColor', 'bolded'], colSpan: 2, border: [true, false, false, false] }, {},
            { text: PdfModel.contentFormat(totaux.totalImmoIncorporelle.ht) + ' ' + money, style: 'bolded', alignment: 'right' },
            { text: PdfModel.contentFormat(totaux.totalImmoIncorporelle.echeancier) + ' ' + money, style: 'bolded', alignment: 'right' },
            { text: PdfModel.contentFormat(totaux.totalImmoIncorporelle.net) + ' ' + money, style: 'bolded', alignment: 'right' }
        ],
        ...buildArrayImmoInc(besoins.immoIncorporelle),
        [
            { text: 'Immobilisations corporelles', style: ['blueColor', 'bolded'], colSpan: 2, border: [true, false, false, false] }, {},
            { text: PdfModel.contentFormat(totaux.totalImmoCorporelle.ht) + ' ' + money, style: 'bolded', alignment: 'right' },
            { text: PdfModel.contentFormat(totaux.totalImmoCorporelle.echeancier) + ' ' + money, style: 'bolded', alignment: 'right' },
            { text: PdfModel.contentFormat(totaux.totalImmoCorporelle.net) + ' ' + money, style: 'bolded', alignment: 'right' },
        ],
        ...buildArrayImmoCor(besoins.immoCorporelle),
        [
            { text: 'Immobilisations financières', style: ['blueColor', 'bolded'], colSpan: 2, border: [true, false, false, false]}, {},
            { text: PdfModel.contentFormat(totaux.totalImmoFinanciere) + ' ' + money, style: 'bolded', alignment: 'right' },
            { text: ' -', style: 'bolded', alignment: 'right' },
            { text: PdfModel.contentFormat(totaux.totalImmoFinanciere) + ' ' + money, style: 'bolded', alignment: 'right' }
        ],
        ...buildArrayImmoF(besoins.immoFinanciere),
        [
            { text: 'TOTAL ACTIF IMMOBILISE', colSpan: 2, style: ['backgroundGreyRecap', 'bolded'], border: [true, false, false, true] }, {},
            { text: PdfModel.contentFormat(totaux.totauxActifImmobilise.brut) + ' ' + money, style: ['backgroundGreyRecap', 'bolded'], alignment: 'right' },
            { text: PdfModel.contentFormat(totaux.totauxActifImmobilise.amortissement) + ' ' + money, style: ['backgroundGreyRecap', 'bolded'], alignment: 'right' },
            { text: PdfModel.contentFormat(totaux.totauxActifImmobilise.net) + ' ' + money, style: ['backgroundGreyRecap', 'bolded'], alignment: 'right' }
        ],
        [
            { text: 'ACTIF CIRCULANT', style: ['blueColor'], colSpan: 2, border: [true, false, false, false] }, {}, { text: '', colSpan: 3 }, {}, {}
        ],
        [
            { text: 'Actif circulant HAO', style: ['blueColor', 'bolded'], colSpan: 2, border: [true, false, false, false]}, {},
            { text: '', style: 'bolded', alignment: 'center' },
            { text: '', style: 'bolded', alignment: 'center' },
            { text: '', style: 'bolded', alignment: 'center' }
        ],
        [
            { text: 'Stock', style: ['blueColor', 'bolded'], colSpan: 2, border: [true, false, false, false]}, {},
            { text: PdfModel.contentFormat(totaux.totalStock) + ' ' + money, style: 'bolded', alignment: 'right' },
            { text: '-', style: 'bolded', alignment: 'right' },
            { text: PdfModel.contentFormat(totaux.totalStock) + ' ' + money, style: 'bolded', alignment: 'right' }
        ],
        ...buildArrayStock(besoins.bfr.stock),
        [
            { text: 'Créances et emplois assimilés', style: ['blueColor', 'bolded'], colSpan: 2, border: [true, false, false, false]}, {},
            { text: PdfModel.contentFormat(totaux.totalCreance) + ' ' + money, style: 'bolded', alignment: 'right' },
            { text: '-', style: 'bolded', alignment: 'right' },
            { text: PdfModel.contentFormat(totaux.totalCreance) + ' ' + money, style: 'bolded', alignment: 'right' }
        ],
        ...buildArrayCreanceC(besoins.bfr.creanceClient),
        [
            { text: 'TOTAL ACTIF CIRCULANT', colSpan: 2, style: ['backgroundGreyRecap', 'bolded'], border: [true, false, false, true] }, {},
            { text: PdfModel.contentFormat(totaux.totalActifCirculant) + ' ' + money, style: ['backgroundGreyRecap', 'bolded'], alignment: 'right' },
            { text: '-', style: ['backgroundGreyRecap', 'bolded'], alignment: 'right' },
            { text: PdfModel.contentFormat(totaux.totalActifCirculant) + ' ' + money, style: ['backgroundGreyRecap', 'bolded'], alignment: 'right' }
        ],
        [
            { text: 'TOTAL TRESORERIE ACTIF', colSpan: 2, style: ['backgroundGreyRecap', 'bolded'] }, {},
            { text: PdfModel.contentFormat(totaux.totalTresorieActif) + ' ' + money, style: ['backgroundGreyRecap', 'bolded'], alignment: 'right' },
            { text: '0', style: ['backgroundGreyRecap', 'bolded'], alignment: 'right' },
            { text: PdfModel.contentFormat(totaux.totalTresorieActif) + ' ' + money, style: ['backgroundGreyRecap', 'bolded'], alignment: 'right' },
        ],
        [
            { text: 'TOTAL GENERAL', colSpan: 2, style: ['backgroundDarkBlueRecap', 'whiteColor', 'bolded']}, {},
            { text: PdfModel.contentFormat(totaux.totalActifGeneral.brut) + ' ' + money, style: 'bolded', alignment: 'right' },
            { text: PdfModel.contentFormat(totaux.totalActifGeneral.amortissement) + ' ' + money, style: 'bolded', alignment: 'right' },
            { text: PdfModel.contentFormat(totaux.totalActifGeneral.net) + ' ' + money, style: 'bolded', alignment: 'right' }
        ],
    ]

    return table(body, widths)
}