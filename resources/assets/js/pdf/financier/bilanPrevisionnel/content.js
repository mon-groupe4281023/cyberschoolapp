import { buildActif } from "./arrayContent/actif"
import { buildPassif } from "./arrayContent/passif"
import buildHeader from "./arrayContent/header"
import buildTotaux from "./arrayContent/totaux"

export function content (meta, projetTitle, resultatNet, dettesFinancieres, besoins, ressources, totaux, mode) {
    return [
        ...buildHeader(meta, projetTitle, mode),               
        buildActif(totaux.totauxActif, besoins),
        { text:'', margin: [0,0,0,25]},
        buildPassif(resultatNet,dettesFinancieres,besoins, ressources, totaux.totauxPassif),
        { text:'', margin: [0,0,0,25]},
        buildTotaux(totaux)
    ]
}
