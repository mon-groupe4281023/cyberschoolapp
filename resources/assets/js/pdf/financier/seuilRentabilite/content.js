import {arround} from '../../../services/helper'
import PdfModel from '../../PdfModel'

var money = '';
var precision = '';

let getDataType =  (annee, type) => {

    if (type === 'point') {
        annee  = PdfModel.contentFormat(arround(annee, precision)) + ' MOIS';
    } else if (type === 'tauxMargeV') {
        annee  = PdfModel.contentFormat(arround(annee, precision)) + ' %';
    } else {
        annee  = PdfModel.contentFormat(arround(annee, precision)) + ' ' + money;
    }
    return annee
} 

function buildArrayYears () {

    let yearsHeader = [
        {text: 'Années d\'exercices', style:['bolded','subheader','lefted',], border:[false,false,false,false]},
        {
            text: 'Année 1', style:['bolded','subheader','righted',], border:[false,false,false,false]
        },
        {
            text: 'Année 2', style:['bolded','subheader','righted',], border:[false,false,false,false]
        },
        {
            text: 'Année 3', style:['bolded','subheader','righted',], border:[false,false,false,false]
        },
    ];
    let result = new Array(yearsHeader);
    return result;
}
function buildArrayRentabilite (rentabilites) {
    let result = [];
    for (let rentabilite in rentabilites) {

        result.push([
            {text: rentabilites[rentabilite].titre, style:['bolded','subheader','lefted', 'backgroundPurple', 'whiteColor'], border:[false,false,false,false]},
            {
                text: getDataType(rentabilites[rentabilite].annee1, rentabilite), style:['bolded','subheader','righted', 'backgroundPurple', 'whiteColor'], border:[false,false,false,false]
            },
            {
                text: (rentabilite !== 'point') ? getDataType(rentabilites[rentabilite].annee1, rentabilite) : '', style:['bolded','subheader','righted', 'backgroundPurple', 'whiteColor'], border:[false,false,false,false]
            },
            {
                text: (rentabilite !== 'point') ? getDataType(rentabilites[rentabilite].annee1, rentabilite) : '', style:['bolded','subheader','righted', 'backgroundPurple', 'whiteColor'], border:[false,false,false,false]
            },
        ]);
        if (rentabilites[rentabilite].children) {
            let childs = rentabilites[rentabilite].children;
            for (let child in childs) {
                result.push([
                    {text: childs[child].titre, style:['lefted', 'small'], border:[false,false,false,false]},
                    {
                        text: PdfModel.contentFormat(arround(childs[child].annee1, precision)) + ' ' + money, style:['righted', 'small'], border:[false,false,false,false]
                    },
                    {
                        text: PdfModel.contentFormat(arround(childs[child].annee2, precision)) + ' ' + money, style:['righted', 'small'], border:[false,false,false,false]
                    },
                    {
                        text: PdfModel.contentFormat(arround(childs[child].annee3, precision)) + ' ' + money, style:['righted', 'small'], border:[false,false,false,false]
                    },
                ]);
            }
        }
    }

    return result;
}
export function content (projet, rentabilite, params, meta, password){
    // implementation du pdf
    money = params['currency'];
    precision = params['precision'];
    return [
        // en-tete du doc
        {
            columns: [
             
                ...(meta.userLogo!=''?   [{
                    width: '30%',
                    stack: [
                        {
                            image:meta.userLogo,
                            alignment: 'right',
                            height: 100,
                            width: 100
                        }
                    ],
                    margin:[5,-22,0,0]
                }] : [] ),
                ...(meta.logoEntreprise && meta.logoEntreprise!=''?   [{
                    width: '30%',
                    stack: [
                        {
                            image:meta.logoEntreprise,
                            alignment: 'right',
                            height: 100,
                            width: 100
                        }
                    ],
                    margin:[0,0,0,0]
                }] : [] ),
            ],
            columnGap: 4,
        },
        ,
        { text: 'Seuil de rentabilité et point mort', style:['bolded', 'bigHeader'], alignment: 'center'},
        {text:'Projet : '+ projet.projettitre, style: 'header',margin: [0,15,0,15]},
        {
            table: {
                headerRows: 0,
                widths: ['*', '*', '*', '*'],
                body: buildArrayYears(),
                layout:'noBorders'
            }
        },{
            table: {
                headerRows: 0,
                widths: ['35%', '*', '*', '*'],
                body: buildArrayRentabilite(rentabilite),
                layout:'noBorders'
            }
        },
    ]
}
