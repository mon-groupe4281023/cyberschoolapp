import PdfModel from '../../PdfModel'
import {content} from './content'
import doc from '../../docDefinitionModel'


class SeuilRentabilitePdf extends PdfModel {

    /**
     *
     * @param base
     * @param base.title {String} Titre du document
     * @param base.data {Object} Ensemble de données
     * @param options {Object} - Autres informations sur le Document
    */
    constructor (base = {}, options = {}) {
        super(base, options)
    }

    docDefinition () {
        const docDefinition = {
            footer: (currentPage) => { return doc.footerFinancier(currentPage,this.options['meta'].infoJuridiques)},
            background: () => { return doc.backgroundBp(this.options['meta'].userLogo)},
            // userPassword: this.options['password'],
            ownerPassword: 'cbscodeteam',
            permissions: {
                printing: 'highResolution', //'lowResolution'
                modifying: false,
                copying: false,
                annotating: true,
                fillingForms: true,
                contentAccessibility: true,
                documentAssembly: true
            }
        }
        return super.docDefinition(docDefinition);
    }

    defaultStyle () {
        return {
            fontSize: 11.2,
            bold: false,
            alignment: 'left',
            // font: this.options['meta'].police
        }
    }

    styles () {
        return Object.assign({}, super.styles(), {
            centered:{
                alignment: 'center'
            },
            righted:{
                alignment: 'right'
            },
            header: {
                fontSize: 15,
                bold: true,
            },
            bigHeader: {
                fontSize: 18,
                bold: true,
                decoration: 'underline',
            },
            subheader: {
                fontSize: 11,
                bold: true,
            },
            quote: {
                italics: true,
            },
            small: {
                fontSize: 8,
            },
            bigger: {
                fontSize: 13,
                italics: true,
            },
            tableHeader: {
                alignment: 'left',
                bold:true
            },
            bolded: {
                bold: true
            },
            italiced: {
                italics: true,
            },
            whiteColor: {
                color: 'white'
            },
            blackColor: {
                color: 'black'
            },
            redColor: {
                color: '#fb3a3a'
            },
            blueColor: {
                color:'#398bf7'
            },
            backgroundBlueRecap: {
                fillColor: '#1e58b3'
            },
            backgroundBlueLight: {
                fillColor: '#d7e1ec'
            },
            backgroundGreen: {
                fillColor: '#00ba8b'
            },
            backgroundPurple:{
                fillColor:'#875faf'
            },
            backgroundRed: {
                fillColor: '#f13c42'
            },
            backgroundPink: {
                fillColor: '#f0d4d6'
            },
            backgroundGreyRecap: {
                fillColor: '#f3f1f1'
            },
            backgroundDarkBlueRecap: {
                fillColor: '#398bf7'
            },
            backgroundLightBlueRecap: {
                fillColor: '#2bbbff'
            },
            backgroundLibelleRecap: {
                // fillColor: '#f3f1f1',
                fillColor: '#ded9d9'
            },
            backgroundTableauStandard: {
                fillColor: '#398bf7'
            }

        })
    }

    getTitlePdf () {
        return `${this.data.projet.projettitre}_${this.title}.pdf`.toLowerCase()
    }

    content () {
        return content(this.data.projet, this.data.rentabilite, this.data.paramsMoney, this.options.meta)
    }

}

export default SeuilRentabilitePdf
