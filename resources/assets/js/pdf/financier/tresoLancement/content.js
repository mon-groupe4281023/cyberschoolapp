import PdfModel from "../../PdfModel"
import {arround, deleteSpace} from "../../../services/helper"

var money = '';
var precision = '';

function buildArrayYear () {
    let yearsHeader = [
        {text: 'Annee 1', style:['bolded','light','lefted'], border:[false,false,false,false], colSpan:2},{},
        {text: 'INITIAL', style:['bolded','light','centered'], border:[false,false,false,false]},
        {text: 'JAN', style:['bolded','light','centered'], border:[false,false,false,false]},
        {text: 'FEV', style:['bolded','light','centered'], border:[false,false,false,false]},
        {text: 'MAR', style:['bolded','light','centered'], border:[false,false,false,false]},
        {text: 'AVR', style:['bolded','light','centered'], border:[false,false,false,false]},
        {text: 'MAI', style:['bolded','light','centered'], border:[false,false,false,false]},
        {text: 'JUN', style:['bolded','light','centered'], border:[false,false,false,false]},
    ];
    let result = new Array(yearsHeader);
    return result;
}
function buildArrayYear1 () {
    let yearsHeader = [
        {text: 'Annee 1', style:['bolded','light','lefted'], border:[false,false,false,false], colSpan:2},{},
        {text: 'JUL', style:['bolded','light','centered'], border:[false,false,false,false]},
        {text: 'AOU', style:['bolded','light','centered'], border:[false,false,false,false]},
        {text: 'SEPT', style:['bolded','light','centered'], border:[false,false,false,false]},
        {text: 'OCT', style:['bolded','light','centered'], border:[false,false,false,false]},
        {text: 'NOV', style:['bolded','light','centered'], border:[false,false,false,false]},
        {text: 'DEC', style:['bolded','light','centered'], border:[false,false,false,false]},
        {text: 'TOTAL', style:['bolded','light','centered'], border:[false,false,false,false]},
    ];
    let result = new Array(yearsHeader);
    return result;
}
function buildArrayTresoSem1 (tresos) {
    let result;
    result = [];
    for (let treso of tresos) {
        let colorHeader = 'backgroundGrey';
        let color = '';
        let cc = '';
        let colorMFC = 'backgroundGrey';
        if (treso.titre === 'Décaissements' || treso.titre === 'Encaissements'){
            color = cc = 'whiteColor';
            colorMFC = (treso.titre === 'Décaissements') ? 'backgroundRed': 'backgroundGreen';
            colorHeader = (treso.titre === 'Décaissements') ? 'backgroundRed': 'backgroundGreen';
        }
        if (treso.tag === 'resultat') {
            colorMFC = '';
            cc = 'whiteColor';
        }
        result.push([
            {text: treso.titre, style:['bolded','light', colorHeader, 'lefted', color], border:[false,false,false,false], colSpan:2},{},
            {text: PdfModel.contentFormat(arround(treso.initial, precision)) + ' ' + money, style:['bolded','light', (treso.initial<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', 'righted', cc, colorMFC], border:[false,false,false,false]},
            {text: PdfModel.contentFormat(arround(treso.mois1, precision)) + ' ' + money, style:['bolded','light', (treso.mois1<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', colorMFC, 'righted', cc], border:[false,false,false,false]},
            {text: PdfModel.contentFormat(arround(treso.mois2, precision)) + ' ' + money, style:['bolded','light', (treso.mois2<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', colorMFC, 'righted', cc], border:[false,false,false,false]},
            {text: PdfModel.contentFormat(arround(treso.mois3, precision)) + ' ' + money, style:['bolded','light', (treso.mois3<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', colorMFC, 'righted', cc], border:[false,false,false,false]},
            {text: PdfModel.contentFormat(arround(treso.mois4, precision)) + ' ' + money, style:['bolded','light',(treso.mois4<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', colorMFC, 'righted', cc], border:[false,false,false,false]},
            {text: PdfModel.contentFormat(arround(treso.mois5, precision)) + ' ' + money, style:['bolded','light',(treso.mois5<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', colorMFC, 'righted', cc], border:[false,false,false,false]},
            {text: PdfModel.contentFormat(arround(treso.mois6, precision)) + ' ' + money, style:['bolded','light',(treso.mois6<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', colorMFC, 'righted', cc], border:[false,false,false,false]},
        ]);
        if (treso.children) {
            for (let child of treso.children) {
                result.push([
                    {text: deleteSpace(child.titre), style:['light','lefted'], border:[false,false,false,true], colSpan:2, borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']}, {},
                    {text: PdfModel.contentFormat(arround(child.initial, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                    {text: PdfModel.contentFormat(arround(child.mois1, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                    {text: PdfModel.contentFormat(arround(child.mois2, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                    {text: PdfModel.contentFormat(arround(child.mois3, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                    {text: PdfModel.contentFormat(arround(child.mois4, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                    {text: PdfModel.contentFormat(arround(child.mois5, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                    {text: PdfModel.contentFormat(arround(child.mois6, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                ]);
                if (child.children) {
                    for (let item of child.children) {
                        result.push([
                            {text: '- ' + deleteSpace(item.titre), margin: [10, 0, 0, 0], style:['light','lefted'], border:[false,false,false,true], colSpan:2, borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']}, {},
                            {text: PdfModel.contentFormat(arround(item.initial, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                            {text: PdfModel.contentFormat(arround(item.mois1, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                            {text: PdfModel.contentFormat(arround(item.mois2, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                            {text: PdfModel.contentFormat(arround(item.mois3, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                            {text: PdfModel.contentFormat(arround(item.mois4, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                            {text: PdfModel.contentFormat(arround(item.mois5, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                            {text: PdfModel.contentFormat(arround(item.mois6, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                        ]);
                    }
                }
            }
        }
        result.push([
            {text: '', margin: [0,0,0,4], border:[false,false,false,false], colSpan:2},{},
            {text: '', margin: [0,0,0,4], border:[false,false,false,false]},
            {text: '', margin: [0,0,0,4], border:[false,false,false,false]},
            {text: '', margin: [0,0,0,4], border:[false,false,false,false]},
            {text: '', margin: [0,0,0,4], border:[false,false,false,false]},
            {text: '', margin: [0,0,0,4], border:[false,false,false,false]},
            {text: '', margin: [0,0,0,4], border:[false,false,false,false]},
            {text: '', margin: [0,0,0,4], border:[false,false,false,false]},
        ])
    }
    return result;
}
function buildArrayTresoSem2 (tresos) {
    let result;
    result = [];
    for (let treso of tresos) {
        let colorHeader = 'backgroundGrey';
        let color = '';
        let cc = '';
        let colorMFC = 'backgroundGrey';
        if (treso.titre === 'Décaissements' || treso.titre === 'Encaissements'){
            colorHeader = (treso.titre == 'Décaissements') ? 'backgroundRed': 'backgroundGreen';
            colorMFC = (treso.titre == 'Décaissements') ? 'backgroundRed': 'backgroundGreen';
            color = cc = 'whiteColor';
        }
        if (treso.tag === 'resultat') {
            colorMFC = '';
            cc = 'whiteColor';
        }
        result.push([
            {text: deleteSpace(treso.titre), style:['bolded','light', colorHeader,'lefted', color], border:[false,false,false,false], colSpan:2},{},
            {text: PdfModel.contentFormat(arround(treso.mois7, precision)) + ' ' + money, style:['bolded','light',(treso.mois7<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', colorMFC,'righted', cc], border:[false,false,false,false]},
            {text: PdfModel.contentFormat(arround(treso.mois8, precision)) + ' ' + money, style:['bolded','light',(treso.mois8<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', colorMFC,'righted', cc], border:[false,false,false,false]},
            {text: PdfModel.contentFormat(arround(treso.mois9, precision)) + ' ' + money, style:['bolded','light', (treso.mois9<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', colorMFC,'righted', cc], border:[false,false,false,false]},
            {text: PdfModel.contentFormat(arround(treso.mois10, precision)) + ' ' + money, style:['bolded','light',(treso.mois10<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', colorMFC,'righted', cc], border:[false,false,false,false]},
            {text: PdfModel.contentFormat(arround(treso.mois11, precision)) + ' ' + money, style:['bolded','light',(treso.mois11<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', colorMFC,'righted', cc], border:[false,false,false,false]},
            {text: PdfModel.contentFormat(arround(treso.mois12, precision)) + ' ' + money, style:['bolded','light',(treso.mois12<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', colorMFC,'righted', cc], border:[false,false,false,false]},
            {text: PdfModel.contentFormat(arround(treso.total, precision)) + ' ' + money, style:['bolded','light',(treso.total<0 && (treso.tag === 'resultat')) ? 'backgroundRed': 'backgroundGreen', colorMFC,'righted', cc], border:[false,false,false,false]},
        ]);
        if (treso.children) {
            let children = treso.children;
            for (let child of children) {
                result.push([
                    {text: deleteSpace(child.titre), style:['light','lefted'], border:[false,false,false,true], colSpan:2, borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},{},
                    {text: PdfModel.contentFormat(arround(child.mois7, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                    {text: PdfModel.contentFormat(arround(child.mois8, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                    {text: PdfModel.contentFormat(arround(child.mois9, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                    {text: PdfModel.contentFormat(arround(child.mois10, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                    {text: PdfModel.contentFormat(arround(child.mois11, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                    {text: PdfModel.contentFormat(arround(child.mois12, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']},
                    {text: PdfModel.contentFormat(arround(child.total, precision)) + ' ' + money, style:['light','righted'], border:[false,false,false,true], borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969']}
                ]);
            }
        }
        result.push([
            {text: '', margin: [0,0,0,15], border:[false,false,false,false], colSpan:2},{},
            {text: '', margin: [0,0,0,15], border:[false,false,false,false]},
            {text: '', margin: [0,0,0,15], border:[false,false,false,false]},
            {text: '', margin: [0,0,0,15], border:[false,false,false,false]},
            {text: '', margin: [0,0,0,15], border:[false,false,false,false]},
            {text: '', margin: [0,0,0,15], border:[false,false,false,false]},
            {text: '', margin: [0,0,0,15], border:[false,false,false,false]},
            {text: '', margin: [0,0,0,15], border:[false,false,false,false]},
        ])
    }
    return result;
}

export function content (paramsMoney, projet, treso, meta) {
    money = paramsMoney['currency']
    precision = paramsMoney['precision']
    return [
        // en-tete du doc
        {
            columns: [
             
                ...(meta.userLogo!=''?   [{
                    width: '30%',
                    stack: [
                        {
                            image:meta.userLogo,
                            alignment: 'right',
                            height: 100,
                            width: 100
                        }
                    ],
                    margin:[5,-22,0,0]
                }] : [] ),
                ...(meta.logoEntreprise && meta.logoEntreprise!=''?   [{
                    width: '30%',
                    stack: [
                        {
                            image:meta.logoEntreprise,
                            alignment: 'right',
                            height: 100,
                            width: 100
                        }
                    ],
                    margin:[0,0,0,0]
                }] : [] ),
            ],
            columnGap: 4,
        },
        { text: 'Trésorerie de lancement',
            style:['bolded', 'bigHeader'], alignment: 'center', margin: [0,30,0,22]},
        {text:'Projet : '+ projet.projettitre, style: 'header',margin: [0,0,0,15]},
        { text:'', margin: [0,0,0,25]},
        {
            table: {
                headerRows: 0,
                widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*'],
                body: buildArrayYear(),
                layout:'noBorders'
            },margin: [2,0,0,22]
        },
        {
            table: {
                headerRows: 0,
                widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*'],
                body: buildArrayTresoSem1(treso),
                layout:'noBorders'
            },margin: [2,0,0,22]
        },{text:'', pageBreak: 'before'},
        {
            table: {
                headerRows: 0,
                widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*'],
                body: buildArrayYear1(),
                layout:'noBorders'
            },margin: [2,0,0,22]
        },
        {
            table: {
                headerRows: 0,
                widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*'],
                body: buildArrayTresoSem2(treso),
                layout:'noBorders'
            },margin: [2,0,0,22]
        }
    ]
}
