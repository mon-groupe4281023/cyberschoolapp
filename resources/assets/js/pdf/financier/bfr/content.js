import {arround} from '../../../services/helper'
import PdfModel from '../../PdfModel'

var money = ''
var precision = ''

function buildArrayYears () {
    let yearsHeader = [
        {text: 'Années d\'exercices', style:['bolded','lefted',], border:[false,false,false,false]},
        {
            text: 'Année 1', style:['bolded','righted',], border:[false,false,false,false]
        },
        {
            text: 'Année 2', style:['bolded','righted',], border:[false,false,false,false]
        },
        {
            text: 'Année 3', style:['bolded','righted',], border:[false,false,false,false]
        }
    ];
    return new Array(yearsHeader);
}
function buildArrayBfr (elements) {
    let result = [];
    elements.forEach(element => {
        result.push([
            {text: element.titre, style:['bolded','subheader','lefted', 'backgroundPink', 'whiteColor'], border:[false,false,false,false]},
            {
                text: PdfModel.contentFormat(arround(element.annee1, precision)) + ' ' + money, style:['bolded','subheader','righted', 'backgroundPink', 'whiteColor'], border:[false,false,false,false]
            },
            {
                text: (element.annee2 === '-') ? '-' : PdfModel.contentFormat(arround(element.annee2, precision)) + ' ' + money, style:['bolded','subheader', 'righted', 'backgroundPink', 'whiteColor'], border:[false,false,false,false]
            },
            {
                text: (element.annee3 === '-') ? '-' : PdfModel.contentFormat(arround(element.annee3, precision)) + ' ' + money, style:['bolded','subheader', 'righted', 'backgroundPink', 'whiteColor'], border:[false,false,false,false]
            }
        ]);
        if (element.children) {
            for (let child of element.children) {
                result.push([
                    {text: child.titre, style:['subheader', (child.group) ? '' : 'bolded','lefted', (child.group) ? '' : 'backgroundPinkLigth'], border:[false,false,false,false]},
                    {
                        text: PdfModel.contentFormat(arround(child.annee1, precision)) + ' ' + money, style:['subheader', 'bolded','righted', (child.group) ? '' : 'backgroundPinkLigth'], border:[false,false,false,false]
                    },
                    {
                        text: (child.annee2 === '-') ? '-' : PdfModel.contentFormat(arround(child.annee2, precision)) + ' ' + money, style:['subheader', 'bolded', 'righted', (child.group) ? '' : 'backgroundPinkLigth'], border:[false,false,false,false]
                    },
                    {
                        text: (child.annee3 === '-') ? '-' : PdfModel.contentFormat(arround(child.annee3, precision)) + ' ' + money, style:['subheader', 'bolded', 'righted', (child.group) ? '' : 'backgroundPinkLigth'], border:[false,false,false,false]
                    },
                ]);
                if (child.children) {
                    /**
                     * sChild petit fils de element
                     */
                    for (let sChild of child.children) {
                        result.push([
                            {text: sChild.titre, style:['lefted', 'subheader'], colSpan:4, border:[false,false,false,false]},
                            {
                                text: PdfModel.contentFormat(arround(sChild.annee1, precision)) + ' ' + money, style:['subheader', 'righted',], colSpan:2, border:[false,false,false,false]
                            },
                            {
                                text: PdfModel.contentFormat(arround(sChild.annee2, precision)) + ' ' + money, style:['subheader', 'righted',], colSpan:2, border:[false,false,false,false]
                            },
                            {
                                text: PdfModel.contentFormat(arround(sChild.annee3, precision)) + ' ' + money, style:['subheader', 'righted',], colSpan:2, border:[false,false,false,false]
                            },
                        ]);
                    }
                }
            }
        }
    });
    return result;
}
export function content (projet, paramsMoney, elements, meta, password) {
    //implementation du pdf
    money = paramsMoney['currency']
    precision = paramsMoney['precision'] 
    return [
        {
            columns: [
             
                ...(meta.userLogo!=''?   [{
                    width: '30%',
                    stack: [
                        {
                            image:meta.userLogo,
                            alignment: 'right',
                            height: 100,
                            width: 100
                        }
                    ],
                    margin:[5,-22,0,0]
                }] : [] ),
                ...(meta.logoEntreprise && meta.logoEntreprise!=''?   [{
                    width: '30%',
                    stack: [
                        {
                            image:meta.logoEntreprise,
                            alignment: 'right',
                            height: 100,
                            width: 100
                        }
                    ],
                    margin:[0,0,0,0]
                }] : [] ),
            ],
            columnGap: 4,
        },
        { text: 'Besoins en fonds de roulement (HT) sur 3 ans', style:['bolded', 'bigHeader'], alignment: 'center'},
        { text:'', },
        {text:'Projet : '+ projet.projettitre, style: 'header', margin: [0,12,0,12]},
        {
            table: {
                headerRows: 0,
                widths: ['*', '*', '*', '*'],
                body: buildArrayYears(),
                layout:'noBorders'
            }
        },
        {
            table: {
                headerRows: 0,
                widths: ['*', '*', '*', '*'],
                body: buildArrayBfr(elements),
                layout:'noBorders'
            }
        },
    ]
}
