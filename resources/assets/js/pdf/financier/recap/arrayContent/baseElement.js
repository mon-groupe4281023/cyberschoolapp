export const styleHeaderCell = ['textBlue', 'bgBlueLight']

export const styleLineCell = ['textXs']

export const styleSubTotal = ['textSm', 'bgGrayLight']

export const borders = [false, false, false, false]

export const headers = [
    {name: 'Libelle', style: ['textLeft']}, 
    {name: 'Quantité', style: ['textCenter']},
    {name: 'Prix Unitaire', style: ['textRight']},
    {name: 'Prix Total', style: ['textRight']},
]
