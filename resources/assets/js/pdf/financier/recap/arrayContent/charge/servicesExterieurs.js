import PdfModel from "../../../../PdfModel";
import { table } from "../../../../toolBuilder";
import { styleHeaderCell, borders, styleLineCell, styleSubTotal } from "../baseElement";

const headers = [
    {name: 'Libelle', style: ['textLeft']}, 
    {name: 'Qte', style: ['textCenter']},
    {name: 'Coût Unitaire', style: ['textCenter']},
    {name: 'Année 1', style: ['textRight']},
    {name: '%', style: ['textCenter']},
    {name: 'Année 2', style: ['textRight']},
    {name: '%', style: ['textCenter']},
    {name: 'Année 3', style: ['textRight']},
]

/**
 * Service Etérieurs
 * @param {Array} services 
 * @param {Object} charges
 * @param {Object} currency
 * @returns {Array}
 */
export function servicesExterieurs (services, charges, currency) {

    const body = [
        [
            ...headers.map(itemHeader => {
                return {
                    text: itemHeader.name, style: [...styleHeaderCell, ...itemHeader.style], border: borders
                }
            })
        ],
        [
            {text: 'Total Services Extérieurs', style: styleSubTotal, border: borders, colSpan: 2}, {}, 
            {text: PdfModel.formatCurrency(charges.totalServExtFrais?.annee1, currency), style: [...styleSubTotal, 'textRight'], colSpan: 2, border: borders}, {}, 
            {text: PdfModel.formatCurrency(charges.totalServExtFrais?.annee2, currency), style: [...styleSubTotal, 'textRight'], colSpan: 2, border: borders}, {}, 
            {text: PdfModel.formatCurrency(charges.totalServExtFrais?.annee3, currency), style: [...styleSubTotal, 'textRight'], colSpan: 2, border: borders}, {}
        ],
        ...services.map(service => {
            return [
                {text: service?.titre, style: styleLineCell, border: borders},
                    {text: service?.quantite, style: [...styleLineCell, 'textCenter'], border: borders},
                    {text: PdfModel.formatCurrency(service?.prix, currency), style: [...styleLineCell, 'textRight'], border: borders},
                    {text: PdfModel.formatCurrency(service?.annee1, currency), style: [...styleLineCell, 'textRight'], border: borders},
                    {text: service?.accroissement1 + ' %', style: [...styleLineCell, 'textCenter'], border: borders},
                    {text: PdfModel.formatCurrency(service?.annee2, currency), style: [...styleLineCell, 'textRight'], border: borders},
                    {text: service?.accroissement2 + ' %', style: [...styleLineCell, 'textCenter'], border: borders},
                    {text: PdfModel.formatCurrency(service?.annee3, currency), style: [...styleLineCell, 'textRight'], border: borders},
            ]
        })
    ]

    return table(body, ['*', '*', '*', '*', '5%', '*', '5%', '*'], [15])
}
