import PdfModel from "../../../../PdfModel";
import { table } from "../../../../toolBuilder";
import { styleHeaderCell, borders, styleLineCell, styleSubTotal } from "../baseElement";

const headers = [
    {name: 'Libelle', style: ['textLeft']}, 
    {name: 'Qte', style: ['textCenter']},
    {name: 'Montant', style: ['textCenter']},
    {name: 'Année 1', style: ['textRight']},
    {name: '%', style: ['textCenter']},
    {name: 'Année 2', style: ['textRight']},
    {name: '%', style: ['textCenter']},
    {name: 'Année 3', style: ['textRight']},

]

/**
 * Apports en matieres Premieres
 * @param {Array} matieresPremieres 
 * @param {Object} charges
 * @param {Object} currency
 * @returns {Array}
 */
export function achatsMatieres (matieresPremieres, charges, currency) {

    let subBody = []

    const subSectionTitles = {
        "charges-variables-produits": '',
        "depenses-produits": 'Total Répartition MP/CA'
    }

    for (const [key, values] of Object.entries(matieresPremieres)) {
        if (key === 'depenses-produits') {
            subBody.push(
                [
                    {text: subSectionTitles[`${key}`], colSpan: 2, style: styleSubTotal, border: borders}, {}, 
                    {text: PdfModel.formatCurrency(charges[`${key}`]?.annee1, currency), colSpan: 2, style: [...styleSubTotal, 'textRight'], border: borders}, {}, 
                    {text: PdfModel.formatCurrency(charges[`${key}`]?.annee2, currency), colSpan: 2, style: [...styleSubTotal, 'textRight'], border: borders}, {}, 
                    {text: PdfModel.formatCurrency(charges[`${key}`]?.annee3, currency), colSpan: 2, style: [...styleSubTotal, 'textRight'], border: borders}, {}
                ],
                ...values.map(value => {
                    return [
                        {text: value?.titre, style: styleLineCell, border: borders},
                        {text: value?.quantite, style: [...styleLineCell, 'textCenter'], border: borders},
                        {text: PdfModel.formatCurrency(value?.prix, currency), style: [...styleLineCell, 'textRight'], border: borders},
                        {text: PdfModel.formatCurrency(value?.annee1, currency), style: [...styleLineCell, 'textRight'], border: borders},
                        {text: value?.accroissement1 + ' %', style: [...styleLineCell, 'textCenter'], border: borders},
                        {text: PdfModel.formatCurrency(value?.annee2, currency), style: [...styleLineCell, 'textRight'], border: borders},
                        {text: value?.accroissement2 + ' %', style: [...styleLineCell, 'textCenter'], border: borders},
                        {text: PdfModel.formatCurrency(value?.annee3, currency), style: [...styleLineCell, 'textRight'], border: borders},
                    ]
                })
            )
        }
    }

    const body = [
        [
            ...headers.map(itemHeader => {
                return {
                    text: itemHeader.name, style: [...styleHeaderCell, ...itemHeader.style], border: borders
                }
            })
        ],
        ...subBody
    ]

    return table(body, ['*', '*', '*', '*', '5%', '*', '5%', '*'], [15])
}
