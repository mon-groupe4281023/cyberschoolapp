import PdfModel from "../../../../PdfModel";
import { table } from "../../../../toolBuilder";
import { styleHeaderCell, borders, styleLineCell, styleSubTotal } from "../baseElement";

const headers = [
    {name: 'Libelle', style: ['textLeft']}, 
    {name: 'Qte', style: ['textCenter']},
    {name: 'Coût Unitaire', style: ['textCenter']},
    {name: 'Année 1', style: ['textRight']},
    {name: '%', style: ['textCenter']},
    {name: 'Année 2', style: ['textRight']},
    {name: '%', style: ['textCenter']},
    {name: 'Année 3', style: ['textRight']},
]

/**
 * Impots taxes
 * @param {Array} impotsTaxe 
 * @param {Object} charges
 * @param {Object} currency
 * @returns {Array}
 */
export function impotsTaxe (impotsTaxe, charges, currency) {

    let subBody = []

    const subSectionTitles = {
        "impots": '3.1 Taxes aux impôts',
        "mairie": '3.2 Taxes de la Mairie'
    }

    for (const [key, values] of Object.entries(impotsTaxe)) {
        subBody.push(
            [
                {text: subSectionTitles[`${key}`], colSpan: 2, style: styleSubTotal, border: borders}, {}, 
                {text: PdfModel.formatCurrency(charges[key]?.annee1, currency), colSpan: 2, border: borders, style: [...styleSubTotal, 'textRight']}, {}, 
                {text: PdfModel.formatCurrency(charges[key]?.annee2, currency), colSpan: 2, border: borders, style: [...styleSubTotal, 'textRight']}, {}, 
                {text: PdfModel.formatCurrency(charges[key]?.annee3, currency), colSpan: 2, border: borders, style: [...styleSubTotal, 'textRight']}, {}
            ],
            ...values.map(value => {
                return [
                    {text: value?.titre, style: styleLineCell, border: borders},
                    {text: value?.quantite, style: [...styleLineCell, 'textCenter'], border: borders},
                    {text: PdfModel.formatCurrency(value?.prix, currency), style: [...styleLineCell, 'textRight'], border: borders},
                    {text: PdfModel.formatCurrency(value?.annee1, currency), style: [...styleLineCell, 'textRight'], border: borders},
                    {text: value?.accroissement1 + ' %', style: [...styleLineCell, 'textCenter'], border: borders},
                    {text: PdfModel.formatCurrency(value?.annee2, currency), style: [...styleLineCell, 'textRight'], border: borders},
                    {text: value?.accroissement2 + ' %', style: [...styleLineCell, 'textCenter'], border: borders},
                    {text: PdfModel.formatCurrency(value?.annee3, currency), style: [...styleLineCell, 'textRight'], border: borders},
                ]
            })
        )
    }

    const body = [
        [
            ...headers.map(itemHeader => {
                return {
                    text: itemHeader.name, style: [...styleHeaderCell, ...itemHeader.style], border: borders
                }
            })
        ],
        ...subBody
    ]

    return table(body, ['*', '*', '*', '*', '5%', '*', '5%', '*'], [15])
}
