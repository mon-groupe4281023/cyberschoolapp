import PdfModel from "../../../../PdfModel";
import { table } from "../../../../toolBuilder";
import { styleHeaderCell, borders, styleLineCell, styleSubTotal } from "../baseElement";

const headers = [
    {name: 'Libelle', style: ['textLeft', 'textXs'], width: '*'}, 
    {name: 'Courant', style: ['textCenter', 'textXs'], width: '8%'},
    {name: 'Eff', style: ['textCenter', 'textXs'], width: '4%'},
    {name: 'M.O', style: ['textCenter', 'textXs'], width: '4%'},
    {name: 'Salaire/Mois', style: ['textCenter', 'textXs'], width: '*'},
    {name: 'Année 1', style: ['textRight', 'textXs'], width: '*'},
    {name: '%', style: ['textCenter'], width: '4%'},
    {name: 'Année 2', style: ['textRight', 'textXs'], width: '*'},
    {name: '%', style: ['textCenter'], width: '4%'},
    {name: 'Année 3', style: ['textRight', 'textXs'], width: '*'},
]

/**
 * Apports en matieres Premieres
 * @param {Array} fraisPersonnel 
 * @param {Object} charges
 * @param {Object} currency
 * @returns {Array}
 */
export function fraisPersonnel (fraisPersonnel, charges, currency) {

    let subBody = []

    const subSectionTitles = {
        "dirigeants": '4.1 Dirigeants',
        "salaries": '4.2 Salariés',
        "stagiaires": '4.3 Stagiaires'
    }

    for (const [key, values] of Object.entries(fraisPersonnel)) {
        subBody.push(
            [
                {text: subSectionTitles[`${key}`], colSpan: 4, style: styleSubTotal, border: borders}, {}, {}, {},
                {text: PdfModel.formatCurrency(charges[key]?.annee1, currency), colSpan: 2, style: [...styleSubTotal, 'textRight'], border: borders}, {},
                {text: PdfModel.formatCurrency(charges[key]?.annee2, currency), colSpan: 2, style: [...styleSubTotal, 'textRight'], border: borders}, {}, 
                {text: PdfModel.formatCurrency(charges[key]?.annee3, currency), colSpan: 2, style: [...styleSubTotal, 'textRight'], border: borders}, {}
            ],
            ...values.map(value => {
                return [
                    {text: value?.titre, style: styleLineCell, border: borders},
                    {text: value?.contrat, style: [...styleLineCell, 'textCenter'], border: borders},
                    {text: value?.effectif, style: [...styleLineCell, 'textCenter'], border: borders},
                    {text: value?.ouvrable, style: [...styleLineCell, 'textCenter'], border: borders},
                    {text: PdfModel.formatCurrency(value?.salaire, currency), style: [...styleLineCell, 'textRight'], border: borders},
                    {text: PdfModel.formatCurrency(value?.annee1, currency), style: [...styleLineCell, 'textRight'], border: borders},
                    {text: value?.accroissement1 + ' %', style: [...styleLineCell, 'textCenter'], border: borders},
                    {text: PdfModel.formatCurrency(value?.annee2, currency), style: [...styleLineCell, 'textRight'], border: borders},
                    {text: value?.accroissement2 + ' %', style: [...styleLineCell, 'textCenter'], border: borders},
                    {text: PdfModel.formatCurrency(value?.annee3, currency), style: [...styleLineCell, 'textRight'], border: borders},
                ]
            })
        )
    }

    const body = [
        [
            ...headers.map(itemHeader => {
                return {
                    text: itemHeader.name, style: [...styleHeaderCell, ...itemHeader.style], border: borders
                }
            })
        ],
        ...subBody
    ]

    return table(body, [...headers.map(column => column.width)], [0])
}
