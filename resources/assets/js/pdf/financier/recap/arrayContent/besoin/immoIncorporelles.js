import PdfModel from "../../../../PdfModel";
import { table } from "../../../../toolBuilder";
import { styleHeaderCell, borders, headers, styleLineCell, styleSubTotal } from "../baseElement";

/**
 * Tableau des Immobilisation Incorporelle
 * @param {Array} immoIncorporelle 
 * @param {Number} total 
 * @returns {Array}
 */
export function immoIncorporelle(immoIncorporelles, total, currency) {

    const body = [
        [
            ...headers.map(itemHeader => {
                return {
                    text: itemHeader.name, style: [...styleHeaderCell, ...itemHeader.style], border: borders
                }
            })
        ],
        ...immoIncorporelles.map(immo => {
            return [
                {text: immo.titre, border: borders, style: styleLineCell},
                {text: immo.quantite, border: borders, style: ['textCenter', ...styleLineCell]},
                {text: PdfModel.formatCurrency(immo.prix, currency), border: borders, style: ['textRight', ...styleLineCell]},
                {text: PdfModel.formatCurrency(immo.prix * immo.quantite, currency), border: borders, style: ['textRight', ...styleLineCell]},
            ]
        }),
        [
            {text: 'Total', colSpan: 3, border: borders, style: styleSubTotal}, {}, {},
            {text: PdfModel.formatCurrency(total, currency), border: borders, style: ['textRight', ...styleSubTotal]}
        ]
    ]

    return table(body, ['*', '*', '*', '*'], [15])
}
