import PdfModel from "../../../../PdfModel";
import { table } from "../../../../toolBuilder";
import { styleHeaderCell, borders, headers, styleLineCell, styleSubTotal } from "../baseElement";

/**
 * 
 * @param {Object} immofinancieres Immobilisation financière
 * @param {Number} total Total Immobilisation Financière
 * @param {Object} currency Monay à ajouter au montant
 * @param {Object} besoins 
 * @returns {Array}
 */
export function immoFinanciere(immofinancieres, total, currency, besoins) {

    let subBody = []

    for (const [key, values] of Object.entries(immofinancieres)) {
        subBody.push([
            ...values.map(value => {
                return [
                    {text: value.titre, border: borders, style: styleLineCell},
                    {text: value.quantite, border: borders, style: ['textCenter', ...styleLineCell]},
                    {text: PdfModel.formatCurrency(value.montant, currency), border: borders, style: ['textRight', ...styleLineCell]},
                    {text: PdfModel.formatCurrency(value.montant * value.quantite, currency), border: borders, style: ['textRight', ...styleLineCell]},
                ]
            }),
            [
                {text: key, colSpan: 3, border: borders, style: styleSubTotal}, {}, {},
                {text: PdfModel.formatCurrency(besoins[`${key}`], currency), border: borders, style: ['textRight', ...styleSubTotal]}
            ]
        ])
    }

    subBody = subBody.flat()

    const body = [
        [
            ...headers.map(itemHeader => {
                return {
                    text: itemHeader.name, style: [...styleHeaderCell, ...itemHeader.style], border: borders
                }
            })
        ],
        ...subBody,
        [
            {text: 'Total', colSpan: 3, border: borders}, {}, {},
            {text: PdfModel.formatCurrency(total, currency), border: borders, style: ['textRight']}
        ]
    ]

    return table(body, ['*', '*', '*', '*'], [15])
}
