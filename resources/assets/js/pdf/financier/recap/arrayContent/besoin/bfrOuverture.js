import PdfModel from "../../../../PdfModel";
import { table } from "../../../../toolBuilder";
import { styleHeaderCell, borders, styleLineCell, styleSubTotal } from "../baseElement";

const headers = [
    {name: 'Libelle', style: ['textLeft']}, 
    {name: 'Charge/Mois', style: ['textRight']},
    {name: '%', style: ['textCenter']},
    {name: 'Durrée', style: ['textCenter']},
    {name: 'Montant', style: ['textRight']},
]

/**
 * Tableau du BFR d'ouverture
 * @param {Array} ouvertureIncorporelle 
 * @param {Number} total 
 * @param {Object} currency
 * @returns {Array}
 */
export function bfrOuverture (ouvertures, total, currency) {

    const body = [
        [
            ...headers.map(itemHeader => {
                return {
                    text: itemHeader.name, style: [...styleHeaderCell, ...itemHeader.style], border: borders
                }
            })
        ],
        ...ouvertures.map(ouverture => {
            return [
                {text: ouverture.titre, border: borders, style: styleLineCell},
                {text: PdfModel.formatCurrency(ouverture.coutMensuel, currency), border: borders, style: ['textRight', ...styleLineCell]},
                {text: ouverture.pourcentage, border: borders, style: ['textCenter', ...styleLineCell]},
                {text: ouverture.duree, border: borders, style: ['textCenter', ...styleLineCell]},
                {text: PdfModel.formatCurrency(ouverture.coutMensuel * ouverture.duree, currency), border: borders, style: ['textRight', ...styleLineCell]},
            ]
        }),
        [
            {text: 'Total', colSpan: 4, border: borders, style: styleSubTotal}, {}, {}, {},
            {text: PdfModel.formatCurrency(total, currency), border: borders, style: ['textRight', ...styleSubTotal]}
        ]
    ]

    return table(body, ['*', '*', '*', '*', '*'], [15])
}
