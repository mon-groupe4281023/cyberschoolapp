import PdfModel from "../../../../PdfModel";
import { table } from "../../../../toolBuilder";
import { styleSubTotal, borders } from "../baseElement";

/**
 * Total des Beoins initial, Première Année, Deuxième Année, Troisième Année
 * @param {Object} total Total Besoins
 * @param {Object} currency Type de monaie
 * @returns {Array}
 */
export function totalBesoin(total, currency) {
    const body = [
        [
            {text: 'Total Global', style: ['textMd', 'bgGray'], border: borders},
            {text: 'Initial', style: ['textMd', 'bgGray', 'textRight'], border: borders},
            {text: 'Année 1', style: ['textMd', 'bgGray', 'textRight'], border: borders},
            {text: 'Année 2', style: ['textMd', 'bgGray', 'textRight'], border: borders},
            {text: 'Année 3', style: ['textMd', 'bgGray', 'textRight'], border: borders}
        ],
        [{text: '',margin: [0, 10, 0, 0], border: borders, colSpan: 5}, {}, {}, {}, {}],
        [
            {text: 'BESOINS', style: ['textMd', 'bgGrayLight'], border: [true, true, false, true]},
            {text: PdfModel.formatCurrency(total.initial, currency), style: ['textMd', 'bgGrayLight', 'textRight'], border: [false, true, false, true]},
            {text: PdfModel.formatCurrency(total.annee1, currency), style: ['textMd', 'bgGrayLight', 'textRight'], border: [false, true, false, true]},
            {text: PdfModel.formatCurrency(total.annee2, currency), style: ['textMd', 'bgGrayLight', 'textRight'], border: [false, true, false, true]},
            {text: PdfModel.formatCurrency(total.annee3, currency), style: ['textMd', 'bgGrayLight', 'textRight'], border: [false, true, true, true]},
        ],
    ]

    return table(body, ['*', '*', '*', '*', '*'], [15])
}
