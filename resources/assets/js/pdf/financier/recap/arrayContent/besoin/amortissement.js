import PdfModel from "../../../../PdfModel";
import { table } from "../../../../toolBuilder";
import { styleHeaderCell, borders, styleLineCell, styleSubTotal } from "../baseElement";

const headers = [
    {name: 'Libelle', style: ['textLeft']},
    {name: 'Prix total HT', style: ['textRight']},
    {name: 'Durée', style: ['textCenter']},
    {name: 'Amortissement/an', style: ['textRight']},
]

/**
 * 
 * @param {Object} amortissement Immobilisation Corporelle et incorporelle
 * @param {Number} besoins besoins
 * @param {Object} currency Monay à ajouter au montant
 * @returns {Array}
 */
export function amortissement(amortissement, besoins, currency) {

    let subBody = []

    const subSectionTitles = {
        "totalImmoIncorporelle": '5.1. Immobilisations Incorporelles',
        "batiment-local-espacevente": '5.5. Bâtiment/Local/Espace de Vente ',
        "materiel-machine-outil": '5.4. Matériel/Machine/outils',
        "mobilier": '5.3. Mobilier',
        "voiture-autres": '5.6. Moyens de transports',
        "materielinfo": '5.2. Matériel Informatique',
    }

    for (const [key, values] of Object.entries(amortissement)) {
        subBody.push(
            [
                {text: subSectionTitles[`${key}`], style: styleSubTotal, border: borders}, 
                {text: PdfModel.formatCurrency(besoins[`${key}`].prixHT, currency), style: [...styleSubTotal, 'textRight'], border: borders}, 
                {text: '', style: styleSubTotal, border: borders},
                {text: PdfModel.formatCurrency(besoins[`${key}`].echeancier, currency), style: [...styleSubTotal, 'textRight'], border: borders}
            ],
            ...values.map(value => {
                return [
                    {text: value?.titre, style: styleLineCell, border: borders},
                    {text: PdfModel.formatCurrency(value?.prixTotalHT, currency), style: [...styleLineCell, 'textRight'], border: borders},
                    {text: value?.amortissement + ' ans', style: [...styleLineCell, 'textCenter'], border: borders},
                    {text: PdfModel.formatCurrency(value?.amortissementAnnuel, currency), style: [...styleLineCell, 'textRight'], border: borders}
                ]
            })
        )
    }

    const body = [
        [
            ...headers.map(itemHeader => {
                return {
                    text: itemHeader.name, style: [...styleHeaderCell, ...itemHeader.style], border: borders
                }
            })
        ],
        ...subBody
    ]

    return table(body, ['40%', '*', '10%', '*'], [15])
}
