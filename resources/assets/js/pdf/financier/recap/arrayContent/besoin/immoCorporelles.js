import PdfModel from "../../../../PdfModel";
import { table } from "../../../../toolBuilder";
import { styleHeaderCell, borders, headers, styleLineCell, styleSubTotal } from "../baseElement";

/**
 * @param {Array} immoIncorporelle 
 * @param {Number} total 
 * @returns {Array}
 */
export function immocorporelle(immocorporelles, total, currency, besoins) {

    const subSectionTitles = {
        materielinfo: 'Matériel Informatique',
        mobilier: 'Mobilier',
        "materiel-machine-outil": 'Materiel Machine Outil',
        "batiment-local-espacevente": 'Batiment Local Espacevente',
        "voiture-autres": "Voiture Autres"
    }

    let subBody = []

    for (const [key, values] of Object.entries(immocorporelles)) {
        subBody.push([
            ...values.map(value => {
                return [
                    {text: value.titre, border: borders, style: styleLineCell},
                    {text: value.quantite, border: borders, style: ['textCenter', ...styleLineCell]},
                    {text: PdfModel.formatCurrency(value.prix, currency), border: borders, style: ['textRight', ...styleLineCell]},
                    {text: PdfModel.formatCurrency(value.prix * value.quantite, currency), border: borders, style: ['textRight', ...styleLineCell]},
                ]
            }),
            [
                {text: subSectionTitles[key], colSpan: 3, border: borders, style: styleSubTotal}, {}, {},
                {text: PdfModel.formatCurrency(besoins[`${key}`]?.prix, currency), border: borders, style: ['textRight', ...styleSubTotal]}
            ]
        ])
    }

    subBody = subBody.flat()

    const body = [
        [
            ...headers.map(itemHeader => {
                return {
                    text: itemHeader.name, style: [...styleHeaderCell, ...itemHeader.style], border: borders
                }
            })
        ],
        ...subBody,
        [
            {text: 'Total', colSpan: 3, border: borders}, {}, {},
            {text: PdfModel.formatCurrency(total, currency), border: borders, style: ['textRight']}
        ]
    ]

    return table(body, ['*', '*', '*', '*'], [15])
}
