import { table } from "../../../../toolBuilder";
import { styleHeaderCell, borders, styleLineCell, styleSubTotal } from "../baseElement";

/**
 * Volumes des ventes par mois
 * @param {Array} articles
 * @returns {Array}
 */
export function volumeVentesArticles (articles) {
    const body = [
        ...articles.map((article, index) => {
            const ventes = Object.values(article?.ventes)
            const total = ventes.reduce((acc, current) => acc + current.venteMensuelle, 0)
            const style = [...((index % 2 === 0) ? styleLineCell: styleHeaderCell), 'textXs']
            return [
                {text: article?.nom, style, border: borders},
                ...ventes.map(vente => {
                    return {text: parseFloat(vente.venteMensuelle).toFixed(2), border: borders, style: [...style, 'textCenter']}
                }),
                {text: parseFloat(total).toFixed(2), border: borders, style: [...style, 'textCenter']}
            ]
        })
    ]
    return table(body, ['30%', ...Object.values(articles[0].ventes).map(() => '*'), '8%'], [0])
}
