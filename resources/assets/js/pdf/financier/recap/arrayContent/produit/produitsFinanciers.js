import PdfModel from "../../../../PdfModel";
import { table } from "../../../../toolBuilder";
import { styleHeaderCell, borders, styleLineCell, styleSubTotal } from "../baseElement";

const headers = [
    {name: 'Libelle', style: ['textLeft', 'textXs'], width: '*'}, 
    {name: 'Qte', style: ['textCenter', 'textXs'], width: '8%'},
    {name: 'Prix Unitaire', style: ['textRight', 'textXs'], width: '*'},
    {name: 'Année 1', style: ['textRight', 'textXs'], width: '*'},
    {name: '%', style: ['textCenter'], width: '4%'},
    {name: 'Année 2', style: ['textRight', 'textXs'], width: '*'},
    {name: '%', style: ['textCenter'], width: '4%'},
    {name: 'Année 3', style: ['textRight', 'textXs'], width: '*'},
]

/**
 * Liste des produits financiers
 * @param {Array} articles
 * @param {Object} currency
 * @returns {Array}
 */
export function produitsFinanciers (produitsFinanciers, currency) {

    const totalAnnee1 = produitsFinanciers.reduce((acc, current) => acc + current?.annee1, 0)
    const totalAnnee2 = produitsFinanciers.reduce((acc, current) => acc + current?.annee2, 0)
    const totalAnnee3 = produitsFinanciers.reduce((acc, current) => acc + current?.annee3, 0)

    const body = [
        [
            ...headers.map(itemHeader => {
                return {
                    text: itemHeader.name, style: [...styleHeaderCell, ...itemHeader.style], border: borders
                }
            })
        ],
        [
            {text: 'Total Produits Financiers', colSpan: 2, border: borders, style: [...styleSubTotal, 'textXs']}, {}, 
            {text: PdfModel.formatCurrency(totalAnnee1, currency), colSpan: 2, border: borders, style: [...styleSubTotal, 'textXs', 'textRight']},{}, 
            {text: PdfModel.formatCurrency(totalAnnee2, currency), colSpan: 2, border: borders, style: [...styleSubTotal, 'textXs', 'textRight']},{},
            {text: PdfModel.formatCurrency(totalAnnee3, currency), colSpan: 2, border: borders, style: [...styleSubTotal, 'textXs', 'textRight']},{}
        ],

        ...produitsFinanciers.map(produit => {
            return [
                {text: produit?.nom, border: borders, style: [styleLineCell, 'textXs']},
                {text: produit?.quantite, border: borders, style: [styleLineCell, 'textXs', 'textCenter']},
                {text: PdfModel.formatCurrency(produit?.prix, currency), border: borders, style: [styleLineCell, 'textXs', 'textRight']},
                {text: PdfModel.formatCurrency(produit?.annee1, currency), border: borders, style: [styleLineCell, 'textXs', 'textRight']},
                {text: produit?.accroissement1, border: borders, style: [styleLineCell, 'textXs', 'textCenter']},
                {text: PdfModel.formatCurrency(produit?.annee2, currency), border: borders, style: [styleLineCell, 'textXs', 'textRight']},
                {text: produit?.accroissement2, border: borders, style: [styleLineCell, 'textXs', 'textCenter']},
                {text: PdfModel.formatCurrency(produit?.annee3, currency), border: borders, style: [styleLineCell, 'textXs', 'textRight']},
            ]
        })
    ]

    return table(body, [...headers.map(column => column.width)], [0])
}
