import PdfModel from "../../../../PdfModel";
import { table } from "../../../../toolBuilder";
import { styleHeaderCell, borders, styleLineCell, styleSubTotal } from "../baseElement";

const headers = [
    {name: 'N°', style: ['textLeft']}, 
    {name: 'Libelle', style: ['textLeft']},
    {name: 'Prix V.U (HT)', style: ['textRight']}
]

/**
 * Liste des articles/produits
 * @param {Array} articles 
 * @param {Object} currency
 * @returns {Array}
 */
export function articles (articles, currency) {

    const body = [
        [
            ...headers.map(itemHeader => {
                return {
                    text: itemHeader.name, style: [...styleHeaderCell, ...itemHeader.style], border: borders
                }
            })
        ],
        [
            {text: 'Articles', style: styleSubTotal, border: borders},
            {
                text: PdfModel.formatCurrency(articles.reduce((acc, current) => acc + current.prixVenteUnitaire, 0), currency), 
                style: [...styleSubTotal, 'textRight'], border: borders, colSpan: 2
            }, {}
        ],
        ...articles.map((article, index) => {
            return [
                {text: index + 1, style: styleLineCell, border: borders},
                {text: article?.nom, style: styleLineCell, border: borders},
                {text: PdfModel.formatCurrency(article?.prixVenteUnitaire, currency), style: [...styleLineCell, 'textRight'], border: borders},
            ]
        })

    ]

    return table(body, ['20%', '*', '*'], [0])
}
