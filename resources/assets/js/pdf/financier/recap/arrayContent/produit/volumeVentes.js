import { table } from "../../../../toolBuilder";
import { styleHeaderCell, borders, styleLineCell, styleSubTotal } from "../baseElement";

const headers = [
    {name: 'Mois', column: 'nom', style: [...styleHeaderCell, 'textLeft', 'textXs'], total: 'Total'}, 
    {name: 'NB jour par mois', column: 'jours', style: [...styleSubTotal, 'textXs'], total: 0},
    {name: 'NB Dimanche (sans vente)', column: 'dimanches', style: [...styleLineCell, 'textXs'], total: 0},
    {name: 'NB Samedi (sans vente) ', column: 'samedis', style: [...styleHeaderCell, 'textXs'], total: 0},
    {name: 'NB Jours fériés (sans vente)', column: 'feries', style: [...styleLineCell, 'textXs'], total: 0},
    {name: 'NB Jours de vente (réel)', column: 'ouvrables', style: [...styleHeaderCell, 'textXs'], total: 0}
]

/**
 * Calendrier des ventes, Liste des jours ouvrables de l'année
 * @param {Array} volumeVentes
 * @returns {Array}
 */
export function volumeVentes (volumeVentes) {
    const body = [
        ...headers.map((header) => {
            const total = (!header.total) ? volumeVentes.reduce((acc, current) => acc + current[header.column], header.total) : header.total
            const style = [...header.style, 'textCenter']
            return [
                {text: header.name, border: borders, style: header.style},
                ...volumeVentes.map(volumeVente => {
                    return {text: volumeVente[header.column], style, border: borders}
                }),
                {text: total, style, border: borders}
            ]
        })
    ]

    return table(body, ['30%', ...volumeVentes.map(() => '*'), '6%'], [0])
}
