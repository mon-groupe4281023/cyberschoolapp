import PdfModel from "../../../../PdfModel";
import { table } from "../../../../toolBuilder";
import { styleHeaderCell, borders, styleLineCell, styleSubTotal } from "../baseElement";

const headers = [
    {name: 'Libelle', style: ['textLeft']}, 
    {name: 'Nom/Prénom', style: ['textCenter']},
    {name: 'Qte', style: ['textCenter']},
    {name: 'Apport unitaire', style: ['textRight']},
    {name: 'Total', style: ['textRight']},
]

/**
 * Apports en capitaux
 * @param {Array} capitaux 
 * @param {Object} ressources
 * @param {Object} currency
 * @returns {Array}
 */
export function apportCapitaux (capitaux, ressources, currency) {

    let subBody = []

    const subSectionTitles = {
        "numeraire": '1.1 Apport Numéraire',
        "nature": '1.2 Apport en Nature'
    }

    for (const [key, values] of Object.entries(capitaux)) {
        subBody.push(
            [
                {text: subSectionTitles[key], colSpan: 4, style: styleSubTotal, border: borders}, {}, {}, {}, 
                {text: PdfModel.formatCurrency(ressources[key], currency), style: [...styleSubTotal, 'textRight'], border: borders}
            ],
            ...values.map(value => {
                return [
                    {text: value?.apporteur, style: styleLineCell, border: borders},
                    {text: value?.identite, style: [...styleLineCell, 'textCenter'], border: borders},
                    {text: value?.quantite, style: [...styleLineCell, 'textCenter'], border: borders},
                    {text: PdfModel.formatCurrency(value?.montant, currency), style: [...styleLineCell, 'textRight'], border: borders},
                    {text: PdfModel.formatCurrency(value?.quantite * value?.montant, currency), style: [...styleLineCell, 'textRight'], border: borders},
                ]
            })
        )
    }

    const body = [
        [
            ...headers.map(itemHeader => {
                return {
                    text: itemHeader.name, style: [...styleHeaderCell, ...itemHeader.style], border: borders
                }
            })
        ],
        ...subBody
    ]

    return table(body, ['*', '*', '*', '*', '*'], [15])
}
