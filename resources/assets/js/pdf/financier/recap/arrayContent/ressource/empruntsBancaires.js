import PdfModel from "../../../../PdfModel";
import { table } from "../../../../toolBuilder";
import { styleHeaderCell, borders, styleLineCell, styleSubTotal } from "../baseElement";

const headers = [
    {name: 'Libelle', style: ['textLeft']}, 
    {name: 'Montant', style: ['textRight']},
    {name: 'Taux(%)', style: ['textCenter']},
    {name: 'Durée(mois)', style: ['textCenter']},
    {name: 'Remboursement', style: ['textRight']},
    {name: 'Echeancier', style: ['textRight']},
]

/**
 * Emprunts Bancaires
 * @param {Array} emprunts 
 * @param {Object} ressources
 * @param {Object} currency
 * @returns {Array}
 */
export function empruntsBancaires (emprunts, ressources, currency) {

    let subBody = []

    const subSectionTitles = {
        "banque-emprunts": '4.1 Banques',
        "microfinance-emprunts": '4.2 Microfinances',
    }

    for (const [key, values] of Object.entries(emprunts)) {

        subBody.push(
            [
                {text: subSectionTitles[`${key}-emprunts`], style: styleSubTotal, border: borders},
                {text: PdfModel.formatCurrency(ressources[`${key}-emprunts`].montant, currency), style: [...styleSubTotal, 'textRight'], border: borders},
                {text: PdfModel.formatCurrency(ressources[`${key}-emprunts`].remboursement, currency), colSpan: 3, style: [...styleSubTotal, 'textRight'], border: borders}, {}, {},
                {text: PdfModel.formatCurrency(ressources[`${key}-emprunts`].echeancier, currency), style: [...styleSubTotal, 'textRight'], border: borders},
            ],
            ...values.map(value => {
                return [
                    {text: value?.creancier, style: styleLineCell, border: borders},
                    {text: PdfModel.formatCurrency(value?.montantPret, currency) , style: [...styleLineCell, 'textRight'], border: borders},
                    {text: value?.taux + ' %', style: [...styleLineCell, 'textCenter'], border: borders},
                    {text: value?.duree + ' mois', style: [...styleLineCell, 'textCenter'], border: borders},
                    {text: PdfModel.formatCurrency(value.remboursement, currency), style: [...styleLineCell, 'textRight'], border: borders},
                    {text: PdfModel.formatCurrency(value.echeancier, currency), style: [...styleLineCell, 'textRight'], border: borders},
                ]
            })
        )
    }

    const body = [
        [
            ...headers.map(itemHeader => {
                return {
                    text: itemHeader.name, style: [...styleHeaderCell, ...itemHeader.style], border: borders
                }
            })
        ],
        ...subBody
    ]

    return table(body, ['*', '*', '*', '*', '*', '*'], [15])
}
