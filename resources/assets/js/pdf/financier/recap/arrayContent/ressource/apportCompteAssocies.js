import PdfModel from "../../../../PdfModel";
import { table } from "../../../../toolBuilder";
import { styleHeaderCell, borders, styleLineCell, styleSubTotal } from "../baseElement";

const headers = [
    {name: 'Libelle', style: ['textLeft']}, 
    {name: 'Nom/Prénom', style: ['textCenter']},
    {name: 'Apport unitaire', style: ['textRight']},
]

/**
 * Apports en Comptes courant Associés
 * @param {Array} comptes 
 * @param {Object} ressources
 * @param {Object} currency
 * @returns {Array}
 */
export function apportCompteAssocies (comptes, ressources, currency) {

    const body = [
        [
            ...headers.map(itemHeader => {
                return {
                    text: itemHeader.name, style: [...styleHeaderCell, ...itemHeader.style], border: borders
                }
            })
        ],
        [
            {text: 'Total Apport Numéraire', colSpan: 2, style: styleSubTotal, border: borders}, {},
            {text: PdfModel.formatCurrency(ressources.compteAssociate, currency), style: [...styleSubTotal, 'textRight'], border: borders}
        ],
        ...comptes.map(compte => {
            return [
                {text: compte?.apporteur, style: styleLineCell, border: borders},
                {text: compte?.identite, style: [...styleLineCell, 'textCenter'], border: borders},
                {text: PdfModel.formatCurrency(compte?.montant, currency), style: [...styleLineCell, 'textRight'], border: borders}
            ]
        })
    ]

    return table(body, ['*', '*', '*',], [15])
}
