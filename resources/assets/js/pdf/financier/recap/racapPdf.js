import PdfModel from "../../../pdf/PdfModel";
import {content} from "./content"
import doc from "../../docDefinitionModel"

class recapPdf extends PdfModel{

    /**
     *
     * @param base
     * @param base.title {String} Titre du document
     * @param base.data {Object} Ensemble de données
     * @param options {Object} - Autres informations sur le Document
     */
    constructor (base = {}, options = {}) {
        super(base, options)
    }

    docDefinition () {
        const docDefinition = {
            pageOrientation: '',
            permissions: {
                printing: 'highResolution', //'lowResolution'
                modifying: false,
                copying: false,
                annotating: true,
                fillingForms: true,
                contentAccessibility: true,
                documentAssembly: true
            }
        }
        return super.docDefinition(docDefinition);
    }

    defaultStyle () {
        return {
            fontSize: 10,
            bold: false,
            alignment: 'left',
            // font: this.options['meta'].police
        }
    }

    styles () {
        return Object.assign({}, super.styles(), {
            textCenter:{
                alignment: 'center'
            },
            textRight:{
                alignment: 'right'
            },
            textLeft: {
                alignment: 'left'
            },
            textXl: {
                fontSize: 18
            },
            textLg: {
                fontSize: 15
            },
            textMd: {
                fontSize: 11
            },
            textSm: {
                fontSize: 9
            },
            textXs: {
                fontSize: 8
            },
            bold: {
                bold: true
            },
            textWhite: {
                color: 'white'
            },
            textBlue: {
                fillColor: '#2359a0'
            },
            textBlack: {
                color: 'black'
            },
            bgBlueLight: {
                fillColor: '#e2eaf5'
            },
            bgGrayLight: {
                fillColor: '#e3e4e7'
            }
        })
    }

    content() {
        return content(this.data?.besoins, this.data?.ressources, this.data?.charges, this.data?.produits,  this.options?.currency)
    }
}

export default recapPdf
