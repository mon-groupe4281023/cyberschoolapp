import { immoIncorporelle } from "./arrayContent/besoin/immoIncorporelles"
import { immocorporelle } from "./arrayContent/besoin/immoCorporelles"
import { immoFinanciere } from "./arrayContent/besoin/immoFinanciere"
import { bfrOuverture } from "./arrayContent/besoin/bfrOuverture"
import { bfrExploitation } from "./arrayContent/besoin/bfrExploitation"
import { amortissement } from "./arrayContent/besoin/amortissement"
import { totalBesoin } from "./arrayContent/besoin/totalBesoin"
import { apportCapitaux } from "./arrayContent/ressource/apportCapitaux"
import { apportCompteAssocies } from "./arrayContent/ressource/apportCompteAssocies"
import { subventionsAides } from "./arrayContent/ressource/subventionsAides"
import { empruntsBancaires } from "./arrayContent/ressource/empruntsBancaires"
import { totalRessources } from "./arrayContent/ressource/totalRessources"
import { achatsMatieres } from "./arrayContent/charge/achatsMatieres"
import { servicesExterieurs } from "./arrayContent/charge/servicesExterieurs"
import { impotsTaxe } from "./arrayContent/charge/impotsTaxe"
import { fraisPersonnel } from "./arrayContent/charge/fraisPersonnel"
import { articles } from "./arrayContent/produit/articles"
import { volumeVentes } from "./arrayContent/produit/volumeVentes"
import { volumeVentesArticles } from "./arrayContent/produit/volumeVentesArticles"
import { chiffreAffaireExploitation } from "./arrayContent/produit/chiffreAffaireExploitation"
import { produitsFinanciers } from "./arrayContent/produit/produitsFinanciers"

/**
 * Contenu du Pdf récapitulatif
 * @param {Object} besoins Listes des besoins
 * @param {Object} ressources Liste des ressources
 * @param {Object} charges Liste des charges
 * @param {Object} currency Monaie en cours d'utilisation
 * @returns {Array}
 */
export function content (besoins, ressources, charges, produits, currency) {

    const margin = [0,5,0,5]
    const styleTitle = ['textMd', 'bold']

    return [
        {text:'INVESTISSEMENTS', style:['textXl', 'bold'], margin},
        /**
         * Besions
         */
        {text:'Besoin', style: styleTitle, margin},
        {text: '1. Immobilisation Incorporelles', style:  styleTitle, margin},
        immoIncorporelle(besoins?.immoIncorporelle, besoins?.totalImmoIncorporelle?.prix, currency),
        {text: '2. Immobilisations Corporelles', style:  styleTitle, margin},
        immocorporelle(besoins?.immoCorporelle, besoins?.totalImmoCorporelle?.prix, currency, besoins),
        {text: '3. Immobilisations Financières', style:  styleTitle, margin},
        immoFinanciere(besoins?.immoFinanciere, besoins?.totalImmoFinanciere, currency, besoins),
        {text: '4. Besoins en fonds de roulement', style:  styleTitle, margin},
        {text: '4.1 Bfr d\'Ouvertures', style:  styleTitle, margin},
        bfrOuverture(besoins?.bfr?.bfrOuverture, besoins?.totalBfrOuverture, currency),
        {text: '', margin},
        {text: '4.2 Bfr d\'Exploitation', style:  styleTitle, margin},
        bfrExploitation(besoins?.bfr, besoins?.totalBfrExploitation, currency, besoins),
        {text: '5. Amortissement des immobilisations', style:  styleTitle, margin},
        amortissement({...{totalImmoIncorporelle: besoins.immoIncorporelle}, ...besoins.immoCorporelle}, besoins, currency),
        totalBesoin(besoins?.totalBesoins, currency),

        /**
         * Ressources
         */
        {text:'Ressources', style: styleTitle, margin},
        {text: '1. Apports en Capitaux', style:  styleTitle, margin},
        apportCapitaux(ressources.capital, ressources, currency),
        {text: '2. Comptes Courants Associés', style:  styleTitle, margin},
        apportCompteAssocies(ressources?.comptes, ressources, currency),
        {text: '3. Subventions Aides Institutions', style:  styleTitle, margin},
        subventionsAides(ressources["subventions-aides"], ressources, currency),
        {text: '4. Emprunts Bancaires', style:  styleTitle, margin},
        empruntsBancaires(ressources?.emprunts, ressources, currency),
        totalRessources(ressources?.totalRessources, currency),

        {text:'EXPLOITATIONS', style:['textXl', 'bold'], margin},
        /**
         * Produits
         */
        {text:'1. Produits et services', style: styleTitle, margin},
        articles(produits?.articles, currency),
        {text:'2. Volume de Vente', style: styleTitle, margin},
        volumeVentes(produits.volumeVente),   
        volumeVentesArticles(produits.articles),
        {text:'3. Chiffres d\'Affaires d\'Exploitation Année N°1-3', style: styleTitle, margin},
        chiffreAffaireExploitation(produits.articles, produits, currency),
        {text:'4. Produits financiers', style: styleTitle, margin},
        produitsFinanciers(produits.produitsFinanciers, currency),
        /**
         * Charges
         */
        {text:'Charges', style: styleTitle, margin},
        {text: '1. Achats de matières premières', style:  styleTitle, margin},
        achatsMatieres(charges['matieres-premieres'], charges, currency),
        {text: '2. Services Extérieurs', style:  styleTitle, margin},
        servicesExterieurs(charges['services-exterieurs'], charges, currency),
        {text: '3. Impôts et taxes', style:  styleTitle, margin},
        impotsTaxe(charges['taxes-impots'], charges, currency),
        {text: '4. Frais de Personnel (Salaires)', style:  styleTitle, margin},
        fraisPersonnel(charges['frais-personnel'], charges, currency),
    ]
}