import PdfModel from '../../PdfModel'
import { calculAmortissementImmoCorporelle, calculRemboursement, calculRemboursementHT } from "../../../services/helper"
const arrayColumns = ['9%.11', '9.11%', '9%.11', '9%', '9%', '9%', '9%', '9.11%', '9.11%', '9.11%', '9.11%']

var money = '';
var precision = '';

function buildArrayYears(allYears) {

    let yearsHeader = [
        { text: 'Années d\'exercices', style: ['bolded', 'subheader', 'lefted',], colSpan: 5, border: [false, false, false, false] }, {}, {},
        {}, {},
        {
            text: allYears.annee1, style: ['bolded', 'subheader', 'righted',], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: allYears.annee2, style: ['bolded', 'subheader', 'righted',], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: allYears.annee3, style: ['bolded', 'subheader', 'righted',], colSpan: 2, border: [false, false, false, false]
        }, {},
    ];
    let result = new Array(yearsHeader);
    return result;
}
function buildArrayCAE(TTcae) {
    let cae = [
        { text: 'CHIFFRE D’AFFAIRE D\'EXPLOITATION', style: ['bolded', 'subheader', 'lefted', 'backgroundGreen', 'whiteColor'], colSpan: 5, border: [false, false, false, false] }, {}, {},
        {}, {},
        {
            text: PdfModel.contentFormat(TTcae.annee1) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreen', 'whiteColor'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(TTcae.annee2) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreen', 'whiteColor'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(TTcae.annee3) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreen', 'whiteColor'], colSpan: 2, border: [false, false, false, false]
        }, {},
    ];
    let result = new Array(cae);
    return result;
}
function buildArrayVentes(TTcae, articles) {
    let ventes = [
        { text: 'Vente d\'articles', style: ['bolded', 'subheader', 'lefted', 'backgroundLightGreen'], colSpan: 5, border: [false, false, false, false] }, {}, {},
        {}, {},
        {
            text: PdfModel.contentFormat(TTcae.annee1) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundLightGreen'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(TTcae.annee2) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundLightGreen'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(TTcae.annee3) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundLightGreen'], colSpan: 2, border: [false, false, false, false]
        }, {},
    ];
    let result = new Array(ventes);
    for (let article of articles) {
        result.push([
            { text: article.nom, style: ['item', 'lefted'], colSpan: 5, border: [false, false, false, false] }, {}, {},
            {}, {},
            {
                text: PdfModel.contentFormat(article.chiffreAffaire.annee1.ht) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(article.chiffreAffaire.annee2.ht) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(article.chiffreAffaire.annee3.ht) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
        ]);
    }
    return result;
}
function buildArrayCE(TTcharges) {
    let cae = [
        { text: 'CHARGES D\'EXPLOITATION', style: ['bolded', 'subheader', 'lefted', 'backgroundRed', 'whiteColor'], colSpan: 5, border: [false, false, false, false] }, {}, {},
        {}, {},
        {
            text: PdfModel.contentFormat(TTcharges.annee1) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundRed', 'whiteColor'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(TTcharges.annee2) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundRed', 'whiteColor'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(TTcharges.annee3) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundRed', 'whiteColor'], colSpan: 2, border: [false, false, false, false]
        }, {},
    ];
    let result = new Array(cae);
    return result;
}
function buildArrayAchats(TTachats, TTdepenses, depenses) {
    let achatHeader = [
        { text: 'Achats', style: ['bolded', 'subheader', 'lefted', 'backgroundPink'], colSpan: 5, border: [false, false, false, false] }, {}, {},
        {}, {},
        {
            text: PdfModel.contentFormat(TTachats.annee1) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundPink'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(TTachats.annee2) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundPink'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(TTachats.annee3) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundPink'], colSpan: 2, border: [false, false, false, false]
        }, {},
    ];
    let result = new Array(achatHeader);
    result.push([
        { text: 'Dépenses variables liées aux produits', style: ['bolded', 'subheader', 'lefted', 'backgroundGreyRecap'], colSpan: 5, border: [false, false, false, false] }, {}, {},
        {}, {},
        {
            text: PdfModel.contentFormat(TTdepenses.annee1) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(TTdepenses.annee2) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(TTdepenses.annee3) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
    ]);
    for (let depense of depenses) {
        result.push([
            { text: depense.titre, style: ['item', 'lefted'], colSpan: 5, border: [false, false, false, false] }, {}, {},
            {}, {},
            {
                text: PdfModel.contentFormat(depense.annee1) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(depense.annee2) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(depense.annee3) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
        ]);
    }
    return result;
}
function buildArraySE(TTse, services) {
    let seHeader = [
        { text: 'Services extérieurs', style: ['bolded', 'subheader', 'lefted', 'backgroundPink'], colSpan: 5, border: [false, false, false, false] }, {}, {},
        {}, {},
        {
            text: PdfModel.contentFormat(TTse.annee1) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundPink'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(TTse.annee2) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundPink'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(TTse.annee3) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundPink'], colSpan: 2, border: [false, false, false, false]
        }, {},
    ];
    let result = new Array(seHeader);

    for (let service of services) {
        result.push([
            { text: service.titre, style: ['item', 'lefted'], colSpan: 5, border: [false, false, false, false] }, {}, {},
            {}, {},
            {
                text: PdfModel.contentFormat(service.annee1) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(service.annee2) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(service.annee3) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
        ]);
    }
    return result;
}
function buildArrayImpotsTaxes(TTtaxe, ssTT, taxes) {
    let taxeHeader = [
        { text: 'Impôts et Taxes', style: ['bolded', 'subheader', 'lefted', 'backgroundPink'], colSpan: 5, border: [false, false, false, false] }, {}, {},
        {}, {},
        {
            text: PdfModel.contentFormat(TTtaxe.annee1) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundPink'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(TTtaxe.annee2) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundPink'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(TTtaxe.annee3) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundPink'], colSpan: 2, border: [false, false, false, false]
        }, {},
    ];
    let result = new Array(taxeHeader);
    result.push([
        { text: 'Taxes aux Impôts', style: ['bolded', 'subheader', 'lefted', 'backgroundGreyRecap'], colSpan: 5, border: [false, false, false, false] }, {}, {},
        {}, {},
        {
            text: PdfModel.contentFormat(ssTT['autre'].annee1) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(ssTT['autre'].annee2) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(ssTT['autre'].annee3) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
    ]);
    for (let autre of taxes['autre']) {
        result.push([
            { text: autre.titre, style: ['item', 'lefted'], colSpan: 5, border: [false, false, false, false] }, {}, {},
            {}, {},
            {
                text: PdfModel.contentFormat(autre.annee1) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(autre.annee2) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(autre.annee3) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
        ]);
    }
    result.push([
        { text: 'Taxes de la Mairie', style: ['bolded', 'subheader', 'lefted', 'backgroundGreyRecap'], colSpan: 5, border: [false, false, false, false] }, {}, {},
        {}, {},
        {
            text: PdfModel.contentFormat(ssTT['mairie'].annee1) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(ssTT['mairie'].annee2) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(ssTT['mairie'].annee3) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
    ]);
    for (let mairie of taxes['mairie']) {
        result.push([
            { text: mairie.titre, style: ['item', 'lefted'], colSpan: 5, border: [false, false, false, false] }, {}, {},
            {}, {},
            {
                text: PdfModel.contentFormat(mairie.annee1) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(mairie.annee2) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(mairie.annee3) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
        ]);
    }
    return result;
}
function buildArrayFP(TTfp, ssTT, fps) {
    let fpHeader = [
        { text: 'Frais de personnel', style: ['bolded', 'subheader', 'lefted', 'backgroundPink'], colSpan: 5, border: [false, false, false, false] }, {}, {},
        {}, {},
        {
            text: PdfModel.contentFormat(TTfp.annee1) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundPink'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(TTfp.annee2) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundPink'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(TTfp.annee3) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundPink'], colSpan: 2, border: [false, false, false, false]
        }, {},
    ];
    let result = new Array(fpHeader);
    result.push([
        { text: 'Rémunération des salariés', style: ['bolded', 'subheader', 'lefted', 'backgroundGreyRecap'], colSpan: 5, border: [false, false, false, false] }, {}, {},
        {}, {},
        {
            text: PdfModel.contentFormat(ssTT['salaire'].annee1) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(ssTT['salaire'].annee2) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(ssTT['salaire'].annee3) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
    ]);
    for (let salaire of fps['salaire']) {
        result.push([
            { text: salaire.titre, style: ['item', 'lefted'], colSpan: 5, border: [false, false, false, false] }, {}, {},
            {}, {},
            {
                text: PdfModel.contentFormat(salaire.annee1) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(salaire.annee2) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(salaire.annee3) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
        ]);
    }
    result.push([
        { text: 'Rémunération des stagiaires', style: ['bolded', 'subheader', 'lefted', 'backgroundGreyRecap'], colSpan: 5, border: [false, false, false, false] }, {}, {},
        {}, {},
        {
            text: PdfModel.contentFormat(ssTT['stagiaire'].annee1) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(ssTT['stagiaire'].annee2) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(ssTT['stagiaire'].annee3) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
    ]);
    for (let stagiaire of fps['stagiaire']) {
        result.push([
            { text: stagiaire.titre, style: ['item', 'lefted'], colSpan: 5, border: [false, false, false, false] }, {}, {},
            {}, {},
            {
                text: PdfModel.contentFormat(stagiaire.annee1) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(stagiaire.annee2) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(stagiaire.annee3) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
        ]);
    }
    result.push([
        { text: 'Charges sociales des employés', style: ['bolded', 'subheader', 'lefted', 'backgroundGreyRecap'], colSpan: 5, border: [false, false, false, false] }, {}, {},
        {}, {},
        {
            text: PdfModel.contentFormat(ssTT['chargeSocialeE'].annee1) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(ssTT['chargeSocialeE'].annee2) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(ssTT['chargeSocialeE'].annee3) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
    ]);
    result.push([
        { text: 'Rémunération des dirigeants', style: ['bolded', 'subheader', 'lefted', 'backgroundGreyRecap'], colSpan: 5, border: [false, false, false, false] }, {}, {},
        {}, {},
        {
            text: PdfModel.contentFormat(ssTT['dirigeant'].annee1) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(ssTT['dirigeant'].annee2) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(ssTT['dirigeant'].annee3) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
    ]);
    for (let dirigeant of fps['dirigeant']) {
        result.push([
            { text: dirigeant.titre, style: ['item', 'lefted'], colSpan: 5, border: [false, false, false, false] }, {}, {},
            {}, {},
            {
                text: PdfModel.contentFormat(dirigeant.annee1) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(dirigeant.annee2) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(dirigeant.annee3) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
        ]);
    }
    result.push([
        { text: 'Charges Patronales', style: ['bolded', 'subheader', 'lefted', 'backgroundGreyRecap'], colSpan: 5, border: [false, false, false, false] }, {}, {},
        {}, {},
        {
            text: PdfModel.contentFormat(ssTT['chargeSocialeD'].annee1) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(ssTT['chargeSocialeD'].annee2) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(ssTT['chargeSocialeD'].annee3) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
    ]);
    return result;
}
function buildArrayDA(TTda, ssTT, da) {
    let daHeader = [
        { text: 'Dotation aux amortissements', style: ['bolded', 'subheader', 'lefted', 'backgroundPink'], colSpan: 5, border: [false, false, false, false] }, {}, {},
        {}, {},
        {
            text: PdfModel.contentFormat(TTda.annee1) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundPink'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(TTda.annee2) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundPink'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(TTda.annee3) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundPink'], colSpan: 2, border: [false, false, false, false]
        }, {},
    ];
    let result = new Array(daHeader);
    result.push([
        { text: 'Immobilisations incorporelles', style: ['bolded', 'subheader', 'lefted', 'backgroundGreyRecap'], colSpan: 5, border: [false, false, false, false] }, {}, {},
        {}, {},
        {
            text: PdfModel.contentFormat(ssTT['immoIncorp'].annee1) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(ssTT['immoIncorp'].annee2) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(ssTT['immoIncorp'].annee3) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
    ]);
    for (let immo of da['immoIncorp']) {
        result.push([
            { text: immo.titre, style: ['itemrighted', 'lefted'], colSpan: 5, border: [false, false, false, false] }, {}, {},
            {}, {},
            {
                text: PdfModel.contentFormat(immo.annee1) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(immo.annee2) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(immo.annee3) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
        ]);
    }
    result.push([
        { text: 'Matériel informatique', style: ['bolded', 'subheader', 'lefted', 'backgroundGreyRecap'], colSpan: 5, border: [false, false, false, false] }, {}, {},
        {}, {},
        {
            text: PdfModel.contentFormat(ssTT['informatique'].annee1) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(ssTT['informatique'].annee2) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(ssTT['informatique'].annee3) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
    ]);
    for (let informatique of da['informatique']) {
        result.push([
            { text: informatique.titre, style: ['itemrighted', 'lefted'], colSpan: 5, border: [false, false, false, false] }, {}, {},
            {}, {},
            {
                text: PdfModel.contentFormat(informatique.annee1) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(informatique.annee2) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(informatique.annee3) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
        ]);
    }
    result.push([
        { text: 'Mobilier', style: ['bolded', 'subheader', 'lefted', 'backgroundGreyRecap'], colSpan: 5, border: [false, false, false, false] }, {}, {},
        {}, {},
        {
            text: PdfModel.contentFormat(ssTT['mobilier'].annee1) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(ssTT['mobilier'].annee2) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(ssTT['mobilier'].annee3) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
    ]);
    for (let mobilier of da['mobilier']) {
        result.push([
            { text: mobilier.titre, style: ['item', 'lefted'], colSpan: 5, border: [false, false, false, false] }, {}, {},
            {}, {},
            {
                text: PdfModel.contentFormat(mobilier.annee1) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(mobilier.annee2) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(mobilier.annee3) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
        ]);
    }
    result.push([
        { text: 'Materiel/Machine/Outils', style: ['bolded', 'subheader', 'lefted', 'backgroundGreyRecap'], colSpan: 5, border: [false, false, false, false] }, {}, {},
        {}, {},
        {
            text: PdfModel.contentFormat(ssTT['materiel'].annee1) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(ssTT['materiel'].annee2) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(ssTT['materiel'].annee3) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
    ]);
    for (let materiel of da['materiel']) {
        result.push([
            { text: materiel.titre, style: ['item', 'lefted'], colSpan: 5, border: [false, false, false, false] }, {}, {},
            {}, {},
            {
                text: PdfModel.contentFormat(materiel.annee1) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(materiel.annee2) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(materiel.annee3) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
        ]);
    }
    result.push([
        { text: 'Batiment/Local/Espace de vente', style: ['bolded', 'subheader', 'lefted', 'backgroundGreyRecap'], colSpan: 5, border: [false, false, false, false] }, {}, {},
        {}, {},
        {
            text: PdfModel.contentFormat(ssTT['batiment'].annee1) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(ssTT['batiment'].annee2) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(ssTT['batiment'].annee3) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
    ]);
    for (let batiment of da['batiment']) {
        result.push([
            { text: batiment.titre, style: ['itemrighted', 'lefted'], colSpan: 5, border: [false, false, false, false] }, {}, {},
            {}, {},
            {
                text: PdfModel.contentFormat(batiment.annee1) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(batiment.annee2) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(batiment.annee3) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
        ]);
    }
    result.push([
        { text: 'Moyens roulants', style: ['bolded', 'subheader', 'lefted', 'backgroundGreyRecap'], colSpan: 5, border: [false, false, false, false] }, {}, {},
        {}, {},
        {
            text: PdfModel.contentFormat(ssTT['voiture'].annee1) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(ssTT['voiture'].annee2) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(ssTT['voiture'].annee3) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundGreyRecap'], colSpan: 2, border: [false, false, false, false]
        }, {},
    ]);
    for (let voiture of da['voiture']) {
        result.push([
            { text: voiture.titre, style: ['itemrighted', 'lefted'], colSpan: 5, border: [false, false, false, false] }, {}, {},
            {}, {},
            {
                text: PdfModel.contentFormat(voiture.annee1) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(voiture.annee2) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(voiture.annee3) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
        ]);
    }
    return result;
}
function buildArrayPF(pf,items

) {
    let daHeader = [
        { text: 'Produits financiers', style: ['bolded', 'subheader', 'lefted', 'backgroundPink'], colSpan: 5, border: [false, false, false, false] }, {}, {},
        {}, {},
        {
            text: PdfModel.contentFormat(pf.annee1) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundPink'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(pf.annee2) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundPink'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(pf.annee3) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundPink'], colSpan: 2, border: [false, false, false, false]
        }, {},
    ];
    let result = new Array(daHeader);

    for (let item of items) {
        result.push([
            { text: item.nom, style: ['item', 'lefted'], colSpan: 5, border: [false, false, false, false] }, {}, {},
            {}, {},
            {
                text: PdfModel.contentFormat(item.annee1) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(item.annee2) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(item.annee3) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
        ]);
    }
    return result;
}
function buildArrayCF(TTcf, cf,tva) {
    let daHeader = [
        { text: 'Charges financières', style: ['bolded', 'subheader', 'lefted', 'backgroundPink'], colSpan: 5, border: [false, false, false, false] }, {}, {},
        {}, {},
        {
            text: PdfModel.contentFormat(TTcf.annee1) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundPink'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(TTcf.annee2) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundPink'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(TTcf.annee3) + ' ' + money, style: ['bolded', 'subheader', 'righted', 'backgroundPink'], colSpan: 2, border: [false, false, false, false]
        }, {},
    ];
    let result = new Array(daHeader);
    //banque
    for (let banque of cf['banque']) {
        result.push([
            { text: banque.creancier, style: ['item', 'lefted'], colSpan: 5, border: [false, false, false, false] }, {}, {},
            {}, {},
            {
                text: PdfModel.contentFormat(banque.remboursement.annee1.interet) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(banque.remboursement.annee2.interet) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(banque.remboursement.annee3.interet) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
        ]);
    }
    //microfinance
    for (let microfinance of cf['microfinance']) {
        result.push([
            { text: microfinance.creancier, style: ['item', 'lefted'], colSpan: 5, border: [false, false, false, false] }, {}, {},
            {}, {},
            {
                text: PdfModel.contentFormat(microfinance.remboursement.annee1.interet) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(microfinance.remboursement.annee2.interet) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(microfinance.remboursement.annee3.interet) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
        ]);
    }
    //subvention Banque
    for (let subvention of cf['subventionB']) {
        result.push([
            { text: subvention.fournisseur, style: ['itemrighted', 'lefted'], colSpan: 5, border: [false, false, false, false] }, {}, {},
            {}, {},
            {
                text: PdfModel.contentFormat(subvention.remboursement.annee1.interet) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(subvention.remboursement.annee2.interet) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(subvention.remboursement.annee3.interet) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
        ]);
    }
    //subvention Institution
    for (let subvention of cf['subventionI']) {
        result.push([
            { text: subvention.fournisseur, style: ['item', 'lefted'], colSpan: 5, border: [false, false, false, false] }, {}, {},
            {}, {},
            {
                text: PdfModel.contentFormat(subvention.remboursement.annee1.interet) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(subvention.remboursement.annee2.interet) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(subvention.remboursement.annee3.interet) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
        ]);
    }
    //subvention Autre
    for (let subvention of cf['subventionA']) {
        result.push([
            { text: subvention.fournisseur, style: ['item', 'lefted'], colSpan: 5, border: [false, false, false, false] }, {}, {},
            {}, {},
            {
                text: PdfModel.contentFormat(subvention.remboursement.annee1.interet) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(subvention.remboursement.annee2.interet) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
            {
                text: PdfModel.contentFormat(subvention.remboursement.annee3.interet) + ' ' + money, style: ['item', 'righted'], colSpan: 2, border: [false, false, false, false]
            }, {},
        ]);
    }
    return result;
}
function buildArrayResultat(item, name) {

    let color1 = (item.annee1 < 0) ? 'backgroundRed' : 'backgroundGreen';
    let color2 = (item.annee2 < 0) ? 'backgroundRed' : 'backgroundGreen';
    let color3 = (item.annee3 < 0) ? 'backgroundRed' : 'backgroundGreen';
    let resHeader = [
        { text: name.toUpperCase(), style: ['bolded', 'subheader', 'lefted', 'backgroundGrey', 'whiteColor'], colSpan: 5, border: [false, false, false, false] }, {}, {}, {}, {},
        {
            text: PdfModel.contentFormat(item.annee1) + ' ' + money, style: ['bolded', 'subheader', 'centered', color1, 'whiteColor'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(item.annee2) + ' ' + money, style: ['bolded', 'subheader', 'centered', color2, 'whiteColor'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(item.annee3) + ' ' + money, style: ['bolded', 'subheader', 'centered', color3, 'whiteColor'], colSpan: 2, border: [false, false, false, false]
        }, {},
    ];
    let result = new Array(resHeader);
    return result;
}
function buildArrayResultatFinancier(item) {

    let color1 = (item.annee1 < 0) ? 'backgroundRed' : 'backgroundGreen';
    let color2 = (item.annee2 < 0) ? 'backgroundRed' : 'backgroundGreen';
    let color3 = (item.annee3 < 0) ? 'backgroundRed' : 'backgroundGreen';
    let resHeader = [
        { text: 'RESULTAT FINANCIER', style: ['bolded', 'subheader', 'lefted', 'backgroundGrey', 'whiteColor'], colSpan: 5, border: [false, false, false, false] }, {}, {}, {}, {},
        {
            text: PdfModel.contentFormat(item.annee1) + ' ' + money, style: ['bolded', 'subheader', 'centered', color1, 'whiteColor'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(item.annee2) + ' ' + money, style: ['bolded', 'subheader', 'centered', color2, 'whiteColor'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(item.annee3)+ ' ' + money, style: ['bolded', 'subheader', 'centered', color3, 'whiteColor'], colSpan: 2, border: [false, false, false, false]
        }, {},
    ];
    let result = new Array(resHeader);
    return result;
}
function buildArrayResultatImpots(item, name) {

    let resHeader = [
        { text: name.toUpperCase(), style: ['bolded', 'subheader', 'lefted', 'backgroundGrey', 'whiteColor'], colSpan: 5, border: [false, false, false, false] }, {}, {}, {}, {},
        {
            text: PdfModel.contentFormat(item.annee1) + ' ' + money, style: ['bolded', 'subheader', 'centered', 'backgroundGrey', 'whiteColor'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(item.annee2) + ' ' + money, style: ['bolded', 'subheader', 'centered', 'backgroundGrey', 'whiteColor'], colSpan: 2, border: [false, false, false, false]
        }, {},
        {
            text: PdfModel.contentFormat(item.annee3) + ' ' + money, style: ['bolded', 'subheader', 'centered', 'backgroundGrey', 'whiteColor'], colSpan: 2, border: [false, false, false, false]
        }, {},
    ];
    let result = new Array(resHeader);
    return result;
}
export function content(projet, years, totaux, elements, paramsMoney, meta, mode = "only") {
    money = paramsMoney['currency']
    precision = paramsMoney['precision']
    return [
        {
            columns: [
                ...(meta.userLogo != '' && mode == "only" ? [{
                    width: '30%',
                    stack: [
                        {
                            image: meta.userLogo,
                            alignment: 'right',
                            height: 100,
                            width: 100
                        }
                    ],
                    margin: [0, -32, 0, 0]
                }] : []),
                ...(meta.logoEntreprise && meta.logoEntreprise != '' && mode == "only" ? [{
                    width: '30%',
                    stack: [
                        {
                            image: meta.logoEntreprise,
                            alignment: 'right',
                            height: 100,
                            width: 100
                        }
                    ],
                    margin: [0, -32, 0, 0]
                }] : []),

            ],
            columnGap: 4,
        },
        { text: 'Compte de Résultats prévisionnels sur 3 ans', style: ['bolded', 'bigHeader'], alignment: 'center', margin: [30, 15, 0, 22] },
        ...(mode == "only" ? [
            { text: 'Projet : ' + projet.projettitre, style: 'header', margin: [0, 0, 0, 10] }, { text: '', margin: [0, 0, 0, 10] }] : []),
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayYears(years),
                layout: 'noBorders'
            }, margin: PdfModel.defaultMargin()
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayCAE(totaux['cae']),
                layout: 'noBorders'
            }, margin: PdfModel.defaultMargin()
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayVentes(totaux['cae'], elements['articles']),
                layout: 'noBorders'
            }, margin: PdfModel.defaultMargin()
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayCE(totaux['charges']),
                layout: 'noBorders'
            }, margin: PdfModel.defaultMargin()
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayAchats(totaux['achats'], totaux['depensesVariables'], elements['depenses']),
                layout: 'noBorders'
            }, margin: PdfModel.defaultMargin()
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArraySE(totaux['se'], elements['services']),
                layout: 'noBorders'
            }, margin: PdfModel.defaultMargin()
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayImpotsTaxes(totaux['taxes'], totaux['sTTtaxes'], elements['taxes']),
                layout: 'noBorders'
            }, margin: PdfModel.defaultMargin()
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayFP(totaux['fp'], totaux['sTTfp'], elements['fp']),
                layout: 'noBorders'
            }, margin: PdfModel.defaultMargin()
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayDA(totaux['da'], totaux['sTTda'], elements['da']),
                layout: 'noBorders'
            }, margin: PdfModel.defaultMargin()
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayResultat(elements['exploitattion'], 'Résultat d\'exploitation'),
                layout: 'noBorders'
            }, margin: PdfModel.defaultMargin()
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayPF(totaux['pf'], elements['pFinancier']),
                layout: 'noBorders'
            }, margin: [0, 0, 0, 5]
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayCF(totaux['cf'], elements['cf'], totaux['activeTva']),
                layout: 'noBorders'
            }, margin: [0, 0, 0, 5]
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayResultatFinancier(totaux['rFinancier']),
                layout: 'noBorders'
            }, margin: PdfModel.defaultMargin()
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayResultat(elements['courantImpot'], 'Résultat courant avant Impôts'),
                layout: 'noBorders'
            }, margin: [0, 0, 0, 5]
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayResultat(elements['imprevus'], "Résultat Exceptionnel"),
                layout: 'noBorders'
            }, margin: [0, 0, 0, 5]
        },
   
    
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayResultatImpots(elements['beneficeImpot'], "Impôt sur les bénéfices ('" + totaux["impotFiscal"] + "')"),
                layout: 'noBorders'
            }, margin: [0, 0, 0, 5]
        },
        {   
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayResultat(elements['exercice'], "Résultat d\'exercice"),
                layout: 'noBorders'
            }, margin: [0, 0, 0, 5]
        },
        {
            table: {
                headerRows: 0,
                widths: arrayColumns,
                body: buildArrayResultat(elements['autofinance'], "Capacité d'autofinancement"),
                layout: 'noBorders'
            },margin: [0, 0, 0, 5]
        },
    ]
}
