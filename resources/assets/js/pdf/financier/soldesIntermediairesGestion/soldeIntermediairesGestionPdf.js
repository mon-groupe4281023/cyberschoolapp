import PdfModel from '../../PdfModel'
import {content} from './content'
import doc from '../../docDefinitionModel'


class SoldeIntermediairesGestionPdf extends PdfModel {

    /**
     *
     * @param base
     * @param base.title {String} Titre du document
     * @param base.data {Object} Ensemble de données
     * @param options {Object} - Autres informations sur le Document
    */
    constructor (base = {}, options = {}) {
        super(base, options)
    }

    docDefinition () {
        const docDefinition = {
            footer: (currentPage) => { return doc.footerFinancier(currentPage, this.options['meta'].infoJuridiques)},
            background: () => { return doc.backgroundBp(this.options['meta'].userLogo)},
            // userPassword: this.options['password'],
            ownerPassword: 'cbscodeteam',
            permissions: {
                printing: 'highResolution', //'lowResolution'
                modifying: false,
                copying: false,
                annotating: true,
                fillingForms: true,
                contentAccessibility: true,
                documentAssembly: true
            }
        }
        return super.docDefinition(docDefinition);
    }

    defaultStyle () {
        return {
            fontSize: 11.2,
            bold: false,
            alignment: 'left',
            // font: this.options['meta'].police
        }
    }

    styles () {
        return Object.assign({}, super.styles(), {
            centered:{
                alignment: 'center'
            },
            righted:{
                alignment: 'right'
            },
            header: {
                fontSize: 15,
                bold: true,
                alignment: 'justify'
            },
            bigHeader: {
                fontSize: 18,
                bold: true,
                decoration: 'underline',
                alignment: 'center'
            },
            subheader: {
                fontSize: 11,
                bold: true,
            },
            quote: {
                italics: true,
                alignment: 'justify'
            },
            small: {
                fontSize: 9,
                alignment: 'justify'
            },
            bigger: {
                fontSize: 13,
                italics: true,
                alignment: 'justify'
            },
            tableHeader: {
                alignment: 'left',
                bold:true
            },
            bolded: {
                bold: true
            },
            italiced: {
                italics: true,
            },
            whiteColor: {
                color: 'white'
            },
            backgroundBlueRecap: {
                fillColor: '#36a2eb'
            },
        })
    }

    getTitlePdf () {
        return `${this.data.projet.projettitre}_${this.title}.pdf`.toLowerCase()
    }

    content () {
        return content(this.data.projet, this.data.solde, this.data.paramsMoney, this.data.years, this.options.meta)
    }
}

export default SoldeIntermediairesGestionPdf
