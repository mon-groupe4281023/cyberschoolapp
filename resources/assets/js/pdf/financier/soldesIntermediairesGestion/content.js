import {arround} from '../../../services/helper'
import PdfModel from '../../PdfModel'

var money = '';
var precision = '';

function buildArrayYears (years) {

    let yearsHeader = [
        {text: 'Années d\'exercices', style:['bolded','subheader','lefted',], border:[false,false,false,false]},
        {
            text: years.annee1, style:['bolded','subheader','centered',], border:[false,false,false,false]
        },
        {
            text: years.annee2, style:['bolded','subheader','centered',], border:[false,false,false,false]
        },
        {
            text: years.annee3, style:['bolded','subheader','centered',], border:[false,false,false,false]
        },
    ];
    let result = new Array(yearsHeader);
    return result;
}
function buildArraySoldeIG (soldes) {
    let result = [];
    for (let solde in soldes) {
        result.push([
            {text: soldes[solde].titre, style:['bolded','subheader','lefted', 'backgroundBlueRecap', 'whiteColor'], border:[false,false,false,false]},
            {
                text: PdfModel.contentFormat(arround(soldes[solde].annee1, precision)) + ' ' + money, style:['backgroundBlueRecap', 'whiteColor', 'bolded','subheader','righted',], border:[false,false,false,false]
            },
            {
                text: PdfModel.contentFormat(arround(soldes[solde].annee2, precision)) + ' ' + money, style:['backgroundBlueRecap', 'whiteColor', 'bolded','subheader','righted',], border:[false,false,false,false]
            },
            {
                text: PdfModel.contentFormat(arround(soldes[solde].annee3, precision)) + ' ' + money, style:['backgroundBlueRecap', 'whiteColor', 'bolded','subheader','righted',], border:[false,false,false,false]
            },
        ]);
        let childs = soldes[solde].children;

        for (let child in childs) {
            result.push([
                {text: childs[child].titre, style:['lefted', 'small'], border:[false,false,false,false]},
                {
                    text: PdfModel.contentFormat(arround(childs[child].annee1, precision)) + ' ' + money, style:['righted', 'small'], border:[false,false,false,false]
                },
                {
                    text: PdfModel.contentFormat(arround(childs[child].annee2, precision)) + ' ' + money, style:['righted', 'small'], border:[false,false,false,false]
                },
                {
                    text: PdfModel.contentFormat(arround(childs[child].annee3, precision)) + ' ' + money, style:['righted', 'small'], border:[false,false,false,false]
                },
            ]);
        }
    }
    return result;
}
export function content (projet, soldes, params, years, meta){
    // implementation du pdf
    money = params['currency'];
    precision = params['precision'];
   return [
        // en-tete du doc
        {
            columns: [
             
                ...(meta.userLogo!=''?   [{
                    width: '30%',
                    stack: [
                        {
                            image:meta.userLogo,
                            alignment: 'right',
                            height: 100,
                            width: 100
                        }
                    ],
                    margin:[5,-22,0,0]
                }] : [] ),
                ...(meta.logoEntreprise && meta.logoEntreprise!=''?   [{
                    width: '30%',
                    stack: [
                        {
                            image:meta.logoEntreprise,
                            alignment: 'right',
                            height: 100,
                            width: 100
                        }
                    ],
                    margin:[0,0,0,0]
                }] : [] ),
            ],
            columnGap: 4,
        },
        { text: 'Solde imtermédiaire de gestion', style:['bolded', 'bigHeader'], alignment: 'center'},
        {text:'Projet : '+ projet.projettitre, style: 'header',margin: [0,15,0,15]},
        {
            table: {
                headerRows: 0,
                widths: ['*', '*', '*', '*'],
                body: buildArrayYears(years),
                layout:'noBorders'
            },
        },{
            table: {
                headerRows: 0,
                widths: ['*', '*', '*', '*'],
                body: buildArraySoldeIG(soldes),
                layout:'noBorders'
            },
        },
    ]
}
