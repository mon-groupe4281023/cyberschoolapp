import PdfModel from '../../PdfModel'

var money = '';
var precision = '';

function buildArrayYears() {
    let yearsHeader = [
        {text: 'Années d\'exercices', style:['bolded','subheader','lefted',], border:[false,false,false,false]},
        {text: 'Durée', style:['bolded','subheader','centered'], border:[false,false,false,false]},
        {text: 'Année 1', style:['bolded','subheader','righted'], border:[false,false,false,false]},
        {text: 'Année 2', style:['bolded','subheader','righted'], border:[false,false,false,false]},
        {text: 'Année 3', style:['bolded','subheader','righted'], border:[false,false,false,false]},
    ];
    return new Array(yearsHeader);
}
function buildArrayImmoCA (elements) {
    let result = [];
    elements.forEach(element => {
        let style = [];
        if (element.amortissement === '') {
            style = ['backgroundBlueRecap','bolded','subheader', 'whiteColor'];
        } else {
            style = ['backgroundBlueLight', 'downHeader']
        }
        result.push([
            {text: element.titre, style: style.concat(['lefted']), border:[false,false,false,false]},
            {text: element.amortissement, style: style.concat(['centered']), border:[false,false,false,false]},
            {text: PdfModel.contentFormat(element.annee1) + ' ' + money, style:style.concat(['righted']), border:[false,false,false,false]},
            {text: PdfModel.contentFormat(element.annee2) + ' ' + money, style:style.concat(['righted']), border:[false,false,false,false]},
            {text: PdfModel.contentFormat(element.annee3) + ' ' + money, style:style.concat(['righted']), border:[false,false,false,false]}
        ]);
        if (element.children) {
            for (let child of element.children) {
                result.push([
                    {text: child.titre, style:['lefted', 'small'], border:[false,false,false,false]},
                    {text: child.amortissement + ' ans', style:['centered', 'small'], border:[false,false,false,false]
                    },
                    {text:  PdfModel.contentFormat(child.annee1) + ' ' + money, style:['righted', 'small'], border:[false,false,false,false]
                    },
                    {text:  PdfModel.contentFormat(child.annee2)  + ' ' + money, style:['righted', 'small'], border:[false,false,false,false]
                    },
                    {text:  PdfModel.contentFormat(child.annee3)  + ' ' + money, style:['righted', 'small'], border:[false,false,false,false]
                    }
                ]);
            }
        }
    });
    return result;
}
export function content(projet, paramsMoney, elements, meta){
    // implementation du pdf
    money = paramsMoney['currency'];
    precision = paramsMoney['precision'];
    return [
        {
            columns: [
             
                ...(meta.userLogo!=''?   [{
                    width: '30%',
                    stack: [
                        {
                            image:meta.userLogo,
                            alignment: 'right',
                            height: 100,
                            width: 100
                        }
                    ],
                    margin:[5,-22,0,0]
                }] : [] ),
                ...(meta.logoEntreprise && meta.logoEntreprise!=''?   [{
                    width: '30%',
                    stack: [
                        {
                            image:meta.logoEntreprise,
                            alignment: 'right',
                            height: 100,
                            width: 100
                        }
                    ],
                    margin:[0,0,0,0]
                }] : [] ),
            ],
            columnGap: 4,
        },
        { text: 'Amortissements (HT) sur 3 ans', style:['bolded', 'bigHeader'], alignment: 'center', margin: [0,0,0,25]},
        {text:'Projet : '+ projet.projettitre, style: 'header', margin: [0,0,0,25]},
        { text:'', },
        {
            table: {
                headerRows: 0,
                widths: ['25%', '10%', '*', '*', '*'],
                body: buildArrayYears(),
                layout:'noBorders'
            }
        },
        {
            table: {
                headerRows: 0,
                widths: ['25%', '10%', '*', '*', '*'],
                body: buildArrayImmoCA(elements),
                layout:'noBorders'
            }
        }
    ]
}
