import PdfModel from '../../PdfModel'
import {content} from './content'
import doc from '../../docDefinitionModel'


class DetailsAmortissementsPdf extends PdfModel {

    /**
     *
     * @param base
     * @param base.title {String} Titre du document
     * @param base.data {Object} Ensemble de données
     * @param options {Object} - Autres informations sur le Document
    */
    constructor (base = {}, options = {}) {
        super(base, options)
    }

    docDefinition () {
        const docDefinition = {
            footer: (currentPage) => { return doc.footerFinancier(currentPage, this.options['meta'].infoJuridiques)},
            background: () => { return doc.backgroundBp(this.options['meta'].userLogo)},
            userPassword: this.options['password'],
            ownerPassword: 'cbscodeteam',
            permissions: {
                printing: 'highResolution', //'lowResolution'
                modifying: false,
                copying: false,
                annotating: true,
                fillingForms: true,
                contentAccessibility: true,
                documentAssembly: true
            }
        }
        return super.docDefinition(docDefinition);
    }

    defaultStyle () {
        return {
            fontSize: 11.2,
            bold: false,
            alignment: 'left',
            // font: this.options['meta'].police
        }
    }

    styles () {
        return Object.assign({}, super.styles(), {
            centered:{
                alignment: 'center'
            },
            lefted:{
                alignment: 'left'
            },
            righted:{
                alignment: 'right'
            },
            downHeader: {
                fontSize: 10.8
            },
            bigHeader: {
                fontSize: 18,
                bold: true,
                decoration: 'underline',
                alignment: 'justify'
            },
            subheader: {
                fontSize: 11,
                alignment: 'justify'
            },
            quote: {
                italics: true,
                alignment: 'justify'
            },
            small: {
                fontSize: 9.5,
            },
            bigger: {
                fontSize: 13,
                italics: true,
                alignment: 'justify'
            },
            tableHeader: {
                alignment: 'left',
                bold:true
            },
            bolded: {
                bold: true
            },
            italiced: {
                italics: true,
            },
            whiteColor: {
                color: 'white'
            },
            blackColor: {
                color: 'black'
            },
            redColor: {
                color: '#fb3a3a'
            },
            backgroundBlueRecap: {
                fillColor: '#4c8caa'
            },
            backgroundBlueLight: {
                fillColor: '#91bdd1'
            },
            backgroundGreen: {
                fillColor: '#00ba8b'
            },
            backgroundGrey:{
                fillColor:'#99abb4'
            },
            backgroundRed: {
                fillColor: '#f13c42'
            },
            backgroundPink: {
                fillColor: '#f0d4d6'
            },
            backgroundGreyRecap: {
                fillColor: '#f3f1f1'
            },
            backgroundDarkBlueRecap: {
                fillColor: '#398bf7'
            },
            backgroundLightBlueRecap: {
                fillColor: '#2bbbff'
            },
            backgroundLightGreen: {
                fillColor: '#e1f2de'
            },
            backgroundTableauStandard: {
                fillColor: '#398bf7'
            }

        })
    }

    getTitlePdf () {
        return `${this.data.projet.projettitre}_${this.title}.pdf`.toLowerCase()
    }

    content () {
        return content(this.data.projet, this.data.paramsMoney, this.data.elements,  this.options.meta)
    }
}

export default DetailsAmortissementsPdf
