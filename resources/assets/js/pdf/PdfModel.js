import pdfMake from 'pdfmake/build/pdfmake'
import pdfFonts from "pdfmake/build/vfs_fonts"
pdfMake.vfs = pdfFonts.pdfMake.vfs;
let  fontsList = import("./fonts")

class PdfModel {

    /**
     *
     * @param base
     * @param base.title {String} Titre du document
     * @param base.data {Object} Ensemble de données
     * @param options {Object} - Autres informations sur le Document
     */
    constructor(base = {}, options = {}) {
        this.options = options;
        this.data = base.data;
        this.couleur=base.data.couleur;
        this.title = base.title;
    }
    /**
     * Initialisation du document
     * @param docDefinition element de config du doc exemple: l'orientation du doc
     * @returns {Object}
     */
    docDefinition(docDefinition = {}) {
        return Object.assign({}, {
            info: {
                title: this.getTitlePdf().toUpperCase(),
                author: 'CYBERSCHOOL ENTREPREUNEURIAT',
                subject: '',
                keywords: '',
                creator: 'CYBERSCHOOL ENTREPREUNEURIAT',
                producer: 'CYBERSCHOOL ENTREPREUNEURIAT'
            },
            defaultStyle: this.defaultStyle(),
            styles: this.styles(),
            footer: this.getDefaultFooter(),
            content: this.content()
        }, docDefinition)
    }

    stripTags(html) {
       
        if (arguments.length < 3) {
            // On enleve le TAG d'ouverture <p>
            html = html.replace(/<[^\/]*>/gi, '');
            // On enleve le TAG dE fermeture </p>
            html = html.replace(/<\/(?!\!)[^>]*>/gi, '\n');
        } else {
            let allowed = arguments[1];
            let specified = eval("[" + arguments[2] + "]");
            if (allowed) {
                let regex = '</?(?!(' + specified.join('|') + '))\b[^>]*>';
                html = html.replace(new RegExp(regex, 'gi'), '');
            } else {
                let regex = '</?(' + specified.join('|') + ')\b[^>]*>';
                html = html.replace(new RegExp(regex, 'gi'), '');
            }
        }
        return html;
    }

    /**
     *
     * @returns {String}
     */
    getTitlePdf() {
        return this.title + '.pdf'
    }

    /**
     * Style de Base d'un PDF
     */
    styles() {
        return {
            /* Background color */
            bgColorHeaderTab: {
                fillColor: this.couleur,
                color: 'white',
                fontSize: 10
            },
            bgColorLightBlue: {
                fillColor: '#F2F6FC',
                color: '#000'
            },
            bgColorGrey: {
                fillColor: '#B3C0D1',
                color: '#000'
            },
            bgColorLightGrey: {
                fillColor: '#E9EEF3',
                color: '#455a64'
            },
            bgColumnsTab: {
                fillColor: '#efefef'
            },
            /* Position */
            centered: {
                alignment: 'center'
            },
            lefted: {
                alignment: 'left'
            },
            bolded: {
                bold: true
            },
            righted: {
                alignment: 'right'
            },
            /* Size */
            normal: {
                fontSize: 9.5,
                font: 'Helvetica',
                bold: false
            },
            small: {
                fontSize: 7,
                bold: false,
                alignment: 'justify'
            },
            header: {
                fontSize: 13,
                bold: true,
                alignment: 'justify'
            },
            title: {
                fontSize: 15,
                bold: true,
                alignment: 'justify',
                font: 'Helvetica'
            },
            subHeaderBold: {
                fontSize: 10.2,
                bold: true
            },
            subHeaderLight: {
                fontSize: 11,
                bold: false
            },
            bigHeader: {
                fontSize: 18,
                bold: true,
                decoration: 'underline',
                alignment: 'center',
                font: 'Helvetica'
            },
            item: {
                fontSize: 8,
                alignment: 'right'
            },
            /* Others*/
            info: {
                fontSize: 10,
                bold: false
            },
            montant: {
                fontSize: 10,
                bold: false
            },
            devisNum: {
                fontSize: 10,
                bold: false,
                alignment: 'center'
            },
            titleMontant: {
                fontSize: 9.5,
                bold: false
            },
            sectionAmount: {
                fillColor: '#e4e7ed',
                bold: true,
                alignment: 'right'
            },
            sectionQte: {
                fillColor: '#e4e7ed',
                bold: true,
                alignment: 'left'
            },
            prestation: {
                fontSize: 12,
                bold: true,
                borderColor: [this.couleur],
               // borderWidth: 3,
            }
        }
    }

    /**
     * Style par défaut appliquer à tt les éléments d'un PDF
     */
    defaultStyle() {
        return {
            fontSize: 11.2,
            bold: false,
            alignment: 'left',
            font: 'Poppins'
        }
    }

    /**
     * Modifie le footer d'un PDF
     * @param {string} footerContent - contenu du nouveau footer
     * @param {array<number>} margin - Marge du contenu
     * @param {array<string>} style - Classe de style du contenu
     */
    setFooter(footerContent, margin = [50, 0, 0, 0], style = ['small']) {
        this.footer = {columns: [{text: footerContent, margin: margin, style: style}]}
    }

    /**
     * Footer par defaut, informations de CBS
     */
    getDefaultFooter() {
        return {
            columns: [
                {
                    text:
                        ``,
                    margin: [50, 0, 0, 0], style: ['small']
                }
            ]
        }
    }

    downloadPdf() {
        fontsList.then((value)=>{
            pdfMake.fonts = value.fonts;
            pdfMake.createPdf(this.docDefinition()).download(this.getTitlePdf())

        })
       
     
        
    }

    openPdf() {
        fontsList.then((value)=>{
            pdfMake.fonts = value.fonts;
               pdfMake.createPdf(this.docDefinition()).open(this.getTitlePdf())

        })
        
    }

    /**
     * Génère un tableau
     * @param columns {array}
     * @param body - {Array} - contenu représentant les lignes du tableau
     * @param options {object}
     * @param options.margins {array}
     * @param propTable {boolean} - Permet de definir si on construit un tableau
     * @returns {object}
     */
    static getArray(columns = ['*'], body = [], options = {margin: [50,0,0,22]}, propTable = true) {
        return (propTable) ? {
            table: {
                headerRows: 0,
                widths: columns,
                body: body,
                layout:'noBorders'
            }, margin: (options.hasOwnProperty('margin')) ? options.margin : []
        } : {
            headerRows: 0,
            widths: columns,
            body: body,
            layout:'noBorders'
        }
    }

    /**
     *
     * Déterminen le nombre de colonne d'un element (Array|Object) qui sera transformer en tableau
     * @param element {Array|Object}
     * @returns {number}
     */
    static getNbrColumns(element) {
        if (element.hasOwnProperty('length'))
            return element.length;
        let j = 0;
        for (const i in element) {
            if (element.hasOwnProperty(i)) {
                j += 1;
            }
        }
        return j;
    }

    /**
     * Formate la size des colonnes d'un tableau
     * @param nbrColumns
     * @param options [] - Position and Size Columns
     * @returns {Array}
     */
    getColumns(nbrColumns, options = []) {
        let columns = [];
        while (columns.length < nbrColumns) {
            let push;
            if (options.length > 0) {
                options.forEach((option, index, tabs) => {
                    if (option[0] === columns.length) {
                        columns[columns.length] = option[1];
                        tabs.splice(index, 1);
                        push = true
                    }
                })
            }
            if (push === undefined)
                columns[columns.length] = '*'
        }
        return columns;
    }

    /**
     * Construit une ligne en créant plusieurs cellules à partir d'un objet
     * @param tabs {object} - l'objet à parcourir
     * @param text {string} - propriété à utiliser dans l'object
     * @param options {object} - border, style ...
     * @returns {Array}
     */
    getBodyByColumns(tabs, text = null, options = {}) {
        let bodyContent = [];
        for (const tab in tabs) {
            if(tabs[tab].hasOwnProperty(text)) {
                let t = (text) ? tabs[tab][text] : tabs[tab]
                bodyContent.push(PdfModel.getCell(t, options))
            }
        }
        return bodyContent;
    }
    
    BodyByColumns (tabs, props = [], text = null, options = {}) {
        let bodyContent = []
        props.forEach(prop => {
            let t = (text) ? tabs[prop][text] : tabs[prop]
            bodyContent.push(PdfModel.getCell(t, options))
        })
        return bodyContent;
    }

    /**
     * Construit une ligne en créant plusieurs cellules à partir d'un tableau
     * @param tabs {Array} - Tableau d'objet | Tableau de String
     * @param text {null} - propriété à utiliser dans l'object
     * @param options {object} - border, style ...
     * @returns {Array}
     */
    getBodyArrayByColumns(tabs, text = null, options = {}) {
        let bodyContent = [];
        tabs.forEach(tab => {
            let t = (text) ? tab[text]: tab
            bodyContent.push(PdfModel.getCell(t, options))
        });
        console.log("Body content", bodyContent);
        return bodyContent;
    }

    /**
     * Créer une cellule d'un tableau
     * @param text {string} contenu de la cellule
     * @param options {object} (style, border, margin, type)
     * @param visible {boolean}
     * @returns {object} definition d'une cellule
     */
    static getCell(text, options = {}, visible = true) {
        let type = (options.hasOwnProperty('type')) ? options.type.name : ''
        const border = [false, false, false, false]
        const colspan = 1
        if (!visible) {
            text = ''
            if (options.hasOwnProperty('type'))
                type = ''
        }
        return {
            text: (type.length > 0) ? PdfModel.contentFormat(text, type) : text,
            style: (options.hasOwnProperty('style')) ? options.style : [],
            colSpan: (options.hasOwnProperty('colspan')) ? options.colspan : colspan,
            borderColor: (options.hasOwnProperty('borderColor')) ? options.borderColor : [],
            border: (options.hasOwnProperty('border')) ? options.border : border,
            margin: (options.hasOwnProperty('margin')) ? options.margin : []
        }
    }

    /**
     * Formate un nombre en lui ajoutant une unité
     * @param text {String | Number} Nombre à formater
     * @param type {String} unnité
     * @returns {string}
     */
    static contentFormat(text, type = '') {
        return PdfModel.format(PdfModel.parseMontant(text))  + ' ' + type
    }

    /**
     *
     * @param ch
     * @return {number}
     */
    static parseMontant (ch) {
        ch = (ch == NaN || ch == undefined) ? 0 : ch
        return Math.round(parseFloat(ch))
    }

    /**
     * formate un nombre en mettant des espaces entre les milliers ex: 2000 => 2 000
     * @param {String | Number} ch chaine à formater
     * @param {String} separator Séparateur
     * @returns String
     */
    static format(ch, separator = ' ') {
        if (ch === undefined) {
            console.warn('champ undefined');
            return '';
        }
        let chh = [];
        let elt = [];
        ch = ch + '';
        if (ch.length === 0)
            return '';
        let tabs = ch.split('');
        if (ch.length <= 3)
            return ch;
        while ((tabs.length - 1) % 3 !== 0)
            elt.push(tabs.shift());
        tabs.forEach((item, index, tab) => {
            chh.push(item);
            if (index % 3 === 0 && index !== tab.length - 1)
                chh.push(separator);
        });
        return elt.join('') + chh.join('')
    }

    /**
     * formate un montant dans la monnaie paramétrée dans cuurencyParams
     * @param {Number} montant montant à formater
     * @param {Object} currencyParams Paramètre de la monaie
     * @param {String} currencyParams.currency symbole de la monaie
     * @param {Number} currencyParams.precision nombre de chiffe après le separateur décimal
     */
    static formatCurrency (montant, currencyParams) {
        let decimalPart = null
        if (currencyParams['decimal-separator']) {
            montant = Math.trunc(parseFloat(montant))
            decimalPart = montant % 1
        }
        montant = `${PdfModel.format(montant, currencyParams['thousand-separator'])}${currencyParams['decimal-separator'] ?? ''}`
        montant += `${decimalPart ?? ''}`
        return (currencyParams['currency-symbol-position'] === 'suffix') ? `${montant} ${currencyParams['currency']}` : `${currencyParams['currency']} ${montant}`
    }

    /**
     * Ensemble de style pour chaque colonne d'un tableau
     * @param styles {Object} - Style Générique à plusieurs colonne
     * @param otherOptions {Array} - Style particulier d'une colonne
     * @param col {Number} - Nombre de colonnes du tableau
     * @returns {[]} - Tableau contenant les styles de chaque colonne
     */
    getOptionsCell(styles, otherOptions = [], col) {
        let fullOptions = [];
        let i = 0;
        while (i < col) {
            let optionsRemove;
            if (otherOptions.length > 0) {
                otherOptions.forEach((other, index, tab) => {
                    if (i === other.position) {
                        if (other.hasOwnProperty('style')) {
                            fullOptions.push(other.style);
                            optionsRemove = tab.splice(index, 1);
                        }
                    }
                });
            }
            if (optionsRemove === undefined)
                fullOptions.push(styles);
            i++
        }
        return fullOptions
    }

    /**
     * Construit plusieurs lignes d'un tableau
     * @param tabs {Array} - le tableau à parcourir
     * @param columns {Array} - Les colonnes à afficher dans le tableau parcouru
     * @param options - border, style ...
     * @param sections {Array} sections du tableau
     * @returns {Array} - Tableau de tableaux représantant les lignes de ce dernier
     */
    getBodyArrayByLine(tabs, columns, options, sections = [[], []]) {
        let bodyContent = []
        tabs.forEach(tab => {
            const line = new Array(columns.length);
            if (tab.hasOwnProperty(sections[0][0])) {
                sections[0].forEach((section, index) => {
                    let optionsSections = sections[1][index], visible;
                    if (optionsSections.hasOwnProperty('visible'))
                        visible = tab[optionsSections.visible];
                    line[index] = PdfModel.getCell(tab[section], optionsSections, visible)
                })
            } else {
                columns.forEach((col, index) => {
                    line[index] = PdfModel.getCell(tab[col], options[index])
                });
            }
            bodyContent.push(line);
        })
        return bodyContent
    }

    /**
     * Construit une colonne de page contenant un text ou une image
     * @param content {Object}
     * @param content.type
     * @param content.content
     * @param styleContent - Style du contenu de la colonne
     * @param styleContent.position
     * @param styleContent.height
     * @param styleContent.width
     * @param styleContent.style
     * @param styleColumn
     * @param styleColumn.width
     * @param styleColumn.margin
     * @returns {
     * {stack: {image: *, width: *, alignment: *, height: *}[], margin: *, width: *}|
     * {margin: *, width: *, text: {width: *, text: {type}, alignment: *, height: *}[]}
     * }
     */
    getColumn(content = {}, styleColumn = {}, styleContent = {}) {
        if (content.hasOwnProperty('type')) {
            if (content.type === 'image' && content.content.length > 3) {
                return {
                    stack: [
                        {
                            image: content.content,
                            alignment: styleContent.position,
                            height: styleContent.height,
                            width: styleContent.width
                        }
                    ],
                    width: styleColumn.width,
                    margin: styleColumn.margin
                }
            }
        }
        return {
            text: [
                {
                    text: (content.content) ? content.content : '',
                    alignment: (styleContent.position) ? styleContent.position : ''
                }
            ],
            width: styleColumn.width,
            margin: styleColumn.margin
        }
    }

    static defaultMargin = () =>   [0, 0, 0, 22];
    

    
      
    /**
     * Content par défaut
     * @returns {Array}
     */
    content() {
        return []
    }
}

export default PdfModel
