const htmlToPdfmake = require("html-to-pdfmake");
class docDefinitionModel {

    static footerFinancier (currentPage,infoJuridiques, couleur, margin = [30,0,30,0], isPageNumber=true, style = ['small'], isRaw=true) {
        console.log("Couleur", couleur);
        console.log("isPageNumber", isPageNumber);
        var styles = { // change the default styles
            b: {bold:true},
            strong: {bold:true},
            u: {decoration:'underline'},
            s: {decoration: 'lineThrough'},
            em: {italics:true},
            i: {italics:true},
            h1: {fontSize:16, bold:false, marginBottom:0},
            h2: {fontSize:14, bold:false, marginBottom:0},
            h3: {fontSize:12, bold:false, marginBottom:0},
            h4: {fontSize:10, bold:false, marginBottom:0},
            h5: {fontSize:8, bold:false, marginBottom:0},
            h6: {fontSize:6, bold:false, marginBottom:0},
            a: {color:'blue', decoration:'underline'},
            strike: {decoration: 'lineThrough'},
            p: {margin:[0, 5, 0, 10],fontSize:6, bold:false},
            ul: {marginBottom:0},
            li: {marginLeft:0},
            table: {marginBottom:5},
            th: {bold:true, fillColor:'#EEEEEE'}
          }
        var infoJuridiques = htmlToPdfmake(infoJuridiques!=''?infoJuridiques:"", {defaultStyles:styles} );



        return {
            table: {
              widths: ['*', '5%'],
              layout: 'headerLineOnly',
  
              body: [
                [
                  {
                    border: [false, isRaw, false, false],
                    borderColor: [couleur, couleur, couleur, couleur],
                    stack: [infoJuridiques],
                    borderWidth: 0.5,
             
                  },
                  ...(isPageNumber?[   {
                    text: `\nPage ${currentPage}`,
                    style: style,
                    alignment: 'left',
                    border: [false, isRaw, false, false],
                    borderColor: [couleur, couleur, couleur, couleur],
                    borderWidth: 0.5,
            
                  }]:[])
               
                ]
              ]
            },
            margin: margin,
          }
          
    }

    static backgroundBp (images) {
        return {
            columns: [{
                width: '*',
                stack: [
                    {
                        image: images,
                        alignment: 'center',
                        height: 350,
                        width: 350,
                        opacity: 0.08
                    }
                ],
                margin:[0, 200,0,0]
            }]
        }
    }
}

export default docDefinitionModel
