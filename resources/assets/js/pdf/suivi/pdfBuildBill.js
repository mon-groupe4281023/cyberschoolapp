import PdfModel from "../PdfModel";
import doc from "../docDefinitionModel"
const htmlToPdfmake = require("html-to-pdfmake");

import {NumberToLetter} from "../../numToLetter"
const htmlStyle = {
  h1: {fontSize:16, bold:false, marginBottom:0},
  h2: {fontSize:14, bold:false, marginBottom:0},
  h3: {fontSize:12, bold:false, marginBottom:0},
  h4: {fontSize:10, bold:false, marginBottom:0},
  h5: {fontSize:8, bold:false, marginBottom:0},
  h6: {fontSize:6, bold:false, marginBottom:0},
}
const htmlStyleHeader = {
  h1: {fontSize:24, bold:false, marginBottom:0},
  h2: {fontSize:22, bold:false, marginBottom:0},
  h3: {fontSize:18, bold:false, marginBottom:0},
  h4: {fontSize:14, bold:false, marginBottom:0},
  h5: {fontSize:10, bold:false, marginBottom:0},
  h6: {fontSize:6, bold:false, marginBottom:0},
}
class PdfBuildBill extends PdfModel {
  constructor(base = {}, type = "Devis", options = []) {
    super(base, options);
    this.type = type;
    this.couleur=base.data.couleur;
    this.title = type +'-'+ options.code
  }

  docDefinition () {
    console.log("Options", this.options);
    const docDefinition = {
        pageOrientation: 'portrait',
        footer: (currentPage) => { return doc.footerFinancier(currentPage, this.options.footer, this.couleur,[30,-40,30,0], false,  ['small'], false)},
     
    }
    return super.docDefinition(docDefinition);
}
  /**
   * En tete du tableau des produits nom Entete, taille et style des colonnes enfants
   * @param elements - elements à retrouver dans le tableau
   * @returns {[][]} - [] tableau des entetes, tableau des taille, tableau des styles
   */
  headerProduct(elements) {
    // console.log("Options", this.options);
    let headerCellElement = [];
    let headerSize = [];
    let styleCellElement = [];
    let i = 0;
    let styles = {
      style: ["montant"],
      borderColor: ["#f5f7fa","#f5f7fa","#f5f7fa","#f5f7fa" ],
      border: [true, false, true, true],
      type: { name: "FCFA" },
    };
    elements.forEach((elt) => {
      if (elt.hasOwnProperty("libelle")) {
        switch (elt.libelle) {
          case "N°":
            headerCellElement.push("numero");
            headerSize.push([i, "5%"]);
            styleCellElement.push({
              position: i,
              style: {
                style: ["montant"],
                borderColor: ["#f5f7fa","#f5f7fa","#f5f7fa","#f5f7fa" ],
                border: [false, false, false, true],
              },
            });
            break;
          case "Désignation":
            headerCellElement.push("nom");
            break;
          case "Unité":
            headerCellElement.push("unite");
            headerSize.push([i, "10%"]);
            break;
          case "Quantité":
            headerCellElement.push("quantite");
            headerSize.push([i, "5%"]);
            styleCellElement.push({
              position: i,
              style: {
                style: ["montant","centered"],
                borderColor: ["#f5f7fa","#f5f7fa","#f5f7fa","#f5f7fa" ],
                border: [false, false, false, true],
              },
            });
            break;
          case "Prix de vente Unitaire HT":
            headerCellElement.push("prixVenteUnitaire");
            headerSize.push([i, "20%"]);
            styleCellElement.push({ position: i, style: {
              borderColor: ["#f5f7fa","#f5f7fa","#f5f7fa","#f5f7fa" ],
              border: [false, false, false, true],
              style: ["righted", "titleMontant"],
              type: {name:"FCFA"},
            } });
            break;
          case "Remise%":
            if (this.options.discount === "%") {
              headerCellElement.push("remise");
            } else {
              headerCellElement.push("montantRemise");
            }
            headerSize.push([i, "10%"]);
            styleCellElement.push({
              position: i,
              style: {
                style: ["righted", "titleMontant"],
                borderColor: ["#f5f7fa","#f5f7fa","#f5f7fa","#f5f7fa" ],
                 border: [false, false, false, true],
                type: { name: this.options.discount },
              },
            });
            break;
          case "TOTAL HT":
            headerCellElement.push("total");
            styleCellElement.push({ position: i, style: {
              borderColor: ["#f5f7fa","#f5f7fa","#f5f7fa","#f5f7fa" ],
              border: [false, false, false, true],
              style: ["righted", "titleMontant"],
              type: {name:"FCFA"},
            } });
            break;
          default:
            console.warn("Il manque un élément au Header");
        }
      }
      i++;
    });
    return [headerCellElement, headerSize, styleCellElement];
  }

  headerTableProduct(tab){
    let result = [];
      tab.forEach(elt=>{
        switch (elt.libelle) {
          case "N°":
            result.push({
              text: elt.libelle,
              style: ["bgColorHeaderTab"],
              borderColor: ["#DCDCDC", "#696969", "#DCDCDC", "#696969"],
              border: [false, false, false, false],
            })
            break;
          case "Désignation":
            result.push({
              text: elt.libelle,
              style: ["bgColorHeaderTab"],
              borderColor: ["#DCDCDC", "#696969", "#DCDCDC", "#696969"],
              border: [false, false, false, false],
            })
            break;
          case "Unité":
            result.push({
              text: elt.libelle,
              style: ["bgColorHeaderTab"],
              borderColor: ["#DCDCDC", "#696969", "#DCDCDC", "#696969"],
              border: [false, false, false, false],
            })
            break;
          case "Quantité":
            result.push({
              text:  'Qte',
              style: ["bgColorHeaderTab","centered"],
              borderColor: ["#DCDCDC", "#696969", "#DCDCDC", "#696969"],
              border: [false, false, false, false],
            })
            break;
          case "Prix de vente Unitaire HT":
            result.push({
              text: "Prix V.U (HT)",
              style: ["bgColorHeaderTab", 'righted'],
              borderColor: ["#DCDCDC", "#696969", "#DCDCDC", "#696969"],
              border: [false, false, false, false],
            })
            break;
          case "Remise%":
            result.push({
              text: elt.libelle,
              style: ["bgColorHeaderTab"],
              borderColor: ["#DCDCDC", "#696969", "#DCDCDC", "#696969"],
              border: [false, false, false, false],
            })
            break;
          case "TOTAL HT":
            result.push({
              text: 'Total (HT)',
              style: ["bgColorHeaderTab","righted"],
              borderColor: ["#DCDCDC", "#696969", "#DCDCDC", "#696969"],
              border: [false, false, false, false],
            })
        }
      })
      return result
  }

  /**
   * Champ des sections d'un tableau d'objets
   * @param nbr {Number} nombre de colonnes
   * @returns {any[][]} liste des colonnes à afficher
   */
  headerSection(nbr) {
    nbr = nbr - 1;
    const styles = {
      style: ["sectionQte"],
      border: [false, false, false, false],
    };
    let styleCellElement = new Array(nbr);
    const sections = new Array(nbr);
    sections[0] = "numeroSection";
    sections[1] = "titleSection";
    styleCellElement[0] = styleCellElement[1] = styles;
    let i = 2;
    while (i < nbr) {
      sections[i] = "";
      styleCellElement[i] = styles;
      i++;
    }
    sections[nbr] = "subTotal";
    styleCellElement[nbr] = {
      style: ["sectionAmount"],
      border: [false, false, false, false],
      type: { name: "FCFA" },
      visible: "subTotalIsVisible",
    };
    return [sections, styleCellElement];
  }

  /**
   *
   * @param text {String} - Titre du montant
   * @param montant {Number | String}
   * @param bg
   * @returns Array - Une ligne du tableau tslesmonahtmlStylents
   */
  montantFacture(text, montant, bg = "bgColorHeaderTab") {
    return [
      PdfModel.getCell(text, {
        style: ["lefted", "titleMontant" ,"bolded"],
        border: [true, true, true, true],
        borderColor: ["#f5f7fa","#f5f7fa","#f5f7fa","#f5f7fa" ],
      }),
      PdfModel.getCell(montant, {
        style: ["righted", bg, "montant", "bolded"],
        border: [true, true, true, true],
        borderColor: ["#f5f7fa","#f5f7fa","#f5f7fa","#f5f7fa" ],
        type: { name: "FCFA" },
      }),
    ];
  }

  allMontant(allMontant) {
    let results = [];
    for (let montant in allMontant) {
      switch (montant) {
        case "remiseTotalMontant":
          if (allMontant[montant].value) {
            results.push(
              this.montantFacture(
                "Remise Totale HT "+ allMontant[montant].percent+"%",
                allMontant[montant].value,
                "bgColorLightGrey"
              )
            );
          }
          break;
          case "autresTaxes":
            for (let item of allMontant[montant]) {
              results.push(
                this.montantFacture(
                  item.libelle+" : "+ item.valeur+"%",
                  item.taxable,
                  "bgColorLightGrey"
                )
              );
            }
  
            break;
 
        case "isVisible":
          if (allMontant[montant] === "HT") {
            results.push(
              this.montantFacture(
                "Total Facturé HT",
                allMontant["montantTotalHt"].value
              )
            );
          } else {
            results.push(
              this.montantFacture(
                "Total Facturé TTC",
                allMontant["montantTotalTtc"].value
              )
            );
          }
          break;
        case "isVisible3":
          if (allMontant[montant] === "TVA") {
            results.push(
              this.montantFacture(
                "TVA " + allMontant["taxesSelectTaux"].tva.taux + "%",
                parseInt(allMontant["tva"].montant),
                "bgColorLightGrey"
              )
            );
          } else {
            results.push(
              this.montantFacture(
                "TPS " + allMontant["taxesSelectTaux"].tps.taux + "%",
                parseInt(allMontant["tps"].montant),
                "bgColorLightGrey"
              )
            );
          }
          break;
   
        default:
      }
    }
    return results;
  }
  allMontantTotal(allMontant) {
    let results = [];
    for (let montant in allMontant) {
      switch (montant) {
   
        case "isVisible2":
          if (allMontant[montant] === "HT") {
            results.push(
              this.montantFacture(
                "Total à payer  HT",
                allMontant["montantTotalHt"].value
              )
            );
          } else {
            results.push(
              this.montantFacture(
                "Total à payer  TTC",
                allMontant["montantTotalTtc"].value
              )
            );
          }
          break;
        default:
      }
    }
    return results;
  }
  customer(data) {
    let result = [] 
    data.forEach((item)=>{
      result.push({
        text: item.inputName,
        border: [false, false, false, false], 
        fontSize:9
      })

    })
      return result;
  }
  /*
   * Contenu du PDF
   * @returns {*[]}
   */
  content() {
   // this.headerTableProduct(this.options.productsHeader)
    return [
      /** Entete **/
      {
        columns: [
          this.getColumn(
            { type: "image", content: this.logo},
            { width: "*", margin: [5, -15, 0, 12] },
            { position: "left",  width: 180 }
          ),
          this.getColumn(
            { content: this.type + " N°" + " : " + this.options.code },
            { width: "60%", margin: [98, 0, 0, 0] },
            { position: "right" }
          ),
        ],
        columnGap: 2,
      },
      /** General Info**/

      {
        columns: [
          {
            width: "70%",
            stack: [...htmlToPdfmake(this.options.mentions.infoEnt, { defaultStyles: htmlStyle })]
          },
          {
            position: "right",
            stack: [this.customer(this.data.infosClient)],
            margin: [0, 0, 80, 0] // adds a left margin of 10 pixels
          }
        ],
        columnGap: 2,
      },
      {
        columns: [
          {
            width: "100%",
            stack: [...htmlToPdfmake(this.options.extra, { defaultStyles: htmlStyleHeader })],
            position: "left",
            margin: [0, 0, 80, 0] // 
          },
      
        ],
      },
      {
        table: PdfModel.getArray(
          ["*"],
          [
            [
              {
                text: "",
                border: [false, false, false, false],
                margin: [0, 7],
              },
            ],
            [
              {
                text: "PRESTATION: " + this.data.titrePrestation.toUpperCase(),
                style: ["prestation", "lefted"],
                border: [true, false, false, false],

              },
            ],
            [
              {
                text: "",
                border: [false, false, false, false],
                margin: [0, 7],
              },
            ],
          ],
          {},
          false
        ),
      },

      /** Facture Info **/

      {
        table: PdfModel.getArray(
          this.getColumns(PdfModel.getNbrColumns(this.data.infosFacture)),
          [
            this.getBodyByColumns(this.data.infosFacture, "libelle", {
              style: ["bgColorHeaderTab"],
              borderColor: ["#DCDCDC", "#696969", "#DCDCDC", "#696969"],
              border: [false, false, false, false],
            }),
            this.getBodyByColumns(this.data.infosFacture, "content", {
              style: ["montant"],
              borderColor: ["#f5f7fa","#f5f7fa","#f5f7fa","#f5f7fa" ],
              border: [false, false, false, true],
            }),
          ],
          {},
          false
        ),
      },

      { text: "", margin: [0, 10] },
      /** Produits**/

      {
        table: PdfModel.getArray(
          this.getColumns(
            this.options.productsHeader.length,
            this.headerProduct(this.options.productsHeader)[1]
          ),
          [
            this.headerTableProduct(this.options.productsHeader)
          ].concat(
            this.getBodyArrayByLine(
              // Tableau de produits
              this.data.produits,
              //colonnes du tableau à afficher
              this.headerProduct(this.options.productsHeader)[0],
              //options de chaque cellule du tableau
              this.getOptionsCell(
                {
                  style: ["info"],
                  borderColor: ["#f5f7fa","#f5f7fa","#f5f7fa","#f5f7fa" ],
                  border: [false, false, false, true],
                },
                this.headerProduct(this.options.productsHeader)[2],
                PdfModel.getNbrColumns(this.options.productsHeader)
              ),
              //colonnes des sections à afficher
              this.headerSection(
                PdfModel.getNbrColumns(this.options.productsHeader)
              )
            )
          ),
          {},
          false
        ),
      },

      { text: "", margin: [0, 10] },
      {
        columns: [
          {
            text: "",
            width:220
          }, 

          {
            table: PdfModel.getArray(
              ["40%", "*"],
              this.allMontant(this.options.montants),
              {},
              false
            ),
          },
        ],
        columnGap: 2,
      },
      {
        columns: [
       
          {
            text: "",
            width:220
          }, 
          {
            table: PdfModel.getArray(
              ["40%", "*"],
              this.allMontantTotal(this.options.montants),
              {},
              false
            ),
          },
        ],
        columnGap: 2,
      },
      {
        text : "",margin: [0, 20],
      
     },
        (this.type=='Facture' ? 
      [
        { text: 'La présente facture est arrêtée à la somme de :' }
      ] :
      (this.type=='Devis' ? 
        [
          { text: 'Le présent devis est arrêté à la somme de :' }
        ] :
        (this.type=='Bon de livraison' ? 
          [
            { text: 'Le présent bon de livraison est arrêté à la somme de :' }
          ] :
          []
        )
        )
       ),


      { text: NumberToLetter(Math.round(this.data.paymentsInfos.paiement.montant)) + " "+"francs CFA",
      style: ["bolded"],},
      { text: "\n", margin: [0, 6,0,10] },
      ...htmlToPdfmake(this.data.footer.mentions,{defaultStyles:htmlStyle} ),
      /** Footer**/
      {
        columns: [
          {
            text: [
              { text: "\n", margin: [0, 6,0,10] },
              {
                text:""
                 ,
                margin: [3, 2],
                style: ["info"],
              },
            ],
          },
          this.getColumn(
            { type: "image", content: this.signature },
            { width: "*", },
            { position: "right", width: 120 }
          ),
         
        ],
      },
    ];
  }
}

export default PdfBuildBill;
