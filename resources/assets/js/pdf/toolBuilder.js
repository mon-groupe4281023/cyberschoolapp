/**
 *  Créer un tableau
 */
export function table (body = [], widths = ['9%.11', '9.11%', '9%.11', '9%', '9%', '9%', '9%', '9.11%', '9.11%', '9.11%', '9.11%'], heights = [20], margin = [0, 0, 0, 22]) {
    return {
        table: {
            headerRows: 0,
            heights,
            widths,
            body,
        }, 
        margin: margin,
        layout: {
            hLineColor: function (i, node) {
                return 'gray';
            },
            vLineColor: function (i, node) {
                return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
            },
        }
    }
}