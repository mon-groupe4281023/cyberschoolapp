(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["suivi.parametres.formulaires.recettes"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/parametres/formulaires/recettes.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/suivi/parametres/formulaires/recettes.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue_numeric__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-numeric */ "./node_modules/vue-numeric/dist/vue-numeric.min.js");
/* harmony import */ var vue_numeric__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_numeric__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _bp_mixins_currenciesMask__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../bp/mixins/currenciesMask */ "./resources/assets/js/views/bp/mixins/currenciesMask.js");
/* harmony import */ var _bp_mixins_formulaire_formulaire__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../bp/mixins/formulaire/formulaire */ "./resources/assets/js/views/bp/mixins/formulaire/formulaire.js");
/* harmony import */ var _bp_baseImageInput__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../bp/baseImageInput */ "./resources/assets/js/views/bp/baseImageInput.vue");
/* harmony import */ var vue_bootstrap_datetimepicker__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vue-bootstrap-datetimepicker */ "./node_modules/vue-bootstrap-datetimepicker/dist/vue-bootstrap-datetimepicker.js");
/* harmony import */ var vue_bootstrap_datetimepicker__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(vue_bootstrap_datetimepicker__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var vuejs_datepicker__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! vuejs-datepicker */ "./node_modules/vuejs-datepicker/dist/build.js");
/* harmony import */ var vuejs_datepicker__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(vuejs_datepicker__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var pc_bootstrap4_datetimepicker_build_css_bootstrap_datetimepicker_css__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css */ "./node_modules/pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css");
/* harmony import */ var pc_bootstrap4_datetimepicker_build_css_bootstrap_datetimepicker_css__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(pc_bootstrap4_datetimepicker_build_css_bootstrap_datetimepicker_css__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! vue-loading-overlay */ "./node_modules/vue-loading-overlay/dist/vue-loading.min.js");
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(vue_loading_overlay__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var vue_loading_overlay_dist_vue_loading_css__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! vue-loading-overlay/dist/vue-loading.css */ "./node_modules/vue-loading-overlay/dist/vue-loading.css");
/* harmony import */ var vue_loading_overlay_dist_vue_loading_css__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(vue_loading_overlay_dist_vue_loading_css__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var vue_full_calendar__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! vue-full-calendar */ "./node_modules/vue-full-calendar/index.js");
/* harmony import */ var fullcalendar_dist_fullcalendar_css__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! fullcalendar/dist/fullcalendar.css */ "./node_modules/fullcalendar/dist/fullcalendar.css");
/* harmony import */ var fullcalendar_dist_fullcalendar_css__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(fullcalendar_dist_fullcalendar_css__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var fullcalendar_dist_locale_fr__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! fullcalendar/dist/locale/fr */ "./node_modules/fullcalendar/dist/locale/fr.js");
/* harmony import */ var fullcalendar_dist_locale_fr__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(fullcalendar_dist_locale_fr__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _services_helper__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../../../services/helper */ "./resources/assets/js/services/helper.js");
/* harmony import */ var _bp_mixins_suivi_data_tables__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../../bp/mixins/suivi/data-tables */ "./resources/assets/js/views/bp/mixins/suivi/data-tables.js");
/* harmony import */ var vue_form_generator_dist_vfg_css__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! vue-form-generator/dist/vfg.css */ "./node_modules/vue-form-generator/dist/vfg.css");
/* harmony import */ var vue_form_generator_dist_vfg_css__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(vue_form_generator_dist_vfg_css__WEBPACK_IMPORTED_MODULE_17__);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


















var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_10___default.a.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 4000
}); // Init plugin

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    VueNumeric: vue_numeric__WEBPACK_IMPORTED_MODULE_1___default.a,
    BaseImageInput: _bp_baseImageInput__WEBPACK_IMPORTED_MODULE_4__["default"],
    Datepick: vuejs_datepicker__WEBPACK_IMPORTED_MODULE_6___default.a,
    FullCalendar: vue_full_calendar__WEBPACK_IMPORTED_MODULE_12__["FullCalendar"]
  },
  name: 'recette',
  mixins: [_bp_mixins_currenciesMask__WEBPACK_IMPORTED_MODULE_2__["default"], _services_helper__WEBPACK_IMPORTED_MODULE_15__["default"], _bp_mixins_suivi_data_tables__WEBPACK_IMPORTED_MODULE_16__["default"], _bp_mixins_formulaire_formulaire__WEBPACK_IMPORTED_MODULE_3__["default"]],
  computed: {},
  data: function data() {
    var _recettes, _ref;

    return _ref = {
      //formgenerator
      formOptions: {
        validateAfterLoad: true,
        validateAfterChanged: true,
        optionsList: []
      },
      index: '',
      config: {},
      tableData1: [{
        id: 1,
        libelle: "N°",
        state: true,
        testValue: "numero",
        "class": "numero",
        "default": true,
        content: ""
      }, {
        id: 2,
        libelle: "Désignation",
        state: true,
        testValue: "designation",
        "class": "designation",
        "default": true,
        content: ""
      }, {
        id: 3,
        libelle: "Qté",
        testValue: "qte",
        "class": "qte",
        state: true,
        "default": true,
        content: ""
      }, {
        id: 4,
        libelle: "Total à payer",
        state: true,
        testValue: "totalPayer",
        "class": "montant",
        "default": true,
        content: ""
      }, {
        id: 5,
        libelle: "Montant Perçu",
        state: true,
        testValue: "monatntPercu",
        "class": "montant",
        "default": true,
        content: ""
      }, {
        id: 6,
        libelle: "Date Perçu",
        state: false,
        "class": "date",
        testValue: "lieu",
        "default": false,
        content: ""
      }, {
        id: 7,
        libelle: "Reste à payer ",
        "class": "montant",
        state: false,
        testValue: "restePayer",
        "default": false,
        content: ""
      }, {
        id: 8,
        libelle: "Date reste ",
        "class": "date",
        state: false,
        testValue: "datereste",
        "default": false,
        content: ""
      }],
      tableData2: [{
        id: 1,
        libelle: "Lieu Dépot",
        state: true,
        testValue: "numero",
        "default": true,
        content: ""
      }, {
        id: 2,
        libelle: "N° Compte",
        state: true,
        testValue: "designation",
        "default": true,
        content: ""
      }, {
        id: 3,
        libelle: "Montant déposé",
        testValue: "qte",
        state: true,
        "class": "montant",
        "default": true,
        content: ""
      }, {
        id: 4,
        libelle: "Date dépot",
        state: true,
        "class": "date",
        testValue: "totalPayer",
        "default": true,
        content: ""
      }, {
        id: 5,
        libelle: "Effectué par",
        state: true,
        testValue: "monatntPercu",
        "default": true,
        content: ""
      }],
      model: {
        vendeur: {
          nom: ''
        },
        recettes: (_recettes = {
          recette: '',
          libelle: [],
          qte: {},
          banque: {
            nom: ''
          }
        }, _defineProperty(_recettes, "libelle", ''), _defineProperty(_recettes, "montantFacture", 0), _defineProperty(_recettes, "resteMontant", 0), _defineProperty(_recettes, "image", ''), _defineProperty(_recettes, "fichier", ''), _defineProperty(_recettes, "montantRecu", 0), _defineProperty(_recettes, "quantite", 1), _defineProperty(_recettes, "type", ''), _defineProperty(_recettes, "updatedAt", ''), _defineProperty(_recettes, "deletedAt", ''), _defineProperty(_recettes, "dates", {
          montantFactureDate: '',
          montantRecuDate: '',
          restAPercevoirDate: '',
          versementDate: ''
        }), _recettes),
        produits: [],
        clients: [],
        options: {
          format: 'DD/MM/YYYY',
          useCurrent: true
        },
        newInputText: [{
          type: "input",
          inputType: "text",
          label: "Label",
          model: "name",
          maxlength: 50,
          required: false,
          placeholder: "User's full name",
          featured: true
        }],
        client: {
          nom: '',
          prenom: '',
          telephone1: '',
          email: ''
        }
      },
      schema: {
        produits: {
          active: {
            groups: [{
              fields: [{
                type: "elSelectCustom",
                label: "Type de l\'entrée",
                model: "recettes.type",
                featured: true,
                value: [{
                  libelle: "Vente",
                  value: "vente"
                }, {
                  libelle: "Salaires",
                  value: "salaire"
                }],
                typeSelect: '',
                required: true,
                enabled: true,
                styleClasses: 'produitservice',
                obj: 'produits'
              }, {
                type: 'elInputCustom',
                label: 'Titre recette',
                model: 'recettes.recette',
                styleClasses: 'produitservice',
                featured: true,
                read: false,
                obj: 'produits'
              }, {
                type: "elSelectCustom",
                label: "Produit/Service",
                model: "recettes.libelle",
                multiple: true,
                value: [{
                  libelle: "RECETTE JOURNALIERE",
                  value: "recetteJournaliere"
                }],
                featured: true,
                required: true,
                enabled: true,
                styleClasses: 'produitservice',
                obj: 'produits',

                /*
                
                onChanged: function(val) {
                    let loader = Vue.$loading.show();
                    let productName = this.model.recettes.libelle;
                    this.model.produits.forEach((produit) => {
                        if(produit.nom == productName){
                            this.model.recettes.produit = produit;
                            this.model.recettes.quantite=1;
                            this.model.recettes.produit.prixVenteUnitaire=produit.prixVenteUnitaire;
                            this.model.recettes.montantFacture= this.model.recettes.produit.prixVenteUnitaire * this.model.recettes.quantite;
                        }
                        loader.hide();
                    })
                    
                },*/
                hideNoneSelectedText: true,
                noneSelectedText: "Aucun"
              }]
            }]
          },
          inactive: {
            groups: [{
              legend: "Produits",
              fields: [{
                type: 'input',
                inputType: 'text',
                label: 'ID (disabled text field)',
                model: 'id',
                readonly: true,
                featured: true,
                required: true,
                enabled: false,
                obj: 'produits'
              }, {
                type: 'input',
                inputType: 'text',
                label: 'Name',
                model: 'name',
                placeholder: 'Your name',
                featured: true,
                required: true,
                enabled: false,
                obj: 'produits'
              }]
            }]
          },
          newInputText: {
            fields: [{
              type: "input",
              inputType: "text",
              label: "Label",
              model: "name",
              maxlength: 50,
              required: false,
              placeholder: "User's full name",
              featured: true
            }]
          },
          newInputSelect: {
            fields: [{
              type: "select",
              label: "Type",
              model: "type",
              values: [{
                id: "93",
                name: "option 1"
              }, {
                id: "93",
                name: "option 2"
              }]
            }]
          },
          newInputNumber: {
            fields: [{
              type: "input",
              inputType: "number",
              label: "Label",
              model: "name",
              required: false,
              featured: true
            }]
          },
          newInputTextarea: {
            fields: [{
              type: "textArea",
              label: "Biography",
              model: "bio",
              hint: "Max " + this.max + " caractères",
              max: 500,
              placeholder: "User's biography",
              rows: 4,
              required: false,
              featured: true
            }]
          },
          newInputDate: {
            fields: [{
              type: "dateTimePicker",
              label: "Date",
              model: "date",
              dateTimePickerOptions: {
                format: 'DD/MM/YYYY',
                useCurrent: true
              },
              featured: true,
              required: true,
              enabled: true
            }]
          }
        },
        client: {
          active: {
            groups: [{
              styleClasses: 'client',
              fields: [{
                type: "elSelectCustom",
                label: "Nom client",
                model: "client.nom",
                value: [''],
                featured: true,
                typeSelect: '',
                required: true,
                enabled: true,
                obj: 'client',
                hideNoneSelectedText: true,
                noneSelectedText: "Aucun"
              }, {
                type: 'elInputCustom',
                label: 'Prenom',
                model: 'client.prenom',
                styleClasses: 'inputElement',
                read: true,
                obj: 'client'
              }, {
                type: 'elInputCustom',
                inputType: 'text',
                label: 'Téléphone 1',
                model: 'client.telephone1',
                styleClasses: 'inputElement',
                read: true,
                obj: 'client'
              }]
            }]
          },
          inactive: {
            groups: [{
              legend: "Client",
              fields: [{
                type: 'input',
                inputType: 'text',
                label: 'Email',
                model: 'client.email',
                disabled: true,
                placeholder: 'email',
                featured: true,
                required: false,
                enabled: true,
                obj: 'client'
              }, {
                type: 'input',
                inputType: 'text',
                label: 'Téléphone 2',
                model: 'client.telephone2',
                placeholder: 'Téléphone2',
                featured: true,
                required: true,
                enabled: false,
                obj: 'client'
              }, {
                type: 'input',
                inputType: 'number',
                label: 'Age',
                model: 'client.age',
                featured: true,
                required: false,
                enabled: false,
                obj: 'client'
              }, {
                type: 'input',
                inputType: 'text',
                label: 'Quartier',
                model: 'client.quartier',
                featured: true,
                required: false,
                enabled: false,
                obj: 'client'
              }, {
                type: 'input',
                inputType: 'text',
                label: 'Ville',
                model: 'client.ville',
                featured: true,
                required: false,
                enabled: false,
                obj: 'client'
              }, {
                type: 'input',
                inputType: 'text',
                label: 'Pays',
                model: 'client.pays',
                featured: true,
                required: false,
                enabled: false,
                obj: 'client'
              }, {
                type: "textArea",
                label: "Note",
                model: "client.note",
                rows: 4,
                enabled: false,
                obj: 'client'
              }]
            }]
          }
        },
        vendeur: {
          active: {
            groups: [{
              styleClasses: 'vendeur',
              fields: [{
                type: "elSelectCustom",
                label: "Nom vendeur",
                model: "vendeur.nom",
                value: [''],
                featured: true,
                required: true,
                enabled: true,
                obj: 'vendeur',
                typeSelect: '',
                hideNoneSelectedText: true,
                noneSelectedText: "Aucun"
              }, {
                type: 'elInputCustom',
                label: 'Prenom',
                model: 'vendeur.prenom',
                styleClasses: 'inputElement',
                featured: true,
                read: true,
                obj: 'vendeur'
              }, {
                type: 'elInputCustom',
                label: 'Téléphone 1',
                styleClasses: 'inputElement',
                model: 'vendeur.telephone1',
                featured: true,
                read: true,
                obj: 'vendeur'
              }]
            }]
          },
          inactive: {
            groups: [{
              legend: "Vendeur",
              fields: [{
                type: 'input',
                inputType: 'text',
                label: 'Email',
                model: 'vendeur.email',
                disabled: true,
                placeholder: 'email',
                featured: true,
                required: false,
                enabled: true,
                obj: 'vendeur'
              }, {
                type: 'input',
                inputType: 'text',
                label: 'Téléphone 2',
                model: 'vendeur.telephone2',
                placeholder: 'Téléphone2',
                featured: true,
                required: true,
                enabled: false,
                obj: 'vendeur'
              }, {
                type: 'input',
                inputType: 'number',
                label: 'Age',
                model: 'vendeur.age',
                featured: true,
                required: false,
                enabled: false,
                obj: 'vendeur'
              }, {
                type: 'input',
                inputType: 'text',
                label: 'Quartier',
                model: 'vendeur.quartier',
                featured: true,
                required: false,
                enabled: false,
                obj: 'vendeur'
              }, {
                type: 'input',
                inputType: 'text',
                label: 'Ville',
                model: 'vendeur.ville',
                featured: true,
                required: false,
                enabled: false,
                obj: 'vendeur'
              }, {
                type: 'input',
                inputType: 'text',
                label: 'Pays',
                model: 'client.pays',
                featured: true,
                required: false,
                enabled: false,
                obj: 'vendeur'
              }, {
                type: "textArea",
                label: "Note",
                model: "client.note",
                rows: 4,
                enabled: false,
                obj: 'vendeur'
              }]
            }]
          }
        },
        image: {
          groups: [{
            legend: "Image",
            styleClasses: 'image',
            fields: [{
              type: "image",
              label: "Image",
              model: "recettes.image",
              required: false,
              browse: false,
              preview: true,
              hideInput: true
            }]
          }],
          status: true,
          fields: []
        },
        fichiers: {
          groups: [{
            legend: "Fichiers",
            styleClasses: 'fichier',
            fields: [{
              type: "fileUploadCustom",
              label: "Preuve opération",
              model: "recettes.fichier",
              featured: true,
              required: true,
              enabled: true
            }]
          }],
          status: true,
          fields: []
        }
      },
      //formgenerator
      events: []
    }, _defineProperty(_ref, "config", {
      locale: 'fr'
    }), _defineProperty(_ref, "form", firebase__WEBPACK_IMPORTED_MODULE_11___default.a.firestore().collection('formulaires')), _defineProperty(_ref, "modeldevis", firebase__WEBPACK_IMPORTED_MODULE_11___default.a.firestore().collection('modeldevis')), _defineProperty(_ref, "perms", JSON.parse(localStorage.getItem('userSession')).perm[0].content[0].value), _defineProperty(_ref, "loading", true), _defineProperty(_ref, "tableTitles", [{
      prop: "type",
      label: "Type",
      width: "10%"
    }, {
      prop: "libelle",
      label: "Nom"
    }, {
      prop: "montantRecu",
      label: "M. reçu"
    }, {
      prop: "quantite",
      label: "Quantité"
    }, {
      prop: "montant",
      label: "M. facturé"
    }, {
      prop: "client",
      label: "Client"
    }, {
      prop: "vendeur",
      label: "Vendeur"
    }, {
      prop: "versement",
      label: "Versement"
    }, {
      prop: "banque",
      label: "Banque"
    }]), _defineProperty(_ref, "filters", [{
      prop: ['type', 'nom', 'montantRecu'],
      value: ''
    }]), _defineProperty(_ref, "recette", {}), _defineProperty(_ref, "storage", firebase__WEBPACK_IMPORTED_MODULE_11___default.a.storage().ref()), _defineProperty(_ref, "banqueRef", firebase__WEBPACK_IMPORTED_MODULE_11___default.a.firestore().collection('banque')), _defineProperty(_ref, "vendeurRef", firebase__WEBPACK_IMPORTED_MODULE_11___default.a.firestore().collection('vendeurs')), _defineProperty(_ref, "clientRef", firebase__WEBPACK_IMPORTED_MODULE_11___default.a.firestore().collection('clients')), _defineProperty(_ref, "produitRef", firebase__WEBPACK_IMPORTED_MODULE_11___default.a.firestore().collection('financier')), _defineProperty(_ref, "banques", []), _defineProperty(_ref, "vendeurs", []), _defineProperty(_ref, "clients", []), _defineProperty(_ref, "produits", []), _defineProperty(_ref, "client", {}), _defineProperty(_ref, "vendeur", {}), _defineProperty(_ref, "fileTask", ''), _defineProperty(_ref, "preuveOperationFile", ''), _defineProperty(_ref, "preuveFactureFile", ''), _defineProperty(_ref, "imageUpload", ''), _defineProperty(_ref, "banque", {}), _defineProperty(_ref, "projectId", JSON.parse(localStorage.getItem('userSession')).projet), _defineProperty(_ref, "options", {
      format: 'DD/MM/YYYY',
      useCurrent: true
    }), _ref;
  },
  created: function created() {
    var _this = this;

    /*
    this.options.id= this.projectId;
    this.modeldevis.doc().add(this.options);
    let loader = Vue.$loading.show();
    */
    //let's populate banque select with data
    this.banqueRef.where('projectId', '==', this.projectId).onSnapshot(function (querySnapshot) {
      if (!querySnapshot.empty) {
        querySnapshot.forEach(function (doc) {
          var obj = doc.data();
          obj.id = doc.id;

          _this.banques.push(obj);

          loader.hide();
        });
      }
    }); //let's populate vendeur select with data

    this.vendeurRef.where('projectId', '==', this.projectId).onSnapshot(function (querySnapshot) {
      if (!querySnapshot.empty) {
        querySnapshot.forEach(function (doc) {
          var obj = doc.data();
          obj.id = doc.id;

          _this.vendeurs.push(obj);

          loader.hide();
        });
      }
    }); //let's populate client select with data

    this.clientRef.where('projectId', '==', this.projectId).onSnapshot(function (querySnapshot) {
      if (!querySnapshot.empty) {
        querySnapshot.forEach(function (doc) {
          var obj = doc.data();
          obj.id = doc.id;

          _this.clients.push(obj); //Populate the SELECT client


          var field = _this.schema.client.active.groups[0].fields.find(function (field) {
            return field.model === 'client.nom';
          });

          field.values.push(obj.nom);
          loader.hide();
        });
      }
    }); //let's populate product select with data

    this.produitRef.doc(this.projectId).get().then(function (doc) {
      var obj = doc.data().produits; // //console.log(obj)

      if (!_this.isEmpty(obj)) {
        obj['articles'].forEach(function (produit, index) {
          _this.model.produits.push(produit); //Populate the SELECT Produit/Service
          //let field=this.schema.produits.active.groups[0].fields.find(field=>field.model==='recettes.libelle');
          //field.values.push(produit.nom);


          console.log(produit.nom); // alert(field.model);
        });
      }
    }); //LOAD
    //let's populate product select with data

    this.produitRef.doc(this.projectId).get().then(function (doc) {
      var obj = doc.data().produits;

      if (!_this.isEmpty(obj)) {
        obj['articles'].forEach(function (produit, index) {
          // this.model.produits.push(produit);
          //Populate the SELECT Produit/Service
          // this.field.values=[];
          _this.formOptions.optionsList.push({
            'value': produit.nom,
            'label': produit.nom
          }); //console.log('Produit',produit.nom)
          // alert(field.model);

        });
      }
    });
    loader.hide();
  },
  methods: {
    isEmpty: function isEmpty(obj) {
      for (var key in obj) {
        if (obj.hasOwnProperty(key)) return false;
      }

      return true;
    },
    updateInput: function updateInput(value) {//console.log('Hello')
    },
    detectFiles: function detectFiles(e) {
      var _this2 = this;

      var fileList = e.target.files || e.dataTransfer.files;
      Array.from(Array(fileList.length).keys()).map(function (x) {
        // this.upload(fileList[x]);
        _this2.preuveOperationFile = fileList[x];
      });
    },
    //Calendrier Event click
    eventSelected: function eventSelected() {
      $('#modalRecette').modal('show');
    },
    //Calendrier
    calendrier: function calendrier() {
      var _this3 = this;

      var loader = vue__WEBPACK_IMPORTED_MODULE_0___default.a.$loading.show();
      this.events = [];
      firebase__WEBPACK_IMPORTED_MODULE_11___default.a.firestore().collection('recettes').where('projectId', '==', this.projectId).get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          var recette = doc.data(); // //console.log(recette);

          var daterecette = _this3.dateFormat(recette.montantRecuDate);

          _this3.events.push({
            'title': recette.libelle,
            'start': daterecette,
            'color': 'green'
          });
        });
        loader.hide();
      })["catch"](function (error) {//console.log("Error getting document:", error);
      });
    },
    detectFile: function detectFile(e) {
      var _this4 = this;

      var fileList = e.target.files || e.dataTransfer.files;
      Array.from(Array(fileList.length).keys()).map(function (x) {
        // this.upload(fileList[x]);
        _this4.preuveFactureFile = fileList[x];
      });
    },
    upload: function upload(file) {
      var metadata = {
        contentType: file.type
      };
      this.fileTask = this.storage.child('suivi/' + file.lastModified).put(file, metadata);
    },
    //Ajout des champs du formulaire
    saveFormConfig: function saveFormConfig() {
      var _this5 = this;

      // alert('Hello');
      // $('#modalAddInput').modal('hide');
      var loader = vue__WEBPACK_IMPORTED_MODULE_0___default.a.$loading.show(); //console.log( this.projectId)

      this.config.model = this.model;
      this.config.schema = this.schema;
      this.config.projectId = this.projectId;
      this.config.form = 'recette'; //console.log(this.config)

      this.form.where('projectId', '==', this.projectId).where('form', '==', 'recette').get().then(function (snapshot) {
        if (snapshot.empty) {
          //console.log('No matching documents.');
          _this5.form.add(_this5.config).then(function (doc) {
            swal({
              title: "Configuration terminée!!",
              text: "Votre configuration a été engistrée avec succès. Vous allez être rédirigé vers le formulaire des recettes",
              type: "success",
              showCancelButton: true,
              confirmButtonText: "ok",
              cancelButtonText: "Annuler",
              closeOnConfirm: false,
              closeOnCancel: false
            }).then(function (result) {
              if (result.value == true) {
                //console.log(result) 
                loader.hide();

                _this5.$router.push({
                  name: 'suivi-recettes'
                });
              } else {}
            });
          }); // return;


          loader.hide();
        }

        snapshot.forEach(function (doc) {
          _this5.form.doc(doc.id).update(_this5.config).then(function (doc) {
            swal({
              title: "Configuration terminée!!",
              text: "Votre configuration a été Mise à jour avec succès. Vous allez être rédirigé vers le formulaire des recettes",
              type: "success",
              showCancelButton: true,
              confirmButtonText: "ok",
              cancelButtonText: "Annuler",
              closeOnConfirm: false,
              closeOnCancel: false
            }).then(function (result) {
              loader.hide();

              if (result.value == true) {
                //console.log(result) 
                loader.hide();

                _this5.$router.push({
                  name: 'suivi-recettes'
                });
              } else {
                loader.hide();
              }
            });
          });
        });
      })["catch"](function (err) {
        console.log('Error getting documents', err);
        loader.hide();
      });
    },
    onInputChange: function onInputChange() {
      //alert(size);
      var obj = {
        type: "input",
        inputType: "text",
        label: "Libelle",
        model: "name",
        placeholder: "Your name",
        required: false
      };
      this.schema.produits.inactive.groups[0].fields.push(obj); //this.index=size
      // let size=this.schema.texte.fields.length;

      /* this.clientRef.doc(clientId).get().then((doc) => {
          this.recettes.client = doc.data();
          loader.hide();
      }); */
    },
    addNewInputText: function addNewInputText() {
      //alert(size);
      var obj = {
        type: "input",
        inputType: "text",
        label: "Libelle",
        model: "name",
        placeholder: "Your name",
        required: false,
        featured: true
      };
      this.schema.produits.newInputText.fields.push(obj); //this.index=size
      // let size=this.schema.texte.fields.length;

      /* this.clientRef.doc(clientId).get().then((doc) => {
          this.recettes.client = doc.data();
          loader.hide();
      }); */
    },
    addNewInputTextarea: function addNewInputTextarea() {
      //alert(size);
      var obj = {
        type: "textArea",
        label: "Biography",
        model: "bio",
        hint: "Max  caractères",
        max: 500,
        placeholder: "User's biography",
        rows: 4,
        required: false,
        featured: true
      };
      this.schema.produits.newInputTextarea.fields.push(obj); //this.index=size
      // let size=this.schema.texte.fields.length;

      /* this.clientRef.doc(clientId).get().then((doc) => {
          this.recettes.client = doc.data();
          loader.hide();
      }); */
    },
    addNewInputDate: function addNewInputDate() {
      //alert(size);
      var obj = {
        type: "dateTimePicker",
        label: "Date",
        model: "date",
        dateTimePickerOptions: {
          format: 'DD/MM/YYYY',
          useCurrent: true
        },
        featured: true,
        required: true,
        enabled: true
      };
      this.schema.produits.newInputDate.fields.push(obj); //this.index=size
      // let size=this.schema.texte.fields.length;

      /* this.clientRef.doc(clientId).get().then((doc) => {
          this.recettes.client = doc.data();
          loader.hide();
      }); */
    },
    addNewRowOption: function addNewRowOption() {
      var obj = {
        id: "93",
        name: "option"
      };
      this.schema.produits.newInputSelect.fields[0].values.push(obj);
    },
    deleteRowOption: function deleteRowOption(index, item) {
      this.schema.produits.newInputSelect.fields[0].values.splice(index, 1);
    },
    addInputText: function addInputText() {
      var _this6 = this;

      var obj = this.schema.produits.newInputText.fields;
      obj.forEach(function (content) {
        _this6.schema.produits.active.groups[0].fields.push(content);
      });
    },
    addInputDate: function addInputDate() {
      var _this7 = this;

      var obj = this.schema.produits.newInputDate.fields;
      obj.forEach(function (content) {
        _this7.schema.produits.active.groups[0].fields.push(content);
      });
    },
    addInputNumber: function addInputNumber() {
      var _this8 = this;

      var obj = this.schema.produits.newInputNumber.fields;
      obj.forEach(function (content) {
        _this8.schema.produits.active.groups[0].fields.push(content);
      });
    },
    addInputTextarea: function addInputTextarea() {
      var _this9 = this;

      var obj = this.schema.produits.newInputTextarea.fields;
      obj.forEach(function (content) {
        _this9.schema.produits.active.groups[0].fields.push(content);
      });
    },
    //quand on selectionne un client, banque ou vendeur
    onClientChange: function onClientChange(val) {
      var _this10 = this;

      var loader = vue__WEBPACK_IMPORTED_MODULE_0___default.a.$loading.show();
      var clientId = val.target.value;
      this.clients.forEach(function (client) {
        if (client.id == clientId) {
          _this10.recettes.client = client;
          loader.hide();
        }
      });
      /* this.clientRef.doc(clientId).get().then((doc) => {
          this.recettes.client = doc.data();
          loader.hide();
      }); */
    },
    onProductChange: function onProductChange(val) {
      var _this11 = this;

      var loader = vue__WEBPACK_IMPORTED_MODULE_0___default.a.$loading.show();
      var productName = val.target.value; // //console.log(produitId);

      this.produits.forEach(function (produit) {
        if (produit.nom == productName) {
          _this11.recettes.produit = produit;
        }

        loader.hide();
      });
    },
    onBankChange: function onBankChange(val) {
      var _this12 = this;

      var loader = vue__WEBPACK_IMPORTED_MODULE_0___default.a.$loading.show();
      var bankId = val.target.value;
      this.banques.forEach(function (bank) {
        if (bank.id == bankId) {
          _this12.recettes.banque = bank;
          loader.hide();
        }
      });
      /* this.banqueRef.doc(bankId).get().then((doc) => {
          loader.hide();
          this.banque = doc.data();
      }); */
    },
    onSellerChange: function onSellerChange(val) {
      var _this13 = this;

      var loader = vue__WEBPACK_IMPORTED_MODULE_0___default.a.$loading.show();
      var sellerId = val.target.value;
      this.vendeurs.forEach(function (vendeur) {
        if (vendeur.id == sellerId) {
          _this13.recettes.vendeur = vendeur;
          loader.hide();
        }
      });
    },
    onUpdate: function onUpdate(evt) {
      var _this14 = this;

      evt.preventDefault();
      this.tableData = []; //on reset le data du datatable pour éviter les doublons

      this.loading = true;
      this.recette.updatedAt = new Date();
      this.ref.doc(this.recette.id).update(this.recette).then(function (recette) {
        // loader.hide();
        $('#modalEditRecette').modal('hide');
        sweetalert2__WEBPACK_IMPORTED_MODULE_10___default.a.fire({
          type: 'success',
          title: 'Vous avez modifié un élément avec succès',
          showConfirmButton: false,
          timer: 2500
        });
        _this14.loading = false;
      })["catch"](function (error) {
        loader.hide();
        sweetalert2__WEBPACK_IMPORTED_MODULE_10___default.a.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Une erreur est survenue!'
        });
      });
    },
    handleAddProd: function handleAddProd() {
      $('#modalAddInput').modal('show'); //this.recette = row;
    },
    handleAddClient: function handleAddClient() {
      $('#modalAddClient').modal('show'); //this.recette = row;
    },
    handleAddVendeur: function handleAddVendeur() {
      $('#modalAddVendeur').modal('show'); //this.recette = row;
    }
  },
  mounted: function mounted() {
    var _this15 = this;

    this.ref.where('projectId', '==', this.projectId).onSnapshot(function (querySnapshot) {
      querySnapshot.forEach(function (doc) {
        var obj = doc.data(); // //console.log(obj);

        obj.id = doc.id;
        obj.vendeur = doc.data().vendeur.nom;
        obj.banque = doc.data().banque.nom;
        obj.client = doc.data().client.nom;
        obj.montant = obj.produit.prixVenteUnitaire * obj.quantite;

        _this15.tableData.push(obj);
      });
      _this15.loading = false;
    });
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/parametres/formulaires/recettes.vue?vue&type=style&index=0&id=6de11c68&scoped=true&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/suivi/parametres/formulaires/recettes.vue?vue&type=style&index=0&id=6de11c68&scoped=true&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.nav-link.show.active i[data-v-6de11c68],.nav-link.active i[data-v-6de11c68]{\r\n    color: #61b174;\n}\n.nav-link[data-v-6de11c68] {\r\n        font-weight: normal;\r\n        text-transform: normal;\r\n        color:#67757c;\r\n        height: 100%\n}\n.nav-pills > li.nav-item[data-v-6de11c68] {\r\n        display: block;\r\n        width: 50%;\r\n        text-align: center;\n}\n.nav-pills > li.nav-item a[data-v-6de11c68]{\r\n        margin: 1px auto;\r\n        padding:1px 0px;\r\n        border-radius: 2px;\r\n        font-size: 0.9rem;\r\n        background-color: #c8cacd;\r\n        height: 100%\n}\n.nav-pills .nav-link.active[data-v-6de11c68], .nav-pills .show > .nav-link[data-v-6de11c68] {\r\n        background-color: #61b174;\n}\n.card-body[data-v-6de11c68] {\r\n        padding: 0px 5px;\r\n        background-color:#f0f0f052 !important\n}\n.nav.nav-tabs.customtab li a[data-v-6de11c68] {\r\n        text-align: center;\r\n        padding: 5px 10px;\n}\n.nav.nav-tabs.customtab li a.active[data-v-6de11c68] {\r\n        background-color: #1E58B3;\r\n        border-bottom-color: #15f57acc;\n}\n.nav.nav-tabs.customtab li[data-v-6de11c68] {\r\n        width: 33.3%;\r\n        display: inline-block;\r\n        font-size: 1.4rem;\r\n        letter-spacing: 1.5px;\n}\n.input-group-text[data-v-6de11c68] {\r\n        width: 153px;\r\n        background-color: #ffffff;\r\n        color: #000000;\n}\n.hidden-xs-down[data-v-6de11c68]{\r\n        font-size:17px;\r\n        font-weight: bold;\n}\n.customtab li a.nav-link.active[data-v-6de11c68] {\r\n    border-bottom: 2px solid #ffffff;\r\n    color: #ffffff;\n}\n.fieldset[data-v-6de11c68] {\r\n        border: 1px solid #ced4da !important;\r\n        padding: 0 0.4em 0 0.4em !important;\r\n        margin: 0.5em 0 0.5em 0 !important;\r\n        box-shadow:  0px 0px 0px 0px #ced4da;\n}\n.row-fieldset[data-v-6de11c68] {\r\n        border: 1px solid #ced4da !important;\r\n        padding: 0 0.2em 0 0.2em !important;\r\n        margin: 0.5em 0 0.5em 0 !important;\r\n        box-shadow:  0px 0px 0px 0px #ced4da;\n}\n.title input[data-v-6de11c68] {\r\n    background-color: #f2f6fc;\r\n    border: 0;\r\n    text-align: center;\r\n    width: 150px !important;\r\n    color: black;\r\n    padding: 10px;\n}\n.title[data-v-6de11c68] {\r\n    margin-top: 10%;\r\n    margin-right: 20%;\n}\n.save[data-v-6de11c68] {\r\n    background-color: #00ba8b;\r\n    border: 0;\r\n    padding: 6px 14px;\r\n    border-radius: 4px;\r\n    color: white;\r\n    font-weight: bold;\n}\nbutton[data-v-6de11c68]:hover{\r\n    cursor: pointer\n}\n.el-input__inner[data-v-6de11c68]{\r\n        width:50px !important;\n}\ntable[data-v-6de11c68] {\r\n    margin-top: 10px;\r\n    border-collapse: collapse;\r\n    width: 95%;\r\n    margin-bottom: 1%;\r\n    margin-left: 2%;\n}\n.dateInput[data-v-6de11c68]{\r\n        width: 150px !important;\r\n        font-weight: 0px !important;\n}\ntd input[data-v-6de11c68] {\r\n    border: 0;\r\n    padding: 6px;\r\n    width: 100%;\n}\nth[data-v-6de11c68],\r\n    td[data-v-6de11c68] {\r\n    position: relative;\r\n    top: 0;\r\n    left:  0;\r\n    border: 1px solid #ebeef5;\n}\n.versement th[data-v-6de11c68]{\r\n        background-color: #06d79c;\r\n        color: white;\r\n        text-align: center;\r\n        padding: 5px;\n}\nth[data-v-6de11c68] {\r\n    background-color: #409eff;\r\n    color: white;\r\n    text-align: center;\r\n    padding: 5px;\n}\n.el-date-table th[data-v-6de11c68] {\r\n    background-color: #f2f6fc !important;\n}\n.el-date-picker table[data-v-6de11c68] {\r\n    width: 90% !important;\n}\ntd input[data-v-6de11c68]:hover {\r\n    background-color: #f2f6fc;\r\n    cursor: pointer;\n}\nth[data-v-6de11c68] {\r\n    position: relative;\r\n    top: 0;\r\n    left: 0;\n}\n.inline[data-v-6de11c68] {\r\n    position: relative;\r\n    top: 0;\r\n    left: 0;\r\n    width: 100%;\n}\n.total[data-v-6de11c68] {\r\n    position: absolute;\r\n    left: 48%;\r\n    top: -2%;\n}\n.next[data-v-6de11c68] {\r\n    background-color: #00ba8b !important;\r\n    color: white;\r\n    padding: 8px;\n}\na[data-v-6de11c68]:hover {\r\n    color: white !important;\n}\n.mt-top[data-v-6de11c68] {\r\n    margin-top: 10%;\n}\n.el-step__title[data-v-6de11c68] {\r\n    font-weight: bold;\n}\n.el-steps--simple[data-v-6de11c68] {\r\n    background-color: #409eff !important;\n}\n.is-success[data-v-6de11c68] {\r\n    color: white !important;\n}\n.el-step.is-simple .el-step__arrow[data-v-6de11c68]::after,\r\n    .el-step.is-simple .el-step__arrow[data-v-6de11c68]::before {\r\n    background: white !important;\n}\n.sign[data-v-6de11c68] {\r\n    height: 200px;\n}\n.designation[data-v-6de11c68]{\r\n        width:20% !important;\n}\n.qte[data-v-6de11c68]{\r\n        width:5% !important;\n}\n.montant[data-v-6de11c68]{\r\n        width:15% !important;\n}\n.date[data-v-6de11c68]{\r\n        width:10% !important;\n}\n.border-none[data-v-6de11c68] {\r\n    border: 0;\n}\nhr.style-eight[data-v-6de11c68] {\r\n    overflow: visible; /* For IE */\r\n    padding: 0;\r\n    border: none;\r\n    border-top: thin solid #333;\r\n    color: #333;\r\n    text-align: center;\r\n    width: 92%;\r\n    margin-left: 3%;\n}\n.footer-note[data-v-6de11c68] {\r\n    font-size: 13px;\r\n    color: #111;\n}\n.place[data-v-6de11c68] {\r\n    position: absolute;\r\n    bottom: 17%;\r\n    width: 25%;\r\n    height: 100px;\r\n    margin-left: 1%;\n}\n[data-v-6de11c68]::-moz-placeholder {\r\n    color: #606266 !important;\r\n    font-weight: normal;\r\n    font-size: 14px;\n}\n[data-v-6de11c68]:-ms-input-placeholder {\r\n    color: #606266 !important;\r\n    font-weight: normal;\r\n    font-size: 14px;\n}\n[data-v-6de11c68]::-ms-input-placeholder {\r\n    color: #606266 !important;\r\n    font-weight: normal;\r\n    font-size: 14px;\n}\n[data-v-6de11c68]::placeholder {\r\n    color: #606266 !important;\r\n    font-weight: normal;\r\n    font-size: 14px;\n}\n.btn-minus[data-v-6de11c68] {\r\n    position: absolute;\r\n    left: 0;\r\n    visibility: hidden;\r\n    padding: 3px;\r\n    margin-left: 1%;\n}\ntr:hover button[data-v-6de11c68] {\r\n    visibility: visible;\n}\n.section-title input[data-v-6de11c68] {\r\n    border: 0;\r\n    color: #606266;\r\n    font-size: 18px;\r\n    background-color: #E4E7ED;\r\n    display: inline-block;\n}\n.section-title input.title-sect[data-v-6de11c68]:focus {\r\n    background-color: #E4E7ED;\n}\n.section-title[data-v-6de11c68] {\r\n    background-color: #E4E7ED;\r\n    padding: 0 !important;\n}\n.total-sect[data-v-6de11c68] {\r\n    width: 123px;\r\n    padding-left: 14px!important;\n}\n.title-sect[data-v-6de11c68] {\r\n    width: 84%;\r\n    padding-left: 16px;\n}\n.numero[data-v-6de11c68] {\r\n    width: 26px;\r\n    margin-right: 7px;\r\n    text-align: center;\r\n    background-color: #E4E7ED;\r\n    color: #606266 !important;\n}\n.box-card[data-v-6de11c68] {\r\n    width: 100%;\n}\n.marg-left[data-v-6de11c68] {\r\n    margin-left: 77%;\r\n    width: 80%;\n}\n.el-card[data-v-6de11c68] {\r\n    border-radius: 0;\n}\n.bd-left[data-v-6de11c68] {\r\n    border-left: 2px solid #00c875;\r\n    padding-left: 2px;\n}\n.dropdown-button[data-v-6de11c68] {\r\n    padding: 0px !important;\r\n    padding-left: 6px !important;\r\n    margin-right: 5px !important;\n}\n.el-select[data-v-6de11c68] {\r\n    width: 230px !important;\n}\n.input-width[data-v-6de11c68]{\r\n    width: 300px!important;\n}\n.new-style[data-v-6de11c68]{\r\n    background-color: red;\n}\n.numero-ligne[data-v-6de11c68]{\r\n    width: 50px!important;\n}\n.unite-col[data-v-6de11c68]{\r\n    width: 60px!important;\n}\n.remise-col[data-v-6de11c68]{\r\n    width: 90px!important;\n}\n.total-col[data-v-6de11c68]{\r\n    width: 220px!important;\r\n    text-align: right!important;\n}\n.el-input-number[data-v-6de11c68]{\r\n    width: 120px!important;\n}\n.showRemiseButton[data-v-6de11c68]{\r\n    position: absolute;\r\n    top: 2px;\r\n    right: 5px; \r\n    padding: 1px!important;\r\n    visibility: hidden;\n}\ntd:hover .showRemiseButton[data-v-6de11c68]{\r\n    visibility: visible;\n}\n.numero[data-v-6de11c68]{\r\n    width: 2%!important;\n}\n.numericred[data-v-6de11c68]{\r\n        background-color: #ee2d2de3 !important;\r\n        color:#f3f3f3;\n}\n.numericgreen[data-v-6de11c68]{\r\n        background-color: #06d79c !important;\r\n        color:#f3f3f3;\n}\n.item-prix-total[data-v-6de11c68]{\r\n        text-align: right !important;\n}\n.titletable[data-v-6de11c68]{\r\n        font-size:17px;\r\n        font-weight: 400\n}\n.footerpanier[data-v-6de11c68]{\r\n        background-color:#c6c7c8;\n}\r\n   \r\n \r\n \r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/parametres/formulaires/recettes.vue?vue&type=style&index=0&id=6de11c68&scoped=true&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/suivi/parametres/formulaires/recettes.vue?vue&type=style&index=0&id=6de11c68&scoped=true&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader??ref--7-1!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./recettes.vue?vue&type=style&index=0&id=6de11c68&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/parametres/formulaires/recettes.vue?vue&type=style&index=0&id=6de11c68&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/parametres/formulaires/recettes.vue?vue&type=template&id=6de11c68&scoped=true&":
/*!******************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/suivi/parametres/formulaires/recettes.vue?vue&type=template&id=6de11c68&scoped=true& ***!
  \******************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "page-wrapper" }, [
    _c("div", { staticClass: "container-fluid" }, [
      _vm._m(0),
      _vm._v(" "),
      _c("div", { staticClass: "row" }, [
        _c(
          "div",
          { staticClass: "col-4" },
          [
            _c(
              "vue-form-generator",
              {
                attrs: {
                  schema: _vm.schema.produits.active,
                  model: _vm.model,
                  options: _vm.formOptions
                },
                on: {
                  "model-updated": _vm.onModelUpdated,
                  remove: _vm.deleteItem
                }
              },
              [
                _c(
                  "span",
                  {
                    staticClass: "input-group-addon",
                    attrs: { slot: "firstName-before" },
                    slot: "firstName-before"
                  },
                  [_c("i", { staticClass: "fa fa-search" })]
                ),
                _vm._v(" "),
                _c(
                  "span",
                  {
                    staticClass: "input-group-addon",
                    attrs: { slot: "lastName-after" },
                    slot: "lastName-after"
                  },
                  [_c("i", { staticClass: "fa fa-search" })]
                )
              ]
            ),
            _vm._v(" "),
            _c("div", { staticClass: "mb-1" }, [
              _c(
                "button",
                {
                  staticClass: "btn btn-info",
                  attrs: { type: "button" },
                  on: {
                    click: function($event) {
                      return _vm.handleAddProd()
                    }
                  }
                },
                [_c("i", { staticClass: "fas fa-plus-circle" })]
              )
            ])
          ],
          1
        ),
        _vm._v(" "),
        _c("div", { staticClass: "col-3" }, [
          _c(
            "div",
            { staticClass: "card-body" },
            [
              _c("vue-form-generator", {
                attrs: {
                  schema: _vm.schema.client.active,
                  model: _vm.model,
                  options: _vm.model.formOptions
                },
                on: { "model-updated": _vm.onModelUpdated }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "mb-1" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-info",
                    attrs: { type: "button" },
                    on: {
                      click: function($event) {
                        return _vm.handleAddClient()
                      }
                    }
                  },
                  [_c("i", { staticClass: "fas fa-plus-circle" })]
                )
              ])
            ],
            1
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-3" }, [
          _c("div", { staticClass: "card" }, [
            _c(
              "div",
              { staticClass: "card-body" },
              [
                _c("vue-form-generator", {
                  attrs: {
                    schema: _vm.schema.vendeur.active,
                    model: _vm.model,
                    options: _vm.formOptions
                  }
                })
              ],
              1
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "mb-1" }, [
            _c(
              "button",
              {
                staticClass: "btn btn-info",
                attrs: { type: "button" },
                on: {
                  click: function($event) {
                    return _vm.handleAddVendeur()
                  }
                }
              },
              [_c("i", { staticClass: "fas fa-plus-circle" })]
            )
          ])
        ]),
        _vm._v(" "),
        _vm._m(1),
        _vm._v(" "),
        _c("div", { staticClass: "col-12" }, [
          _c("div", { staticClass: "card" }, [
            _c("div", { staticClass: "card-body" }, [
              _vm._m(2),
              _vm._v(" "),
              _c(
                "table",
                [
                  _c(
                    "tr",
                    _vm._l(_vm.tableData1, function(item, index) {
                      return _c("th", { key: index, class: item.class }, [
                        _vm._v(_vm._s(item.libelle))
                      ])
                    }),
                    0
                  ),
                  _vm._v(" "),
                  _vm._l(_vm.TableContent, function(item, index) {
                    return _c(
                      "tr",
                      { key: index },
                      [
                        _vm._l(item, function(content, indexContent) {
                          return _c(
                            "td",
                            {
                              key: indexContent,
                              class:
                                content.typeinput === "total"
                                  ? "item-prix-total"
                                  : ""
                            },
                            [
                              content.type === "montant" &&
                              content.typeinput === "recu"
                                ? _c(
                                    "strong",
                                    [
                                      _c("vue-numeric", {
                                        class:
                                          this.totalResteAPayer === 0
                                            ? "form-control text-right numeric  "
                                            : "form-control text-right numericgreen ",
                                        attrs: {
                                          currency:
                                            _vm.currentCurrencyMask["currency"],
                                          "currency-symbol-position":
                                            _vm.currentCurrencyMask[
                                              "currency-symbol-position"
                                            ],
                                          "output-type": "String",
                                          "empty-value": "0",
                                          precision:
                                            _vm.currentCurrencyMask[
                                              "precision"
                                            ],
                                          "decimal-separator":
                                            _vm.currentCurrencyMask[
                                              "decimal-separator"
                                            ],
                                          "thousand-separator":
                                            _vm.currentCurrencyMask[
                                              "thousand-separator"
                                            ],
                                          "read-only-class": "item-total",
                                          min: _vm.currentCurrencyMask.min
                                        },
                                        on: {
                                          input: function($event) {
                                            return _vm.calculMontantReste(index)
                                          }
                                        },
                                        model: {
                                          value: content.value,
                                          callback: function($$v) {
                                            _vm.$set(
                                              content,
                                              "value",
                                              _vm._n($$v)
                                            )
                                          },
                                          expression: "content.value"
                                        }
                                      })
                                    ],
                                    1
                                  )
                                : content.type === "montant" &&
                                  content.typeinput === "total"
                                ? _c(
                                    "strong",
                                    [
                                      _c("vue-numeric", {
                                        staticClass:
                                          "form-control text-right numeric",
                                        attrs: {
                                          currency:
                                            _vm.currentCurrencyMask["currency"],
                                          "currency-symbol-position":
                                            _vm.currentCurrencyMask[
                                              "currency-symbol-position"
                                            ],
                                          "output-type": "String",
                                          "empty-value": "0",
                                          "read-only": true,
                                          precision:
                                            _vm.currentCurrencyMask[
                                              "precision"
                                            ],
                                          "decimal-separator":
                                            _vm.currentCurrencyMask[
                                              "decimal-separator"
                                            ],
                                          "thousand-separator":
                                            _vm.currentCurrencyMask[
                                              "thousand-separator"
                                            ],
                                          "read-only-class": "item-prix-total",
                                          min: _vm.currentCurrencyMask.min
                                        },
                                        model: {
                                          value: content.value,
                                          callback: function($$v) {
                                            _vm.$set(
                                              content,
                                              "value",
                                              _vm._n($$v)
                                            )
                                          },
                                          expression: "content.value"
                                        }
                                      })
                                    ],
                                    1
                                  )
                                : content.type === "montant" &&
                                  content.typeinput === "reste"
                                ? _c(
                                    "strong",
                                    [
                                      _c("vue-numeric", {
                                        class:
                                          content.value === 0
                                            ? "form-control text-right numeric"
                                            : "form-control text-right numeric numericred",
                                        attrs: {
                                          currency:
                                            _vm.currentCurrencyMask["currency"],
                                          "currency-symbol-position":
                                            _vm.currentCurrencyMask[
                                              "currency-symbol-position"
                                            ],
                                          "output-type": "String",
                                          "empty-value": "0",
                                          precision:
                                            _vm.currentCurrencyMask[
                                              "precision"
                                            ],
                                          "decimal-separator":
                                            _vm.currentCurrencyMask[
                                              "decimal-separator"
                                            ],
                                          "thousand-separator":
                                            _vm.currentCurrencyMask[
                                              "thousand-separator"
                                            ],
                                          "read-only-class": "item-total",
                                          min: _vm.currentCurrencyMask.min
                                        },
                                        model: {
                                          value: content.value,
                                          callback: function($$v) {
                                            _vm.$set(
                                              content,
                                              "value",
                                              _vm._n($$v)
                                            )
                                          },
                                          expression: "content.value"
                                        }
                                      })
                                    ],
                                    1
                                  )
                                : content.type === "qte"
                                ? _c(
                                    "h6",
                                    [
                                      _c("el-input-number", {
                                        staticClass: "el-input-number",
                                        attrs: { size: "small", min: 1 },
                                        on: {
                                          change: function($event) {
                                            return _vm.calculTotalPrix(index)
                                          }
                                        },
                                        model: {
                                          value: content.value,
                                          callback: function($$v) {
                                            _vm.$set(content, "value", $$v)
                                          },
                                          expression: "content.value"
                                        }
                                      })
                                    ],
                                    1
                                  )
                                : content.type === "date"
                                ? _c(
                                    "h6",
                                    [
                                      _c("el-date-picker", {
                                        staticClass: "dateInput",
                                        attrs: {
                                          type: "date",
                                          format: "dd MMM yyyy",
                                          placeholder: "Choississez un jour"
                                        },
                                        model: {
                                          value: content.value,
                                          callback: function($$v) {
                                            _vm.$set(content, "value", $$v)
                                          },
                                          expression: "content.value"
                                        }
                                      })
                                    ],
                                    1
                                  )
                                : _c("h6", [_vm._v(_vm._s(content))])
                            ]
                          )
                        }),
                        _vm._v(" "),
                        _c("el-button", {
                          attrs: {
                            size: "mini",
                            icon: "el-icon-remove-outline",
                            circle: ""
                          },
                          on: {
                            click: function($event) {
                              return _vm.removeRawProduit(index, item)
                            }
                          }
                        })
                      ],
                      2
                    )
                  }),
                  _vm._v(" "),
                  _c("tr"),
                  _vm._v(" "),
                  _c("tr", { staticClass: "footerpanier" }, [
                    _c("td", { attrs: { colspan: "3" } }, [_vm._v("Total ")]),
                    _vm._v(" "),
                    _c(
                      "td",
                      {
                        staticClass: "item-prix-total",
                        attrs: { colspan: "1" }
                      },
                      [
                        _c("vue-numeric", {
                          staticClass: "form-control text-right numeric",
                          attrs: {
                            currency: _vm.currentCurrencyMask["currency"],
                            "currency-symbol-position":
                              _vm.currentCurrencyMask[
                                "currency-symbol-position"
                              ],
                            "output-type": "String",
                            "empty-value": "0",
                            "read-only": true,
                            precision: _vm.currentCurrencyMask["precision"],
                            "decimal-separator":
                              _vm.currentCurrencyMask["decimal-separator"],
                            "thousand-separator":
                              _vm.currentCurrencyMask["thousand-separator"],
                            "read-only-class": "item-prix-total",
                            min: _vm.currentCurrencyMask.min
                          },
                          model: {
                            value: this.totalPrixProduits,
                            callback: function($$v) {
                              _vm.$set(this, "totalPrixProduits", _vm._n($$v))
                            },
                            expression: "this.totalPrixProduits"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "td",
                      {
                        staticClass: "item-prix-total",
                        attrs: { colspan: "1" }
                      },
                      [
                        _c("vue-numeric", {
                          staticClass: "form-control text-right numeric",
                          attrs: {
                            currency: _vm.currentCurrencyMask["currency"],
                            "currency-symbol-position":
                              _vm.currentCurrencyMask[
                                "currency-symbol-position"
                              ],
                            "output-type": "String",
                            "empty-value": "0",
                            "read-only": true,
                            precision: _vm.currentCurrencyMask["precision"],
                            "decimal-separator":
                              _vm.currentCurrencyMask["decimal-separator"],
                            "thousand-separator":
                              _vm.currentCurrencyMask["thousand-separator"],
                            "read-only-class": "item-prix-total",
                            min: _vm.currentCurrencyMask.min
                          },
                          model: {
                            value: this.totalMontantRecu,
                            callback: function($$v) {
                              _vm.$set(this, "totalMontantRecu", _vm._n($$v))
                            },
                            expression: "this.totalMontantRecu"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c("td", { attrs: { colspan: "1" } }),
                    _vm._v(" "),
                    _c(
                      "td",
                      {
                        staticClass: "item-prix-total",
                        attrs: { colspan: "1" }
                      },
                      [
                        _c("vue-numeric", {
                          staticClass: "form-control text-right numeric",
                          attrs: {
                            currency: _vm.currentCurrencyMask["currency"],
                            "currency-symbol-position":
                              _vm.currentCurrencyMask[
                                "currency-symbol-position"
                              ],
                            "output-type": "String",
                            "empty-value": "0",
                            "read-only": true,
                            precision: _vm.currentCurrencyMask["precision"],
                            "decimal-separator":
                              _vm.currentCurrencyMask["decimal-separator"],
                            "thousand-separator":
                              _vm.currentCurrencyMask["thousand-separator"],
                            "read-only-class": "item-prix-total",
                            min: _vm.currentCurrencyMask.min
                          },
                          model: {
                            value: this.totalResteAPayer,
                            callback: function($$v) {
                              _vm.$set(this, "totalResteAPayer", _vm._n($$v))
                            },
                            expression: "this.totalResteAPayer"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c("td", { attrs: { colspan: "1" } })
                  ])
                ],
                2
              )
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-8" }, [
          _c("div", { staticClass: "card" }, [
            _c("div", { staticClass: "card-body" }, [
              _vm._m(3),
              _vm._v(" "),
              _c(
                "table",
                { staticClass: "versement" },
                [
                  _c(
                    "tr",
                    _vm._l(_vm.tableData2, function(item, index) {
                      return _c(
                        "th",
                        {
                          key: index,
                          staticClass: "font-weight-bold",
                          class: item.class,
                          attrs: { width: "auto" }
                        },
                        [_vm._v(_vm._s(item.libelle))]
                      )
                    }),
                    0
                  ),
                  _vm._v(" "),
                  _vm._l(_vm.Tableversement, function(item, index) {
                    return _c(
                      "tr",
                      { key: index },
                      [
                        _vm._l(item, function(content, indexContent) {
                          return _c("td", { key: indexContent }, [
                            content.type === "montant"
                              ? _c(
                                  "strong",
                                  [
                                    _c("vue-numeric", {
                                      staticClass:
                                        "form-control text-right numeric",
                                      attrs: {
                                        currency:
                                          _vm.currentCurrencyMask["currency"],
                                        "currency-symbol-position":
                                          _vm.currentCurrencyMask[
                                            "currency-symbol-position"
                                          ],
                                        "output-type": "String",
                                        "empty-value": "0",
                                        precision:
                                          _vm.currentCurrencyMask["precision"],
                                        "decimal-separator":
                                          _vm.currentCurrencyMask[
                                            "decimal-separator"
                                          ],
                                        "thousand-separator":
                                          _vm.currentCurrencyMask[
                                            "thousand-separator"
                                          ],
                                        "read-only-class": "item-total",
                                        min: _vm.currentCurrencyMask.min
                                      },
                                      model: {
                                        value: content.value,
                                        callback: function($$v) {
                                          _vm.$set(
                                            content,
                                            "value",
                                            _vm._n($$v)
                                          )
                                        },
                                        expression: "content.value"
                                      }
                                    })
                                  ],
                                  1
                                )
                              : content.type === "qte"
                              ? _c(
                                  "h4",
                                  [
                                    _c("el-input-number", {
                                      staticClass: "el-input-number",
                                      attrs: { size: "small", min: 1 },
                                      model: {
                                        value: content.value,
                                        callback: function($$v) {
                                          _vm.$set(content, "value", $$v)
                                        },
                                        expression: "content.value"
                                      }
                                    })
                                  ],
                                  1
                                )
                              : content.type === "date"
                              ? _c(
                                  "h4",
                                  [
                                    _c("el-date-picker", {
                                      staticClass: "dateInput",
                                      attrs: {
                                        type: "date",
                                        format: "dd MMM yyyy",
                                        placeholder: "Choississez un jour"
                                      },
                                      model: {
                                        value: content.value,
                                        callback: function($$v) {
                                          _vm.$set(content, "value", $$v)
                                        },
                                        expression: "content.value"
                                      }
                                    })
                                  ],
                                  1
                                )
                              : content.type === "texte"
                              ? _c(
                                  "h4",
                                  [
                                    _c("el-input", {
                                      attrs: {
                                        placeholder: "Entrez quelque chose"
                                      },
                                      model: {
                                        value: content.value,
                                        callback: function($$v) {
                                          _vm.$set(content, "value", $$v)
                                        },
                                        expression: "content.value"
                                      }
                                    })
                                  ],
                                  1
                                )
                              : _c(
                                  "h4",
                                  [
                                    _c("el-input", {
                                      model: {
                                        value: content.value,
                                        callback: function($$v) {
                                          _vm.$set(content, "value", $$v)
                                        },
                                        expression: "content.value"
                                      }
                                    })
                                  ],
                                  1
                                )
                          ])
                        }),
                        _vm._v(" "),
                        _c("el-button", {
                          attrs: {
                            size: "mini",
                            icon: "el-icon-remove-outline",
                            circle: ""
                          }
                        })
                      ],
                      2
                    )
                  })
                ],
                2
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "mb-1" },
                [
                  _c("el-button", {
                    attrs: {
                      size: "mini",
                      type: "primary",
                      icon: "el-icon-circle-plus",
                      circle: ""
                    }
                  })
                ],
                1
              )
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-4" }, [
          _c("div", { staticClass: "card" }, [
            _c(
              "div",
              { staticClass: "card-body" },
              [
                _c(
                  "vue-form-generator",
                  {
                    attrs: {
                      schema: _vm.schema.fichiers,
                      model: _vm.model,
                      options: _vm.model.formOptions
                    },
                    on: {
                      "model-updated": _vm.onModelUpdated,
                      "remove-tag": _vm.deleteItem
                    }
                  },
                  [
                    _c(
                      "span",
                      {
                        staticClass: "input-group-addon",
                        attrs: { slot: "firstName-before" },
                        slot: "firstName-before"
                      },
                      [_c("i", { staticClass: "fa fa-search" })]
                    ),
                    _vm._v(" "),
                    _c(
                      "span",
                      {
                        staticClass: "input-group-addon",
                        attrs: { slot: "lastName-after" },
                        slot: "lastName-after"
                      },
                      [_c("i", { staticClass: "fa fa-search" })]
                    )
                  ]
                ),
                _vm._v(" "),
                _c("div", { staticClass: "mb-1" }, [
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-info",
                      attrs: { type: "button" },
                      on: {
                        click: function($event) {
                          return _vm.handleAddFile()
                        }
                      }
                    },
                    [_c("i", { staticClass: "fas fa-plus-circle" })]
                  )
                ])
              ],
              1
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "card" }, [
            _c("div", { staticClass: "card-body" }, [
              _c("div", { staticClass: "click2edit m-b-40" }, [
                _vm._v(
                  "Cliquez sur le boutton enregistrer pour sauvegarder toutes les modifications."
                )
              ]),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass: "btn btn-success btn-rounded",
                  attrs: { id: "save", type: "button" },
                  on: {
                    click: function($event) {
                      return _vm.saveFormConfig()
                    }
                  }
                },
                [_vm._v("Enregistrer")]
              )
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "row" }, [
        _c(
          "div",
          {
            staticClass: "modal fade",
            attrs: { id: "modalAddInput", tabindex: "-1", role: "dialog" }
          },
          [
            _c(
              "div",
              {
                staticClass: "modal-dialog modal-lg",
                attrs: { role: "document" }
              },
              [
                _c("div", { staticClass: "modal-content" }, [
                  _vm._m(4),
                  _vm._v(" "),
                  _vm._m(5),
                  _vm._v(" "),
                  _c("div", { staticClass: "modal-body" }, [
                    _c("div", { staticClass: "tab-content" }, [
                      _c(
                        "div",
                        {
                          staticClass: "tab-pane active",
                          attrs: { id: "home", role: "tabpanel" }
                        },
                        [
                          _c("div", { staticClass: "row" }, [
                            _c("div", { staticClass: "col-md-12" }),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-6" }, [
                              _c(
                                "div",
                                _vm._l(
                                  _vm.schema.produits.active.groups[0].fields,
                                  function(input, index) {
                                    return _c("div", { key: index }, [
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "form-group row mb-1 visibilite"
                                        },
                                        [
                                          _c(
                                            "label",
                                            {
                                              staticClass:
                                                "col-5 col-form-label"
                                            },
                                            [_vm._v("Visibilité")]
                                          ),
                                          _vm._v(" "),
                                          _c("el-switch", {
                                            staticStyle: { display: "inline" },
                                            attrs: {
                                              "active-color": "#13ce66",
                                              "inactive-color": "#ff4949",
                                              "active-text": "activer",
                                              "inactive-text": "désactiver"
                                            },
                                            on: {
                                              change: function($event) {
                                                return _vm.onVisibleChange(
                                                  input,
                                                  index
                                                )
                                              }
                                            },
                                            model: {
                                              value: input.enabled,
                                              callback: function($$v) {
                                                _vm.$set(input, "enabled", $$v)
                                              },
                                              expression: "input.enabled"
                                            }
                                          })
                                        ],
                                        1
                                      )
                                    ])
                                  }
                                ),
                                0
                              )
                            ]),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-md-6" },
                              [
                                _c(
                                  "vue-form-generator",
                                  {
                                    attrs: {
                                      schema:
                                        _vm.schema.produits.active.groups[0],
                                      model: _vm.model,
                                      options: _vm.formOptions
                                    },
                                    scopedSlots: _vm._u([
                                      {
                                        key: "field",
                                        fn: function(props) {
                                          return [
                                            _c("tr", [
                                              _c("td", [
                                                _c("h1", [
                                                  _vm._v(
                                                    " " +
                                                      _vm._s(props.field.label)
                                                  )
                                                ])
                                              ]),
                                              _c("td"),
                                              _c("td")
                                            ])
                                          ]
                                        }
                                      }
                                    ])
                                  },
                                  [
                                    _vm._v(
                                      "\n                                            //this would be the general template for each field\n                                            "
                                    )
                                  ]
                                )
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-6" }, [
                              _c(
                                "div",
                                _vm._l(
                                  _vm.schema.produits.inactive.groups[0].fields,
                                  function(input, index) {
                                    return _c("div", { key: index }, [
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "form-group row mb-1 visibilite"
                                        },
                                        [
                                          _c(
                                            "label",
                                            {
                                              staticClass:
                                                "col-5 col-form-label"
                                            },
                                            [_vm._v("Visibilité")]
                                          ),
                                          _vm._v(" "),
                                          _c("el-switch", {
                                            staticStyle: { display: "inline" },
                                            attrs: {
                                              "active-color": "#13ce66",
                                              "inactive-color": "#ff4949",
                                              "active-text": "activer",
                                              "inactive-text": "désactiver"
                                            },
                                            on: {
                                              change: function($event) {
                                                return _vm.onVisibleChange(
                                                  input,
                                                  index
                                                )
                                              }
                                            },
                                            model: {
                                              value: input.enabled,
                                              callback: function($$v) {
                                                _vm.$set(input, "enabled", $$v)
                                              },
                                              expression: "input.enabled"
                                            }
                                          })
                                        ],
                                        1
                                      )
                                    ])
                                  }
                                ),
                                0
                              )
                            ]),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-md-6" },
                              [
                                _c(
                                  "vue-form-generator",
                                  {
                                    attrs: {
                                      schema:
                                        _vm.schema.produits.inactive.groups[0],
                                      model: _vm.model,
                                      options: _vm.formOptions
                                    },
                                    scopedSlots: _vm._u([
                                      {
                                        key: "field",
                                        fn: function(props) {
                                          return [
                                            _c("tr", [
                                              _c("td", [
                                                _c("h1", [
                                                  _vm._v(
                                                    " " +
                                                      _vm._s(props.field.label)
                                                  )
                                                ])
                                              ]),
                                              _c("td"),
                                              _c("td")
                                            ])
                                          ]
                                        }
                                      }
                                    ])
                                  },
                                  [
                                    _vm._v(
                                      "\n                                            //this would be the general template for each field\n                                            "
                                    )
                                  ]
                                )
                              ],
                              1
                            )
                          ])
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "tab-pane",
                          attrs: { id: "ajout", role: "tabpanel" }
                        },
                        [
                          _c("div", { staticClass: "card-body" }, [
                            _c("div", { staticClass: "form-group row mb-1" }, [
                              _vm._m(6),
                              _vm._v(" "),
                              _c("div", { staticClass: "col-12" }, [
                                _c(
                                  "select",
                                  {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.model.typeChamp,
                                        expression: "model.typeChamp"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: { id: "type" },
                                    on: {
                                      change: [
                                        function($event) {
                                          var $$selectedVal = Array.prototype.filter
                                            .call(
                                              $event.target.options,
                                              function(o) {
                                                return o.selected
                                              }
                                            )
                                            .map(function(o) {
                                              var val =
                                                "_value" in o
                                                  ? o._value
                                                  : o.value
                                              return val
                                            })
                                          _vm.$set(
                                            _vm.model,
                                            "typeChamp",
                                            $event.target.multiple
                                              ? $$selectedVal
                                              : $$selectedVal[0]
                                          )
                                        },
                                        _vm.onInputChange
                                      ]
                                    }
                                  },
                                  [
                                    _c(
                                      "option",
                                      { attrs: { value: "inputText" } },
                                      [_vm._v("Input Texte")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "inputNumber" } },
                                      [_vm._v("Input number")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "inputDate" } },
                                      [_vm._v("Date")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "inputTextarea" } },
                                      [_vm._v("Zone de texte")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "inputSelect" } },
                                      [_vm._v("Input Select")]
                                    )
                                  ]
                                )
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "row" }, [
                              _c("div", { staticClass: "col-md-6" }, [
                                _vm.model.typeChamp == "inputText"
                                  ? _c(
                                      "div",
                                      [
                                        _vm._l(
                                          _vm.schema.produits.newInputText
                                            .fields,
                                          function(input, index) {
                                            return _c("div", { key: index }, [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Libellé")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "col-7" },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value: input.label,
                                                            expression:
                                                              "input.label"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          type: "text",
                                                          id: "lieu"
                                                        },
                                                        domProps: {
                                                          value: input.label
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              input,
                                                              "label",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("PlaceHolder")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "col-7" },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value:
                                                              input.placeholder,
                                                            expression:
                                                              "input.placeholder"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          type: "text",
                                                          id: "lieu"
                                                        },
                                                        domProps: {
                                                          value:
                                                            input.placeholder
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              input,
                                                              "placeholder",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Obligatoire")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c("el-switch", {
                                                    staticStyle: {
                                                      display: "inline"
                                                    },
                                                    attrs: {
                                                      "active-color": "#13ce66",
                                                      "inactive-color":
                                                        "#ff4949",
                                                      "active-text": "oui",
                                                      "inactive-text": "non"
                                                    },
                                                    model: {
                                                      value: input.required,
                                                      callback: function($$v) {
                                                        _vm.$set(
                                                          input,
                                                          "required",
                                                          $$v
                                                        )
                                                      },
                                                      expression:
                                                        "input.required"
                                                    }
                                                  })
                                                ],
                                                1
                                              )
                                            ])
                                          }
                                        ),
                                        _vm._v(" "),
                                        _c("div", [
                                          _c("div", { staticClass: "mb-1" }, [
                                            _c(
                                              "button",
                                              {
                                                staticClass: "btn btn-info",
                                                attrs: { type: "button" },
                                                on: {
                                                  click: function($event) {
                                                    return _vm.addNewInputText()
                                                  }
                                                }
                                              },
                                              [
                                                _c("i", {
                                                  staticClass:
                                                    "fas fa-plus-circle"
                                                })
                                              ]
                                            )
                                          ])
                                        ])
                                      ],
                                      2
                                    )
                                  : _vm._e()
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-md-6" },
                                [
                                  _vm.model.typeChamp == "inputText"
                                    ? _c("vue-form-generator", {
                                        attrs: {
                                          schema:
                                            _vm.schema.produits.newInputText,
                                          model: _vm.model,
                                          options: _vm.formOptions
                                        }
                                      })
                                    : _vm._e()
                                ],
                                1
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "row" }, [
                              _c("div", { staticClass: "col-md-6" }, [
                                _vm.model.typeChamp == "inputNumber"
                                  ? _c(
                                      "div",
                                      [
                                        _vm._l(
                                          _vm.schema.produits.newInputNumber
                                            .fields,
                                          function(input, index) {
                                            return _c("div", { key: index }, [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Libellé")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "col-7" },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value: input.label,
                                                            expression:
                                                              "input.label"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          type: "text",
                                                          id: "lieu"
                                                        },
                                                        domProps: {
                                                          value: input.label
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              input,
                                                              "label",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Obligatoire")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c("el-switch", {
                                                    staticStyle: {
                                                      display: "inline"
                                                    },
                                                    attrs: {
                                                      "active-color": "#13ce66",
                                                      "inactive-color":
                                                        "#ff4949",
                                                      "active-text": "oui",
                                                      "inactive-text": "non"
                                                    },
                                                    model: {
                                                      value: input.required,
                                                      callback: function($$v) {
                                                        _vm.$set(
                                                          input,
                                                          "required",
                                                          $$v
                                                        )
                                                      },
                                                      expression:
                                                        "input.required"
                                                    }
                                                  })
                                                ],
                                                1
                                              )
                                            ])
                                          }
                                        ),
                                        _vm._v(" "),
                                        _c("div", [
                                          _c("div", { staticClass: "mb-1" }, [
                                            _c(
                                              "button",
                                              {
                                                staticClass: "btn btn-info",
                                                attrs: { type: "button" },
                                                on: {
                                                  click: function($event) {
                                                    return _vm.addNewInputNumber()
                                                  }
                                                }
                                              },
                                              [
                                                _c("i", {
                                                  staticClass:
                                                    "fas fa-plus-circle"
                                                })
                                              ]
                                            )
                                          ])
                                        ])
                                      ],
                                      2
                                    )
                                  : _vm._e()
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-md-6" },
                                [
                                  _vm.model.typeChamp == "inputNumber"
                                    ? _c("vue-form-generator", {
                                        attrs: {
                                          schema:
                                            _vm.schema.produits.newInputNumber,
                                          model: _vm.model,
                                          options: _vm.formOptions
                                        }
                                      })
                                    : _vm._e()
                                ],
                                1
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "row" }, [
                              _c("div", { staticClass: "col-md-6" }, [
                                _vm.model.typeChamp == "inputDate"
                                  ? _c(
                                      "div",
                                      [
                                        _vm._l(
                                          _vm.schema.produits.newInputDate
                                            .fields,
                                          function(input, index) {
                                            return _c("div", { key: index }, [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Libellé")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "col-7" },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value: input.label,
                                                            expression:
                                                              "input.label"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          type: "text",
                                                          id: "lieu"
                                                        },
                                                        domProps: {
                                                          value: input.label
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              input,
                                                              "label",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Obligatoire")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c("el-switch", {
                                                    staticStyle: {
                                                      display: "inline"
                                                    },
                                                    attrs: {
                                                      "active-color": "#13ce66",
                                                      "inactive-color":
                                                        "#ff4949",
                                                      "active-text": "oui",
                                                      "inactive-text": "non"
                                                    },
                                                    model: {
                                                      value: input.required,
                                                      callback: function($$v) {
                                                        _vm.$set(
                                                          input,
                                                          "required",
                                                          $$v
                                                        )
                                                      },
                                                      expression:
                                                        "input.required"
                                                    }
                                                  })
                                                ],
                                                1
                                              )
                                            ])
                                          }
                                        ),
                                        _vm._v(" "),
                                        _c("div", [
                                          _c("div", { staticClass: "mb-1" }, [
                                            _c(
                                              "button",
                                              {
                                                staticClass: "btn btn-info",
                                                attrs: { type: "button" },
                                                on: {
                                                  click: function($event) {
                                                    return _vm.addNewInputDate()
                                                  }
                                                }
                                              },
                                              [
                                                _c("i", {
                                                  staticClass:
                                                    "fas fa-plus-circle"
                                                })
                                              ]
                                            )
                                          ])
                                        ])
                                      ],
                                      2
                                    )
                                  : _vm._e()
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-md-6" },
                                [
                                  _vm.model.typeChamp == "inputDate"
                                    ? _c("vue-form-generator", {
                                        attrs: {
                                          schema:
                                            _vm.schema.produits.newInputDate,
                                          model: _vm.model,
                                          options: _vm.formOptions
                                        }
                                      })
                                    : _vm._e()
                                ],
                                1
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "row" }, [
                              _c("div", { staticClass: "col-md-6" }, [
                                _vm.model.typeChamp == "inputTextarea"
                                  ? _c(
                                      "div",
                                      [
                                        _vm._l(
                                          _vm.schema.produits.newInputTextarea
                                            .fields,
                                          function(input, index) {
                                            return _c("div", { key: index }, [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Libellé")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "col-7" },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value: input.label,
                                                            expression:
                                                              "input.label"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          type: "text",
                                                          id: "lieu"
                                                        },
                                                        domProps: {
                                                          value: input.label
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              input,
                                                              "label",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Lignes")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "col-7" },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value: input.rows,
                                                            expression:
                                                              "input.rows"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          type: "number",
                                                          id: "lieu"
                                                        },
                                                        domProps: {
                                                          value: input.rows
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              input,
                                                              "rows",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("PlaceHolder")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "col-7" },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value:
                                                              input.placeholder,
                                                            expression:
                                                              "input.placeholder"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          type: "text",
                                                          id: "lieu"
                                                        },
                                                        domProps: {
                                                          value:
                                                            input.placeholder
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              input,
                                                              "placeholder",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Max caractères")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "col-7" },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value: input.max,
                                                            expression:
                                                              "input.max"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          type: "number",
                                                          id: "lieu"
                                                        },
                                                        domProps: {
                                                          value: input.max
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              input,
                                                              "max",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Obligatoire")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c("el-switch", {
                                                    staticStyle: {
                                                      display: "inline"
                                                    },
                                                    attrs: {
                                                      "active-color": "#13ce66",
                                                      "inactive-color":
                                                        "#ff4949",
                                                      "active-text": "oui",
                                                      "inactive-text": "non"
                                                    },
                                                    model: {
                                                      value: input.required,
                                                      callback: function($$v) {
                                                        _vm.$set(
                                                          input,
                                                          "required",
                                                          $$v
                                                        )
                                                      },
                                                      expression:
                                                        "input.required"
                                                    }
                                                  })
                                                ],
                                                1
                                              )
                                            ])
                                          }
                                        ),
                                        _vm._v(" "),
                                        _c("div", [
                                          _c("div", { staticClass: "mb-1" }, [
                                            _c(
                                              "button",
                                              {
                                                staticClass: "btn btn-info",
                                                attrs: { type: "button" },
                                                on: {
                                                  click: function($event) {
                                                    return _vm.addNewInputTextarea()
                                                  }
                                                }
                                              },
                                              [
                                                _c("i", {
                                                  staticClass:
                                                    "fas fa-plus-circle"
                                                })
                                              ]
                                            )
                                          ])
                                        ])
                                      ],
                                      2
                                    )
                                  : _vm._e()
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-md-6" },
                                [
                                  _vm.model.typeChamp == "inputTextarea"
                                    ? _c("vue-form-generator", {
                                        attrs: {
                                          schema:
                                            _vm.schema.produits
                                              .newInputTextarea,
                                          model: _vm.model,
                                          options: _vm.formOptions
                                        }
                                      })
                                    : _vm._e()
                                ],
                                1
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "row" }, [
                              _c("div", { staticClass: "col-md-6" }, [
                                _vm.model.typeChamp == "inputSelect"
                                  ? _c(
                                      "div",
                                      [
                                        _vm._l(
                                          _vm.schema.produits.newInputSelect
                                            .fields,
                                          function(input, index) {
                                            return _c("div", { key: index }, [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Libellé")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "col-7" },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value: input.label,
                                                            expression:
                                                              "input.label"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          type: "text",
                                                          id: "lieu"
                                                        },
                                                        domProps: {
                                                          value: input.label
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              input,
                                                              "label",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "table-responsive"
                                                },
                                                [
                                                  _c(
                                                    "table",
                                                    {
                                                      staticClass:
                                                        "table editable-table table-bordered table-1 m-b-0 color-bordered-table info-bordered-table"
                                                    },
                                                    [
                                                      _vm._m(7, true),
                                                      _vm._v(" "),
                                                      _c(
                                                        "tbody",
                                                        [
                                                          _vm._l(
                                                            input.values,
                                                            function(
                                                              item,
                                                              index
                                                            ) {
                                                              return _c(
                                                                "tr",
                                                                { key: index },
                                                                [
                                                                  _c("td", [
                                                                    _c(
                                                                      "input",
                                                                      {
                                                                        directives: [
                                                                          {
                                                                            name:
                                                                              "model",
                                                                            rawName:
                                                                              "v-model",
                                                                            value:
                                                                              item.name,
                                                                            expression:
                                                                              "item.name"
                                                                          }
                                                                        ],
                                                                        staticClass:
                                                                          "form-control",
                                                                        attrs: {
                                                                          type:
                                                                            "text"
                                                                        },
                                                                        domProps: {
                                                                          value:
                                                                            item.name
                                                                        },
                                                                        on: {
                                                                          input: function(
                                                                            $event
                                                                          ) {
                                                                            if (
                                                                              $event
                                                                                .target
                                                                                .composing
                                                                            ) {
                                                                              return
                                                                            }
                                                                            _vm.$set(
                                                                              item,
                                                                              "name",
                                                                              $event
                                                                                .target
                                                                                .value
                                                                            )
                                                                          }
                                                                        }
                                                                      }
                                                                    )
                                                                  ]),
                                                                  _vm._v(" "),
                                                                  _c("td", [
                                                                    _c(
                                                                      "button",
                                                                      {
                                                                        staticClass:
                                                                          "btn btn-danger btn-sm",
                                                                        attrs: {
                                                                          type:
                                                                            "button"
                                                                        },
                                                                        on: {
                                                                          click: function(
                                                                            $event
                                                                          ) {
                                                                            return _vm.deleteRowOption(
                                                                              index,
                                                                              item
                                                                            )
                                                                          }
                                                                        }
                                                                      },
                                                                      [
                                                                        _c(
                                                                          "i",
                                                                          {
                                                                            staticClass:
                                                                              "fas fa-minus-circle"
                                                                          }
                                                                        )
                                                                      ]
                                                                    )
                                                                  ])
                                                                ]
                                                              )
                                                            }
                                                          ),
                                                          _vm._v(" "),
                                                          _c("tr", [
                                                            _c(
                                                              "td",
                                                              {
                                                                attrs: {
                                                                  colspan: "5"
                                                                }
                                                              },
                                                              [
                                                                _c(
                                                                  "button",
                                                                  {
                                                                    staticClass:
                                                                      "btn btn-info btn-sm",
                                                                    attrs: {
                                                                      type:
                                                                        "button"
                                                                    },
                                                                    on: {
                                                                      click: function(
                                                                        $event
                                                                      ) {
                                                                        return _vm.addNewRowOption()
                                                                      }
                                                                    }
                                                                  },
                                                                  [
                                                                    _c("i", {
                                                                      staticClass:
                                                                        "fas fa-plus-circle"
                                                                    })
                                                                  ]
                                                                )
                                                              ]
                                                            )
                                                          ])
                                                        ],
                                                        2
                                                      )
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Obligatoire")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c("el-switch", {
                                                    staticStyle: {
                                                      display: "inline"
                                                    },
                                                    attrs: {
                                                      "active-color": "#13ce66",
                                                      "inactive-color":
                                                        "#ff4949",
                                                      "active-text": "oui",
                                                      "inactive-text": "non"
                                                    },
                                                    model: {
                                                      value: input.required,
                                                      callback: function($$v) {
                                                        _vm.$set(
                                                          input,
                                                          "required",
                                                          $$v
                                                        )
                                                      },
                                                      expression:
                                                        "input.required"
                                                    }
                                                  })
                                                ],
                                                1
                                              )
                                            ])
                                          }
                                        ),
                                        _vm._v(" "),
                                        _c("div", [
                                          _c("div", { staticClass: "mb-1" }, [
                                            _c(
                                              "button",
                                              {
                                                staticClass: "btn btn-info",
                                                attrs: { type: "button" },
                                                on: {
                                                  click: function($event) {
                                                    return _vm.addNewInputSelect()
                                                  }
                                                }
                                              },
                                              [
                                                _c("i", {
                                                  staticClass:
                                                    "fas fa-plus-circle"
                                                })
                                              ]
                                            )
                                          ])
                                        ])
                                      ],
                                      2
                                    )
                                  : _vm._e()
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-md-6" },
                                [
                                  _vm.model.typeChamp == "inputSelect"
                                    ? _c("vue-form-generator", {
                                        attrs: {
                                          schema:
                                            _vm.schema.produits.newInputSelect,
                                          model: _vm.model,
                                          options: _vm.formOptions
                                        }
                                      })
                                    : _vm._e()
                                ],
                                1
                              )
                            ])
                          ])
                        ]
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "modal-footer" }, [
                      _vm.model.typeChamp == "inputTextarea"
                        ? _c(
                            "button",
                            {
                              staticClass: "btn btn-rounded btn-success",
                              on: {
                                click: function($event) {
                                  return _vm.addInputTextarea()
                                }
                              }
                            },
                            [_vm._v("Enregistrer")]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.model.typeChamp == "inputTextarea"
                        ? _c(
                            "button",
                            {
                              staticClass: "btn btn-rounded btn-success",
                              on: {
                                click: function($event) {
                                  return _vm.addInputSelect()
                                }
                              }
                            },
                            [_vm._v("Enregistrer")]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.model.typeChamp == "inputDate"
                        ? _c(
                            "button",
                            {
                              staticClass: "btn btn-rounded btn-success",
                              on: {
                                click: function($event) {
                                  return _vm.addInputDate()
                                }
                              }
                            },
                            [_vm._v("Enregistrer")]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.model.typeChamp == "inputText"
                        ? _c(
                            "button",
                            {
                              staticClass: "btn btn-rounded btn-success",
                              on: {
                                click: function($event) {
                                  return _vm.addInputText()
                                }
                              }
                            },
                            [_vm._v("Enregistrer")]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.model.typeChamp == "inputNumber"
                        ? _c(
                            "button",
                            {
                              staticClass: "btn btn-rounded btn-success",
                              on: {
                                click: function($event) {
                                  return _vm.addInputNumber()
                                }
                              }
                            },
                            [_vm._v("Enregistrer")]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-rounded btn-danger",
                          attrs: { type: "button", "data-dismiss": "modal" }
                        },
                        [_vm._v("Fermer")]
                      )
                    ])
                  ])
                ])
              ]
            )
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            ref: "modalRecette",
            staticClass: "modal fade",
            attrs: { id: "modalRecette", tabindex: "-1", role: "dialog" }
          },
          [_vm._m(8)]
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "row" }, [
        _c(
          "div",
          {
            staticClass: "modal fade",
            attrs: { id: "modalAddClient", tabindex: "-1", role: "dialog" }
          },
          [
            _c(
              "div",
              {
                staticClass: "modal-dialog modal-lg",
                attrs: { role: "document" }
              },
              [
                _c("div", { staticClass: "modal-content" }, [
                  _vm._m(9),
                  _vm._v(" "),
                  _vm._m(10),
                  _vm._v(" "),
                  _c("div", { staticClass: "modal-body" }, [
                    _c("div", { staticClass: "tab-content" }, [
                      _c(
                        "div",
                        {
                          staticClass: "tab-pane active",
                          attrs: { id: "home", role: "tabpanel" }
                        },
                        [
                          _c("div", { staticClass: "row" }, [
                            _c("div", { staticClass: "col-md-12" }),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-6" }, [
                              _c(
                                "div",
                                _vm._l(
                                  _vm.schema.client.active.groups[0].fields,
                                  function(input, index) {
                                    return _c("div", { key: index }, [
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "form-group row mb-1 visibilite"
                                        },
                                        [
                                          _c(
                                            "label",
                                            {
                                              staticClass:
                                                "col-5 col-form-label"
                                            },
                                            [_vm._v("Visibilité")]
                                          ),
                                          _vm._v(" "),
                                          _c("el-switch", {
                                            staticStyle: { display: "inline" },
                                            attrs: {
                                              "active-color": "#13ce66",
                                              "inactive-color": "#ff4949",
                                              "active-text": "activer",
                                              "inactive-text": "désactiver"
                                            },
                                            on: {
                                              change: function($event) {
                                                return _vm.onVisibleChange(
                                                  input,
                                                  index
                                                )
                                              }
                                            },
                                            model: {
                                              value: input.enabled,
                                              callback: function($$v) {
                                                _vm.$set(input, "enabled", $$v)
                                              },
                                              expression: "input.enabled"
                                            }
                                          })
                                        ],
                                        1
                                      )
                                    ])
                                  }
                                ),
                                0
                              )
                            ]),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-md-6" },
                              [
                                _c(
                                  "vue-form-generator",
                                  {
                                    attrs: {
                                      schema:
                                        _vm.schema.client.active.groups[0],
                                      model: _vm.model,
                                      options: _vm.formOptions
                                    },
                                    scopedSlots: _vm._u([
                                      {
                                        key: "field",
                                        fn: function(props) {
                                          return [
                                            _c("tr", [
                                              _c("td", [
                                                _c("h1", [
                                                  _vm._v(
                                                    " " +
                                                      _vm._s(props.field.label)
                                                  )
                                                ])
                                              ]),
                                              _c("td"),
                                              _c("td")
                                            ])
                                          ]
                                        }
                                      }
                                    ])
                                  },
                                  [
                                    _vm._v(
                                      "\n                                                //this would be the general template for each field\n                                                "
                                    )
                                  ]
                                )
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-6" }, [
                              _c(
                                "div",
                                _vm._l(
                                  _vm.schema.client.inactive.groups[0].fields,
                                  function(input, index) {
                                    return _c("div", { key: index }, [
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "form-group row mb-1 visibilite"
                                        },
                                        [
                                          _c(
                                            "label",
                                            {
                                              staticClass:
                                                "col-5 col-form-label"
                                            },
                                            [_vm._v("Visibilité")]
                                          ),
                                          _vm._v(" "),
                                          _c("el-switch", {
                                            staticStyle: { display: "inline" },
                                            attrs: {
                                              "active-color": "#13ce66",
                                              "inactive-color": "#ff4949",
                                              "active-text": "activer",
                                              "inactive-text": "désactiver"
                                            },
                                            on: {
                                              change: function($event) {
                                                return _vm.onVisibleChange(
                                                  input,
                                                  index
                                                )
                                              }
                                            },
                                            model: {
                                              value: input.enabled,
                                              callback: function($$v) {
                                                _vm.$set(input, "enabled", $$v)
                                              },
                                              expression: "input.enabled"
                                            }
                                          })
                                        ],
                                        1
                                      )
                                    ])
                                  }
                                ),
                                0
                              )
                            ]),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-md-6" },
                              [
                                _c(
                                  "vue-form-generator",
                                  {
                                    attrs: {
                                      schema:
                                        _vm.schema.client.inactive.groups[0],
                                      model: _vm.model,
                                      options: _vm.formOptions
                                    },
                                    scopedSlots: _vm._u([
                                      {
                                        key: "field",
                                        fn: function(props) {
                                          return [
                                            _c("tr", [
                                              _c("td", [
                                                _c("h1", [
                                                  _vm._v(
                                                    " " +
                                                      _vm._s(props.field.label)
                                                  )
                                                ])
                                              ]),
                                              _c("td"),
                                              _c("td")
                                            ])
                                          ]
                                        }
                                      }
                                    ])
                                  },
                                  [
                                    _vm._v(
                                      "\n                                                //this would be the general template for each field\n                                                "
                                    )
                                  ]
                                )
                              ],
                              1
                            )
                          ])
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "tab-pane",
                          attrs: { id: "ajout", role: "tabpanel" }
                        },
                        [
                          _c("div", { staticClass: "card-body" }, [
                            _c("div", { staticClass: "form-group row mb-1" }, [
                              _vm._m(11),
                              _vm._v(" "),
                              _c("div", { staticClass: "col-12" }, [
                                _c(
                                  "select",
                                  {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.model.typeChamp,
                                        expression: "model.typeChamp"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: { id: "type" },
                                    on: {
                                      change: [
                                        function($event) {
                                          var $$selectedVal = Array.prototype.filter
                                            .call(
                                              $event.target.options,
                                              function(o) {
                                                return o.selected
                                              }
                                            )
                                            .map(function(o) {
                                              var val =
                                                "_value" in o
                                                  ? o._value
                                                  : o.value
                                              return val
                                            })
                                          _vm.$set(
                                            _vm.model,
                                            "typeChamp",
                                            $event.target.multiple
                                              ? $$selectedVal
                                              : $$selectedVal[0]
                                          )
                                        },
                                        _vm.onInputChange
                                      ]
                                    }
                                  },
                                  [
                                    _c(
                                      "option",
                                      { attrs: { value: "inputText" } },
                                      [_vm._v("Input Texte")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "inputNumber" } },
                                      [_vm._v("Input number")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "inputDate" } },
                                      [_vm._v("Date")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "inputTextarea" } },
                                      [_vm._v("Zone de texte")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "inputSelect" } },
                                      [_vm._v("Input Select")]
                                    )
                                  ]
                                )
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "row" }, [
                              _c("div", { staticClass: "col-md-6" }, [
                                _vm.model.typeChamp == "inputText"
                                  ? _c(
                                      "div",
                                      [
                                        _vm._l(
                                          _vm.schema.produits.newInputText
                                            .fields,
                                          function(input, index) {
                                            return _c("div", { key: index }, [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Libellé")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "col-7" },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value: input.label,
                                                            expression:
                                                              "input.label"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          type: "text",
                                                          id: "lieu"
                                                        },
                                                        domProps: {
                                                          value: input.label
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              input,
                                                              "label",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("PlaceHolder")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "col-7" },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value:
                                                              input.placeholder,
                                                            expression:
                                                              "input.placeholder"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          type: "text",
                                                          id: "lieu"
                                                        },
                                                        domProps: {
                                                          value:
                                                            input.placeholder
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              input,
                                                              "placeholder",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Obligatoire")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c("el-switch", {
                                                    staticStyle: {
                                                      display: "inline"
                                                    },
                                                    attrs: {
                                                      "active-color": "#13ce66",
                                                      "inactive-color":
                                                        "#ff4949",
                                                      "active-text": "oui",
                                                      "inactive-text": "non"
                                                    },
                                                    model: {
                                                      value: input.required,
                                                      callback: function($$v) {
                                                        _vm.$set(
                                                          input,
                                                          "required",
                                                          $$v
                                                        )
                                                      },
                                                      expression:
                                                        "input.required"
                                                    }
                                                  })
                                                ],
                                                1
                                              )
                                            ])
                                          }
                                        ),
                                        _vm._v(" "),
                                        _c("div", [
                                          _c("div", { staticClass: "mb-1" }, [
                                            _c(
                                              "button",
                                              {
                                                staticClass: "btn btn-info",
                                                attrs: { type: "button" },
                                                on: {
                                                  click: function($event) {
                                                    return _vm.addNewInputText()
                                                  }
                                                }
                                              },
                                              [
                                                _c("i", {
                                                  staticClass:
                                                    "fas fa-plus-circle"
                                                })
                                              ]
                                            )
                                          ])
                                        ])
                                      ],
                                      2
                                    )
                                  : _vm._e()
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-md-6" },
                                [
                                  _vm.model.typeChamp == "inputText"
                                    ? _c("vue-form-generator", {
                                        attrs: {
                                          schema:
                                            _vm.schema.produits.newInputText,
                                          model: _vm.model,
                                          options: _vm.formOptions
                                        }
                                      })
                                    : _vm._e()
                                ],
                                1
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "row" }, [
                              _c("div", { staticClass: "col-md-6" }, [
                                _vm.model.typeChamp == "inputNumber"
                                  ? _c(
                                      "div",
                                      [
                                        _vm._l(
                                          _vm.schema.produits.newInputNumber
                                            .fields,
                                          function(input, index) {
                                            return _c("div", { key: index }, [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Libellé")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "col-7" },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value: input.label,
                                                            expression:
                                                              "input.label"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          type: "text",
                                                          id: "lieu"
                                                        },
                                                        domProps: {
                                                          value: input.label
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              input,
                                                              "label",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Obligatoire")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c("el-switch", {
                                                    staticStyle: {
                                                      display: "inline"
                                                    },
                                                    attrs: {
                                                      "active-color": "#13ce66",
                                                      "inactive-color":
                                                        "#ff4949",
                                                      "active-text": "oui",
                                                      "inactive-text": "non"
                                                    },
                                                    model: {
                                                      value: input.required,
                                                      callback: function($$v) {
                                                        _vm.$set(
                                                          input,
                                                          "required",
                                                          $$v
                                                        )
                                                      },
                                                      expression:
                                                        "input.required"
                                                    }
                                                  })
                                                ],
                                                1
                                              )
                                            ])
                                          }
                                        ),
                                        _vm._v(" "),
                                        _c("div", [
                                          _c("div", { staticClass: "mb-1" }, [
                                            _c(
                                              "button",
                                              {
                                                staticClass: "btn btn-info",
                                                attrs: { type: "button" },
                                                on: {
                                                  click: function($event) {
                                                    return _vm.addNewInputNumber()
                                                  }
                                                }
                                              },
                                              [
                                                _c("i", {
                                                  staticClass:
                                                    "fas fa-plus-circle"
                                                })
                                              ]
                                            )
                                          ])
                                        ])
                                      ],
                                      2
                                    )
                                  : _vm._e()
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-md-6" },
                                [
                                  _vm.model.typeChamp == "inputNumber"
                                    ? _c("vue-form-generator", {
                                        attrs: {
                                          schema:
                                            _vm.schema.produits.newInputNumber,
                                          model: _vm.model,
                                          options: _vm.formOptions
                                        }
                                      })
                                    : _vm._e()
                                ],
                                1
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "row" }, [
                              _c("div", { staticClass: "col-md-6" }, [
                                _vm.model.typeChamp == "inputDate"
                                  ? _c(
                                      "div",
                                      [
                                        _vm._l(
                                          _vm.schema.produits.newInputDate
                                            .fields,
                                          function(input, index) {
                                            return _c("div", { key: index }, [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Libellé")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "col-7" },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value: input.label,
                                                            expression:
                                                              "input.label"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          type: "text",
                                                          id: "lieu"
                                                        },
                                                        domProps: {
                                                          value: input.label
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              input,
                                                              "label",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Obligatoire")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c("el-switch", {
                                                    staticStyle: {
                                                      display: "inline"
                                                    },
                                                    attrs: {
                                                      "active-color": "#13ce66",
                                                      "inactive-color":
                                                        "#ff4949",
                                                      "active-text": "oui",
                                                      "inactive-text": "non"
                                                    },
                                                    model: {
                                                      value: input.required,
                                                      callback: function($$v) {
                                                        _vm.$set(
                                                          input,
                                                          "required",
                                                          $$v
                                                        )
                                                      },
                                                      expression:
                                                        "input.required"
                                                    }
                                                  })
                                                ],
                                                1
                                              )
                                            ])
                                          }
                                        ),
                                        _vm._v(" "),
                                        _c("div", [
                                          _c("div", { staticClass: "mb-1" }, [
                                            _c(
                                              "button",
                                              {
                                                staticClass: "btn btn-info",
                                                attrs: { type: "button" },
                                                on: {
                                                  click: function($event) {
                                                    return _vm.addNewInputDate()
                                                  }
                                                }
                                              },
                                              [
                                                _c("i", {
                                                  staticClass:
                                                    "fas fa-plus-circle"
                                                })
                                              ]
                                            )
                                          ])
                                        ])
                                      ],
                                      2
                                    )
                                  : _vm._e()
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-md-6" },
                                [
                                  _vm.model.typeChamp == "inputDate"
                                    ? _c("vue-form-generator", {
                                        attrs: {
                                          schema:
                                            _vm.schema.produits.newInputDate,
                                          model: _vm.model,
                                          options: _vm.formOptions
                                        }
                                      })
                                    : _vm._e()
                                ],
                                1
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "row" }, [
                              _c("div", { staticClass: "col-md-6" }, [
                                _vm.model.typeChamp == "inputTextarea"
                                  ? _c(
                                      "div",
                                      [
                                        _vm._l(
                                          _vm.schema.produits.newInputTextarea
                                            .fields,
                                          function(input, index) {
                                            return _c("div", { key: index }, [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Libellé")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "col-7" },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value: input.label,
                                                            expression:
                                                              "input.label"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          type: "text",
                                                          id: "lieu"
                                                        },
                                                        domProps: {
                                                          value: input.label
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              input,
                                                              "label",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Lignes")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "col-7" },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value: input.rows,
                                                            expression:
                                                              "input.rows"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          type: "number",
                                                          id: "lieu"
                                                        },
                                                        domProps: {
                                                          value: input.rows
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              input,
                                                              "rows",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("PlaceHolder")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "col-7" },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value:
                                                              input.placeholder,
                                                            expression:
                                                              "input.placeholder"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          type: "text",
                                                          id: "lieu"
                                                        },
                                                        domProps: {
                                                          value:
                                                            input.placeholder
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              input,
                                                              "placeholder",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Max caractères")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "col-7" },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value: input.max,
                                                            expression:
                                                              "input.max"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          type: "number",
                                                          id: "lieu"
                                                        },
                                                        domProps: {
                                                          value: input.max
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              input,
                                                              "max",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Obligatoire")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c("el-switch", {
                                                    staticStyle: {
                                                      display: "inline"
                                                    },
                                                    attrs: {
                                                      "active-color": "#13ce66",
                                                      "inactive-color":
                                                        "#ff4949",
                                                      "active-text": "oui",
                                                      "inactive-text": "non"
                                                    },
                                                    model: {
                                                      value: input.required,
                                                      callback: function($$v) {
                                                        _vm.$set(
                                                          input,
                                                          "required",
                                                          $$v
                                                        )
                                                      },
                                                      expression:
                                                        "input.required"
                                                    }
                                                  })
                                                ],
                                                1
                                              )
                                            ])
                                          }
                                        ),
                                        _vm._v(" "),
                                        _c("div", [
                                          _c("div", { staticClass: "mb-1" }, [
                                            _c(
                                              "button",
                                              {
                                                staticClass: "btn btn-info",
                                                attrs: { type: "button" },
                                                on: {
                                                  click: function($event) {
                                                    return _vm.addNewInputTextarea()
                                                  }
                                                }
                                              },
                                              [
                                                _c("i", {
                                                  staticClass:
                                                    "fas fa-plus-circle"
                                                })
                                              ]
                                            )
                                          ])
                                        ])
                                      ],
                                      2
                                    )
                                  : _vm._e()
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-md-6" },
                                [
                                  _vm.model.typeChamp == "inputTextarea"
                                    ? _c("vue-form-generator", {
                                        attrs: {
                                          schema:
                                            _vm.schema.produits
                                              .newInputTextarea,
                                          model: _vm.model,
                                          options: _vm.formOptions
                                        }
                                      })
                                    : _vm._e()
                                ],
                                1
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "row" }, [
                              _c("div", { staticClass: "col-md-6" }, [
                                _vm.model.typeChamp == "inputSelect"
                                  ? _c(
                                      "div",
                                      [
                                        _vm._l(
                                          _vm.schema.produits.newInputSelect
                                            .fields,
                                          function(input, index) {
                                            return _c("div", { key: index }, [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Libellé")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "col-7" },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value: input.label,
                                                            expression:
                                                              "input.label"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          type: "text",
                                                          id: "lieu"
                                                        },
                                                        domProps: {
                                                          value: input.label
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              input,
                                                              "label",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "table-responsive"
                                                },
                                                [
                                                  _c(
                                                    "table",
                                                    {
                                                      staticClass:
                                                        "table editable-table table-bordered table-1 m-b-0 color-bordered-table info-bordered-table"
                                                    },
                                                    [
                                                      _vm._m(12, true),
                                                      _vm._v(" "),
                                                      _c(
                                                        "tbody",
                                                        [
                                                          _vm._l(
                                                            input.values,
                                                            function(
                                                              item,
                                                              index
                                                            ) {
                                                              return _c(
                                                                "tr",
                                                                { key: index },
                                                                [
                                                                  _c("td", [
                                                                    _c(
                                                                      "input",
                                                                      {
                                                                        directives: [
                                                                          {
                                                                            name:
                                                                              "model",
                                                                            rawName:
                                                                              "v-model",
                                                                            value:
                                                                              item.name,
                                                                            expression:
                                                                              "item.name"
                                                                          }
                                                                        ],
                                                                        staticClass:
                                                                          "form-control",
                                                                        attrs: {
                                                                          type:
                                                                            "text"
                                                                        },
                                                                        domProps: {
                                                                          value:
                                                                            item.name
                                                                        },
                                                                        on: {
                                                                          input: function(
                                                                            $event
                                                                          ) {
                                                                            if (
                                                                              $event
                                                                                .target
                                                                                .composing
                                                                            ) {
                                                                              return
                                                                            }
                                                                            _vm.$set(
                                                                              item,
                                                                              "name",
                                                                              $event
                                                                                .target
                                                                                .value
                                                                            )
                                                                          }
                                                                        }
                                                                      }
                                                                    )
                                                                  ]),
                                                                  _vm._v(" "),
                                                                  _c("td", [
                                                                    _c(
                                                                      "button",
                                                                      {
                                                                        staticClass:
                                                                          "btn btn-danger btn-sm",
                                                                        attrs: {
                                                                          type:
                                                                            "button"
                                                                        },
                                                                        on: {
                                                                          click: function(
                                                                            $event
                                                                          ) {
                                                                            return _vm.deleteRowOption(
                                                                              index,
                                                                              item
                                                                            )
                                                                          }
                                                                        }
                                                                      },
                                                                      [
                                                                        _c(
                                                                          "i",
                                                                          {
                                                                            staticClass:
                                                                              "fas fa-minus-circle"
                                                                          }
                                                                        )
                                                                      ]
                                                                    )
                                                                  ])
                                                                ]
                                                              )
                                                            }
                                                          ),
                                                          _vm._v(" "),
                                                          _c("tr", [
                                                            _c(
                                                              "td",
                                                              {
                                                                attrs: {
                                                                  colspan: "5"
                                                                }
                                                              },
                                                              [
                                                                _c(
                                                                  "button",
                                                                  {
                                                                    staticClass:
                                                                      "btn btn-info btn-sm",
                                                                    attrs: {
                                                                      type:
                                                                        "button"
                                                                    },
                                                                    on: {
                                                                      click: function(
                                                                        $event
                                                                      ) {
                                                                        return _vm.addNewRowOption()
                                                                      }
                                                                    }
                                                                  },
                                                                  [
                                                                    _c("i", {
                                                                      staticClass:
                                                                        "fas fa-plus-circle"
                                                                    })
                                                                  ]
                                                                )
                                                              ]
                                                            )
                                                          ])
                                                        ],
                                                        2
                                                      )
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Obligatoire")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c("el-switch", {
                                                    staticStyle: {
                                                      display: "inline"
                                                    },
                                                    attrs: {
                                                      "active-color": "#13ce66",
                                                      "inactive-color":
                                                        "#ff4949",
                                                      "active-text": "oui",
                                                      "inactive-text": "non"
                                                    },
                                                    model: {
                                                      value: input.required,
                                                      callback: function($$v) {
                                                        _vm.$set(
                                                          input,
                                                          "required",
                                                          $$v
                                                        )
                                                      },
                                                      expression:
                                                        "input.required"
                                                    }
                                                  })
                                                ],
                                                1
                                              )
                                            ])
                                          }
                                        ),
                                        _vm._v(" "),
                                        _c("div", [
                                          _c("div", { staticClass: "mb-1" }, [
                                            _c(
                                              "button",
                                              {
                                                staticClass: "btn btn-info",
                                                attrs: { type: "button" },
                                                on: {
                                                  click: function($event) {
                                                    return _vm.addNewInputSelect()
                                                  }
                                                }
                                              },
                                              [
                                                _c("i", {
                                                  staticClass:
                                                    "fas fa-plus-circle"
                                                })
                                              ]
                                            )
                                          ])
                                        ])
                                      ],
                                      2
                                    )
                                  : _vm._e()
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-md-6" },
                                [
                                  _vm.model.typeChamp == "inputSelect"
                                    ? _c("vue-form-generator", {
                                        attrs: {
                                          schema:
                                            _vm.schema.produits.newInputSelect,
                                          model: _vm.model,
                                          options: _vm.formOptions
                                        }
                                      })
                                    : _vm._e()
                                ],
                                1
                              )
                            ])
                          ])
                        ]
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "modal-footer" }, [
                      _vm.model.typeChamp == "inputTextarea"
                        ? _c(
                            "button",
                            {
                              staticClass: "btn btn-rounded btn-success",
                              on: {
                                click: function($event) {
                                  return _vm.addInputTextarea()
                                }
                              }
                            },
                            [_vm._v("Enregistrer")]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.model.typeChamp == "inputTextarea"
                        ? _c(
                            "button",
                            {
                              staticClass: "btn btn-rounded btn-success",
                              on: {
                                click: function($event) {
                                  return _vm.addInputSelect()
                                }
                              }
                            },
                            [_vm._v("Enregistrer")]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.model.typeChamp == "inputDate"
                        ? _c(
                            "button",
                            {
                              staticClass: "btn btn-rounded btn-success",
                              on: {
                                click: function($event) {
                                  return _vm.addInputDate()
                                }
                              }
                            },
                            [_vm._v("Enregistrer")]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.model.typeChamp == "inputText"
                        ? _c(
                            "button",
                            {
                              staticClass: "btn btn-rounded btn-success",
                              on: {
                                click: function($event) {
                                  return _vm.addInputText()
                                }
                              }
                            },
                            [_vm._v("Enregistrer")]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.model.typeChamp == "inputNumber"
                        ? _c(
                            "button",
                            {
                              staticClass: "btn btn-rounded btn-success",
                              on: {
                                click: function($event) {
                                  return _vm.addInputNumber()
                                }
                              }
                            },
                            [_vm._v("Enregistrer")]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-rounded btn-danger",
                          attrs: { type: "button", "data-dismiss": "modal" }
                        },
                        [_vm._v("Fermer")]
                      )
                    ])
                  ])
                ])
              ]
            )
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            ref: "modalRecette",
            staticClass: "modal fade",
            attrs: { id: "modalRecette", tabindex: "-1", role: "dialog" }
          },
          [_vm._m(13)]
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            staticClass: "modal fade",
            attrs: { id: "modalAddVendeur", tabindex: "-1", role: "dialog" }
          },
          [
            _c(
              "div",
              {
                staticClass: "modal-dialog modal-lg",
                attrs: { role: "document" }
              },
              [
                _c("div", { staticClass: "modal-content" }, [
                  _vm._m(14),
                  _vm._v(" "),
                  _vm._m(15),
                  _vm._v(" "),
                  _c("div", { staticClass: "modal-body" }, [
                    _c("div", { staticClass: "tab-content" }, [
                      _c(
                        "div",
                        {
                          staticClass: "tab-pane active",
                          attrs: { id: "home", role: "tabpanel" }
                        },
                        [
                          _c("div", { staticClass: "row" }, [
                            _c("div", { staticClass: "col-md-12" }),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-6" }, [
                              _c(
                                "div",
                                _vm._l(
                                  _vm.schema.vendeur.active.groups[0].fields,
                                  function(input, index) {
                                    return _c("div", { key: index }, [
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "form-group row mb-1 visibilite"
                                        },
                                        [
                                          _c(
                                            "label",
                                            {
                                              staticClass:
                                                "col-5 col-form-label"
                                            },
                                            [_vm._v("Visibilité")]
                                          ),
                                          _vm._v(" "),
                                          _c("el-switch", {
                                            staticStyle: { display: "inline" },
                                            attrs: {
                                              "active-color": "#13ce66",
                                              "inactive-color": "#ff4949",
                                              "active-text": "activer",
                                              "inactive-text": "désactiver"
                                            },
                                            on: {
                                              change: function($event) {
                                                return _vm.onVisibleChange(
                                                  input,
                                                  index
                                                )
                                              }
                                            },
                                            model: {
                                              value: input.enabled,
                                              callback: function($$v) {
                                                _vm.$set(input, "enabled", $$v)
                                              },
                                              expression: "input.enabled"
                                            }
                                          })
                                        ],
                                        1
                                      )
                                    ])
                                  }
                                ),
                                0
                              )
                            ]),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-md-6" },
                              [
                                _c(
                                  "vue-form-generator",
                                  {
                                    attrs: {
                                      schema:
                                        _vm.schema.vendeur.active.groups[0],
                                      model: _vm.model,
                                      options: _vm.formOptions
                                    },
                                    scopedSlots: _vm._u([
                                      {
                                        key: "field",
                                        fn: function(props) {
                                          return [
                                            _c("tr", [
                                              _c("td", [
                                                _c("h1", [
                                                  _vm._v(
                                                    " " +
                                                      _vm._s(props.field.label)
                                                  )
                                                ])
                                              ]),
                                              _c("td"),
                                              _c("td")
                                            ])
                                          ]
                                        }
                                      }
                                    ])
                                  },
                                  [
                                    _vm._v(
                                      "\n                                            //this would be the general template for each field\n                                            "
                                    )
                                  ]
                                )
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-6" }, [
                              _c(
                                "div",
                                _vm._l(
                                  _vm.schema.vendeur.inactive.groups[0].fields,
                                  function(input, index) {
                                    return _c("div", { key: index }, [
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "form-group row mb-1 visibilite"
                                        },
                                        [
                                          _c(
                                            "label",
                                            {
                                              staticClass:
                                                "col-5 col-form-label"
                                            },
                                            [_vm._v("Visibilité")]
                                          ),
                                          _vm._v(" "),
                                          _c("el-switch", {
                                            staticStyle: { display: "inline" },
                                            attrs: {
                                              "active-color": "#13ce66",
                                              "inactive-color": "#ff4949",
                                              "active-text": "activer",
                                              "inactive-text": "désactiver"
                                            },
                                            on: {
                                              change: function($event) {
                                                return _vm.onVisibleChange(
                                                  input,
                                                  index
                                                )
                                              }
                                            },
                                            model: {
                                              value: input.enabled,
                                              callback: function($$v) {
                                                _vm.$set(input, "enabled", $$v)
                                              },
                                              expression: "input.enabled"
                                            }
                                          })
                                        ],
                                        1
                                      )
                                    ])
                                  }
                                ),
                                0
                              )
                            ]),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-md-6" },
                              [
                                _c(
                                  "vue-form-generator",
                                  {
                                    attrs: {
                                      schema:
                                        _vm.schema.vendeur.inactive.groups[0],
                                      model: _vm.model,
                                      options: _vm.formOptions
                                    },
                                    scopedSlots: _vm._u([
                                      {
                                        key: "field",
                                        fn: function(props) {
                                          return [
                                            _c("tr", [
                                              _c("td", [
                                                _c("h1", [
                                                  _vm._v(
                                                    " " +
                                                      _vm._s(props.field.label)
                                                  )
                                                ])
                                              ]),
                                              _c("td"),
                                              _c("td")
                                            ])
                                          ]
                                        }
                                      }
                                    ])
                                  },
                                  [
                                    _vm._v(
                                      "\n                                            //this would be the general template for each field\n                                            "
                                    )
                                  ]
                                )
                              ],
                              1
                            )
                          ])
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "tab-pane",
                          attrs: { id: "ajout", role: "tabpanel" }
                        },
                        [
                          _c("div", { staticClass: "card-body" }, [
                            _c("div", { staticClass: "form-group row mb-1" }, [
                              _vm._m(16),
                              _vm._v(" "),
                              _c("div", { staticClass: "col-12" }, [
                                _c(
                                  "select",
                                  {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.model.typeChamp,
                                        expression: "model.typeChamp"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: { id: "type" },
                                    on: {
                                      change: [
                                        function($event) {
                                          var $$selectedVal = Array.prototype.filter
                                            .call(
                                              $event.target.options,
                                              function(o) {
                                                return o.selected
                                              }
                                            )
                                            .map(function(o) {
                                              var val =
                                                "_value" in o
                                                  ? o._value
                                                  : o.value
                                              return val
                                            })
                                          _vm.$set(
                                            _vm.model,
                                            "typeChamp",
                                            $event.target.multiple
                                              ? $$selectedVal
                                              : $$selectedVal[0]
                                          )
                                        },
                                        _vm.onInputChange
                                      ]
                                    }
                                  },
                                  [
                                    _c(
                                      "option",
                                      { attrs: { value: "inputText" } },
                                      [_vm._v("Input Texte")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "inputNumber" } },
                                      [_vm._v("Input number")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "inputDate" } },
                                      [_vm._v("Date")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "inputTextarea" } },
                                      [_vm._v("Zone de texte")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "inputSelect" } },
                                      [_vm._v("Input Select")]
                                    )
                                  ]
                                )
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "row" }, [
                              _c("div", { staticClass: "col-md-6" }, [
                                _vm.model.typeChamp == "inputText"
                                  ? _c(
                                      "div",
                                      [
                                        _vm._l(
                                          _vm.schema.produits.newInputText
                                            .fields,
                                          function(input, index) {
                                            return _c("div", { key: index }, [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Libellé")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "col-7" },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value: input.label,
                                                            expression:
                                                              "input.label"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          type: "text",
                                                          id: "lieu"
                                                        },
                                                        domProps: {
                                                          value: input.label
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              input,
                                                              "label",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("PlaceHolder")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "col-7" },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value:
                                                              input.placeholder,
                                                            expression:
                                                              "input.placeholder"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          type: "text",
                                                          id: "lieu"
                                                        },
                                                        domProps: {
                                                          value:
                                                            input.placeholder
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              input,
                                                              "placeholder",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Obligatoire")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c("el-switch", {
                                                    staticStyle: {
                                                      display: "inline"
                                                    },
                                                    attrs: {
                                                      "active-color": "#13ce66",
                                                      "inactive-color":
                                                        "#ff4949",
                                                      "active-text": "oui",
                                                      "inactive-text": "non"
                                                    },
                                                    model: {
                                                      value: input.required,
                                                      callback: function($$v) {
                                                        _vm.$set(
                                                          input,
                                                          "required",
                                                          $$v
                                                        )
                                                      },
                                                      expression:
                                                        "input.required"
                                                    }
                                                  })
                                                ],
                                                1
                                              )
                                            ])
                                          }
                                        ),
                                        _vm._v(" "),
                                        _c("div", [
                                          _c("div", { staticClass: "mb-1" }, [
                                            _c(
                                              "button",
                                              {
                                                staticClass: "btn btn-info",
                                                attrs: { type: "button" },
                                                on: {
                                                  click: function($event) {
                                                    return _vm.addNewInputText()
                                                  }
                                                }
                                              },
                                              [
                                                _c("i", {
                                                  staticClass:
                                                    "fas fa-plus-circle"
                                                })
                                              ]
                                            )
                                          ])
                                        ])
                                      ],
                                      2
                                    )
                                  : _vm._e()
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-md-6" },
                                [
                                  _vm.model.typeChamp == "inputText"
                                    ? _c("vue-form-generator", {
                                        attrs: {
                                          schema:
                                            _vm.schema.produits.newInputText,
                                          model: _vm.model,
                                          options: _vm.formOptions
                                        }
                                      })
                                    : _vm._e()
                                ],
                                1
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "row" }, [
                              _c("div", { staticClass: "col-md-6" }, [
                                _vm.model.typeChamp == "inputNumber"
                                  ? _c(
                                      "div",
                                      [
                                        _vm._l(
                                          _vm.schema.produits.newInputNumber
                                            .fields,
                                          function(input, index) {
                                            return _c("div", { key: index }, [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Libellé")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "col-7" },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value: input.label,
                                                            expression:
                                                              "input.label"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          type: "text",
                                                          id: "lieu"
                                                        },
                                                        domProps: {
                                                          value: input.label
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              input,
                                                              "label",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Obligatoire")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c("el-switch", {
                                                    staticStyle: {
                                                      display: "inline"
                                                    },
                                                    attrs: {
                                                      "active-color": "#13ce66",
                                                      "inactive-color":
                                                        "#ff4949",
                                                      "active-text": "oui",
                                                      "inactive-text": "non"
                                                    },
                                                    model: {
                                                      value: input.required,
                                                      callback: function($$v) {
                                                        _vm.$set(
                                                          input,
                                                          "required",
                                                          $$v
                                                        )
                                                      },
                                                      expression:
                                                        "input.required"
                                                    }
                                                  })
                                                ],
                                                1
                                              )
                                            ])
                                          }
                                        ),
                                        _vm._v(" "),
                                        _c("div", [
                                          _c("div", { staticClass: "mb-1" }, [
                                            _c(
                                              "button",
                                              {
                                                staticClass: "btn btn-info",
                                                attrs: { type: "button" },
                                                on: {
                                                  click: function($event) {
                                                    return _vm.addNewInputNumber()
                                                  }
                                                }
                                              },
                                              [
                                                _c("i", {
                                                  staticClass:
                                                    "fas fa-plus-circle"
                                                })
                                              ]
                                            )
                                          ])
                                        ])
                                      ],
                                      2
                                    )
                                  : _vm._e()
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-md-6" },
                                [
                                  _vm.model.typeChamp == "inputNumber"
                                    ? _c("vue-form-generator", {
                                        attrs: {
                                          schema:
                                            _vm.schema.produits.newInputNumber,
                                          model: _vm.model,
                                          options: _vm.formOptions
                                        }
                                      })
                                    : _vm._e()
                                ],
                                1
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "row" }, [
                              _c("div", { staticClass: "col-md-6" }, [
                                _vm.model.typeChamp == "inputDate"
                                  ? _c(
                                      "div",
                                      [
                                        _vm._l(
                                          _vm.schema.produits.newInputDate
                                            .fields,
                                          function(input, index) {
                                            return _c("div", { key: index }, [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Libellé")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "col-7" },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value: input.label,
                                                            expression:
                                                              "input.label"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          type: "text",
                                                          id: "lieu"
                                                        },
                                                        domProps: {
                                                          value: input.label
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              input,
                                                              "label",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Obligatoire")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c("el-switch", {
                                                    staticStyle: {
                                                      display: "inline"
                                                    },
                                                    attrs: {
                                                      "active-color": "#13ce66",
                                                      "inactive-color":
                                                        "#ff4949",
                                                      "active-text": "oui",
                                                      "inactive-text": "non"
                                                    },
                                                    model: {
                                                      value: input.required,
                                                      callback: function($$v) {
                                                        _vm.$set(
                                                          input,
                                                          "required",
                                                          $$v
                                                        )
                                                      },
                                                      expression:
                                                        "input.required"
                                                    }
                                                  })
                                                ],
                                                1
                                              )
                                            ])
                                          }
                                        ),
                                        _vm._v(" "),
                                        _c("div", [
                                          _c("div", { staticClass: "mb-1" }, [
                                            _c(
                                              "button",
                                              {
                                                staticClass: "btn btn-info",
                                                attrs: { type: "button" },
                                                on: {
                                                  click: function($event) {
                                                    return _vm.addNewInputDate()
                                                  }
                                                }
                                              },
                                              [
                                                _c("i", {
                                                  staticClass:
                                                    "fas fa-plus-circle"
                                                })
                                              ]
                                            )
                                          ])
                                        ])
                                      ],
                                      2
                                    )
                                  : _vm._e()
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-md-6" },
                                [
                                  _vm.model.typeChamp == "inputDate"
                                    ? _c("vue-form-generator", {
                                        attrs: {
                                          schema:
                                            _vm.schema.produits.newInputDate,
                                          model: _vm.model,
                                          options: _vm.formOptions
                                        }
                                      })
                                    : _vm._e()
                                ],
                                1
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "row" }, [
                              _c("div", { staticClass: "col-md-6" }, [
                                _vm.model.typeChamp == "inputTextarea"
                                  ? _c(
                                      "div",
                                      [
                                        _vm._l(
                                          _vm.schema.produits.newInputTextarea
                                            .fields,
                                          function(input, index) {
                                            return _c("div", { key: index }, [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Libellé")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "col-7" },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value: input.label,
                                                            expression:
                                                              "input.label"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          type: "text",
                                                          id: "lieu"
                                                        },
                                                        domProps: {
                                                          value: input.label
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              input,
                                                              "label",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Lignes")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "col-7" },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value: input.rows,
                                                            expression:
                                                              "input.rows"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          type: "number",
                                                          id: "lieu"
                                                        },
                                                        domProps: {
                                                          value: input.rows
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              input,
                                                              "rows",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("PlaceHolder")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "col-7" },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value:
                                                              input.placeholder,
                                                            expression:
                                                              "input.placeholder"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          type: "text",
                                                          id: "lieu"
                                                        },
                                                        domProps: {
                                                          value:
                                                            input.placeholder
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              input,
                                                              "placeholder",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Max caractères")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "col-7" },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value: input.max,
                                                            expression:
                                                              "input.max"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          type: "number",
                                                          id: "lieu"
                                                        },
                                                        domProps: {
                                                          value: input.max
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              input,
                                                              "max",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Obligatoire")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c("el-switch", {
                                                    staticStyle: {
                                                      display: "inline"
                                                    },
                                                    attrs: {
                                                      "active-color": "#13ce66",
                                                      "inactive-color":
                                                        "#ff4949",
                                                      "active-text": "oui",
                                                      "inactive-text": "non"
                                                    },
                                                    model: {
                                                      value: input.required,
                                                      callback: function($$v) {
                                                        _vm.$set(
                                                          input,
                                                          "required",
                                                          $$v
                                                        )
                                                      },
                                                      expression:
                                                        "input.required"
                                                    }
                                                  })
                                                ],
                                                1
                                              )
                                            ])
                                          }
                                        ),
                                        _vm._v(" "),
                                        _c("div", [
                                          _c("div", { staticClass: "mb-1" }, [
                                            _c(
                                              "button",
                                              {
                                                staticClass: "btn btn-info",
                                                attrs: { type: "button" },
                                                on: {
                                                  click: function($event) {
                                                    return _vm.addNewInputTextarea()
                                                  }
                                                }
                                              },
                                              [
                                                _c("i", {
                                                  staticClass:
                                                    "fas fa-plus-circle"
                                                })
                                              ]
                                            )
                                          ])
                                        ])
                                      ],
                                      2
                                    )
                                  : _vm._e()
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-md-6" },
                                [
                                  _vm.model.typeChamp == "inputTextarea"
                                    ? _c("vue-form-generator", {
                                        attrs: {
                                          schema:
                                            _vm.schema.produits
                                              .newInputTextarea,
                                          model: _vm.model,
                                          options: _vm.formOptions
                                        }
                                      })
                                    : _vm._e()
                                ],
                                1
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "row" }, [
                              _c("div", { staticClass: "col-md-6" }, [
                                _vm.model.typeChamp == "inputSelect"
                                  ? _c(
                                      "div",
                                      [
                                        _vm._l(
                                          _vm.schema.produits.newInputSelect
                                            .fields,
                                          function(input, index) {
                                            return _c("div", { key: index }, [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Libellé")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "col-7" },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value: input.label,
                                                            expression:
                                                              "input.label"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          type: "text",
                                                          id: "lieu"
                                                        },
                                                        domProps: {
                                                          value: input.label
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              input,
                                                              "label",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "table-responsive"
                                                },
                                                [
                                                  _c(
                                                    "table",
                                                    {
                                                      staticClass:
                                                        "table editable-table table-bordered table-1 m-b-0 color-bordered-table info-bordered-table"
                                                    },
                                                    [
                                                      _vm._m(17, true),
                                                      _vm._v(" "),
                                                      _c(
                                                        "tbody",
                                                        [
                                                          _vm._l(
                                                            input.values,
                                                            function(
                                                              item,
                                                              index
                                                            ) {
                                                              return _c(
                                                                "tr",
                                                                { key: index },
                                                                [
                                                                  _vm._v(
                                                                    "\n                                                    \n                                        s          "
                                                                  ),
                                                                  _c("td", [
                                                                    _c(
                                                                      "input",
                                                                      {
                                                                        directives: [
                                                                          {
                                                                            name:
                                                                              "model",
                                                                            rawName:
                                                                              "v-model",
                                                                            value:
                                                                              item.name,
                                                                            expression:
                                                                              "item.name"
                                                                          }
                                                                        ],
                                                                        staticClass:
                                                                          "form-control",
                                                                        attrs: {
                                                                          type:
                                                                            "text"
                                                                        },
                                                                        domProps: {
                                                                          value:
                                                                            item.name
                                                                        },
                                                                        on: {
                                                                          input: function(
                                                                            $event
                                                                          ) {
                                                                            if (
                                                                              $event
                                                                                .target
                                                                                .composing
                                                                            ) {
                                                                              return
                                                                            }
                                                                            _vm.$set(
                                                                              item,
                                                                              "name",
                                                                              $event
                                                                                .target
                                                                                .value
                                                                            )
                                                                          }
                                                                        }
                                                                      }
                                                                    )
                                                                  ]),
                                                                  _vm._v(" "),
                                                                  _c("td", [
                                                                    _c(
                                                                      "button",
                                                                      {
                                                                        staticClass:
                                                                          "btn btn-danger btn-sm",
                                                                        attrs: {
                                                                          type:
                                                                            "button"
                                                                        },
                                                                        on: {
                                                                          click: function(
                                                                            $event
                                                                          ) {
                                                                            return _vm.deleteRowOption(
                                                                              index,
                                                                              item
                                                                            )
                                                                          }
                                                                        }
                                                                      },
                                                                      [
                                                                        _c(
                                                                          "i",
                                                                          {
                                                                            staticClass:
                                                                              "fas fa-minus-circle"
                                                                          }
                                                                        )
                                                                      ]
                                                                    )
                                                                  ])
                                                                ]
                                                              )
                                                            }
                                                          ),
                                                          _vm._v(" "),
                                                          _c("tr", [
                                                            _c(
                                                              "td",
                                                              {
                                                                attrs: {
                                                                  colspan: "5"
                                                                }
                                                              },
                                                              [
                                                                _c(
                                                                  "button",
                                                                  {
                                                                    staticClass:
                                                                      "btn btn-info btn-sm",
                                                                    attrs: {
                                                                      type:
                                                                        "button"
                                                                    },
                                                                    on: {
                                                                      click: function(
                                                                        $event
                                                                      ) {
                                                                        return _vm.addNewRowOption()
                                                                      }
                                                                    }
                                                                  },
                                                                  [
                                                                    _c("i", {
                                                                      staticClass:
                                                                        "fas fa-plus-circle"
                                                                    })
                                                                  ]
                                                                )
                                                              ]
                                                            )
                                                          ])
                                                        ],
                                                        2
                                                      )
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "form-group row mb-1"
                                                },
                                                [
                                                  _c(
                                                    "label",
                                                    {
                                                      staticClass:
                                                        "col-5 col-form-label"
                                                    },
                                                    [_vm._v("Obligatoire")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c("el-switch", {
                                                    staticStyle: {
                                                      display: "inline"
                                                    },
                                                    attrs: {
                                                      "active-color": "#13ce66",
                                                      "inactive-color":
                                                        "#ff4949",
                                                      "active-text": "oui",
                                                      "inactive-text": "non"
                                                    },
                                                    model: {
                                                      value: input.required,
                                                      callback: function($$v) {
                                                        _vm.$set(
                                                          input,
                                                          "required",
                                                          $$v
                                                        )
                                                      },
                                                      expression:
                                                        "input.required"
                                                    }
                                                  })
                                                ],
                                                1
                                              )
                                            ])
                                          }
                                        ),
                                        _vm._v(" "),
                                        _c("div", [
                                          _c("div", { staticClass: "mb-1" }, [
                                            _c(
                                              "button",
                                              {
                                                staticClass: "btn btn-info",
                                                attrs: { type: "button" },
                                                on: {
                                                  click: function($event) {
                                                    return _vm.addNewInputSelect()
                                                  }
                                                }
                                              },
                                              [
                                                _c("i", {
                                                  staticClass:
                                                    "fas fa-plus-circle"
                                                })
                                              ]
                                            )
                                          ])
                                        ])
                                      ],
                                      2
                                    )
                                  : _vm._e()
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-md-6" },
                                [
                                  _vm.model.typeChamp == "inputSelect"
                                    ? _c("vue-form-generator", {
                                        attrs: {
                                          schema:
                                            _vm.schema.produits.newInputSelect,
                                          model: _vm.model,
                                          options: _vm.formOptions
                                        }
                                      })
                                    : _vm._e()
                                ],
                                1
                              )
                            ])
                          ])
                        ]
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "modal-footer" }, [
                      _vm.model.typeChamp == "inputTextarea"
                        ? _c(
                            "button",
                            {
                              staticClass: "btn btn-rounded btn-success",
                              on: {
                                click: function($event) {
                                  return _vm.addInputTextarea()
                                }
                              }
                            },
                            [_vm._v("Enregistrer")]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.model.typeChamp == "inputTextarea"
                        ? _c(
                            "button",
                            {
                              staticClass: "btn btn-rounded btn-success",
                              on: {
                                click: function($event) {
                                  return _vm.addInputSelect()
                                }
                              }
                            },
                            [_vm._v("Enregistrer")]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.model.typeChamp == "inputDate"
                        ? _c(
                            "button",
                            {
                              staticClass: "btn btn-rounded btn-success",
                              on: {
                                click: function($event) {
                                  return _vm.addInputDate()
                                }
                              }
                            },
                            [_vm._v("Enregistrer")]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.model.typeChamp == "inputText"
                        ? _c(
                            "button",
                            {
                              staticClass: "btn btn-rounded btn-success",
                              on: {
                                click: function($event) {
                                  return _vm.addInputText()
                                }
                              }
                            },
                            [_vm._v("Enregistrer")]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.model.typeChamp == "inputNumber"
                        ? _c(
                            "button",
                            {
                              staticClass: "btn btn-rounded btn-success",
                              on: {
                                click: function($event) {
                                  return _vm.addInputNumber()
                                }
                              }
                            },
                            [_vm._v("Enregistrer")]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-rounded btn-danger",
                          attrs: { type: "button", "data-dismiss": "modal" }
                        },
                        [_vm._v("Fermer")]
                      )
                    ])
                  ])
                ])
              ]
            )
          ]
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row page-titles" }, [
      _c("div", { staticClass: "col-md-5 align-self-center" }, [
        _c("h3", { staticClass: "text-themecolor" }, [
          _c("b", [_vm._v("Formulaire Recettes")])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-7 align-self-center" }, [
        _c("ol", { staticClass: "breadcrumb" }, [
          _c("li", { staticClass: "breadcrumb-item" }, [
            _c("a", { attrs: { href: "/bp/home" } }, [_vm._v("Accueil")])
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "breadcrumb-item" }, [
            _c("a", { attrs: { href: "bp/suivi" } }, [_vm._v(" Finances")])
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "breadcrumb-item" }, [_vm._v("Recettes")])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-2" }, [
      _c("div", { staticClass: "card" }, [
        _c("div", { staticClass: "card-body" })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("strong", [
      _c("h2", [
        _c("i", { staticClass: "el-icon-shopping-cart-1" }, [
          _c("span", { staticClass: "titletable" }, [
            _vm._v("Detail de l'entrée")
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("strong", [
      _c("h2", [
        _c("i", { staticClass: "el-icon-money" }, [
          _c("span", { staticClass: "titletable" }, [_vm._v("Dépot de fonds")])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c("h4", { staticClass: "modal-title" }, [_vm._v("Produits")]),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "ul",
      { staticClass: "nav nav-tabs profile-tab", attrs: { role: "tablist" } },
      [
        _c("li", { staticClass: "nav-item" }, [
          _c(
            "a",
            {
              staticClass: "nav-link active",
              attrs: { "data-toggle": "tab", href: "#home", role: "tab" }
            },
            [_vm._v("Liste des champs")]
          )
        ]),
        _vm._v(" "),
        _c("li", { staticClass: "nav-item" }, [
          _c(
            "a",
            {
              staticClass: "nav-link",
              attrs: { "data-toggle": "tab", href: "#ajout", role: "tab" }
            },
            [_vm._v("Ajouter un champs")]
          )
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "label",
      {
        staticClass: "col-5 col-form-label p-r-0",
        attrs: { for: "example-text-input" }
      },
      [
        _c("span", { staticClass: "text-red" }, [_vm._v("*")]),
        _vm._v(" Type Champs\n                                            ")
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { attrs: { width: "100%" } }, [_vm._v("Options")]),
        _vm._v(" "),
        _c("th", { attrs: { width: "4%" } }, [_vm._v("Action")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass: "modal-dialog modal-dialog-centered",
        attrs: { role: "document" }
      },
      [
        _c("div", { staticClass: "modal-content" }, [
          _c("div", { staticClass: "modal-header" }, [
            _c("h4", { staticClass: "modal-title" }, [_vm._v("Modal!")]),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass: "close",
                attrs: {
                  type: "button",
                  "data-dismiss": "modal",
                  "aria-label": "Close"
                }
              },
              [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "modal-body" }, [
            _c("h1", [_vm._v("Modal")])
          ])
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c("h4", { staticClass: "modal-title" }, [_vm._v("Client")]),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "ul",
      { staticClass: "nav nav-tabs profile-tab", attrs: { role: "tablist" } },
      [
        _c("li", { staticClass: "nav-item" }, [
          _c(
            "a",
            {
              staticClass: "nav-link active",
              attrs: { "data-toggle": "tab", href: "#home", role: "tab" }
            },
            [_vm._v("Liste des champs")]
          )
        ]),
        _vm._v(" "),
        _c("li", { staticClass: "nav-item" }, [
          _c(
            "a",
            {
              staticClass: "nav-link",
              attrs: { "data-toggle": "tab", href: "#ajout", role: "tab" }
            },
            [_vm._v("Ajouter un champs")]
          )
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "label",
      {
        staticClass: "col-5 col-form-label p-r-0",
        attrs: { for: "example-text-input" }
      },
      [
        _c("span", { staticClass: "text-red" }, [_vm._v("*")]),
        _vm._v(" Type Champs\n                                                ")
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { attrs: { width: "100%" } }, [_vm._v("Options")]),
        _vm._v(" "),
        _c("th", { attrs: { width: "4%" } }, [_vm._v("Action")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass: "modal-dialog modal-dialog-centered",
        attrs: { role: "document" }
      },
      [
        _c("div", { staticClass: "modal-content" }, [
          _c("div", { staticClass: "modal-header" }, [
            _c("h4", { staticClass: "modal-title" }, [_vm._v("Modal!")]),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass: "close",
                attrs: {
                  type: "button",
                  "data-dismiss": "modal",
                  "aria-label": "Close"
                }
              },
              [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "modal-body" }, [
            _c("h1", [_vm._v("Modal")])
          ])
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c("h4", { staticClass: "modal-title" }, [_vm._v("Vendeur")]),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "ul",
      { staticClass: "nav nav-tabs profile-tab", attrs: { role: "tablist" } },
      [
        _c("li", { staticClass: "nav-item" }, [
          _c(
            "a",
            {
              staticClass: "nav-link active",
              attrs: { "data-toggle": "tab", href: "#home", role: "tab" }
            },
            [_vm._v("Liste des champs")]
          )
        ]),
        _vm._v(" "),
        _c("li", { staticClass: "nav-item" }, [
          _c(
            "a",
            {
              staticClass: "nav-link",
              attrs: { "data-toggle": "tab", href: "#ajout", role: "tab" }
            },
            [_vm._v("Ajouter un champs")]
          )
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "label",
      {
        staticClass: "col-5 col-form-label p-r-0",
        attrs: { for: "example-text-input" }
      },
      [
        _c("span", { staticClass: "text-red" }, [_vm._v("*")]),
        _vm._v(" Type Champs\n                                            ")
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { attrs: { width: "100%" } }, [_vm._v("Options")]),
        _vm._v(" "),
        _c("th", { attrs: { width: "4%" } }, [_vm._v("Action")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/assets/js/views/suivi/parametres/formulaires/recettes.vue":
/*!*****************************************************************************!*\
  !*** ./resources/assets/js/views/suivi/parametres/formulaires/recettes.vue ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _recettes_vue_vue_type_template_id_6de11c68_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./recettes.vue?vue&type=template&id=6de11c68&scoped=true& */ "./resources/assets/js/views/suivi/parametres/formulaires/recettes.vue?vue&type=template&id=6de11c68&scoped=true&");
/* harmony import */ var _recettes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./recettes.vue?vue&type=script&lang=js& */ "./resources/assets/js/views/suivi/parametres/formulaires/recettes.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _recettes_vue_vue_type_style_index_0_id_6de11c68_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./recettes.vue?vue&type=style&index=0&id=6de11c68&scoped=true&lang=css& */ "./resources/assets/js/views/suivi/parametres/formulaires/recettes.vue?vue&type=style&index=0&id=6de11c68&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _recettes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _recettes_vue_vue_type_template_id_6de11c68_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _recettes_vue_vue_type_template_id_6de11c68_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "6de11c68",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/views/suivi/parametres/formulaires/recettes.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/assets/js/views/suivi/parametres/formulaires/recettes.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************!*\
  !*** ./resources/assets/js/views/suivi/parametres/formulaires/recettes.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_recettes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./recettes.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/parametres/formulaires/recettes.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_recettes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/views/suivi/parametres/formulaires/recettes.vue?vue&type=style&index=0&id=6de11c68&scoped=true&lang=css&":
/*!**************************************************************************************************************************************!*\
  !*** ./resources/assets/js/views/suivi/parametres/formulaires/recettes.vue?vue&type=style&index=0&id=6de11c68&scoped=true&lang=css& ***!
  \**************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_recettes_vue_vue_type_style_index_0_id_6de11c68_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/style-loader!../../../../../../../node_modules/css-loader??ref--7-1!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./recettes.vue?vue&type=style&index=0&id=6de11c68&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/parametres/formulaires/recettes.vue?vue&type=style&index=0&id=6de11c68&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_recettes_vue_vue_type_style_index_0_id_6de11c68_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_recettes_vue_vue_type_style_index_0_id_6de11c68_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_recettes_vue_vue_type_style_index_0_id_6de11c68_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_recettes_vue_vue_type_style_index_0_id_6de11c68_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_recettes_vue_vue_type_style_index_0_id_6de11c68_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/assets/js/views/suivi/parametres/formulaires/recettes.vue?vue&type=template&id=6de11c68&scoped=true&":
/*!************************************************************************************************************************!*\
  !*** ./resources/assets/js/views/suivi/parametres/formulaires/recettes.vue?vue&type=template&id=6de11c68&scoped=true& ***!
  \************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_recettes_vue_vue_type_template_id_6de11c68_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./recettes.vue?vue&type=template&id=6de11c68&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/parametres/formulaires/recettes.vue?vue&type=template&id=6de11c68&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_recettes_vue_vue_type_template_id_6de11c68_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_recettes_vue_vue_type_template_id_6de11c68_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);
//# sourceMappingURL=suivi.parametres.formulaires.recettes.js.map