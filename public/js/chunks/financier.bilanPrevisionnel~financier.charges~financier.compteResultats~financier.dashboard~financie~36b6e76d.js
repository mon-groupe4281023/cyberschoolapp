(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["financier.bilanPrevisionnel~financier.charges~financier.compteResultats~financier.dashboard~financie~36b6e76d"],{

/***/ "./resources/assets/js/views/bp/financier/default.chargePatronale.json":
/*!*****************************************************************************!*\
  !*** ./resources/assets/js/views/bp/financier/default.chargePatronale.json ***!
  \*****************************************************************************/
/*! exports provided: dirigeant, employe, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"dirigeant\":22.6,\"employe\":9.5}");

/***/ }),

/***/ "./resources/assets/js/views/bp/mixins/financier/charges.js":
/*!******************************************************************!*\
  !*** ./resources/assets/js/views/bp/mixins/financier/charges.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _services_helper_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../services/helper.js */ "./resources/assets/js/services/helper.js");
/* harmony import */ var _currenciesMask__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../currenciesMask */ "./resources/assets/js/views/bp/mixins/currenciesMask.js");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! util */ "./node_modules/node-libs-browser/node_modules/util/util.js");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(util__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _financier_default_chargePatronale_json__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../financier/default.chargePatronale.json */ "./resources/assets/js/views/bp/financier/default.chargePatronale.json");
var _financier_default_chargePatronale_json__WEBPACK_IMPORTED_MODULE_3___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../financier/default.chargePatronale.json */ "./resources/assets/js/views/bp/financier/default.chargePatronale.json", 1);
// assets/js/views/bp/mixins/financier/charges




/* harmony default export */ __webpack_exports__["default"] = ({
  mixins: [_currenciesMask__WEBPACK_IMPORTED_MODULE_1__["default"]],
  data: function data() {
    return {
      /* totuax et sous-totaux pour achat de matiere premiere */
      totalAchatMpDepVariable: {
        annee1: 0,
        annee2: 0,
        annee3: 0,
        depensesMensuellesLancement: {
          "01": {
            "ttc": 0,
            "ht": 0
          },
          "02": {
            "ttc": 0,
            "ht": 0
          },
          "03": {
            "ttc": 0,
            "ht": 0
          },
          "04": {
            "ttc": 0,
            "ht": 0
          },
          "05": {
            "ttc": 0,
            "ht": 0
          },
          "06": {
            "ttc": 0,
            "ht": 0
          },
          "07": {
            "ttc": 0,
            "ht": 0
          },
          "08": {
            "ttc": 0,
            "ht": 0
          },
          "09": {
            "ttc": 0,
            "ht": 0
          },
          "10": {
            "ttc": 0,
            "ht": 0
          },
          "11": {
            "ttc": 0,
            "ht": 0
          },
          "12": {
            "ttc": 0,
            "ht": 0
          }
        },
        depensesLancement: {
          ht: 0,
          ttc: 0
        }
      },
      totalAchatMpCharVariable: {
        annee1: 0,
        annee2: 0,
        annee3: 0
      },

      /* Service exterieurs */
      totalServExtFrais: {
        annee1: 0,
        annee2: 0,
        annee3: 0,
        depensesMensuellesLancement: {
          "01": 0,
          "02": 0,
          "03": 0,
          "04": 0,
          "05": 0,
          "06": 0,
          "07": 0,
          "08": 0,
          "09": 0,
          "10": 0,
          "11": 0,
          "12": 0
        },
        depensesLancement: 0
      },

      /* Taxes et impots */
      subTotalTaxImpImpot: {
        annee1: 0,
        annee2: 0,
        annee3: 0
      },
      subTotalTaxImpMairie: {
        annee1: 0,
        annee2: 0,
        annee3: 0
      },
      subTotalFraisSalarie: {
        annee1: 0,
        annee2: 0,
        annee3: 0,
        effectif: 0
      },
      subTotalFraisDirigeant: {
        annee1: 0,
        annee2: 0,
        annee3: 0,
        effectif: 0
      },
      subTotalFraisStagiaire: {
        annee1: 0,
        annee2: 0,
        annee3: 0,
        effectif: 0
      },
      subTotalFraisCharge: {
        annee1: 0,
        annee2: 0,
        annee3: 0
      },
      totalFraisPersonnel: {
        annee1: 0,
        annee2: 0,
        annee3: 0,
        effectif: 0,
        accroissement1: 0,
        accroissement2: 0
      },
      paramChargePatronaleDirigeant: 0,
      paramChargePatronaleEmploye: 0
    };
  },
  created: function created() {
    /** initialisation des charges variables dns la mixin 
     * on aura juste besoin de mettre a jour depuis sur les etats-f
    */
    this.paramChargePatronaleEmploye = _financier_default_chargePatronale_json__WEBPACK_IMPORTED_MODULE_3__.employe;
    this.paramChargePatronaleDirigeant = _financier_default_chargePatronale_json__WEBPACK_IMPORTED_MODULE_3__.dirigeant;
  },
  methods: {
    /**
     * 
     * @param {Array} ventesMensuelles 
     * @return {int}
     */
    totalAchatHTChargeProduitAnnuelle: function totalAchatHTChargeProduitAnnuelle(ventesMensuelles) {
      var result = 0;

      if (!Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["isEmpty"])(ventesMensuelles)) {
        for (var property in ventesMensuelles) {
          result += ventesMensuelles[property].ht;
        }
      }

      return result;
    },

    /**
     * 
     * @param {Array} ventesMensuelles 
     * @return {int}
     */
    totalAchatTTCChargeProduitAnnuelle: function totalAchatTTCChargeProduitAnnuelle(ventesMensuelles) {
      var result = 0;

      if (!Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["isEmpty"])(ventesMensuelles)) {
        for (var property in ventesMensuelles) {
          result += ventesMensuelles[property].ttc;
        }
      }

      return result;
    },

    /**
     * 
     * @param {Array} ventesMensuelles 
     * @param {string} month
     * @return {int}
     */
    calculTvaChargeProduitMensuelle: function calculTvaChargeProduitMensuelle(ventesMensuelles, month) {
      var result = 0;

      if (!Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["isEmpty"])(ventesMensuelles)) {
        result = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["isEmpty"])(ventesMensuelles[month]) ? 0 : ventesMensuelles[month].ttc - ventesMensuelles[month].ht;
      }

      return result;
    },

    /**
     * 
     * @param {Array} ventesMensuelles 
     * @return {int}
     */
    calculTvaChargeProduitAnnuelle: function calculTvaChargeProduitAnnuelle(ventesMensuelles) {
      var result = 0;

      if (!Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["isEmpty"])(ventesMensuelles)) {
        for (var property in ventesMensuelles) {
          result += ventesMensuelles[property].ttc - ventesMensuelles[property].ht;
        }
      }

      return result;
    },
    accroissementPrevisionnelAnnee3: function accroissementPrevisionnelAnnee3(item, section, collection) {
      item.annee3 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])((1 + item.accroissement2 / 100) * item.annee2, this.currentCurrencyMask[':precision']);

      switch (section) {
        case "achat.depense":
          this.calculTotalAchatMpDepVariable(collection);
          break;

        case "achat.charge":
          this.calculTotalAchatMpCharVariable(collection);
          break;

        case "service.frais":
          this.calculTotalServExtFrais(collection);
          break;

        case "taxe.impot":
          this.calculSubTotalTaxImpImpot(collection);
          break;

        case "taxe.mairie":
          this.calculSubTotalTaxeImpMairie(collection);
          break;

        case "frais.salarie":
          this.calculSubTotalFraisSalarie(collection);
          break;

        case "frais.dirigeant":
          this.calculSubTotalFraisDirigeant(collection);
          break;

        case "frais.stagiaire":
          this.calculSubTotalFraisStagiaire(collection);
          break;
      }
    },

    /* Mise a jours des depenses previsionnelles pour l'anne2*/
    accroissementPrevisionnelAnnee2: function accroissementPrevisionnelAnnee2(item, section, collection) {
      item.annee2 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])((1 + item.accroissement1 / 100) * item.annee1, this.currentCurrencyMask[':precision']);
      this.accroissementPrevisionnelAnnee3(item, section, collection); //this.capitalPrevisionnelAccruAnnee3(item);
    },

    /* calcul le capital investit lors de la premiere annee */
    totalInitial: function totalInitial(item, section, collection) {
      item.annee1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(item.quantite * item.prix, this.currentCurrencyMask[':precision']);
      this.accroissementPrevisionnelAnnee2(item, section, collection); //this.capitalPrevisionnelAccruAnnee2(item);
    },
    calculTotalAchatMpDepVariableHT: function calculTotalAchatMpDepVariableHT(depensesVariablesMP, tva) {
      var _this = this;

      var sumAnnee1 = 0;
      var sumAnnee2 = 0;
      var sumAnnee3 = 0;

      for (var mois in this.totalAchatMpDepVariable.depensesMensuellesLancement) {
        this.totalAchatMpDepVariable.depensesMensuellesLancement[mois].ttc = 0;
        this.totalAchatMpDepVariable.depensesMensuellesLancement[mois].ht = 0;
      }

      this.totalAchatMpDepVariable.depensesLancement.ht = 0;
      this.totalAchatMpDepVariable.depensesLancement.ttc = 0; // TODO: check that it is not empty

      if (depensesVariablesMP) {
        depensesVariablesMP.forEach(function (item) {
          sumAnnee1 = sumAnnee1 + Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["HT"])(item.annee1, tva);
          sumAnnee2 = sumAnnee2 + Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["HT"])(item.annee2, tva);
          sumAnnee3 = sumAnnee3 + Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["HT"])(item.annee3, tva);

          for (var _mois in _this.totalAchatMpDepVariable.depensesMensuellesLancement) {
            _this.totalAchatMpDepVariable.depensesMensuellesLancement[_mois].ttc = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(_this.totalAchatMpDepVariable.depensesMensuellesLancement[_mois].ttc + item.ventes[_mois].ttc, _this.currentCurrencyMask[':precision']);
            _this.totalAchatMpDepVariable.depensesMensuellesLancement[_mois].ht = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(_this.totalAchatMpDepVariable.depensesMensuellesLancement[_mois].ht + item.ventes[_mois].ht, _this.currentCurrencyMask[':precision']);
            _this.totalAchatMpDepVariable.depensesLancement.ht = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(_this.totalAchatMpDepVariable.depensesLancement.ht + item.ventes[_mois].ht, _this.currentCurrencyMask[':precision']);
            _this.totalAchatMpDepVariable.depensesLancement.ttc = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(_this.totalAchatMpDepVariable.depensesLancement.ttc + item.ventes[_mois].ttc, _this.currentCurrencyMask[':precision']);
          }
        });
      }

      this.totalAchatMpDepVariable.annee1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee1, this.currentCurrencyMask[':precision']);
      this.totalAchatMpDepVariable.annee2 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee2, this.currentCurrencyMask[':precision']);
      this.totalAchatMpDepVariable.annee3 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee3, this.currentCurrencyMask[':precision']);
    },
    calculTotalAchatMpCharVariableHT: function calculTotalAchatMpCharVariableHT(chargesVariablesMP, tva) {
      var sumAnnee1 = 0;
      var sumAnnee2 = 0;
      var sumAnnee3 = 0;

      if (chargesVariablesMP) {
        chargesVariablesMP.forEach(function (item) {
          sumAnnee1 = sumAnnee1 + Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["HT"])(item.annee1, tva);
          sumAnnee2 = sumAnnee2 + Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["HT"])(item.annee2, tva);
          sumAnnee3 = sumAnnee3 + Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["HT"])(item.annee3, tva);
        });
      }

      this.totalAchatMpCharVariable.annee1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee1, this.currentCurrencyMask[':precision']);
      this.totalAchatMpCharVariable.annee2 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee2, this.currentCurrencyMask[':precision']);
      this.totalAchatMpCharVariable.annee3 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee3, this.currentCurrencyMask[':precision']);
    },
    calculTotalAchatMpDepVariable: function calculTotalAchatMpDepVariable(depensesVariablesMP) {
      var _this2 = this;

      var sumAnnee1 = 0;
      var sumAnnee2 = 0;
      var sumAnnee3 = 0;

      for (var mois in this.totalAchatMpDepVariable.depensesMensuellesLancement) {
        this.totalAchatMpDepVariable.depensesMensuellesLancement[mois].ttc = 0;
        this.totalAchatMpDepVariable.depensesMensuellesLancement[mois].ht = 0;
      }

      this.totalAchatMpDepVariable.depensesLancement.ht = 0;
      this.totalAchatMpDepVariable.depensesLancement.ttc = 0; // TODO: check that it is not empty

      if (depensesVariablesMP) {
        depensesVariablesMP.forEach(function (item) {
          sumAnnee1 = sumAnnee1 + item.annee1;
          sumAnnee2 = sumAnnee2 + item.annee2;
          sumAnnee3 = sumAnnee3 + item.annee3;

          for (var _mois2 in _this2.totalAchatMpDepVariable.depensesMensuellesLancement) {
            _this2.totalAchatMpDepVariable.depensesMensuellesLancement[_mois2].ttc = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(_this2.totalAchatMpDepVariable.depensesMensuellesLancement[_mois2].ttc + item.ventes[_mois2].ttc, _this2.currentCurrencyMask[':precision']);
            _this2.totalAchatMpDepVariable.depensesMensuellesLancement[_mois2].ht = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(_this2.totalAchatMpDepVariable.depensesMensuellesLancement[_mois2].ht + item.ventes[_mois2].ht, _this2.currentCurrencyMask[':precision']);
            _this2.totalAchatMpDepVariable.depensesLancement.ht = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(_this2.totalAchatMpDepVariable.depensesLancement.ht + item.ventes[_mois2].ht, _this2.currentCurrencyMask[':precision']);
            _this2.totalAchatMpDepVariable.depensesLancement.ttc = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(_this2.totalAchatMpDepVariable.depensesLancement.ttc + item.ventes[_mois2].ttc, _this2.currentCurrencyMask[':precision']);
          }
        });
      }

      this.totalAchatMpDepVariable.annee1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee1, this.currentCurrencyMask[':precision']);
      this.totalAchatMpDepVariable.annee2 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee2, this.currentCurrencyMask[':precision']);
      this.totalAchatMpDepVariable.annee3 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee3, this.currentCurrencyMask[':precision']);
    },
    calculTotalAchatMpCharVariable: function calculTotalAchatMpCharVariable(chargesVariablesMP) {
      var sumAnnee1 = 0;
      var sumAnnee2 = 0;
      var sumAnnee3 = 0;

      if (chargesVariablesMP) {
        chargesVariablesMP.forEach(function (item) {
          sumAnnee1 = sumAnnee1 + item.annee1;
          sumAnnee2 = sumAnnee2 + item.annee2;
          sumAnnee3 = sumAnnee3 + item.annee3;
        });
      }

      this.totalAchatMpCharVariable.annee1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee1, this.currentCurrencyMask[':precision']);
      this.totalAchatMpCharVariable.annee2 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee2, this.currentCurrencyMask[':precision']);
      this.totalAchatMpCharVariable.annee3 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee3, this.currentCurrencyMask[':precision']);
    },
    calculTotalServExtFrais: function calculTotalServExtFrais(servicesExterieurs) {
      var _this3 = this;

      var sumAnnee1 = 0;
      var sumAnnee2 = 0;
      var sumAnnee3 = 0;

      for (var mois in this.totalServExtFrais.depensesMensuellesLancement) {
        this.totalServExtFrais.depensesMensuellesLancement[mois] = 0;
      }

      if (servicesExterieurs) {
        servicesExterieurs.forEach(function (item) {
          sumAnnee1 = sumAnnee1 + item.annee1;
          sumAnnee2 = sumAnnee2 + item.annee2;
          sumAnnee3 = sumAnnee3 + item.annee3;

          for (var _mois3 in _this3.totalServExtFrais.depensesMensuellesLancement) {
            _this3.totalServExtFrais.depensesMensuellesLancement[_mois3] = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(_this3.totalServExtFrais.depensesMensuellesLancement[_mois3] + _this3.calculServiceExterieurDuMois(item, Number(_mois3)), _this3.currentCurrencyMask[':precision']);
            _this3.totalServExtFrais.depensesLancement = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(_this3.totalServExtFrais.depensesLancement + _this3.calculServiceExterieurDuMois(item, Number(_mois3)), _this3.currentCurrencyMask[':precision']);
          }
        });
      }

      this.totalServExtFrais.annee1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee1, this.currentCurrencyMask[':precision']);
      this.totalServExtFrais.annee2 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee2, this.currentCurrencyMask[':precision']);
      this.totalServExtFrais.annee3 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee3, this.currentCurrencyMask[':precision']);
    },
    calculServiceExterieurDuMois: function calculServiceExterieurDuMois(service, moisDurant) {
      if (moisDurant <= service.quantite) {
        return service.prix;
      }

      return 0;
    },
    calculTotalServExtFraisHT: function calculTotalServExtFraisHT(servicesExterieurs, tva) {
      var _this4 = this;

      var sumAnnee1 = 0;
      var sumAnnee2 = 0;
      var sumAnnee3 = 0;

      for (var mois in this.totalServExtFrais.depensesMensuellesLancement) {
        this.totalServExtFrais.depensesMensuellesLancement[mois] = 0;
      }

      if (servicesExterieurs) {
        servicesExterieurs.forEach(function (item) {
          sumAnnee1 = sumAnnee1 + Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["HT"])(item.annee1, tva);
          sumAnnee2 = sumAnnee2 + Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["HT"])(item.annee2, tva);
          sumAnnee3 = sumAnnee3 + Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["HT"])(item.annee3, tva);

          for (var _mois4 in _this4.totalServExtFrais.depensesMensuellesLancement) {
            _this4.totalServExtFrais.depensesMensuellesLancement[_mois4] = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(_this4.totalServExtFrais.depensesMensuellesLancement[_mois4] + _this4.calculServiceExterieurDuMoisHT(item, Number(_mois4), tva), _this4.currentCurrencyMask[':precision']);
            _this4.totalServExtFrais.depensesLancement = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(_this4.totalServExtFrais.depensesLancement + _this4.calculServiceExterieurDuMoisHT(item, Number(_mois4), tva), _this4.currentCurrencyMask[':precision']);
          }
        });
      }

      this.totalServExtFrais.annee1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee1, this.currentCurrencyMask[':precision']);
      this.totalServExtFrais.annee2 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee2, this.currentCurrencyMask[':precision']);
      this.totalServExtFrais.annee3 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee3, this.currentCurrencyMask[':precision']);
    },
    calculServiceExterieurDuMoisHT: function calculServiceExterieurDuMoisHT(service, moisDurant, tva) {
      if (moisDurant <= service.quantite) {
        return Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["HT"])(service.prix, tva);
      }

      return 0;
    },
    calculSubTotalTaxeImpMairie: function calculSubTotalTaxeImpMairie(taxesImpotsMairie) {
      var sumAnnee1 = 0;
      var sumAnnee2 = 0;
      var sumAnnee3 = 0;

      if (taxesImpotsMairie) {
        taxesImpotsMairie.forEach(function (item) {
          sumAnnee1 = sumAnnee1 + item.annee1;
          sumAnnee2 = sumAnnee2 + item.annee2;
          sumAnnee3 = sumAnnee3 + item.annee3;
        });
      }

      this.subTotalTaxImpMairie.annee1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee1, this.currentCurrencyMask[':precision']);
      this.subTotalTaxImpMairie.annee2 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee2, this.currentCurrencyMask[':precision']);
      this.subTotalTaxImpMairie.annee3 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee3, this.currentCurrencyMask[':precision']);
    },
    calculSubTotalTaxImpImpot: function calculSubTotalTaxImpImpot(taxesImpotsImpots) {
      var sumAnnee1 = 0;
      var sumAnnee2 = 0;
      var sumAnnee3 = 0;

      if (taxesImpotsImpots) {
        taxesImpotsImpots.forEach(function (item) {
          sumAnnee1 = sumAnnee1 + item.annee1;
          sumAnnee2 = sumAnnee2 + item.annee2;
          sumAnnee3 = sumAnnee3 + item.annee3;
        });
      }

      this.subTotalTaxImpImpot.annee1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee1, this.currentCurrencyMask[':precision']);
      this.subTotalTaxImpImpot.annee2 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee2, this.currentCurrencyMask[':precision']);
      this.subTotalTaxImpImpot.annee3 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee3, this.currentCurrencyMask[':precision']);
    },
    // calculs hors taxe pour la mairie
    calculSubTotalTaxeImpMairieHT: function calculSubTotalTaxeImpMairieHT(taxesImpotsMairie, tva) {
      var sumAnnee1 = 0;
      var sumAnnee2 = 0;
      var sumAnnee3 = 0;

      if (taxesImpotsMairie) {
        taxesImpotsMairie.forEach(function (item) {
          sumAnnee1 = sumAnnee1 + Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["HT"])(item.annee1, tva);
          sumAnnee2 = sumAnnee2 + Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["HT"])(item.annee2, tva);
          sumAnnee3 = sumAnnee3 + Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["HT"])(item.annee3, tva);
        });
      }

      this.subTotalTaxImpMairie.annee1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee1, this.currentCurrencyMask[':precision']);
      this.subTotalTaxImpMairie.annee2 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee2, this.currentCurrencyMask[':precision']);
      this.subTotalTaxImpMairie.annee3 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee3, this.currentCurrencyMask[':precision']);
    },
    calculSubTotalTaxImpImpotHT: function calculSubTotalTaxImpImpotHT(taxesImpotsImpots, tva) {
      var sumAnnee1 = 0;
      var sumAnnee2 = 0;
      var sumAnnee3 = 0;

      if (taxesImpotsImpots) {
        taxesImpotsImpots.forEach(function (item) {
          sumAnnee1 = sumAnnee1 + Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["HT"])(item.annee1, tva);
          sumAnnee2 = sumAnnee2 + Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["HT"])(item.annee2, tva);
          sumAnnee3 = sumAnnee3 + Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["HT"])(item.annee3, tva);
        });
      }

      this.subTotalTaxImpImpot.annee1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee1, this.currentCurrencyMask[':precision']);
      this.subTotalTaxImpImpot.annee2 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee2, this.currentCurrencyMask[':precision']);
      this.subTotalTaxImpImpot.annee3 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee3, this.currentCurrencyMask[':precision']);
    },

    /* calcul le salaire total sur la premiere annee en fonction des mois ouvrables */
    salaireInitial: function salaireInitial(item, section, collection) {
      if (item.group == 'charges-sociales') {
        item.annee1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(item.taux * item.effectif * item.ouvrable * item.salaire / 100, this.currentCurrencyMask[':precision']);
      } else {
        item.annee1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(item.effectif * item.salaire * item.ouvrable, this.currentCurrencyMask[':precision']);
      }

      this.accroissementPrevisionnelAnnee2(item, section, collection);
    },
    calculTotalFraisPersonnelMensuel: function calculTotalFraisPersonnelMensuel(arrayPersonnel, moisDurant) {
      var _this5 = this;

      var sum = 0;
      arrayPersonnel.forEach(function (personnel) {
        sum += _this5.calculSalaireDuMois(personnel, moisDurant);
      });
      return Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sum, this.currentCurrencyMask[':precision']);
    },
    calculChargeSocialeMensuelle: function calculChargeSocialeMensuelle(arrayPersonnel, partCharge, moisDurant) {
      return Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(this.calculTotalFraisPersonnelMensuel(arrayPersonnel, moisDurant) * partCharge / 100, this.currentCurrencyMask[':precision']);
    },
    calculSubTotalFraisSalarie: function calculSubTotalFraisSalarie(personnelSalaries) {
      var sumAnnee1 = 0;
      var sumAnnee2 = 0;
      var sumAnnee3 = 0;

      if (personnelSalaries) {
        personnelSalaries.forEach(function (item) {
          sumAnnee1 = sumAnnee1 + item.annee1;
          sumAnnee2 = sumAnnee2 + item.annee2;
          sumAnnee3 = sumAnnee3 + item.annee3;
        });
      }

      this.subTotalFraisSalarie.annee1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee1, this.currentCurrencyMask[':precision']);
      this.subTotalFraisSalarie.annee2 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee2, this.currentCurrencyMask[':precision']);
      this.subTotalFraisSalarie.annee3 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee3, this.currentCurrencyMask[':precision']);
      this.calculSubTotalFraisCharge();
    },
    calculSubTotalFraisStagiaire: function calculSubTotalFraisStagiaire(personnelStagiaires) {
      var sumAnnee1 = 0;
      var sumAnnee2 = 0;
      var sumAnnee3 = 0;

      if (personnelStagiaires) {
        personnelStagiaires.forEach(function (item) {
          sumAnnee1 = sumAnnee1 + item.annee1;
          sumAnnee2 = sumAnnee2 + item.annee2;
          sumAnnee3 = sumAnnee3 + item.annee3;
        });
      }

      this.subTotalFraisStagiaire.annee1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee1, this.currentCurrencyMask[':precision']);
      this.subTotalFraisStagiaire.annee2 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee2, this.currentCurrencyMask[':precision']);
      this.subTotalFraisStagiaire.annee3 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee3, this.currentCurrencyMask[':precision']);
      this.calculSubTotalFraisCharge();
    },
    calculSubTotalFraisDirigeant: function calculSubTotalFraisDirigeant(personnelDirigeants) {
      var sumAnnee1 = 0;
      var sumAnnee2 = 0;
      var sumAnnee3 = 0;

      if (personnelDirigeants) {
        personnelDirigeants.forEach(function (item) {
          sumAnnee1 = sumAnnee1 + item.annee1;
          sumAnnee2 = sumAnnee2 + item.annee2;
          sumAnnee3 = sumAnnee3 + item.annee3;
        });
      }

      this.subTotalFraisDirigeant.annee1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee1, this.currentCurrencyMask[':precision']);
      this.subTotalFraisDirigeant.annee2 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee2, this.currentCurrencyMask[':precision']);
      this.subTotalFraisDirigeant.annee3 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee3, this.currentCurrencyMask[':precision']);
      this.calculSubTotalFraisCharge();
    },
    calculSalaireDuMois: function calculSalaireDuMois(employer, moisDurant) {
      if (moisDurant <= employer.ouvrable) {
        return Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(employer.salaire * employer.effectif, this.currentCurrencyMask[':precision']);
      }

      return 0;
    },
    // alculs des frais hors taxe
    calculSubTotalFraisSalarieHT: function calculSubTotalFraisSalarieHT(personnelSalaries, tva) {
      var sumAnnee1 = 0;
      var sumAnnee2 = 0;
      var sumAnnee3 = 0;

      if (personnelSalaries) {
        personnelSalaries.forEach(function (item) {
          sumAnnee1 = sumAnnee1 + Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["HT"])(item.annee1, tva);
          sumAnnee2 = sumAnnee2 + Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["HT"])(item.annee2, tva);
          sumAnnee3 = sumAnnee3 + Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["HT"])(item.annee3, tva);
        });
      }

      this.subTotalFraisSalarie.annee1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee1, this.currentCurrencyMask[':precision']);
      this.subTotalFraisSalarie.annee2 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee2, this.currentCurrencyMask[':precision']);
      this.subTotalFraisSalarie.annee3 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee3, this.currentCurrencyMask[':precision']);
      this.calculSubTotalFraisCharge();
    },
    calculSubTotalFraisStagiaireHT: function calculSubTotalFraisStagiaireHT(personnelStagiaires, tva) {
      var sumAnnee1 = 0;
      var sumAnnee2 = 0;
      var sumAnnee3 = 0;

      if (personnelStagiaires) {
        personnelStagiaires.forEach(function (item) {
          sumAnnee1 = sumAnnee1 + Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["HT"])(item.annee1, tva);
          sumAnnee2 = sumAnnee2 + Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["HT"])(item.annee2, tva);
          sumAnnee3 = sumAnnee3 + Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["HT"])(item.annee3, tva);
        });
      }

      this.subTotalFraisStagiaire.annee1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee1, this.currentCurrencyMask[':precision']);
      this.subTotalFraisStagiaire.annee2 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee2, this.currentCurrencyMask[':precision']);
      this.subTotalFraisStagiaire.annee3 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee3, this.currentCurrencyMask[':precision']);
      this.calculSubTotalFraisCharge();
    },
    calculSubTotalFraisDirigeantHT: function calculSubTotalFraisDirigeantHT(personnelDirigeants, tva) {
      var sumAnnee1 = 0;
      var sumAnnee2 = 0;
      var sumAnnee3 = 0;

      if (personnelDirigeants) {
        personnelDirigeants.forEach(function (item) {
          sumAnnee1 = sumAnnee1 + Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["HT"])(item.annee1, tva);
          sumAnnee2 = sumAnnee2 + Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["HT"])(item.annee2, tva);
          sumAnnee3 = sumAnnee3 + Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["HT"])(item.annee3, tva);
        });
      }

      this.subTotalFraisDirigeant.annee1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee1, this.currentCurrencyMask[':precision']);
      this.subTotalFraisDirigeant.annee2 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee2, this.currentCurrencyMask[':precision']);
      this.subTotalFraisDirigeant.annee3 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(sumAnnee3, this.currentCurrencyMask[':precision']);
      this.calculSubTotalFraisCharge();
    },
    calculSalaireDuMoisHT: function calculSalaireDuMoisHT(employer, moisDurant, tva) {
      if (moisDurant <= employer.ouvrable) {
        return Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["HT"])(employer.salaire, tva) * employer.effectif, this.currentCurrencyMask[':precision']);
      }

      return 0;
    },
    // cumul des charges sociales
    calculSubTotalFraisCharge: function calculSubTotalFraisCharge() {
      this.subTotalFraisCharge.annee1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(this.chargeSocialeDirigeant.annee1 + this.chargeSocialeEmploye.annee1, this.currentCurrencyMask[':precision']);
      this.subTotalFraisCharge.annee2 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(this.chargeSocialeDirigeant.annee2 + this.chargeSocialeEmploye.annee2, this.currentCurrencyMask[':precision']);
      this.subTotalFraisCharge.annee3 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(this.chargeSocialeDirigeant.annee3 + this.chargeSocialeEmploye.annee3, this.currentCurrencyMask[':precision']);
      this.calculTotalFraisPersonnel();
    },
    calculTotalFraisPersonnel: function calculTotalFraisPersonnel() {
      this.totalFraisPersonnel.annee1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(this.subTotalFraisSalarie.annee1 + this.subTotalFraisStagiaire.annee1 + this.subTotalFraisCharge.annee1 + this.subTotalFraisDirigeant.annee1, this.currentCurrencyMask[':precision']);
      this.totalFraisPersonnel.annee2 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(this.subTotalFraisSalarie.annee2 + this.subTotalFraisStagiaire.annee2 + this.subTotalFraisCharge.annee2 + this.subTotalFraisDirigeant.annee2, this.currentCurrencyMask[':precision']);
      this.totalFraisPersonnel.accroissement1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(this.totalFraisPersonnel.annee1 == 0 ? 0 : (this.totalFraisPersonnel.annee2 - this.totalFraisPersonnel.annee1) * 100 / this.totalFraisPersonnel.annee1, 2);
      this.totalFraisPersonnel.annee3 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(this.subTotalFraisSalarie.annee3 + this.subTotalFraisStagiaire.annee3 + this.subTotalFraisCharge.annee3 + this.subTotalFraisDirigeant.annee3, this.currentCurrencyMask[':precision']);
      this.totalFraisPersonnel.accroissement2 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(this.totalFraisPersonnel.annee2 == 0 ? 0 : (this.totalFraisPersonnel.annee3 - this.totalFraisPersonnel.annee2) * 100 / this.totalFraisPersonnel.annee2, 2);
    },
    calculEffectifPersonnel: function calculEffectifPersonnel(personnels) {
      var sum = 0;

      if (personnels) {
        personnels.forEach(function (personnel) {
          sum = sum + personnel.effectif;
        });
      }

      return sum;
    },
    calculEffectifTotal: function calculEffectifTotal() {
      this.totalFraisPersonnel.effectif = this.subTotalFraisSalarie.effectif + this.subTotalFraisDirigeant.effectif + this.subTotalFraisStagiaire.effectif;
    },
    emptyTotauxCharges: function emptyTotauxCharges() {
      /* totuax et sous-totaux pour achat de matiere premiere */
      this.totalAchatMpDepVariable = {
        annee1: 0,
        annee2: 0,
        annee3: 0,
        depensesMensuellesLancement: {
          "01": {
            "ttc": 0,
            "ht": 0
          },
          "02": {
            "ttc": 0,
            "ht": 0
          },
          "03": {
            "ttc": 0,
            "ht": 0
          },
          "04": {
            "ttc": 0,
            "ht": 0
          },
          "05": {
            "ttc": 0,
            "ht": 0
          },
          "06": {
            "ttc": 0,
            "ht": 0
          },
          "07": {
            "ttc": 0,
            "ht": 0
          },
          "08": {
            "ttc": 0,
            "ht": 0
          },
          "09": {
            "ttc": 0,
            "ht": 0
          },
          "10": {
            "ttc": 0,
            "ht": 0
          },
          "11": {
            "ttc": 0,
            "ht": 0
          },
          "12": {
            "ttc": 0,
            "ht": 0
          }
        },
        depensesLancement: {
          ht: 0,
          ttc: 0
        }
      }, this.totalAchatMpCharVariable = {
        annee1: 0,
        annee2: 0,
        annee3: 0
      },
      /* Service exterieurs */
      this.totalServExtFrais = {
        annee1: 0,
        annee2: 0,
        annee3: 0,
        depensesMensuellesLancement: {
          "01": 0,
          "02": 0,
          "03": 0,
          "04": 0,
          "05": 0,
          "06": 0,
          "07": 0,
          "08": 0,
          "09": 0,
          "10": 0,
          "11": 0,
          "12": 0
        },
        depensesLancement: 0
      },
      /* Taxes et impots */
      this.subTotalTaxImpImpot = {
        annee1: 0,
        annee2: 0,
        annee3: 0
      }, this.subTotalTaxImpMairie = {
        annee1: 0,
        annee2: 0,
        annee3: 0
      }, this.subTotalFraisSalarie = {
        annee1: 0,
        annee2: 0,
        annee3: 0,
        effectif: 0
      }, this.subTotalFraisDirigeant = {
        annee1: 0,
        annee2: 0,
        annee3: 0,
        effectif: 0
      }, this.subTotalFraisStagiaire = {
        annee1: 0,
        annee2: 0,
        annee3: 0,
        effectif: 0
      }, this.subTotalFraisCharge = {
        annee1: 0,
        annee2: 0,
        annee3: 0
      }, this.totalFraisPersonnel = {
        annee1: 0,
        annee2: 0,
        annee3: 0,
        effectif: 0,
        accroissement1: 0,
        accroissement2: 0
      };
    }
  },
  computed: {
    chargeSocialeDirigeant: {
      get: function get() {
        var result = {
          titre: "Les Dirigeants",
          taux: this.paramChargePatronaleDirigeant,
          effectif: this.subTotalFraisDirigeant.effectif,
          annee1: 0,
          annee2: 0,
          annee3: 0,
          accroissement1: 0,
          accroissement2: 0
        };
        result.annee1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(result.taux * this.subTotalFraisDirigeant.annee1 / 100, this.currentCurrencyMask[':precision']);
        result.annee2 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(result.taux * this.subTotalFraisDirigeant.annee2 / 100, this.currentCurrencyMask[':precision']); // acroissement des charges sociales sur l'annee1

        if (result.annee1 > 0) {
          result.accroissement1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])((result.annee2 - result.annee1) * 100 / result.annee1, 2);
        } else {
          result.accroissement1 = 0;
        }

        result.annee3 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(result.taux * this.subTotalFraisDirigeant.annee3 / 100, this.currentCurrencyMask[':precision']); // acroissement des charges sociales sur l'annee2

        if (result.annee2 > 0) {
          result.accroissement2 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])((result.annee3 - result.annee2) * 100 / result.annee2, 2);
        } else {
          result.accroissement2 = 0;
        }

        return result;
      },
      set: function set(taux) {
        this.paramChargePatronaleDirigeant = Object(util__WEBPACK_IMPORTED_MODULE_2__["isNullOrUndefined"])(taux) ? this.paramChargePatronaleDirigeant : taux;
      }
    },
    chargeSocialeEmploye: {
      get: function get() {
        var result = {
          titre: "Les autres salaires",
          taux: this.paramChargePatronaleEmploye,
          effectif: this.totalFraisPersonnel.effectif - this.subTotalFraisDirigeant.effectif,
          annee1: 0,
          annee2: 0,
          annee3: 0,
          accroissement1: 0,
          accroissement2: 0
        };
        result.annee1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(result.taux * (this.subTotalFraisSalarie.annee1 + this.subTotalFraisStagiaire.annee1) / 100, this.currentCurrencyMask[':precision']);
        result.annee2 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(result.taux * (this.subTotalFraisSalarie.annee2 + this.subTotalFraisStagiaire.annee2) / 100, this.currentCurrencyMask[':precision']); // acroissement des charges sociales sur l'annee1

        if (result.annee1 > 0) {
          result.accroissement1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])((result.annee2 - result.annee1) * 100 / result.annee1, this.currentCurrencyMask[':precision']);
        } else {
          result.accroissement1 = 0;
        }

        result.annee3 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(result.taux * (this.subTotalFraisSalarie.annee3 + this.subTotalFraisStagiaire.annee3) / 100, this.currentCurrencyMask[':precision']); // acroissement des charges sociales sur l'annee2

        if (result.annee2 > 0) {
          result.accroissement2 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])((result.annee3 - result.annee2) * 100 / result.annee2, this.currentCurrencyMask[':precision']);
        } else {
          result.accroissement2 = 0;
        }

        return result;
      },
      set: function set(taux) {
        this.paramChargePatronaleEmploye = Object(util__WEBPACK_IMPORTED_MODULE_2__["isNullOrUndefined"])(taux) ? this.paramChargePatronaleEmploye : taux;
      }
    },
    totalTaxeImpot: function totalTaxeImpot() {
      var result = {
        annee1: 0,
        annee2: 0,
        annee3: 0
      };
      result.annee1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(this.subTotalTaxImpMairie.annee1 + this.subTotalTaxImpImpot.annee1, this.currentCurrencyMask[':precision']);
      result.annee2 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(this.subTotalTaxImpMairie.annee2 + this.subTotalTaxImpImpot.annee2, this.currentCurrencyMask[':precision']);
      result.annee3 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(this.subTotalTaxImpMairie.annee3 + this.subTotalTaxImpImpot.annee3, this.currentCurrencyMask[':precision']);
      return result;
    },
    totalAchat: function totalAchat() {
      var result = {
        annee1: 0,
        annee2: 0,
        annee3: 0
      };
      result.annee1 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(this.totalAchatMpDepVariable.annee1 + this.totalAchatMpCharVariable.annee1, this.currentCurrencyMask[':precision']);
      result.annee2 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(this.totalAchatMpDepVariable.annee2 + this.totalAchatMpCharVariable.annee2, this.currentCurrencyMask[':precision']);
      result.annee3 = Object(_services_helper_js__WEBPACK_IMPORTED_MODULE_0__["arround"])(this.totalAchatMpDepVariable.annee3 + this.totalAchatMpCharVariable.annee3, this.currentCurrencyMask[':precision']);
      return result;
    }
  }
});

/***/ })

}]);
//# sourceMappingURL=financier.bilanPrevisionnel~financier.charges~financier.compteResultats~financier.dashboard~financie~36b6e76d.js.map