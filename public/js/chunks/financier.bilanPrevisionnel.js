(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["financier.bilanPrevisionnel"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/financier/bilanPrevisionnel.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/bp/financier/bilanPrevisionnel.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vue_numeric__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue-numeric */ "./node_modules/vue-numeric/dist/vue-numeric.min.js");
/* harmony import */ var vue_numeric__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(vue_numeric__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _mixins_currenciesMask__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../mixins/currenciesMask */ "./resources/assets/js/views/bp/mixins/currenciesMask.js");
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vue-loading-overlay */ "./node_modules/vue-loading-overlay/dist/vue-loading.min.js");
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(vue_loading_overlay__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var vue_loading_overlay_dist_vue_loading_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vue-loading-overlay/dist/vue-loading.css */ "./node_modules/vue-loading-overlay/dist/vue-loading.css");
/* harmony import */ var vue_loading_overlay_dist_vue_loading_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(vue_loading_overlay_dist_vue_loading_css__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _mixins_financier_produits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../mixins/financier/produits */ "./resources/assets/js/views/bp/mixins/financier/produits.js");
/* harmony import */ var _mixins_financier_charges__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../mixins/financier/charges */ "./resources/assets/js/views/bp/mixins/financier/charges.js");
/* harmony import */ var _mixins_financier_ressources__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../mixins/financier/ressources */ "./resources/assets/js/views/bp/mixins/financier/ressources.js");
/* harmony import */ var _mixins_financier_besoins__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../mixins/financier/besoins */ "./resources/assets/js/views/bp/mixins/financier/besoins.js");
/* harmony import */ var _default_emptyBesoins_json__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./default.emptyBesoins.json */ "./resources/assets/js/views/bp/financier/default.emptyBesoins.json");
var _default_emptyBesoins_json__WEBPACK_IMPORTED_MODULE_10___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./default.emptyBesoins.json */ "./resources/assets/js/views/bp/financier/default.emptyBesoins.json", 1);
/* harmony import */ var _default_emptyRessources_json__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./default.emptyRessources.json */ "./resources/assets/js/views/bp/financier/default.emptyRessources.json");
var _default_emptyRessources_json__WEBPACK_IMPORTED_MODULE_11___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./default.emptyRessources.json */ "./resources/assets/js/views/bp/financier/default.emptyRessources.json", 1);
/* harmony import */ var _default_emptyProduits_json__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./default.emptyProduits.json */ "./resources/assets/js/views/bp/financier/default.emptyProduits.json");
var _default_emptyProduits_json__WEBPACK_IMPORTED_MODULE_12___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./default.emptyProduits.json */ "./resources/assets/js/views/bp/financier/default.emptyProduits.json", 1);
/* harmony import */ var _default_emptyCharges_json__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./default.emptyCharges.json */ "./resources/assets/js/views/bp/financier/default.emptyCharges.json");
var _default_emptyCharges_json__WEBPACK_IMPORTED_MODULE_13___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./default.emptyCharges.json */ "./resources/assets/js/views/bp/financier/default.emptyCharges.json", 1);
/* harmony import */ var _default_chargePatronale_json__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./default.chargePatronale.json */ "./resources/assets/js/views/bp/financier/default.chargePatronale.json");
var _default_chargePatronale_json__WEBPACK_IMPORTED_MODULE_14___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./default.chargePatronale.json */ "./resources/assets/js/views/bp/financier/default.chargePatronale.json", 1);
/* harmony import */ var _services_helper__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../../services/helper */ "./resources/assets/js/services/helper.js");
/* harmony import */ var _default_tvanormale_json__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./default.tvanormale.json */ "./resources/assets/js/views/bp/financier/default.tvanormale.json");
var _default_tvanormale_json__WEBPACK_IMPORTED_MODULE_16___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./default.tvanormale.json */ "./resources/assets/js/views/bp/financier/default.tvanormale.json", 1);
/* harmony import */ var bootstrap_vue__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! bootstrap-vue */ "./node_modules/bootstrap-vue/esm/index.js");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! util */ "./node_modules/node-libs-browser/node_modules/util/util.js");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(util__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var _pdf_PdfBilanPrevisionnel__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./pdf/PdfBilanPrevisionnel */ "./resources/assets/js/views/bp/financier/pdf/PdfBilanPrevisionnel.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



 // Import component for loading

 // Import stylesheet for loading

 // import des fonctions de calculs des besoins et ressources:




 // default datas bp financier






 // tva par defaut 

 // bootstrap-vue for the open/close button effects



 // Init plugin

vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(bootstrap_vue__WEBPACK_IMPORTED_MODULE_17__["default"]);
/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    VueNumeric: vue_numeric__WEBPACK_IMPORTED_MODULE_2___default.a
  },
  mixins: [_mixins_financier_produits__WEBPACK_IMPORTED_MODULE_6__["default"], _mixins_financier_charges__WEBPACK_IMPORTED_MODULE_7__["default"], _mixins_currenciesMask__WEBPACK_IMPORTED_MODULE_3__["default"], _mixins_financier_ressources__WEBPACK_IMPORTED_MODULE_8__["default"], _mixins_financier_besoins__WEBPACK_IMPORTED_MODULE_9__["default"]],
  data: function data() {
    return {
      infoProjet: JSON.parse(localStorage.getItem('userSession')),
      projet: JSON.parse(localStorage.getItem('userSession')).projet,
      besoins: _default_emptyBesoins_json__WEBPACK_IMPORTED_MODULE_10__,
      ressources: _default_emptyRessources_json__WEBPACK_IMPORTED_MODULE_11__,
      produits: _default_emptyProduits_json__WEBPACK_IMPORTED_MODULE_12__,
      charges: _default_emptyCharges_json__WEBPACK_IMPORTED_MODULE_13__,
      paramTva: _default_tvanormale_json__WEBPACK_IMPORTED_MODULE_16__,
      dataSource: {
        "immoIncorporelle": {
          "items": [],
          "result": 0
        },
        "immoCorporelle": {
          "items": [],
          "result": 0
        },
        "amortissementCorporelle": {
          "items": [],
          "result": 0
        },
        "immoFinanciere": {
          "items": [],
          "result": 0
        },
        "stockEnCours": {
          "items": [],
          "result": 0
        },
        "creance": {
          "items": [],
          "result": 0
        },
        "compteCourant": {
          "items": [],
          "result": 0
        },
        "capital": {
          "items": [],
          "result": 0
        },
        "emprunt": {
          "items": [],
          "result": 0
        },
        "detteFournisseur": {
          "items": [],
          "result": 0
        },
        "detteDiverse": {
          "items": [],
          "result": 0
        }
      },
      anneeEnCours: 0
    };
  },
  computed: {
    subTotalActifsCirculants: function subTotalActifsCirculants() {
      return this.dataSource.creance.result + this.dataSource.stockEnCours.result + this.dataSource.compteCourant.result;
    },
    subTotalActifsImmobilises: function subTotalActifsImmobilises() {
      return this.dataSource.immoIncorporelle.result + this.dataSource.immoCorporelle.result + this.dataSource.immoFinanciere.result + this.dataSource.amortissementCorporelle.result;
    },
    totalActif: function totalActif() {
      return this.subTotalActifsCirculants + this.subTotalActifsImmobilises;
    },
    subTotalCapitauxPropres: function subTotalCapitauxPropres() {
      return this.dataSource.capital.result + this.dataSource.emprunt.result;
    },
    subTotalCapitauxPropresAvecResultatNet: function subTotalCapitauxPropresAvecResultatNet() {
      return this.subTotalCapitauxPropres + this.resultatNet;
    },
    subTotalDettes: function subTotalDettes() {
      return this.dataSource.detteFournisseur.result + this.dataSource.detteDiverse.result;
    },
    totalPassif: function totalPassif() {
      return this.subTotalCapitauxPropres + this.subTotalDettes;
    },
    totalPassifAvecResultatNet: function totalPassifAvecResultatNet() {
      return this.subTotalCapitauxPropresAvecResultatNet + this.subTotalDettes;
    },
    resultatNet: function resultatNet() {
      return this.totalActif - this.totalPassif;
    }
  },
  methods: {
    isNullOrUndefined: function isNullOrUndefined(something) {
      return Object(util__WEBPACK_IMPORTED_MODULE_18__["isNullOrUndefined"])(something);
    },
    changeAnneeEnCours: function changeAnneeEnCours(value) {
      var _this = this;

      var loader = vue__WEBPACK_IMPORTED_MODULE_0___default.a.$loading.show();
      var annee = 'annee0';
      this.anneeEnCours = value; // init dataSource ...

      for (var property in this.dataSource) {
        this.dataSource[property] = {
          "items": [],
          "result": 0
        };
      }
      /* calcul de tous les totaux  */


      this.emptyTotauxBesoins();
      this.emptyTotauxRessources();
      this.emptyTotauxProduits();
      this.emptyTotauxCharges();
      /* emprunts et subventions (seulement la premiere année) */

      this.calculSubTotalEmpruntBanque(this.ressources.emprunts['banque']);
      this.calculSubTotalEmpruntMicroFin(this.ressources.emprunts['microfinance']);
      this.calculSubTotalAidBanque(this.ressources['subventions-aides'].banque);
      this.calculSubTotalAidInstitution(this.ressources['subventions-aides'].institution);
      this.calculSubTotalAidAutre(this.ressources['subventions-aides'].autres);
      /** calcul des remboursements des emprunts */

      this.calculTotalRemboursAnnuelEmpruntBanque(this.ressources.emprunts['banque']);
      this.calculTotalRemboursAnnuelEmpruntMicroFin(this.ressources.emprunts['microfinance']);
      this.calculTotalRemboursAnnuelAidBanque(this.ressources['subventions-aides'].banque);
      this.calculTotalRemboursAnnuelAidInstitution(this.ressources['subventions-aides'].institution);
      this.calculTotalRemboursAnnuelAidAutre(this.ressources['subventions-aides'].autres);

      switch (value) {
        case 1:
          // bilan previsionnel de la 1ere année                        

          /** emprunt initial contracté */
          this.dataSource.emprunt.result = this.totalSubventionAide.montant + this.totalEmpruntBancaire.montant;
          this.dataSource.emprunt.items = this.ressources.emprunts['banque'].concat(this.ressources.emprunts['microfinance'], this.ressources['subventions-aides'].banque, this.ressources['subventions-aides'].institution, this.ressources['subventions-aides'].autres).map(function (item) {
            return {
              "titre": Object(util__WEBPACK_IMPORTED_MODULE_18__["isNullOrUndefined"])(item.fournisseur) ? item.creancier : item.fournisseur,
              "montant": item.montantPret
            };
          });
          /** stocks en cours et creances clients  et dettes */

          this.calculSubTotalBfrStockAnnee1(this.besoins.bfr.stock);
          this.calculSubTotalBfrCreanceCliAnnee1(this.besoins.bfr.creanceClient);
          this.calculSubTotalBfrDetFourAnnee1(this.besoins.bfr.dettes);
          break;

        case 2:
          // bilan previsionnel de la 2e année
          annee = 'annee1';
          /** stocks en cours et creances clients  et dettes */

          this.calculSubTotalBfrStockAnnee2(this.besoins.bfr.stock);
          this.calculSubTotalBfrCreanceCliAnnee2(this.besoins.bfr.creanceClient);
          this.calculSubTotalBfrDetFourAnnee2(this.besoins.bfr.dettes);
          break;

        case 3:
          // bilan previsionnel de la 3e année
          annee = 'annee2';
          /** stocks en cours et creances clients  et dettes */

          this.calculSubTotalBfrStockAnnee3(this.besoins.bfr.stock);
          this.calculSubTotalBfrCreanceCliAnnee3(this.besoins.bfr.creanceClient);
          this.calculSubTotalBfrDetFourAnnee3(this.besoins.bfr.dettes);
          break;
      }
      /* capitaux (propre)  / sans provision */


      this.calculSubTotalCapAppNumeraire(this.ressources.capital.numeraire);
      this.calculSubTotalCapAppNature(this.ressources.capital.nature);
      this.dataSource.capital.items = this.ressources.capital.numeraire.concat(this.ressources.capital.nature);
      this.dataSource.capital.result = this.subTotalCapAppNature + this.subTotalCapAppNumeraire;
      /** fill immo incorporelle pour toutes les années */

      this.dataSource.immoIncorporelle.items = this.besoins.immoIncorporelle;
      this.calculTotalImmoIncorporelle(this.dataSource.immoIncorporelle.items);
      this.dataSource.immoIncorporelle.result = this.totalImmoIncorporelle;
      /* immo corporelle materiel informatique dépréciées */

      var materielInfo = [];
      this.besoins.immoCorporelle.materielinfo.forEach(function (item) {
        // retrait de la valeur de l'amortissement
        var amortissementCumule = _this.calculAmortissementUnitaireImmoCorporelle(item)[annee];

        materielInfo.push({
          "prix": Object(util__WEBPACK_IMPORTED_MODULE_18__["isNullOrUndefined"])(amortissementCumule) ? item.prix : item.prix - amortissementCumule,
          "titre": item.titre,
          "quantite": item.quantite
        }); // remplissage du tableau des amortissemens cumulés payés

        if (value > 1 && amortissementCumule > 0) {
          _this.dataSource.amortissementCorporelle.items.push({
            "prix": amortissementCumule,
            "quantite": item.quantite,
            "titre": item.titre
          });
        }
      });
      /* immo corporelle mobilier dépréciées */

      var mobilier = [];
      this.besoins.immoCorporelle.mobilier.forEach(function (item) {
        // retrait de la valeur de l'amortissement
        var amortissementCumule = _this.calculAmortissementUnitaireImmoCorporelle(item)[annee];

        mobilier.push({
          "prix": Object(util__WEBPACK_IMPORTED_MODULE_18__["isNullOrUndefined"])(amortissementCumule) ? item.prix : item.prix - amortissementCumule,
          "titre": item.titre,
          "quantite": item.quantite
        }); // remplissage du tableau des amortissemens cumulés payés

        if (value > 1 && amortissementCumule > 0) {
          _this.dataSource.amortissementCorporelle.items.push({
            "prix": amortissementCumule,
            "quantite": item.quantite,
            "titre": item.titre
          });
        }
      });
      /* immo corporelle machines / outils dépréciées */

      var materielMachineOutil = [];
      this.besoins.immoCorporelle['materiel-machine-outil'].forEach(function (item) {
        // retrait de la valeur de l'amortissement
        var amortissementCumule = _this.calculAmortissementUnitaireImmoCorporelle(item)[annee];

        materielMachineOutil.push({
          "prix": Object(util__WEBPACK_IMPORTED_MODULE_18__["isNullOrUndefined"])(amortissementCumule) ? item.prix : item.prix - amortissementCumule,
          "titre": item.titre,
          "quantite": item.quantite
        }); // remplissage du tableau des amortissemens cumulés payés

        if (value > 1 && amortissementCumule > 0) {
          _this.dataSource.amortissementCorporelle.items.push({
            "prix": amortissementCumule,
            "quantite": item.quantite,
            "titre": item.titre
          });
        }
      });
      /* immo corporelle batiment / local dépréciées */

      var batimentLocalEspacevente = [];
      this.besoins.immoCorporelle['batiment-local-espacevente'].forEach(function (item) {
        // retrait de la valeur de l'amortissement
        var amortissementCumule = _this.calculAmortissementUnitaireImmoCorporelle(item)[annee];

        batimentLocalEspacevente.push({
          "prix": Object(util__WEBPACK_IMPORTED_MODULE_18__["isNullOrUndefined"])(amortissementCumule) ? item.prix : item.prix - amortissementCumule,
          "titre": item.titre,
          "quantite": item.quantite
        }); // remplissage du tableau des amortissemens cumulés payés

        if (value > 1 && amortissementCumule > 0) {
          _this.dataSource.amortissementCorporelle.items.push({
            "prix": amortissementCumule,
            "quantite": item.quantite,
            "titre": item.titre
          });
        }
      });
      /* immo corporelle matériel roulant dépréciées */

      var voitureAutres = [];
      this.besoins.immoCorporelle['voiture-autres'].forEach(function (item) {
        // retrait de la valeur de l'amortissement
        var amortissementCumule = _this.calculAmortissementUnitaireImmoCorporelle(item)[annee];

        voitureAutres.push({
          "prix": Object(util__WEBPACK_IMPORTED_MODULE_18__["isNullOrUndefined"])(amortissementCumule) ? item.prix : item.prix - amortissementCumule,
          "titre": item.titre,
          "quantite": item.quantite
        }); // remplissage du tableau des amortissemens cumulés payés

        if (value > 1 && amortissementCumule > 0) {
          _this.dataSource.amortissementCorporelle.items.push({
            "prix": amortissementCumule,
            "quantite": item.quantite,
            "titre": item.titre
          });
        }
      });
      /* calcul des totaux des immo corporelles de l'année en cours */

      this.calculSubTotalImmoCorpMatInfo(materielInfo);
      this.calculSubTotalImmoCorpMobilier(mobilier);
      this.calculSubTotalImmoCorpMatMachOutil(materielMachineOutil);
      this.calculSubTotalImmoCorpBatLocEspaceVente(batimentLocalEspacevente);
      this.calculSubTotalImmoCorpVoitAutre(voitureAutres);
      this.dataSource.immoCorporelle.result = this.totalImmoCorporelle.prix;
      this.dataSource.immoCorporelle.items = materielInfo.concat(mobilier, materielMachineOutil, batimentLocalEspacevente, voitureAutres);
      /* calcul des totaux des paiements des ammortissement corporelles */

      this.calculTotalAmortisAnnuelImmoCorpMatInfo(this.besoins.immoCorporelle.materielinfo);
      this.calculTotalAmortisAnnuelImmoCorpMobilier(this.besoins.immoCorporelle.mobilier);
      this.calculTotalAmortisAnnuelImmoCorpMatMachOutil(this.besoins.immoCorporelle['materiel-machine-outil']);
      this.calculTotalAmortisAnnuelImmoCorpBatLocEspaceVente(this.besoins.immoCorporelle['batiment-local-espacevente']);
      this.calculTotalAmortisAnnuelImmoCorpVoitAutre(this.besoins.immoCorporelle['voiture-autres']);

      for (var i = value - 1; i >= 1; i--) {
        this.dataSource.amortissementCorporelle.result += Object(util__WEBPACK_IMPORTED_MODULE_18__["isNullOrUndefined"])(this.totalAmortisCorporelleAnnuel['annee' + i]) ? 0 : this.totalAmortisCorporelleAnnuel['annee' + i];
      }
      /* comptes courant */


      this.dataSource.compteCourant.items = this.ressources.comptes;
      this.calculTotalCompteAssocie(this.ressources.comptes);
      this.dataSource.compteCourant.result = this.totalCompteAssocie; // this.dataSource.stockEnCours = this.besoins.bfr.stocks.map()

      /* immo financiere */

      this.calculSubTotalImmoFinCaution(this.besoins.immoFinanciere.cautions);
      this.calculSubTotalImmoFinDepGarantie(this.besoins.immoFinanciere.depots);
      this.calculSubTotalImmoFinAutre(this.besoins.immoFinanciere.autres);
      this.dataSource.immoFinanciere.items = this.besoins.immoFinanciere.cautions.concat(this.besoins.immoFinanciere.depots, this.besoins.immoFinanciere.autres);
      this.dataSource.immoFinanciere.result = this.totalImmoFinanciere;
      /** total stocks de l'annee en cours */

      this.dataSource.stockEnCours.result = this.subTotalBfrStock['annee' + value];
      this.besoins.bfr.stock.forEach(function (stock) {
        _this.dataSource.stockEnCours.items.push({
          "titre": stock.titre,
          "montant": stock['annee' + value]
        });
      });
      /** total créances clients de l'annee en cours */

      this.dataSource.creance.result = this.subTotalBfrCreanceCli['annee' + value];
      this.besoins.bfr.creanceClient.forEach(function (creance) {
        _this.dataSource.creance.items.push({
          "titre": creance.titre,
          "montant": creance['annee' + value]
        });
      });
      /** total dettes fournisseurs (commerciale) année en cours */

      this.dataSource.detteFournisseur.result = this.subTotalBfrDetFour['annee' + value];
      this.besoins.bfr.dettes.forEach(function (dette) {
        _this.dataSource.detteFournisseur.items.push({
          "titre": dette.titre,
          "montant": dette['annee' + value]
        });
      });
      /** emprunt initial (annee1) ou déprécié après un an et plus */
      // calcul des remboursements annuels cumulés

      var remboursementAnnuelCumuleSubvention = 0;
      var remboursementAnnuelCumuleEmprunt = 0;

      for (var _i = value - 1; _i >= 1; _i--) {
        remboursementAnnuelCumuleEmprunt += Object(util__WEBPACK_IMPORTED_MODULE_18__["isNullOrUndefined"])(this.totalRemboursementAnnuelEmprunt['annee' + _i]) ? 0 : this.totalRemboursementAnnuelEmprunt['annee' + _i].montant;
        remboursementAnnuelCumuleSubvention += Object(util__WEBPACK_IMPORTED_MODULE_18__["isNullOrUndefined"])(this.totalRemboursementAnnuelSubvention['annee' + _i]) ? 0 : this.totalRemboursementAnnuelSubvention['annee' + _i].montant;
      }

      this.dataSource.emprunt.result = Math.max(this.totalSubventionAide.montant - remboursementAnnuelCumuleSubvention + this.totalEmpruntBancaire.montant - remboursementAnnuelCumuleEmprunt, 0); // concatenation des emprunts conctractés (banque|microfinance|subventions)

      this.ressources.emprunts['banque'].concat(this.ressources.emprunts['microfinance'], this.ressources['subventions-aides'].banque, this.ressources['subventions-aides'].institution, this.ressources['subventions-aides'].autres).forEach(function (item) {
        var cumulRemboursement = 0;

        for (var _i2 = value - 1; _i2 >= 1; _i2--) {
          cumulRemboursement += Object(util__WEBPACK_IMPORTED_MODULE_18__["isNullOrUndefined"])(_this.calculRemboursement(item)['annee' + _i2]) ? 0 : _this.calculRemboursement(item)['annee' + _i2].montant;
        }

        _this.dataSource.emprunt.items.push({
          "titre": Object(util__WEBPACK_IMPORTED_MODULE_18__["isNullOrUndefined"])(item.fournisseur) ? item.creancier : item.fournisseur,
          "montant": Math.max(item.montantPret - cumulRemboursement, 0)
        }); // remplissage du tbleau de dettes déja | en cours de remboursement


        _this.dataSource.detteDiverse.items.push({
          "titre": Object(util__WEBPACK_IMPORTED_MODULE_18__["isNullOrUndefined"])(item.fournisseur) ? item.creancier : item.fournisseur,
          "montant": _this.calculRemboursement(item)['annee' + value].montant
        });

        _this.dataSource.detteDiverse.result = _this.totalRemboursementAnnuelEmprunt['annee' + value].montant + _this.totalRemboursementAnnuelSubvention['annee' + value].montant;
      });
      loader.hide();
    },
    headerTotaux: function headerTotaux() {
      return {
        circulants: this.subTotalActifsCirculants,
        immobilises: this.subTotalActifsImmobilises,
        capitauxPropres: this.subTotalCapitauxPropresAvecResultatNet,
        dettes: this.subTotalDettes,
        totalActif: this.totalActif,
        totalPassif: this.totalPassifAvecResultatNet
      };
    },
    printPdf: function printPdf() {
      var loader = vue__WEBPACK_IMPORTED_MODULE_0___default.a.$loading.show();
      var pdf = new _pdf_PdfBilanPrevisionnel__WEBPACK_IMPORTED_MODULE_19__["default"]({
        data: {
          dataSource: this.dataSource,
          headerTotaux: this.headerTotaux(),
          resultatNet: this.resultatNet
        },
        title: this.infoProjet.projettitre
      });
      pdf.openPdf();
      loader.hide();
    }
  },
  mounted: function mounted() {
    var _this2 = this;

    // le loader en mode plugin
    var loader = vue__WEBPACK_IMPORTED_MODULE_0___default.a.$loading.show();
    firebase__WEBPACK_IMPORTED_MODULE_1___default.a.firestore().collection('financier').doc(this.projet).get().then(function (docSnap) {
      if (docSnap.exists) {
        // tva parametree du projet
        if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('parametres'))) {
          if (Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('parametres')['tva'])) {
            Toast.fire({
              type: 'warning',
              title: 'Votre Projet n\'a pas de Tva configurée, nous utiliserons un parametre par défaut',
              customClass: "bg-saphire"
            });
          } else {
            if (docSnap.get('parametres')['tva'].length > 1) {
              _this2.paramTva = Object(util__WEBPACK_IMPORTED_MODULE_18__["isNullOrUndefined"])(docSnap.get('parametres')['tva'].find(function (item) {
                return item.alias === 'normale';
              })) ? docSnap.get('parametres')['tva'][0] : docSnap.get('parametres')['tva'].find(function (item) {
                return item.alias === 'normale';
              });
            } else if (docSnap.get('parametres')['tva'].length = 1) {
              _this2.paramTva = docSnap.get('parametres')['tva'][0];
            }
          }

          ; // recuperer les charges patronales definies du projet

          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('parametres'))) {
            if (!Object(util__WEBPACK_IMPORTED_MODULE_18__["isNullOrUndefined"])(docSnap.get("parametres")['chargePatronale'])) {
              // affectation directe
              _this2.chargeSocialeEmploye = docSnap.get("parametres")['chargePatronale'].employe;
              _this2.chargeSocialeDirigeant = docSnap.get("parametres")['chargePatronale'].dirigeant;
            }
          }

          ;
        } else {
          if (docSnap.get('parametres')['tva'].length > 1) {
            _this2.paramTva = Object(util__WEBPACK_IMPORTED_MODULE_18__["isNullOrUndefined"])(docSnap.get('parametres')['tva'].find(function (item) {
              return item.alias === 'normale';
            })) ? docSnap.get('parametres')['tva'][0] : docSnap.get('parametres')['tva'].find(function (item) {
              return item.alias === 'normale';
            });
          } else if (docSnap.get('parametres')['tva'].length = 1) {
            _this2.paramTva = docSnap.get('parametres')['tva'][0];
          }
        } // definition HT pour les besoins


        if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('besoins'))) {
          // immobilisations incorporelles
          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('besoins')['immoIncorporelle'])) {
            _this2.besoins['immoIncorporelle'] = docSnap.get('besoins')['immoIncorporelle'].map(function (item) {
              return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, 'prix', _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
            });
          } // immo corporelle


          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('besoins')['immoCorporelle'])) {
            // materiel informatique
            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('besoins')['immoCorporelle']['materielinfo'])) {
              _this2.besoins['immoCorporelle']['materielinfo'] = docSnap.get('besoins')['immoCorporelle']['materielinfo'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, 'prix', _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });
            } // mobilier


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('besoins')['immoCorporelle']['mobilier'])) {
              _this2.besoins['immoCorporelle']['mobilier'] = docSnap.get('besoins')['immoCorporelle']['mobilier'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, 'prix', _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });
            } // materiel-machine-outils


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('besoins')['immoCorporelle']['materiel-machine-outil'])) {
              _this2.besoins['immoCorporelle']['materiel-machine-outil'] = docSnap.get('besoins')['immoCorporelle']['materiel-machine-outil'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, 'prix', _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });
            } // batiment-local-espacevente


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('besoins')['immoCorporelle']['batiment-local-espacevente'])) {
              _this2.besoins['immoCorporelle']['batiment-local-espacevente'] = docSnap.get('besoins')['immoCorporelle']['batiment-local-espacevente'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, 'prix', _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });
            } // voiture-autres


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('besoins')['immoCorporelle']['voiture-autres'])) {
              _this2.besoins['immoCorporelle']['voiture-autres'] = docSnap.get('besoins')['immoCorporelle']['voiture-autres'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, 'prix', _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });
            }
          } // bfr


          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('besoins')['bfr'])) {
            // bfr ouverture
            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('besoins')['bfr']['bfrOuverture'])) {
              _this2.besoins['bfr']['bfrOuverture'] = docSnap.get('besoins')['bfr']['bfrOuverture'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, 'coutMensuel', _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });
            } // stocks


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('besoins')['bfr']['stock'])) {
              _this2.besoins['bfr']['stock'] = docSnap.get('besoins')['bfr']['stock'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, ['annee1', 'annee2', 'annee3'], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });
            } // creance client


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('besoins')['bfr']['creanceClient'])) {
              _this2.besoins['bfr']['creanceClient'] = docSnap.get('besoins')['bfr']['creanceClient'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, ['annee1', 'annee2', 'annee3'], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });
            } // dettes


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('besoins')['bfr']['dettes'])) {
              _this2.besoins['bfr']['dettes'] = docSnap.get('besoins')['bfr']['dettes'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, ['annee1', 'annee2', 'annee3'], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });
            }
          } // immo financiere


          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('besoins')['immoFinanciere'])) {
            // cautions
            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('besoins')['immoFinanciere']['cautions'])) {
              _this2.besoins['immoFinanciere']['cautions'] = docSnap.get('besoins')['immoFinanciere']['cautions'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, 'montant', _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });
            } // depots de garantie


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('besoins')['immoFinanciere']['depots'])) {
              _this2.besoins['immoFinanciere']['depots'] = docSnap.get('besoins')['immoFinanciere']['depots'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, 'montant', _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });
            } // autres


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('besoins')['immoFinanciere']['autres'])) {
              _this2.besoins['immoFinanciere']['autres'] = docSnap.get('besoins')['immoFinanciere']['autres'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, 'montant', _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });
            }
          }
        } // definition HT pour les ressources


        if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('ressources'))) {
          // capital
          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('ressources')['capital'])) {
            // numeraire
            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('ressources')['capital']['numeraire'])) {
              _this2.ressources['capital']['numeraire'] = docSnap.get('ressources')['capital']['numeraire'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, "montant", _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });
            } // nature


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('ressources')['capital']['nature'])) {
              _this2.ressources['capital']['nature'] = docSnap.get('ressources')['capital']['nature'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, "montant", _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });
            }
          } // comptes courants associes


          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('ressources')['comptes'])) {
            _this2.ressources['comptes'] = docSnap.get('ressources')['comptes'].map(function (item) {
              return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, "montant", _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
            });
          } // emprunts


          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('ressources')['emprunts'])) {
            // banque
            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('ressources')['emprunts']['banque'])) {
              _this2.ressources['emprunts']['banque'] = docSnap.get('ressources')['emprunts']['banque'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, ["montantPret", "montantRemboursement"], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });
            } // microfinance


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('ressources')['emprunts']['microfinance'])) {
              _this2.ressources['emprunts']['microfinance'] = docSnap.get('ressources')['emprunts']['microfinance'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, ["montantPret", "montantRemboursement"], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });
            }
          } // subventions-aides


          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('ressources')['subventions-aides'])) {
            // banque
            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('ressources')['subventions-aides']['banque'])) {
              _this2.ressources['subventions-aides']['banque'] = docSnap.get('ressources')['subventions-aides']['banque'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, ["montantPret", "montantRemboursement"], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });
            } // institution


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('ressources')['subventions-aides']['institution'])) {
              _this2.ressources['subventions-aides']['institution'] = docSnap.get('ressources')['subventions-aides']['institution'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, ["montantPret", "montantRemboursement"], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });
            } // autres


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('ressources')['subventions-aides']['autres'])) {
              _this2.ressources['subventions-aides']['autres'] = docSnap.get('ressources')['subventions-aides']['autres'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, ["montantPret", "montantRemboursement"], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });
            }
          }
        } // definition HT pour les produits


        if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('produits'))) {
          // calcul du chiffre d'affaires articles
          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('produits')['articles'])) {
            _this2.produits.articles = docSnap.get('produits')['articles'];
          } // volume de vente


          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('produits')['volumeVente'])) {
            _this2.produits['volumeVente'] = docSnap.get('produits')['volumeVente'];
          } // resultats exceptionnels


          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('produits')['resultatsExceptionnels'])) {
            // entrees
            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('produits')['resultatsExceptionnels']['entrees'])) {
              _this2.produits['resultatsExceptionnels']['entrees'] = docSnap.get('produits')['resultatsExceptionnels']['entrees'];
            } // sorties


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('produits')['resultatsExceptionnels']['sorties'])) {
              _this2.produits['resultatsExceptionnels']['sorties'] = docSnap.get('produits')['resultatsExceptionnels']['sorties'];
            }
          }
        }
        /**
        ** Calcul du remboursement emprunt
        **/
        // definition HT pour les charges


        if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('charges'))) {
          // services exterieurs
          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('charges')['services-exterieurs'])) {
            _this2.charges['services-exterieurs'] = docSnap.get('charges')['services-exterieurs'].map(function (item) {
              return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, ['annee1', 'annee2', 'annee3', 'prix'], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
            });
          } // frais de personnel


          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('charges')['frais-personnel'])) {
            // stagiaires
            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('charges')['frais-personnel']['stagiaires'])) {
              _this2.charges['frais-personnel']['stagiaires'] = docSnap.get('charges')['frais-personnel']['stagiaires'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, ['annee1', 'annee2', 'annee3', 'salaire'], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });
            } // salaries


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('charges')['frais-personnel']['salaries'])) {
              _this2.charges['frais-personnel']['salaries'] = docSnap.get('charges')['frais-personnel']['salaries'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, ['annee1', 'annee2', 'annee3', 'salaire'], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });
            } // dirigeants


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('charges')['frais-personnel']['dirigeants'])) {
              _this2.charges['frais-personnel']['dirigeants'] = docSnap.get('charges')['frais-personnel']['dirigeants'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, ['annee1', 'annee2', 'annee3', 'salaire'], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });
            }
          } // taxes et impots


          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('charges')['taxes-impots'])) {
            // impots
            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('charges')['taxes-impots']['impots'])) {
              _this2.charges['taxes-impots']['impots'] = docSnap.get('charges')['taxes-impots']['impots'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, ['annee1', 'annee2', 'annee3', 'prix'], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });
            } // mairie


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('charges')['taxes-impots']['mairie'])) {
              _this2.charges['taxes-impots']['mairie'] = docSnap.get('charges')['taxes-impots']['mairie'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, ['annee1', 'annee2', 'annee3', 'prix'], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });
            }
          } // depenses produits


          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('charges')['matieres-premieres']['depenses-produits'])) {
            _this2.charges["matieres-premieres"]["depenses-produits"] = docSnap.get('charges')['matieres-premieres']['depenses-produits'].map(function (item) {
              return Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["convertFieldToHT"])(item, ['annee1', 'annee2', 'annee3', 'prix'], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
            });
          } // charges variables


          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_15__["isEmpty"])(docSnap.get('charges')['matieres-premieres']['charges-variables-produits'])) {
            _this2.charges['matieres-premieres']['charges-variables-produits'] = docSnap.get('charges')['matieres-premieres']['charges-variables-produits'];
          }
        }
      }

      loader.hide();
    })["catch"](function (error) {
      loader.hide();

      switch (error.code) {
        case 'unavailable':
          Swal.fire('erreur ', '<span class="text-danger text-bold">Pas de connexion internet</span>', 'error');
          break;

        /*
        default:
            Swal.fire(
                'Pas',
                error.message,
                'error'
            );
            break;
            */
      }

      ;
    }).then(function (value) {
      // calcul du bilan initial
      _this2.changeAnneeEnCours(1);

      loader.hide();
    });
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/financier/bilanPrevisionnel.vue?vue&type=style&index=0&id=2ba009ac&scoped=true&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/bp/financier/bilanPrevisionnel.vue?vue&type=style&index=0&id=2ba009ac&scoped=true&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports
exports.i(__webpack_require__(/*! -!../../../../../../node_modules/css-loader??ref--7-1!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../resources/assets/sass/financier.scss */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./resources/assets/sass/financier.scss"), "");
exports.i(__webpack_require__(/*! -!../../../../../../node_modules/css-loader??ref--7-1!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../resources/assets/sass/tiny-inputs.scss */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./resources/assets/sass/tiny-inputs.scss"), "");

// module
exports.push([module.i, "\nh4.font-weight-bold.d-flex[data-v-2ba009ac] {\n            cursor: pointer;\n}\n.el-header[data-v-2ba009ac], .el-footer[data-v-2ba009ac] {\n        background-color: #B3C0D1;\n        color: #333;\n        text-transform: uppercase;\n        font-weight: bold;\n        height: auto !important ;\n}\n.d-flex.line-item[data-v-2ba009ac] {\n        line-height: 0.64em;\n        font-weight: bolder;\n}\n.el-main[data-v-2ba009ac] {\n        background-color: #E9EEF3;\n        color: #333;\n}\n/*-- ============================================================== \n Small Desktop & above all (1024px) \n ============================================================== */\n@media(min-width:1120px) {\n.el-container.lg-horizontal > .el-container.flex-pole[data-v-2ba009ac] {\n            flex-basis: 50%;\n}\n.el-container.lg-horizontal[data-v-2ba009ac] {\n            flex-direction: row !important;\n}\n.el-container.lg-vertical[data-v-2ba009ac] {\n            flex-direction: column !important;\n}\n}\n\n\n/*-- ============================================================== \n Phone and below ipad(767px) \n ============================================================== */\n@media(max-width:1118px) {\n.el-container.md-horizontal[data-v-2ba009ac] {\n            flex-direction: row !important;\n}\n.el-container.md-vertical[data-v-2ba009ac] {\n            flex-direction: column !important;\n}\nspan.ml-auto[data-v-2ba009ac]  {\n            position: relative;\n            display: block;\n            float: right;\n}\n}\nbody > .el-container[data-v-2ba009ac]  {\n        margin-bottom: 40px;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./resources/assets/sass/financier.scss":
/*!*******************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./resources/assets/sass/financier.scss ***!
  \*******************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.bg-platinum {\r\n        background-color: #ede6e3;\n}\n.bg-shade-solde {\r\n        border-color: #36a2eb !important;\r\n        outline-color: #36a2eb !important;\r\n        background-color: #36a2eb !important;\n}\n.bg-besoin {\r\n        background-color: #f02d34ed !important;\r\n        color: white !important;\n}\n.bg-shade-besoin {\r\n        background-color: #e2acae4d !important;\r\n        color: #2f3d4a !important;\n}\n.bg-ressource {\r\n        background-color: #4352f9 !important;\r\n        color: white !important;\n}\n.bg-shade-ressource {\r\n        background-color: #a5bcd440 !important;\r\n        color: #2f3d4a !important;\n}\n.bg-vente {\r\n        background-color: #63ab58ed !important;\r\n        color: white !important;\n}\n.bg-shade-vente {\r\n        background-color: #bae2b340 !important;\r\n        color: #2f3d4a !important;\n}\n.bg-charge {\r\n        background-color: #f02d34ed !important;\r\n        color: white !important;\n}\n.bg-shade-charge {\r\n        background-color: #e2acae4d !important;\r\n        color: #2f3d4a !important;\n}\n.bg-amortissement {\r\n        background-color: #4c8caa !important;\n}\n.bg-shade-amortissement {\r\n        background-color: #287da54d !important;\n}\n.bg-bfr {\r\n        /*background-color: #e02c6ffa !important; */\r\n        background-color: #d33972fa !important;\n}\n.bg-seuil {\r\n        background-color: #875faf !important;\n}\n.bg-shade-seuil {\r\n        background-color: #6633997d !important;\n}\n.bg-shade-bfr {\r\n        background-color: #e02c6f33 !important;\n}\n.bg-jungle {\r\n        background-color: #095256;\n}\n.bg-saphire {\r\n        background-color: #1a5e63;\n}\n.bg-violet {\r\n        background-color: #46237a;\n}\n.bg-positif {\r\n        background-color: #27ae60 !important;\n}\n.bg-negatif {\r\n        background-color: #eb4d4b !important;\n}\n.collapsed > .when-opened,\r\n    :not(.collapsed) > .when-closed {\r\n        display: none;\n}\n.card-body > ul.customtab > li > a.nav-link.show.active,.card-body > ul.customtab > li > a.nav-link.active {\r\n        font-weight: 600;\r\n        text-transform: uppercase;\r\n        color:#67757c;\r\n        opacity:1.1;\n}\n.nav-link.show.active i,.nav-link.active i{\r\n       color: #61b174;\n}\nspan.stepTitle.active{\r\n       color: #67757c !important;\r\n       font-weight: 500 !important;\n}\n.nav-link {\r\n        font-weight: normal;\r\n        text-transform: normal;\r\n        color:#67757c;\n}\n.card-body > ul.customtab > li > a {\r\n     opacity: 0.6;\n}\n.card-body table {\r\n       font-size: 14px;\n}\n.table-head-first-red-column{\r\n       text-align: left !important;\n}\n.color-bordered-table.info-bordered-table{\r\n    border-color: #398bf7 !important;\n}\n.color-bordered-table.info-bordered-table thead th {\r\n        background-color: #398bf7;\r\n        color: #ffffff;\r\n        text-align: center;\n}\n.table-head-first-green-column{\r\n    background-color: white !important;\r\n    color: green !important;\r\n    text-align: justify !important;\n}\n.table-head-first-#2f3d4a-column{\r\n    background-color: white !important;\r\n    color: #2f3d4a !important;\r\n    text-align: justify !important;\n}\n.table-group-header {\r\n    background-color: #dedede; color: #455a64; font-weight:800;\n}\n.bg-light-grey{\r\n        background-color: #dedede !important; color: #2f3d4a !important;\n}\n.table-total {\r\n    background-color:#84d697b8; color:#2f3d4a; font-weight:800;\n}\n.table-group-header .item-total, .table-total .item-total {\r\n        font-weight: 900;\n}\n.no-border-tr td{\r\n    border-color: transparent;\n}\n.vue-form-wizard .wizard-header {\r\n        display: none;\r\n        visibility: hidden;\n}\n.nav-pills > li.nav-item {\r\n        display: block;\r\n        width: 50%;\r\n        text-align: center;\n}\n.nav-pills > li.nav-item a{\r\n        margin: 1px auto;\r\n        padding:1px 0px;\r\n        border-radius: 2px;\r\n        font-size: 0.9rem;\n}\n.nav-pills .nav-link.active, .nav-pills .show > .nav-link {\r\n        background-color: #61b174;\n}\n.card-body {\r\n        padding: 0px 5px;\n}\n.nav.nav-tabs.customtab li a {\r\n        text-align: center;\r\n        padding: 5px 10px;\n}\n.nav.nav-tabs.customtab li a.active {\r\n        background-color: #0080003b;\r\n        border-bottom-color: #f32b2bcc;\n}\n.nav.nav-tabs.customtab li {\r\n        width: 25%;\r\n        display: inline-block;\r\n        font-size: 1.4rem;\r\n        letter-spacing: 1.5px;\n}\n.dark-input {\r\n        color:#455a64;\n}\nspan.item-total {\r\n        vertical-align: sub;\r\n        font-weight: 500;\n}\n.bg-grey {\r\n        background-color: #99abb4 !important;\r\n        /* background-color: #cdced1 !important; */\n}\n.vue-loading {\r\n        position: absolute;\r\n        top: 0px;\r\n        left: 0px;\r\n        z-index: 1000;\r\n        margin: 0px;\r\n        padding: 0px;\r\n        width: 100%;\r\n        height: 100%;\r\n        border: none;\r\n        background-color: rgba(230, 233, 236, 0.8);\r\n        cursor: wait;\r\n        opacity: 0;\r\n        transition: opacity .4s;\n}\n.csb-text-chart{\r\n        color: black;\r\n        font-weight: bold;\r\n        font-size: 15px;\r\n        font-family: Georgia, Verdana, Arial, sans-serif;\n}\n.cbs-text-donnut{\r\n        font-size: 15px!important;\n}\n.pointer {\r\n        cursor: pointer !important;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./resources/assets/sass/tiny-inputs.scss":
/*!*********************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./resources/assets/sass/tiny-inputs.scss ***!
  \*********************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input.form-control, select.form-control {\r\n    min-height: 30px !important;\r\n    height: calc(1.25rem + 2px) !important;\r\n    padding: .155rem .40rem !important;\r\n    font-size: 0.86rem;\n}\n.input-group-append{\r\n    min-height: 30px !important;\r\n    height: calc(1.25rem + 2px) !important;\n}\ntextarea.form-control,textarea{\r\n    font-size: 0.86rem;\n}\n.input-group-text {\r\n    font-size: 0.80rem;\r\n    padding: .18rem .25rem;\r\n    font-weight: 600;\n}\n.table td, .table th {\r\n    padding: .21rem !important;\r\n    min-height: calc(1.24rem + 2px) !important;\n}\r\n/***********\r\n    la taille minimale ds input de monnaie formatee\r\n    ********/\n.empty-numeric-placeholder {\r\n        color: transparent;\r\n        visibility: hidden;\r\n        line-height: 0.1px !important;\n}\r\n    \r\n    \r\n/*-- ============================================================== \r\n Ipad & above all(768px) \r\n ============================================================== */\n@media(min-width:768px) {\r\n    /** input number special width **/\n.two-digit-long {\r\n      width: calc(3.5em + 1px) !important;\r\n      line-height: 1.3px;\n}\n}\r\n  \r\n\r\n/*-- ============================================================== \r\n Phone and below ipad(767px) \r\n ============================================================== */\n@media(max-width:767px) {\r\n  \r\n    /** input number special width **/\n.two-digit-long {\r\n      width: calc(2.5em + 1px) !important;\r\n      line-height: 1.3px;\n}\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/financier/bilanPrevisionnel.vue?vue&type=style&index=0&id=2ba009ac&scoped=true&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/bp/financier/bilanPrevisionnel.vue?vue&type=style&index=0&id=2ba009ac&scoped=true&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader??ref--7-1!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./bilanPrevisionnel.vue?vue&type=style&index=0&id=2ba009ac&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/financier/bilanPrevisionnel.vue?vue&type=style&index=0&id=2ba009ac&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/financier/bilanPrevisionnel.vue?vue&type=template&id=2ba009ac&scoped=true&":
/*!***********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/bp/financier/bilanPrevisionnel.vue?vue&type=template&id=2ba009ac&scoped=true& ***!
  \***********************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "page-wrapper" }, [
    _c("div", { staticClass: "container-fluid" }, [
      _c("div", { staticClass: "card" }, [
        _c("div", { staticClass: "card-body p-1" }, [
          _c(
            "h2",
            [
              _c(
                "center",
                { staticClass: "bg-dark text-white text-uppercase" },
                [_vm._v("Bilan Previsionnel HT")]
              )
            ],
            1
          ),
          _vm._v(" "),
          _c("div", { staticClass: "d-flex d-flex-row mt-3" }, [
            _vm._m(0),
            _vm._v(" "),
            _c("div", { staticClass: "col-sm text-left text-lg-right" }, [
              _c(
                "h4",
                {
                  staticClass: "font-weight-bold pointer",
                  on: {
                    click: function($event) {
                      return _vm.changeAnneeEnCours(1)
                    }
                  }
                },
                [_vm._v("Année 1")]
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-sm text-left text-lg-right" }, [
              _c(
                "h4",
                {
                  staticClass: "font-weight-bold pointer",
                  on: {
                    click: function($event) {
                      return _vm.changeAnneeEnCours(2)
                    }
                  }
                },
                [_vm._v("Année 2")]
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-sm text-left text-lg-right" }, [
              _c(
                "h4",
                {
                  staticClass: "font-weight-bold pointer",
                  on: {
                    click: function($event) {
                      return _vm.changeAnneeEnCours(3)
                    }
                  }
                },
                [_vm._v("Année 3")]
              )
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("section", { staticClass: "el-container md-vertical lg-horizontal" }, [
        _c(
          "section",
          { staticClass: "el-container mx-1 flex-pole is-vertical" },
          [
            _c("el-header", { staticClass: "mb-3 p-2 text-center" }, [
              _vm._v(" Actif ")
            ]),
            _vm._v(" "),
            _c(
              "section",
              { staticClass: "el-container mb-1 is-vertical" },
              [
                _c(
                  "el-header",
                  { staticClass: "d-flex p-2" },
                  [
                    _vm._v("Actifs immobilisés\n                        "),
                    _c("vue-numeric", {
                      attrs: {
                        currency: _vm.currentCurrencyMask["currency"],
                        "currency-symbol-position":
                          _vm.currentCurrencyMask["currency-symbol-position"],
                        "output-type": "String",
                        "empty-value": "0",
                        precision: _vm.currentCurrencyMask["precision"],
                        "decimal-separator":
                          _vm.currentCurrencyMask["decimal-separator"],
                        "thousand-separator":
                          _vm.currentCurrencyMask["thousand-separator"],
                        "read-only-class": "font-weight-bold ml-auto",
                        min: _vm.currentCurrencyMask.min,
                        "read-only": "",
                        value: _vm.subTotalActifsImmobilises
                      }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "el-main",
                  { staticClass: "p-2" },
                  [
                    _c(
                      "h4",
                      {
                        directives: [
                          {
                            name: "b-toggle",
                            rawName: "v-b-toggle.collapseImmoIncorp",
                            modifiers: { collapseImmoIncorp: true }
                          }
                        ],
                        staticClass: "font-weight-bold d-flex pointer"
                      },
                      [
                        _c("i", {
                          staticClass: "fa fa-plus-square when-closed"
                        }),
                        _vm._v(" "),
                        _c("i", {
                          staticClass: "fa fa-minus-square when-opened"
                        }),
                        _vm._v(" "),
                        _c("span", { staticClass: "mx-1" }, [
                          _vm._v("Immobilisations incorporelles")
                        ]),
                        _vm._v(" "),
                        _c("vue-numeric", {
                          attrs: {
                            currency: _vm.currentCurrencyMask["currency"],
                            "currency-symbol-position":
                              _vm.currentCurrencyMask[
                                "currency-symbol-position"
                              ],
                            "output-type": "String",
                            "empty-value": "0",
                            precision: _vm.currentCurrencyMask["precision"],
                            "decimal-separator":
                              _vm.currentCurrencyMask["decimal-separator"],
                            "thousand-separator":
                              _vm.currentCurrencyMask["thousand-separator"],
                            "read-only-class": "font-weight-bold ml-auto",
                            min: _vm.currentCurrencyMask.min,
                            "read-only": ""
                          },
                          model: {
                            value: _vm.dataSource.immoIncorporelle.result,
                            callback: function($$v) {
                              _vm.$set(
                                _vm.dataSource.immoIncorporelle,
                                "result",
                                _vm._n($$v)
                              )
                            },
                            expression: "dataSource.immoIncorporelle.result"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "b-collapse",
                      {
                        staticClass: "pb-3",
                        attrs: { visible: "", id: "collapseImmoIncorp" }
                      },
                      _vm._l(_vm.dataSource.immoIncorporelle.items, function(
                        incorp,
                        index
                      ) {
                        return _c(
                          "div",
                          {
                            key: "indexIncorp-" + index,
                            staticClass: "d-flex p-2 line-item"
                          },
                          [
                            _c("span", [_vm._v(_vm._s(incorp.titre))]),
                            _vm._v(" "),
                            _c("vue-numeric", {
                              attrs: {
                                currency: _vm.currentCurrencyMask["currency"],
                                "currency-symbol-position":
                                  _vm.currentCurrencyMask[
                                    "currency-symbol-position"
                                  ],
                                "output-type": "String",
                                "empty-value": "0",
                                precision: _vm.currentCurrencyMask["precision"],
                                "decimal-separator":
                                  _vm.currentCurrencyMask["decimal-separator"],
                                "thousand-separator":
                                  _vm.currentCurrencyMask["thousand-separator"],
                                "read-only-class": " ml-auto",
                                min: _vm.currentCurrencyMask.min,
                                "read-only": true,
                                value: incorp.prix * incorp.quantite
                              }
                            })
                          ],
                          1
                        )
                      }),
                      0
                    ),
                    _vm._v(" "),
                    _c(
                      "h4",
                      {
                        directives: [
                          {
                            name: "b-toggle",
                            rawName: "v-b-toggle.collapseImmoCorp",
                            modifiers: { collapseImmoCorp: true }
                          }
                        ],
                        staticClass: "font-weight-bold d-flex pointer"
                      },
                      [
                        _c("i", {
                          staticClass: "fa fa-plus-square when-closed"
                        }),
                        _vm._v(" "),
                        _c("i", {
                          staticClass: "fa fa-minus-square when-opened"
                        }),
                        _vm._v(" "),
                        _c("span", { staticClass: "mx-1" }, [
                          _vm._v("Immobilisations corporelles")
                        ]),
                        _vm._v(" "),
                        _c("vue-numeric", {
                          attrs: {
                            currency: _vm.currentCurrencyMask["currency"],
                            "currency-symbol-position":
                              _vm.currentCurrencyMask[
                                "currency-symbol-position"
                              ],
                            "output-type": "String",
                            "empty-value": "0",
                            precision: _vm.currentCurrencyMask["precision"],
                            "decimal-separator":
                              _vm.currentCurrencyMask["decimal-separator"],
                            "thousand-separator":
                              _vm.currentCurrencyMask["thousand-separator"],
                            "read-only-class": "font-weight-bold ml-auto",
                            min: _vm.currentCurrencyMask.min,
                            "read-only": ""
                          },
                          model: {
                            value: _vm.dataSource.immoCorporelle.result,
                            callback: function($$v) {
                              _vm.$set(
                                _vm.dataSource.immoCorporelle,
                                "result",
                                _vm._n($$v)
                              )
                            },
                            expression: "dataSource.immoCorporelle.result"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "b-collapse",
                      {
                        staticClass: "pb-3",
                        attrs: { visible: "", id: "collapseImmoCorp" }
                      },
                      _vm._l(_vm.dataSource.immoCorporelle.items, function(
                        corp,
                        index
                      ) {
                        return _c(
                          "div",
                          {
                            key: "indexImmoCorp-" + index,
                            staticClass: "d-flex p-2 line-item"
                          },
                          [
                            _c("span", [_vm._v(_vm._s(corp.titre))]),
                            _vm._v(" "),
                            _c("vue-numeric", {
                              attrs: {
                                currency: _vm.currentCurrencyMask["currency"],
                                "currency-symbol-position":
                                  _vm.currentCurrencyMask[
                                    "currency-symbol-position"
                                  ],
                                "output-type": "String",
                                "empty-value": "0",
                                precision: _vm.currentCurrencyMask["precision"],
                                "decimal-separator":
                                  _vm.currentCurrencyMask["decimal-separator"],
                                "thousand-separator":
                                  _vm.currentCurrencyMask["thousand-separator"],
                                "read-only-class": " ml-auto",
                                min: _vm.currentCurrencyMask.min,
                                "read-only": true,
                                value: corp.prix * corp.quantite
                              }
                            })
                          ],
                          1
                        )
                      }),
                      0
                    ),
                    _vm._v(" "),
                    _c(
                      "h4",
                      {
                        directives: [
                          {
                            name: "b-toggle",
                            rawName: "v-b-toggle.collapseImmoFin",
                            modifiers: { collapseImmoFin: true }
                          }
                        ],
                        staticClass: "font-weight-bold d-flex pointer"
                      },
                      [
                        _c("i", {
                          staticClass: "fa fa-plus-square when-closed"
                        }),
                        _vm._v(" "),
                        _c("i", {
                          staticClass: "fa fa-minus-square when-opened"
                        }),
                        _vm._v(" "),
                        _c("span", { staticClass: "mx-1" }, [
                          _vm._v("Immobilisations financières")
                        ]),
                        _vm._v(" "),
                        _c("vue-numeric", {
                          attrs: {
                            currency: _vm.currentCurrencyMask["currency"],
                            "currency-symbol-position":
                              _vm.currentCurrencyMask[
                                "currency-symbol-position"
                              ],
                            "output-type": "String",
                            "empty-value": "0",
                            precision: _vm.currentCurrencyMask["precision"],
                            "decimal-separator":
                              _vm.currentCurrencyMask["decimal-separator"],
                            "thousand-separator":
                              _vm.currentCurrencyMask["thousand-separator"],
                            "read-only-class": " ml-auto",
                            min: _vm.currentCurrencyMask.min,
                            "read-only": ""
                          },
                          model: {
                            value: _vm.dataSource.immoFinanciere.result,
                            callback: function($$v) {
                              _vm.$set(
                                _vm.dataSource.immoFinanciere,
                                "result",
                                _vm._n($$v)
                              )
                            },
                            expression: "dataSource.immoFinanciere.result"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "b-collapse",
                      {
                        staticClass: "pb-3",
                        attrs: { visible: "", id: "collapseImmoFin" }
                      },
                      _vm._l(_vm.dataSource.immoFinanciere.items, function(
                        fin,
                        index
                      ) {
                        return _c(
                          "div",
                          {
                            key: "indexImmoFin-" + index,
                            staticClass: "d-flex p-2 line-item"
                          },
                          [
                            _c("span", [_vm._v(_vm._s(fin.titre))]),
                            _vm._v(" "),
                            _c("vue-numeric", {
                              attrs: {
                                currency: _vm.currentCurrencyMask["currency"],
                                "currency-symbol-position":
                                  _vm.currentCurrencyMask[
                                    "currency-symbol-position"
                                  ],
                                "output-type": "String",
                                "empty-value": "0",
                                precision: _vm.currentCurrencyMask["precision"],
                                "decimal-separator":
                                  _vm.currentCurrencyMask["decimal-separator"],
                                "thousand-separator":
                                  _vm.currentCurrencyMask["thousand-separator"],
                                "read-only-class": " ml-auto",
                                min: _vm.currentCurrencyMask.min,
                                "read-only": true,
                                value: fin.montant * fin.quantite
                              }
                            })
                          ],
                          1
                        )
                      }),
                      0
                    ),
                    _vm._v(" "),
                    _vm.anneeEnCours > 1
                      ? _c(
                          "h4",
                          {
                            directives: [
                              {
                                name: "b-toggle",
                                rawName: "v-b-toggle.collapseAvanceImmoCorp",
                                modifiers: { collapseAvanceImmoCorp: true }
                              }
                            ],
                            staticClass: "font-weight-bold d-flex pointer"
                          },
                          [
                            _c("i", {
                              staticClass: "fa fa-plus-square when-closed"
                            }),
                            _vm._v(" "),
                            _c("i", {
                              staticClass: "fa fa-minus-square when-opened"
                            }),
                            _vm._v(" "),
                            _c("span", { staticClass: "mx-1" }, [
                              _vm._v("Amortissements")
                            ]),
                            _vm._v(" "),
                            _c("vue-numeric", {
                              attrs: {
                                currency: _vm.currentCurrencyMask["currency"],
                                "currency-symbol-position":
                                  _vm.currentCurrencyMask[
                                    "currency-symbol-position"
                                  ],
                                "output-type": "String",
                                "empty-value": "0",
                                precision: _vm.currentCurrencyMask["precision"],
                                "decimal-separator":
                                  _vm.currentCurrencyMask["decimal-separator"],
                                "thousand-separator":
                                  _vm.currentCurrencyMask["thousand-separator"],
                                "read-only-class": "font-weight-bold ml-auto",
                                min: _vm.currentCurrencyMask.min,
                                "read-only": ""
                              },
                              model: {
                                value:
                                  _vm.dataSource.amortissementCorporelle.result,
                                callback: function($$v) {
                                  _vm.$set(
                                    _vm.dataSource.amortissementCorporelle,
                                    "result",
                                    _vm._n($$v)
                                  )
                                },
                                expression:
                                  "dataSource.amortissementCorporelle.result"
                              }
                            })
                          ],
                          1
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.anneeEnCours > 1
                      ? _c(
                          "b-collapse",
                          {
                            staticClass: "pb-3",
                            attrs: { visible: "", id: "collapseAvanceImmoCorp" }
                          },
                          _vm._l(
                            _vm.dataSource.amortissementCorporelle.items,
                            function(corp, index) {
                              return _c(
                                "div",
                                {
                                  key: "indexImmoCorp-" + index,
                                  staticClass: "d-flex p-2 line-item"
                                },
                                [
                                  _c("span", [_vm._v(_vm._s(corp.titre))]),
                                  _vm._v(" "),
                                  _c("vue-numeric", {
                                    attrs: {
                                      currency:
                                        _vm.currentCurrencyMask["currency"],
                                      "currency-symbol-position":
                                        _vm.currentCurrencyMask[
                                          "currency-symbol-position"
                                        ],
                                      "output-type": "String",
                                      "empty-value": "0",
                                      precision:
                                        _vm.currentCurrencyMask["precision"],
                                      "decimal-separator":
                                        _vm.currentCurrencyMask[
                                          "decimal-separator"
                                        ],
                                      "thousand-separator":
                                        _vm.currentCurrencyMask[
                                          "thousand-separator"
                                        ],
                                      "read-only-class": " ml-auto",
                                      min: _vm.currentCurrencyMask.min,
                                      "read-only": true,
                                      value: corp.prix * corp.quantite
                                    }
                                  })
                                ],
                                1
                              )
                            }
                          ),
                          0
                        )
                      : _vm._e()
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "section",
              { staticClass: "el-container mb-3 is-vertical" },
              [
                _c(
                  "el-header",
                  { staticClass: "d-flex p-2" },
                  [
                    _vm._v("Actifs circulants\n                        "),
                    _c("vue-numeric", {
                      attrs: {
                        currency: _vm.currentCurrencyMask["currency"],
                        "currency-symbol-position":
                          _vm.currentCurrencyMask["currency-symbol-position"],
                        "output-type": "String",
                        "empty-value": "0",
                        precision: _vm.currentCurrencyMask["precision"],
                        "decimal-separator":
                          _vm.currentCurrencyMask["decimal-separator"],
                        "thousand-separator":
                          _vm.currentCurrencyMask["thousand-separator"],
                        "read-only-class": "font-weight-bold ml-auto",
                        min: _vm.currentCurrencyMask.min,
                        "read-only": "",
                        value: _vm.subTotalActifsCirculants
                      }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "el-main",
                  { staticClass: "p-2" },
                  [
                    _c(
                      "h4",
                      {
                        directives: [
                          {
                            name: "b-toggle",
                            rawName: "v-b-toggle.collapseStock",
                            modifiers: { collapseStock: true }
                          }
                        ],
                        staticClass: "font-weight-bold d-flex pointer"
                      },
                      [
                        _c("i", {
                          staticClass: "fa fa-plus-square when-closed"
                        }),
                        _vm._v(" "),
                        _c("i", {
                          staticClass: "fa fa-minus-square when-opened"
                        }),
                        _vm._v(" "),
                        _c("span", { staticClass: "mx-1" }, [
                          _vm._v("Stocks en cours")
                        ]),
                        _vm._v(" "),
                        _c("vue-numeric", {
                          attrs: {
                            currency: _vm.currentCurrencyMask["currency"],
                            "currency-symbol-position":
                              _vm.currentCurrencyMask[
                                "currency-symbol-position"
                              ],
                            "output-type": "String",
                            "empty-value": "0",
                            precision: _vm.currentCurrencyMask["precision"],
                            "decimal-separator":
                              _vm.currentCurrencyMask["decimal-separator"],
                            "thousand-separator":
                              _vm.currentCurrencyMask["thousand-separator"],
                            "read-only-class": "font-weight-bold ml-auto",
                            min: _vm.currentCurrencyMask.min,
                            "read-only": ""
                          },
                          model: {
                            value: _vm.dataSource.stockEnCours.result,
                            callback: function($$v) {
                              _vm.$set(
                                _vm.dataSource.stockEnCours,
                                "result",
                                _vm._n($$v)
                              )
                            },
                            expression: "dataSource.stockEnCours.result"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "b-collapse",
                      {
                        staticClass: "pb-3",
                        attrs: { visible: "", id: "collapseStock" }
                      },
                      _vm._l(_vm.dataSource.stockEnCours.items, function(
                        stock,
                        index
                      ) {
                        return _c(
                          "div",
                          {
                            key: "indexStock-" + index,
                            staticClass: "d-flex p-2 line-item"
                          },
                          [
                            _c("span", [_vm._v(_vm._s(stock.titre))]),
                            _vm._v(" "),
                            _c("vue-numeric", {
                              attrs: {
                                currency: _vm.currentCurrencyMask["currency"],
                                "currency-symbol-position":
                                  _vm.currentCurrencyMask[
                                    "currency-symbol-position"
                                  ],
                                "output-type": "String",
                                "empty-value": "0",
                                precision: _vm.currentCurrencyMask["precision"],
                                "decimal-separator":
                                  _vm.currentCurrencyMask["decimal-separator"],
                                "thousand-separator":
                                  _vm.currentCurrencyMask["thousand-separator"],
                                "read-only-class": " ml-auto",
                                min: _vm.currentCurrencyMask.min,
                                "read-only": true,
                                value: stock.montant
                              }
                            })
                          ],
                          1
                        )
                      }),
                      0
                    ),
                    _vm._v(" "),
                    _c(
                      "h4",
                      {
                        directives: [
                          {
                            name: "b-toggle",
                            rawName: "v-b-toggle.collapseCreance",
                            modifiers: { collapseCreance: true }
                          }
                        ],
                        staticClass: "font-weight-bold d-flex pointer"
                      },
                      [
                        _c("i", {
                          staticClass: "fa fa-plus-square when-closed"
                        }),
                        _vm._v(" "),
                        _c("i", {
                          staticClass: "fa fa-minus-square when-opened"
                        }),
                        _vm._v(" "),
                        _c("span", { staticClass: "mx-1" }, [
                          _vm._v("Créance")
                        ]),
                        _vm._v(" "),
                        _c("vue-numeric", {
                          attrs: {
                            currency: _vm.currentCurrencyMask["currency"],
                            "currency-symbol-position":
                              _vm.currentCurrencyMask[
                                "currency-symbol-position"
                              ],
                            "output-type": "String",
                            "empty-value": "0",
                            precision: _vm.currentCurrencyMask["precision"],
                            "decimal-separator":
                              _vm.currentCurrencyMask["decimal-separator"],
                            "thousand-separator":
                              _vm.currentCurrencyMask["thousand-separator"],
                            "read-only-class": "font-weight-bold ml-auto",
                            min: _vm.currentCurrencyMask.min,
                            "read-only": ""
                          },
                          model: {
                            value: _vm.dataSource.creance.result,
                            callback: function($$v) {
                              _vm.$set(
                                _vm.dataSource.creance,
                                "result",
                                _vm._n($$v)
                              )
                            },
                            expression: "dataSource.creance.result"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "b-collapse",
                      {
                        staticClass: "pb-3",
                        attrs: { visible: "", id: "collapseCreance" }
                      },
                      _vm._l(_vm.dataSource.creance.items, function(
                        creance,
                        index
                      ) {
                        return _c(
                          "div",
                          {
                            key: "indexCréance-" + index,
                            staticClass: "d-flex p-2 line-item"
                          },
                          [
                            _c("span", [_vm._v(_vm._s(creance.titre))]),
                            _vm._v(" "),
                            _c("vue-numeric", {
                              attrs: {
                                currency: _vm.currentCurrencyMask["currency"],
                                "currency-symbol-position":
                                  _vm.currentCurrencyMask[
                                    "currency-symbol-position"
                                  ],
                                "output-type": "String",
                                "empty-value": "0",
                                precision: _vm.currentCurrencyMask["precision"],
                                "decimal-separator":
                                  _vm.currentCurrencyMask["decimal-separator"],
                                "thousand-separator":
                                  _vm.currentCurrencyMask["thousand-separator"],
                                "read-only-class": " ml-auto",
                                min: _vm.currentCurrencyMask.min,
                                "read-only": true
                              },
                              model: {
                                value: creance.montant,
                                callback: function($$v) {
                                  _vm.$set(creance, "montant", $$v)
                                },
                                expression: "creance.montant"
                              }
                            })
                          ],
                          1
                        )
                      }),
                      0
                    ),
                    _vm._v(" "),
                    _c(
                      "h4",
                      {
                        directives: [
                          {
                            name: "b-toggle",
                            rawName: "v-b-toggle.collapseCompte",
                            modifiers: { collapseCompte: true }
                          }
                        ],
                        staticClass: "font-weight-bold d-flex pointer"
                      },
                      [
                        _c("i", {
                          staticClass: "fa fa-plus-square when-closed"
                        }),
                        _vm._v(" "),
                        _c("i", {
                          staticClass: "fa fa-minus-square when-opened"
                        }),
                        _vm._v(" "),
                        _c("span", { staticClass: "mx-1" }, [
                          _vm._v("Compte courant")
                        ]),
                        _vm._v(" "),
                        _c("vue-numeric", {
                          attrs: {
                            currency: _vm.currentCurrencyMask["currency"],
                            "currency-symbol-position":
                              _vm.currentCurrencyMask[
                                "currency-symbol-position"
                              ],
                            "output-type": "String",
                            "empty-value": "0",
                            precision: _vm.currentCurrencyMask["precision"],
                            "decimal-separator":
                              _vm.currentCurrencyMask["decimal-separator"],
                            "thousand-separator":
                              _vm.currentCurrencyMask["thousand-separator"],
                            "read-only-class": "font-weight-bold ml-auto",
                            min: _vm.currentCurrencyMask.min,
                            "read-only": ""
                          },
                          model: {
                            value: _vm.dataSource.compteCourant.result,
                            callback: function($$v) {
                              _vm.$set(
                                _vm.dataSource.compteCourant,
                                "result",
                                _vm._n($$v)
                              )
                            },
                            expression: "dataSource.compteCourant.result"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "b-collapse",
                      {
                        staticClass: "pb-3",
                        attrs: { visible: "", id: "collapseCompte" }
                      },
                      _vm._l(_vm.dataSource.compteCourant.items, function(
                        compte,
                        index
                      ) {
                        return _c(
                          "div",
                          {
                            key: "indexCompteCourant-" + index,
                            staticClass: "d-flex p-2 line-item"
                          },
                          [
                            _c("span", [_vm._v(_vm._s(compte.identite))]),
                            _vm._v(" "),
                            _c("vue-numeric", {
                              attrs: {
                                currency: _vm.currentCurrencyMask["currency"],
                                "currency-symbol-position":
                                  _vm.currentCurrencyMask[
                                    "currency-symbol-position"
                                  ],
                                "output-type": "String",
                                "empty-value": "0",
                                precision: _vm.currentCurrencyMask["precision"],
                                "decimal-separator":
                                  _vm.currentCurrencyMask["decimal-separator"],
                                "thousand-separator":
                                  _vm.currentCurrencyMask["thousand-separator"],
                                "read-only-class": " ml-auto",
                                min: _vm.currentCurrencyMask.min,
                                "read-only": true,
                                value: compte.montant * compte.quantite
                              }
                            })
                          ],
                          1
                        )
                      }),
                      0
                    )
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "el-footer",
              { staticClass: "d-flex p-2 mb-3" },
              [
                _vm._v(" Total Actif\n                    "),
                _c("vue-numeric", {
                  attrs: {
                    currency: _vm.currentCurrencyMask["currency"],
                    "currency-symbol-position":
                      _vm.currentCurrencyMask["currency-symbol-position"],
                    "output-type": "String",
                    "empty-value": "0",
                    precision: _vm.currentCurrencyMask["precision"],
                    "decimal-separator":
                      _vm.currentCurrencyMask["decimal-separator"],
                    "thousand-separator":
                      _vm.currentCurrencyMask["thousand-separator"],
                    "read-only-class": " ml-auto",
                    min: _vm.currentCurrencyMask.min,
                    "read-only": "",
                    value: _vm.totalActif
                  }
                })
              ],
              1
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "section",
          { staticClass: "el-container mx-1 flex-pole is-vertical" },
          [
            _c("el-header", { staticClass: "mb-3 p-2 text-center" }, [
              _vm._v("Passif")
            ]),
            _vm._v(" "),
            _c(
              "section",
              { staticClass: "el-container mb-1 is-vertical" },
              [
                _c(
                  "el-header",
                  { staticClass: "d-flex p-2" },
                  [
                    _vm._v("Capitaux propres\n                        "),
                    _c("vue-numeric", {
                      attrs: {
                        currency: _vm.currentCurrencyMask["currency"],
                        "currency-symbol-position":
                          _vm.currentCurrencyMask["currency-symbol-position"],
                        "output-type": "String",
                        "empty-value": "0",
                        precision: _vm.currentCurrencyMask["precision"],
                        "decimal-separator":
                          _vm.currentCurrencyMask["decimal-separator"],
                        "thousand-separator":
                          _vm.currentCurrencyMask["thousand-separator"],
                        "read-only-class": "font-weight-bold ml-auto",
                        min: _vm.currentCurrencyMask.min,
                        "read-only": "",
                        value: _vm.subTotalCapitauxPropresAvecResultatNet
                      }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "el-main",
                  { staticClass: "p-2" },
                  [
                    _c(
                      "h4",
                      {
                        directives: [
                          {
                            name: "b-toggle",
                            rawName: "v-b-toggle.collapseCapital",
                            modifiers: { collapseCapital: true }
                          }
                        ],
                        staticClass: "font-weight-bold d-flex"
                      },
                      [
                        _c("i", {
                          staticClass: "fa fa-plus-square when-closed"
                        }),
                        _vm._v(" "),
                        _c("i", {
                          staticClass: "fa fa-minus-square when-opened"
                        }),
                        _vm._v(" "),
                        _c("span", { staticClass: "mx-1" }, [
                          _vm._v("Capitaux numéraire et nature")
                        ]),
                        _vm._v(" "),
                        _c("vue-numeric", {
                          attrs: {
                            currency: _vm.currentCurrencyMask["currency"],
                            "currency-symbol-position":
                              _vm.currentCurrencyMask[
                                "currency-symbol-position"
                              ],
                            "output-type": "String",
                            "empty-value": "0",
                            precision: _vm.currentCurrencyMask["precision"],
                            "decimal-separator":
                              _vm.currentCurrencyMask["decimal-separator"],
                            "thousand-separator":
                              _vm.currentCurrencyMask["thousand-separator"],
                            "read-only-class": "font-weight-bold ml-auto",
                            min: _vm.currentCurrencyMask.min,
                            "read-only": ""
                          },
                          model: {
                            value: _vm.dataSource.capital.result,
                            callback: function($$v) {
                              _vm.$set(
                                _vm.dataSource.capital,
                                "result",
                                _vm._n($$v)
                              )
                            },
                            expression: "dataSource.capital.result"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "b-collapse",
                      {
                        staticClass: "pb-3",
                        attrs: { visible: "", id: "collapseCapital" }
                      },
                      _vm._l(_vm.dataSource.capital.items, function(
                        cap,
                        index
                      ) {
                        return _c(
                          "div",
                          {
                            key: "indexCapital-" + index,
                            staticClass: "d-flex p-2 line-item"
                          },
                          [
                            _c("span", [_vm._v(_vm._s(cap.identite))]),
                            _vm._v(" "),
                            _c("vue-numeric", {
                              attrs: {
                                currency: _vm.currentCurrencyMask["currency"],
                                "currency-symbol-position":
                                  _vm.currentCurrencyMask[
                                    "currency-symbol-position"
                                  ],
                                "output-type": "String",
                                "empty-value": "0",
                                precision: _vm.currentCurrencyMask["precision"],
                                "decimal-separator":
                                  _vm.currentCurrencyMask["decimal-separator"],
                                "thousand-separator":
                                  _vm.currentCurrencyMask["thousand-separator"],
                                "read-only-class": " ml-auto",
                                min: _vm.currentCurrencyMask.min,
                                "read-only": true,
                                value: cap.montant * cap.quantite
                              }
                            })
                          ],
                          1
                        )
                      }),
                      0
                    ),
                    _vm._v(" "),
                    _c(
                      "h4",
                      {
                        directives: [
                          {
                            name: "b-toggle",
                            rawName: "v-b-toggle.collapseEmprunt",
                            modifiers: { collapseEmprunt: true }
                          }
                        ],
                        staticClass: "font-weight-bold d-flex"
                      },
                      [
                        _c("i", {
                          staticClass: "fa fa-plus-square when-closed"
                        }),
                        _vm._v(" "),
                        _c("i", {
                          staticClass: "fa fa-minus-square when-opened"
                        }),
                        _vm._v(" "),
                        _c("span", { staticClass: "mx-1" }, [
                          _vm._v("Emprunts")
                        ]),
                        _vm._v(" "),
                        _c("vue-numeric", {
                          attrs: {
                            currency: _vm.currentCurrencyMask["currency"],
                            "currency-symbol-position":
                              _vm.currentCurrencyMask[
                                "currency-symbol-position"
                              ],
                            "output-type": "String",
                            "empty-value": "0",
                            precision: _vm.currentCurrencyMask["precision"],
                            "decimal-separator":
                              _vm.currentCurrencyMask["decimal-separator"],
                            "thousand-separator":
                              _vm.currentCurrencyMask["thousand-separator"],
                            "read-only-class": "font-weight-bold ml-auto",
                            min: _vm.currentCurrencyMask.min,
                            "read-only": ""
                          },
                          model: {
                            value: _vm.dataSource.emprunt.result,
                            callback: function($$v) {
                              _vm.$set(
                                _vm.dataSource.emprunt,
                                "result",
                                _vm._n($$v)
                              )
                            },
                            expression: "dataSource.emprunt.result"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "b-collapse",
                      {
                        staticClass: "pb-3",
                        attrs: { visible: true, id: "collapseEmprunt" }
                      },
                      _vm._l(_vm.dataSource.emprunt.items, function(
                        emp,
                        index
                      ) {
                        return _c(
                          "div",
                          {
                            key: "indexEmprunt-" + index,
                            staticClass: "d-flex p-2 line-item"
                          },
                          [
                            _c("span", [_vm._v(_vm._s(emp.titre))]),
                            _vm._v(" "),
                            _c("vue-numeric", {
                              attrs: {
                                currency: _vm.currentCurrencyMask["currency"],
                                "currency-symbol-position":
                                  _vm.currentCurrencyMask[
                                    "currency-symbol-position"
                                  ],
                                "output-type": "String",
                                "empty-value": "0",
                                precision: _vm.currentCurrencyMask["precision"],
                                "decimal-separator":
                                  _vm.currentCurrencyMask["decimal-separator"],
                                "thousand-separator":
                                  _vm.currentCurrencyMask["thousand-separator"],
                                "read-only-class": " ml-auto",
                                min: _vm.currentCurrencyMask.min,
                                "read-only": true,
                                value: emp.montant
                              }
                            })
                          ],
                          1
                        )
                      }),
                      0
                    ),
                    _vm._v(" "),
                    _c(
                      "h4",
                      { staticClass: "font-weight-bold d-flex" },
                      [
                        _c("i", { staticClass: "fa fa-square" }),
                        _vm._v(" "),
                        _c("span", { staticClass: "mx-1" }, [
                          _vm._v("Résultat net")
                        ]),
                        _vm._v(" "),
                        _c("vue-numeric", {
                          attrs: {
                            currency: _vm.currentCurrencyMask["currency"],
                            "currency-symbol-position":
                              _vm.currentCurrencyMask[
                                "currency-symbol-position"
                              ],
                            "output-type": "String",
                            "empty-value": "0",
                            precision: _vm.currentCurrencyMask["precision"],
                            "decimal-separator":
                              _vm.currentCurrencyMask["decimal-separator"],
                            "thousand-separator":
                              _vm.currentCurrencyMask["thousand-separator"],
                            "read-only-class": "font-weight-bold ml-auto",
                            min: _vm.currentCurrencyMask.min,
                            "read-only": "",
                            value: _vm.resultatNet
                          }
                        })
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "section",
              { staticClass: "el-container mb-3 is-vertical" },
              [
                _c(
                  "el-header",
                  { staticClass: "d-flex p-2" },
                  [
                    _vm._v("Dettes\n                        "),
                    _c("vue-numeric", {
                      attrs: {
                        currency: _vm.currentCurrencyMask["currency"],
                        "currency-symbol-position":
                          _vm.currentCurrencyMask["currency-symbol-position"],
                        "output-type": "String",
                        "empty-value": "0",
                        precision: _vm.currentCurrencyMask["precision"],
                        "decimal-separator":
                          _vm.currentCurrencyMask["decimal-separator"],
                        "thousand-separator":
                          _vm.currentCurrencyMask["thousand-separator"],
                        "read-only-class": "font-weight-bold ml-auto",
                        min: _vm.currentCurrencyMask.min,
                        "read-only": "",
                        value: _vm.subTotalDettes
                      }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "el-main",
                  { staticClass: "p-2" },
                  [
                    _c(
                      "h4",
                      {
                        directives: [
                          {
                            name: "b-toggle",
                            rawName: "v-b-toggle.collapseDetteFournisseur",
                            modifiers: { collapseDetteFournisseur: true }
                          }
                        ],
                        staticClass: "font-weight-bold d-flex pointer"
                      },
                      [
                        _c("i", {
                          staticClass: "fa fa-plus-square when-closed"
                        }),
                        _vm._v(" "),
                        _c("i", {
                          staticClass: "fa fa-minus-square when-opened"
                        }),
                        _vm._v(" "),
                        _c("span", { staticClass: "mx-1" }, [
                          _vm._v("Dettes commerciales")
                        ]),
                        _vm._v(" "),
                        _c("vue-numeric", {
                          attrs: {
                            currency: _vm.currentCurrencyMask["currency"],
                            "currency-symbol-position":
                              _vm.currentCurrencyMask[
                                "currency-symbol-position"
                              ],
                            "output-type": "String",
                            "empty-value": "0",
                            precision: _vm.currentCurrencyMask["precision"],
                            "decimal-separator":
                              _vm.currentCurrencyMask["decimal-separator"],
                            "thousand-separator":
                              _vm.currentCurrencyMask["thousand-separator"],
                            "read-only-class": "font-weight-bold ml-auto",
                            min: _vm.currentCurrencyMask.min,
                            "read-only": ""
                          },
                          model: {
                            value: _vm.dataSource.detteFournisseur.result,
                            callback: function($$v) {
                              _vm.$set(
                                _vm.dataSource.detteFournisseur,
                                "result",
                                _vm._n($$v)
                              )
                            },
                            expression: "dataSource.detteFournisseur.result"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "b-collapse",
                      {
                        staticClass: "pb-3",
                        attrs: { visible: "", id: "collapseDetteFournisseur" }
                      },
                      _vm._l(_vm.dataSource.detteFournisseur.items, function(
                        dette,
                        index
                      ) {
                        return _c(
                          "div",
                          {
                            key: "indexEmprunt-" + index,
                            staticClass: "d-flex p-2 line-item"
                          },
                          [
                            _c("span", [_vm._v(_vm._s(dette.titre))]),
                            _vm._v(" "),
                            _c("vue-numeric", {
                              attrs: {
                                currency: _vm.currentCurrencyMask["currency"],
                                "currency-symbol-position":
                                  _vm.currentCurrencyMask[
                                    "currency-symbol-position"
                                  ],
                                "output-type": "String",
                                "empty-value": "0",
                                precision: _vm.currentCurrencyMask["precision"],
                                "decimal-separator":
                                  _vm.currentCurrencyMask["decimal-separator"],
                                "thousand-separator":
                                  _vm.currentCurrencyMask["thousand-separator"],
                                "read-only-class": " ml-auto",
                                min: _vm.currentCurrencyMask.min,
                                "read-only": true,
                                value: dette.montant
                              }
                            })
                          ],
                          1
                        )
                      }),
                      0
                    ),
                    _vm._v(" "),
                    _c(
                      "h4",
                      {
                        directives: [
                          {
                            name: "b-toggle",
                            rawName: "v-b-toggle.collapseDetteDiverse",
                            modifiers: { collapseDetteDiverse: true }
                          }
                        ],
                        staticClass: "font-weight-bold d-flex pointer"
                      },
                      [
                        _c("i", {
                          staticClass: "fa fa-plus-square when-closed"
                        }),
                        _vm._v(" "),
                        _c("i", {
                          staticClass: "fa fa-minus-square when-opened"
                        }),
                        _vm._v(" "),
                        _c("span", { staticClass: "mx-1" }, [
                          _vm._v("Autres créances")
                        ]),
                        _vm._v(" "),
                        _c("vue-numeric", {
                          attrs: {
                            currency: _vm.currentCurrencyMask["currency"],
                            "currency-symbol-position":
                              _vm.currentCurrencyMask[
                                "currency-symbol-position"
                              ],
                            "output-type": "String",
                            "empty-value": "0",
                            precision: _vm.currentCurrencyMask["precision"],
                            "decimal-separator":
                              _vm.currentCurrencyMask["decimal-separator"],
                            "thousand-separator":
                              _vm.currentCurrencyMask["thousand-separator"],
                            "read-only-class": "font-weight-bold ml-auto",
                            min: _vm.currentCurrencyMask.min,
                            "read-only": ""
                          },
                          model: {
                            value: _vm.dataSource.detteDiverse.result,
                            callback: function($$v) {
                              _vm.$set(
                                _vm.dataSource.detteDiverse,
                                "result",
                                _vm._n($$v)
                              )
                            },
                            expression: "dataSource.detteDiverse.result"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "b-collapse",
                      {
                        staticClass: "pb-3",
                        attrs: { visible: "", id: "collapseDetteDiverse" }
                      },
                      _vm._l(_vm.dataSource.detteDiverse.items, function(
                        remboursement,
                        index
                      ) {
                        return _c(
                          "div",
                          {
                            key: "indexDetteDiverse-" + index,
                            staticClass: "d-flex p-2 line-item"
                          },
                          [
                            _c("span", [_vm._v(_vm._s(remboursement.titre))]),
                            _vm._v(" "),
                            _c("vue-numeric", {
                              attrs: {
                                currency: _vm.currentCurrencyMask["currency"],
                                "currency-symbol-position":
                                  _vm.currentCurrencyMask[
                                    "currency-symbol-position"
                                  ],
                                "output-type": "String",
                                "empty-value": "0",
                                precision: _vm.currentCurrencyMask["precision"],
                                "decimal-separator":
                                  _vm.currentCurrencyMask["decimal-separator"],
                                "thousand-separator":
                                  _vm.currentCurrencyMask["thousand-separator"],
                                "read-only-class": " ml-auto",
                                min: _vm.currentCurrencyMask.min,
                                "read-only": true,
                                value: remboursement.montant
                              }
                            })
                          ],
                          1
                        )
                      }),
                      0
                    )
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "el-footer",
              { staticClass: "d-flex p-2" },
              [
                _vm._v(" Total Passif\n                    "),
                _c("vue-numeric", {
                  attrs: {
                    currency: _vm.currentCurrencyMask["currency"],
                    "currency-symbol-position":
                      _vm.currentCurrencyMask["currency-symbol-position"],
                    "output-type": "String",
                    "empty-value": "0",
                    precision: _vm.currentCurrencyMask["precision"],
                    "decimal-separator":
                      _vm.currentCurrencyMask["decimal-separator"],
                    "thousand-separator":
                      _vm.currentCurrencyMask["thousand-separator"],
                    "read-only-class": " ml-auto",
                    min: _vm.currentCurrencyMask.min,
                    "read-only": "",
                    value: _vm.totalPassifAvecResultatNet
                  }
                })
              ],
              1
            )
          ],
          1
        )
      ]),
      _vm._v(" "),
      _c(
        "a",
        {
          staticStyle: {
            position: "relative",
            display: "block",
            width: "400px"
          },
          attrs: { href: "#" },
          on: {
            click: function($event) {
              $event.preventDefault()
              return _vm.printPdf()
            }
          }
        },
        [
          _c("img", {
            staticClass: "p-0 m-t-10",
            staticStyle: { display: "inline-block" },
            attrs: { src: "/bp/assets/images/icon/pdf.png" }
          }),
          _vm._v(" "),
          _c(
            "span",
            {
              staticStyle: { position: "absolute", top: "25px", left: "60px" }
            },
            [
              _c("h5", { staticClass: "font-medium" }, [
                _vm._v(_vm._s(this.infoProjet.projettitre) + ".pdf")
              ]),
              _vm._v(" "),
              _c("h6", { staticStyle: { "font-size": "11px" } }, [
                _vm._v("Bilan Prévisionnel")
              ])
            ]
          )
        ]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-sm-4 text-left" }, [
      _c("h4", { staticClass: "font-weight-bold" }, [
        _vm._v("Années d'exercices")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/assets/js/views/bp/financier/bilanPrevisionnel.vue":
/*!**********************************************************************!*\
  !*** ./resources/assets/js/views/bp/financier/bilanPrevisionnel.vue ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _bilanPrevisionnel_vue_vue_type_template_id_2ba009ac_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bilanPrevisionnel.vue?vue&type=template&id=2ba009ac&scoped=true& */ "./resources/assets/js/views/bp/financier/bilanPrevisionnel.vue?vue&type=template&id=2ba009ac&scoped=true&");
/* harmony import */ var _bilanPrevisionnel_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./bilanPrevisionnel.vue?vue&type=script&lang=js& */ "./resources/assets/js/views/bp/financier/bilanPrevisionnel.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _bilanPrevisionnel_vue_vue_type_style_index_0_id_2ba009ac_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./bilanPrevisionnel.vue?vue&type=style&index=0&id=2ba009ac&scoped=true&lang=css& */ "./resources/assets/js/views/bp/financier/bilanPrevisionnel.vue?vue&type=style&index=0&id=2ba009ac&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _bilanPrevisionnel_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _bilanPrevisionnel_vue_vue_type_template_id_2ba009ac_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _bilanPrevisionnel_vue_vue_type_template_id_2ba009ac_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "2ba009ac",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/views/bp/financier/bilanPrevisionnel.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/assets/js/views/bp/financier/bilanPrevisionnel.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************!*\
  !*** ./resources/assets/js/views/bp/financier/bilanPrevisionnel.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_bilanPrevisionnel_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./bilanPrevisionnel.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/financier/bilanPrevisionnel.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_bilanPrevisionnel_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/views/bp/financier/bilanPrevisionnel.vue?vue&type=style&index=0&id=2ba009ac&scoped=true&lang=css&":
/*!*******************************************************************************************************************************!*\
  !*** ./resources/assets/js/views/bp/financier/bilanPrevisionnel.vue?vue&type=style&index=0&id=2ba009ac&scoped=true&lang=css& ***!
  \*******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_bilanPrevisionnel_vue_vue_type_style_index_0_id_2ba009ac_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/style-loader!../../../../../../node_modules/css-loader??ref--7-1!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./bilanPrevisionnel.vue?vue&type=style&index=0&id=2ba009ac&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/financier/bilanPrevisionnel.vue?vue&type=style&index=0&id=2ba009ac&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_bilanPrevisionnel_vue_vue_type_style_index_0_id_2ba009ac_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_bilanPrevisionnel_vue_vue_type_style_index_0_id_2ba009ac_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_bilanPrevisionnel_vue_vue_type_style_index_0_id_2ba009ac_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_bilanPrevisionnel_vue_vue_type_style_index_0_id_2ba009ac_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_bilanPrevisionnel_vue_vue_type_style_index_0_id_2ba009ac_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/assets/js/views/bp/financier/bilanPrevisionnel.vue?vue&type=template&id=2ba009ac&scoped=true&":
/*!*****************************************************************************************************************!*\
  !*** ./resources/assets/js/views/bp/financier/bilanPrevisionnel.vue?vue&type=template&id=2ba009ac&scoped=true& ***!
  \*****************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_bilanPrevisionnel_vue_vue_type_template_id_2ba009ac_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./bilanPrevisionnel.vue?vue&type=template&id=2ba009ac&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/financier/bilanPrevisionnel.vue?vue&type=template&id=2ba009ac&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_bilanPrevisionnel_vue_vue_type_template_id_2ba009ac_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_bilanPrevisionnel_vue_vue_type_template_id_2ba009ac_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/assets/js/views/bp/financier/default.emptyBesoins.json":
/*!**************************************************************************!*\
  !*** ./resources/assets/js/views/bp/financier/default.emptyBesoins.json ***!
  \**************************************************************************/
/*! exports provided: immoIncorporelle, immoCorporelle, immoFinanciere, bfr, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"immoIncorporelle\":[],\"immoCorporelle\":{\"materielinfo\":[],\"mobilier\":[],\"materiel-machine-outil\":[],\"batiment-local-espacevente\":[],\"voiture-autres\":[]},\"immoFinanciere\":{\"cautions\":[],\"depots\":[],\"autres\":[]},\"bfr\":{\"bfrOuverture\":[],\"stock\":[],\"creanceClient\":[],\"dettes\":[]}}");

/***/ }),

/***/ "./resources/assets/js/views/bp/financier/default.emptyCharges.json":
/*!**************************************************************************!*\
  !*** ./resources/assets/js/views/bp/financier/default.emptyCharges.json ***!
  \**************************************************************************/
/*! exports provided: matieres-premieres, services-exterieurs, taxes-impots, frais-personnel, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"matieres-premieres\":{\"depenses-produits\":[],\"charges-variables-produits\":[]},\"services-exterieurs\":[],\"taxes-impots\":{\"impots\":[],\"mairie\":[]},\"frais-personnel\":{\"dirigeants\":[],\"salaries\":[],\"stagiaires\":[]}}");

/***/ }),

/***/ "./resources/assets/js/views/bp/financier/default.emptyProduits.json":
/*!***************************************************************************!*\
  !*** ./resources/assets/js/views/bp/financier/default.emptyProduits.json ***!
  \***************************************************************************/
/*! exports provided: articles, volumeVente, resultatsExceptionnels, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"articles\":[],\"volumeVente\":[],\"resultatsExceptionnels\":{\"entrees\":[],\"sorties\":[]}}");

/***/ }),

/***/ "./resources/assets/js/views/bp/financier/default.emptyRessources.json":
/*!*****************************************************************************!*\
  !*** ./resources/assets/js/views/bp/financier/default.emptyRessources.json ***!
  \*****************************************************************************/
/*! exports provided: capital, comptes, emprunts, subventions-aides, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"capital\":{\"numeraire\":[],\"nature\":[]},\"comptes\":[],\"emprunts\":{\"banque\":[],\"microfinance\":[]},\"subventions-aides\":{\"banque\":[],\"institution\":[],\"autres\":[]}}");

/***/ }),

/***/ "./resources/assets/js/views/bp/financier/pdf/PdfBilanPrevisionnel.js":
/*!****************************************************************************!*\
  !*** ./resources/assets/js/views/bp/financier/pdf/PdfBilanPrevisionnel.js ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _suivi_devis_pdf_Pdf__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../suivi/devis/pdf/Pdf */ "./resources/assets/js/views/suivi/devis/pdf/Pdf.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }



var PdfBilanPrevisionnel = /*#__PURE__*/function (_Pdf) {
  _inherits(PdfBilanPrevisionnel, _Pdf);

  var _super = _createSuper(PdfBilanPrevisionnel);

  /**
   *
   * @param base
   * @param base.title {String} Titre du document
   * @param base.data {Object} Ensemble de données
   * @param options {Array} - Autres informations sur le Document
   */
  function PdfBilanPrevisionnel() {
    var base = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

    _classCallCheck(this, PdfBilanPrevisionnel);

    return _super.call(this, base, options);
  }
  /**
   * Style par défaut appliquer à tt les éléments d'un PDF
   */


  _createClass(PdfBilanPrevisionnel, [{
    key: "defaultStyle",
    value: function defaultStyle() {
      this.defaultStyleP = {
        fontSize: 11.2,
        bold: false,
        alignment: 'left',
        font: 'Times-New-Romance'
      };
    }
  }, {
    key: "docDefinition",
    value: function docDefinition() {
      var _docDefinition = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      this.getDefaultFooter();
      return _get(_getPrototypeOf(PdfBilanPrevisionnel.prototype), "docDefinition", this).call(this, _docDefinition);
    }
  }, {
    key: "buildArray",
    value: function buildArray(element, columns) {
      return this.getBodyArrayByLine(element.items, columns, this.getOptionsCell({
        style: ['lefted', 'normal'],
        borderColor: ['#efefef', '#efefef', '#efefef', '#efefef'],
        border: [false, false, false, true]
      }, [{
        position: 1,
        style: {
          style: ['righted', 'normal'],
          borderColor: ['#efefef', '#efefef', '#efefef', '#efefef'],
          border: [false, false, false, true],
          type: {
            name: 'FCFA'
          }
        }
      }], 2));
    }
  }, {
    key: "columnsHeader",
    value: function columnsHeader() {
      return {
        columns: [this.getColumn({
          content: 'ACTIF'
        }, {
          width: '50%',
          margin: [115, 15]
        }), this.getColumn({
          content: 'PASSIF'
        }, {
          width: '50%',
          margin: [112, 15]
        })],
        columnGap: 5
      };
    }
  }, {
    key: "subColumnsHeader",
    value: function subColumnsHeader(title, price) {
      var first = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
      var result = [this.getCell(title.toUpperCase(), {
        style: ['lefted', 'bgColorLightGrey', 'subHeaderBold']
      }), this.getCell(price, {
        style: ['righted', 'bgColorLightGrey'],
        type: {
          name: 'FCFA'
        }
      })];
      return first ? result : [result];
    }
  }, {
    key: "content",
    value: function content() {
      return [{
        text: 'BILAN PREVISIONNEL HT'
      },
      /**
       * Actif, Passif
       */
      this.columnsHeader(),
      /**
       * ACTIFS IMMOBILISÉS, Capitaux propres
       */
      {
        columns: [{
          width: '*',
          table: _suivi_devis_pdf_Pdf__WEBPACK_IMPORTED_MODULE_0__["default"].getArray(['*', '*'], [[this.getCell('ACTIFS IMMOBILISÉS', {
            style: ['lefted', 'bgColorGrey', 'header']
          }), this.getCell(this.data.headerTotaux.immobilises, {
            style: ['righted', 'bgColorGrey', 'header'],
            type: {
              name: 'FCFA'
            }
          })]])
        }, {
          width: '*',
          table: _suivi_devis_pdf_Pdf__WEBPACK_IMPORTED_MODULE_0__["default"].getArray(['*', '*'], [[this.getCell('CAPITAUX PROPRES', {
            style: ['lefted', 'bgColorGrey', 'header']
          }), this.getCell(this.data.headerTotaux.capitauxPropres, {
            style: ['righted', 'bgColorGrey', 'header'],
            type: {
              name: 'FCFA'
            }
          })]])
        }],
        columnGap: 5
      },
      /**
       * Elements ACTIFS IMMOBILISÉS and Elements Capitaux propres
       */
      {
        columns: [
        /**
         * Elements ACTIFS IMMOBILISÉS
         */
        {
          width: '*',
          table: _suivi_devis_pdf_Pdf__WEBPACK_IMPORTED_MODULE_0__["default"].getArray(['65%', '*'], [this.subColumnsHeader('Immobilisations incorporelles', this.data.dataSource.immoIncorporelle.result, true)].concat(this.buildArray(this.data.dataSource.immoIncorporelle, ['titre', 'prix'])).concat(this.subColumnsHeader('Immobilisations corporelles', this.data.dataSource.immoCorporelle.result)).concat(this.buildArray(this.data.dataSource.immoIncorporelle, ['titre', 'prix'])).concat(this.subColumnsHeader('Immobilisations financières', this.data.dataSource.immoFinanciere.result)).concat(this.buildArray(this.data.dataSource.immoFinanciere, ['titre', 'montant'])).concat(this.subColumnsHeader('Amortissements', this.data.dataSource.amortissementCorporelle.result)).concat(this.buildArray(this.data.dataSource.amortissementCorporelle, ['titre', 'prix'])))
        },
        /**
         * Elements Capitaux propres
         */
        {
          width: '*',
          table: _suivi_devis_pdf_Pdf__WEBPACK_IMPORTED_MODULE_0__["default"].getArray(['65%', '*'], [this.subColumnsHeader('Capitaux numéraire et nature', this.data.dataSource.capital.result, true)].concat(this.buildArray(this.data.dataSource.capital, ['apporteur', 'montant'])).concat(this.subColumnsHeader('Emprunts', this.data.dataSource.emprunt.result)).concat(this.buildArray(this.data.dataSource.emprunt, ['titre', 'montant'])).concat(this.subColumnsHeader('Résultat net', this.data.resultatNet)))
        }],
        columnGap: 5
      }, {
        text: '',
        pageBreak: 'before'
      },
      /**
       * Actif, Passif
       */
      this.columnsHeader(),
      /**
       *  ACTIFS CIRCULANTS, Dettes
       */
      {
        columns: [{
          width: '*',
          table: _suivi_devis_pdf_Pdf__WEBPACK_IMPORTED_MODULE_0__["default"].getArray(['*', '*'], [[this.getCell('ACTIFS CIRCULANTS', {
            style: ['lefted', 'bgColorGrey', 'header']
          }), this.getCell(this.data.headerTotaux.circulants, {
            style: ['righted', 'bgColorGrey', 'header'],
            type: {
              name: 'FCFA'
            }
          })]])
        }, {
          width: '*',
          table: _suivi_devis_pdf_Pdf__WEBPACK_IMPORTED_MODULE_0__["default"].getArray(['*', '*'], [[this.getCell('DETTES', {
            style: ['lefted', 'bgColorGrey', 'header']
          }), this.getCell(this.data.headerTotaux.dettes, {
            style: ['righted', 'bgColorGrey', 'header'],
            type: {
              name: 'FCFA'
            }
          })]])
        }],
        columnGap: 5
      },
      /**
       * Elements ACTIFS CIRCULANTS and Dettes
       */
      {
        columns: [{
          width: '*',
          table: _suivi_devis_pdf_Pdf__WEBPACK_IMPORTED_MODULE_0__["default"].getArray(['*', '*'], [this.subColumnsHeader('Stocks en cours', this.data.dataSource.stockEnCours.result, true)].concat(this.buildArray(this.data.dataSource.stockEnCours, ['titre', 'montant'])).concat(this.subColumnsHeader('Créance', this.data.dataSource.creance.result)).concat(this.buildArray(this.data.dataSource.creance, ['titre', 'montant'])).concat(this.subColumnsHeader('Compte courant', this.data.dataSource.compteCourant.result)).concat(this.buildArray(this.data.dataSource.compteCourant, ['apporteur', 'montant'])))
        }, {
          width: '*',
          table: _suivi_devis_pdf_Pdf__WEBPACK_IMPORTED_MODULE_0__["default"].getArray(['*', '*'], [this.subColumnsHeader('Dettes commerciales', this.data.dataSource.detteFournisseur.result, true)].concat(this.buildArray(this.data.dataSource.detteFournisseur, ['titre', 'montant'])).concat(this.subColumnsHeader('Autres créances', this.data.dataSource.detteDiverse.result)).concat(this.buildArray(this.data.dataSource.detteDiverse, ['titre', 'montant'])))
        }]
      }];
    }
  }]);

  return PdfBilanPrevisionnel;
}(_suivi_devis_pdf_Pdf__WEBPACK_IMPORTED_MODULE_0__["default"]);

/* harmony default export */ __webpack_exports__["default"] = (PdfBilanPrevisionnel);

/***/ })

}]);
//# sourceMappingURL=financier.bilanPrevisionnel.js.map