(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["bp.profile.user"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/profile/user.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/bp/profile/user.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_form_wizard__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-form-wizard */ "./node_modules/vue-form-wizard/dist/vue-form-wizard.js");
/* harmony import */ var vue_form_wizard__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue_form_wizard__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _services_helper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/helper */ "./resources/assets/js/services/helper.js");
/* harmony import */ var click_confirm__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! click-confirm */ "./node_modules/click-confirm/dist/click-confirm.js");
/* harmony import */ var click_confirm__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(click_confirm__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vue-loading-overlay */ "./node_modules/vue-loading-overlay/dist/vue-loading.min.js");
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(vue_loading_overlay__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var vue_good_table__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! vue-good-table */ "./node_modules/vue-good-table/dist/vue-good-table.esm.js");
/* harmony import */ var vue_good_table_dist_vue_good_table_css__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! vue-good-table/dist/vue-good-table.css */ "./node_modules/vue-good-table/dist/vue-good-table.css");
/* harmony import */ var vue_good_table_dist_vue_good_table_css__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(vue_good_table_dist_vue_good_table_css__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _mixins_currenciesMask__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../mixins/currenciesMask */ "./resources/assets/js/views/bp/mixins/currenciesMask.js");
/* harmony import */ var _currency_bus__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../currency-bus */ "./resources/assets/js/currency-bus.js");
/* harmony import */ var _store_summary__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../store/summary */ "./resources/assets/js/store/summary.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//








 // Import the EventBus we just created.


 // Init plugin

vue__WEBPACK_IMPORTED_MODULE_1___default.a.use(vue_loading_overlay__WEBPACK_IMPORTED_MODULE_5___default.a, {
  isFullPage: true,
  canCancel: false,
  loader: "spinner"
});
/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    FormWizard: vue_form_wizard__WEBPACK_IMPORTED_MODULE_0__["FormWizard"],
    TabContent: vue_form_wizard__WEBPACK_IMPORTED_MODULE_0__["TabContent"],
    VueGoodTable: vue_good_table__WEBPACK_IMPORTED_MODULE_6__["VueGoodTable"],
    ClickConfirm: click_confirm__WEBPACK_IMPORTED_MODULE_3___default.a
  },
  mixins: [_mixins_currenciesMask__WEBPACK_IMPORTED_MODULE_8__["default"]],
  props: {},
  data: function data() {
    var userInfo = JSON.parse(localStorage.getItem("userSession"));
    var userid = userInfo.uid;
    var name = userInfo.name;
    var prenom = userInfo.firstName;
    var telephone = userInfo.phoneNumber;
    var email = userInfo.email;
    return {
      users_count: '',
      loader: vue__WEBPACK_IMPORTED_MODULE_1___default.a.$loading.show(),
      tasks_count: '',
      recent_incomplete_tasks: {},
      user: {
        name: name,
        prenom: prenom,
        email: email,
        telephone: telephone
      },
      setting: {
        devise: JSON.parse(localStorage.getItem("currencySession")).id,
        tva: '10%'
      },
      todos: [],
      todoForm: new Form({
        'todo': ''
      }),
      show_todo_status: '',
      projets: [],
      errors: [],
      ref: firebase__WEBPACK_IMPORTED_MODULE_4___default.a.firestore().collection('summary').where("user", "==", userid),
      columns: [{
        label: 'Identifiant',
        field: 'identifiant'
      }, {
        label: 'Projet',
        field: 'titre',
        filterable: true
      }, {
        label: 'Forme Juridique',
        field: 'formJuridique'
      }, {
        label: 'Action',
        field: 'action',
        html: true
      }],
      rows: []
    };
  },
  created: function created() {
    var _this = this;

    this.ref.onSnapshot(function (querySnapshot) {
      _this.projets = [];
      querySnapshot.forEach(function (doc) {
        _this.rows.push({
          'identifiant': doc.data().identifiant,
          'titre': doc.data().titre,
          'formJuridique': doc.data().formJuridique,
          'action': doc.id
        });
      });

      _this.loader.hide();
    });
  },
  updated: function updated() {},
  methods: {
    printPdf: function printPdf(id) {},
    submit: function submit(e) {
      var email = this.user.email;
      var nom = this.user.nom;
      var prenom = this.user.prenom;
      var tel = this.user.telephone;
      var URLphoto = '';
      var userCurrent = firebase__WEBPACK_IMPORTED_MODULE_4___default.a.auth().currentUser;
      userCurrent.updateProfile({
        displayName: nom + ' ' + prenom,
        photoURL: URLphoto,
        phoneNumber: tel
      }).then(function () {
        /*let ent = db.collection('entreprise').doc(entrepri);
        let zone = db.collection('zone_collection').doc(zoneid);*/
        var authId = userCurrent.uid;
        db.collection("users").doc(authId).update({
          nom: nom,
          prenom: prenom,
          tel: tel,
          photo: URLphoto
        });
      });
      var userSession = {
        name: nom,
        firstName: prenom,
        displayName: nom + ' ' + prenom,
        email: email,
        phoneNumber: tel,
        photoURL: URLphoto,
        uid: userCurrent.uid
      };
    },
    emitGlobalCurrencyEvent: function emitGlobalCurrencyEvent() {
      // Send the event on a channel (i-got-clicked) with a payload (the click count.)
      _currency_bus__WEBPACK_IMPORTED_MODULE_9__["CurrencyBus"].$emit('reload-currency', this.setting.devise);
    },
    getAuthUserFullName: function getAuthUserFullName() {
      if (localStorage.getItem("userSession")) {
        var userInfo = JSON.parse(localStorage.getItem("userSession"));
        var datef = userInfo.firstTimeConnection;
        var date = new Date(datef).toLocaleString(); //console.log(userInfo);

        var profileName = userInfo.displayName;
        return profileName;
      }
    },
    getAuthUserName: function getAuthUserName() {
      if (localStorage.getItem("userSession")) {
        var userInfo = JSON.parse(localStorage.getItem("userSession"));
        var datef = userInfo.firstTimeConnection;
        var date = new Date(datef).toLocaleString(); //console.log(userInfo);

        var Name = userInfo.name;
        return Name;
      }
    },
    getAuthUserEmail: function getAuthUserEmail() {
      if (localStorage.getItem("userSession")) {
        var userInfo = JSON.parse(localStorage.getItem("userSession"));
        var datef = userInfo.firstTimeConnection;
        var date = new Date(datef).toLocaleString(); //console.log(userInfo);

        var email = userInfo.email;
        return email;
      }
    },
    getAuthUserPhone: function getAuthUserPhone() {
      if (localStorage.getItem("userSession")) {
        var userInfo = JSON.parse(localStorage.getItem("userSession"));
        var datef = userInfo.firstTimeConnection;
        var date = new Date(datef).toLocaleString(); //console.log(userInfo);

        var phoneNumber = userInfo.phoneNumber;
        return phoneNumber;
      }
    },
    getAuthUser: function getAuthUser(name) {
      return this.$store.getters.getAuthUser(name);
    },
    storeTodo: function storeTodo() {
      var _this2 = this;

      this.todoForm.post('/api/todo').then(function (response) {
        toastr['success'](response.message);

        _this2.todos.unshift(response.data);
      })["catch"](function (response) {
        toastr['error'](response.message);
      });
    },
    deleteTodo: function deleteTodo(todo) {
      var _this3 = this;

      axios["delete"]('/api/todo/' + todo.id).then(function (response) {
        toastr['success'](response.data.message);

        _this3.getTodos();
      })["catch"](function (error) {
        toastr['error'](error.response.data.message);
      });
    },
    toggleTodoStatus: function toggleTodoStatus(todo) {
      axios.post('/api/todo/status', {
        id: todo.id
      }).then(function (response) {
        todo.status = !todo.status;
      })["catch"](function (error) {
        toastr['error'](error.response.message);
      });
    },
    filterTodo: function filterTodo() {
      this.getTodos();
    },
    getProgress: function getProgress(task) {
      return 'width: ' + task.progress + '%;';
    },
    getProgressColor: function getProgressColor(task) {
      return _services_helper__WEBPACK_IMPORTED_MODULE_2__["default"].taskColor(task.progress);
    },
    // export du 
    "export": function _export(identifiant) {
      var _this4 = this;

      /*
      let loader = this.$loading.show({
        // Optional parameters
        container: this.fullPage ? null : this.$refs.formContainer,
        canCancel: true,
        onCancel: (loader)  => {
         },
      });
      // simulate AJAX
      setTimeout(() => {
        loader.hide()
      },5000)   
      */
      alert('Hello');
      firebase__WEBPACK_IMPORTED_MODULE_4___default.a.firestore().collection('summary').doc(identifiant).get().then(function (doc) {
        if (doc.exists) {
          console.log("Document data:", doc.data());
          alert('Hello');

          try {
            _this4.printPdfSummary(doc.data());
          } catch (error) {}
        } else {
          // doc.data() will be undefined in this case
          console.log("No such document!");
        }
      });
    }
  },
  computed: {},
  filters: {
    moment: function moment(date) {
      return _services_helper__WEBPACK_IMPORTED_MODULE_2__["default"].formatDate(date);
    },
    momentWithTime: function momentWithTime(date) {
      return _services_helper__WEBPACK_IMPORTED_MODULE_2__["default"].formatDateTime(date);
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/profile/user.vue?vue&type=style&index=0&lang=css&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/bp/profile/user.vue?vue&type=style&index=0&lang=css& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.strikethrough{\ntext-decoration: line-through;\n}\n.over {\n    position: relative;\n    width: 100%;\n}\n.overlay {\n    position: absolute; \n    bottom: 0; \n    background: rgb(0, 0, 0);\n    background: rgba(0, 0, 0, 0.5); /* Black see-through */\n    color: #f1f1f1; \n    width: 95%;\n    transition: .5s ease;\n    opacity:0;\n    color: white;\n    font-size: 40px;\n    font-weight: bold;\n    padding: 50px;\n    margin-right:50px;\n    text-align: center;\n}\n.over1:hover .overlay {\n    opacity: 0.8;\n}\n.over2:hover .overlay {\nopacity: 0.8;\n}\n.over3:hover .overlay {\nopacity: 0.8;\n}\n.over4:hover .overlay {\nopacity: 0.8;\n}\n.strikethrough{\n    text-decoration: line-through;\n}\nH1 {\n    font-weight: 900;\n}\n#bizcanvas {\n    border: 3px solid #3160AA;\n    width: 100%;\n}\n#bizcanvas td {\n    vertical-align: top;\n    height: 200px;\n    width: 200px;\n    padding: 6px;\n}\n\n/* icon des cases bus. model*/\n[class^=\"icon-\"], [class*=\" icon-\"] {\n    font-size: 18px;\n    margin-right: 5px;\n}\n#bizcanvas H4 {\n    font-weight: 700;\n    font-size: 1em;\n}\nh4 a {\n    color: #3160AA;\n}\n#bizcanvas H5 {\n    font-weight: 700;\n    font-size: 0.7em;\n}\n#bizcanvas p {\n    font-weight: 300;\n    font-size: 0.8em;\n}\ntextarea {\n    width: 96%;\n    max-width: 96%;\n    min-width: 96%;\n    padding: 10px 2%;\n    height: 240px;\n    border: none;\n}\n.my-card {\n    border: solid 1px #398bf7 !important;\n}\n\n\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/profile/user.vue?vue&type=style&index=0&lang=css&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/bp/profile/user.vue?vue&type=style&index=0&lang=css& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader??ref--7-1!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./user.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/profile/user.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/profile/user.vue?vue&type=template&id=479093af&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/bp/profile/user.vue?vue&type=template&id=479093af& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "page-wrapper" }, [
    _c(
      "div",
      { staticClass: "container-fluid", staticStyle: { "margin-top": "10px" } },
      [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-lg-4 col-xlg-3 col-md-5" }, [
            _c("div", { staticClass: "card" }, [
              _c(
                "div",
                { staticClass: "card-body" },
                [
                  _c("center", { staticClass: "m-t-30" }, [
                    _c("img", {
                      staticClass: "img-circle",
                      attrs: {
                        src: "/bp/assets/images/users/logodefault.png",
                        width: "150"
                      }
                    }),
                    _vm._v(" "),
                    _c("h4", { staticClass: "card-title m-t-10" }, [
                      _vm._v(_vm._s(_vm.getAuthUserFullName()) + " ")
                    ]),
                    _vm._v(" "),
                    _c("h6", { staticClass: "card-subtitle" }, [
                      _vm._v(_vm._s(_vm.getAuthUserEmail()))
                    ])
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _vm._m(0),
              _vm._v(" "),
              _c("div", { staticClass: "card-body" }, [
                _c("small", { staticClass: "text-muted" }, [_vm._v("Email ")]),
                _vm._v(" "),
                _c("h6", [_vm._v(_vm._s(_vm.getAuthUserEmail()))]),
                _vm._v(" "),
                _c("small", { staticClass: "text-muted p-t-30 db" }, [
                  _vm._v("Téléphone")
                ]),
                _vm._v(" "),
                _c("h6", [_vm._v(_vm._s(_vm.getAuthUserPhone()))]),
                _vm._v(" "),
                _vm._m(1),
                _vm._v(" "),
                _c("small", { staticClass: "text-muted p-t-30 db" }, [
                  _vm._v("Profils sociaux")
                ]),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _vm._m(2),
                _vm._v(" "),
                _vm._m(3),
                _vm._v(" "),
                _vm._m(4)
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-lg-8 col-xlg-9 col-md-7" }, [
            _c("div", { staticClass: "card" }, [
              _vm._m(5),
              _vm._v(" "),
              _c("div", { staticClass: "tab-content" }, [
                _c(
                  "div",
                  {
                    staticClass: "tab-pane active",
                    attrs: { id: "home", role: "tabpanel" }
                  },
                  [
                    _c("div", { staticClass: "card-body" }, [
                      _c("div", { staticClass: "sl-item" }, [
                        _c("div", { staticClass: "sl-right" }, [
                          _c(
                            "table",
                            {
                              staticClass:
                                "table table-responsive table-striped"
                            },
                            [
                              _vm._m(6),
                              _vm._v(" "),
                              _c(
                                "tbody",
                                _vm._l(_vm.rows, function(item) {
                                  return _c("tr", [
                                    _c("td", [
                                      _vm._v(_vm._s(item.identifiant))
                                    ]),
                                    _vm._v(" "),
                                    _c("td", [
                                      _vm._v(_vm._s(item.titre.toUpperCase()))
                                    ]),
                                    _vm._v(" "),
                                    _c("td", [
                                      _vm._v(_vm._s(item.formJuridique))
                                    ]),
                                    _vm._v(" "),
                                    _c("td", [
                                      _c(
                                        "a",
                                        {
                                          staticClass: "btn btn-success",
                                          attrs: {
                                            href: "/bp/summary/" + item.action,
                                            "data-toggle": "tooltip",
                                            title: "consulter"
                                          }
                                        },
                                        [_c("i", { staticClass: "fa fa-eye" })]
                                      ),
                                      _vm._v(" "),
                                      _vm._m(7, true)
                                    ])
                                  ])
                                }),
                                0
                              )
                            ]
                          )
                        ])
                      ])
                    ])
                  ]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "tab-pane",
                    attrs: { id: "profile", role: "tabpanel" }
                  },
                  [
                    _c("div", { staticClass: "card-body" }, [
                      _c(
                        "form",
                        { staticClass: "form-horizontal form-material" },
                        [
                          _c("div", { staticClass: "form-group" }, [
                            _c("label", { staticClass: "col-md-12" }, [
                              _vm._v("Devise")
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-12" }, [
                              _c(
                                "select",
                                {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.setting.devise,
                                      expression: "setting.devise"
                                    }
                                  ],
                                  staticClass: "form-control form-control-line",
                                  attrs: { name: "sortBy" },
                                  on: {
                                    change: [
                                      function($event) {
                                        var $$selectedVal = Array.prototype.filter
                                          .call($event.target.options, function(
                                            o
                                          ) {
                                            return o.selected
                                          })
                                          .map(function(o) {
                                            var val =
                                              "_value" in o ? o._value : o.value
                                            return val
                                          })
                                        _vm.$set(
                                          _vm.setting,
                                          "devise",
                                          $event.target.multiple
                                            ? $$selectedVal
                                            : $$selectedVal[0]
                                        )
                                      },
                                      function($event) {
                                        return _vm.emitGlobalCurrencyEvent()
                                      }
                                    ]
                                  }
                                },
                                _vm._l(_vm.currencyMaskConfig, function(
                                  monnaie
                                ) {
                                  return _c(
                                    "option",
                                    {
                                      key: monnaie.id,
                                      domProps: { value: monnaie.id }
                                    },
                                    [
                                      _c("i", {
                                        staticClass: "flag-icon",
                                        class: monnaie["icon-flag"]
                                      }),
                                      _vm._v(
                                        " " +
                                          _vm._s(monnaie.literal) +
                                          "\n                                            "
                                      )
                                    ]
                                  )
                                }),
                                0
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group" }, [
                            _c("label", { staticClass: "col-md-12" }, [
                              _vm._v("TVA")
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-12" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.setting.tva,
                                    expression: "setting.tva"
                                  }
                                ],
                                staticClass: "form-control form-control-line",
                                attrs: { type: "text" },
                                domProps: { value: _vm.setting.tva },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.setting,
                                      "tva",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ]),
                          _vm._v(" "),
                          _vm._m(8)
                        ]
                      )
                    ])
                  ]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "tab-pane",
                    attrs: { id: "settings", role: "tabpanel" }
                  },
                  [
                    _c("div", { staticClass: "card-body" }, [
                      _c(
                        "form",
                        { staticClass: "form-horizontal form-material" },
                        [
                          _c("div", { staticClass: "form-group" }, [
                            _c("label", { staticClass: "col-md-12" }, [
                              _vm._v("Nom")
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-12" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.user.name,
                                    expression: "user.name"
                                  }
                                ],
                                staticClass: "form-control form-control-line",
                                attrs: { type: "text" },
                                domProps: { value: _vm.user.name },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.user,
                                      "name",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group" }, [
                            _c("label", { staticClass: "col-md-12" }, [
                              _vm._v("Prénom")
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-12" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.user.prenom,
                                    expression: "user.prenom"
                                  }
                                ],
                                staticClass: "form-control form-control-line",
                                attrs: { type: "text" },
                                domProps: { value: _vm.user.prenom },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.user,
                                      "prenom",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group" }, [
                            _c(
                              "label",
                              {
                                staticClass: "col-md-12",
                                attrs: { for: "example-email" }
                              },
                              [_vm._v("Email")]
                            ),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-12" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.user.email,
                                    expression: "user.email"
                                  }
                                ],
                                staticClass: "form-control form-control-line",
                                attrs: {
                                  type: "email",
                                  name: "example-email",
                                  id: "example-email"
                                },
                                domProps: { value: _vm.user.email },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.user,
                                      "email",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group" }, [
                            _c("label", { staticClass: "col-md-12" }, [
                              _vm._v("Téléphone")
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-12" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.user.telephone,
                                    expression: "user.telephone"
                                  }
                                ],
                                staticClass: "form-control form-control-line",
                                attrs: { type: "text" },
                                domProps: { value: _vm.user.telephone },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.user,
                                      "telephone",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ]),
                          _vm._v(" "),
                          _vm._m(9),
                          _vm._v(" "),
                          _vm._m(10)
                        ]
                      )
                    ])
                  ]
                )
              ])
            ])
          ])
        ])
      ]
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [_c("hr")])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "map-box" }, [
      _c("iframe", {
        staticStyle: { border: "0" },
        attrs: {
          src: "",
          width: "100%",
          height: "150",
          frameborder: "0",
          allowfullscreen: ""
        }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("button", { staticClass: "btn btn-circle btn-secondary" }, [
      _c("i", { staticClass: "fab fa-facebook-f" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("button", { staticClass: "btn btn-circle btn-secondary" }, [
      _c("i", { staticClass: "fab fa-twitter" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("button", { staticClass: "btn btn-circle btn-secondary" }, [
      _c("i", { staticClass: "fab fa-youtube" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "ul",
      { staticClass: "nav nav-tabs profile-tab", attrs: { role: "tablist" } },
      [
        _c("li", { staticClass: "nav-item" }, [
          _c(
            "a",
            {
              staticClass: "nav-link active",
              attrs: { "data-toggle": "tab", href: "#home", role: "tab" }
            },
            [_vm._v("Projets")]
          )
        ]),
        _vm._v(" "),
        _c("li", { staticClass: "nav-item" }, [
          _c(
            "a",
            {
              staticClass: "nav-link",
              attrs: { "data-toggle": "tab", href: "#settings", role: "tab" }
            },
            [_vm._v("Profile")]
          )
        ]),
        _vm._v(" "),
        _c("li", { staticClass: "nav-item" }, [
          _c(
            "a",
            {
              staticClass: "nav-link",
              attrs: { "data-toggle": "tab", href: "#profile", role: "tab" }
            },
            [_vm._v("Paramètres")]
          )
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("Identifiant")]),
        _vm._v(" "),
        _c("th", [_vm._v("Titre")]),
        _vm._v(" "),
        _c("th", [_vm._v("Forme Juridique")]),
        _vm._v(" "),
        _c("th", [_vm._v("Action")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      {
        staticClass: "btn btn-primary",
        attrs: { href: "#", "data-toggle": "tooltip", title: "télécharger" }
      },
      [_c("i", { staticClass: "fa fa-download" })]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group" }, [
      _c("div", { staticClass: "col-sm-12" }, [
        _c("button", { staticClass: "btn btn-success" }, [
          _vm._v("Mise à Jour")
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group" }, [
      _c("label", { staticClass: "col-md-12" }, [_vm._v("Mot de passe")]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-12" }, [
        _c("input", {
          staticClass: "form-control form-control-line",
          attrs: { type: "password", value: "password" }
        })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group" }, [
      _c("div", { staticClass: "col-sm-12" }, [
        _c(
          "button",
          { staticClass: "btn btn-success", attrs: { type: "submit" } },
          [_vm._v("Mise à Jour")]
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/assets/js/currency-bus.js":
/*!*********************************************!*\
  !*** ./resources/assets/js/currency-bus.js ***!
  \*********************************************/
/*! exports provided: CurrencyBus */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CurrencyBus", function() { return CurrencyBus; });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);

var CurrencyBus = new vue__WEBPACK_IMPORTED_MODULE_0___default.a();

/***/ }),

/***/ "./resources/assets/js/store/summary.js":
/*!**********************************************!*\
  !*** ./resources/assets/js/store/summary.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_0__);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }



var summaryStore = function summaryStore(id) {
  _classCallCheck(this, summaryStore);

  this.docRef = firebase__WEBPACK_IMPORTED_MODULE_0___default.a.firestore().collection('summary').doc(id);
};

/* harmony default export */ __webpack_exports__["default"] = (summaryStore);

/***/ }),

/***/ "./resources/assets/js/views/bp/profile/user.vue":
/*!*******************************************************!*\
  !*** ./resources/assets/js/views/bp/profile/user.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _user_vue_vue_type_template_id_479093af___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./user.vue?vue&type=template&id=479093af& */ "./resources/assets/js/views/bp/profile/user.vue?vue&type=template&id=479093af&");
/* harmony import */ var _user_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./user.vue?vue&type=script&lang=js& */ "./resources/assets/js/views/bp/profile/user.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _user_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./user.vue?vue&type=style&index=0&lang=css& */ "./resources/assets/js/views/bp/profile/user.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _user_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _user_vue_vue_type_template_id_479093af___WEBPACK_IMPORTED_MODULE_0__["render"],
  _user_vue_vue_type_template_id_479093af___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/views/bp/profile/user.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/assets/js/views/bp/profile/user.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/assets/js/views/bp/profile/user.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_user_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./user.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/profile/user.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_user_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/views/bp/profile/user.vue?vue&type=style&index=0&lang=css&":
/*!****************************************************************************************!*\
  !*** ./resources/assets/js/views/bp/profile/user.vue?vue&type=style&index=0&lang=css& ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_user_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/style-loader!../../../../../../node_modules/css-loader??ref--7-1!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./user.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/profile/user.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_user_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_user_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_user_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_user_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_user_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/assets/js/views/bp/profile/user.vue?vue&type=template&id=479093af&":
/*!**************************************************************************************!*\
  !*** ./resources/assets/js/views/bp/profile/user.vue?vue&type=template&id=479093af& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_user_vue_vue_type_template_id_479093af___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./user.vue?vue&type=template&id=479093af& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/profile/user.vue?vue&type=template&id=479093af&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_user_vue_vue_type_template_id_479093af___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_user_vue_vue_type_template_id_479093af___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);
//# sourceMappingURL=bp.profile.user.js.map