(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["financier.dashboard"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/layouts/sidebar-financier.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/layouts/sidebar-financier.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _services_helper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../services/helper */ "./resources/assets/js/services/helper.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _assets_js_views_bp_mixins_currenciesMask__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../assets/js/views/bp/mixins/currenciesMask */ "./resources/assets/js/views/bp/mixins/currenciesMask.js");
/* harmony import */ var _currency_bus__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../currency-bus */ "./resources/assets/js/currency-bus.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


 // Import the EventBus.


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      perm: JSON.parse(localStorage.getItem('userSession')).permissions.bpFinancier
    };
  },
  mixins: [_assets_js_views_bp_mixins_currenciesMask__WEBPACK_IMPORTED_MODULE_2__["default"]],
  methods: {},
  computed: {
    menuLevel1: function menuLevel1() {
      return this.perm.filter(function (i) {
        return i.haschildren == false && i.checked;
      });
    },
    menuLevel2: function menuLevel2() {
      return this.perm.filter(function (i) {
        return i.haschildren && i.checked;
      });
    }
  },
  created: function created() {
    var _this = this;

    // Listen for the i-got-clicked event and its payload.
    _currency_bus__WEBPACK_IMPORTED_MODULE_3__["CurrencyBus"].$on('reload-currency', function (currencyId) {
      // console.log(`Oh, that's nice. It's gotten ${currencyId} reload ! :)`)
      _this.applyCurrencyMask(currencyId);
    });
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/financier/dashboard.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/bp/financier/dashboard.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _layouts_sidebar_financier__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../layouts/sidebar-financier */ "./resources/assets/js/layouts/sidebar-financier.vue");
/* harmony import */ var _mixins_financier_besoins__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../mixins/financier/besoins */ "./resources/assets/js/views/bp/mixins/financier/besoins.js");
/* harmony import */ var _mixins_financier_ressources__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../mixins/financier/ressources */ "./resources/assets/js/views/bp/mixins/financier/ressources.js");
/* harmony import */ var _mixins_currenciesMask__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../mixins/currenciesMask */ "./resources/assets/js/views/bp/mixins/currenciesMask.js");
/* harmony import */ var _mixins_financier_produits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../mixins/financier/produits */ "./resources/assets/js/views/bp/mixins/financier/produits.js");
/* harmony import */ var _mixins_financier_charges__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../mixins/financier/charges */ "./resources/assets/js/views/bp/mixins/financier/charges.js");
/* harmony import */ var _default_chargePatronale_json__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./default.chargePatronale.json */ "./resources/assets/js/views/bp/financier/default.chargePatronale.json");
var _default_chargePatronale_json__WEBPACK_IMPORTED_MODULE_8___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./default.chargePatronale.json */ "./resources/assets/js/views/bp/financier/default.chargePatronale.json", 1);
/* harmony import */ var vue_numeric__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! vue-numeric */ "./node_modules/vue-numeric/dist/vue-numeric.min.js");
/* harmony import */ var vue_numeric__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(vue_numeric__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _default_emptyBesoins_json__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./default.emptyBesoins.json */ "./resources/assets/js/views/bp/financier/default.emptyBesoins.json");
var _default_emptyBesoins_json__WEBPACK_IMPORTED_MODULE_10___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./default.emptyBesoins.json */ "./resources/assets/js/views/bp/financier/default.emptyBesoins.json", 1);
/* harmony import */ var _default_emptyRessources_json__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./default.emptyRessources.json */ "./resources/assets/js/views/bp/financier/default.emptyRessources.json");
var _default_emptyRessources_json__WEBPACK_IMPORTED_MODULE_11___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./default.emptyRessources.json */ "./resources/assets/js/views/bp/financier/default.emptyRessources.json", 1);
/* harmony import */ var _default_emptyProduits_json__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./default.emptyProduits.json */ "./resources/assets/js/views/bp/financier/default.emptyProduits.json");
var _default_emptyProduits_json__WEBPACK_IMPORTED_MODULE_12___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./default.emptyProduits.json */ "./resources/assets/js/views/bp/financier/default.emptyProduits.json", 1);
/* harmony import */ var _default_emptyCharges_json__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./default.emptyCharges.json */ "./resources/assets/js/views/bp/financier/default.emptyCharges.json");
var _default_emptyCharges_json__WEBPACK_IMPORTED_MODULE_13___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./default.emptyCharges.json */ "./resources/assets/js/views/bp/financier/default.emptyCharges.json", 1);
/* harmony import */ var _services_helper__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../../services/helper */ "./resources/assets/js/services/helper.js");
/* harmony import */ var _default_tvanormale_json__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./default.tvanormale.json */ "./resources/assets/js/views/bp/financier/default.tvanormale.json");
var _default_tvanormale_json__WEBPACK_IMPORTED_MODULE_15___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./default.tvanormale.json */ "./resources/assets/js/views/bp/financier/default.tvanormale.json", 1);
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! util */ "./node_modules/node-libs-browser/node_modules/util/util.js");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(util__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var vue_apexcharts__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! vue-apexcharts */ "./node_modules/vue-apexcharts/dist/vue-apexcharts.js");
/* harmony import */ var vue_apexcharts__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(vue_apexcharts__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var apexcharts__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! apexcharts */ "./node_modules/apexcharts/dist/apexcharts.esm.js");
/* harmony import */ var _toolbar__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./toolbar */ "./resources/assets/js/views/bp/financier/toolbar.js");
/* harmony import */ var _toolbar__WEBPACK_IMPORTED_MODULE_19___default = /*#__PURE__*/__webpack_require__.n(_toolbar__WEBPACK_IMPORTED_MODULE_19__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// elements de menus


 //fonctions produits et charges









 // default datas bp financier





 // tva par defaut 


 // Import VueApexChart




/* harmony default export */ __webpack_exports__["default"] = ({
  mixins: [_mixins_financier_besoins__WEBPACK_IMPORTED_MODULE_3__["default"], _mixins_financier_ressources__WEBPACK_IMPORTED_MODULE_4__["default"], _mixins_currenciesMask__WEBPACK_IMPORTED_MODULE_5__["default"], _mixins_financier_produits__WEBPACK_IMPORTED_MODULE_6__["default"], _mixins_financier_charges__WEBPACK_IMPORTED_MODULE_7__["default"]],
  data: function data() {
    return {
      listeBesoins: [],
      grapheOptions: {
        labels: ['Immo incorporelles', 'Immo Corporelles', 'Immo financières', 'BFR', 'Remboursement Emprunts'],
        colors: ['#1a0c72', '#06d79c', '#36A2EB', '#FFCE56', '#FF0000'],
        chart: {
          stacked: false,
          zoom: {
            type: 'x',
            enabled: true,
            autoScaleYaxis: true
          },
          toolbar: {
            show: true,
            autoSelected: 'zoom'
          },
          height: '50%'
        },
        plotOptions: {
          pie: {
            expandOnClick: false,
            donut: {
              labels: {
                show: true,
                total: {
                  showAlways: true,
                  show: true
                }
              }
            }
          }
        },
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
      },
      listeRessources: [],
      grapheRessourceOptions: {
        labels: ['Apport Capital', 'C. Courant Associés', 'Emprunts bancaires', 'Subventions'],
        colors: ["#1a0c72", "#06d79c", "#36A2EB", "#FFCE56"],
        chart: {
          stacked: false,
          zoom: {
            type: 'x',
            enabled: true,
            autoScaleYaxis: true
          },
          toolbar: {
            show: true,
            autoSelected: 'zoom'
          },
          height: '50%'
        },
        plotOptions: {
          expandOnClick: false,
          pie: {
            donut: {
              labels: {
                show: true,
                total: {
                  showAlways: true,
                  show: true
                }
              }
            }
          }
        },
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
      },
      listeCA: [],
      grapheCAOptions: {
        labels: ['Chiffre d\'Affaires HT'],
        colors: ["#1a0c72"],
        chart: {
          stacked: true,
          height: '100%',
          zoom: {
            enabled: true,
            type: 'x',
            autoScaleYaxis: false,
            zoomedArea: {
              fill: {
                color: '#90CAF9',
                opacity: 0.4
              },
              stroke: {
                color: '#0D47A1',
                opacity: 0.4,
                width: 1
              }
            }
          },
          toolbar: {
            show: true,
            tools: {
              download: true,
              selection: true,
              zoom: true,
              zoomin: true,
              zoomout: true,
              pan: true,
              customIcons: []
            },
            autoSelected: 'zoom'
          }
        },
        plotOptions: {
          expandOnClick: false,
          pie: {
            donut: {
              labels: {
                show: true,
                total: {
                  showAlways: true,
                  show: true
                }
              }
            }
          }
        },
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 300
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
      },
      listeCACV: [],
      grapheCACVOptions: {
        labels: ['Chiffre d\'Affaires HT', 'Charge Variable'],
        colors: ["#1a0c72", "#FF0000"],
        chart: {
          stacked: true,
          toolbar: {
            show: true,
            tools: {
              download: true,
              selection: true,
              zoom: true,
              zoomin: true,
              zoomout: true,
              pan: true,
              customIcons: []
            },
            autoSelected: 'zoom'
          },
          height: '100%',
          zoom: {
            enabled: true,
            type: 'x',
            autoScaleYaxis: false,
            zoomedArea: {
              fill: {
                color: '#90CAF9',
                opacity: 0.4
              },
              stroke: {
                color: '#0D47A1',
                opacity: 0.4,
                width: 1
              }
            }
          }
        },
        plotOptions: {
          expandOnClick: false,
          pie: {
            donut: {
              labels: {
                show: true,
                total: {
                  showAlways: true,
                  show: true
                }
              }
            }
          }
        },
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
      },
      listeCVCACF: [],
      grapheCVCACFOptions: {
        labels: ['Charges', 'Chiffre d\'Affaires HT'],
        colors: ["#FF0000", "#06d79c"],
        dataLabels: {
          enabled: true
        },
        chart: {
          stacked: true,
          toolbar: {
            show: true,
            tools: {
              download: true,
              selection: true,
              zoom: true,
              zoomin: true,
              zoomout: true,
              pan: true,
              reset: true | '<img src="/static/icons/reset.png" width="20">',
              customIcons: []
            },
            autoSelected: 'zoom'
          },
          height: '100%',
          zoom: {
            enabled: true,
            type: 'x',
            autoScaleYaxis: false,
            zoomedArea: {
              fill: {
                color: '#90CAF9',
                opacity: 0.4
              },
              stroke: {
                color: '#0D47A1',
                opacity: 0.4,
                width: 1
              }
            }
          }
        },
        plotOptions: {
          pie: {
            expandOnClick: false,
            donut: {
              labels: {
                show: true,
                total: {
                  showAlways: true,
                  show: true
                }
              }
            }
          }
        },
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
      },
      serie: [{
        name: 'Chiffre d\'Affaire',
        type: 'column',
        data: []
      }, {
        name: 'Seuil de rentabilite',
        type: 'line',
        data: []
      }, {
        name: 'Point mort',
        type: 'column',
        data: []
      }],
      chartOptions: {
        chart: {
          height: 350,
          type: 'line',
          stacked: true,
          id: 'seuil'
        },
        dataLabels: {
          enabled: false
        },
        colors: ["#008FFB", "#FFFF00", "#00E396"],
        stroke: {
          width: 3
        },
        title: {
          text: 'Seuil de rentabilite et Point mort'
        },
        xaxis: {
          categories: ['Mois 01', 'Mois 02', 'Mois 03', 'Mois 04', 'Mois 05', 'Mois 06', 'Mois 07', 'Mois 08', 'Mois 09', 'Mois 10', 'Mois 11', 'Mois 12']
        },
        yaxis: [{
          show: true,
          showAlways: true,
          forceNiceScale: false,
          axisTicks: {
            show: true
          },
          axisBorder: {
            show: true,
            color: '#1a0c72'
          },
          labels: {
            style: {
              color: '#1a0c72'
            }
          },
          tooltip: {
            enabled: true
          }
        }, {
          show: false,
          showAlways: false,
          seriesName: "Chiffre d'Affaire",
          opposite: false,
          axisTicks: {
            show: true
          },
          axisBorder: {
            show: true,
            color: '#00E396'
          },
          labels: {
            style: {
              color: '#00E396'
            }
          },
          title: {
            text: "Seuil de rentabilite",
            style: {
              color: '#00E396'
            }
          }
        }, {
          show: false,
          showAlways: false,
          seriesName: "Chiffre d'Affaire",
          opposite: false,
          axisTicks: {
            show: true
          },
          axisBorder: {
            show: true,
            color: '#008FFB'
          },
          labels: {
            style: {
              color: '#008FFB'
            }
          },
          title: {
            text: "Point mort",
            style: {
              color: '#008FFB'
            }
          }
        }],
        tooltip: {
          fixed: {
            enabled: true,
            position: 'topLeft',
            // topRight, topLeft, bottomRight, bottomLeft
            offsetY: 30,
            offsetX: 60
          }
        },
        legend: {
          horizontalAlign: 'left',
          offsetX: 40
        }
      },
      series: [{
        name: 'Solde cumule (-)',
        type: 'column',
        data: []
      }, {
        name: 'Solde cumule (+)',
        type: 'column',
        data: []
      }, {
        name: 'Solde mois',
        type: 'line',
        data: []
      }],
      options: {
        chart: {
          height: 350,
          type: 'line',
          stacked: true
        },
        dataLabels: {
          enabled: false
        },
        colors: ["#FF0000", "#00E396", function (_ref) {
          var value = _ref.value,
              seriesIndex = _ref.seriesIndex,
              w = _ref.w;

          if (value <= 0) {
            return "#FF0000";
          } else {
            return "#008FFB";
          }
        }],
        stroke: {
          width: 3
        },
        title: {
          text: 'Plan de tresorerie'
        },
        markers: {
          size: 2
        },
        xaxis: {
          categories: ['Initial', 'Mois 01', 'Mois 02', 'Mois 03', 'Mois 04', 'Mois 05', 'Mois 06', 'Mois 07', 'Mois 08', 'Mois 09', 'Mois 10', 'Mois 11', 'Mois 12']
        },
        yaxis: [{
          seriesName: "Solde fin mois",
          show: true,
          showAlways: true,
          forceNiceScale: false,
          axisTicks: {
            show: true
          },
          axisBorder: {
            show: true,
            color: '#1a0c72'
          },
          labels: {
            style: {
              color: '#1a0c72'
            }
          },
          tooltip: {
            enabled: true
          },
          title: {
            text: "Solde fin mois",
            style: {
              color: '#1a0c72'
            }
          }
        }, {
          seriesName: "Solde fin mois",
          show: false,
          showAlways: false,
          opposite: false,
          axisTicks: {
            show: true
          },
          axisBorder: {
            show: true,
            color: '#008FFB'
          },
          labels: {
            style: {
              color: '#008FFB'
            }
          }
        }, {
          show: false,
          showAlways: false,
          opposite: false,
          axisTicks: {
            show: true
          },
          axisBorder: {
            show: true,
            color: '#00E396'
          },
          labels: {
            style: {
              color: '#00E396'
            }
          },
          title: {
            text: "Solde fin mois",
            style: {
              color: '#00E396'
            }
          }
        }],
        tooltip: {
          fixed: {
            enabled: true,
            position: 'topLeft',
            // topRight, topLeft, bottomRight, bottomLeft
            offsetY: 30,
            offsetX: 60
          }
        },
        legend: {
          horizontalAlign: 'left',
          offsetX: 40
        }
      },
      projet: JSON.parse(localStorage.getItem('userSession')).projet,
      besoins: _default_emptyBesoins_json__WEBPACK_IMPORTED_MODULE_10__,
      ressources: _default_emptyRessources_json__WEBPACK_IMPORTED_MODULE_11__,
      produits: _default_emptyProduits_json__WEBPACK_IMPORTED_MODULE_12__,
      charges: _default_emptyCharges_json__WEBPACK_IMPORTED_MODULE_13__,
      paramTva: _default_tvanormale_json__WEBPACK_IMPORTED_MODULE_15__,
      donutDatabesoins: [],
      donutDataressources: [],
      donutDataca: [],
      donutDatacacv: [],
      donutDatacacvcf: [],
      names: ["Charge Financières", "Dotation aux amortissements", "Frais de personnel", "Impots et taxes", "Services Exterieurs", "Achat"],
      values: [[40, 10, 10], [40, 10, 10], [30, 40, 30], [15, 20, 40], [10, 30, 30], [5, 10, 30]],
      barData: [{
        mois: 'Initial',
        ca: 10
      }, {
        mois: 'Mois 1',
        ca: 10
      }, {
        mois: 'Mois 2',
        ca: 50
      }, {
        mois: 'Mois 3',
        ca: 20
      }, {
        mois: 'Mois 4',
        ca: 20
      }, {
        mois: 'Mois 5',
        ca: 30
      }, {
        mois: 'Mois 6',
        ca: 20
      }, {
        mois: 'Mois 7',
        ca: 20
      }, {
        mois: 'Mois 8',
        ca: 20
      }, {
        mois: 'Mois 9',
        ca: 20
      }, {
        mois: 'Mois 10',
        ca: 20
      }, {
        mois: 'Mois 11',
        ca: 20
      }, {
        mois: 'Mois 12',
        ca: 20
      }],
      lineData: [{
        mois: 'Initial',
        ca: 10
      }, {
        mois: 'Mois 1',
        ca: 10
      }, {
        mois: 'Mois 2',
        ca: 50
      }, {
        mois: 'Mois 3',
        ca: 20
      }, {
        mois: 'Mois 4',
        ca: 20
      }, {
        mois: 'Mois 5',
        ca: 30
      }, {
        mois: 'Mois 6',
        ca: 20
      }, {
        mois: 'Mois 7',
        ca: 20
      }, {
        mois: 'Mois 8',
        ca: 20
      }, {
        mois: 'Mois 9',
        ca: 20
      }, {
        mois: 'Mois 10',
        ca: 20
      }, {
        mois: 'Mois 11',
        ca: 20
      }, {
        mois: 'Mois 12',
        ca: 20
      }]
    };
  },
  methods: {
    besoinsPourcentage: function besoinsPourcentage(element) {
      if (element !== 0) {
        return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(element * 100 / this.totalBesoins.initial, 1);
      } else {
        return 0;
      } //return Math.round(element*100/this.totalBesoins.initial);

    },
    ressourcesPourcentage: function ressourcesPourcentage(element) {
      //return Math.round(element*100/this.totalRessources.initial);
      if (element !== 0) {
        return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(element * 100 / this.totalRessources.initial, 1);
      } else {
        return 0;
      }
    }
  },
  computed: {
    totalRemboursements: function totalRemboursements() {
      // combine les remboursements nets des prets et subventions
      var result = {
        annee1: 0,
        annee2: 0,
        annee3: 0,
        annee4: 0
      };
      this.calculRemboursement(this.ressources.emprunts.banque);
      result.annee1 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalRemboursementAnnuelEmprunt.annee1.montant + this.totalRemboursementAnnuelSubvention.annee1.montant, this.currentCurrencyMask[':precision']);
      result.annee2 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalRemboursementAnnuelEmprunt.annee2.montant + this.totalRemboursementAnnuelSubvention.annee2.montant, this.currentCurrencyMask[':precision']);
      result.annee3 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalRemboursementAnnuelEmprunt.annee3.montant + this.totalRemboursementAnnuelSubvention.annee3.montant, this.currentCurrencyMask[':precision']);
      result.annee4 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalRemboursementAnnuelEmprunt.annee4.montant + this.totalRemboursementAnnuelSubvention.annee4.montant, this.currentCurrencyMask[':precision']);
      return result;
    },
    chargesFixes: function chargesFixes() {
      var result = {
        annee1: 0,
        annee2: 0,
        annee3: 0
      };
      result.annee1 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalServExtFrais.annee1 + this.totalFraisPersonnel.annee1 + this.totalTaxeImpot.annee1 + this.totalInterets.annee1 + this.totalAmortisCorporelleAnnuel.annee1, this.currentCurrencyMask[':precision']);
      result.annee2 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalServExtFrais.annee2 + this.totalFraisPersonnel.annee2 + this.totalTaxeImpot.annee2 + this.totalInterets.annee2 + this.totalAmortisCorporelleAnnuel.annee2, this.currentCurrencyMask[':precision']);
      result.annee3 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalServExtFrais.annee3 + this.totalFraisPersonnel.annee3 + this.totalTaxeImpot.annee3 + this.totalInterets.annee3 + this.totalAmortisCorporelleAnnuel.annee3, this.currentCurrencyMask[':precision']);
      return result;
    },
    totalRessources: function totalRessources() {
      var initial = 0;
      var annee1 = 0;
      var annee2 = 0;
      var annee3 = 0;
      initial = initial + this.totalApportCapital + this.totalCompteAssocie + this.totalEmpruntBancaire.montant + this.totalSubventionAide.montant;
      /* annee1 = annee1 + this.capaciteAutofinancement.annee1;
      annee2 = annee2 + this.capaciteAutofinancement.annee2;
      annee3 = annee3 + this.capaciteAutofinancement.annee3;
      annee4 = annee4 + this.capaciteAutofinancement.annee4; */

      return {
        initial: Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(initial, this.currentCurrencyMask[':precision']),
        annee1: Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(annee1, this.currentCurrencyMask[':precision']),
        annee2: Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(annee2, this.currentCurrencyMask[':precision']),
        annee3: Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(annee3, this.currentCurrencyMask[':precision'])
      };
    },
    totalBesoins: function totalBesoins() {
      var initial = 0;
      var annee1 = 0;
      var annee2 = 0;
      var annee3 = 0;
      var annee4 = 0;
      initial = initial + this.totalImmoIncorporelle + this.totalImmoCorporelle.prix + this.totalImmoFinanciere + this.totalBfrOuverture;
      annee1 = annee1 + this.totalAmortisCorporelleAnnuel.annee1 + this.totalBfrExploitation.annee1 + this.totalRemboursements.annee1 + this.variationBfr.annee1;
      annee2 = annee2 + this.totalAmortisCorporelleAnnuel.annee2 + this.totalBfrExploitation.annee2 + this.totalRemboursements.annee2 + this.variationBfr.annee2;
      annee3 = annee3 + this.totalAmortisCorporelleAnnuel.annee3 + this.totalBfrExploitation.annee3 + this.totalRemboursements.annee3 + this.variationBfr.annee3;
      return {
        initial: Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(initial, this.currentCurrencyMask[':precision']),
        annee1: Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(annee1, this.currentCurrencyMask[':precision']),
        annee2: Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(annee2, this.currentCurrencyMask[':precision']),
        annee3: Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(annee3, this.currentCurrencyMask[':precision'])
      };
    },
    totalCharge: function totalCharge() {
      return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalAchat.annee1 + this.totalServExtFrais.annee1 + this.totalTaxeImpot.annee1 + this.totalFraisPersonnel.annee1 + this.totalAmortisCorporelleAnnuel.annee1 + this.totalInterets.annee1, this.currentCurrencyMask[':precision']);
    },
    totalInterets: function totalInterets() {
      // charges financiere
      var result = {
        annee1: 0,
        annee2: 0,
        annee3: 0,
        annee4: 0
      };
      result.annee1 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalRemboursementAnnuelEmprunt.annee1.interet + this.totalRemboursementAnnuelSubvention.annee1.interet, this.currentCurrencyMask[':precision']);
      result.annee2 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalRemboursementAnnuelEmprunt.annee2.interet + this.totalRemboursementAnnuelSubvention.annee2.interet, this.currentCurrencyMask[':precision']);
      result.annee3 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalRemboursementAnnuelEmprunt.annee3.interet + this.totalRemboursementAnnuelSubvention.annee3.interet, this.currentCurrencyMask[':precision']);
      result.annee4 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalRemboursementAnnuelEmprunt.annee4.interet + this.totalRemboursementAnnuelSubvention.annee4.interet, this.currentCurrencyMask[':precision']);
      return result;
    },
    margeBruteCommerciale: function margeBruteCommerciale() {
      var result = {
        annee1: 0,
        annee2: 0,
        annee3: 0
      };
      result.annee1 = this.totalChiffreAffaireHT.annee1.montant - this.totalAchatMpDepVariable.annee1;
      result.annee2 = this.totalChiffreAffaireHT.annee2.montant - this.totalAchatMpDepVariable.annee2;
      result.annee3 = this.totalChiffreAffaireHT.annee3.montant - this.totalAchatMpDepVariable.annee3;
      return {
        annee1: Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(result.annee1, this.currentCurrencyMask[':precision']),
        annee2: Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(result.annee2, this.currentCurrencyMask[':precision']),
        annee3: Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(result.annee3, this.currentCurrencyMask[':precision'])
      };
    },
    margeCoutsVariables: function margeCoutsVariables() {
      var result = {
        annee1: 0,
        annee2: 0,
        annee3: 0
      };
      result.annee1 = this.margeBruteCommerciale.annee1 - this.totalAchatMpCharVariable.annee1;
      result.annee2 = this.margeBruteCommerciale.annee2 - this.totalAchatMpCharVariable.annee2;
      result.annee3 = this.margeBruteCommerciale.annee3 - this.totalAchatMpCharVariable.annee3;
      return {
        annee1: Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(result.annee1, this.currentCurrencyMask[':precision']),
        annee2: Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(result.annee2, this.currentCurrencyMask[':precision']),
        annee3: Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(result.annee3, this.currentCurrencyMask[':precision'])
      };
    },
    tauxMargeVariable: function tauxMargeVariable() {
      var result = {
        annee1: 0,
        annee2: 0,
        annee3: 0
      };

      if (this.totalChiffreAffaireHT.annee1.montant > 0) {
        result.annee1 = this.margeCoutsVariables.annee1 * 100 / this.totalChiffreAffaireHT.annee1.montant;
      }

      ;

      if (this.totalChiffreAffaireHT.annee2.montant > 0) {
        result.annee2 = this.margeCoutsVariables.annee2 * 100 / this.totalChiffreAffaireHT.annee2.montant;
      }

      ;

      if (this.totalChiffreAffaireHT.annee3.montant > 0) {
        result.annee3 = this.margeCoutsVariables.annee3 * 100 / this.totalChiffreAffaireHT.annee3.montant;
      }

      ;
      return {
        annee1: Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(result.annee1, this.currentCurrencyMask[':precision']),
        annee2: Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(result.annee2, this.currentCurrencyMask[':precision']),
        annee3: Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(result.annee3, this.currentCurrencyMask[':precision'])
      };
    },
    seuilRentabilite: function seuilRentabilite() {
      var result = {
        annee1: 0,
        annee2: 0,
        annee3: 0
      };
      result.annee1 = this.tauxMargeVariable.annee1 > 0 ? this.chargesFixes.annee1 * 100 / this.tauxMargeVariable.annee1 : 0;
      result.annee2 = this.tauxMargeVariable.annee2 > 0 ? this.chargesFixes.annee2 * 100 / this.tauxMargeVariable.annee2 : 0;
      result.annee3 = this.tauxMargeVariable.annee3 > 0 ? this.chargesFixes.annee3 * 100 / this.tauxMargeVariable.annee3 : 0;
      return {
        annee1: Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(result.annee1, this.currentCurrencyMask[':precision']),
        annee2: Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(result.annee2, this.currentCurrencyMask[':precision']),
        annee3: Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(result.annee3, this.currentCurrencyMask[':precision'])
      };
    },
    pointMort: function pointMort() {
      var result = 0;
      result = this.totalChiffreAffaireHT.annee1.montant > 0 ? this.seuilRentabilite.annee1 / this.totalChiffreAffaireHT.annee1.montant * 12 : 0;
      return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(result, this.currentCurrencyMask[':precision']);
    },
    chargesFinancieres: function chargesFinancieres() {
      var result = {
        "mois1": 0,
        "mois2": 0,
        "mois3": 0,
        "mois4": 0,
        "mois5": 0,
        "mois6": 0,
        "mois7": 0,
        "mois8": 0,
        "mois9": 0,
        "mois10": 0,
        "mois11": 0,
        "mois12": 0,
        "total": 0,
        "initial": 0
      }; // insertion des items de remboursement d'emprunt et subventions

      [].concat(this.ressources['emprunts']['microfinance'], this.ressources['emprunts']['banque'], this.ressources['subventions-aides']['institution'], this.ressources['subventions-aides']['autres'], this.ressources['subventions-aides']['banque']).forEach(function (emprunt) {
        // cumul des totaux financiers
        result.mois1 += Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["versement"])(1, {
          "capital": emprunt.montantPret,
          "duree": emprunt.duree,
          "taux": emprunt.taux / 100
        });
        result.mois2 += Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["versement"])(2, {
          "capital": emprunt.montantPret,
          "duree": emprunt.duree,
          "taux": emprunt.taux / 100
        });
        result.mois3 += Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["versement"])(3, {
          "capital": emprunt.montantPret,
          "duree": emprunt.duree,
          "taux": emprunt.taux / 100
        });
        result.mois4 += Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["versement"])(4, {
          "capital": emprunt.montantPret,
          "duree": emprunt.duree,
          "taux": emprunt.taux / 100
        });
        result.mois5 += Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["versement"])(5, {
          "capital": emprunt.montantPret,
          "duree": emprunt.duree,
          "taux": emprunt.taux / 100
        });
        result.mois6 += Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["versement"])(6, {
          "capital": emprunt.montantPret,
          "duree": emprunt.duree,
          "taux": emprunt.taux / 100
        });
        result.mois7 += Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["versement"])(7, {
          "capital": emprunt.montantPret,
          "duree": emprunt.duree,
          "taux": emprunt.taux / 100
        });
        result.mois8 += Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["versement"])(8, {
          "capital": emprunt.montantPret,
          "duree": emprunt.duree,
          "taux": emprunt.taux / 100
        });
        result.mois9 += Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["versement"])(9, {
          "capital": emprunt.montantPret,
          "duree": emprunt.duree,
          "taux": emprunt.taux / 100
        });
        result.mois10 += Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["versement"])(10, {
          "capital": emprunt.montantPret,
          "duree": emprunt.duree,
          "taux": emprunt.taux / 100
        });
        result.mois11 += Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["versement"])(11, {
          "capital": emprunt.montantPret,
          "duree": emprunt.duree,
          "taux": emprunt.taux / 100
        });
        result.mois12 += Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["versement"])(12, {
          "capital": emprunt.montantPret,
          "duree": emprunt.duree,
          "taux": emprunt.taux / 100
        });
        result.total += Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["cumulVersementAnnuel"])(1, {
          "capital": emprunt.montantPret,
          "duree": emprunt.duree,
          "taux": emprunt.taux / 100
        });
      });
      return result;
    },
    totalInvestissements: function totalInvestissements() {
      return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalApportCapital + this.totalCompteAssocie + this.totalEmpruntBancaire.montant + this.totalSubventionAide.montant, this.currentCurrencyMask[':precision']);
    },
    totalAchatsBesoins: function totalAchatsBesoins() {
      return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalImmoIncorporelle + this.totalImmoCorporelle.prix + this.totalImmoFinanciere + this.totalBfrOuverture, this.currentCurrencyMask[':precision']);
    },
    totalEncaissementTresorerie: function totalEncaissementTresorerie() {
      return {
        "initial": this.totalInvestissements,
        "mois1": this.totalChiffreAffaireHT.annee1.volumeVente["01"],
        "mois2": this.totalChiffreAffaireHT.annee1.volumeVente["02"],
        "mois3": this.totalChiffreAffaireHT.annee1.volumeVente["03"],
        "mois4": this.totalChiffreAffaireHT.annee1.volumeVente["04"],
        "mois5": this.totalChiffreAffaireHT.annee1.volumeVente["05"],
        "mois6": this.totalChiffreAffaireHT.annee1.volumeVente["06"],
        "mois7": this.totalChiffreAffaireHT.annee1.volumeVente["07"],
        "mois8": this.totalChiffreAffaireHT.annee1.volumeVente["08"],
        "mois9": this.totalChiffreAffaireHT.annee1.volumeVente["09"],
        "mois10": this.totalChiffreAffaireHT.annee1.volumeVente["10"],
        "mois11": this.totalChiffreAffaireHT.annee1.volumeVente["11"],
        "mois12": this.totalChiffreAffaireHT.annee1.volumeVente["12"],
        "total": Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalInvestissements + this.totalChiffreAffaireHT.annee1.montant, this.currentCurrencyMask[':precision'])
      };
    },
    totalDecaissementTresorerie: function totalDecaissementTresorerie() {
      var result = {
        "mois1": 0,
        "mois2": 0,
        "mois3": 0,
        "mois4": 0,
        "mois5": 0,
        "mois6": 0,
        "mois7": 0,
        "mois8": 0,
        "mois9": 0,
        "mois10": 0,
        "mois11": 0,
        "mois12": 0,
        "total": 0,
        "initial": 0
      };
      result['initial'] = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalAchatsBesoins, this.currentCurrencyMask[':precision']);
      result.mois1 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalAchatMpDepVariable.depensesMensuellesLancement["01"].ttc + this.totalServExtFrais.depensesMensuellesLancement["01"] + this.totalTaxeImpot.annee1 / 12 + this.chargesFinancieres.mois1 + this.totalTva.annee1.volumeVente['01'] + this.calculTotalFraisPersonnelMensuel(this.charges['frais-personnel']['dirigeants'].concat(this.charges['frais-personnel']['salaries'], this.charges['frais-personnel']['stagiaires']), 1) + this.calculChargeSocialeMensuelle(this.charges['frais-personnel']['dirigeants'], this.chargeSocialeDirigeant.taux, 1) + this.calculChargeSocialeMensuelle(this.charges['frais-personnel']['salaries'].concat(this.charges['frais-personnel']['stagiaires']), this.chargeSocialeEmploye.taux, 1), this.currentCurrencyMask[':precision']);
      result.mois2 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalAchatMpDepVariable.depensesMensuellesLancement["02"].ttc + this.totalServExtFrais.depensesMensuellesLancement["02"] + this.totalTaxeImpot.annee1 / 12 + this.chargesFinancieres.mois2 + this.totalTva.annee1.volumeVente['02'] + this.calculTotalFraisPersonnelMensuel(this.charges['frais-personnel']['dirigeants'].concat(this.charges['frais-personnel']['salaries'], this.charges['frais-personnel']['stagiaires']), 2) + this.calculChargeSocialeMensuelle(this.charges['frais-personnel']['dirigeants'], this.chargeSocialeDirigeant.taux, 2) + this.calculChargeSocialeMensuelle(this.charges['frais-personnel']['salaries'].concat(this.charges['frais-personnel']['stagiaires']), this.chargeSocialeEmploye.taux, 2), this.currentCurrencyMask[':precision']);
      result.mois3 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalAchatMpDepVariable.depensesMensuellesLancement["03"].ttc + this.totalServExtFrais.depensesMensuellesLancement["03"] + this.totalTaxeImpot.annee1 / 12 + this.chargesFinancieres.mois3 + this.totalTva.annee1.volumeVente['03'] + this.calculTotalFraisPersonnelMensuel(this.charges['frais-personnel']['dirigeants'].concat(this.charges['frais-personnel']['salaries'], this.charges['frais-personnel']['stagiaires']), 3) + this.calculChargeSocialeMensuelle(this.charges['frais-personnel']['dirigeants'], this.chargeSocialeDirigeant.taux, 3) + this.calculChargeSocialeMensuelle(this.charges['frais-personnel']['salaries'].concat(this.charges['frais-personnel']['stagiaires']), this.chargeSocialeEmploye.taux, 3), this.currentCurrencyMask[':precision']);
      result.mois4 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalAchatMpDepVariable.depensesMensuellesLancement["04"].ttc + this.totalServExtFrais.depensesMensuellesLancement["04"] + this.totalTaxeImpot.annee1 / 12 + this.chargesFinancieres.mois4 + this.totalTva.annee1.volumeVente['04'] + this.calculTotalFraisPersonnelMensuel(this.charges['frais-personnel']['dirigeants'].concat(this.charges['frais-personnel']['salaries'], this.charges['frais-personnel']['stagiaires']), 4) + this.calculChargeSocialeMensuelle(this.charges['frais-personnel']['dirigeants'], this.chargeSocialeDirigeant.taux, 4) + this.calculChargeSocialeMensuelle(this.charges['frais-personnel']['salaries'].concat(this.charges['frais-personnel']['stagiaires']), this.chargeSocialeEmploye.taux, 4), this.currentCurrencyMask[':precision']);
      result.mois5 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalAchatMpDepVariable.depensesMensuellesLancement["05"].ttc + this.totalServExtFrais.depensesMensuellesLancement["05"] + this.totalTaxeImpot.annee1 / 12 + this.chargesFinancieres.mois5 + this.totalTva.annee1.volumeVente['05'] + this.calculTotalFraisPersonnelMensuel(this.charges['frais-personnel']['dirigeants'].concat(this.charges['frais-personnel']['salaries'], this.charges['frais-personnel']['stagiaires']), 5) + this.calculChargeSocialeMensuelle(this.charges['frais-personnel']['dirigeants'], this.chargeSocialeDirigeant.taux, 5) + this.calculChargeSocialeMensuelle(this.charges['frais-personnel']['salaries'].concat(this.charges['frais-personnel']['stagiaires']), this.chargeSocialeEmploye.taux, 5), this.currentCurrencyMask[':precision']);
      result.mois6 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalAchatMpDepVariable.depensesMensuellesLancement["06"].ttc + this.totalServExtFrais.depensesMensuellesLancement["06"] + this.totalTaxeImpot.annee1 / 12 + this.chargesFinancieres.mois6 + this.totalTva.annee1.volumeVente['06'] + this.calculTotalFraisPersonnelMensuel(this.charges['frais-personnel']['dirigeants'].concat(this.charges['frais-personnel']['salaries'], this.charges['frais-personnel']['stagiaires']), 6) + this.calculChargeSocialeMensuelle(this.charges['frais-personnel']['dirigeants'], this.chargeSocialeDirigeant.taux, 6) + this.calculChargeSocialeMensuelle(this.charges['frais-personnel']['salaries'].concat(this.charges['frais-personnel']['stagiaires']), this.chargeSocialeEmploye.taux, 6), this.currentCurrencyMask[':precision']);
      result.mois7 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalAchatMpDepVariable.depensesMensuellesLancement["07"].ttc + this.totalServExtFrais.depensesMensuellesLancement["07"] + this.totalTaxeImpot.annee1 / 12 + this.chargesFinancieres.mois7 + this.totalTva.annee1.volumeVente['07'] + this.calculTotalFraisPersonnelMensuel(this.charges['frais-personnel']['dirigeants'].concat(this.charges['frais-personnel']['salaries'], this.charges['frais-personnel']['stagiaires']), 7) + this.calculChargeSocialeMensuelle(this.charges['frais-personnel']['dirigeants'], this.chargeSocialeDirigeant.taux, 7) + this.calculChargeSocialeMensuelle(this.charges['frais-personnel']['salaries'].concat(this.charges['frais-personnel']['stagiaires']), this.chargeSocialeEmploye.taux, 7), this.currentCurrencyMask[':precision']);
      result.mois8 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalAchatMpDepVariable.depensesMensuellesLancement["08"].ttc + this.totalServExtFrais.depensesMensuellesLancement["08"] + this.totalTaxeImpot.annee1 / 12 + this.chargesFinancieres.mois8 + this.totalTva.annee1.volumeVente['08'] + this.calculTotalFraisPersonnelMensuel(this.charges['frais-personnel']['dirigeants'].concat(this.charges['frais-personnel']['salaries'], this.charges['frais-personnel']['stagiaires']), 8) + this.calculChargeSocialeMensuelle(this.charges['frais-personnel']['dirigeants'], this.chargeSocialeDirigeant.taux, 8) + this.calculChargeSocialeMensuelle(this.charges['frais-personnel']['salaries'].concat(this.charges['frais-personnel']['stagiaires']), this.chargeSocialeEmploye.taux, 8), this.currentCurrencyMask[':precision']);
      result.mois9 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalAchatMpDepVariable.depensesMensuellesLancement["09"].ttc + this.totalServExtFrais.depensesMensuellesLancement["09"] + this.totalTaxeImpot.annee1 / 12 + this.chargesFinancieres.mois9 + this.totalTva.annee1.volumeVente['09'] + this.calculTotalFraisPersonnelMensuel(this.charges['frais-personnel']['dirigeants'].concat(this.charges['frais-personnel']['salaries'], this.charges['frais-personnel']['stagiaires']), 9) + this.calculChargeSocialeMensuelle(this.charges['frais-personnel']['dirigeants'], this.chargeSocialeDirigeant.taux, 9) + this.calculChargeSocialeMensuelle(this.charges['frais-personnel']['salaries'].concat(this.charges['frais-personnel']['stagiaires']), this.chargeSocialeEmploye.taux, 9), this.currentCurrencyMask[':precision']);
      result.mois10 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalAchatMpDepVariable.depensesMensuellesLancement["10"].ttc + this.totalServExtFrais.depensesMensuellesLancement["10"] + this.totalTaxeImpot.annee1 / 12 + this.chargesFinancieres.mois10 + this.totalTva.annee1.volumeVente['10'] + this.calculTotalFraisPersonnelMensuel(this.charges['frais-personnel']['dirigeants'].concat(this.charges['frais-personnel']['salaries'], this.charges['frais-personnel']['stagiaires']), 10) + this.calculChargeSocialeMensuelle(this.charges['frais-personnel']['dirigeants'], this.chargeSocialeDirigeant.taux, 10) + this.calculChargeSocialeMensuelle(this.charges['frais-personnel']['salaries'].concat(this.charges['frais-personnel']['stagiaires']), this.chargeSocialeEmploye.taux, 10), this.currentCurrencyMask[':precision']);
      result.mois11 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalAchatMpDepVariable.depensesMensuellesLancement["11"].ttc + this.totalServExtFrais.depensesMensuellesLancement["11"] + this.totalTaxeImpot.annee1 / 12 + this.chargesFinancieres.mois11 + this.totalTva.annee1.volumeVente['11'] + this.calculTotalFraisPersonnelMensuel(this.charges['frais-personnel']['dirigeants'].concat(this.charges['frais-personnel']['salaries'], this.charges['frais-personnel']['stagiaires']), 11) + this.calculChargeSocialeMensuelle(this.charges['frais-personnel']['dirigeants'], this.chargeSocialeDirigeant.taux, 11) + this.calculChargeSocialeMensuelle(this.charges['frais-personnel']['salaries'].concat(this.charges['frais-personnel']['stagiaires']), this.chargeSocialeEmploye.taux, 11), this.currentCurrencyMask[':precision']);
      result.mois12 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalAchatMpDepVariable.depensesMensuellesLancement["12"].ttc + this.totalServExtFrais.depensesMensuellesLancement["12"] + this.totalTaxeImpot.annee1 / 12 + this.chargesFinancieres.mois12 + this.totalTva.annee1.volumeVente['12'] + this.calculTotalFraisPersonnelMensuel(this.charges['frais-personnel']['dirigeants'].concat(this.charges['frais-personnel']['salaries'], this.charges['frais-personnel']['stagiaires']), 12) + this.calculChargeSocialeMensuelle(this.charges['frais-personnel']['dirigeants'], this.chargeSocialeDirigeant.taux, 12) + this.calculChargeSocialeMensuelle(this.charges['frais-personnel']['salaries'].concat(this.charges['frais-personnel']['stagiaires']), this.chargeSocialeEmploye.taux, 12), this.currentCurrencyMask[':precision']);
      result.total = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalAchatsBesoins + this.totalAchatMpDepVariable.depensesLancement.ttc + this.totalServExtFrais.depensesLancement + this.totalTaxeImpot.annee1 + this.chargesFinancieres.total + this.totalTva.annee1.montant + this.totalFraisPersonnel.annee1, this.currentCurrencyMask[':precision']);
      return result;
    },
    soldeFinMois: function soldeFinMois() {
      var result = {
        "mois1": 0,
        "mois2": 0,
        "mois3": 0,
        "mois4": 0,
        "mois5": 0,
        "mois6": 0,
        "mois7": 0,
        "mois8": 0,
        "mois9": 0,
        "mois10": 0,
        "mois11": 0,
        "mois12": 0,
        "total": 0,
        "initial": 0
      };
      result.initial = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalEncaissementTresorerie.initial - this.totalDecaissementTresorerie.initial, this.currentCurrencyMask[':precision']);
      result.mois1 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalEncaissementTresorerie.mois1 - this.totalDecaissementTresorerie.mois1, this.currentCurrencyMask[':precision']);
      result.mois2 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalEncaissementTresorerie.mois2 - this.totalDecaissementTresorerie.mois2, this.currentCurrencyMask[':precision']);
      result.mois3 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalEncaissementTresorerie.mois3 - this.totalDecaissementTresorerie.mois3, this.currentCurrencyMask[':precision']);
      result.mois4 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalEncaissementTresorerie.mois4 - this.totalDecaissementTresorerie.mois4, this.currentCurrencyMask[':precision']);
      result.mois5 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalEncaissementTresorerie.mois5 - this.totalDecaissementTresorerie.mois5, this.currentCurrencyMask[':precision']);
      result.mois6 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalEncaissementTresorerie.mois6 - this.totalDecaissementTresorerie.mois6, this.currentCurrencyMask[':precision']);
      result.mois7 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalEncaissementTresorerie.mois7 - this.totalDecaissementTresorerie.mois7, this.currentCurrencyMask[':precision']);
      result.mois9 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalEncaissementTresorerie.mois9 - this.totalDecaissementTresorerie.mois9, this.currentCurrencyMask[':precision']);
      result.mois10 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalEncaissementTresorerie.mois10 - this.totalDecaissementTresorerie.mois10, this.currentCurrencyMask[':precision']);
      result.mois11 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalEncaissementTresorerie.mois11 - this.totalDecaissementTresorerie.mois11, this.currentCurrencyMask[':precision']);
      result.mois12 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalEncaissementTresorerie.mois12 - this.totalDecaissementTresorerie.mois12, this.currentCurrencyMask[':precision']);
      result.total = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.totalEncaissementTresorerie.total - this.totalDecaissementTresorerie.total, this.currentCurrencyMask[':precision']);
      return result;
    },
    soldeCumule: function soldeCumule() {
      var result = {
        "mois1": 0,
        "mois2": 0,
        "mois3": 0,
        "mois4": 0,
        "mois5": 0,
        "mois6": 0,
        "mois7": 0,
        "mois8": 0,
        "mois9": 0,
        "mois10": 0,
        "mois11": 0,
        "mois12": 0,
        "total": 0,
        "initial": 0
      };
      result.initial = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.soldeFinMois.initial, this.currentCurrencyMask[':precision']);
      result.mois1 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.soldeFinMois.mois1 + result.initial, this.currentCurrencyMask[':precision']);
      result.mois2 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.soldeFinMois.mois2 + result.mois1, this.currentCurrencyMask[':precision']);
      result.mois3 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.soldeFinMois.mois3 + result.mois2, this.currentCurrencyMask[':precision']);
      result.mois4 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.soldeFinMois.mois4 + result.mois3, this.currentCurrencyMask[':precision']);
      result.mois5 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.soldeFinMois.mois5 + result.mois4, this.currentCurrencyMask[':precision']);
      result.mois6 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.soldeFinMois.mois6 + result.mois5, this.currentCurrencyMask[':precision']);
      result.mois7 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.soldeFinMois.mois7 + result.mois6, this.currentCurrencyMask[':precision']);
      result.mois8 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.soldeFinMois.mois8 + result.mois7, this.currentCurrencyMask[':precision']);
      result.mois9 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.soldeFinMois.mois9 + result.mois8, this.currentCurrencyMask[':precision']);
      result.mois10 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.soldeFinMois.mois10 + result.mois9, this.currentCurrencyMask[':precision']);
      result.mois11 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.soldeFinMois.mois11 + result.mois10, this.currentCurrencyMask[':precision']);
      result.mois12 = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.soldeFinMois.mois12 + result.mois11, this.currentCurrencyMask[':precision']);
      result.total = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.soldeFinMois.total + result.mois12, this.currentCurrencyMask[':precision']);
      return result;
    },
    intervalleTresorerieAnnee1: function intervalleTresorerieAnnee1() {
      var arrayValue = [];

      for (var property in this.soldeFinMois) {
        if (property != 'total') {
          arrayValue.push(this.soldeCumule[property]);
        }

        ;

        if (property != 'total') {
          arrayValue.push(this.soldeFinMois[property]);
        }

        ;
      }

      var minSolde = arrayValue.reduce(function (a, b) {
        return Math.min(a, b);
      });
      var maxSolde = arrayValue.reduce(function (a, b) {
        return Math.max(a, b);
      }); // on cree un nouvel objet de config

      var oldOptions = {};

      for (var _property in this.options) {
        oldOptions[_property] = this.options[_property];
      }

      ;
      oldOptions['yaxis'][0]['max'] = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(1.2 * maxSolde, 0);
      oldOptions['yaxis'][0]['min'] = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(1.2 * minSolde, 0);
      oldOptions['yaxis'][1]['max'] = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(1.2 * maxSolde, 0);
      oldOptions['yaxis'][1]['min'] = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(1.2 * minSolde, 0);
      oldOptions['yaxis'][2]['max'] = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(1.2 * maxSolde, 0);
      oldOptions['yaxis'][2]['min'] = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(1.2 * minSolde, 0);
      this.options = oldOptions;
      return {
        'max': maxSolde,
        'min': minSolde
      };
    },
    intervalleChiffreAffaireAnnee1: function intervalleChiffreAffaireAnnee1() {
      var _this = this;

      var arrayValue = [];

      for (var property in this.totalChiffreAffaire.annee1.volumeVente) {
        if (property != 'total') {
          arrayValue.push(this.totalChiffreAffaire.annee1.volumeVente[property]);
        }

        ;
      }

      var minSolde = arrayValue.reduce(function (a, b) {
        return Math.min(a, b, _this.pointMort);
      });
      var maxSolde = arrayValue.reduce(function (a, b) {
        return Math.max(a, b, _this.pointMort);
      }); // on cree un nouvel objet de config

      var oldOptions = {};

      for (var _property2 in this.chartOptions) {
        oldOptions[_property2] = this.chartOptions[_property2];
      }

      ;
      oldOptions['yaxis'][0]['max'] = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(1.2 * maxSolde, 0);
      oldOptions['yaxis'][0]['min'] = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(1.2 * minSolde, 0);
      oldOptions['yaxis'][1]['max'] = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(1.2 * maxSolde, 0);
      oldOptions['yaxis'][1]['min'] = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(1.2 * minSolde, 0);
      oldOptions['yaxis'][2]['max'] = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(1.2 * maxSolde, 0);
      oldOptions['yaxis'][2]['min'] = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(1.2 * minSolde, 0);
      this.chartOptions = oldOptions;
      return {
        'max': maxSolde,
        'min': minSolde
      };
    },

    /****************************************************************************************************************************
     *  Mise a jour des valeurs du graphe | Series => Seuil de renta, ChartOptions => option seuil de renta,
     *  Serie => Treso, Options => Options treso.
     * **************************************************************************************************************************/
    treso: function treso() {
      var SC_Pos = [this.soldeCumule.initial, this.soldeCumule.mois1, this.soldeCumule.mois2, this.soldeCumule.mois3, this.soldeCumule.mois4, this.soldeCumule.mois5, this.soldeCumule.mois6, this.soldeCumule.mois7, this.soldeCumule.mois8, this.soldeCumule.mois9, this.soldeCumule.mois10, this.soldeCumule.mois11, this.soldeCumule.mois12]; //Solde cumule negatif

      var SC_Neg = [null, null, null, null, null, null, null, null, null, null, null, null, null];

      for (var i = 0; i <= 12; i++) {
        if (SC_Pos[i] < 0) {
          SC_Neg[i] = SC_Pos[i];
          SC_Pos[i] = null;
        }
      }

      this.$set(this.series, 0, {
        name: 'Solde cumule (-)',
        type: 'column',
        data: SC_Neg
      });
      this.$set(this.series, 1, {
        name: 'Solde cumule (+)',
        type: 'column',
        data: SC_Pos
      });
      this.$set(this.series, 2, {
        name: 'Solde fin mois',
        type: 'line',
        data: [this.soldeFinMois.initial, this.soldeFinMois.mois1, this.soldeFinMois.mois2, this.soldeFinMois.mois3, this.soldeFinMois.mois4, this.soldeFinMois.mois5, this.soldeFinMois.mois6, this.soldeFinMois.mois7, this.soldeFinMois.mois8, this.soldeFinMois.mois9, this.soldeFinMois.mois10, this.soldeFinMois.mois11, this.soldeFinMois.mois12]
      });
      return this.series;
    },
    seuil: function seuil() {
      var seuil = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(this.seuilRentabilite.annee1 / 12, this.currentCurrencyMask[':precision']);
      var CA = [this.totalChiffreAffaireHT.annee1.volumeVente["01"], this.totalChiffreAffaireHT.annee1.volumeVente["02"], this.totalChiffreAffaireHT.annee1.volumeVente["03"], this.totalChiffreAffaireHT.annee1.volumeVente["04"], this.totalChiffreAffaireHT.annee1.volumeVente["05"], this.totalChiffreAffaireHT.annee1.volumeVente["06"], this.totalChiffreAffaireHT.annee1.volumeVente["07"], this.totalChiffreAffaireHT.annee1.volumeVente["08"], this.totalChiffreAffaireHT.annee1.volumeVente["09"], this.totalChiffreAffaireHT.annee1.volumeVente["10"], this.totalChiffreAffaireHT.annee1.volumeVente["11"], this.totalChiffreAffaireHT.annee1.volumeVente["12"]]; //Point Mort

      var PM = [null, null, null, null, null, null, null, null, null, null, null, null];
      var annee = "";
      var mois = "";

      for (var i = 0; i <= 35; i++) {
        if (this.pointMort === i + 1) {
          switch (true) {
            case this.pointMort < 13:
              annee = "annee1";
              mois = i + 1 < 10 ? "0" + (i + 1) : (i + 1).toString();
              break;

            case this.pointMort > 12 && this.pointMort < 25:
              annee = "annee2";
              mois = i - 11 < 10 ? "0" + (i - 11) : (i - 11).toString();
              break;

            case this.pointMort > 24 && this.pointMort < 37:
              annee = "annee3";
              mois = i - 23 < 10 ? "0" + (i - 23) : (i - 23).toString();
              break;
          }

          PM[i] = this.totalChiffreAffaireHT[annee].volumeVente[mois];
          CA[i] = null;
        }
      }

      this.$set(this.serie, 0, {
        name: 'Chiffre d\'Affaire',
        type: 'column',
        data: CA
      });
      this.$set(this.serie, 1, {
        name: 'Seuil de rentabilte',
        type: 'line',
        data: [seuil, seuil, seuil, seuil, seuil, seuil, seuil, seuil, seuil, seuil, seuil, seuil]
      });
      this.$set(this.serie, 2, {
        name: 'Point mort',
        type: 'column',
        data: PM
      });
      return this.serie;
    }
  },
  components: {
    sideMenu: _layouts_sidebar_financier__WEBPACK_IMPORTED_MODULE_2__["default"],
    apexchart: vue_apexcharts__WEBPACK_IMPORTED_MODULE_17___default.a,
    VueNumeric: vue_numeric__WEBPACK_IMPORTED_MODULE_9___default.a
  },
  mounted: function mounted() {
    var _this2 = this;

    // le loader en mode plugin
    var loader = vue__WEBPACK_IMPORTED_MODULE_0___default.a.$loading.show(); //Chanegement de la valeur option treso
    // recuperation des elements de calcul
    // console.log(JSON.parse(localStorage.getItem('userSession')).projet);

    firebase__WEBPACK_IMPORTED_MODULE_1___default.a.firestore().collection('financier').doc(this.projet).get().then(function (docSnap) {
      if (docSnap.exists) {
        // tva parametree du projet
        if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('parametres'))) {
          if (Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('parametres')['tva'])) {
            Toast.fire({
              type: 'warning',
              title: 'Votre Projet n\'a pas de Tva configurée, nous utiliserons un parametre par défaut',
              customClass: "bg-saphire"
            });
          } else {
            if (docSnap.get('parametres')['tva'].length > 1) {
              _this2.paramTva = Object(util__WEBPACK_IMPORTED_MODULE_16__["isNullOrUndefined"])(docSnap.get('parametres')['tva'].find(function (item) {
                return item.alias === 'normale';
              })) ? docSnap.get('parametres')['tva'][0] : docSnap.get('parametres')['tva'].find(function (item) {
                return item.alias === 'normale';
              });
            } else if (docSnap.get('parametres')['tva'].length = 1) {
              _this2.paramTva = docSnap.get('parametres')['tva'][0];
            }
          }

          ;

          if (!Object(util__WEBPACK_IMPORTED_MODULE_16__["isNullOrUndefined"])(docSnap.data().parametres.chargePatronale)) {
            _this2.chargePatronale = docSnap.data().parametres.chargePatronale;
          }

          _this2.chargeSocialeDirigeant = _this2.chargePatronale.dirigeant;
          _this2.chargeSocialeEmploye = _this2.chargePatronale.employe;
        } else {
          if (docSnap.get('parametres')['tva'].length > 1) {
            _this2.paramTva = Object(util__WEBPACK_IMPORTED_MODULE_16__["isNullOrUndefined"])(docSnap.get('parametres')['tva'].find(function (item) {
              return item.alias === 'normale';
            })) ? docSnap.get('parametres')['tva'][0] : docSnap.get('parametres')['tva'].find(function (item) {
              return item.alias === 'normale';
            });
          } else if (docSnap.get('parametres')['tva'].length = 1) {
            _this2.paramTva = docSnap.get('parametres')['tva'][0];
          }
        } // definition HT pour les besoins


        if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('besoins'))) {
          // immobilisations incorporelles
          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('besoins')['immoIncorporelle'])) {
            _this2.besoins['immoIncorporelle'] = docSnap.get('besoins')['immoIncorporelle'].map(function (item) {
              return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, 'prix', _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
            });

            _this2.calculTotalImmoIncorporelle(_this2.besoins['immoIncorporelle']);
          } // immo corporelle


          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('besoins')['immoCorporelle'])) {
            // materiel informatique
            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('besoins')['immoCorporelle']['materielinfo'])) {
              _this2.besoins['immoCorporelle']['materielinfo'] = docSnap.get('besoins')['immoCorporelle']['materielinfo'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, 'prix', _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });

              _this2.calculSubTotalImmoCorpMatInfo(_this2.besoins['immoCorporelle']['materielinfo']);

              _this2.calculTotalAmortisAnnuelImmoCorpMatInfo(_this2.besoins['immoCorporelle']['materielinfo']);
            } // mobilier


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('besoins')['immoCorporelle']['mobilier'])) {
              _this2.besoins['immoCorporelle']['mobilier'] = docSnap.get('besoins')['immoCorporelle']['mobilier'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, 'prix', _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });

              _this2.calculSubTotalImmoCorpMobilier(_this2.besoins['immoCorporelle']['mobilier']);

              _this2.calculTotalAmortisAnnuelImmoCorpMobilier(_this2.besoins['immoCorporelle']['mobilier']);
            } // materiel-machine-outils


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('besoins')['immoCorporelle']['materiel-machine-outil'])) {
              _this2.besoins['immoCorporelle']['materiel-machine-outil'] = docSnap.get('besoins')['immoCorporelle']['materiel-machine-outil'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, 'prix', _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });

              _this2.calculSubTotalImmoCorpMatMachOutil(_this2.besoins['immoCorporelle']['materiel-machine-outil']);

              _this2.calculTotalAmortisAnnuelImmoCorpMatMachOutil(_this2.besoins['immoCorporelle']['materiel-machine-outil']);
            } // batiment-local-espacevente


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('besoins')['immoCorporelle']['batiment-local-espacevente'])) {
              _this2.besoins['immoCorporelle']['batiment-local-espacevente'] = docSnap.get('besoins')['immoCorporelle']['batiment-local-espacevente'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, 'prix', _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });

              _this2.calculSubTotalImmoCorpBatLocEspaceVente(_this2.besoins['immoCorporelle']['batiment-local-espacevente']);

              _this2.calculTotalAmortisAnnuelImmoCorpBatLocEspaceVente(_this2.besoins['immoCorporelle']['batiment-local-espacevente']);
            } // voiture-autres


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('besoins')['immoCorporelle']['voiture-autres'])) {
              _this2.besoins['immoCorporelle']['voiture-autres'] = docSnap.get('besoins')['immoCorporelle']['voiture-autres'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, 'prix', _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });

              _this2.calculSubTotalImmoCorpVoitAutre(_this2.besoins['immoCorporelle']['voiture-autres']);

              _this2.calculTotalAmortisAnnuelImmoCorpVoitAutre(_this2.besoins['immoCorporelle']['voiture-autres']);
            }
          } // bfr


          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('besoins')['bfr'])) {
            // bfr ouverture
            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('besoins')['bfr']['bfrOuverture'])) {
              _this2.besoins['bfr']['bfrOuverture'] = docSnap.get('besoins')['bfr']['bfrOuverture'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, 'coutMensuel', _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });

              _this2.calculTotalBfrOuverture(_this2.besoins.bfr['bfrOuverture']);
            } // stocks


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('besoins')['bfr']['stock'])) {
              _this2.besoins['bfr']['stock'] = docSnap.get('besoins')['bfr']['stock'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, ['annee1', 'annee2', 'annee3'], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });

              _this2.calculSubTotalBfrStockAnnee1(_this2.besoins.bfr['stock']);

              _this2.calculSubTotalBfrStockAnnee2(_this2.besoins.bfr['stock']);

              _this2.calculSubTotalBfrStockAnnee3(_this2.besoins.bfr['stock']);
            } // creance client


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('besoins')['bfr']['creanceClient'])) {
              _this2.besoins['bfr']['creanceClient'] = docSnap.get('besoins')['bfr']['creanceClient'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, ['annee1', 'annee2', 'annee3'], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });

              _this2.calculSubTotalBfrCreanceCliAnnee1(_this2.besoins.bfr['creanceClient']);

              _this2.calculSubTotalBfrCreanceCliAnnee2(_this2.besoins.bfr['creanceClient']);

              _this2.calculSubTotalBfrCreanceCliAnnee3(_this2.besoins.bfr['creanceClient']);
            } // dettes


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('besoins')['bfr']['dettes'])) {
              _this2.besoins['bfr']['dettes'] = docSnap.get('besoins')['bfr']['dettes'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, ['annee1', 'annee2', 'annee3'], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });

              _this2.calculSubTotalBfrDetFourAnnee1(_this2.besoins.bfr['dettes']);

              _this2.calculSubTotalBfrDetFourAnnee2(_this2.besoins.bfr['dettes']);

              _this2.calculSubTotalBfrDetFourAnnee3(_this2.besoins.bfr['dettes']);
            }
          } // immo financiere


          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('besoins')['immoFinanciere'])) {
            // cautions
            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('besoins')['immoFinanciere']['cautions'])) {
              _this2.besoins['immoFinanciere']['cautions'] = docSnap.get('besoins')['immoFinanciere']['cautions'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, 'montant', _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });

              _this2.calculSubTotalImmoFinCaution(_this2.besoins['immoFinanciere']['cautions']);
            } // depots de garantie


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('besoins')['immoFinanciere']['depots'])) {
              _this2.besoins['immoFinanciere']['depots'] = docSnap.get('besoins')['immoFinanciere']['depots'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, 'montant', _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });

              _this2.calculSubTotalImmoFinDepGarantie(_this2.besoins['immoFinanciere']['depots']);
            } // autres


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('besoins')['immoFinanciere']['autres'])) {
              _this2.besoins['immoFinanciere']['autres'] = docSnap.get('besoins')['immoFinanciere']['autres'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, 'montant', _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });

              _this2.calculSubTotalImmoFinAutre(_this2.besoins['immoFinanciere']['autres']);
            }
          }
        } // definition HT pour les ressources


        if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('ressources'))) {
          // capital
          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('ressources')['capital'])) {
            // numeraire
            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('ressources')['capital']['numeraire'])) {
              _this2.ressources['capital']['numeraire'] = docSnap.get('ressources')['capital']['numeraire'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, "montant", _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });

              _this2.calculSubTotalCapAppNumeraire(_this2.ressources.capital['numeraire']);
            } // nature


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('ressources')['capital']['nature'])) {
              _this2.ressources['capital']['nature'] = docSnap.get('ressources')['capital']['nature'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, "montant", _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });

              _this2.calculSubTotalCapAppNature(_this2.ressources.capital['nature']);
            }
          } // comptes courants associes


          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('ressources')['comptes'])) {
            _this2.ressources['comptes'] = docSnap.get('ressources')['comptes'].map(function (item) {
              return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, "montant", _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
            });

            _this2.calculTotalCompteAssocie(_this2.ressources.comptes);
          } // emprunts


          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('ressources')['emprunts'])) {
            // banque
            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('ressources')['emprunts']['banque'])) {
              _this2.ressources['emprunts']['banque'] = docSnap.get('ressources')['emprunts']['banque'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, ["montantPret", "montantRemboursement"], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });

              _this2.calculSubTotalEmpruntBanque(_this2.ressources.emprunts['banque']);

              _this2.calculTotalRemboursAnnuelEmpruntBanque(_this2.ressources.emprunts['banque']);
            } // microfinance


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('ressources')['emprunts']['microfinance'])) {
              _this2.ressources['emprunts']['microfinance'] = docSnap.get('ressources')['emprunts']['microfinance'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, ["montantPret", "montantRemboursement"], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });

              _this2.calculSubTotalEmpruntMicroFin(_this2.ressources.emprunts['microfinance']);

              _this2.calculTotalRemboursAnnuelEmpruntMicroFin(_this2.ressources.emprunts['microfinance']);
            }
          } // subventions-aides


          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('ressources')['subventions-aides'])) {
            // banque
            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('ressources')['subventions-aides']['banque'])) {
              _this2.ressources['subventions-aides']['banque'] = docSnap.get('ressources')['subventions-aides']['banque'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, ["montantPret", "montantRemboursement"], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });

              _this2.calculSubTotalAidBanque(_this2.ressources['subventions-aides'].banque);

              _this2.calculTotalRemboursAnnuelAidBanque(_this2.ressources['subventions-aides'].banque);
            } // institution


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('ressources')['subventions-aides']['institution'])) {
              _this2.ressources['subventions-aides']['institution'] = docSnap.get('ressources')['subventions-aides']['institution'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, ["montantPret", "montantRemboursement"], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });

              _this2.calculSubTotalAidInstitution(_this2.ressources['subventions-aides'].institution);

              _this2.calculTotalRemboursAnnuelAidInstitution(_this2.ressources['subventions-aides'].institution);
            } // autres


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('ressources')['subventions-aides']['autres'])) {
              _this2.ressources['subventions-aides']['autres'] = docSnap.get('ressources')['subventions-aides']['autres'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, ["montantPret", "montantRemboursement"], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });

              _this2.calculSubTotalAidAutre(_this2.ressources['subventions-aides'].autres);

              _this2.calculTotalRemboursAnnuelAidAutre(_this2.ressources['subventions-aides'].autres);
            }
          }
        } // definition HT pour les produits


        if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('produits'))) {
          // calcul du chiffre d'affaires articles
          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('produits')['articles'])) {
            _this2.produits.articles = docSnap.get('produits')['articles'];

            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(_this2.produits.articles)) {
              // this.calculTotalChiffreAffaireAnnee1(this.produits.articles);
              _this2.calculTotalChiffreAffaireAnnee1(_this2.produits.articles);

              _this2.calculTotalChiffreAffaireAnnee2(_this2.produits.articles);

              _this2.calculTotalChiffreAffaireAnnee3(_this2.produits.articles);
            }
          } // volume de vente


          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('produits')['volumeVente'])) {
            _this2.produits['volumeVente'] = docSnap.get('produits')['volumeVente'];
          } // resultats exceptionnels


          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('produits')['resultatsExceptionnels'])) {
            // entrees
            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('produits')['resultatsExceptionnels']['entrees'])) {
              _this2.produits['resultatsExceptionnels']['entrees'] = docSnap.get('produits')['resultatsExceptionnels']['entrees'];

              _this2.calculSubTotalVentesExceptionnelles(_this2.produits['resultatsExceptionnels']['entrees']);
            } // sorties


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('produits')['resultatsExceptionnels']['sorties'])) {
              _this2.produits['resultatsExceptionnels']['sorties'] = docSnap.get('produits')['resultatsExceptionnels']['sorties'];

              _this2.calculSubTotalAchatsExceptionnels(_this2.produits['resultatsExceptionnels']['sorties']);
            }
          }
        }
        /**
        ** Calcul du remboursement emprunt
        **/
        // console.log(this.totalAchatMpDepVariable.annee1);
        // definition HT pour les charges


        if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('charges'))) {
          // services exterieurs
          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('charges')['services-exterieurs'])) {
            _this2.charges['services-exterieurs'] = docSnap.get('charges')['services-exterieurs'].map(function (item) {
              return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, ['annee1', 'annee2', 'annee3', 'prix'], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
            });

            _this2.calculTotalServExtFrais(_this2.charges['services-exterieurs']);
          } // frais de personnel


          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('charges')['frais-personnel'])) {
            // stagiaires
            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('charges')['frais-personnel']['stagiaires'])) {
              _this2.charges['frais-personnel']['stagiaires'] = docSnap.get('charges')['frais-personnel']['stagiaires'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, ['annee1', 'annee2', 'annee3', 'salaire'], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });

              _this2.calculSubTotalFraisStagiaire(_this2.charges['frais-personnel']['stagiaires']);
            } // salaries


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('charges')['frais-personnel']['salaries'])) {
              _this2.charges['frais-personnel']['salaries'] = docSnap.get('charges')['frais-personnel']['salaries'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, ['annee1', 'annee2', 'annee3', 'salaire'], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });

              _this2.calculSubTotalFraisSalarie(_this2.charges['frais-personnel']['salaries']);
            } // dirigeants


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('charges')['frais-personnel']['dirigeants'])) {
              _this2.charges['frais-personnel']['dirigeants'] = docSnap.get('charges')['frais-personnel']['dirigeants'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, ['annee1', 'annee2', 'annee3', 'salaire'], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });

              _this2.calculSubTotalFraisDirigeant(_this2.charges['frais-personnel']['dirigeants']);
            }
          } // taxes et impots


          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('charges')['taxes-impots'])) {
            // impots
            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('charges')['taxes-impots']['impots'])) {
              _this2.charges['taxes-impots']['impots'] = docSnap.get('charges')['taxes-impots']['impots'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, ['annee1', 'annee2', 'annee3', 'prix'], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });

              _this2.calculSubTotalTaxImpImpot(_this2.charges['taxes-impots'].impots);
            } // mairie


            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('charges')['taxes-impots']['mairie'])) {
              _this2.charges['taxes-impots']['mairie'] = docSnap.get('charges')['taxes-impots']['mairie'].map(function (item) {
                return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, ['annee1', 'annee2', 'annee3', 'prix'], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
              });

              _this2.calculSubTotalTaxeImpMairie(_this2.charges['taxes-impots'].mairie);
            }
          } // depenses produits


          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('charges')['matieres-premieres']['depenses-produits'])) {
            _this2.charges["matieres-premieres"]["depenses-produits"] = docSnap.get('charges')['matieres-premieres']['depenses-produits'].map(function (item) {
              return Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["convertFieldToHT"])(item, ['annee1', 'annee2', 'annee3', 'prix'], _this2.paramTva.value / 100, _this2.currentCurrencyMask[':precision']);
            });

            if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(_this2.charges["matieres-premieres"]["depenses-produits"])) {
              _this2.calculTotalAchatMpDepVariable(_this2.charges["matieres-premieres"]["depenses-produits"]);
            }
          } // charges variables


          if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["isEmpty"])(docSnap.get('charges')['matieres-premieres']['charges-variables-produits'])) {
            _this2.charges['matieres-premieres']['charges-variables-produits'] = docSnap.get('charges')['matieres-premieres']['charges-variables-produits'];

            _this2.calculTotalAchatMpCharVariable(_this2.charges['matieres-premieres']['charges-variables-produits']);
          }
        }
      }

      loader.hide();
    })["catch"](function (error) {
      loader.hide();

      switch (error.code) {
        case 'unavailable':
          Swal.fire('erreur ', '<span class="text-danger text-bold">Pas de connexion internet</span>', 'error');
          break;

        /*
        default:
            Swal.fire(
                'Pas',
                error.message,
                'error'
            );
            break;
            */
      }

      ;
    }).then(function () {
      //PUSH BESOINS
      _this2.donutDatabesoins.push({
        label: 'Immo incorporelles',
        value: _this2.totalImmoIncorporelle
      });

      _this2.donutDatabesoins.push({
        label: 'Immo corporelles',
        value: _this2.totalImmoCorporelle.prix
      });

      _this2.donutDatabesoins.push({
        label: 'Immo Financière',
        value: _this2.totalImmoFinanciere
      });

      _this2.donutDatabesoins.push({
        label: 'Bfr',
        value: _this2.totalBfrOuverture
      });

      _this2.donutDatabesoins.push({
        label: 'Remboursement emprunts',
        value: 0
      }); //PUSH RESSOURCES


      var sumcapital = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(_this2.subTotalCapAppNumeraire + _this2.subTotalCapAppNature, _this2.currentCurrencyMask[':precision']);

      _this2.donutDataressources.push({
        label: 'Capital',
        value: sumcapital
      });

      _this2.donutDataressources.push({
        label: 'Comptes courant associes',
        value: _this2.totalCompteAssocie
      });

      var emprunt = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(_this2.subTotalEmpruntBanque.montant + _this2.subTotalEmpruntMicroFin.montant, _this2.currentCurrencyMask[':precision']);

      _this2.donutDataressources.push({
        label: 'Emprunts',
        value: emprunt
      });

      var subvention = Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(_this2.subTotalAidBanque.montant + _this2.subTotalAidInstitution.montant, _this2.currentCurrencyMask[':precision']);

      _this2.donutDataressources.push({
        label: 'Subventions',
        value: subvention
      }); //CHIFFRE D'AFFAIRES


      _this2.donutDataca.push({
        label: 'CA',
        value: Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(_this2.totalChiffreAffaireHT.annee1.montant, _this2.currentCurrencyMask[':precision'])
      });

      _this2.listeCA = [_this2.donutDataca[0].value]; //CA-CV

      _this2.donutDatacacv.push({
        label: 'CA - CV',
        value: Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(_this2.totalChiffreAffaireHT.annee1.montant - (_this2.totalAchatMpDepVariable.annee1 + _this2.totalAchatMpCharVariable.annee1), _this2.currentCurrencyMask[':precision'])
      });

      _this2.donutDatacacv.push({
        label: 'CV',
        value: Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(_this2.totalAchatMpDepVariable.annee1 + _this2.totalAchatMpCharVariable.annee1, _this2.currentCurrencyMask[':precision'])
      });

      _this2.listeCACV = [_this2.donutDatacacv[0].value, _this2.donutDatacacv[1].value]; //CA-(CV+CF)

      _this2.donutDatacacvcf.push({
        label: 'CF',
        value: Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(_this2.chargesFixes.annee1 + _this2.totalAchatMpDepVariable.annee1 + _this2.totalAchatMpCharVariable.annee1, _this2.currentCurrencyMask[':precision'])
      });

      _this2.donutDatacacvcf.push({
        label: 'CA - (CF + CV)',
        value: Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["arround"])(_this2.totalChiffreAffaireHT.annee1.montant - (_this2.chargesFixes.annee1 + _this2.totalAchatMpDepVariable.annee1 + _this2.totalAchatMpCharVariable.annee1), _this2.currentCurrencyMask[':precision'])
      });

      if (Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["checkValue"])(_this2.donutDatacacvcf)) {
        _this2.listeCVCACF = [_this2.donutDatacacvcf[0].value, _this2.donutDatacacvcf[1].value];
      } else {
        _this2.listeCVCACF = 0;
      }

      if (Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["checkValue"])(_this2.donutDatabesoins)) {
        _this2.listeBesoins = [_this2.donutDatabesoins[0].value, _this2.donutDatabesoins[1].value, _this2.donutDatabesoins[2].value, _this2.donutDatabesoins[3].value, _this2.donutDatabesoins[4].value];
      } else {
        _this2.listeBesoins = 0;
      }

      if (Object(_services_helper__WEBPACK_IMPORTED_MODULE_14__["checkValue"])(_this2.donutDataressources)) {
        _this2.listeRessources = [_this2.donutDataressources[0].value, _this2.donutDataressources[1].value, _this2.donutDataressources[2].value, _this2.donutDataressources[3].value];
      } else {
        _this2.listeRessources = 0;
      }
    });
    $('.collapse').collapse();
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/layouts/sidebar-financier.vue?vue&type=style&index=0&id=2ae30a12&scoped=true&lang=css&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/layouts/sidebar-financier.vue?vue&type=style&index=0&id=2ae30a12&scoped=true&lang=css& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports
exports.i(__webpack_require__(/*! -!../../../../node_modules/css-loader??ref--7-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../sass/verticalsidemenu.scss */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./resources/assets/sass/verticalsidemenu.scss"), "");

// module
exports.push([module.i, "\na[data-v-2ae30a12]{\n    color: white;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/financier/dashboard.vue?vue&type=style&index=0&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/bp/financier/dashboard.vue?vue&type=style&index=0&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.strikethrough{\n    text-decoration: line-through;\n}\n.over {\n    position: relative;\n    width: 100%;\n}\n.textred{\n    color:red;\n}\n.textgreen{\n    color:#06d79c;\n}\n.textblue{\n    color:#5343dd;\n}\n.overlay {\n    position: absolute;\n    bottom: 0;\n    background: rgb(0, 0, 0);\n    background: rgba(0, 0, 0, 0.5); /* Black see-through */\n    color: #f1f1f1;\n    width: 95%;\n    transition: .5s ease;\n    opacity:0;\n    color: white;\n    font-size: 40px;\n    font-weight: bold;\n    padding: 50px;\n    margin-right:50px;\n    text-align: center;\n}\n.over1:hover .overlay {\n    opacity: 0.8;\n}\n.over2:hover .overlay {\n    opacity: 0.8;\n}\n.over3:hover .overlay {\n    opacity: 0.8;\n}\n.over4:hover .overlay {\n    opacity: 0.8;\n}\n.immoincor{\n    color:#1a0c72;\n}\n.immocor{\n    color:#06d79c;\n}\n.immofin{\n    color:#36A2EB;\n}\n.emprunt{\n    color:#FF0000;\n}\n.bfr{\n    color:#FFCE56;\n}\n\n/* ressources donut*/\n.appcapital{\n    color:#1a0c72;\n}\n.ccassoc{\n    color:#06d79c;\n}\n.empbanc{\n    color:#36A2EB;\n}\n.subvention{\n    color:#FFCE56;\n}\n.chart {\n    width: 100%;\n    height: 100%;\n}\n\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./resources/assets/sass/verticalsidemenu.scss":
/*!**************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./resources/assets/sass/verticalsidemenu.scss ***!
  \**************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n#sidebarnav > li.dropdowm {\r\n    display:inline-block;\n}\n#sidebarnav li.dropdowm:hover {\r\n    color: white;\r\n    background-color: lightblue;\n}\n#sidebarnav li.dropdowm {\r\n    margin-right:15px; \r\n    font-style: bolder;\r\n    background-color: #80808020; \r\n    color: #ffffff80;\n}\n.sidebar-nav > ul > li.active > a {\r\n    color: #ffffff !important;\r\n    border-color: #61b174;\r\n    background-color: #61b174 !important;\n}\n.titre {\r\n    font-size: 16px;\r\n    font-weight: bolder;\n}\n.icon-white {\r\n    color: #ffffff !important;\n}\n.sidebar-nav .has-arrow::after {\r\n    border-color: white;\n}\n.sidebar-nav .active > .has-arrow::after, .sidebar-nav li > .has-arrow.active::after, .sidebar-nav .has-arrow[aria-expanded=true]::after {\r\n    border-color: white;\n}\n.sidebar-nav > ul.collapse.in {\r\n    background-color: #3f4f5d;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/layouts/sidebar-financier.vue?vue&type=style&index=0&id=2ae30a12&scoped=true&lang=css&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/layouts/sidebar-financier.vue?vue&type=style&index=0&id=2ae30a12&scoped=true&lang=css& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--7-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./sidebar-financier.vue?vue&type=style&index=0&id=2ae30a12&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/layouts/sidebar-financier.vue?vue&type=style&index=0&id=2ae30a12&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/financier/dashboard.vue?vue&type=style&index=0&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/bp/financier/dashboard.vue?vue&type=style&index=0&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader??ref--7-1!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./dashboard.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/financier/dashboard.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/layouts/sidebar-financier.vue?vue&type=template&id=2ae30a12&scoped=true&":
/*!************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/layouts/sidebar-financier.vue?vue&type=template&id=2ae30a12&scoped=true& ***!
  \************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "aside",
    { staticClass: "left-sidebar" },
    [
      _c(
        "el-menu",
        {
          staticClass: "el-menu-vertical-demo",
          staticStyle: {
            height: "100%",
            width: "300px",
            position: "fixed",
            "z-index": "20",
            "padding-top": "80px",
            "overflow-x": "hidden",
            "overflow-y": "auto"
          },
          attrs: {
            "default-active": "2",
            "background-color": "#242a33",
            "text-color": "#fff",
            "active-text-color": "#ffd04b"
          }
        },
        [
          _vm._l(_vm.menuLevel1, function(item, index) {
            return _c(
              "router-link",
              { key: index, attrs: { to: { name: item.route } } },
              [
                _c("el-menu-item", { attrs: { index: index } }, [
                  _c("i", { class: item.icon }),
                  _vm._v(" "),
                  _c("span", [_vm._v(_vm._s(item.libelle))])
                ])
              ],
              1
            )
          }),
          _vm._v(" "),
          _vm._l(_vm.menuLevel2, function(item, index) {
            return _c(
              "el-submenu",
              { key: index, attrs: { index: index } },
              [
                _c("template", { slot: "title" }, [
                  _c("i", { class: item.icon }),
                  _vm._v(" "),
                  _c("span", [_vm._v(_vm._s(item.libelle))])
                ]),
                _vm._v(" "),
                _vm._l(item.content, function(itemlevel2, indexlevel2) {
                  return _c(
                    "router-link",
                    {
                      key: indexlevel2,
                      attrs: { to: { name: itemlevel2.route } }
                    },
                    [
                      itemlevel2.checked
                        ? _c(
                            "el-menu-item",
                            { attrs: { index: index + "-" + indexlevel2 } },
                            [_vm._v(_vm._s(itemlevel2.libelle))]
                          )
                        : _vm._e()
                    ],
                    1
                  )
                })
              ],
              2
            )
          })
        ],
        2
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/financier/dashboard.vue?vue&type=template&id=4e44c142&":
/*!***************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/bp/financier/dashboard.vue?vue&type=template&id=4e44c142& ***!
  \***************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "page-wrapper" }, [
    _c("div", { staticClass: "container-fluid" }, [
      _vm._m(0),
      _vm._v(" "),
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-lg-6" }, [
          _c("div", { staticClass: "card" }, [
            _c("div", { staticClass: "card-body" }, [
              _c("div", { staticClass: "d-flex no-block" }, [
                _vm._m(1),
                _vm._v(" "),
                _c("div", { staticClass: "align-self-center" }, [
                  _vm._m(2),
                  _vm._v(" "),
                  _c("h4", [
                    _vm._v("Besoins: "),
                    _c(
                      "b",
                      { staticClass: "textred" },
                      [
                        _c("vue-numeric", {
                          attrs: {
                            currency: _vm.currentCurrencyMask["currency"],
                            "currency-symbol-position":
                              _vm.currentCurrencyMask[
                                "currency-symbol-position"
                              ],
                            "output-type": "String",
                            "empty-value": "0",
                            precision: _vm.currentCurrencyMask["precision"],
                            "decimal-separator":
                              _vm.currentCurrencyMask["decimal-separator"],
                            "thousand-separator":
                              _vm.currentCurrencyMask["thousand-separator"],
                            "read-only-class": "item-total",
                            min: _vm.currentCurrencyMask.min,
                            "read-only": ""
                          },
                          model: {
                            value: _vm.totalBesoins.initial,
                            callback: function($$v) {
                              _vm.$set(_vm.totalBesoins, "initial", _vm._n($$v))
                            },
                            expression: "totalBesoins.initial"
                          }
                        })
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("h4", [
                    _vm._v("Ressources: "),
                    _c(
                      "b",
                      { staticClass: "textblue" },
                      [
                        _c("vue-numeric", {
                          attrs: {
                            currency: _vm.currentCurrencyMask["currency"],
                            "currency-symbol-position":
                              _vm.currentCurrencyMask[
                                "currency-symbol-position"
                              ],
                            "output-type": "String",
                            "empty-value": "0",
                            precision: _vm.currentCurrencyMask["precision"],
                            "decimal-separator":
                              _vm.currentCurrencyMask["decimal-separator"],
                            "thousand-separator":
                              _vm.currentCurrencyMask["thousand-separator"],
                            "read-only-class": "item-total",
                            min: _vm.currentCurrencyMask.min,
                            "read-only": ""
                          },
                          model: {
                            value: _vm.totalRessources.initial,
                            callback: function($$v) {
                              _vm.$set(
                                _vm.totalRessources,
                                "initial",
                                _vm._n($$v)
                              )
                            },
                            expression: "totalRessources.initial"
                          }
                        })
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("h4", [
                    _vm._v("Solde: "),
                    _c(
                      "b",
                      {
                        class:
                          _vm.totalRessources.initial -
                            _vm.totalBesoins.initial >
                          0
                            ? "textgreen"
                            : "textred"
                      },
                      [
                        _c("vue-numeric", {
                          attrs: {
                            currency: _vm.currentCurrencyMask["currency"],
                            "currency-symbol-position":
                              _vm.currentCurrencyMask[
                                "currency-symbol-position"
                              ],
                            "output-type": "String",
                            "empty-value": "0",
                            precision: _vm.currentCurrencyMask["precision"],
                            "decimal-separator":
                              _vm.currentCurrencyMask["decimal-separator"],
                            "thousand-separator":
                              _vm.currentCurrencyMask["thousand-separator"],
                            "read-only-class": "item-total",
                            min: _vm.currentCurrencyMask.min,
                            "read-only": "",
                            value:
                              _vm.totalRessources.initial -
                              _vm.totalBesoins.initial
                          }
                        })
                      ],
                      1
                    )
                  ])
                ])
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-lg-6" }, [
          _c("div", { staticClass: "card" }, [
            _c("div", { staticClass: "card-body" }, [
              _c("div", { staticClass: "d-flex no-block" }, [
                _vm._m(3),
                _vm._v(" "),
                _c("div", { staticClass: "align-self-center" }, [
                  _vm._m(4),
                  _vm._v(" "),
                  _c("h4", [
                    _vm._v("Chiffre d'Affaires: "),
                    _c(
                      "b",
                      { staticClass: "textblue" },
                      [
                        _c("vue-numeric", {
                          attrs: {
                            currency: _vm.currentCurrencyMask["currency"],
                            "currency-symbol-position":
                              _vm.currentCurrencyMask[
                                "currency-symbol-position"
                              ],
                            "output-type": "String",
                            "empty-value": "0",
                            precision: _vm.currentCurrencyMask["precision"],
                            "decimal-separator":
                              _vm.currentCurrencyMask["decimal-separator"],
                            "thousand-separator":
                              _vm.currentCurrencyMask["thousand-separator"],
                            "read-only-class": "item-total",
                            min: _vm.currentCurrencyMask.min,
                            "read-only": "",
                            value: this.totalChiffreAffaireHT.annee1.montant
                          }
                        })
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("h4", [
                    _vm._v("Charges: "),
                    _c(
                      "b",
                      { staticClass: "textred" },
                      [
                        _c("vue-numeric", {
                          attrs: {
                            currency: _vm.currentCurrencyMask["currency"],
                            "currency-symbol-position":
                              _vm.currentCurrencyMask[
                                "currency-symbol-position"
                              ],
                            "output-type": "String",
                            "empty-value": "0",
                            precision: _vm.currentCurrencyMask["precision"],
                            "decimal-separator":
                              _vm.currentCurrencyMask["decimal-separator"],
                            "thousand-separator":
                              _vm.currentCurrencyMask["thousand-separator"],
                            "read-only-class": "item-total",
                            min: _vm.currentCurrencyMask.min,
                            "read-only": "",
                            value: this.totalCharge
                          }
                        })
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("h4", [
                    _vm._v("Bénéfice: "),
                    _c(
                      "b",
                      {
                        class:
                          this.totalChiffreAffaireHT.annee1.montant -
                            this.totalCharge >
                          0
                            ? "textgreen"
                            : "textred"
                      },
                      [
                        _c("vue-numeric", {
                          attrs: {
                            currency: _vm.currentCurrencyMask["currency"],
                            "currency-symbol-position":
                              _vm.currentCurrencyMask[
                                "currency-symbol-position"
                              ],
                            "output-type": "String",
                            "empty-value": "0",
                            precision: _vm.currentCurrencyMask["precision"],
                            "decimal-separator":
                              _vm.currentCurrencyMask["decimal-separator"],
                            "thousand-separator":
                              _vm.currentCurrencyMask["thousand-separator"],
                            "read-only-class": "item-total",
                            min: _vm.currentCurrencyMask.min,
                            "read-only": "",
                            value:
                              this.totalChiffreAffaireHT.annee1.montant -
                              this.totalCharge
                          }
                        })
                      ],
                      1
                    )
                  ])
                ])
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-lg-12" }, [
          _c("div", { staticClass: "card" }, [
            _c("div", { staticClass: "card-body" }, [
              _c("div", { staticClass: "row" }, [
                _c(
                  "div",
                  { staticClass: "col-md-6" },
                  [
                    _vm._m(5),
                    _vm._v(" "),
                    _c("apexchart", {
                      attrs: {
                        type: "donut",
                        width: "480",
                        options: _vm.grapheOptions,
                        series: _vm.listeBesoins
                      }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "col-md-6" },
                  [
                    _vm._m(6),
                    _vm._v(" "),
                    _c("apexchart", {
                      attrs: {
                        type: "donut",
                        width: "450",
                        options: _vm.grapheRessourceOptions,
                        series: _vm.listeRessources
                      }
                    })
                  ],
                  1
                )
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-12" }, [
          _c("div", { staticClass: "card" }, [
            _c("div", { staticClass: "card-body" }, [
              _c("div", { staticClass: "row" }, [
                _c(
                  "div",
                  { staticClass: "col-md-4" },
                  [
                    _vm._m(7),
                    _vm._v(" "),
                    _c("apexchart", {
                      attrs: {
                        type: "donut",
                        width: "360",
                        options: _vm.grapheCAOptions,
                        series: _vm.listeCA
                      }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "col-md-4" },
                  [
                    _vm._m(8),
                    _vm._v(" "),
                    _c("apexchart", {
                      attrs: {
                        type: "donut",
                        width: "360",
                        options: _vm.grapheCACVOptions,
                        series: _vm.listeCACV
                      }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "col-md-4" },
                  [
                    _vm._m(9),
                    _vm._v(" "),
                    _c("apexchart", {
                      attrs: {
                        type: "donut",
                        width: "360",
                        options: _vm.grapheCVCACFOptions,
                        series: _vm.listeCVCACF
                      }
                    })
                  ],
                  1
                )
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-lg-12" }, [
          _c("div", { staticClass: "card" }, [
            _c("div", { staticClass: "card-body" }, [
              _c("div", { staticClass: "container" }, [
                _vm._m(10),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "row",
                    staticStyle: { height: "30%!important" }
                  },
                  [
                    _c(
                      "div",
                      { staticStyle: { height: "20%!important" } },
                      [
                        _c("apexchart", {
                          attrs: {
                            type: "line",
                            width: "100%",
                            height: "350",
                            options: _vm.chartOptions,
                            series: _vm.seuil
                          }
                        })
                      ],
                      1
                    )
                  ]
                )
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-lg-12" }, [
          _c("div", { staticClass: "card" }, [
            _c("div", { staticClass: "card-body" }, [
              _c("div", { staticClass: "container" }, [
                _vm._m(11),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "row",
                    staticStyle: { height: "30%!important" }
                  },
                  [
                    _c(
                      "div",
                      { staticStyle: { height: "20%!important" } },
                      [
                        _c("apexchart", {
                          attrs: {
                            type: "line",
                            width: "100%",
                            height: "350",
                            options: _vm.options,
                            series: _vm.treso
                          }
                        })
                      ],
                      1
                    )
                  ]
                )
              ])
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row page-titles" }, [
      _c("div", { staticClass: "col-md-5 align-self-center" }, [
        _c("h3", { staticClass: "text-themecolor" }, [
          _vm._v("Tableau de bord")
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-7 align-self-center" }, [
        _c("ol", { staticClass: "breadcrumb" }, [
          _c("li", { staticClass: "breadcrumb-item" }, [
            _c("a", { attrs: { href: "javascript:void(0)" } }, [
              _vm._v("Accueil")
            ])
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "breadcrumb-item" }, [_vm._v("bpfinancier")]),
          _vm._v(" "),
          _c("li", { staticClass: "breadcrumb-item active" }, [
            _vm._v("Tableau de bord")
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "m-r-20 align-self-center" }, [
      _c("span", { staticClass: "lstick m-r-20" }),
      _c("img", {
        attrs: { src: "/bp/assets/images/icon/expense.png", alt: "Income" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h2", [
      _vm._v("Plan de Financement"),
      _c("sup", [_vm._v("HT")]),
      _vm._v(" Initial")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "m-r-20 align-self-center" }, [
      _c("span", { staticClass: "lstick m-r-20" }),
      _c("img", {
        attrs: { src: "/bp/assets/images/icon/expense.png", alt: "Income" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h2", [
      _vm._v("Compte de Résultats"),
      _c("sup", [_vm._v("HT")]),
      _vm._v(" Année 1")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "d-flex no-block align-items-center" }, [
      _c("h3", { staticClass: "card-title" }, [
        _c("b", [_vm._v("Répartition des Besoins")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "d-flex no-block align-items-center" }, [
      _c("h3", { staticClass: "card-title" }, [
        _c("b", [_vm._v("Répartition des Ressources")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", { staticClass: "cbs-text-donnut" }, [
      _vm._v("Chiffre d'affaires "),
      _c("sup", [_c("i", [_vm._v("HT")])])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", { staticClass: "cbs-text-donnut" }, [
      _vm._v("Chiffre d'affaires"),
      _c("sup", [_c("i", [_vm._v("HT")])]),
      _vm._v(" (-) charge variable")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", { staticClass: "cbs-text-donnut" }, [
      _vm._v("Chiffre d'affaires"),
      _c("sup", [_c("i", [_vm._v("HT")])]),
      _vm._v(" (-) charge fixe & variable")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "d-flex no-block align-items-center" }, [
      _c("h3", { staticClass: "card-title" }, [
        _c("b", [_vm._v("Seuil de rentabilité, Point mort")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "d-flex no-block align-items-center" }, [
      _c("h3", { staticClass: "card-title" }, [
        _c("b", [_vm._v("Plan de la tresorerie")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/assets/js/currency-bus.js":
/*!*********************************************!*\
  !*** ./resources/assets/js/currency-bus.js ***!
  \*********************************************/
/*! exports provided: CurrencyBus */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CurrencyBus", function() { return CurrencyBus; });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);

var CurrencyBus = new vue__WEBPACK_IMPORTED_MODULE_0___default.a();

/***/ }),

/***/ "./resources/assets/js/layouts/sidebar-financier.vue":
/*!***********************************************************!*\
  !*** ./resources/assets/js/layouts/sidebar-financier.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _sidebar_financier_vue_vue_type_template_id_2ae30a12_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./sidebar-financier.vue?vue&type=template&id=2ae30a12&scoped=true& */ "./resources/assets/js/layouts/sidebar-financier.vue?vue&type=template&id=2ae30a12&scoped=true&");
/* harmony import */ var _sidebar_financier_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sidebar-financier.vue?vue&type=script&lang=js& */ "./resources/assets/js/layouts/sidebar-financier.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _sidebar_financier_vue_vue_type_style_index_0_id_2ae30a12_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./sidebar-financier.vue?vue&type=style&index=0&id=2ae30a12&scoped=true&lang=css& */ "./resources/assets/js/layouts/sidebar-financier.vue?vue&type=style&index=0&id=2ae30a12&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _sidebar_financier_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _sidebar_financier_vue_vue_type_template_id_2ae30a12_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _sidebar_financier_vue_vue_type_template_id_2ae30a12_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "2ae30a12",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/layouts/sidebar-financier.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/assets/js/layouts/sidebar-financier.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ./resources/assets/js/layouts/sidebar-financier.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_sidebar_financier_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./sidebar-financier.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/layouts/sidebar-financier.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_sidebar_financier_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/layouts/sidebar-financier.vue?vue&type=style&index=0&id=2ae30a12&scoped=true&lang=css&":
/*!********************************************************************************************************************!*\
  !*** ./resources/assets/js/layouts/sidebar-financier.vue?vue&type=style&index=0&id=2ae30a12&scoped=true&lang=css& ***!
  \********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_sidebar_financier_vue_vue_type_style_index_0_id_2ae30a12_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--7-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./sidebar-financier.vue?vue&type=style&index=0&id=2ae30a12&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/layouts/sidebar-financier.vue?vue&type=style&index=0&id=2ae30a12&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_sidebar_financier_vue_vue_type_style_index_0_id_2ae30a12_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_sidebar_financier_vue_vue_type_style_index_0_id_2ae30a12_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_sidebar_financier_vue_vue_type_style_index_0_id_2ae30a12_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_sidebar_financier_vue_vue_type_style_index_0_id_2ae30a12_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_sidebar_financier_vue_vue_type_style_index_0_id_2ae30a12_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/assets/js/layouts/sidebar-financier.vue?vue&type=template&id=2ae30a12&scoped=true&":
/*!******************************************************************************************************!*\
  !*** ./resources/assets/js/layouts/sidebar-financier.vue?vue&type=template&id=2ae30a12&scoped=true& ***!
  \******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_sidebar_financier_vue_vue_type_template_id_2ae30a12_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./sidebar-financier.vue?vue&type=template&id=2ae30a12&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/layouts/sidebar-financier.vue?vue&type=template&id=2ae30a12&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_sidebar_financier_vue_vue_type_template_id_2ae30a12_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_sidebar_financier_vue_vue_type_template_id_2ae30a12_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/assets/js/views/bp/financier/dashboard.vue":
/*!**************************************************************!*\
  !*** ./resources/assets/js/views/bp/financier/dashboard.vue ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _dashboard_vue_vue_type_template_id_4e44c142___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dashboard.vue?vue&type=template&id=4e44c142& */ "./resources/assets/js/views/bp/financier/dashboard.vue?vue&type=template&id=4e44c142&");
/* harmony import */ var _dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./dashboard.vue?vue&type=script&lang=js& */ "./resources/assets/js/views/bp/financier/dashboard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _dashboard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dashboard.vue?vue&type=style&index=0&lang=css& */ "./resources/assets/js/views/bp/financier/dashboard.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _dashboard_vue_vue_type_template_id_4e44c142___WEBPACK_IMPORTED_MODULE_0__["render"],
  _dashboard_vue_vue_type_template_id_4e44c142___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/views/bp/financier/dashboard.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/assets/js/views/bp/financier/dashboard.vue?vue&type=script&lang=js&":
/*!***************************************************************************************!*\
  !*** ./resources/assets/js/views/bp/financier/dashboard.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./dashboard.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/financier/dashboard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/views/bp/financier/dashboard.vue?vue&type=style&index=0&lang=css&":
/*!***********************************************************************************************!*\
  !*** ./resources/assets/js/views/bp/financier/dashboard.vue?vue&type=style&index=0&lang=css& ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_dashboard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/style-loader!../../../../../../node_modules/css-loader??ref--7-1!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./dashboard.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/financier/dashboard.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_dashboard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_dashboard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_dashboard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_dashboard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_dashboard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/assets/js/views/bp/financier/dashboard.vue?vue&type=template&id=4e44c142&":
/*!*********************************************************************************************!*\
  !*** ./resources/assets/js/views/bp/financier/dashboard.vue?vue&type=template&id=4e44c142& ***!
  \*********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_dashboard_vue_vue_type_template_id_4e44c142___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./dashboard.vue?vue&type=template&id=4e44c142& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/financier/dashboard.vue?vue&type=template&id=4e44c142&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_dashboard_vue_vue_type_template_id_4e44c142___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_dashboard_vue_vue_type_template_id_4e44c142___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/assets/js/views/bp/financier/default.emptyBesoins.json":
/*!**************************************************************************!*\
  !*** ./resources/assets/js/views/bp/financier/default.emptyBesoins.json ***!
  \**************************************************************************/
/*! exports provided: immoIncorporelle, immoCorporelle, immoFinanciere, bfr, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"immoIncorporelle\":[],\"immoCorporelle\":{\"materielinfo\":[],\"mobilier\":[],\"materiel-machine-outil\":[],\"batiment-local-espacevente\":[],\"voiture-autres\":[]},\"immoFinanciere\":{\"cautions\":[],\"depots\":[],\"autres\":[]},\"bfr\":{\"bfrOuverture\":[],\"stock\":[],\"creanceClient\":[],\"dettes\":[]}}");

/***/ }),

/***/ "./resources/assets/js/views/bp/financier/default.emptyCharges.json":
/*!**************************************************************************!*\
  !*** ./resources/assets/js/views/bp/financier/default.emptyCharges.json ***!
  \**************************************************************************/
/*! exports provided: matieres-premieres, services-exterieurs, taxes-impots, frais-personnel, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"matieres-premieres\":{\"depenses-produits\":[],\"charges-variables-produits\":[]},\"services-exterieurs\":[],\"taxes-impots\":{\"impots\":[],\"mairie\":[]},\"frais-personnel\":{\"dirigeants\":[],\"salaries\":[],\"stagiaires\":[]}}");

/***/ }),

/***/ "./resources/assets/js/views/bp/financier/default.emptyProduits.json":
/*!***************************************************************************!*\
  !*** ./resources/assets/js/views/bp/financier/default.emptyProduits.json ***!
  \***************************************************************************/
/*! exports provided: articles, volumeVente, resultatsExceptionnels, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"articles\":[],\"volumeVente\":[],\"resultatsExceptionnels\":{\"entrees\":[],\"sorties\":[]}}");

/***/ }),

/***/ "./resources/assets/js/views/bp/financier/default.emptyRessources.json":
/*!*****************************************************************************!*\
  !*** ./resources/assets/js/views/bp/financier/default.emptyRessources.json ***!
  \*****************************************************************************/
/*! exports provided: capital, comptes, emprunts, subventions-aides, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"capital\":{\"numeraire\":[],\"nature\":[]},\"comptes\":[],\"emprunts\":{\"banque\":[],\"microfinance\":[]},\"subventions-aides\":{\"banque\":[],\"institution\":[],\"autres\":[]}}");

/***/ }),

/***/ "./resources/assets/js/views/bp/financier/toolbar.js":
/*!***********************************************************!*\
  !*** ./resources/assets/js/views/bp/financier/toolbar.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function toolbar() {
  return {
    stacked: true,
    toolbar: {
      show: true,
      tools: {
        download: true,
        selection: true,
        zoom: true,
        zoomin: true,
        zoomout: true,
        pan: true,
        reset: true | '<img src="/static/icons/reset.png" width="20">',
        customIcons: []
      },
      autoSelected: 'zoom'
    },
    zoom: {
      enabled: true,
      type: 'x',
      autoScaleYaxis: false,
      zoomedArea: {
        fill: {
          color: '#90CAF9',
          opacity: 0.4
        },
        stroke: {
          color: '#0D47A1',
          opacity: 0.4,
          width: 1
        }
      }
    },
    animations: {
      enabled: true,
      easing: 'easeinout',
      speed: 800,
      animateGradually: {
        enabled: true,
        delay: 150
      },
      dynamicAnimation: {
        enabled: true,
        speed: 350
      }
    },
    selection: {
      enabled: true,
      type: 'x',
      fill: {
        color: '#24292e',
        opacity: 0.1
      },
      stroke: {
        width: 1,
        dashArray: 3,
        color: '#24292e',
        opacity: 0.4
      },
      xaxis: {
        min: undefined,
        max: undefined
      },
      yaxis: {
        min: undefined,
        max: undefined
      }
    }
  };
}

function dataLabel() {
  return {
    enabled: true,
    enabledOnSeries: undefined,
    textAnchor: 'middle',
    offsetX: 0,
    offsetY: 0,
    style: {
      fontSize: '14px',
      fontFamily: 'Helvetica, Arial, sans-serif',
      colors: undefined
    },
    dropShadow: {
      enabled: false,
      top: 1,
      left: 1,
      blur: 1,
      opacity: 0.45
    }
  };
}

/***/ })

}]);
//# sourceMappingURL=financier.dashboard.js.map