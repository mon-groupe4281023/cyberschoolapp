(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["financier.charges"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/financier/charges.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/bp/financier/charges.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue_form_wizard__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-form-wizard */ "./node_modules/vue-form-wizard/dist/vue-form-wizard.js");
/* harmony import */ var vue_form_wizard__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_form_wizard__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vue_bootstrap_typeahead__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue-bootstrap-typeahead */ "./node_modules/vue-bootstrap-typeahead/src/components/VueBootstrapTypeahead.vue");
/* harmony import */ var vue_form_wizard_dist_vue_form_wizard_min_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vue-form-wizard/dist/vue-form-wizard.min.css */ "./node_modules/vue-form-wizard/dist/vue-form-wizard.min.css");
/* harmony import */ var vue_form_wizard_dist_vue_form_wizard_min_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(vue_form_wizard_dist_vue_form_wizard_min_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var vue_numeric__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vue-numeric */ "./node_modules/vue-numeric/dist/vue-numeric.min.js");
/* harmony import */ var vue_numeric__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(vue_numeric__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _mixins_currenciesMask__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../mixins/currenciesMask */ "./resources/assets/js/views/bp/mixins/currenciesMask.js");
/* harmony import */ var _mixins_financier_charges__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../mixins/financier/charges */ "./resources/assets/js/views/bp/mixins/financier/charges.js");
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! vue-loading-overlay */ "./node_modules/vue-loading-overlay/dist/vue-loading.min.js");
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(vue_loading_overlay__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var vue_loading_overlay_dist_vue_loading_css__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! vue-loading-overlay/dist/vue-loading.css */ "./node_modules/vue-loading-overlay/dist/vue-loading.css");
/* harmony import */ var vue_loading_overlay_dist_vue_loading_css__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(vue_loading_overlay_dist_vue_loading_css__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _layouts_header_bp_suivi_vue__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../layouts/header-bp-suivi.vue */ "./resources/assets/js/layouts/header-bp-suivi.vue");
/* harmony import */ var _layouts_footer_bp_vue__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../layouts/footer-bp.vue */ "./resources/assets/js/layouts/footer-bp.vue");
/* harmony import */ var _layouts_sidebar_financier__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../layouts/sidebar-financier */ "./resources/assets/js/layouts/sidebar-financier.vue");
/* harmony import */ var _layouts_formmenufinancier_vue__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../layouts/formmenufinancier.vue */ "./resources/assets/js/layouts/formmenufinancier.vue");
/* harmony import */ var _layouts_formmenufinanciertop_vue__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../../layouts/formmenufinanciertop.vue */ "./resources/assets/js/layouts/formmenufinanciertop.vue");
/* harmony import */ var _default_charges_json__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./default.charges.json */ "./resources/assets/js/views/bp/financier/default.charges.json");
var _default_charges_json__WEBPACK_IMPORTED_MODULE_15___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./default.charges.json */ "./resources/assets/js/views/bp/financier/default.charges.json", 1);
/* harmony import */ var _default_chargePatronale_json__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./default.chargePatronale.json */ "./resources/assets/js/views/bp/financier/default.chargePatronale.json");
var _default_chargePatronale_json__WEBPACK_IMPORTED_MODULE_16___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./default.chargePatronale.json */ "./resources/assets/js/views/bp/financier/default.chargePatronale.json", 1);
/* harmony import */ var _services_helper__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../../services/helper */ "./resources/assets/js/services/helper.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var _default_tva_json__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./default.tva.json */ "./resources/assets/js/views/bp/financier/default.tva.json");
var _default_tva_json__WEBPACK_IMPORTED_MODULE_19___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./default.tva.json */ "./resources/assets/js/views/bp/financier/default.tva.json", 1);
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! util */ "./node_modules/node-libs-browser/node_modules/util/util.js");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_20___default = /*#__PURE__*/__webpack_require__.n(util__WEBPACK_IMPORTED_MODULE_20__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//









 // elements de menus





 // default data for charge




 // tva cadeaux



var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 4000
}); // Init plugin

vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(vue_loading_overlay__WEBPACK_IMPORTED_MODULE_7___default.a, {
  isFullPage: true,
  canCancel: false,
  loader: "spinner"
}); // init Form Wizard plugin

vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(vue_form_wizard__WEBPACK_IMPORTED_MODULE_1___default.a);
/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    VueFormWizard: vue_form_wizard__WEBPACK_IMPORTED_MODULE_1___default.a,
    VueBootstrapTypeahead: vue_bootstrap_typeahead__WEBPACK_IMPORTED_MODULE_2__["default"],
    VueNumeric: vue_numeric__WEBPACK_IMPORTED_MODULE_4___default.a,
    AppHeader: _layouts_header_bp_suivi_vue__WEBPACK_IMPORTED_MODULE_10__["default"],
    AppFooter: _layouts_footer_bp_vue__WEBPACK_IMPORTED_MODULE_11__["default"],
    sideMenu: _layouts_sidebar_financier__WEBPACK_IMPORTED_MODULE_12__["default"],
    Menuform: _layouts_formmenufinancier_vue__WEBPACK_IMPORTED_MODULE_13__["default"],
    Menuformtop: _layouts_formmenufinanciertop_vue__WEBPACK_IMPORTED_MODULE_14__["default"]
  },
  mixins: [_mixins_currenciesMask__WEBPACK_IMPORTED_MODULE_5__["default"], _mixins_financier_charges__WEBPACK_IMPORTED_MODULE_6__["default"]],
  data: function data() {
    return {
      ref: firebase__WEBPACK_IMPORTED_MODULE_18___default.a.firestore().collection('financier'),
      financierExist: '',
      //check if financier exist or not
      bpfinancier: _default_charges_json__WEBPACK_IMPORTED_MODULE_15__,
      projetId: JSON.parse(localStorage.getItem('userSession')).projet,
      chargePatronale: _default_chargePatronale_json__WEBPACK_IMPORTED_MODULE_16__
    };
  },
  created: function created() {
    var _this = this;

    var loader = vue__WEBPACK_IMPORTED_MODULE_0___default.a.$loading.show(); // on recupère l'id du projet dans la session

    var id = JSON.parse(localStorage.getItem("userSession")).projet;
    this.ref.doc(id).get().then(function (doc) {
      loader.hide(); //on vérifie si la partie besoins n'est pas vide

      if (doc.exists) {
        _this.financierExist = 'Oui';
        console.log('oui', doc.data());
        var chg = doc.data().charges;

        if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_17__["isEmpty"])(chg)) {
          _this.bpfinancier = chg;
        } else {
          var produits = doc.get('produits');
        } // construction du parametrage tva financier


        if (!Object(_services_helper__WEBPACK_IMPORTED_MODULE_17__["isEmpty"])(doc.data().parametres)) {
          if (!Object(util__WEBPACK_IMPORTED_MODULE_20__["isNullOrUndefined"])(doc.data().parametres.chargePatronale)) {
            _this.chargePatronale = doc.data().parametres.chargePatronale;
          }
        }

        _this.chargeSocialeEmploye = _this.chargePatronale.employe;
        _this.chargeSocialeDirigeant = _this.chargePatronale.dirigeant;
      } else {}
      /*
      this.financierExist = 'Non';
      // enregistrement d'un financier vide pour enregistrer ls charges avant ls besoins
      this.ref.doc(this.projetId).set({
      user: JSON.parse(localStorage.getItem("userSession")).uid,
      projet: this.projetId, besoins: {}, ressources: {}, produits: {}, charges: {},
      parametres: { "tva": defaultTva, "currency": null}
      }).catch( (error) => {
      console.log(error.message);
      this.financierExist = 'Non';
      }).then( (financier) => {
      this.financierExist = 'Oui';
      }) */
      // initialisation des totaux


      _this.calculTotalAchatMpDepVariable(_this.bpfinancier['matieres-premieres']['depenses-produits']);

      _this.calculTotalAchatMpCharVariable(_this.bpfinancier['matieres-premieres']['charges-variables-produits']);

      _this.calculTotalServExtFrais(_this.bpfinancier['services-exterieurs']);

      _this.calculSubTotalTaxeImpMairie(_this.bpfinancier['taxes-impots'].mairie);

      _this.calculSubTotalTaxImpImpot(_this.bpfinancier['taxes-impots'].impots);

      _this.calculSubTotalFraisSalarie(_this.bpfinancier['frais-personnel'].salaries);

      _this.calculSubTotalFraisStagiaire(_this.bpfinancier['frais-personnel'].stagiaires);

      _this.calculSubTotalFraisDirigeant(_this.bpfinancier['frais-personnel'].dirigeants); // items computed


      _this.calculSubTotalFraisCharge(); // effectifs


      _this.subTotalFraisSalarie.effectif = _this.calculEffectifPersonnel(_this.bpfinancier['frais-personnel'].salaries);
      _this.subTotalFraisStagiaire.effectif = _this.calculEffectifPersonnel(_this.bpfinancier['frais-personnel']['stagiaires']);
      _this.subTotalFraisDirigeant.effectif = _this.calculEffectifPersonnel(_this.bpfinancier['frais-personnel']['dirigeants']);

      _this.calculEffectifTotal();
    })["catch"](function (error) {
      loader.hide();
    });
  },
  methods: {
    addNewRow: function addNewRow(array) {
      var target = null;

      switch (array.toLowerCase()) {
        case "matieres-premieres.depenses-produits":
          target = this.bpfinancier['matieres-premieres']['depenses-produits'];
          target.push({
            "titre": "Element",
            "quantite": 1,
            "prix": 0,
            "annee1": 0,
            "commentaire": "",
            "accroissement1": 5,
            "annee2": 0,
            "accroissement2": 5,
            "annee3": 0,
            "group": "depenses-produits"
          });
          break;

        case "matieres-premieres.charges-variables-produits":
          target = this.bpfinancier['matieres-premieres']['charges-variables-produits'];
          target.push({
            "titre": "Matière Première N°X",
            "quantite": 1,
            "prix": 0,
            "commentaire": "",
            "annee1": 0,
            "accroissement1": 2,
            "annee2": 0,
            "accroissement2": 2,
            "annee3": 0,
            "group": "charges-variables-produits"
          });
          break;

        case "services-exterieurs":
          target = this.bpfinancier['services-exterieurs'];
          target.push({
            "titre": "Tiers",
            "quantite": 12,
            "prix": 0,
            "annee1": 0,
            "commentaire": "",
            "accroissement1": 2,
            "annee2": 0,
            "accroissement2": 2,
            "annee3": 0
          });
          break;

        case "taxes-impots.impots":
          target = this.bpfinancier['taxes-impots']['impots'];
          target.push({
            "titre": "Autre impot",
            "quantite": 1,
            "prix": 0,
            "annee1": 0,
            "commentaire": "",
            "accroissement1": 2,
            "annee2": 0,
            "accroissement2": 2,
            "annee3": 0,
            "group": "impots"
          });
          break;

        case "taxes-impots.mairie":
          target = this.bpfinancier['taxes-impots']['mairie'];
          target.push({
            "titre": "Autre taxe",
            "quantite": 1,
            "prix": 0,
            "annee1": 0,
            "commentaire": "",
            "accroissement1": 2,
            "annee2": 0,
            "accroissement2": 2,
            "annee3": 0,
            "group": "mairie"
          });
          break;

        case "frais-personnel.dirigeants":
          target = this.bpfinancier['frais-personnel']['dirigeants'];
          target.push({
            "titre": "Titre",
            "contrat": "CDI",
            "effectif": 1,
            "ouvrable": 12,
            "salaire": 0,
            "annee1": 0,
            "accroissement1": 2,
            "annee2": 0,
            "accroissement2": 2,
            "annee3": 0,
            "group": "dirigeants"
          });
          this.calculSubTotalFraisDirigeant(target);
          break;

        case "frais-personnel.salaries":
          target = this.bpfinancier['frais-personnel']['salaries'];
          target.push({
            "titre": "Titre",
            "contrat": "CDD",
            "effectif": 1,
            "ouvrable": 12,
            "salaire": 0,
            "annee1": 0,
            "accroissement1": 2,
            "annee2": 0,
            "accroissement2": 2,
            "annee3": 0,
            "group": "salaries"
          });
          this.calculSubTotalFraisSalarie(target);
          break;

        case "frais-personnel.stagiaires":
          target = this.bpfinancier['frais-personnel']['stagiaires'];
          target.push({
            "titre": "Poste",
            "contrat": "CDD",
            "effectif": 1,
            "ouvrable": 6,
            "salaire": 0,
            "annee1": 0,
            "accroissement1": 2,
            "annee2": 0,
            "accroissement2": 2,
            "annee3": 0,
            "group": "stagiaires"
          });
          this.calculSubTotalFraisStagiaire(target);
          break;
      }
    },
    deleteRow: function deleteRow(array, item) {
      var _this2 = this;

      var target = null;
      var recalculTotal = null;

      switch (array.toLowerCase()) {
        case "matieres-premieres.depenses-produits":
          target = this.bpfinancier['matieres-premieres']['depenses-produits'];

          recalculTotal = function recalculTotal() {
            _this2.calculTotalAchatMpDepVariable(target);
          };

          break;

        case "matieres-premieres.charges-variables-produits":
          target = this.bpfinancier['matieres-premieres']['charges-variables-produits'];

          recalculTotal = function recalculTotal() {
            _this2.calculTotalAchatMpCharVariable(target);
          };

          break;

        case "services-exterieurs":
          target = this.bpfinancier['services-exterieurs'];

          recalculTotal = function recalculTotal() {
            _this2.calculTotalServExtFrais(target);
          };

          break;

        case "taxes-impots.impots":
          target = this.bpfinancier['taxes-impots']['impots'];

          recalculTotal = function recalculTotal() {
            _this2.calculSubTotalTaxImpImpot(target);
          };

          break;

        case "taxes-impots.mairie":
          target = this.bpfinancier['taxes-impots']['mairie'];

          recalculTotal = function recalculTotal() {
            _this2.calculSubTotalTaxeImpMairie(target);
          };

          break;

        case "frais-personnel.salaries":
          target = this.bpfinancier['frais-personnel']['salaries'];

          recalculTotal = function recalculTotal() {
            _this2.calculSubTotalFraisSalarie(target);
          };

          break;

        case "frais-personnel.dirigeants":
          target = this.bpfinancier['frais-personnel']['dirigeants'];

          recalculTotal = function recalculTotal() {
            _this2.calculSubTotalFraisDirigeant(target);
          };

          break;

        case "frais-personnel.stagiaires":
          target = this.bpfinancier['frais-personnel']['stagiaires'];

          recalculTotal = function recalculTotal() {
            _this2.calculSubTotalFraisStagiaire(target);
          };

          break;
      }

      var idx = target.indexOf(item);

      if (idx > -1) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire({
          title: 'Attention',
          text: "Vous ne pourrez pas revenir en arrière !",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'je confirme',
          cancelButtonText: 'j\'annule'
        }).then(function (result) {
          if (result.value) {
            target.splice(idx, 1);
            recalculTotal();
          }
        });
      }
    },
    updateEffectifPersonnel: function updateEffectifPersonnel(personnel) {
      /**
       * mise a jour du sous effectif
       * mise a jour des salaires
       * mise a jour des totaux (effectif/salaire/annee1/annee2/annee3)
       **/
      switch (personnel.group) {
        case "stagiaires":
          this.salaireInitial(personnel, 'frais.stagiaire', this.bpfinancier['frais-personnel']['stagiaires']);
          this.subTotalFraisStagiaire.effectif = this.calculEffectifPersonnel(this.bpfinancier['frais-personnel']['stagiaires']);
          break;

        case "salaries":
          this.salaireInitial(personnel, 'frais.salarie', this.bpfinancier['frais-personnel']['salaries']);
          this.subTotalFraisSalarie.effectif = this.calculEffectifPersonnel(this.bpfinancier['frais-personnel'].salaries);
          break;

        case "dirigeants":
          this.salaireInitial(personnel, 'frais.dirigeant', this.bpfinancier['frais-personnel']['dirigeants']);
          this.subTotalFraisDirigeant.effectif = this.calculEffectifPersonnel(this.bpfinancier['frais-personnel']['dirigeants']);
          break;

        default:
          console.log('groupe viser:' + personnel.group);
          break;
      }

      ;
      this.calculEffectifTotal();
    },
    calculEffectifSalaries: function calculEffectifSalaries(personnelSalaries) {
      var sum = 0;
      personnelSalaries.forEach(function (salarie) {
        sum = sum + salarie.effectif;
      });
      this.subTotalFraisSalarie.effectif = sum;
    },
    calculEffectifStagiaires: function calculEffectifStagiaires(personnelStagiaires) {
      var sum = 0;
      personnelStagiaires.forEach(function (stagiaire) {
        sum = sum + stagiaire.effectif;
      });
      this.subTotalFraisStagiaire.effectif = sum;
    },
    calculEffectifDirigeants: function calculEffectifDirigeants(personnelsDirigeant) {
      var sum = 0;
      personnelsDirigeant.forEach(function (stagiaire) {
        sum = sum + stagiaire.effectif;
      });
      this.subTotalFraisDirigeant.effectif = sum;
    },
    nextTab: function nextTab(props) {
      var _this3 = this;

      //si on est pas au dernier step
      if (!props.isLastStep) {
        var loader = vue__WEBPACK_IMPORTED_MODULE_0___default.a.$loading.show(); //alors on met à jour la collection

        this.ref.doc(this.projetId).update({
          charges: this.bpfinancier
        }).then(function (financier) {
          loader.hide();
          Toast.fire({
            type: 'success',
            title: 'Enregistrement automatique des charges avec succès',
            customClass: "bg-success"
          });
          props.nextTab();
        })["catch"](function (error) {
          loader.hide();
          sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire('Une erreur est survenue', error.message, 'error');
        });
      } else {
        var _loader = vue__WEBPACK_IMPORTED_MODULE_0___default.a.$loading.show(); //alors on met à jour la collection


        this.ref.doc(this.projetId).update({
          charges: this.bpfinancier
        }).then(function (financier) {
          _loader.hide();

          sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire('Terminé!!!', 'Vous avez terminé l\'enregistrement des éléments de base. Vous Allez être redirigé vers le récapitulatif', 'success').then(function (result) {
            _this3.$router.push({
              name: 'recapitulatif'
            });
          });
        })["catch"](function (error) {
          _loader.hide();

          sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire('Une erreur est survenue', error.message, 'error');
        });
      }
    },
    stepSaved: function stepSaved(e) {
      var loader = vue__WEBPACK_IMPORTED_MODULE_0___default.a.$loading.show(); // console.log(this.financierExist)

      if (this.financierExist == 'Oui') {
        this.ref.doc(this.projetId).update({
          charges: this.bpfinancier
        }).then(function () {
          loader.hide();
          sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire('Charges enrégistrées avec succès', 'success');
        })["catch"](function (error) {
          loader.hide();
          sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire('Une erreur est survenue', error.message, 'error');
        });
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/financier/charges.vue?vue&type=style&index=0&id=3f866a26&scoped=true&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/bp/financier/charges.vue?vue&type=style&index=0&id=3f866a26&scoped=true&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports
exports.i(__webpack_require__(/*! -!../../../../../../node_modules/css-loader??ref--7-1!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../resources/assets/sass/financier.scss */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./resources/assets/sass/financier.scss"), "");
exports.i(__webpack_require__(/*! -!../../../../../../node_modules/css-loader??ref--7-1!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../resources/assets/sass/tiny-inputs.scss */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./resources/assets/sass/tiny-inputs.scss"), "");

// module
exports.push([module.i, "\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/financier/charges.vue?vue&type=style&index=0&id=3f866a26&scoped=true&lang=css&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/bp/financier/charges.vue?vue&type=style&index=0&id=3f866a26&scoped=true&lang=css& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader??ref--7-1!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./charges.vue?vue&type=style&index=0&id=3f866a26&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/financier/charges.vue?vue&type=style&index=0&id=3f866a26&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/layouts/formmenufinancier.vue?vue&type=template&id=75510f80&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/layouts/formmenufinancier.vue?vue&type=template&id=75510f80& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "ul",
      { staticClass: "nav nav-tabs customtab", attrs: { role: "tablist" } },
      [
        _c(
          "li",
          { staticClass: "nav-item" },
          [
            _c(
              "router-link",
              { staticClass: "nav-link", attrs: { to: { name: "besoins" } } },
              [
                _c("span", [_c("i", { staticClass: "fas fa-clipboard-list" })]),
                _vm._v(" "),
                _c("span", { staticClass: "hidden-xs-down" }, [
                  _vm._v("Besoins")
                ])
              ]
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "li",
          { staticClass: "nav-item" },
          [
            _c(
              "router-link",
              {
                staticClass: "nav-link",
                attrs: { to: { name: "ressources" } }
              },
              [
                _c("span", [_c("i", { staticClass: "ti-wallet" })]),
                _vm._v(" "),
                _c("span", { staticClass: "hidden-xs-down" }, [
                  _vm._v("Ressources")
                ])
              ]
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "li",
          { staticClass: "nav-item" },
          [
            _c(
              "router-link",
              { staticClass: "nav-link ", attrs: { to: { name: "produits" } } },
              [
                _c("span", [_c("i", { staticClass: "ti-dropbox-alt" })]),
                _vm._v(" "),
                _c("span", { staticClass: "hidden-xs-down" }, [
                  _vm._v("Produits")
                ])
              ]
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "li",
          { staticClass: "nav-item" },
          [
            _c(
              "router-link",
              { staticClass: "nav-link", attrs: { to: { name: "charges" } } },
              [
                _c("span", [_c("i", { staticClass: "fas fa-calculator" })]),
                _vm._v(" "),
                _c("span", { staticClass: "hidden-xs-down" }, [
                  _vm._v("Charges")
                ])
              ]
            )
          ],
          1
        )
      ]
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/financier/charges.vue?vue&type=template&id=3f866a26&scoped=true&":
/*!*************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/bp/financier/charges.vue?vue&type=template&id=3f866a26&scoped=true& ***!
  \*************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "page-wrapper" }, [
    _c("div", { staticClass: "container-fluid" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-lg-12 col-xlg-12 col-md-12" }, [
          _c("div", { staticClass: "card" }, [
            _c("div", { staticClass: "tab-content" }, [
              _c(
                "div",
                { staticClass: "card-body" },
                [
                  _c("menuformtop"),
                  _vm._v(" "),
                  _c("menuform"),
                  _vm._v(" "),
                  _c("div", { staticClass: "tab-content" }, [
                    _c(
                      "div",
                      { staticClass: "tab-pane active", attrs: { role: "" } },
                      [
                        _c(
                          "form-wizard",
                          {
                            attrs: {
                              "step-size": "sm",
                              title: "",
                              nextButtonText: "Suivant",
                              backButtonText: "Précédent",
                              finishButtonText: "Enregistrer",
                              color: "#61b174",
                              subtitle: "",
                              shape: "circle"
                            },
                            scopedSlots: _vm._u([
                              {
                                key: "footer",
                                fn: function(props) {
                                  return [
                                    _c(
                                      "div",
                                      { staticClass: "wizard-footer-left" },
                                      [
                                        props.activeTabIndex > 0
                                          ? _c(
                                              "wizard-button",
                                              {
                                                style: props.fillButtonStyle,
                                                nativeOn: {
                                                  click: function($event) {
                                                    return props.prevTab()
                                                  }
                                                }
                                              },
                                              [_vm._v("Précédent")]
                                            )
                                          : _vm._e()
                                      ],
                                      1
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      { staticClass: "wizard-footer-right" },
                                      [
                                        _c(
                                          "wizard-button",
                                          {
                                            staticClass: "btn btn-info mr-1",
                                            staticStyle: {
                                              "background-color": "#398bf7"
                                            },
                                            nativeOn: {
                                              click: function($event) {
                                                return _vm.stepSaved(props)
                                              }
                                            }
                                          },
                                          [
                                            _vm._v(
                                              "\n                                                        Enregistrer\n                                                    "
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        !props.isLastStep
                                          ? _c(
                                              "wizard-button",
                                              {
                                                staticClass:
                                                  "wizard-footer-right",
                                                style: props.fillButtonStyle,
                                                nativeOn: {
                                                  click: function($event) {
                                                    return _vm.nextTab(props)
                                                  }
                                                }
                                              },
                                              [_vm._v("Suivant")]
                                            )
                                          : _c(
                                              "wizard-button",
                                              {
                                                staticClass:
                                                  "wizard-footer-right finish-button",
                                                style: props.fillButtonStyle,
                                                nativeOn: {
                                                  click: function($event) {
                                                    return _vm.nextTab(props)
                                                  }
                                                }
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    props.isLastStep
                                                      ? "Terminé"
                                                      : "Suivant"
                                                  )
                                                )
                                              ]
                                            )
                                      ],
                                      1
                                    )
                                  ]
                                }
                              }
                            ])
                          },
                          [
                            _c(
                              "tab-content",
                              {
                                attrs: {
                                  title: "1. Achat Matières Premières (MP)",
                                  icon: "ti-user"
                                }
                              },
                              [
                                _c("div", { staticClass: "col-md-12" }, [
                                  _c("div", { staticClass: "form-group" }, [
                                    _c(
                                      "div",
                                      { staticClass: "table-responsive" },
                                      [
                                        _c(
                                          "table",
                                          {
                                            staticClass:
                                              "table table-bordered table-1 m-b-0 color-bordered-table info-bordered-table"
                                          },
                                          [
                                            _c("col"),
                                            _vm._v(" "),
                                            _c("col", {
                                              staticStyle: { width: "5%" }
                                            }),
                                            _vm._v(" "),
                                            _c("col"),
                                            _vm._v(" "),
                                            _c("col"),
                                            _vm._v(" "),
                                            _c("col"),
                                            _vm._v(" "),
                                            _c("col", {
                                              staticStyle: { width: "6%" }
                                            }),
                                            _vm._v(" "),
                                            _c("col"),
                                            _vm._v(" "),
                                            _c("col", {
                                              staticStyle: { width: "6%" }
                                            }),
                                            _vm._v(" "),
                                            _c("col"),
                                            _vm._v(" "),
                                            _c("thead", [
                                              _c("tr", [
                                                _c(
                                                  "th",
                                                  {
                                                    staticClass:
                                                      "table-head-first-red-column"
                                                  },
                                                  [_vm._v("Répartition MP/CA")]
                                                ),
                                                _vm._v(" "),
                                                _c("th", [_vm._v("Quantité")]),
                                                _vm._v(" "),
                                                _c("th", [
                                                  _vm._v(
                                                    "(MP/CA)\n                                                                        "
                                                  ),
                                                  _c(
                                                    "span",
                                                    {
                                                      attrs: {
                                                        "data-container":
                                                          "body",
                                                        "data-toggle":
                                                          "popover",
                                                        "data-placement": "top",
                                                        "data-content":
                                                          "explication"
                                                      }
                                                    },
                                                    [
                                                      _c("i", {
                                                        staticClass:
                                                          "icon-info",
                                                        attrs: {
                                                          "data-toggle":
                                                            "tooltip",
                                                          title: "explication"
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]),
                                                _vm._v(" "),
                                                _c("th", [_vm._v("Année-1")]),
                                                _vm._v(" "),
                                                _c("th", [
                                                  _vm._v("Commentaires")
                                                ]),
                                                _vm._v(" "),
                                                _c("th", [
                                                  _vm._v("% "),
                                                  _c("i", {
                                                    staticClass:
                                                      "fas fa-caret-up"
                                                  })
                                                ]),
                                                _vm._v(" "),
                                                _c("th", [_vm._v("Année-2")]),
                                                _vm._v(" "),
                                                _c("th", [
                                                  _vm._v("% "),
                                                  _c("i", {
                                                    staticClass:
                                                      "fas fa-caret-up"
                                                  })
                                                ]),
                                                _vm._v(" "),
                                                _c("th", [_vm._v("Année-3")])
                                              ])
                                            ]),
                                            _vm._v(" "),
                                            _c(
                                              "tbody",
                                              [
                                                _c("tr", [
                                                  _c("td"),
                                                  _vm._v(" "),
                                                  _c("td"),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    {
                                                      staticClass:
                                                        "empty-numeric-placeholder"
                                                    },
                                                    [_vm._v("          ")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    {
                                                      staticClass:
                                                        "empty-numeric-placeholder"
                                                    },
                                                    [_vm._v("           ")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c("td"),
                                                  _vm._v(" "),
                                                  _c("td"),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    {
                                                      staticClass:
                                                        "empty-numeric-placeholder"
                                                    },
                                                    [_vm._v("           ")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c("td"),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    {
                                                      staticClass:
                                                        "empty-numeric-placeholder"
                                                    },
                                                    [_vm._v("           ")]
                                                  )
                                                ]),
                                                _vm._v(" "),
                                                _c(
                                                  "tr",
                                                  {
                                                    staticClass: "table-total"
                                                  },
                                                  [
                                                    _c(
                                                      "td",
                                                      {
                                                        attrs: { colspan: "2" }
                                                      },
                                                      [
                                                        _vm._v(
                                                          " Total dépenses variables liées aux produits"
                                                        )
                                                      ]
                                                    ),
                                                    _vm._v(" "),
                                                    _c("td"),
                                                    _vm._v(" "),
                                                    _c(
                                                      "td",
                                                      [
                                                        _c(
                                                          "center",
                                                          [
                                                            _c("vue-numeric", {
                                                              attrs: {
                                                                currency:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "currency"
                                                                  ],
                                                                "currency-symbol-position":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "currency-symbol-position"
                                                                  ],
                                                                "output-type":
                                                                  "String",
                                                                "empty-value":
                                                                  "0",
                                                                precision:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "precision"
                                                                  ],
                                                                "decimal-separator":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "decimal-separator"
                                                                  ],
                                                                "thousand-separator":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "thousand-separator"
                                                                  ],
                                                                "read-only-class":
                                                                  "item-total",
                                                                min:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "min"
                                                                  ],
                                                                "read-only": ""
                                                              },
                                                              model: {
                                                                value:
                                                                  _vm
                                                                    .totalAchatMpDepVariable
                                                                    .annee1,
                                                                callback: function(
                                                                  $$v
                                                                ) {
                                                                  _vm.$set(
                                                                    _vm.totalAchatMpDepVariable,
                                                                    "annee1",
                                                                    _vm._n($$v)
                                                                  )
                                                                },
                                                                expression:
                                                                  "totalAchatMpDepVariable.annee1"
                                                              }
                                                            })
                                                          ],
                                                          1
                                                        )
                                                      ],
                                                      1
                                                    ),
                                                    _vm._v(" "),
                                                    _c("td"),
                                                    _vm._v(" "),
                                                    _c("td"),
                                                    _vm._v(" "),
                                                    _c(
                                                      "td",
                                                      [
                                                        _c(
                                                          "center",
                                                          [
                                                            _c("vue-numeric", {
                                                              attrs: {
                                                                currency:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "currency"
                                                                  ],
                                                                "currency-symbol-position":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "currency-symbol-position"
                                                                  ],
                                                                "output-type":
                                                                  "String",
                                                                "empty-value":
                                                                  "0",
                                                                precision:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "precision"
                                                                  ],
                                                                "decimal-separator":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "decimal-separator"
                                                                  ],
                                                                "thousand-separator":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "thousand-separator"
                                                                  ],
                                                                "read-only-class":
                                                                  "item-total",
                                                                min:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "min"
                                                                  ],
                                                                "read-only": ""
                                                              },
                                                              model: {
                                                                value:
                                                                  _vm
                                                                    .totalAchatMpDepVariable
                                                                    .annee2,
                                                                callback: function(
                                                                  $$v
                                                                ) {
                                                                  _vm.$set(
                                                                    _vm.totalAchatMpDepVariable,
                                                                    "annee2",
                                                                    _vm._n($$v)
                                                                  )
                                                                },
                                                                expression:
                                                                  "totalAchatMpDepVariable.annee2"
                                                              }
                                                            })
                                                          ],
                                                          1
                                                        )
                                                      ],
                                                      1
                                                    ),
                                                    _vm._v(" "),
                                                    _c("td"),
                                                    _vm._v(" "),
                                                    _c(
                                                      "td",
                                                      [
                                                        _c(
                                                          "center",
                                                          [
                                                            _c("vue-numeric", {
                                                              attrs: {
                                                                currency:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "currency"
                                                                  ],
                                                                "currency-symbol-position":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "currency-symbol-position"
                                                                  ],
                                                                "output-type":
                                                                  "String",
                                                                "empty-value":
                                                                  "0",
                                                                precision:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "precision"
                                                                  ],
                                                                "decimal-separator":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "decimal-separator"
                                                                  ],
                                                                "thousand-separator":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "thousand-separator"
                                                                  ],
                                                                "read-only-class":
                                                                  "item-total",
                                                                min:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "min"
                                                                  ],
                                                                "read-only": ""
                                                              },
                                                              model: {
                                                                value:
                                                                  _vm
                                                                    .totalAchatMpDepVariable
                                                                    .annee3,
                                                                callback: function(
                                                                  $$v
                                                                ) {
                                                                  _vm.$set(
                                                                    _vm.totalAchatMpDepVariable,
                                                                    "annee3",
                                                                    _vm._n($$v)
                                                                  )
                                                                },
                                                                expression:
                                                                  "totalAchatMpDepVariable.annee3"
                                                              }
                                                            })
                                                          ],
                                                          1
                                                        )
                                                      ],
                                                      1
                                                    )
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c("tr", [
                                                  _c("td"),
                                                  _vm._v(" "),
                                                  _c("td"),
                                                  _vm._v(" "),
                                                  _c("td"),
                                                  _vm._v(" "),
                                                  _c("td"),
                                                  _vm._v(" "),
                                                  _c("td"),
                                                  _vm._v(" "),
                                                  _c("td"),
                                                  _vm._v(" "),
                                                  _c("td"),
                                                  _vm._v(" "),
                                                  _c("td"),
                                                  _vm._v(" "),
                                                  _c("td")
                                                ]),
                                                _vm._v(" "),
                                                _c(
                                                  "tr",
                                                  {
                                                    staticClass:
                                                      "table-group-header"
                                                  },
                                                  [
                                                    _c(
                                                      "td",
                                                      {
                                                        attrs: { colspan: "2" }
                                                      },
                                                      [
                                                        _vm._v(
                                                          " Dépenses variables liées au CA"
                                                        )
                                                      ]
                                                    ),
                                                    _vm._v(" "),
                                                    _c("td"),
                                                    _vm._v(" "),
                                                    _c(
                                                      "td",
                                                      [
                                                        _c(
                                                          "center",
                                                          [
                                                            _c("vue-numeric", {
                                                              attrs: {
                                                                currency:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "currency"
                                                                  ],
                                                                "currency-symbol-position":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "currency-symbol-position"
                                                                  ],
                                                                "output-type":
                                                                  "String",
                                                                "empty-value":
                                                                  "0",
                                                                precision:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "precision"
                                                                  ],
                                                                "decimal-separator":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "decimal-separator"
                                                                  ],
                                                                "thousand-separator":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "thousand-separator"
                                                                  ],
                                                                "read-only-class":
                                                                  "item-total",
                                                                min:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "min"
                                                                  ],
                                                                "read-only": ""
                                                              },
                                                              model: {
                                                                value:
                                                                  _vm
                                                                    .totalAchatMpDepVariable
                                                                    .annee1,
                                                                callback: function(
                                                                  $$v
                                                                ) {
                                                                  _vm.$set(
                                                                    _vm.totalAchatMpDepVariable,
                                                                    "annee1",
                                                                    _vm._n($$v)
                                                                  )
                                                                },
                                                                expression:
                                                                  "totalAchatMpDepVariable.annee1"
                                                              }
                                                            })
                                                          ],
                                                          1
                                                        )
                                                      ],
                                                      1
                                                    ),
                                                    _vm._v(" "),
                                                    _c("td"),
                                                    _vm._v(" "),
                                                    _c("td"),
                                                    _vm._v(" "),
                                                    _c(
                                                      "td",
                                                      [
                                                        _c(
                                                          "center",
                                                          [
                                                            _c("vue-numeric", {
                                                              attrs: {
                                                                currency:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "currency"
                                                                  ],
                                                                "currency-symbol-position":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "currency-symbol-position"
                                                                  ],
                                                                "output-type":
                                                                  "String",
                                                                "empty-value":
                                                                  "0",
                                                                precision:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "precision"
                                                                  ],
                                                                "decimal-separator":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "decimal-separator"
                                                                  ],
                                                                "thousand-separator":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "thousand-separator"
                                                                  ],
                                                                "read-only-class":
                                                                  "item-total",
                                                                min:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "min"
                                                                  ],
                                                                "read-only": ""
                                                              },
                                                              model: {
                                                                value:
                                                                  _vm
                                                                    .totalAchatMpDepVariable
                                                                    .annee2,
                                                                callback: function(
                                                                  $$v
                                                                ) {
                                                                  _vm.$set(
                                                                    _vm.totalAchatMpDepVariable,
                                                                    "annee2",
                                                                    _vm._n($$v)
                                                                  )
                                                                },
                                                                expression:
                                                                  "totalAchatMpDepVariable.annee2"
                                                              }
                                                            })
                                                          ],
                                                          1
                                                        )
                                                      ],
                                                      1
                                                    ),
                                                    _vm._v(" "),
                                                    _c("td"),
                                                    _vm._v(" "),
                                                    _c(
                                                      "td",
                                                      [
                                                        _c(
                                                          "center",
                                                          [
                                                            _c("vue-numeric", {
                                                              attrs: {
                                                                currency:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "currency"
                                                                  ],
                                                                "currency-symbol-position":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "currency-symbol-position"
                                                                  ],
                                                                "output-type":
                                                                  "String",
                                                                "empty-value":
                                                                  "0",
                                                                precision:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "precision"
                                                                  ],
                                                                "decimal-separator":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "decimal-separator"
                                                                  ],
                                                                "thousand-separator":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "thousand-separator"
                                                                  ],
                                                                "read-only-class":
                                                                  "item-total",
                                                                min:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "min"
                                                                  ],
                                                                "read-only": ""
                                                              },
                                                              model: {
                                                                value:
                                                                  _vm
                                                                    .totalAchatMpDepVariable
                                                                    .annee3,
                                                                callback: function(
                                                                  $$v
                                                                ) {
                                                                  _vm.$set(
                                                                    _vm.totalAchatMpDepVariable,
                                                                    "annee3",
                                                                    _vm._n($$v)
                                                                  )
                                                                },
                                                                expression:
                                                                  "totalAchatMpDepVariable.annee3"
                                                              }
                                                            })
                                                          ],
                                                          1
                                                        )
                                                      ],
                                                      1
                                                    )
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _vm._l(
                                                  _vm.bpfinancier[
                                                    "matieres-premieres"
                                                  ]["depenses-produits"],
                                                  function(depense, index) {
                                                    return _c(
                                                      "tr",
                                                      {
                                                        key:
                                                          "depenseMP-" + index
                                                      },
                                                      [
                                                        _c("td", [
                                                          _vm._v(
                                                            "\n                                                                            " +
                                                              _vm._s(
                                                                depense.titre
                                                              ) +
                                                              "\n                                                                        "
                                                          )
                                                        ]),
                                                        _vm._v(" "),
                                                        _c(
                                                          "td",
                                                          {
                                                            staticClass:
                                                              "text-center"
                                                          },
                                                          [
                                                            _c("span", [
                                                              _vm._v(
                                                                _vm._s(
                                                                  depense.quantite
                                                                )
                                                              )
                                                            ])
                                                          ]
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "td",
                                                          {
                                                            staticClass:
                                                              "text-center"
                                                          },
                                                          [
                                                            _c("vue-numeric", {
                                                              attrs: {
                                                                currency:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "currency"
                                                                  ],
                                                                "currency-symbol-position":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "currency-symbol-position"
                                                                  ],
                                                                max:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "max"
                                                                  ],
                                                                min:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "min"
                                                                  ],
                                                                minus:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "minus"
                                                                  ],
                                                                "output-type":
                                                                  "currentCurrencyMask['output-type']",
                                                                "empty-value":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "empty-value"
                                                                  ],
                                                                "read-only": "",
                                                                precision:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "precision"
                                                                  ],
                                                                "decimal-separator":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "decimal-separator"
                                                                  ],
                                                                "thousand-separator":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "thousand-separator"
                                                                  ]
                                                              },
                                                              model: {
                                                                value:
                                                                  depense.prix,
                                                                callback: function(
                                                                  $$v
                                                                ) {
                                                                  _vm.$set(
                                                                    depense,
                                                                    "prix",
                                                                    _vm._n($$v)
                                                                  )
                                                                },
                                                                expression:
                                                                  "depense.prix"
                                                              }
                                                            })
                                                          ],
                                                          1
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "td",
                                                          [
                                                            _c(
                                                              "center",
                                                              [
                                                                _c(
                                                                  "vue-numeric",
                                                                  {
                                                                    attrs: {
                                                                      currency:
                                                                        _vm
                                                                          .currentCurrencyMask[
                                                                          "currency"
                                                                        ],
                                                                      "currency-symbol-position":
                                                                        _vm
                                                                          .currentCurrencyMask[
                                                                          "currency-symbol-position"
                                                                        ],
                                                                      "output-type":
                                                                        "String",
                                                                      "empty-value":
                                                                        "0",
                                                                      precision:
                                                                        _vm
                                                                          .currentCurrencyMask[
                                                                          "precision"
                                                                        ],
                                                                      "decimal-separator":
                                                                        _vm
                                                                          .currentCurrencyMask[
                                                                          "decimal-separator"
                                                                        ],
                                                                      "thousand-separator":
                                                                        _vm
                                                                          .currentCurrencyMask[
                                                                          "thousand-separator"
                                                                        ],
                                                                      "read-only-class":
                                                                        "item-total",
                                                                      min:
                                                                        _vm
                                                                          .currentCurrencyMask[
                                                                          "min"
                                                                        ],
                                                                      "read-only":
                                                                        ""
                                                                    },
                                                                    model: {
                                                                      value:
                                                                        depense.annee1,
                                                                      callback: function(
                                                                        $$v
                                                                      ) {
                                                                        _vm.$set(
                                                                          depense,
                                                                          "annee1",
                                                                          _vm._n(
                                                                            $$v
                                                                          )
                                                                        )
                                                                      },
                                                                      expression:
                                                                        "depense.annee1"
                                                                    }
                                                                  }
                                                                )
                                                              ],
                                                              1
                                                            )
                                                          ],
                                                          1
                                                        ),
                                                        _vm._v(" "),
                                                        _c("td", [
                                                          _c("input", {
                                                            directives: [
                                                              {
                                                                name: "model",
                                                                rawName:
                                                                  "v-model",
                                                                value:
                                                                  depense.commentaire,
                                                                expression:
                                                                  "depense.commentaire"
                                                              }
                                                            ],
                                                            staticClass:
                                                              "form-control",
                                                            attrs: {
                                                              type: "text"
                                                            },
                                                            domProps: {
                                                              value:
                                                                depense.commentaire
                                                            },
                                                            on: {
                                                              input: function(
                                                                $event
                                                              ) {
                                                                if (
                                                                  $event.target
                                                                    .composing
                                                                ) {
                                                                  return
                                                                }
                                                                _vm.$set(
                                                                  depense,
                                                                  "commentaire",
                                                                  $event.target
                                                                    .value
                                                                )
                                                              }
                                                            }
                                                          })
                                                        ]),
                                                        _vm._v(" "),
                                                        _c(
                                                          "td",
                                                          {
                                                            staticClass:
                                                              "text-center"
                                                          },
                                                          [
                                                            _c("vue-numeric", {
                                                              attrs: {
                                                                currency: "%",
                                                                "currency-symbol-position":
                                                                  "suffix",
                                                                max: 200,
                                                                min: 0,
                                                                minus: false,
                                                                "empty-value":
                                                                  "0",
                                                                precision: 1,
                                                                "read-only": ""
                                                              },
                                                              on: {
                                                                input: function(
                                                                  $event
                                                                ) {
                                                                  return _vm.accroissementPrevisionnelAnnee2(
                                                                    depense,
                                                                    "achat.depense",
                                                                    _vm
                                                                      .bpfinancier[
                                                                      "matieres-premieres"
                                                                    ][
                                                                      "depenses-produits"
                                                                    ]
                                                                  )
                                                                }
                                                              },
                                                              model: {
                                                                value:
                                                                  depense.accroissement1,
                                                                callback: function(
                                                                  $$v
                                                                ) {
                                                                  _vm.$set(
                                                                    depense,
                                                                    "accroissement1",
                                                                    _vm._n($$v)
                                                                  )
                                                                },
                                                                expression:
                                                                  "depense.accroissement1"
                                                              }
                                                            })
                                                          ],
                                                          1
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "td",
                                                          [
                                                            _c(
                                                              "center",
                                                              [
                                                                _c(
                                                                  "vue-numeric",
                                                                  {
                                                                    attrs: {
                                                                      currency:
                                                                        _vm
                                                                          .currentCurrencyMask[
                                                                          "currency"
                                                                        ],
                                                                      "currency-symbol-position":
                                                                        _vm
                                                                          .currentCurrencyMask[
                                                                          "currency-symbol-position"
                                                                        ],
                                                                      "output-type":
                                                                        "String",
                                                                      "empty-value":
                                                                        "0",
                                                                      precision:
                                                                        _vm
                                                                          .currentCurrencyMask[
                                                                          "precision"
                                                                        ],
                                                                      "decimal-separator":
                                                                        _vm
                                                                          .currentCurrencyMask[
                                                                          "decimal-separator"
                                                                        ],
                                                                      "thousand-separator":
                                                                        _vm
                                                                          .currentCurrencyMask[
                                                                          "thousand-separator"
                                                                        ],
                                                                      "read-only-class":
                                                                        "item-total",
                                                                      min:
                                                                        _vm
                                                                          .currentCurrencyMask[
                                                                          "min"
                                                                        ],
                                                                      "read-only":
                                                                        ""
                                                                    },
                                                                    model: {
                                                                      value:
                                                                        depense.annee2,
                                                                      callback: function(
                                                                        $$v
                                                                      ) {
                                                                        _vm.$set(
                                                                          depense,
                                                                          "annee2",
                                                                          _vm._n(
                                                                            $$v
                                                                          )
                                                                        )
                                                                      },
                                                                      expression:
                                                                        "depense.annee2"
                                                                    }
                                                                  }
                                                                )
                                                              ],
                                                              1
                                                            )
                                                          ],
                                                          1
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "td",
                                                          {
                                                            staticClass:
                                                              "text-center"
                                                          },
                                                          [
                                                            _c("vue-numeric", {
                                                              attrs: {
                                                                currency: "%",
                                                                "currency-symbol-position":
                                                                  "suffix",
                                                                max: 200,
                                                                min: 0,
                                                                minus: false,
                                                                "empty-value":
                                                                  "0",
                                                                precision: 1,
                                                                "read-only": ""
                                                              },
                                                              on: {
                                                                input: function(
                                                                  $event
                                                                ) {
                                                                  return _vm.accroissementPrevisionnelAnnee3(
                                                                    depense,
                                                                    "achat.depense",
                                                                    _vm
                                                                      .bpfinancier[
                                                                      "matieres-premieres"
                                                                    ][
                                                                      "depenses-produits"
                                                                    ]
                                                                  )
                                                                }
                                                              },
                                                              model: {
                                                                value:
                                                                  depense.accroissement2,
                                                                callback: function(
                                                                  $$v
                                                                ) {
                                                                  _vm.$set(
                                                                    depense,
                                                                    "accroissement2",
                                                                    _vm._n($$v)
                                                                  )
                                                                },
                                                                expression:
                                                                  "depense.accroissement2"
                                                              }
                                                            })
                                                          ],
                                                          1
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "td",
                                                          [
                                                            _c(
                                                              "center",
                                                              [
                                                                _c(
                                                                  "vue-numeric",
                                                                  {
                                                                    attrs: {
                                                                      currency:
                                                                        _vm
                                                                          .currentCurrencyMask[
                                                                          "currency"
                                                                        ],
                                                                      "currency-symbol-position":
                                                                        _vm
                                                                          .currentCurrencyMask[
                                                                          "currency-symbol-position"
                                                                        ],
                                                                      "output-type":
                                                                        "String",
                                                                      "empty-value":
                                                                        "0",
                                                                      precision:
                                                                        _vm
                                                                          .currentCurrencyMask[
                                                                          "precision"
                                                                        ],
                                                                      "decimal-separator":
                                                                        _vm
                                                                          .currentCurrencyMask[
                                                                          "decimal-separator"
                                                                        ],
                                                                      "thousand-separator":
                                                                        _vm
                                                                          .currentCurrencyMask[
                                                                          "thousand-separator"
                                                                        ],
                                                                      "read-only-class":
                                                                        "item-total",
                                                                      min:
                                                                        _vm
                                                                          .currentCurrencyMask[
                                                                          "min"
                                                                        ],
                                                                      "read-only":
                                                                        ""
                                                                    },
                                                                    model: {
                                                                      value:
                                                                        depense.annee3,
                                                                      callback: function(
                                                                        $$v
                                                                      ) {
                                                                        _vm.$set(
                                                                          depense,
                                                                          "annee3",
                                                                          _vm._n(
                                                                            $$v
                                                                          )
                                                                        )
                                                                      },
                                                                      expression:
                                                                        "depense.annee3"
                                                                    }
                                                                  }
                                                                )
                                                              ],
                                                              1
                                                            )
                                                          ],
                                                          1
                                                        )
                                                      ]
                                                    )
                                                  }
                                                )
                                              ],
                                              2
                                            )
                                          ]
                                        )
                                      ]
                                    )
                                  ])
                                ])
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "tab-content",
                              {
                                attrs: {
                                  title: "2. Services Extérieurs",
                                  icon: "ti-settings"
                                }
                              },
                              [
                                _c("div", { staticClass: "table-responsive" }, [
                                  _c(
                                    "table",
                                    {
                                      staticClass:
                                        "table table-bordered table-1 m-b-0 color-bordered-table info-bordered-table",
                                      attrs: { width: "100%" }
                                    },
                                    [
                                      _c("col", {
                                        staticStyle: { width: "1rem" }
                                      }),
                                      _vm._v(" "),
                                      _c("col", {
                                        staticStyle: { width: "20%" }
                                      }),
                                      _vm._v(" "),
                                      _c("col", {
                                        staticStyle: { width: "6%" }
                                      }),
                                      _vm._v(" "),
                                      _c("col", {}),
                                      _vm._v(" "),
                                      _c("col", {}),
                                      _vm._v(" "),
                                      _c("col", {
                                        staticStyle: { width: "15%" }
                                      }),
                                      _vm._v(" "),
                                      _c("col", {
                                        staticStyle: { width: "6%" }
                                      }),
                                      _vm._v(" "),
                                      _c("col", {}),
                                      _vm._v(" "),
                                      _c("col", {
                                        staticStyle: { width: "6%" }
                                      }),
                                      _vm._v(" "),
                                      _c("col"),
                                      _vm._v(" "),
                                      _c("thead", [
                                        _c("tr", [
                                          _c("th", {
                                            staticClass:
                                              "table-head-first-red-column"
                                          }),
                                          _vm._v(" "),
                                          _c(
                                            "th",
                                            {
                                              staticClass:
                                                "table-head-first-red-column"
                                            },
                                            [_vm._v("Services Extérieurs")]
                                          ),
                                          _vm._v(" "),
                                          _c("th", [_vm._v("Quantité")]),
                                          _vm._v(" "),
                                          _c("th", [_vm._v("Coût unitaire")]),
                                          _vm._v(" "),
                                          _c("th", [_vm._v("Année-1")]),
                                          _vm._v(" "),
                                          _c("th", [_vm._v("Commentaires")]),
                                          _vm._v(" "),
                                          _c("th", [
                                            _vm._v("% "),
                                            _c("i", {
                                              staticClass: "fas fa-caret-up"
                                            })
                                          ]),
                                          _vm._v(" "),
                                          _c("th", [_vm._v("Année-2")]),
                                          _vm._v(" "),
                                          _c("th", [
                                            _vm._v("% "),
                                            _c("i", {
                                              staticClass: "fas fa-caret-up"
                                            })
                                          ]),
                                          _vm._v(" "),
                                          _c("th", [_vm._v("Année-3")])
                                        ])
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "tbody",
                                        [
                                          _c("tr", [
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              {
                                                staticClass:
                                                  "empty-numeric-placeholder"
                                              },
                                              [_vm._v("          ")]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              {
                                                staticClass:
                                                  "empty-numeric-placeholder"
                                              },
                                              [_vm._v("           ")]
                                            ),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              {
                                                staticClass:
                                                  "empty-numeric-placeholder"
                                              },
                                              [_vm._v("           ")]
                                            ),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              {
                                                staticClass:
                                                  "empty-numeric-placeholder"
                                              },
                                              [_vm._v("           ")]
                                            )
                                          ]),
                                          _vm._v(" "),
                                          _c(
                                            "tr",
                                            { staticClass: "table-total" },
                                            [
                                              _c(
                                                "td",
                                                { attrs: { colspan: "2" } },
                                                [
                                                  _vm._v(
                                                    " Total Services Extérieurs"
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm
                                                              .totalServExtFrais
                                                              .annee1,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.totalServExtFrais,
                                                              "annee1",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "totalServExtFrais.annee1"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm
                                                              .totalServExtFrais
                                                              .annee2,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.totalServExtFrais,
                                                              "annee2",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "totalServExtFrais.annee2"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm
                                                              .totalServExtFrais
                                                              .annee3,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.totalServExtFrais,
                                                              "annee3",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "totalServExtFrais.annee3"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c("tr", [
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td")
                                          ]),
                                          _vm._v(" "),
                                          _c(
                                            "tr",
                                            {
                                              staticClass: "table-group-header"
                                            },
                                            [
                                              _c(
                                                "td",
                                                { attrs: { colspan: "2" } },
                                                [_vm._v(" Frais Généraux")]
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm
                                                              .totalServExtFrais
                                                              .annee1,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.totalServExtFrais,
                                                              "annee1",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "totalServExtFrais.annee1"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm
                                                              .totalServExtFrais
                                                              .annee2,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.totalServExtFrais,
                                                              "annee2",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "totalServExtFrais.annee2"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm
                                                              .totalServExtFrais
                                                              .annee3,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.totalServExtFrais,
                                                              "annee3",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "totalServExtFrais.annee3"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _vm._l(
                                            _vm.bpfinancier[
                                              "services-exterieurs"
                                            ],
                                            function(service, index) {
                                              return _c(
                                                "tr",
                                                { key: "service-" + index },
                                                [
                                                  _c("td", [
                                                    _c(
                                                      "button",
                                                      {
                                                        staticClass:
                                                          "btn btn-sm btn-danger",
                                                        attrs: {
                                                          type: "button"
                                                        },
                                                        on: {
                                                          click: function(
                                                            $event
                                                          ) {
                                                            return _vm.deleteRow(
                                                              "services-exterieurs",
                                                              service
                                                            )
                                                          }
                                                        }
                                                      },
                                                      [
                                                        _c("i", {
                                                          staticClass:
                                                            "fas fa-minus-circle"
                                                        })
                                                      ]
                                                    )
                                                  ]),
                                                  _vm._v(" "),
                                                  _c("td", [
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName: "v-model",
                                                          value: service.titre,
                                                          expression:
                                                            "service.titre"
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control text-dark",
                                                      attrs: { type: "text" },
                                                      domProps: {
                                                        value: service.titre
                                                      },
                                                      on: {
                                                        input: function(
                                                          $event
                                                        ) {
                                                          if (
                                                            $event.target
                                                              .composing
                                                          ) {
                                                            return
                                                          }
                                                          _vm.$set(
                                                            service,
                                                            "titre",
                                                            $event.target.value
                                                          )
                                                        }
                                                      }
                                                    })
                                                  ]),
                                                  _vm._v(" "),
                                                  _c("td", [
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName:
                                                            "v-model.number",
                                                          value:
                                                            service.quantite,
                                                          expression:
                                                            "service.quantite",
                                                          modifiers: {
                                                            number: true
                                                          }
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control text-center",
                                                      attrs: {
                                                        type: "number",
                                                        min: "1"
                                                      },
                                                      domProps: {
                                                        value: service.quantite
                                                      },
                                                      on: {
                                                        change: function(
                                                          $event
                                                        ) {
                                                          return _vm.totalInitial(
                                                            service,
                                                            "service.frais",
                                                            _vm.bpfinancier[
                                                              "services-exterieurs"
                                                            ]
                                                          )
                                                        },
                                                        input: function(
                                                          $event
                                                        ) {
                                                          if (
                                                            $event.target
                                                              .composing
                                                          ) {
                                                            return
                                                          }
                                                          _vm.$set(
                                                            service,
                                                            "quantite",
                                                            _vm._n(
                                                              $event.target
                                                                .value
                                                            )
                                                          )
                                                        },
                                                        blur: function($event) {
                                                          return _vm.$forceUpdate()
                                                        }
                                                      }
                                                    })
                                                  ]),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c("vue-numeric", {
                                                        staticClass:
                                                          "form-control text-center",
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          max:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "max"
                                                            ],
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          minus:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "minus"
                                                            ],
                                                          "output-type":
                                                            "currentCurrencyMask['output-type']",
                                                          "empty-value":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "empty-value"
                                                            ],
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ]
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            return _vm.totalInitial(
                                                              service,
                                                              "service.frais",
                                                              _vm.bpfinancier[
                                                                "services-exterieurs"
                                                              ]
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value: service.prix,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              service,
                                                              "prix",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "service.prix"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c(
                                                        "center",
                                                        [
                                                          _c("vue-numeric", {
                                                            attrs: {
                                                              currency:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency"
                                                                ],
                                                              "currency-symbol-position":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency-symbol-position"
                                                                ],
                                                              "output-type":
                                                                "String",
                                                              "empty-value":
                                                                "0",
                                                              precision:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "precision"
                                                                ],
                                                              "decimal-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "decimal-separator"
                                                                ],
                                                              "thousand-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "thousand-separator"
                                                                ],
                                                              "read-only-class":
                                                                "item-total",
                                                              min:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "min"
                                                                ],
                                                              "read-only": ""
                                                            },
                                                            model: {
                                                              value:
                                                                service.annee1,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  service,
                                                                  "annee1",
                                                                  _vm._n($$v)
                                                                )
                                                              },
                                                              expression:
                                                                "service.annee1"
                                                            }
                                                          })
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c("td", [
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName: "v-model",
                                                          value:
                                                            service.commentaire,
                                                          expression:
                                                            "service.commentaire"
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control",
                                                      attrs: { type: "text" },
                                                      domProps: {
                                                        value:
                                                          service.commentaire
                                                      },
                                                      on: {
                                                        input: function(
                                                          $event
                                                        ) {
                                                          if (
                                                            $event.target
                                                              .composing
                                                          ) {
                                                            return
                                                          }
                                                          _vm.$set(
                                                            service,
                                                            "commentaire",
                                                            $event.target.value
                                                          )
                                                        }
                                                      }
                                                    })
                                                  ]),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c("vue-numeric", {
                                                        staticClass:
                                                          "form-control text-center",
                                                        attrs: {
                                                          currency: "%",
                                                          "currency-symbol-position":
                                                            "suffix",
                                                          max: 200,
                                                          min: 0,
                                                          minus: false,
                                                          "empty-value": "0",
                                                          precision: 1
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            return _vm.accroissementPrevisionnelAnnee2(
                                                              service,
                                                              "service.frais",
                                                              _vm.bpfinancier[
                                                                "services-exterieurs"
                                                              ]
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value:
                                                            service.accroissement1,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              service,
                                                              "accroissement1",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "service.accroissement1"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c(
                                                        "center",
                                                        [
                                                          _c("vue-numeric", {
                                                            attrs: {
                                                              currency:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency"
                                                                ],
                                                              "currency-symbol-position":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency-symbol-position"
                                                                ],
                                                              "output-type":
                                                                "String",
                                                              "empty-value":
                                                                "0",
                                                              precision:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "precision"
                                                                ],
                                                              "decimal-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "decimal-separator"
                                                                ],
                                                              "thousand-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "thousand-separator"
                                                                ],
                                                              "read-only-class":
                                                                "item-total",
                                                              min:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "min"
                                                                ],
                                                              "read-only": ""
                                                            },
                                                            model: {
                                                              value:
                                                                service.annee2,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  service,
                                                                  "annee2",
                                                                  _vm._n($$v)
                                                                )
                                                              },
                                                              expression:
                                                                "service.annee2"
                                                            }
                                                          })
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c("vue-numeric", {
                                                        staticClass:
                                                          "form-control text-center",
                                                        attrs: {
                                                          currency: "%",
                                                          "currency-symbol-position":
                                                            "suffix",
                                                          max: 200,
                                                          min: 0,
                                                          minus: false,
                                                          "empty-value": "0",
                                                          precision: 1
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            return _vm.accroissementPrevisionnelAnnee3(
                                                              service,
                                                              "service.frais",
                                                              _vm.bpfinancier[
                                                                "services-exterieurs"
                                                              ]
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value:
                                                            service.accroissement2,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              service,
                                                              "accroissement2",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "service.accroissement2"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c(
                                                        "center",
                                                        [
                                                          _c("vue-numeric", {
                                                            attrs: {
                                                              currency:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency"
                                                                ],
                                                              "currency-symbol-position":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency-symbol-position"
                                                                ],
                                                              "output-type":
                                                                "String",
                                                              "empty-value":
                                                                "0",
                                                              precision:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "precision"
                                                                ],
                                                              "decimal-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "decimal-separator"
                                                                ],
                                                              "thousand-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "thousand-separator"
                                                                ],
                                                              "read-only-class":
                                                                "item-total",
                                                              min:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "min"
                                                                ],
                                                              "read-only": ""
                                                            },
                                                            model: {
                                                              value:
                                                                service.annee3,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  service,
                                                                  "annee3",
                                                                  _vm._n($$v)
                                                                )
                                                              },
                                                              expression:
                                                                "service.annee3"
                                                            }
                                                          })
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  )
                                                ]
                                              )
                                            }
                                          ),
                                          _vm._v(" "),
                                          _c("tr", [
                                            _c(
                                              "td",
                                              { attrs: { colspan: "10" } },
                                              [
                                                _c(
                                                  "button",
                                                  {
                                                    staticClass:
                                                      "btn btn-sm btn-info",
                                                    attrs: { type: "button" },
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.addNewRow(
                                                          "services-exterieurs"
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _c("i", {
                                                      staticClass:
                                                        "fas fa-plus-circle"
                                                    })
                                                  ]
                                                )
                                              ]
                                            )
                                          ])
                                        ],
                                        2
                                      )
                                    ]
                                  )
                                ])
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "tab-content",
                              {
                                attrs: {
                                  title: "3. Taxes et Impôts",
                                  icon: "ti-check"
                                }
                              },
                              [
                                _c("div", { staticClass: "table-responsive" }, [
                                  _c(
                                    "table",
                                    {
                                      staticClass:
                                        "table table-bordered table-1 m-b-0 color-bordered-table info-bordered-table",
                                      attrs: { width: "100%" }
                                    },
                                    [
                                      _c("col", {
                                        staticStyle: { width: "1rem" }
                                      }),
                                      _vm._v(" "),
                                      _c("col", {
                                        staticStyle: { width: "20%" }
                                      }),
                                      _vm._v(" "),
                                      _c("col", {
                                        staticStyle: { width: "5%" }
                                      }),
                                      _vm._v(" "),
                                      _c("col"),
                                      _vm._v(" "),
                                      _c("col"),
                                      _vm._v(" "),
                                      _c("col", {
                                        staticStyle: { width: "6%" }
                                      }),
                                      _vm._v(" "),
                                      _c("col"),
                                      _vm._v(" "),
                                      _c("col", {
                                        staticStyle: { width: "6%" }
                                      }),
                                      _vm._v(" "),
                                      _c("col"),
                                      _vm._v(" "),
                                      _c("thead", [
                                        _c("tr", [
                                          _c("th", {
                                            staticClass:
                                              "table-head-first-red-column"
                                          }),
                                          _vm._v(" "),
                                          _c(
                                            "th",
                                            {
                                              staticClass:
                                                "table-head-first-red-column"
                                            },
                                            [_vm._v("Taxes et Impôts")]
                                          ),
                                          _vm._v(" "),
                                          _c("th", [_vm._v("Quantité")]),
                                          _vm._v(" "),
                                          _c("th", [_vm._v("Coût unitaire")]),
                                          _vm._v(" "),
                                          _c("th", [_vm._v("Année-1")]),
                                          _vm._v(" "),
                                          _c("th", [
                                            _vm._v("% "),
                                            _c("i", {
                                              staticClass: "fas fa-caret-up"
                                            })
                                          ]),
                                          _vm._v(" "),
                                          _c("th", [_vm._v("Année-2")]),
                                          _vm._v(" "),
                                          _c("th", [
                                            _vm._v("% "),
                                            _c("i", {
                                              staticClass: "fas fa-caret-up"
                                            })
                                          ]),
                                          _vm._v(" "),
                                          _c("th", [_vm._v("Année-3")])
                                        ])
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "tbody",
                                        [
                                          _c("tr", [
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              {
                                                staticClass:
                                                  "empty-numeric-placeholder"
                                              },
                                              [_vm._v("          ")]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              {
                                                staticClass:
                                                  "empty-numeric-placeholder"
                                              },
                                              [_vm._v("           ")]
                                            ),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              {
                                                staticClass:
                                                  "empty-numeric-placeholder"
                                              },
                                              [_vm._v("           ")]
                                            ),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              {
                                                staticClass:
                                                  "empty-numeric-placeholder"
                                              },
                                              [_vm._v("           ")]
                                            )
                                          ]),
                                          _vm._v(" "),
                                          _c(
                                            "tr",
                                            { staticClass: "table-total" },
                                            [
                                              _c(
                                                "td",
                                                { attrs: { colspan: "2" } },
                                                [
                                                  _vm._v(
                                                    " Total Taxes et Impôts"
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm.totalTaxeImpot
                                                              .annee1,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.totalTaxeImpot,
                                                              "annee1",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "totalTaxeImpot.annee1"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm.totalTaxeImpot
                                                              .annee2,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.totalTaxeImpot,
                                                              "annee2",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "totalTaxeImpot.annee2"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm.totalTaxeImpot
                                                              .annee3,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.totalTaxeImpot,
                                                              "annee3",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "totalTaxeImpot.annee3"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c("tr", [
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td")
                                          ]),
                                          _vm._v(" "),
                                          _c(
                                            "tr",
                                            {
                                              staticClass: "table-group-header"
                                            },
                                            [
                                              _c(
                                                "td",
                                                { attrs: { colspan: "2" } },
                                                [_vm._v("Taxes aux Impôts")]
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm
                                                              .subTotalTaxImpImpot
                                                              .annee1,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.subTotalTaxImpImpot,
                                                              "annee1",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "subTotalTaxImpImpot.annee1"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm
                                                              .subTotalTaxImpImpot
                                                              .annee2,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.subTotalTaxImpImpot,
                                                              "annee2",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "subTotalTaxImpImpot.annee2"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm
                                                              .subTotalTaxImpImpot
                                                              .annee3,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.subTotalTaxImpImpot,
                                                              "annee3",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "subTotalTaxImpImpot.annee3"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _vm._l(
                                            _vm.bpfinancier["taxes-impots"][
                                              "impots"
                                            ],
                                            function(impot, index) {
                                              return _c(
                                                "tr",
                                                { key: "impot-" + index },
                                                [
                                                  _c("td", [
                                                    _c(
                                                      "button",
                                                      {
                                                        staticClass:
                                                          "btn btn-sm btn-danger",
                                                        attrs: {
                                                          type: "button"
                                                        },
                                                        on: {
                                                          click: function(
                                                            $event
                                                          ) {
                                                            return _vm.deleteRow(
                                                              "taxes-impots.impots",
                                                              impot
                                                            )
                                                          }
                                                        }
                                                      },
                                                      [
                                                        _c("i", {
                                                          staticClass:
                                                            "fas fa-minus-circle"
                                                        })
                                                      ]
                                                    )
                                                  ]),
                                                  _vm._v(" "),
                                                  _c("td", [
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName: "v-model",
                                                          value: impot.titre,
                                                          expression:
                                                            "impot.titre"
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control text-dark",
                                                      attrs: { type: "text" },
                                                      domProps: {
                                                        value: impot.titre
                                                      },
                                                      on: {
                                                        input: function(
                                                          $event
                                                        ) {
                                                          if (
                                                            $event.target
                                                              .composing
                                                          ) {
                                                            return
                                                          }
                                                          _vm.$set(
                                                            impot,
                                                            "titre",
                                                            $event.target.value
                                                          )
                                                        }
                                                      }
                                                    })
                                                  ]),
                                                  _vm._v(" "),
                                                  _c("td", [
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName:
                                                            "v-model.number",
                                                          value: impot.quantite,
                                                          expression:
                                                            "impot.quantite",
                                                          modifiers: {
                                                            number: true
                                                          }
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control text-center",
                                                      attrs: {
                                                        type: "number",
                                                        min: "1"
                                                      },
                                                      domProps: {
                                                        value: impot.quantite
                                                      },
                                                      on: {
                                                        change: function(
                                                          $event
                                                        ) {
                                                          return _vm.totalInitial(
                                                            impot,
                                                            "taxe.impot",
                                                            _vm.bpfinancier[
                                                              "taxes-impots"
                                                            ]["impots"]
                                                          )
                                                        },
                                                        input: function(
                                                          $event
                                                        ) {
                                                          if (
                                                            $event.target
                                                              .composing
                                                          ) {
                                                            return
                                                          }
                                                          _vm.$set(
                                                            impot,
                                                            "quantite",
                                                            _vm._n(
                                                              $event.target
                                                                .value
                                                            )
                                                          )
                                                        },
                                                        blur: function($event) {
                                                          return _vm.$forceUpdate()
                                                        }
                                                      }
                                                    })
                                                  ]),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c("vue-numeric", {
                                                        staticClass:
                                                          "form-control text-center",
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          max:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "max"
                                                            ],
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          minus:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "minus"
                                                            ],
                                                          "output-type":
                                                            "currentCurrencyMask['output-type']",
                                                          "empty-value":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "empty-value"
                                                            ],
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ]
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            return _vm.totalInitial(
                                                              impot,
                                                              "taxe.impot",
                                                              _vm.bpfinancier[
                                                                "taxes-impots"
                                                              ]["impots"]
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value: impot.prix,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              impot,
                                                              "prix",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "impot.prix"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c(
                                                        "center",
                                                        [
                                                          _c("vue-numeric", {
                                                            attrs: {
                                                              currency:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency"
                                                                ],
                                                              "currency-symbol-position":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency-symbol-position"
                                                                ],
                                                              "output-type":
                                                                "String",
                                                              "empty-value":
                                                                "0",
                                                              precision:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "precision"
                                                                ],
                                                              "decimal-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "decimal-separator"
                                                                ],
                                                              "thousand-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "thousand-separator"
                                                                ],
                                                              "read-only-class":
                                                                "item-total",
                                                              min:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "min"
                                                                ],
                                                              "read-only": ""
                                                            },
                                                            model: {
                                                              value:
                                                                impot.annee1,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  impot,
                                                                  "annee1",
                                                                  _vm._n($$v)
                                                                )
                                                              },
                                                              expression:
                                                                "impot.annee1"
                                                            }
                                                          })
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c("vue-numeric", {
                                                        staticClass:
                                                          "form-control text-center",
                                                        attrs: {
                                                          currency: "%",
                                                          "currency-symbol-position":
                                                            "suffix",
                                                          max: 200,
                                                          min: 0,
                                                          minus: false,
                                                          "empty-value": "0",
                                                          precision: 1
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            return _vm.accroissementPrevisionnelAnnee2(
                                                              impot,
                                                              "taxe.impot",
                                                              _vm.bpfinancier[
                                                                "taxes-impots"
                                                              ]["impots"]
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value:
                                                            impot.accroissement1,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              impot,
                                                              "accroissement1",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "impot.accroissement1"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c(
                                                        "center",
                                                        [
                                                          _c("vue-numeric", {
                                                            attrs: {
                                                              currency:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency"
                                                                ],
                                                              "currency-symbol-position":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency-symbol-position"
                                                                ],
                                                              "output-type":
                                                                "String",
                                                              "empty-value":
                                                                "0",
                                                              precision:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "precision"
                                                                ],
                                                              "decimal-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "decimal-separator"
                                                                ],
                                                              "thousand-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "thousand-separator"
                                                                ],
                                                              "read-only-class":
                                                                "item-total",
                                                              min:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "min"
                                                                ],
                                                              "read-only": ""
                                                            },
                                                            model: {
                                                              value:
                                                                impot.annee2,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  impot,
                                                                  "annee2",
                                                                  _vm._n($$v)
                                                                )
                                                              },
                                                              expression:
                                                                "impot.annee2"
                                                            }
                                                          })
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c("vue-numeric", {
                                                        staticClass:
                                                          "form-control text-center",
                                                        attrs: {
                                                          currency: "%",
                                                          "currency-symbol-position":
                                                            "suffix",
                                                          max: 200,
                                                          min: 0,
                                                          minus: false,
                                                          "empty-value": "0",
                                                          precision: 1
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            return _vm.accroissementPrevisionnelAnnee3(
                                                              impot,
                                                              "taxe.impot",
                                                              _vm.bpfinancier[
                                                                "taxes-impots"
                                                              ]["impots"]
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value:
                                                            impot.accroissement2,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              impot,
                                                              "accroissement2",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "impot.accroissement2"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c(
                                                        "center",
                                                        [
                                                          _c("vue-numeric", {
                                                            attrs: {
                                                              currency:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency"
                                                                ],
                                                              "currency-symbol-position":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency-symbol-position"
                                                                ],
                                                              "output-type":
                                                                "String",
                                                              "empty-value":
                                                                "0",
                                                              precision:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "precision"
                                                                ],
                                                              "decimal-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "decimal-separator"
                                                                ],
                                                              "thousand-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "thousand-separator"
                                                                ],
                                                              "read-only-class":
                                                                "item-total",
                                                              min:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "min"
                                                                ],
                                                              "read-only": ""
                                                            },
                                                            model: {
                                                              value:
                                                                impot.annee3,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  impot,
                                                                  "annee3",
                                                                  _vm._n($$v)
                                                                )
                                                              },
                                                              expression:
                                                                "impot.annee3"
                                                            }
                                                          })
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  )
                                                ]
                                              )
                                            }
                                          ),
                                          _vm._v(" "),
                                          _c("tr", [
                                            _c(
                                              "td",
                                              { attrs: { colspan: "9" } },
                                              [
                                                _c(
                                                  "button",
                                                  {
                                                    staticClass:
                                                      "btn btn-sm btn-info",
                                                    attrs: { type: "button" },
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.addNewRow(
                                                          "taxes-impots.impots"
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _c("i", {
                                                      staticClass:
                                                        "fas fa-plus-circle"
                                                    })
                                                  ]
                                                )
                                              ]
                                            )
                                          ]),
                                          _vm._v(" "),
                                          _c("tr", [
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td")
                                          ]),
                                          _vm._v(" "),
                                          _c(
                                            "tr",
                                            {
                                              staticClass: "table-group-header"
                                            },
                                            [
                                              _c(
                                                "td",
                                                { attrs: { colspan: "2" } },
                                                [_vm._v("Taxes de la Mairie")]
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm
                                                              .subTotalTaxImpMairie
                                                              .annee1,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.subTotalTaxImpMairie,
                                                              "annee1",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "subTotalTaxImpMairie.annee1"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm
                                                              .subTotalTaxImpMairie
                                                              .annee2,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.subTotalTaxImpMairie,
                                                              "annee2",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "subTotalTaxImpMairie.annee2"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm
                                                              .subTotalTaxImpMairie
                                                              .annee3,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.subTotalTaxImpMairie,
                                                              "annee3",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "subTotalTaxImpMairie.annee3"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _vm._l(
                                            _vm.bpfinancier["taxes-impots"][
                                              "mairie"
                                            ],
                                            function(impot, index) {
                                              return _c(
                                                "tr",
                                                { key: "mairie-" + index },
                                                [
                                                  _c("td", [
                                                    _c(
                                                      "button",
                                                      {
                                                        staticClass:
                                                          "btn btn-sm btn-danger",
                                                        attrs: {
                                                          type: "button"
                                                        },
                                                        on: {
                                                          click: function(
                                                            $event
                                                          ) {
                                                            return _vm.deleteRow(
                                                              "taxes-impots.mairie",
                                                              impot
                                                            )
                                                          }
                                                        }
                                                      },
                                                      [
                                                        _c("i", {
                                                          staticClass:
                                                            "fas fa-minus-circle"
                                                        })
                                                      ]
                                                    )
                                                  ]),
                                                  _vm._v(" "),
                                                  _c("td", [
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName: "v-model",
                                                          value: impot.titre,
                                                          expression:
                                                            "impot.titre"
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control text-dark",
                                                      attrs: { type: "text" },
                                                      domProps: {
                                                        value: impot.titre
                                                      },
                                                      on: {
                                                        input: function(
                                                          $event
                                                        ) {
                                                          if (
                                                            $event.target
                                                              .composing
                                                          ) {
                                                            return
                                                          }
                                                          _vm.$set(
                                                            impot,
                                                            "titre",
                                                            $event.target.value
                                                          )
                                                        }
                                                      }
                                                    })
                                                  ]),
                                                  _vm._v(" "),
                                                  _c("td", [
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName:
                                                            "v-model.number",
                                                          value: impot.quantite,
                                                          expression:
                                                            "impot.quantite",
                                                          modifiers: {
                                                            number: true
                                                          }
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control text-center",
                                                      attrs: {
                                                        type: "number",
                                                        min: "1"
                                                      },
                                                      domProps: {
                                                        value: impot.quantite
                                                      },
                                                      on: {
                                                        change: function(
                                                          $event
                                                        ) {
                                                          return _vm.totalInitial(
                                                            impot,
                                                            "taxe.mairie",
                                                            _vm.bpfinancier[
                                                              "taxes-impots"
                                                            ]["mairie"]
                                                          )
                                                        },
                                                        input: function(
                                                          $event
                                                        ) {
                                                          if (
                                                            $event.target
                                                              .composing
                                                          ) {
                                                            return
                                                          }
                                                          _vm.$set(
                                                            impot,
                                                            "quantite",
                                                            _vm._n(
                                                              $event.target
                                                                .value
                                                            )
                                                          )
                                                        },
                                                        blur: function($event) {
                                                          return _vm.$forceUpdate()
                                                        }
                                                      }
                                                    })
                                                  ]),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c("vue-numeric", {
                                                        staticClass:
                                                          "form-control text-center",
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          max:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "max"
                                                            ],
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          minus:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "minus"
                                                            ],
                                                          "output-type":
                                                            "currentCurrencyMask['output-type']",
                                                          "empty-value":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "empty-value"
                                                            ],
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ]
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            return _vm.totalInitial(
                                                              impot,
                                                              "taxe.mairie",
                                                              _vm.bpfinancier[
                                                                "taxes-impots"
                                                              ]["mairie"]
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value: impot.prix,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              impot,
                                                              "prix",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "impot.prix"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c(
                                                        "center",
                                                        [
                                                          _c("vue-numeric", {
                                                            attrs: {
                                                              currency:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency"
                                                                ],
                                                              "currency-symbol-position":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency-symbol-position"
                                                                ],
                                                              "output-type":
                                                                "String",
                                                              "empty-value":
                                                                "0",
                                                              precision:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "precision"
                                                                ],
                                                              "decimal-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "decimal-separator"
                                                                ],
                                                              "thousand-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "thousand-separator"
                                                                ],
                                                              "read-only-class":
                                                                "item-total",
                                                              min:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "min"
                                                                ],
                                                              "read-only": ""
                                                            },
                                                            model: {
                                                              value:
                                                                impot.annee1,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  impot,
                                                                  "annee1",
                                                                  _vm._n($$v)
                                                                )
                                                              },
                                                              expression:
                                                                "impot.annee1"
                                                            }
                                                          })
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c("vue-numeric", {
                                                        staticClass:
                                                          "form-control text-center",
                                                        attrs: {
                                                          currency: "%",
                                                          "currency-symbol-position":
                                                            "suffix",
                                                          max: 200,
                                                          min: 0,
                                                          minus: false,
                                                          "empty-value": "0",
                                                          precision: 1
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            return _vm.accroissementPrevisionnelAnnee2(
                                                              impot,
                                                              "taxe.mairie",
                                                              _vm.bpfinancier[
                                                                "taxes-impots"
                                                              ]["mairie"]
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value:
                                                            impot.accroissement1,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              impot,
                                                              "accroissement1",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "impot.accroissement1"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c(
                                                        "center",
                                                        [
                                                          _c("vue-numeric", {
                                                            attrs: {
                                                              currency:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency"
                                                                ],
                                                              "currency-symbol-position":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency-symbol-position"
                                                                ],
                                                              "output-type":
                                                                "String",
                                                              "empty-value":
                                                                "0",
                                                              precision:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "precision"
                                                                ],
                                                              "decimal-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "decimal-separator"
                                                                ],
                                                              "thousand-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "thousand-separator"
                                                                ],
                                                              "read-only-class":
                                                                "item-total",
                                                              min:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "min"
                                                                ],
                                                              "read-only": ""
                                                            },
                                                            model: {
                                                              value:
                                                                impot.annee2,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  impot,
                                                                  "annee2",
                                                                  _vm._n($$v)
                                                                )
                                                              },
                                                              expression:
                                                                "impot.annee2"
                                                            }
                                                          })
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c("vue-numeric", {
                                                        staticClass:
                                                          "form-control text-center",
                                                        attrs: {
                                                          currency: "%",
                                                          "currency-symbol-position":
                                                            "suffix",
                                                          max: 200,
                                                          min: 0,
                                                          minus: false,
                                                          "empty-value": "0",
                                                          precision: 1
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            return _vm.accroissementPrevisionnelAnnee3(
                                                              impot,
                                                              "taxe.mairie",
                                                              _vm.bpfinancier[
                                                                "taxes-impots"
                                                              ]["mairie"]
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value:
                                                            impot.accroissement2,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              impot,
                                                              "accroissement2",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "impot.accroissement2"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c(
                                                        "center",
                                                        [
                                                          _c("vue-numeric", {
                                                            attrs: {
                                                              currency:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency"
                                                                ],
                                                              "currency-symbol-position":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency-symbol-position"
                                                                ],
                                                              "output-type":
                                                                "String",
                                                              "empty-value":
                                                                "0",
                                                              precision:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "precision"
                                                                ],
                                                              "decimal-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "decimal-separator"
                                                                ],
                                                              "thousand-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "thousand-separator"
                                                                ],
                                                              "read-only-class":
                                                                "item-total",
                                                              min:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "min"
                                                                ],
                                                              "read-only": ""
                                                            },
                                                            model: {
                                                              value:
                                                                impot.annee3,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  impot,
                                                                  "annee3",
                                                                  _vm._n($$v)
                                                                )
                                                              },
                                                              expression:
                                                                "impot.annee3"
                                                            }
                                                          })
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  )
                                                ]
                                              )
                                            }
                                          ),
                                          _vm._v(" "),
                                          _c("tr", [
                                            _c(
                                              "td",
                                              { attrs: { colspan: "9" } },
                                              [
                                                _c(
                                                  "button",
                                                  {
                                                    staticClass:
                                                      "btn btn-sm btn-info",
                                                    attrs: { type: "button" },
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.addNewRow(
                                                          "taxes-impots.mairie"
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _c("i", {
                                                      staticClass:
                                                        "fas fa-plus-circle"
                                                    })
                                                  ]
                                                )
                                              ]
                                            )
                                          ])
                                        ],
                                        2
                                      )
                                    ]
                                  )
                                ])
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "tab-content",
                              {
                                attrs: {
                                  title: "4. Frais (Salaires) de Personnel",
                                  icon: "ti-check"
                                }
                              },
                              [
                                _c("div", { staticClass: "table-responsive" }, [
                                  _c(
                                    "table",
                                    {
                                      staticClass:
                                        "table table-bordered table-1 m-b-0 color-bordered-table info-bordered-table",
                                      attrs: { width: "100%" }
                                    },
                                    [
                                      _c("col", {
                                        staticStyle: { width: "1rem" }
                                      }),
                                      _vm._v(" "),
                                      _c("col", {
                                        staticStyle: { width: "20%" }
                                      }),
                                      _vm._v(" "),
                                      _c("col", {
                                        staticStyle: { width: "7%" }
                                      }),
                                      _vm._v(" "),
                                      _c("col", {
                                        staticStyle: { width: "6%" }
                                      }),
                                      _vm._v(" "),
                                      _c("col", {
                                        staticStyle: { width: "10%" }
                                      }),
                                      _vm._v(" "),
                                      _c("col", {}),
                                      _vm._v(" "),
                                      _c("col", {}),
                                      _vm._v(" "),
                                      _c("col", {
                                        staticStyle: { width: "6%" }
                                      }),
                                      _vm._v(" "),
                                      _c("col", {}),
                                      _vm._v(" "),
                                      _c("col", {
                                        staticStyle: { width: "6%" }
                                      }),
                                      _vm._v(" "),
                                      _c("col"),
                                      _vm._v(" "),
                                      _c("thead", [
                                        _c("tr", [
                                          _c("th", {
                                            staticClass:
                                              "table-head-first-red-column"
                                          }),
                                          _vm._v(" "),
                                          _c(
                                            "th",
                                            {
                                              staticClass:
                                                "table-head-first-red-column"
                                            },
                                            [_vm._v("Frais de Personnels")]
                                          ),
                                          _vm._v(" "),
                                          _c("th", [_vm._v("Contrats")]),
                                          _vm._v(" "),
                                          _c("th", [_vm._v("Effectif")]),
                                          _vm._v(" "),
                                          _c("th", [_vm._v("Mois ouvrés")]),
                                          _vm._v(" "),
                                          _c("th", [_vm._v("Salaire/Mois")]),
                                          _vm._v(" "),
                                          _c("th", [_vm._v("Année-1")]),
                                          _vm._v(" "),
                                          _c("th", [
                                            _vm._v("% "),
                                            _c("i", {
                                              staticClass: "fas fa-caret-up"
                                            })
                                          ]),
                                          _vm._v(" "),
                                          _c("th", [_vm._v("Année-2")]),
                                          _vm._v(" "),
                                          _c("th", [
                                            _vm._v("% "),
                                            _c("i", {
                                              staticClass: "fas fa-caret-up"
                                            })
                                          ]),
                                          _vm._v(" "),
                                          _c("th", [_vm._v("Année-3")])
                                        ])
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "tbody",
                                        [
                                          _c("tr", [
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              {
                                                staticClass:
                                                  "empty-numeric-placeholder"
                                              },
                                              [_vm._v("          ")]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              {
                                                staticClass:
                                                  "empty-numeric-placeholder"
                                              },
                                              [_vm._v("           ")]
                                            ),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              {
                                                staticClass:
                                                  "empty-numeric-placeholder"
                                              },
                                              [_vm._v("           ")]
                                            ),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              {
                                                staticClass:
                                                  "empty-numeric-placeholder"
                                              },
                                              [_vm._v("           ")]
                                            )
                                          ]),
                                          _vm._v(" "),
                                          _c(
                                            "tr",
                                            { staticClass: "table-total" },
                                            [
                                              _c(
                                                "td",
                                                { attrs: { colspan: "2" } },
                                                [
                                                  _vm._v(
                                                    " Total Frais de Personnels"
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c("center", [
                                                    _vm._v(
                                                      _vm._s(
                                                        this.totalFraisPersonnel
                                                          .effectif
                                                      )
                                                    )
                                                  ])
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm
                                                              .totalFraisPersonnel
                                                              .annee1,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.totalFraisPersonnel,
                                                              "annee1",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "totalFraisPersonnel.annee1"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency: "%",
                                                          "currency-symbol-position":
                                                            "suffix",
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          minus: false,
                                                          precision: 1,
                                                          max: 200,
                                                          "decimal-separator":
                                                            ",",
                                                          "thousand-separator":
                                                            "",
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value: this
                                                            .totalFraisPersonnel
                                                            .accroissement1,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              this
                                                                .totalFraisPersonnel,
                                                              "accroissement1",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "this.totalFraisPersonnel.accroissement1"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm
                                                              .totalFraisPersonnel
                                                              .annee2,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.totalFraisPersonnel,
                                                              "annee2",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "totalFraisPersonnel.annee2"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency: "%",
                                                          "currency-symbol-position":
                                                            "suffix",
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          minus: false,
                                                          precision: 1,
                                                          max: 200,
                                                          "decimal-separator":
                                                            ",",
                                                          "thousand-separator":
                                                            "",
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value: this
                                                            .totalFraisPersonnel
                                                            .accroissement2,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              this
                                                                .totalFraisPersonnel,
                                                              "accroissement2",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "this.totalFraisPersonnel.accroissement2"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm
                                                              .totalFraisPersonnel
                                                              .annee3,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.totalFraisPersonnel,
                                                              "annee3",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "totalFraisPersonnel.annee3"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c("tr", [
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td")
                                          ]),
                                          _vm._v(" "),
                                          _c(
                                            "tr",
                                            {
                                              staticClass: "table-group-header"
                                            },
                                            [
                                              _c(
                                                "td",
                                                { attrs: { colspan: "2" } },
                                                [_vm._v("Dirigeants")]
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c("center", [
                                                    _vm._v(
                                                      _vm._s(
                                                        this
                                                          .subTotalFraisDirigeant
                                                          .effectif
                                                      )
                                                    )
                                                  ])
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm
                                                              .subTotalFraisDirigeant
                                                              .annee1,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.subTotalFraisDirigeant,
                                                              "annee1",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "subTotalFraisDirigeant.annee1"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm
                                                              .subTotalFraisDirigeant
                                                              .annee2,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.subTotalFraisDirigeant,
                                                              "annee2",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "subTotalFraisDirigeant.annee2"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm
                                                              .subTotalFraisDirigeant
                                                              .annee3,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.subTotalFraisDirigeant,
                                                              "annee3",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "subTotalFraisDirigeant.annee3"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _vm._l(
                                            _vm.bpfinancier["frais-personnel"][
                                              "dirigeants"
                                            ],
                                            function(dirigeant, index) {
                                              return _c(
                                                "tr",
                                                { key: "dirigeant-" + index },
                                                [
                                                  _c("td", [
                                                    _c(
                                                      "button",
                                                      {
                                                        staticClass:
                                                          "btn btn-sm btn-danger",
                                                        attrs: {
                                                          type: "button"
                                                        },
                                                        on: {
                                                          click: function(
                                                            $event
                                                          ) {
                                                            return _vm.deleteRow(
                                                              "frais-personnel.dirigeants",
                                                              dirigeant
                                                            )
                                                          }
                                                        }
                                                      },
                                                      [
                                                        _c("i", {
                                                          staticClass:
                                                            "fas fa-minus-circle"
                                                        })
                                                      ]
                                                    )
                                                  ]),
                                                  _vm._v(" "),
                                                  _c("td", [
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName: "v-model",
                                                          value:
                                                            dirigeant.titre,
                                                          expression:
                                                            "dirigeant.titre"
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control text-dark",
                                                      attrs: { type: "text" },
                                                      domProps: {
                                                        value: dirigeant.titre
                                                      },
                                                      on: {
                                                        input: function(
                                                          $event
                                                        ) {
                                                          if (
                                                            $event.target
                                                              .composing
                                                          ) {
                                                            return
                                                          }
                                                          _vm.$set(
                                                            dirigeant,
                                                            "titre",
                                                            $event.target.value
                                                          )
                                                        }
                                                      }
                                                    })
                                                  ]),
                                                  _vm._v(" "),
                                                  _c("td", [
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName: "v-model",
                                                          value:
                                                            dirigeant.contrat,
                                                          expression:
                                                            "dirigeant.contrat"
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control",
                                                      attrs: { type: "text" },
                                                      domProps: {
                                                        value: dirigeant.contrat
                                                      },
                                                      on: {
                                                        input: function(
                                                          $event
                                                        ) {
                                                          if (
                                                            $event.target
                                                              .composing
                                                          ) {
                                                            return
                                                          }
                                                          _vm.$set(
                                                            dirigeant,
                                                            "contrat",
                                                            $event.target.value
                                                          )
                                                        }
                                                      }
                                                    })
                                                  ]),
                                                  _vm._v(" "),
                                                  _c("td", [
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName:
                                                            "v-model.number",
                                                          value:
                                                            dirigeant.effectif,
                                                          expression:
                                                            "dirigeant.effectif",
                                                          modifiers: {
                                                            number: true
                                                          }
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control text-center",
                                                      attrs: {
                                                        type: "number",
                                                        min: "0"
                                                      },
                                                      domProps: {
                                                        value:
                                                          dirigeant.effectif
                                                      },
                                                      on: {
                                                        input: [
                                                          function($event) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              dirigeant,
                                                              "effectif",
                                                              _vm._n(
                                                                $event.target
                                                                  .value
                                                              )
                                                            )
                                                          },
                                                          function($event) {
                                                            return _vm.updateEffectifPersonnel(
                                                              dirigeant
                                                            )
                                                          }
                                                        ],
                                                        blur: function($event) {
                                                          return _vm.$forceUpdate()
                                                        }
                                                      }
                                                    })
                                                  ]),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c("vue-numeric", {
                                                        staticClass:
                                                          "form-control text-center",
                                                        attrs: {
                                                          currency: "mois",
                                                          "currency-symbol-position":
                                                            "suffix",
                                                          max: 200,
                                                          min: 0,
                                                          minus: false,
                                                          "empty-value": "0",
                                                          precision: 0
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            return _vm.salaireInitial(
                                                              dirigeant,
                                                              "frais.dirigeant",
                                                              _vm.bpfinancier[
                                                                "frais-personnel"
                                                              ]["dirigeants"]
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value:
                                                            dirigeant.ouvrable,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              dirigeant,
                                                              "ouvrable",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "dirigeant.ouvrable"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c("vue-numeric", {
                                                        staticClass:
                                                          "form-control text-center",
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          max:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "max"
                                                            ],
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          minus:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "minus"
                                                            ],
                                                          "output-type":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "output-type"
                                                            ],
                                                          "empty-value":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "empty-value"
                                                            ],
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ]
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            return _vm.salaireInitial(
                                                              dirigeant,
                                                              "frais.dirigeant",
                                                              _vm.bpfinancier[
                                                                "frais-personnel"
                                                              ]["dirigeants"]
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value:
                                                            dirigeant.salaire,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              dirigeant,
                                                              "salaire",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "dirigeant.salaire"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c(
                                                        "center",
                                                        [
                                                          _c("vue-numeric", {
                                                            attrs: {
                                                              currency:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency"
                                                                ],
                                                              "currency-symbol-position":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency-symbol-position"
                                                                ],
                                                              "output-type":
                                                                "String",
                                                              "empty-value":
                                                                "0",
                                                              precision:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "precision"
                                                                ],
                                                              "decimal-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "decimal-separator"
                                                                ],
                                                              "thousand-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "thousand-separator"
                                                                ],
                                                              "read-only-class":
                                                                "item-total",
                                                              min:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "min"
                                                                ],
                                                              "read-only": ""
                                                            },
                                                            model: {
                                                              value:
                                                                dirigeant.annee1,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  dirigeant,
                                                                  "annee1",
                                                                  _vm._n($$v)
                                                                )
                                                              },
                                                              expression:
                                                                "dirigeant.annee1"
                                                            }
                                                          })
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c("vue-numeric", {
                                                        staticClass:
                                                          "form-control text-center",
                                                        attrs: {
                                                          currency: "%",
                                                          "currency-symbol-position":
                                                            "suffix",
                                                          max: 200,
                                                          min: 0,
                                                          minus: false,
                                                          "empty-value": "0",
                                                          precision: 1
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            return _vm.accroissementPrevisionnelAnnee2(
                                                              dirigeant,
                                                              "frais.dirigeant",
                                                              _vm.bpfinancier[
                                                                "frais-personnel"
                                                              ]["dirigeants"]
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value:
                                                            dirigeant.accroissement1,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              dirigeant,
                                                              "accroissement1",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "dirigeant.accroissement1"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c(
                                                        "center",
                                                        [
                                                          _c("vue-numeric", {
                                                            attrs: {
                                                              currency:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency"
                                                                ],
                                                              "currency-symbol-position":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency-symbol-position"
                                                                ],
                                                              "output-type":
                                                                "String",
                                                              "empty-value":
                                                                "0",
                                                              precision:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "precision"
                                                                ],
                                                              "decimal-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "decimal-separator"
                                                                ],
                                                              "thousand-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "thousand-separator"
                                                                ],
                                                              "read-only-class":
                                                                "item-total",
                                                              min:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "min"
                                                                ],
                                                              "read-only": ""
                                                            },
                                                            model: {
                                                              value:
                                                                dirigeant.annee2,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  dirigeant,
                                                                  "annee2",
                                                                  _vm._n($$v)
                                                                )
                                                              },
                                                              expression:
                                                                "dirigeant.annee2"
                                                            }
                                                          })
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c("vue-numeric", {
                                                        staticClass:
                                                          "form-control text-center",
                                                        attrs: {
                                                          currency: "%",
                                                          "currency-symbol-position":
                                                            "suffix",
                                                          max: 200,
                                                          min: 0,
                                                          minus: false,
                                                          "empty-value": "0",
                                                          precision: 1
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            return _vm.accroissementPrevisionnelAnnee3(
                                                              dirigeant,
                                                              "frais.dirigeant",
                                                              _vm.bpfinancier[
                                                                "frais-personnel"
                                                              ]["dirigeants"]
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value:
                                                            dirigeant.accroissement2,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              dirigeant,
                                                              "accroissement2",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "dirigeant.accroissement2"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c(
                                                        "center",
                                                        [
                                                          _c("vue-numeric", {
                                                            attrs: {
                                                              currency:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency"
                                                                ],
                                                              "currency-symbol-position":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency-symbol-position"
                                                                ],
                                                              "output-type":
                                                                "String",
                                                              "empty-value":
                                                                "0",
                                                              precision:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "precision"
                                                                ],
                                                              "decimal-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "decimal-separator"
                                                                ],
                                                              "thousand-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "thousand-separator"
                                                                ],
                                                              "read-only-class":
                                                                "item-total",
                                                              min:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "min"
                                                                ],
                                                              "read-only": ""
                                                            },
                                                            model: {
                                                              value:
                                                                dirigeant.annee3,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  dirigeant,
                                                                  "annee3",
                                                                  _vm._n($$v)
                                                                )
                                                              },
                                                              expression:
                                                                "dirigeant.annee3"
                                                            }
                                                          })
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  )
                                                ]
                                              )
                                            }
                                          ),
                                          _vm._v(" "),
                                          _c("tr", [
                                            _c(
                                              "td",
                                              { attrs: { colspan: "11" } },
                                              [
                                                _c(
                                                  "button",
                                                  {
                                                    staticClass:
                                                      "btn btn-sm btn-info",
                                                    attrs: { type: "button" },
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.addNewRow(
                                                          "frais-personnel.dirigeants"
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _c("i", {
                                                      staticClass:
                                                        "fas fa-plus-circle"
                                                    })
                                                  ]
                                                )
                                              ]
                                            )
                                          ]),
                                          _vm._v(" "),
                                          _c("tr", [
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td"),
                                            _vm._v(" "),
                                            _c("td")
                                          ]),
                                          _vm._v(" "),
                                          _c(
                                            "tr",
                                            {
                                              staticClass: "table-group-header"
                                            },
                                            [
                                              _c(
                                                "td",
                                                { attrs: { colspan: "2" } },
                                                [_vm._v("Salariés")]
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c("center", [
                                                    _vm._v(
                                                      _vm._s(
                                                        this
                                                          .subTotalFraisSalarie
                                                          .effectif
                                                      )
                                                    )
                                                  ])
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm
                                                              .subTotalFraisSalarie
                                                              .annee1,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.subTotalFraisSalarie,
                                                              "annee1",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "subTotalFraisSalarie.annee1"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm
                                                              .subTotalFraisSalarie
                                                              .annee2,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.subTotalFraisSalarie,
                                                              "annee2",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "subTotalFraisSalarie.annee2"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm
                                                              .subTotalFraisSalarie
                                                              .annee3,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.subTotalFraisSalarie,
                                                              "annee3",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "subTotalFraisSalarie.annee3"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _vm._l(
                                            _vm.bpfinancier["frais-personnel"][
                                              "salaries"
                                            ],
                                            function(salarie, index) {
                                              return _c(
                                                "tr",
                                                { key: "salarie-" + index },
                                                [
                                                  _c("td", [
                                                    _c(
                                                      "button",
                                                      {
                                                        staticClass:
                                                          "btn btn-sm btn-danger",
                                                        attrs: {
                                                          type: "button"
                                                        },
                                                        on: {
                                                          click: function(
                                                            $event
                                                          ) {
                                                            return _vm.deleteRow(
                                                              "frais-personnel.salaries",
                                                              salarie
                                                            )
                                                          }
                                                        }
                                                      },
                                                      [
                                                        _c("i", {
                                                          staticClass:
                                                            "fas fa-minus-circle"
                                                        })
                                                      ]
                                                    )
                                                  ]),
                                                  _vm._v(" "),
                                                  _c("td", [
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName: "v-model",
                                                          value: salarie.titre,
                                                          expression:
                                                            "salarie.titre"
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control text-dark",
                                                      attrs: { type: "text" },
                                                      domProps: {
                                                        value: salarie.titre
                                                      },
                                                      on: {
                                                        input: function(
                                                          $event
                                                        ) {
                                                          if (
                                                            $event.target
                                                              .composing
                                                          ) {
                                                            return
                                                          }
                                                          _vm.$set(
                                                            salarie,
                                                            "titre",
                                                            $event.target.value
                                                          )
                                                        }
                                                      }
                                                    })
                                                  ]),
                                                  _vm._v(" "),
                                                  _c("td", [
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName: "v-model",
                                                          value:
                                                            salarie.contrat,
                                                          expression:
                                                            "salarie.contrat"
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control",
                                                      attrs: { type: "text" },
                                                      domProps: {
                                                        value: salarie.contrat
                                                      },
                                                      on: {
                                                        input: function(
                                                          $event
                                                        ) {
                                                          if (
                                                            $event.target
                                                              .composing
                                                          ) {
                                                            return
                                                          }
                                                          _vm.$set(
                                                            salarie,
                                                            "contrat",
                                                            $event.target.value
                                                          )
                                                        }
                                                      }
                                                    })
                                                  ]),
                                                  _vm._v(" "),
                                                  _c("td", [
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName:
                                                            "v-model.number",
                                                          value:
                                                            salarie.effectif,
                                                          expression:
                                                            "salarie.effectif",
                                                          modifiers: {
                                                            number: true
                                                          }
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control text-center",
                                                      attrs: {
                                                        type: "number",
                                                        min: "0"
                                                      },
                                                      domProps: {
                                                        value: salarie.effectif
                                                      },
                                                      on: {
                                                        input: [
                                                          function($event) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              salarie,
                                                              "effectif",
                                                              _vm._n(
                                                                $event.target
                                                                  .value
                                                              )
                                                            )
                                                          },
                                                          function($event) {
                                                            return _vm.updateEffectifPersonnel(
                                                              salarie
                                                            )
                                                          }
                                                        ],
                                                        blur: function($event) {
                                                          return _vm.$forceUpdate()
                                                        }
                                                      }
                                                    })
                                                  ]),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c("vue-numeric", {
                                                        staticClass:
                                                          "form-control text-center",
                                                        attrs: {
                                                          currency: "mois",
                                                          "currency-symbol-position":
                                                            "suffix",
                                                          max: 200,
                                                          min: 0,
                                                          minus: false,
                                                          "empty-value": "0",
                                                          precision: 0
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            return _vm.salaireInitial(
                                                              salarie,
                                                              "frais.salarie",
                                                              _vm.bpfinancier[
                                                                "frais-personnel"
                                                              ]["salaries"]
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value:
                                                            salarie.ouvrable,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              salarie,
                                                              "ouvrable",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "salarie.ouvrable"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c("vue-numeric", {
                                                        staticClass:
                                                          "form-control text-center",
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          max:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "max"
                                                            ],
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          minus:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "minus"
                                                            ],
                                                          "output-type":
                                                            "currentCurrencyMask['output-type']",
                                                          "empty-value":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "empty-value"
                                                            ],
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ]
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            return _vm.salaireInitial(
                                                              salarie,
                                                              "frais.salarie",
                                                              _vm.bpfinancier[
                                                                "frais-personnel"
                                                              ]["salaries"]
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value:
                                                            salarie.salaire,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              salarie,
                                                              "salaire",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "salarie.salaire"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c(
                                                        "center",
                                                        [
                                                          _c("vue-numeric", {
                                                            attrs: {
                                                              currency:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency"
                                                                ],
                                                              "currency-symbol-position":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency-symbol-position"
                                                                ],
                                                              "output-type":
                                                                "String",
                                                              "empty-value":
                                                                "0",
                                                              precision:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "precision"
                                                                ],
                                                              "decimal-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "decimal-separator"
                                                                ],
                                                              "thousand-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "thousand-separator"
                                                                ],
                                                              "read-only-class":
                                                                "item-total",
                                                              min:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "min"
                                                                ],
                                                              "read-only": ""
                                                            },
                                                            model: {
                                                              value:
                                                                salarie.annee1,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  salarie,
                                                                  "annee1",
                                                                  _vm._n($$v)
                                                                )
                                                              },
                                                              expression:
                                                                "salarie.annee1"
                                                            }
                                                          })
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c("vue-numeric", {
                                                        staticClass:
                                                          "form-control text-center",
                                                        attrs: {
                                                          currency: "%",
                                                          "currency-symbol-position":
                                                            "suffix",
                                                          max: 200,
                                                          min: 0,
                                                          minus: false,
                                                          "empty-value": "0",
                                                          precision: 1
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            return _vm.accroissementPrevisionnelAnnee2(
                                                              salarie,
                                                              "frais.salarie",
                                                              _vm.bpfinancier[
                                                                "frais-personnel"
                                                              ]["salaries"]
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value:
                                                            salarie.accroissement1,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              salarie,
                                                              "accroissement1",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "salarie.accroissement1"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c(
                                                        "center",
                                                        [
                                                          _c("vue-numeric", {
                                                            attrs: {
                                                              currency:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency"
                                                                ],
                                                              "currency-symbol-position":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency-symbol-position"
                                                                ],
                                                              "output-type":
                                                                "String",
                                                              "empty-value":
                                                                "0",
                                                              precision:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "precision"
                                                                ],
                                                              "decimal-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "decimal-separator"
                                                                ],
                                                              "thousand-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "thousand-separator"
                                                                ],
                                                              "read-only-class":
                                                                "item-total",
                                                              min:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "min"
                                                                ],
                                                              "read-only": ""
                                                            },
                                                            model: {
                                                              value:
                                                                salarie.annee2,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  salarie,
                                                                  "annee2",
                                                                  _vm._n($$v)
                                                                )
                                                              },
                                                              expression:
                                                                "salarie.annee2"
                                                            }
                                                          })
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c("vue-numeric", {
                                                        staticClass:
                                                          "form-control text-center",
                                                        attrs: {
                                                          currency: "%",
                                                          "currency-symbol-position":
                                                            "suffix",
                                                          max: 200,
                                                          min: 0,
                                                          minus: false,
                                                          "empty-value": "0",
                                                          precision: 1
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            return _vm.accroissementPrevisionnelAnnee3(
                                                              salarie,
                                                              "frais.salarie",
                                                              _vm.bpfinancier[
                                                                "frais-personnel"
                                                              ]["salaries"]
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value:
                                                            salarie.accroissement2,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              salarie,
                                                              "accroissement2",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "salarie.accroissement2"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c(
                                                        "center",
                                                        [
                                                          _c("vue-numeric", {
                                                            attrs: {
                                                              currency:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency"
                                                                ],
                                                              "currency-symbol-position":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency-symbol-position"
                                                                ],
                                                              "output-type":
                                                                "String",
                                                              "empty-value":
                                                                "0",
                                                              precision:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "precision"
                                                                ],
                                                              "decimal-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "decimal-separator"
                                                                ],
                                                              "thousand-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "thousand-separator"
                                                                ],
                                                              "read-only-class":
                                                                "item-total",
                                                              min:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "min"
                                                                ],
                                                              "read-only": ""
                                                            },
                                                            model: {
                                                              value:
                                                                salarie.annee3,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  salarie,
                                                                  "annee3",
                                                                  _vm._n($$v)
                                                                )
                                                              },
                                                              expression:
                                                                "salarie.annee3"
                                                            }
                                                          })
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  )
                                                ]
                                              )
                                            }
                                          ),
                                          _vm._v(" "),
                                          _c("tr", [
                                            _c(
                                              "td",
                                              { attrs: { colspan: "11" } },
                                              [
                                                _c(
                                                  "button",
                                                  {
                                                    staticClass:
                                                      "btn btn-sm btn-info",
                                                    attrs: { type: "button" },
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.addNewRow(
                                                          "frais-personnel.salaries"
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _c("i", {
                                                      staticClass:
                                                        "fas fa-plus-circle"
                                                    })
                                                  ]
                                                )
                                              ]
                                            )
                                          ]),
                                          _vm._v(" "),
                                          _c(
                                            "tr",
                                            {
                                              staticClass: "table-group-header"
                                            },
                                            [
                                              _c(
                                                "td",
                                                { attrs: { colspan: "2" } },
                                                [_vm._v("Stagiaires")]
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c("center", [
                                                    _vm._v(
                                                      _vm._s(
                                                        this
                                                          .subTotalFraisStagiaire
                                                          .effectif
                                                      )
                                                    )
                                                  ])
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm
                                                              .subTotalFraisStagiaire
                                                              .annee1,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.subTotalFraisStagiaire,
                                                              "annee1",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "subTotalFraisStagiaire.annee1"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm
                                                              .subTotalFraisStagiaire
                                                              .annee2,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.subTotalFraisStagiaire,
                                                              "annee2",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "subTotalFraisStagiaire.annee2"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm
                                                              .subTotalFraisStagiaire
                                                              .annee3,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.subTotalFraisStagiaire,
                                                              "annee3",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "subTotalFraisStagiaire.annee3"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _vm._l(
                                            _vm.bpfinancier["frais-personnel"][
                                              "stagiaires"
                                            ],
                                            function(stagiaire, index) {
                                              return _c(
                                                "tr",
                                                { key: "stagiaire-" + index },
                                                [
                                                  _c("td", [
                                                    _c(
                                                      "button",
                                                      {
                                                        staticClass:
                                                          "btn btn-sm btn-danger",
                                                        attrs: {
                                                          type: "button"
                                                        },
                                                        on: {
                                                          click: function(
                                                            $event
                                                          ) {
                                                            return _vm.deleteRow(
                                                              "frais-personnel.stagiaires",
                                                              stagiaire
                                                            )
                                                          }
                                                        }
                                                      },
                                                      [
                                                        _c("i", {
                                                          staticClass:
                                                            "fas fa-minus-circle"
                                                        })
                                                      ]
                                                    )
                                                  ]),
                                                  _vm._v(" "),
                                                  _c("td", [
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName: "v-model",
                                                          value:
                                                            stagiaire.titre,
                                                          expression:
                                                            "stagiaire.titre"
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control text-dark",
                                                      attrs: { type: "text" },
                                                      domProps: {
                                                        value: stagiaire.titre
                                                      },
                                                      on: {
                                                        input: function(
                                                          $event
                                                        ) {
                                                          if (
                                                            $event.target
                                                              .composing
                                                          ) {
                                                            return
                                                          }
                                                          _vm.$set(
                                                            stagiaire,
                                                            "titre",
                                                            $event.target.value
                                                          )
                                                        }
                                                      }
                                                    })
                                                  ]),
                                                  _vm._v(" "),
                                                  _c("td", [
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName: "v-model",
                                                          value:
                                                            stagiaire.contrat,
                                                          expression:
                                                            "stagiaire.contrat"
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control",
                                                      attrs: { type: "text" },
                                                      domProps: {
                                                        value: stagiaire.contrat
                                                      },
                                                      on: {
                                                        input: function(
                                                          $event
                                                        ) {
                                                          if (
                                                            $event.target
                                                              .composing
                                                          ) {
                                                            return
                                                          }
                                                          _vm.$set(
                                                            stagiaire,
                                                            "contrat",
                                                            $event.target.value
                                                          )
                                                        }
                                                      }
                                                    })
                                                  ]),
                                                  _vm._v(" "),
                                                  _c("td", [
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName:
                                                            "v-model.number",
                                                          value:
                                                            stagiaire.effectif,
                                                          expression:
                                                            "stagiaire.effectif",
                                                          modifiers: {
                                                            number: true
                                                          }
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control text-center",
                                                      attrs: {
                                                        type: "number",
                                                        min: "0"
                                                      },
                                                      domProps: {
                                                        value:
                                                          stagiaire.effectif
                                                      },
                                                      on: {
                                                        input: [
                                                          function($event) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              stagiaire,
                                                              "effectif",
                                                              _vm._n(
                                                                $event.target
                                                                  .value
                                                              )
                                                            )
                                                          },
                                                          function($event) {
                                                            return _vm.updateEffectifPersonnel(
                                                              stagiaire
                                                            )
                                                          }
                                                        ],
                                                        blur: function($event) {
                                                          return _vm.$forceUpdate()
                                                        }
                                                      }
                                                    })
                                                  ]),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c("vue-numeric", {
                                                        staticClass:
                                                          "form-control text-center",
                                                        attrs: {
                                                          currency: "mois",
                                                          "currency-symbol-position":
                                                            "suffix",
                                                          max: 200,
                                                          min: 0,
                                                          minus: false,
                                                          "empty-value": "0",
                                                          precision: 0
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            return _vm.salaireInitial(
                                                              stagiaire,
                                                              "frais.stagiaire",
                                                              _vm.bpfinancier[
                                                                "frais-personnel"
                                                              ]["stagiaires"]
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value:
                                                            stagiaire.ouvrable,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              stagiaire,
                                                              "ouvrable",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "stagiaire.ouvrable"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c("vue-numeric", {
                                                        staticClass:
                                                          "form-control text-center",
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          max:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "max"
                                                            ],
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          minus:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "minus"
                                                            ],
                                                          "output-type":
                                                            "currentCurrencyMask['output-type']",
                                                          "empty-value":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "empty-value"
                                                            ],
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ]
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            return _vm.salaireInitial(
                                                              stagiaire,
                                                              "frais.stagiaire",
                                                              _vm.bpfinancier[
                                                                "frais-personnel"
                                                              ]["stagiaires"]
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value:
                                                            stagiaire.salaire,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              stagiaire,
                                                              "salaire",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "stagiaire.salaire"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c(
                                                        "center",
                                                        [
                                                          _c("vue-numeric", {
                                                            attrs: {
                                                              currency:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency"
                                                                ],
                                                              "currency-symbol-position":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency-symbol-position"
                                                                ],
                                                              "output-type":
                                                                "String",
                                                              "empty-value":
                                                                "0",
                                                              precision:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "precision"
                                                                ],
                                                              "decimal-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "decimal-separator"
                                                                ],
                                                              "thousand-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "thousand-separator"
                                                                ],
                                                              "read-only-class":
                                                                "item-total",
                                                              min:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "min"
                                                                ],
                                                              "read-only": ""
                                                            },
                                                            model: {
                                                              value:
                                                                stagiaire.annee1,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  stagiaire,
                                                                  "annee1",
                                                                  _vm._n($$v)
                                                                )
                                                              },
                                                              expression:
                                                                "stagiaire.annee1"
                                                            }
                                                          })
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c("vue-numeric", {
                                                        staticClass:
                                                          "form-control text-center",
                                                        attrs: {
                                                          currency: "%",
                                                          "currency-symbol-position":
                                                            "suffix",
                                                          max: 200,
                                                          min: 0,
                                                          minus: false,
                                                          "empty-value": "0",
                                                          precision: 1
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            return _vm.accroissementPrevisionnelAnnee2(
                                                              stagiaire,
                                                              "frais.stagiaire",
                                                              _vm.bpfinancier[
                                                                "frais-personnel"
                                                              ]["stagiaires"]
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value:
                                                            stagiaire.accroissement1,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              stagiaire,
                                                              "accroissement1",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "stagiaire.accroissement1"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c(
                                                        "center",
                                                        [
                                                          _c("vue-numeric", {
                                                            attrs: {
                                                              currency:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency"
                                                                ],
                                                              "currency-symbol-position":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency-symbol-position"
                                                                ],
                                                              "output-type":
                                                                "String",
                                                              "empty-value":
                                                                "0",
                                                              precision:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "precision"
                                                                ],
                                                              "decimal-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "decimal-separator"
                                                                ],
                                                              "thousand-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "thousand-separator"
                                                                ],
                                                              "read-only-class":
                                                                "item-total",
                                                              min:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "min"
                                                                ],
                                                              "read-only": ""
                                                            },
                                                            model: {
                                                              value:
                                                                stagiaire.annee2,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  stagiaire,
                                                                  "annee2",
                                                                  _vm._n($$v)
                                                                )
                                                              },
                                                              expression:
                                                                "stagiaire.annee2"
                                                            }
                                                          })
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c("vue-numeric", {
                                                        staticClass:
                                                          "form-control text-center",
                                                        attrs: {
                                                          currency: "%",
                                                          "currency-symbol-position":
                                                            "suffix",
                                                          max: 200,
                                                          min: 0,
                                                          minus: false,
                                                          "empty-value": "0",
                                                          precision: 1
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            return _vm.accroissementPrevisionnelAnnee3(
                                                              stagiaire,
                                                              "frais.stagiaire",
                                                              _vm.bpfinancier[
                                                                "frais-personnel"
                                                              ]["stagiaires"]
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value:
                                                            stagiaire.accroissement2,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              stagiaire,
                                                              "accroissement2",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "stagiaire.accroissement2"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "td",
                                                    [
                                                      _c(
                                                        "center",
                                                        [
                                                          _c("vue-numeric", {
                                                            attrs: {
                                                              currency:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency"
                                                                ],
                                                              "currency-symbol-position":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "currency-symbol-position"
                                                                ],
                                                              "output-type":
                                                                "String",
                                                              "empty-value":
                                                                "0",
                                                              precision:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "precision"
                                                                ],
                                                              "decimal-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "decimal-separator"
                                                                ],
                                                              "thousand-separator":
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "thousand-separator"
                                                                ],
                                                              "read-only-class":
                                                                "item-total",
                                                              min:
                                                                _vm
                                                                  .currentCurrencyMask[
                                                                  "min"
                                                                ],
                                                              "read-only": ""
                                                            },
                                                            model: {
                                                              value:
                                                                stagiaire.annee3,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  stagiaire,
                                                                  "annee3",
                                                                  _vm._n($$v)
                                                                )
                                                              },
                                                              expression:
                                                                "stagiaire.annee3"
                                                            }
                                                          })
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  )
                                                ]
                                              )
                                            }
                                          ),
                                          _vm._v(" "),
                                          _c("tr", [
                                            _c(
                                              "td",
                                              { attrs: { colspan: "11" } },
                                              [
                                                _c(
                                                  "button",
                                                  {
                                                    staticClass:
                                                      "btn btn-sm btn-info",
                                                    attrs: { type: "button" },
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.addNewRow(
                                                          "frais-personnel.stagiaires"
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _c("i", {
                                                      staticClass:
                                                        "fas fa-plus-circle"
                                                    })
                                                  ]
                                                )
                                              ]
                                            )
                                          ]),
                                          _vm._v(" "),
                                          _c(
                                            "tr",
                                            {
                                              staticClass: "table-group-header"
                                            },
                                            [
                                              _c(
                                                "td",
                                                { attrs: { colspan: "2" } },
                                                [_vm._v("Charges Sociales")]
                                              ),
                                              _vm._v(" "),
                                              _c("td", [_vm._v(" Taux")]),
                                              _vm._v(" "),
                                              _c("td", [
                                                _vm._v(
                                                  "\n                                                                    Effectif\n                                                                "
                                                )
                                              ]),
                                              _vm._v(" "),
                                              _c("td", {
                                                attrs: { colspan: "2" }
                                              }),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm
                                                              .subTotalFraisCharge
                                                              .annee1,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.subTotalFraisCharge,
                                                              "annee1",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "subTotalFraisCharge.annee1"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm
                                                              .subTotalFraisCharge
                                                              .annee2,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.subTotalFraisCharge,
                                                              "annee2",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "subTotalFraisCharge.annee2"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c("td"),
                                              _vm._v(" "),
                                              _c(
                                                "td",
                                                [
                                                  _c(
                                                    "center",
                                                    [
                                                      _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "String",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          "read-only-class":
                                                            "item-total",
                                                          min:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "min"
                                                            ],
                                                          "read-only": ""
                                                        },
                                                        model: {
                                                          value:
                                                            _vm
                                                              .subTotalFraisCharge
                                                              .annee3,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              _vm.subTotalFraisCharge,
                                                              "annee3",
                                                              _vm._n($$v)
                                                            )
                                                          },
                                                          expression:
                                                            "subTotalFraisCharge.annee3"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c("tr", [
                                            _c(
                                              "td",
                                              { attrs: { colspan: "2" } },
                                              [
                                                _c("center", [
                                                  _vm._v(
                                                    " " +
                                                      _vm._s(
                                                        _vm
                                                          .chargeSocialeDirigeant
                                                          .titre
                                                      ) +
                                                      " "
                                                  )
                                                ])
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              [
                                                _c(
                                                  "center",
                                                  [
                                                    _c("vue-numeric", {
                                                      attrs: {
                                                        currency: "%",
                                                        "currency-symbol-position":
                                                          "suffix",
                                                        "output-type": "String",
                                                        "empty-value": "0",
                                                        minus: false,
                                                        precision: 1,
                                                        max: 200,
                                                        min: 0,
                                                        "decimal-separator":
                                                          ",",
                                                        "thousand-separator":
                                                          "",
                                                        "read-only-class":
                                                          "item-total",
                                                        "read-only": ""
                                                      },
                                                      model: {
                                                        value:
                                                          _vm
                                                            .chargeSocialeDirigeant
                                                            .taux,
                                                        callback: function(
                                                          $$v
                                                        ) {
                                                          _vm.$set(
                                                            _vm.chargeSocialeDirigeant,
                                                            "taux",
                                                            _vm._n($$v)
                                                          )
                                                        },
                                                        expression:
                                                          "chargeSocialeDirigeant.taux"
                                                      }
                                                    })
                                                  ],
                                                  1
                                                )
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              [
                                                _c("center", [
                                                  _vm._v(
                                                    _vm._s(
                                                      _vm.chargeSocialeDirigeant
                                                        .effectif
                                                    )
                                                  )
                                                ])
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c("td", {
                                              attrs: { colspan: "2" }
                                            }),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              [
                                                _c(
                                                  "center",
                                                  [
                                                    _c("vue-numeric", {
                                                      attrs: {
                                                        currency:
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "currency"
                                                          ],
                                                        "currency-symbol-position":
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "currency-symbol-position"
                                                          ],
                                                        "output-type": "String",
                                                        "empty-value": "0",
                                                        precision:
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "precision"
                                                          ],
                                                        "decimal-separator":
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "decimal-separator"
                                                          ],
                                                        "thousand-separator":
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "thousand-separator"
                                                          ],
                                                        "read-only-class":
                                                          "item-total",
                                                        min:
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "min"
                                                          ],
                                                        "read-only": ""
                                                      },
                                                      model: {
                                                        value:
                                                          _vm
                                                            .chargeSocialeDirigeant
                                                            .annee1,
                                                        callback: function(
                                                          $$v
                                                        ) {
                                                          _vm.$set(
                                                            _vm.chargeSocialeDirigeant,
                                                            "annee1",
                                                            _vm._n($$v)
                                                          )
                                                        },
                                                        expression:
                                                          "chargeSocialeDirigeant.annee1"
                                                      }
                                                    })
                                                  ],
                                                  1
                                                )
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              [
                                                _c(
                                                  "center",
                                                  [
                                                    _c("vue-numeric", {
                                                      attrs: {
                                                        currency: "%",
                                                        "currency-symbol-position":
                                                          "suffix",
                                                        "output-type": "String",
                                                        "empty-value": "0",
                                                        minus: false,
                                                        precision: 1,
                                                        max: 200,
                                                        "decimal-separator":
                                                          ",",
                                                        "thousand-separator":
                                                          "",
                                                        "read-only-class":
                                                          "item-total",
                                                        min:
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "min"
                                                          ],
                                                        "read-only": ""
                                                      },
                                                      model: {
                                                        value:
                                                          _vm
                                                            .chargeSocialeDirigeant
                                                            .accroissement1,
                                                        callback: function(
                                                          $$v
                                                        ) {
                                                          _vm.$set(
                                                            _vm.chargeSocialeDirigeant,
                                                            "accroissement1",
                                                            _vm._n($$v)
                                                          )
                                                        },
                                                        expression:
                                                          "chargeSocialeDirigeant.accroissement1"
                                                      }
                                                    })
                                                  ],
                                                  1
                                                )
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              [
                                                _c(
                                                  "center",
                                                  [
                                                    _c("vue-numeric", {
                                                      attrs: {
                                                        currency:
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "currency"
                                                          ],
                                                        "currency-symbol-position":
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "currency-symbol-position"
                                                          ],
                                                        "output-type": "String",
                                                        "empty-value": "0",
                                                        precision:
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "precision"
                                                          ],
                                                        max: 200,
                                                        "decimal-separator":
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "decimal-separator"
                                                          ],
                                                        "thousand-separator":
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "thousand-separator"
                                                          ],
                                                        "read-only-class":
                                                          "item-total",
                                                        min:
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "min"
                                                          ],
                                                        "read-only": ""
                                                      },
                                                      model: {
                                                        value:
                                                          _vm
                                                            .chargeSocialeDirigeant
                                                            .annee2,
                                                        callback: function(
                                                          $$v
                                                        ) {
                                                          _vm.$set(
                                                            _vm.chargeSocialeDirigeant,
                                                            "annee2",
                                                            _vm._n($$v)
                                                          )
                                                        },
                                                        expression:
                                                          "chargeSocialeDirigeant.annee2"
                                                      }
                                                    })
                                                  ],
                                                  1
                                                )
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              [
                                                _c(
                                                  "center",
                                                  [
                                                    _c("vue-numeric", {
                                                      attrs: {
                                                        currency: "%",
                                                        "currency-symbol-position":
                                                          "suffix",
                                                        "output-type": "String",
                                                        "empty-value": "0",
                                                        minus: false,
                                                        precision: 1,
                                                        max: 200,
                                                        "decimal-separator":
                                                          ",",
                                                        "thousand-separator":
                                                          "",
                                                        "read-only-class":
                                                          "item-total",
                                                        min:
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "min"
                                                          ],
                                                        "read-only": ""
                                                      },
                                                      model: {
                                                        value:
                                                          _vm
                                                            .chargeSocialeDirigeant
                                                            .accroissement2,
                                                        callback: function(
                                                          $$v
                                                        ) {
                                                          _vm.$set(
                                                            _vm.chargeSocialeDirigeant,
                                                            "accroissement2",
                                                            _vm._n($$v)
                                                          )
                                                        },
                                                        expression:
                                                          "chargeSocialeDirigeant.accroissement2"
                                                      }
                                                    })
                                                  ],
                                                  1
                                                )
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              [
                                                _c(
                                                  "center",
                                                  [
                                                    _c("vue-numeric", {
                                                      attrs: {
                                                        currency:
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "currency"
                                                          ],
                                                        "currency-symbol-position":
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "currency-symbol-position"
                                                          ],
                                                        "output-type": "String",
                                                        "empty-value": "0",
                                                        precision:
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "precision"
                                                          ],
                                                        "decimal-separator":
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "decimal-separator"
                                                          ],
                                                        "thousand-separator":
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "thousand-separator"
                                                          ],
                                                        "read-only-class":
                                                          "item-total",
                                                        min:
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "min"
                                                          ],
                                                        "read-only": ""
                                                      },
                                                      model: {
                                                        value:
                                                          _vm
                                                            .chargeSocialeDirigeant
                                                            .annee3,
                                                        callback: function(
                                                          $$v
                                                        ) {
                                                          _vm.$set(
                                                            _vm.chargeSocialeDirigeant,
                                                            "annee3",
                                                            _vm._n($$v)
                                                          )
                                                        },
                                                        expression:
                                                          "chargeSocialeDirigeant.annee3"
                                                      }
                                                    })
                                                  ],
                                                  1
                                                )
                                              ],
                                              1
                                            )
                                          ]),
                                          _vm._v(" "),
                                          _c("tr", [
                                            _c(
                                              "td",
                                              { attrs: { colspan: "2" } },
                                              [
                                                _c("center", [
                                                  _vm._v(
                                                    " " +
                                                      _vm._s(
                                                        _vm.chargeSocialeEmploye
                                                          .titre
                                                      ) +
                                                      " "
                                                  )
                                                ])
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              [
                                                _c(
                                                  "center",
                                                  [
                                                    _c("vue-numeric", {
                                                      attrs: {
                                                        currency: "%",
                                                        "currency-symbol-position":
                                                          "suffix",
                                                        "output-type": "String",
                                                        "empty-value": "0",
                                                        minus: false,
                                                        precision: 1,
                                                        max: 200,
                                                        min: 0,
                                                        "decimal-separator":
                                                          ",",
                                                        "thousand-separator":
                                                          "",
                                                        "read-only-class":
                                                          "item-total",
                                                        "read-only": ""
                                                      },
                                                      model: {
                                                        value:
                                                          _vm
                                                            .chargeSocialeEmploye
                                                            .taux,
                                                        callback: function(
                                                          $$v
                                                        ) {
                                                          _vm.$set(
                                                            _vm.chargeSocialeEmploye,
                                                            "taux",
                                                            _vm._n($$v)
                                                          )
                                                        },
                                                        expression:
                                                          "chargeSocialeEmploye.taux"
                                                      }
                                                    })
                                                  ],
                                                  1
                                                )
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              [
                                                _c("center", [
                                                  _vm._v(
                                                    _vm._s(
                                                      _vm.chargeSocialeEmploye
                                                        .effectif
                                                    )
                                                  )
                                                ])
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c("td", {
                                              attrs: { colspan: "2" }
                                            }),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              [
                                                _c(
                                                  "center",
                                                  [
                                                    _c("vue-numeric", {
                                                      attrs: {
                                                        currency:
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "currency"
                                                          ],
                                                        "currency-symbol-position":
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "currency-symbol-position"
                                                          ],
                                                        "output-type": "String",
                                                        "empty-value": "0",
                                                        precision:
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "precision"
                                                          ],
                                                        "decimal-separator":
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "decimal-separator"
                                                          ],
                                                        "thousand-separator":
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "thousand-separator"
                                                          ],
                                                        "read-only-class":
                                                          "item-total",
                                                        min:
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "min"
                                                          ],
                                                        "read-only": ""
                                                      },
                                                      model: {
                                                        value:
                                                          _vm
                                                            .chargeSocialeEmploye
                                                            .annee1,
                                                        callback: function(
                                                          $$v
                                                        ) {
                                                          _vm.$set(
                                                            _vm.chargeSocialeEmploye,
                                                            "annee1",
                                                            _vm._n($$v)
                                                          )
                                                        },
                                                        expression:
                                                          "chargeSocialeEmploye.annee1"
                                                      }
                                                    })
                                                  ],
                                                  1
                                                )
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              [
                                                _c(
                                                  "center",
                                                  [
                                                    _c("vue-numeric", {
                                                      attrs: {
                                                        currency: "%",
                                                        "currency-symbol-position":
                                                          "suffix",
                                                        "output-type": "String",
                                                        "empty-value": "0",
                                                        minus: false,
                                                        precision: 1,
                                                        max: 200,
                                                        "decimal-separator":
                                                          ",",
                                                        "thousand-separator":
                                                          "",
                                                        "read-only-class":
                                                          "item-total",
                                                        min:
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "min"
                                                          ],
                                                        "read-only": ""
                                                      },
                                                      model: {
                                                        value:
                                                          _vm
                                                            .chargeSocialeEmploye
                                                            .accroissement1,
                                                        callback: function(
                                                          $$v
                                                        ) {
                                                          _vm.$set(
                                                            _vm.chargeSocialeEmploye,
                                                            "accroissement1",
                                                            _vm._n($$v)
                                                          )
                                                        },
                                                        expression:
                                                          "chargeSocialeEmploye.accroissement1"
                                                      }
                                                    })
                                                  ],
                                                  1
                                                )
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              [
                                                _c(
                                                  "center",
                                                  [
                                                    _c("vue-numeric", {
                                                      attrs: {
                                                        currency:
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "currency"
                                                          ],
                                                        "currency-symbol-position":
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "currency-symbol-position"
                                                          ],
                                                        "output-type": "String",
                                                        "empty-value": "0",
                                                        precision:
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "precision"
                                                          ],
                                                        max: 200,
                                                        "decimal-separator":
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "decimal-separator"
                                                          ],
                                                        "thousand-separator":
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "thousand-separator"
                                                          ],
                                                        "read-only-class":
                                                          "item-total",
                                                        min:
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "min"
                                                          ],
                                                        "read-only": ""
                                                      },
                                                      model: {
                                                        value:
                                                          _vm
                                                            .chargeSocialeEmploye
                                                            .annee2,
                                                        callback: function(
                                                          $$v
                                                        ) {
                                                          _vm.$set(
                                                            _vm.chargeSocialeEmploye,
                                                            "annee2",
                                                            _vm._n($$v)
                                                          )
                                                        },
                                                        expression:
                                                          "chargeSocialeEmploye.annee2"
                                                      }
                                                    })
                                                  ],
                                                  1
                                                )
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              [
                                                _c(
                                                  "center",
                                                  [
                                                    _c("vue-numeric", {
                                                      attrs: {
                                                        currency: "%",
                                                        "currency-symbol-position":
                                                          "suffix",
                                                        "output-type": "String",
                                                        "empty-value": "0",
                                                        minus: false,
                                                        precision: 1,
                                                        max: 200,
                                                        "decimal-separator":
                                                          ",",
                                                        "thousand-separator":
                                                          "",
                                                        "read-only-class":
                                                          "item-total",
                                                        min:
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "min"
                                                          ],
                                                        "read-only": ""
                                                      },
                                                      model: {
                                                        value:
                                                          _vm
                                                            .chargeSocialeEmploye
                                                            .accroissement2,
                                                        callback: function(
                                                          $$v
                                                        ) {
                                                          _vm.$set(
                                                            _vm.chargeSocialeEmploye,
                                                            "accroissement2",
                                                            _vm._n($$v)
                                                          )
                                                        },
                                                        expression:
                                                          "chargeSocialeEmploye.accroissement2"
                                                      }
                                                    })
                                                  ],
                                                  1
                                                )
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "td",
                                              [
                                                _c(
                                                  "center",
                                                  [
                                                    _c("vue-numeric", {
                                                      attrs: {
                                                        currency:
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "currency"
                                                          ],
                                                        "currency-symbol-position":
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "currency-symbol-position"
                                                          ],
                                                        "output-type": "String",
                                                        "empty-value": "0",
                                                        precision:
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "precision"
                                                          ],
                                                        "decimal-separator":
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "decimal-separator"
                                                          ],
                                                        "thousand-separator":
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "thousand-separator"
                                                          ],
                                                        "read-only-class":
                                                          "item-total",
                                                        min:
                                                          _vm
                                                            .currentCurrencyMask[
                                                            "min"
                                                          ],
                                                        "read-only": ""
                                                      },
                                                      model: {
                                                        value:
                                                          _vm
                                                            .chargeSocialeEmploye
                                                            .annee3,
                                                        callback: function(
                                                          $$v
                                                        ) {
                                                          _vm.$set(
                                                            _vm.chargeSocialeEmploye,
                                                            "annee3",
                                                            _vm._n($$v)
                                                          )
                                                        },
                                                        expression:
                                                          "chargeSocialeEmploye.annee3"
                                                      }
                                                    })
                                                  ],
                                                  1
                                                )
                                              ],
                                              1
                                            )
                                          ])
                                        ],
                                        2
                                      )
                                    ]
                                  )
                                ])
                              ]
                            )
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ])
                ],
                1
              )
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/assets/js/layouts/formmenufinancier.vue":
/*!***********************************************************!*\
  !*** ./resources/assets/js/layouts/formmenufinancier.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _formmenufinancier_vue_vue_type_template_id_75510f80___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./formmenufinancier.vue?vue&type=template&id=75510f80& */ "./resources/assets/js/layouts/formmenufinancier.vue?vue&type=template&id=75510f80&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _formmenufinancier_vue_vue_type_template_id_75510f80___WEBPACK_IMPORTED_MODULE_0__["render"],
  _formmenufinancier_vue_vue_type_template_id_75510f80___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/layouts/formmenufinancier.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/assets/js/layouts/formmenufinancier.vue?vue&type=template&id=75510f80&":
/*!******************************************************************************************!*\
  !*** ./resources/assets/js/layouts/formmenufinancier.vue?vue&type=template&id=75510f80& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_formmenufinancier_vue_vue_type_template_id_75510f80___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./formmenufinancier.vue?vue&type=template&id=75510f80& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/layouts/formmenufinancier.vue?vue&type=template&id=75510f80&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_formmenufinancier_vue_vue_type_template_id_75510f80___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_formmenufinancier_vue_vue_type_template_id_75510f80___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/assets/js/views/bp/financier/charges.vue":
/*!************************************************************!*\
  !*** ./resources/assets/js/views/bp/financier/charges.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _charges_vue_vue_type_template_id_3f866a26_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./charges.vue?vue&type=template&id=3f866a26&scoped=true& */ "./resources/assets/js/views/bp/financier/charges.vue?vue&type=template&id=3f866a26&scoped=true&");
/* harmony import */ var _charges_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./charges.vue?vue&type=script&lang=js& */ "./resources/assets/js/views/bp/financier/charges.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _charges_vue_vue_type_style_index_0_id_3f866a26_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./charges.vue?vue&type=style&index=0&id=3f866a26&scoped=true&lang=css& */ "./resources/assets/js/views/bp/financier/charges.vue?vue&type=style&index=0&id=3f866a26&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _charges_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _charges_vue_vue_type_template_id_3f866a26_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _charges_vue_vue_type_template_id_3f866a26_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "3f866a26",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/views/bp/financier/charges.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/assets/js/views/bp/financier/charges.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/assets/js/views/bp/financier/charges.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_charges_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./charges.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/financier/charges.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_charges_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/views/bp/financier/charges.vue?vue&type=style&index=0&id=3f866a26&scoped=true&lang=css&":
/*!*********************************************************************************************************************!*\
  !*** ./resources/assets/js/views/bp/financier/charges.vue?vue&type=style&index=0&id=3f866a26&scoped=true&lang=css& ***!
  \*********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_charges_vue_vue_type_style_index_0_id_3f866a26_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/style-loader!../../../../../../node_modules/css-loader??ref--7-1!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./charges.vue?vue&type=style&index=0&id=3f866a26&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/financier/charges.vue?vue&type=style&index=0&id=3f866a26&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_charges_vue_vue_type_style_index_0_id_3f866a26_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_charges_vue_vue_type_style_index_0_id_3f866a26_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_charges_vue_vue_type_style_index_0_id_3f866a26_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_charges_vue_vue_type_style_index_0_id_3f866a26_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_charges_vue_vue_type_style_index_0_id_3f866a26_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/assets/js/views/bp/financier/charges.vue?vue&type=template&id=3f866a26&scoped=true&":
/*!*******************************************************************************************************!*\
  !*** ./resources/assets/js/views/bp/financier/charges.vue?vue&type=template&id=3f866a26&scoped=true& ***!
  \*******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_charges_vue_vue_type_template_id_3f866a26_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./charges.vue?vue&type=template&id=3f866a26&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/bp/financier/charges.vue?vue&type=template&id=3f866a26&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_charges_vue_vue_type_template_id_3f866a26_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_charges_vue_vue_type_template_id_3f866a26_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);
//# sourceMappingURL=financier.charges.js.map