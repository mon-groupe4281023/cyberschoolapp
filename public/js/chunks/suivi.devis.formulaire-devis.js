(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["suivi.devis.formulaire-devis"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/components/StepNav.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/suivi/devis/components/StepNav.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "StepNav",
  props: ["activeIndex"],
  data: function data() {
    return {};
  },
  methods: {
    moveToStatistiquesPage: function moveToStatistiquesPage(index) {
      this.$router.push({
        name: "Statistiques"
      });
    },
    moveToModeldevisPage: function moveToModeldevisPage(index) {
      this.$router.push({
        name: "Devis"
      });
    },
    moveToOperationsPage: function moveToOperationsPage(index) {
      this.$router.push({
        name: "Operations"
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/components/headerInfos.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/suivi/devis/components/headerInfos.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "HeaderInfos",
  props: ['numeroFacture', 'imageUrl', 'informationsClients', 'prestation', 'objDevis'],
  data: function data() {
    return {
      filteredCustomers: [],
      projectId: JSON.parse(localStorage.getItem('userSession')).projet,
      clientRef: firebase__WEBPACK_IMPORTED_MODULE_0___default.a.firestore().collection('clients'),
      // modelfactureref: firebase.firestore().collection("devisfacture"),
      idDevis: localStorage.getItem("idDevis"),
      client: "",
      customers: [],
      // customersId: [],
      informations: "",
      infos: {},
      options: [{
        value: 'Particulier',
        label: 'Particulier'
      }, {
        value: 'Entreprise',
        label: 'Entreprise'
      }],
      type: ""
    };
  },
  methods: {
    //  Permet de filtrer les clients par type : Entreprise, Particulier
    filterCustomerByType: function filterCustomerByType(type) {
      this.client = "Nom Client";

      if (type === "Entreprise") {
        this.client = "Nom Entreprise";
      }

      this.filteredCustomers = this.customers.filter(function (customer) {
        return customer.type === type;
      });
    },
    // Recupération des infos du client sélectionné   
    selectCustomer: function selectCustomer(customer) {
      this.informationsClients.forEach(function (obj) {
        if (obj.key === "nom") {
          obj.inputName = customer.nom;
        } else if (obj.key === "phone") {
          obj.inputName = customer.tel1;
        } else if (obj.key === "adresse") {
          obj.inputName = customer.quartier;
        } else if (obj.key === "site") {
          obj.inputName = customer.site;
        }
      });
    }
  },
  created: function created() {
    var _this = this;

    //  Appear by default(Fields) when no customer is selected
    var header = this.objDevis;
    this.informations = header.enterpriseInfos; // Get customers stored in the DB

    var loader = Vue.$loading.show();
    this.clientRef.where("projectId", '==', this.projectId).onSnapshot(function (querySnapshot) {
      if (!querySnapshot.empty) {
        querySnapshot.forEach(function (doc) {
          var obj = doc.data();

          _this.customers.push(obj);

          _this.filteredCustomers.push(obj);

          loader.hide();
        });
      }
    });
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/components/secondTable.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/suivi/devis/components/secondTable.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _bp_mixins_currenciesMask__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../bp/mixins/currenciesMask */ "./resources/assets/js/views/bp/mixins/currenciesMask.js");
/* harmony import */ var _services_helper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../services/helper */ "./resources/assets/js/services/helper.js");
/* harmony import */ var vue_numeric__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue-numeric */ "./node_modules/vue-numeric/dist/vue-numeric.min.js");
/* harmony import */ var vue_numeric__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(vue_numeric__WEBPACK_IMPORTED_MODULE_2__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


 // import helper from "../../../../services/helper"

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'SecondTable',
  components: {
    VueNumeric: vue_numeric__WEBPACK_IMPORTED_MODULE_2___default.a
  },
  props: ["paiementsInfos"],
  mixins: [_bp_mixins_currenciesMask__WEBPACK_IMPORTED_MODULE_0__["default"]],
  data: function data() {
    return {
      part: 0
    };
  },
  methods: {
    calculPart: function calculPart() {
      if (this.paiementsInfos.paiement.montant && this.paiementsInfos.accompte.montant) {
        var reste = this.paiementsInfos.paiement.montant - this.paiementsInfos.accompte.montant;
        var part = Object(_services_helper__WEBPACK_IMPORTED_MODULE_1__["arround"])(reste / this.paiementsInfos.paiement.montant * 100, 2);
        this.$emit("getPart", part);
      }
    }
  },
  watch: {
    "paiementsInfos.paiement.montant": function paiementsInfosPaiementMontant() {
      this.calculPart();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/formulaire-devis.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/suivi/devis/formulaire-devis.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_StepNav__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/StepNav */ "./resources/assets/js/views/suivi/devis/components/StepNav.vue");
/* harmony import */ var _components_basicInfosTable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/basicInfosTable */ "./resources/assets/js/views/suivi/devis/components/basicInfosTable.vue");
/* harmony import */ var _components_headerInfos__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/headerInfos */ "./resources/assets/js/views/suivi/devis/components/headerInfos.vue");
/* harmony import */ var _components_secondTable__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/secondTable */ "./resources/assets/js/views/suivi/devis/components/secondTable.vue");
/* harmony import */ var _components_totalTable__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/totalTable */ "./resources/assets/js/views/suivi/devis/components/totalTable.vue");
/* harmony import */ var vue_numeric__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vue-numeric */ "./node_modules/vue-numeric/dist/vue-numeric.min.js");
/* harmony import */ var vue_numeric__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(vue_numeric__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _bp_mixins_currenciesMask__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../bp/mixins/currenciesMask */ "./resources/assets/js/views/bp/mixins/currenciesMask.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _pdf_pdfBuildBill__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./pdf/pdfBuildBill */ "./resources/assets/js/views/suivi/devis/pdf/pdfBuildBill.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
 // import PdfBuildBill from "./pdf/pdfBuildBill";









/* harmony default export */ __webpack_exports__["default"] = ({
  name: "FormulaireDevis",
  props: ['objDevis'],
  components: {
    BasicInfosTable: _components_basicInfosTable__WEBPACK_IMPORTED_MODULE_1__["default"],
    SecondTable: _components_secondTable__WEBPACK_IMPORTED_MODULE_3__["default"],
    HeaderInfos: _components_headerInfos__WEBPACK_IMPORTED_MODULE_2__["default"],
    TotalTable: _components_totalTable__WEBPACK_IMPORTED_MODULE_4__["default"],
    VueNumeric: vue_numeric__WEBPACK_IMPORTED_MODULE_5___default.a,
    StepNav: _components_StepNav__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  mixins: [_bp_mixins_currenciesMask__WEBPACK_IMPORTED_MODULE_6__["default"]],
  data: function data() {
    return {
      activeIndex: 2,
      stateMessage: "Désactiver la signature",
      numeroFacture: "",
      paiementsInfos: {
        "paiement": {
          "libelle": "Paiement",
          "montant": 0,
          "type": "paiement"
        },
        "accompte": {
          "libelle": "Acompte",
          "montant": 0,
          "type": "accompte"
        },
        "part": {
          "libelle": "Part(%)",
          "montant": 0,
          "type": "part"
        },
        "date": {
          "libelle": "Date de règlement",
          "value": "",
          "type": "date"
        }
      },
      switchState: true,
      informationsClients: [],
      prestation: {
        "value": ""
      },
      lesMontants: {
        "isVisible": "HT",
        // Afficher ou masquer le montant Ht et TTC facturé
        "remiseTotalMontant": {
          "value": 0
        },
        "isVisible3": "TVA",
        //Afficher la TVA ou la TPS
        "isVisible2": "TTC",
        //Afficher ou masquer le montant Ht et TTC à payer
        "montantTotalHt": {
          "value": 0
        },
        "montantTotalTtc": {
          "value": 0
        },
        "tva": {
          "montant": 0 // Montant de la TVA

        },
        "tps": {
          "montant": 0 // Montant de la TPS

        },
        "firstSelect": {
          // Le select se trouvant au niveau du montant Total facturé
          "type": "HT",
          "datas": [{
            "value": 'TTC',
            "label": 'TTC'
          }, {
            "value": 'HT',
            "label": 'HT'
          }]
        },
        "taxesSelectLibelle": {
          // Le select affichant les libellés des taux : TVA et TPS
          "libelle": "TVA",
          "datas": [{
            "value": 'TVA',
            "label": 'TVA'
          }, {
            "value": 'TPS',
            "label": 'TPS'
          }]
        },
        "taxesSelectTaux": {
          // Le select affichant les taux de TVA
          "tva": {
            "taux": 18,
            "datas": [{
              "value": '18',
              "label": '18'
            }, {
              "value": '9',
              "label": '9'
            }, {
              "value": '5',
              "label": '5'
            }]
          },
          "tps": {
            // Le select affichant le taux de TPS
            "taux": 9.5,
            "datas": [{
              "value": '9.5',
              "label": '9.5'
            }]
          }
        },
        "montantTotalSelect": {
          // Le select se trouvant au niveau des montant à payer
          "type": "TTC",
          "datas": [{
            "value": 'TTC',
            "label": 'TTC'
          }, {
            "value": 'HT',
            "label": 'HT'
          }]
        }
      },
      footer: "",
      imageUrl: "",
      signature: "",
      entrepriseInfos: "",
      active: 1,
      produitRef: firebase__WEBPACK_IMPORTED_MODULE_7___default.a.firestore().collection('financier'),
      mesProduits: [],
      clientRef: firebase__WEBPACK_IMPORTED_MODULE_7___default.a.firestore().collection('clients'),
      modelfactureref: firebase__WEBPACK_IMPORTED_MODULE_7___default.a.firestore().collection("devisfacture"),
      projectId: JSON.parse(localStorage.getItem('userSession')).projet,
      idDevis: localStorage.getItem("idDevis"),
      showRemise: false,
      facture: {},
      // montantRemise:{},
      devisFooter: {
        mentions: "",
        lieu: "",
        date: this.getCurrentDate()
      },
      getNewLibelle: [],
      visible: false,
      selectedElement: [],
      selectedElement2: [],
      sectionId: 1,
      subTotal: 0,
      rowIndex: 1,
      remiseExist: false,
      textarea: "",
      datas: [],
      // rate: 0,
      popover: false,
      // infoClient: [],
      value: "",
      prestations: [{
        value1: "Achat",
        label: "Achat"
      }, {
        value1: "Vente",
        label: "Vente"
      }, {
        value1: "Prestation de services",
        label: "Prestation de services"
      }],
      vendeurs: [{
        value2: "JES PRO",
        label: "JES PRO"
      }, {
        value2: "CODETEAM",
        label: "CODETEAM"
      }],
      payements: [{
        value3: "Cash",
        label: "Cash"
      }, {
        value3: "Mobile Money",
        label: "Mobile Money"
      }, {
        value3: "Virement Bancaire",
        label: "Virement Bancaire"
      }]
    };
  },
  methods: {
    addSectionInterligneAvant: function addSectionInterligneAvant(index) {
      var obj = {
        numeroSection: this.sectionId++,
        titleSection: "",
        subTotalIsVisible: true
      };
      this.datas.splice(index, 0, obj);
    },
    addSectionInterligneApres: function addSectionInterligneApres(index) {
      var obj = {
        numeroSection: this.sectionId++,
        titleSection: "",
        subTotalIsVisible: true
      };
      this.datas.splice(index + 1, 0, obj);
    },
    addInterligneAvant: function addInterligneAvant(index) {
      var obj = {
        numeroSection: this.sectionId++,
        titleSection: "",
        subTotalIsVisible: true
      };
      this.datas.splice(index + 1, 0, obj);
    },
    addInterligneApres: function addInterligneApres(index) {
      // let numero = this.rowIndex;
      //  if(this.sectionId-1 !== 0){
      //     numero = this.sectionId-1 + '.' + this.rowIndex
      // }
      var obj = {
        numero: "---",
        designation: "",
        unite: "",
        quantite: 1,
        prixVenteUnitaire: 0,
        montantRemise: 0,
        remise: 0,
        total: 0
      };
      this.datas.splice(index + 1, 0, obj);
    },
    getPart: function getPart(part) {
      this.paiementsInfos.part.montant = part;
    },
    goback: function goback() {
      this.active = 1;
      window.scrollTo(0, 0);
      this.numeroFacture = 'DEVIS N° ' + this.selectedElement[0].content;
    },
    changeState: function changeState() {
      if (!this.switchState) {
        this.stateMessage = 'Activer la signature';
      } else {
        this.stateMessage = 'Désactiver la signature';
      }
    },
    // Implémentation de la suppression de section
    removeSection: function removeSection(index) {
      this.datas.splice(index, 1); // this.datas.forEach(obj =>{
      //   if(obj.numeroSection){
      //      obj.numeroSection--
      //   }
      // })
    },
    // Suppression de la section et de tous ses enfants
    removeSectionChild: function removeSectionChild(numSection) {
      var _this = this;

      this.datas.forEach(function (element, index) {
        if (element.numero && element.numero.startsWith(numSection + ".")) {
          _this.datas.splice(index, 1);
        }
      });
    },
    displaySubtotal: function displaySubtotal(index) {
      if (this.datas[index].subTotalIsVisible === true) {
        this.datas[index].subTotalIsVisible = false;
      } else {
        this.datas[index].subTotalIsVisible = true;
      }
    },
    // Générateur de PDF
    printPdf: function printPdf(type) {
      var loader = Vue.$loading.show();
      var d = new Date();
      this.facture.infosFacture[3].content = d.toLocaleDateString();
      var discount = this.showRemise ? 'FCFA' : '%';
      var pdf = new _pdf_pdfBuildBill__WEBPACK_IMPORTED_MODULE_8__["default"]({
        data: this.facture,
        title: type
      }, type, {
        productsHeader: this.selectedElement2,
        mentions: {
          footer: this.footer,
          infoEnt: this.entrepriseInfos
        },
        montants: this.lesMontants,
        discount: discount
      });

      if (JSON.parse(localStorage.getItem('headersModelDevis')).logo) {
        pdf.logo = "data:image/png;base64," + JSON.parse(localStorage.getItem('headersModelDevis')).logo;
      } else {
        pdf.logo = '';
      }

      if (JSON.parse(localStorage.getItem('headersModelDevis')).signature && this.switchState) {
        pdf.signature = "data:image/png;base64," + JSON.parse(localStorage.getItem('headersModelDevis')).signature;
      } else {
        pdf.signature = '';
      }

      pdf.getDefaultFooter();
      pdf.openPdf();
      loader.hide();
    },
    // Sauvegarde du devis ou de la facture
    saveDevis: function saveDevis() {
      var _this2 = this;

      if (this.informationsClients.length !== 0 && this.prestation.value) {
        window.scrollTo(0, 0);
        this.facture = {
          idDevis: this.idDevis,
          projectId: this.projectId,
          titrePrestation: this.prestation.value,
          infosClient: this.informationsClients,
          infosFacture: this.selectedElement,
          produits: this.datas,
          paymentsInfos: this.paiementsInfos,
          footer: this.devisFooter
        }; // this.active = 2;

        this.numeroFacture = 'FACTURE N° ' + this.selectedElement[0].content;
        this.modelfactureref.where("idDevis", '==', this.idDevis).get().then(function (querySnapshot) {
          if (querySnapshot.empty) {
            _this2.modelfactureref.add(_this2.facture);
          } else {
            querySnapshot.forEach(function (doc) {
              _this2.modelfactureref.doc(doc.id).update(_this2.facture);
            });
          }
        });
      } else {
        Swal.fire({
          icon: 'info',
          text: 'Veuillez sélectionner le client et saisir la prestation avant de valider le devis'
        });
      }
    },
    getCurrentDate: function getCurrentDate() {
      var date = new Date().toLocaleDateString();
      return date;
    },
    // Implémentation de l'ajout de lignes
    addRows: function addRows() {
      // this.rowIndex = this.rowIndex + 1;
      var numero = this.rowIndex;

      if (this.sectionId - 1 !== 0) {
        numero = this.sectionId - 1 + '.' + this.rowIndex;
      }

      this.datas.push({
        numero: numero.toString(),
        designation: "",
        unite: "",
        quantite: 1,
        prixVenteUnitaire: 0,
        montantRemise: 0,
        remise: 0,
        total: 0
      });
      this.rowIndex++;
    },
    removeRows: function removeRows(index) {
      var _this3 = this;

      this.datas.splice(index, 1);
      var numero = 1;
      var numStart = "";
      var regex = /[1-9]\.[1-9]/;
      this.datas.forEach(function (obj, indice) {
        if (obj.numero) {
          if (!obj.numero.match(regex)) {
            obj.numero = indice + 1;
            _this3.rowIndex = _this3.datas[_this3.datas.length - 1].numero + 1;

            if (_this3.datas.length === 0) {
              _this3.rowIndex = 1;
            }
          } else {
            numStart = obj.numero.split(".")[0];
            _this3.rowIndex = numero + 1;
            obj.numero = numStart + "." + numero.toString();
            numero++;
          }
        }
      }); // Recommence l'indice de la ligne à 1 si le tableau ne possède aucun élément
      // if(this.datas.length !== 0){
      //   this.rowIndex = this.datas[this.datas.length -1].numero + 1;
      // }
      // else{
      //   this.rowIndex = 1
      // }
      // Renumérotation des lignes
      // this.datas.forEach(obj =>{
      //   if(!obj.numeroSection){
      //     if(obj.numero.includes(".")){
      //       let element = obj.numero.split(".")
      //       let numEnd = element[1];
      //       let numStart = element[0];
      //       numEnd = parseInt(numEnd, 10) -1;
      //       obj.numero = numStart + "." + numEnd.toString();
      //     }
      //     else{
      //       obj.numero = parseInt(obj.numero, 10)-1;
      //       let lastElement = this.datas[this.datas.length-1];
      //       let numOfLastElement = lastElement.numero;
      //       this.rowIndex = numOfLastElement+1;
      //     }
      //   }
      // });
      // Recalcul du sous total Section
      // this.calculSousTotalSection(this.datas[index].numeroSection)

      this.calculMontantTotalHt();
    },
    // Implémentation de l'ajout de sections
    addSection: function addSection(index) {
      this.rowIndex = 1;
      this.subTotal = 0;
      this.datas.push({
        numeroSection: this.sectionId++,
        titleSection: "",
        subTotalIsVisible: true
      });
      this.addRows();
    },
    next: function next() {
      if (this.active++ >= 3) this.active = 3;
      return this.active;
    },
    afficheMontantRemise: function afficheMontantRemise() {
      if (this.showRemise === true) {
        this.showRemise = false;
      } else {
        this.showRemise = true;
      }
    },
    // Calcul du total HT sur chaque ligne
    calculTotalRow: function calculTotalRow(objIndex) {
      var idSection = null;
      var obj = this.datas[objIndex];

      if (obj.prixVenteUnitaire !== "" && !isNaN(obj.prixVenteUnitaire)) {
        obj.total = obj.prixVenteUnitaire * obj.quantite;
        idSection = obj.numero.split(".");
      }

      this.calculMontantTotalHt(objIndex);
      this.calculSousTotalSection(idSection[0]);
    },
    //  Calcul du montant de la remise sur chaque ligne
    calculMontantRemise: function calculMontantRemise(objIndex) {
      this.datas.forEach(function (obj, index) {
        if (objIndex === index) {
          if (obj.remise !== "" && !isNaN(obj.remise)) {
            obj.montantRemise = obj.total * obj.remise / 100;
          }
        }
      });
      this.calculRemiseTotal();
    },
    calculSousTotalSection: function calculSousTotalSection(idSection) {
      var sum = 0;

      if (idSection) {
        idSection.toString();
      }

      this.datas.forEach(function (obj) {
        if (obj.numero && obj.numero.startsWith(idSection + ".")) {
          sum = sum + parseInt(obj.total, 10);
        }
      });
      this.subTotal = sum;
      this.datas.forEach(function (obj) {
        if (obj.numeroSection && obj.numeroSection == idSection) {
          obj.subTotal = sum;
        }
      });
    },
    // Calcul du montant Ht facturé
    calculMontantTotalHt: function calculMontantTotalHt() {
      var sum = 0;
      this.datas.forEach(function (element) {
        if (element.total !== "" && !isNaN(element.total)) sum = sum + parseInt(element.total, 10);
      });
      this.lesMontants.montantTotalHt.value = sum;
    },
    // Calcul de la remise total
    calculRemiseTotal: function calculRemiseTotal() {
      var sum = 0;
      this.datas.forEach(function (obj) {
        if (!obj.numeroSection) {
          //Check if it is not a section
          sum = sum + obj.montantRemise;
        }
      });
      this.lesMontants.remiseTotalMontant.value = sum;
    },
    // Sélectionne les produits qui se trouvent dans la DB
    getProduct: function getProduct(productName, index) {
      var _this4 = this;

      this.mesProduits.forEach(function (element) {
        if (element.nom === productName) {
          // element.quantite++;
          // Disable selected product
          // element.disabled = true;
          // Recupération des informations du produit;
          _this4.datas[index].prixVenteUnitaire = element.prixVenteUnitaire;
          _this4.datas[index].nom = element.nom;
          _this4.datas[index].quantite = 1;

          _this4.calculTotalRow(index);
        }
      });
    },
    // Génère les codes factures
    codeFactureGenerator: function codeFactureGenerator() {
      var letters = ["A", "B", "C", "D", "E", "F", "G"];
      var random = Math.ceil(Math.random() * 6);
      var alLetter = letters[random];
      var integer = Math.floor(Math.random() * 10);
      var code = integer + Math.floor(Math.random() * (70 - 11) + 11) + "345" + alLetter;
      return code;
    },
    defaultsInputs: function defaultsInputs(item) {
      return item !== 'quantite' && item !== 'designation' && item !== 'total' && item !== 'remise' && item !== 'numero' && item !== 'unite' && item !== 'prix';
    }
  },
  created: function created() {
    var _this5 = this;

    var header = this.objDevis;
    this.selectedElement = header.tableau1;
    this.selectedElement2 = header.tableau2;
    this.informationsClients = header.clientInfo;
    this.footer = header.footerInfo;

    if (header.signature !== null) {
      this.signature = header.signature;
    } // Recupération des informations de l'entreprise issue de la config du devis


    if (header.entrepriseInfos !== null) {
      this.entrepriseInfos = header.enterpriseInfos;
    }

    if (header.logo !== null) {
      this.imageUrl = header.logo;
    }
    /* Vérifie si la remise existe  
     Si True le champ remise Total est affiché dans le tableau des totaux
     Sinon il n'est pas affiché
    */


    this.selectedElement2.forEach(function (obj) {
      if (obj.libelle === "Remise%") {
        _this5.remiseExist = true;
      }
    }); // Affichage Date du jour et code Facture

    this.selectedElement.forEach(function (item) {
      if (item.type === "date") {
        var date = new Date();
        item.content = date;
      } else if (item.type === "code") {
        var generator = _this5.codeFactureGenerator();

        item.content = generator;
      }
    }); // Numero du Devis

    this.numeroFacture = 'DEVIS N° ' + this.selectedElement[0].content;
    /* Recupère les données du devis/facture : 
      - les produits
      - les informations saisi dans le premier tableau
      - les infos du tableau des paiements
    */

    if (!localStorage.getItem('facture' + this.idDevis)) {
      var loader = Vue.$loading.show();
      this.modelfactureref.where('idDevis', '==', this.idDevis).onSnapshot(function (querySnapshot) {
        if (!querySnapshot.empty) {
          querySnapshot.forEach(function (doc) {
            _this5.facture = doc.data();
            _this5.datas = _this5.facture.produits;
            _this5.selectedElement = _this5.facture.infosFacture;
            _this5.devisFooter = _this5.facture.footer;
            _this5.informationsClients = _this5.facture.infosClient;
            _this5.prestation.value = _this5.facture.titrePrestation; // Recupération du montant de l'accompte

            _this5.paiementsInfos.accompte.montant = _this5.facture.paymentsInfos.accompte.montant; // Recupération de la date de règlement

            _this5.paiementsInfos.date.value = _this5.facture.paymentsInfos.date.value; // Recupérer l'index de la dernière ligne
            //  if(this.datas.length !== 0){
            //     this.lastElement = this.datas[this.datas.length -1].numero;
            //  }
            //  let lastElement = this.datas[this.datas.length-1];
            //  let numOfLastElement = lastElement.numero;
            //  this.rowIndex = parseInt(numOfLastElement, 10)+1;
            // Appel de la fonction de calcul du montantTotalHt

            _this5.calculMontantTotalHt(); // End


            loader.hide();
          });
        } else {
          loader.hide();
        }
      });
    } // Recupère les produits entrés dans la BD
    // let loader = Vue.$loading.show();


    this.produitRef.doc(this.projectId).get().then(function (doc) {
      var obj = doc.data().produits;

      if (!obj.empty) {
        obj['articles'].forEach(function (produit, index) {
          _this5.mesProduits.push(produit); // loader.hide()

        });
      } //  else{
      //     loader.hide()
      //   }

    });
  },
  mounted: function mounted() {
    var _this6 = this;

    // Configuration
    // Clés permettant l'affichage des produits sur le devis
    this.selectedElement2.forEach(function (obj) {
      _this6.getNewLibelle.push(obj.type);
    });
  },
  watch: {
    "lesMontants.montantTotalTtc.value": function lesMontantsMontantTotalTtcValue() {
      this.paiementsInfos.paiement.montant = this.lesMontants.montantTotalTtc.value;
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/components/StepNav.vue?vue&type=style&index=0&id=589ce2d6&scoped=true&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/suivi/devis/components/StepNav.vue?vue&type=style&index=0&id=589ce2d6&scoped=true&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\nli.nav-item[data-v-589ce2d6]{\n  font-size: 18px!important;\n  width: 24.6%;\n  text-align: center;\n  background: white;\n  padding: 20px;\n}\n.step-nav[data-v-589ce2d6]{\n  box-shadow: 1px 0px 2px rgba(0, 0, 0, 0.3);\n  margin-top: 1%;\n  z-index: 2000;\n  background-color:#f5f7fa;\n}\n.step-nav[data-v-589ce2d6]:hover{\n  background-color: #198cf0!important;\n  color: white!important;\n  cursor: pointer;\n}\n.step-nav[data-v-589ce2d6]:active{\n  background-color: #198cf0!important;\n  color: white!important;\n  cursor: pointer;\n}\n.active[data-v-589ce2d6]{\n  background-color: #198cf0!important;\n  color: white!important;\n}\ndiv.content[data-v-589ce2d6]{\n  z-index: 1000;\n  width: 81%;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/components/headerInfos.vue?vue&type=style&index=0&id=5db00f37&scoped=true&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/suivi/devis/components/headerInfos.vue?vue&type=style&index=0&id=5db00f37&scoped=true&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\ntextarea[data-v-5db00f37]{\r\n  resize: vertical!important;\r\n  padding-left: 0!important;\r\n  border: 0!important;\r\n  width: 86%;\n}\n.title-head[data-v-5db00f37] {\r\n  background-color: #f2f6fc!important;\r\n  width: 228px !important;\r\n  color: black!important;\r\n  padding: 10px;\n}\ndiv.el-input-group__prepend i[data-v-5db00f37]{\r\n  font-size: 14px!important;\n}\n.input[data-v-5db00f37] {\r\n  width: 228px !important;\n}\r\n/* .input .el-input-group__prepend .el-input__inner{\r\n    border: none!important;\r\n} */\n.input-element[data-v-5db00f37] {\r\n  margin-left: 77%;\n}\n.bd-left[data-v-5db00f37]{\r\n  border-left: 2px solid #00c875;\r\n  padding-left: 2px;\r\n  width: 93.8%!important;\r\n  margin-bottom: 2%;\r\n  margin-left: 2.5%;\n}\n.item[data-v-5db00f37]{\r\n  color: black;\r\n  line-height: 1.5em;\r\n  font-weight: bolder;\n}\nspan[data-v-5db00f37]{\r\n  font-weight: bold;\r\n  font-size: 20px!important;\r\n  float: left;\r\n  padding-right: 1%;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/components/secondTable.vue?vue&type=style&index=0&id=ea5ec20e&scoped=true&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/suivi/devis/components/secondTable.vue?vue&type=style&index=0&id=ea5ec20e&scoped=true&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\ntable[data-v-ea5ec20e] {\n border-collapse: collapse;\n width: 45%; \n margin-top: 5%;\n}\n.style2[data-v-ea5ec20e]{\n  width: 18%!important;\n  color:black!important;\n}\n.style3[data-v-ea5ec20e]{\n  width: 34%!important;\n}\ntable th[data-v-ea5ec20e]{\n  padding: 5px;\n  font-size: 14px!important;\n}\ntable th[data-v-ea5ec20e], table td[data-v-ea5ec20e]{\n  border: 1px solid #ebeef5;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/formulaire-devis.vue?vue&type=style&index=0&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/suivi/devis/formulaire-devis.vue?vue&type=style&index=0&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\ntextarea.form-control{\r\n  resize: vertical!important;\r\n  padding-left: 10%;\r\n  display: block!important;\n}\n.select-icon .el-input input{\r\n  visibility: hidden;\r\n  width: 0;\r\n  padding: 0;\r\n  border: none;\n}\n.select-icon{\r\n  position:absolute;\r\n  top: 0;\r\n  left: 10%;\r\n  width: 0!important;\n}\n.image-devis{\r\n  width: 300px;\r\n  margin: auto;\n}\n.image-devis img{\r\n  border: 1px solid #409eff;\r\n  padding: 10px;\r\n  background: #409eff;\n}\n.modal-footer .btn-primary{\r\n  background-color: #00ba8b!important;\r\n  border: 1px solid #00ba8b!important;\n}\n.modal-footer{\r\n  width:80%;\r\n  margin: auto;\n}\n.icon-style{\r\n  cursor: pointer;\n}\n.el-button--info:focus{\r\n  border: 0!important;\n}\n.fade-enter-active, .fade-leave-active {\r\n  transition: opacity .5s;\n}\n.fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {\r\n  opacity: 0;\n}\n.checkbox{\r\n  position: absolute;\r\n  left: 0;\r\n  padding: 3px;\r\n  margin-left: 1%;\n}\n.delete-section{\r\n  position: absolute;\r\n  left: 10px;\n}\n.facture-title {\r\n  width: 200px !important;\r\n  font-weight: bold;\r\n  background-color: #f2f6fc;\r\n  text-align: center;\r\n  color: black;\r\n  padding: 10px;\n}\n.switchBtn{\r\n  position: absolute;\r\n  top: 0;\r\n  right: 20%;\n}\n.designation{\r\n  position: absolute;\r\n  right: 46px;\r\n  top: 7px;\r\n  padding: 4px!important;\n}\n.title {\r\n  background-color: #f2f6fc;\r\n  width: 228px !important;\r\n  color: black;\r\n  padding: 10px;\r\n  margin-top: 10%;\n}\n.marg{\r\n  margin-right: 2%!important;\n}\n.save {\r\n  background-color: #00ba8b;\r\n  border: 0;\r\n  padding: 6px 14px;\r\n  border-radius: 4px;\r\n  color: white;\r\n  font-weight: bold;\r\n  cursor: pointer;\n}\nbutton:hover{\r\n  cursor: pointer\n}\ntable {\r\n  border-collapse: collapse;\r\n  width: 93%;\r\n  margin-bottom: 1%;\r\n  margin-left: 3%;\n}\ntd input {\r\n  border: 0;\r\n  padding: 6px;\r\n  width: 100%;\n}\ntbody{\r\n  position: relative;\r\n  top: 0;\r\n  left: 0;\n}\ntable.base-info th,\r\ntable.base-info td {\r\n  position: relative;\r\n  top: 0;\r\n  left:  0;\r\n  border: 1px solid #ebeef5;\r\n  font-size: 14px!important;\n}\nth {\r\n  background-color: #409eff;\r\n  color: white;\r\n  text-align: left;\r\n  /* padding: 5px; */\r\n  position: relative;\r\n  top: 0;\r\n  left: 0;\n}\n.el-date-table th {\r\n  background-color: #f2f6fc !important;\n}\n.el-date-picker table {\r\n  width: 90% !important;\n}\n.inline {\r\n  position: relative;\r\n  top: 0;\r\n  left: 0;\r\n  width: 100%;\n}\n.total {\r\n  margin-right: 2%;\r\n  width: 50%!important;\n}\n.next {\r\n  background-color: #00ba8b !important;\r\n  color: white;\r\n  padding: 8px;\n}\na:hover {\r\n  color: white !important;\n}\n.mt-top {\r\n  margin-top: 10%;\n}\n.el-step__title {\r\n  font-weight: bold;\n}\n.el-steps--simple {\r\n  background-color: #409eff !important;\r\n  padding: 0px 1%!important;\n}\n.el-step.is-simple:not(:last-of-type) .el-step__title {\r\n    max-width: 90%!important; \r\n    padding: 7px!important;\r\n    padding-right: 10px!important;\n}\n.is-success {\r\n  color: white !important;\n}\n.el-step.is-simple .el-step__arrow::after,\r\n.el-step.is-simple .el-step__arrow::before {\r\n  background: white !important;\n}\n.el-step__title.is-process{color:#C0C4CC!important}\n.sign {\r\n  height: 200px;\r\n  position: relative;\r\n  top: 0;\r\n  left: 0;\n}\n.border-none {\r\n  border: 0;\n}\nhr.style-eight {\r\n  overflow: visible; /* For IE */\r\n  padding: 0;\r\n  border: none;\r\n  border-top: thin solid #333;\r\n  color: #333;\r\n  text-align: center;\r\n  width: 92%;\r\n  margin-left: 3%;\n}\n.footer-note {\r\n  font-size: 13px;\r\n  color: #111;\n}\ntable.base-info th{\r\n  padding: 5px;\n}\n.place {\r\n  position: absolute;\r\n  bottom: 17%;\r\n  width: 25%;\r\n  height: 100px;\r\n  margin-left: 1%;\n}\n::-moz-placeholder {\r\n  color: #606266 !important;\r\n  font-weight: normal;\r\n  font-size: 14px;\n}\n:-ms-input-placeholder {\r\n  color: #606266 !important;\r\n  font-weight: normal;\r\n  font-size: 14px;\n}\n::-ms-input-placeholder {\r\n  color: #606266 !important;\r\n  font-weight: normal;\r\n  font-size: 14px;\n}\n::placeholder {\r\n  color: #606266 !important;\r\n  font-weight: normal;\r\n  font-size: 14px;\n}\n.btn-minus {\r\n  position: absolute;\r\n  right: 0;\r\n  visibility: hidden;\r\n  padding: 3px; \r\n  margin-right: 2%;\n}\ntr:hover button {\r\n  visibility: visible;\n}\n.section-style{\r\n  width: auto!important;\r\n  background: transparent!important;\r\n  font-size: 16px!important;\n}\n#section-color{\r\n  background: #E4E7ED!important;\r\n  border: none!important;\n}\n#section-color:focus{\r\n  background: white!important;\r\n  border: none;\n}\n.el-input-group__append {\r\n    background: #E4E7ED;\r\n    border: none;\n}\n.el-input-group__prepend {\r\n    background: #E4E7ED;\r\n    border: none;\r\n    font-weight: bold;\r\n    font-size: 17px!important;\r\n    padding-left: 10px;\n}\n.box-card {\r\n  width: 100%;\n}\n.marg-left {\r\n  margin-left: 77%;\r\n  width: 80%;\n}\n.el-card {\r\n  border-radius: 0;\n}\n.bd-left {\r\n  border-left: 2px solid #00c875;\r\n  padding-left: 2px;\n}\n.dropdown-button {\r\n  padding: 0px !important;\r\n  padding-left: 6px !important;\r\n  margin-right: 5px !important;\n}\n.selection {\r\n  width: 100% !important;\n}\n.el-select{\r\n  width: 230px;\r\n  font-weight: normal!important;\n}\n.input-width {\r\n  width: 100%!important;\r\n  font-weight: normal!important;\n}\n.input-width .el-input__inner{\r\n  font-weight: normal!important;\n}\n.new-style{\r\n  background-color: red;\n}\n.numero-ligne{\r\n  width: 40px!important;\r\n  text-align: right;\n}\n.unite-col{\r\n  width: 60px!important;\n}\n.remise-col{\r\n  width: 90px!important;\n}\n.total-col{\r\n  width: 220px!important;\r\n  /* text-align: right!important; */\n}\n.el-input-number{\r\n  width: 120px!important;\n}\n.showRemiseButton{\r\n  position: absolute;\r\n  top: 2px;\r\n  right: 5px; \r\n  padding: 1px!important;\r\n  visibility: hidden;\n}\ntd:hover .showRemiseButton{\r\n  visibility: visible;\n} \r\n/* .code-width{\r\n  width: 80px!important;\r\n} */\n.styling1{\r\n  width: 8%!important;\r\n  padding: 0px 20px!\n}\n.styling2{\r\n  width: 21%!important;\n}\n.styling4{\r\n  width: 15%!important;\n}\n.styling3, .styling5{\r\n  width: 19%!important;\n}\n.styling8{\r\n  width: 40%!important;\n}\n.styling10{\r\n  width: 3%!important;\n}\n.styling11{\r\n  width: 20%!important;\n}\n.el-date-editor.el-input, .el-date-editor.el-input__inner {\r\n    width: 100%!important;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/components/StepNav.vue?vue&type=style&index=0&id=589ce2d6&scoped=true&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/suivi/devis/components/StepNav.vue?vue&type=style&index=0&id=589ce2d6&scoped=true&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader??ref--7-1!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./StepNav.vue?vue&type=style&index=0&id=589ce2d6&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/components/StepNav.vue?vue&type=style&index=0&id=589ce2d6&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/components/headerInfos.vue?vue&type=style&index=0&id=5db00f37&scoped=true&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/suivi/devis/components/headerInfos.vue?vue&type=style&index=0&id=5db00f37&scoped=true&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader??ref--7-1!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./headerInfos.vue?vue&type=style&index=0&id=5db00f37&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/components/headerInfos.vue?vue&type=style&index=0&id=5db00f37&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/components/secondTable.vue?vue&type=style&index=0&id=ea5ec20e&scoped=true&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/suivi/devis/components/secondTable.vue?vue&type=style&index=0&id=ea5ec20e&scoped=true&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader??ref--7-1!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./secondTable.vue?vue&type=style&index=0&id=ea5ec20e&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/components/secondTable.vue?vue&type=style&index=0&id=ea5ec20e&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/formulaire-devis.vue?vue&type=style&index=0&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/suivi/devis/formulaire-devis.vue?vue&type=style&index=0&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader??ref--7-1!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./formulaire-devis.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/formulaire-devis.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/components/StepNav.vue?vue&type=template&id=589ce2d6&scoped=true&":
/*!***********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/suivi/devis/components/StepNav.vue?vue&type=template&id=589ce2d6&scoped=true& ***!
  \***********************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "ml-3 mt-2 position-fixed content" }, [
    _c("ul", { staticClass: "nav customtab", attrs: { role: "tablist" } }, [
      _c(
        "li",
        {
          class: ["nav-item", "step-nav", { active: _vm.activeIndex === 1 }],
          on: {
            click: function($event) {
              return _vm.moveToStatistiquesPage((_vm.index = 1))
            }
          }
        },
        [
          _vm._m(0),
          _vm._v(" "),
          _c("span", { staticClass: "hidden-xs-down" }, [
            _vm._v("Statitisques")
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "li",
        {
          class: ["nav-item", "step-nav", { active: _vm.activeIndex === 2 }],
          on: {
            click: function($event) {
              return _vm.moveToModeldevisPage((_vm.index = 2))
            }
          }
        },
        [
          _vm._m(1),
          _vm._v(" "),
          _c("span", { staticClass: "hidden-xs-down" }, [
            _vm._v("Créer Devis / Factures")
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "li",
        {
          class: ["nav-item", "step-nav", { active: _vm.activeIndex === 3 }],
          on: {
            click: function($event) {
              return _vm.moveToOperationsPage((_vm.index = 3))
            }
          }
        },
        [
          _vm._m(2),
          _vm._v(" "),
          _c("span", { staticClass: "hidden-xs-down" }, [_vm._v("Opérations")])
        ]
      ),
      _vm._v(" "),
      _vm._m(3)
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", [_c("i", { staticClass: " fas fa-chart-bar" })])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", [_c("i", { staticClass: "fas fa-plus-circle" })])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", [_c("i", { staticClass: "fas fa-retweet" })])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "nav-item step-nav" }, [
      _c("span", [_c("i", { staticClass: "fas fa-cog" })]),
      _vm._v(" "),
      _c("span", { staticClass: "hidden-xs-down" }, [_vm._v("Paramétrages")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/components/headerInfos.vue?vue&type=template&id=5db00f37&scoped=true&":
/*!***************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/suivi/devis/components/headerInfos.vue?vue&type=template&id=5db00f37&scoped=true& ***!
  \***************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("el-col", { attrs: { span: 12 } }, [
        _c("div", { staticClass: "pl-5" }, [
          _c("img", {
            attrs: {
              src: "data:image/png;base64," + _vm.imageUrl,
              width: "364",
              height: "152"
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "grid-content pl-3 mb-3" }, [
          _c(
            "div",
            {
              staticClass: "pl-5",
              domProps: { innerHTML: _vm._s(_vm.informations) }
            },
            [_vm._v(_vm._s(_vm.informations))]
          )
        ])
      ]),
      _vm._v(" "),
      _c(
        "div",
        [
          _c(
            "el-col",
            { attrs: { span: 8 } },
            [
              _c(
                "div",
                { staticClass: "grid-content marg-left mb-1" },
                [
                  _c(
                    "div",
                    {
                      staticClass:
                        "title-head font-weight-bold text-center mb-4"
                    },
                    [_vm._v(_vm._s(_vm.numeroFacture))]
                  ),
                  _vm._v(" "),
                  _c(
                    "el-select",
                    {
                      staticClass: "mb-2",
                      attrs: { placeholder: "Par type Client", size: "mini" },
                      model: {
                        value: _vm.type,
                        callback: function($$v) {
                          _vm.type = $$v
                        },
                        expression: "type"
                      }
                    },
                    _vm._l(_vm.options, function(item) {
                      return _c("el-option", {
                        key: item.value,
                        attrs: { label: item.label, value: item.value },
                        nativeOn: {
                          click: function($event) {
                            return _vm.filterCustomerByType(item.value)
                          }
                        }
                      })
                    }),
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-select",
                    {
                      attrs: {
                        size: "mini",
                        placeholder: "Sélectionner un client"
                      },
                      model: {
                        value: _vm.informationsClients[0].inputName,
                        callback: function($$v) {
                          _vm.$set(_vm.informationsClients[0], "inputName", $$v)
                        },
                        expression: "informationsClients[0].inputName"
                      }
                    },
                    _vm._l(_vm.filteredCustomers, function(customer, index) {
                      return _c("el-option", {
                        key: index,
                        attrs: { label: customer.nom, value: customer.nom },
                        nativeOn: {
                          click: function($event) {
                            return _vm.selectCustomer(customer)
                          }
                        }
                      })
                    }),
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _vm._l(_vm.informationsClients, function(element, index) {
                return _c("div", { key: index, staticClass: "input-element" }, [
                  _c(
                    "div",
                    { staticClass: "mt-1" },
                    [
                      element.state && element.key !== "nom"
                        ? _c(
                            "el-input",
                            {
                              staticClass: "input",
                              attrs: { size: "mini", readonly: "" },
                              model: {
                                value: element.inputName,
                                callback: function($$v) {
                                  _vm.$set(element, "inputName", $$v)
                                },
                                expression: "element.inputName"
                              }
                            },
                            [
                              element.key !== "nom"
                                ? _c("template", { slot: "prepend" }, [
                                    _c("i", { class: element.icon })
                                  ])
                                : _vm._e()
                            ],
                            2
                          )
                        : _vm._e()
                    ],
                    1
                  )
                ])
              })
            ],
            2
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("el-card", { staticClass: "bd-left", attrs: { shadow: "always" } }, [
        _c("span", { staticClass: "before" }, [_vm._v("Prestation : ")]),
        _vm._v(" "),
        _c("textarea", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.prestation.value,
              expression: "prestation.value"
            }
          ],
          staticClass: "form-control pl-3",
          attrs: {
            id: "message-text",
            rows: "1",
            placeholder: "Saisir la prestation"
          },
          domProps: { value: _vm.prestation.value },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.prestation, "value", $event.target.value)
            }
          }
        })
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/components/secondTable.vue?vue&type=template&id=ea5ec20e&scoped=true&":
/*!***************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/suivi/devis/components/secondTable.vue?vue&type=template&id=ea5ec20e&scoped=true& ***!
  \***************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("table", [
      _c("tr", [
        _c("th", { staticClass: "font-weight-bold" }, [
          _vm._v(
            "\n              " +
              _vm._s(_vm.paiementsInfos.paiement.libelle) +
              "\n          "
          )
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "font-weight-bold" }, [
          _vm._v(
            "\n              " +
              _vm._s(_vm.paiementsInfos.accompte.libelle) +
              "\n          "
          )
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "font-weight-bold" }, [
          _vm._v(
            "\n              " +
              _vm._s(_vm.paiementsInfos.part.libelle) +
              "\n          "
          )
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "font-weight-bold" }, [
          _vm._v(
            "\n              " +
              _vm._s(_vm.paiementsInfos.date.libelle) +
              "\n          "
          )
        ])
      ]),
      _vm._v(" "),
      _c("tr", [
        _c(
          "td",
          [
            _c("vue-numeric", {
              staticClass: "text-right text-dark",
              attrs: {
                currency: _vm.currentCurrencyMask["currency"],
                "currency-symbol-position":
                  _vm.currentCurrencyMask["currency-symbol-position"],
                "output-type": "Number",
                "empty-value": "0",
                precision: _vm.currentCurrencyMask["precision"],
                "decimal-separator":
                  _vm.currentCurrencyMask["decimal-separator"],
                "thousand-separator":
                  _vm.currentCurrencyMask["thousand-separator"],
                readonly: ""
              },
              model: {
                value: _vm.paiementsInfos.paiement.montant,
                callback: function($$v) {
                  _vm.$set(_vm.paiementsInfos.paiement, "montant", $$v)
                },
                expression: "paiementsInfos.paiement.montant"
              }
            })
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "td",
          [
            _c("vue-numeric", {
              staticClass: "text-right",
              attrs: {
                currency: _vm.currentCurrencyMask["currency"],
                "currency-symbol-position":
                  _vm.currentCurrencyMask["currency-symbol-position"],
                "output-type": "Number",
                "empty-value": "0",
                precision: _vm.currentCurrencyMask["precision"],
                "decimal-separator":
                  _vm.currentCurrencyMask["decimal-separator"],
                "thousand-separator":
                  _vm.currentCurrencyMask["thousand-separator"]
              },
              on: {
                input: function($event) {
                  return _vm.calculPart()
                }
              },
              model: {
                value: _vm.paiementsInfos.accompte.montant,
                callback: function($$v) {
                  _vm.$set(_vm.paiementsInfos.accompte, "montant", $$v)
                },
                expression: "paiementsInfos.accompte.montant"
              }
            })
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "td",
          { staticClass: "style2" },
          [
            _c("vue-numeric", {
              staticClass: "text-right",
              attrs: {
                currency: "%",
                "currency-symbol-position": "suffix",
                minus: false,
                "empty-value": "0",
                precision: 2,
                readonly: ""
              },
              model: {
                value: _vm.paiementsInfos.part.montant,
                callback: function($$v) {
                  _vm.$set(_vm.paiementsInfos.part, "montant", $$v)
                },
                expression: "paiementsInfos.part.montant"
              }
            })
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "td",
          { staticClass: "style3" },
          [
            _c("el-date-picker", {
              attrs: {
                format: "dd MMM yyyy",
                "value-format": "dd MMM yyyy",
                type: "date",
                placeholder: "Choisir la date"
              },
              model: {
                value: _vm.paiementsInfos.date.value,
                callback: function($$v) {
                  _vm.$set(_vm.paiementsInfos.date, "value", $$v)
                },
                expression: "paiementsInfos.date.value"
              }
            })
          ],
          1
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/formulaire-devis.vue?vue&type=template&id=7e66b440&":
/*!*********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/views/suivi/devis/formulaire-devis.vue?vue&type=template&id=7e66b440& ***!
  \*********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "page-wrapper" },
    [
      _c("StepNav", { attrs: { activeIndex: _vm.activeIndex } }),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "container-fluid", staticStyle: { "margin-top": "7%" } },
        [
          _c("div", { staticClass: "row" }, [
            _c(
              "div",
              { staticClass: "col-12" },
              [
                _c(
                  "el-steps",
                  {
                    attrs: {
                      active: _vm.active,
                      direction: "vertical",
                      "finish-status": "success",
                      simple: ""
                    }
                  },
                  [
                    _c("el-step", { attrs: { title: " 1 - Devis" } }),
                    _vm._v(" "),
                    _c("el-step", { attrs: { title: " 2 - Facture" } }),
                    _vm._v(" "),
                    _c("el-step", { attrs: { title: " 3 - Bon de Livraison" } })
                  ],
                  1
                ),
                _vm._v(" "),
                _vm.active === 1
                  ? _c("div", { staticClass: "card" }, [
                      _c(
                        "div",
                        { staticClass: "card-body" },
                        [
                          _c(
                            "el-row",
                            { attrs: { gutter: 30 } },
                            [
                              _c("HeaderInfos", {
                                attrs: {
                                  objDevis: _vm.objDevis,
                                  prestation: _vm.prestation,
                                  informationsClients: _vm.informationsClients,
                                  imageUrl: _vm.imageUrl,
                                  numeroFacture: _vm.numeroFacture
                                }
                              })
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("div", [
                        _c("div", { staticClass: "element mb-5" }, [
                          _c("table", { staticClass: "base-info" }, [
                            _c(
                              "tr",
                              _vm._l(_vm.selectedElement, function(
                                item,
                                index
                              ) {
                                return _c(
                                  "th",
                                  {
                                    key: index,
                                    staticClass: "font-weight-bold"
                                  },
                                  [_vm._v(_vm._s(item.libelle))]
                                )
                              }),
                              0
                            ),
                            _vm._v(" "),
                            _c(
                              "tr",
                              _vm._l(_vm.selectedElement, function(
                                item,
                                index
                              ) {
                                return _c(
                                  "td",
                                  { key: index, class: "styling" + item.id },
                                  [
                                    item.type === "date"
                                      ? _c("el-date-picker", {
                                          attrs: {
                                            format: "dd MMM yyyy",
                                            "value-format": "dd MMM yyyy",
                                            type: "date",
                                            placeholder: "Choisir une date"
                                          },
                                          model: {
                                            value: item.content,
                                            callback: function($$v) {
                                              _vm.$set(item, "content", $$v)
                                            },
                                            expression: "item.content"
                                          }
                                        })
                                      : _vm._e(),
                                    _vm._v(" "),
                                    item.type === "code"
                                      ? _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: item.content,
                                              expression: "item.content"
                                            }
                                          ],
                                          staticClass: "code-width",
                                          attrs: { type: "text", readonly: "" },
                                          domProps: { value: item.content },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                item,
                                                "content",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      : _vm._e(),
                                    _vm._v(" "),
                                    item.type === "lieu"
                                      ? _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: item.content,
                                              expression: "item.content"
                                            }
                                          ],
                                          attrs: { type: "text" },
                                          domProps: { value: item.content },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                item,
                                                "content",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      : _vm._e(),
                                    _vm._v(" "),
                                    item.type === "prestation"
                                      ? _c(
                                          "el-select",
                                          {
                                            staticClass: "selection",
                                            attrs: {
                                              placeholder:
                                                "Choisir une prestation"
                                            },
                                            model: {
                                              value: item.content,
                                              callback: function($$v) {
                                                _vm.$set(item, "content", $$v)
                                              },
                                              expression: "item.content"
                                            }
                                          },
                                          _vm._l(_vm.prestations, function(
                                            item
                                          ) {
                                            return _c("el-option", {
                                              key: item.value1,
                                              attrs: {
                                                label: item.label,
                                                value: item.value1,
                                                disabled: item.disabled
                                              }
                                            })
                                          }),
                                          1
                                        )
                                      : _vm._e(),
                                    _vm._v(" "),
                                    item.type === "vendeur"
                                      ? _c(
                                          "el-select",
                                          {
                                            staticClass: "selection",
                                            attrs: {
                                              placeholder: "Choisir un vendeur"
                                            },
                                            model: {
                                              value: item.content,
                                              callback: function($$v) {
                                                _vm.$set(item, "content", $$v)
                                              },
                                              expression: "item.content"
                                            }
                                          },
                                          _vm._l(_vm.vendeurs, function(item) {
                                            return _c("el-option", {
                                              key: item.value2,
                                              attrs: {
                                                label: item.label,
                                                value: item.value2,
                                                disabled: item.disabled
                                              }
                                            })
                                          }),
                                          1
                                        )
                                      : _vm._e(),
                                    _vm._v(" "),
                                    item.type === "payement"
                                      ? _c(
                                          "el-select",
                                          {
                                            staticClass: "selection",
                                            attrs: {
                                              placeholder: "Choisir le payement"
                                            },
                                            model: {
                                              value: item.content,
                                              callback: function($$v) {
                                                _vm.$set(item, "content", $$v)
                                              },
                                              expression: "item.content"
                                            }
                                          },
                                          _vm._l(_vm.payements, function(item) {
                                            return _c("el-option", {
                                              key: item.value3,
                                              attrs: {
                                                label: item.label,
                                                value: item.value3,
                                                disabled: item.disabled
                                              }
                                            })
                                          }),
                                          1
                                        )
                                      : _vm._e()
                                  ],
                                  1
                                )
                              }),
                              0
                            )
                          ])
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "mt-4 element" },
                          [
                            _c(
                              "table",
                              { staticClass: "base-info" },
                              [
                                _c(
                                  "tr",
                                  _vm._l(_vm.selectedElement2, function(
                                    item,
                                    index
                                  ) {
                                    return _c(
                                      "th",
                                      {
                                        key: index,
                                        staticClass: "font-weight-bold",
                                        class: "styling" + item.id
                                      },
                                      [
                                        _vm._v(
                                          _vm._s(item.libelle) +
                                            "\n                    "
                                        ),
                                        item.libelle === "Remise%"
                                          ? _c("el-button", {
                                              staticClass: "showRemiseButton",
                                              attrs: {
                                                type: "info",
                                                icon: "el-icon-caret-bottom",
                                                plain: ""
                                              },
                                              on: {
                                                click: function($event) {
                                                  return _vm.afficheMontantRemise()
                                                }
                                              }
                                            })
                                          : _vm._e()
                                      ],
                                      1
                                    )
                                  }),
                                  0
                                ),
                                _vm._v(" "),
                                _vm._l(_vm.datas, function(produit, index) {
                                  return _c("tbody", { key: index }, [
                                    _c(
                                      "tr",
                                      [
                                        produit.numeroSection
                                          ? _c(
                                              "td",
                                              {
                                                attrs: {
                                                  colspan:
                                                    _vm.selectedElement2.length
                                                }
                                              },
                                              [
                                                _c(
                                                  "el-input",
                                                  {
                                                    attrs: {
                                                      placeholder:
                                                        "Saisir le titre de la section",
                                                      id: "section-color",
                                                      size: "small",
                                                      type: "text"
                                                    },
                                                    model: {
                                                      value:
                                                        produit.titleSection,
                                                      callback: function($$v) {
                                                        _vm.$set(
                                                          produit,
                                                          "titleSection",
                                                          $$v
                                                        )
                                                      },
                                                      expression:
                                                        "produit.titleSection"
                                                    }
                                                  },
                                                  [
                                                    _c(
                                                      "template",
                                                      { slot: "prepend" },
                                                      [
                                                        _vm._v(
                                                          _vm._s(
                                                            produit.numeroSection
                                                          )
                                                        )
                                                      ]
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "template",
                                                      { slot: "append" },
                                                      [
                                                        !produit.subTotalIsVisible
                                                          ? _c(
                                                              "span",
                                                              {
                                                                staticClass:
                                                                  "h5"
                                                              },
                                                              [_vm._v("...")]
                                                            )
                                                          : _vm._e(),
                                                        _vm._v(" "),
                                                        produit.subTotalIsVisible
                                                          ? _c("vue-numeric", {
                                                              staticClass:
                                                                "text-right font-weight-bold section-style",
                                                              attrs: {
                                                                currency:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "currency"
                                                                  ],
                                                                "currency-symbol-position":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "currency-symbol-position"
                                                                  ],
                                                                "output-type":
                                                                  "Number",
                                                                "empty-value":
                                                                  "0",
                                                                precision:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "precision"
                                                                  ],
                                                                "decimal-separator":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "decimal-separator"
                                                                  ],
                                                                "thousand-separator":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "thousand-separator"
                                                                  ],
                                                                readonly: ""
                                                              },
                                                              model: {
                                                                value:
                                                                  produit.subTotal,
                                                                callback: function(
                                                                  $$v
                                                                ) {
                                                                  _vm.$set(
                                                                    produit,
                                                                    "subTotal",
                                                                    $$v
                                                                  )
                                                                },
                                                                expression:
                                                                  "produit.subTotal"
                                                              }
                                                            })
                                                          : _vm._e(),
                                                        _vm._v(" "),
                                                        !produit.subTotalIsVisible
                                                          ? _c(
                                                              "span",
                                                              {
                                                                staticClass:
                                                                  "icon-style",
                                                                on: {
                                                                  click: function(
                                                                    $event
                                                                  ) {
                                                                    return _vm.displaySubtotal(
                                                                      index
                                                                    )
                                                                  }
                                                                }
                                                              },
                                                              [
                                                                _c("i", {
                                                                  staticClass:
                                                                    "fa fa-eye"
                                                                })
                                                              ]
                                                            )
                                                          : _vm._e(),
                                                        _vm._v(" "),
                                                        produit.subTotalIsVisible
                                                          ? _c(
                                                              "span",
                                                              {
                                                                staticClass:
                                                                  "icon-style",
                                                                on: {
                                                                  click: function(
                                                                    $event
                                                                  ) {
                                                                    return _vm.displaySubtotal(
                                                                      index
                                                                    )
                                                                  }
                                                                }
                                                              },
                                                              [
                                                                _c("i", {
                                                                  staticClass:
                                                                    "fa fa-eye-slash"
                                                                })
                                                              ]
                                                            )
                                                          : _vm._e()
                                                      ],
                                                      1
                                                    )
                                                  ],
                                                  2
                                                )
                                              ],
                                              1
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        produit.numeroSection
                                          ? _c(
                                              "el-dropdown",
                                              {
                                                staticClass: "delete-section",
                                                attrs: { size: "small" }
                                              },
                                              [
                                                _c(
                                                  "el-dropdown-menu",
                                                  {
                                                    attrs: { slot: "dropdown" },
                                                    slot: "dropdown"
                                                  },
                                                  [
                                                    _c(
                                                      "transition",
                                                      {
                                                        attrs: { name: "fade" }
                                                      },
                                                      [
                                                        _c(
                                                          "el-popover",
                                                          {
                                                            staticClass:
                                                              "bnt-minus",
                                                            attrs: {
                                                              transition:
                                                                "el-fade-in-linear",
                                                              placement: "top",
                                                              width: "260"
                                                            },
                                                            model: {
                                                              value:
                                                                produit.visible,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  produit,
                                                                  "visible",
                                                                  $$v
                                                                )
                                                              },
                                                              expression:
                                                                "produit.visible"
                                                            }
                                                          },
                                                          [
                                                            _c("p", [
                                                              _vm._v(
                                                                "Voulez-vous vraiment supprimer?"
                                                              )
                                                            ]),
                                                            _vm._v(" "),
                                                            _c(
                                                              "div",
                                                              {
                                                                staticStyle: {
                                                                  "text-align":
                                                                    "right",
                                                                  margin: "0"
                                                                }
                                                              },
                                                              [
                                                                _c(
                                                                  "el-button",
                                                                  {
                                                                    attrs: {
                                                                      size:
                                                                        "mini",
                                                                      type:
                                                                        "text"
                                                                    },
                                                                    on: {
                                                                      click: function(
                                                                        $event
                                                                      ) {
                                                                        produit.visible = false
                                                                      }
                                                                    }
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      "Annuler"
                                                                    )
                                                                  ]
                                                                ),
                                                                _vm._v(" "),
                                                                _c(
                                                                  "el-button",
                                                                  {
                                                                    attrs: {
                                                                      type:
                                                                        "primary",
                                                                      size:
                                                                        "mini"
                                                                    },
                                                                    on: {
                                                                      click: function(
                                                                        $event
                                                                      ) {
                                                                        produit.visible = false
                                                                        _vm.removeSection(
                                                                          index
                                                                        )
                                                                      }
                                                                    }
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      "Confirmer"
                                                                    )
                                                                  ]
                                                                )
                                                              ],
                                                              1
                                                            ),
                                                            _vm._v(" "),
                                                            _c(
                                                              "el-dropdown-item",
                                                              {
                                                                attrs: {
                                                                  slot:
                                                                    "reference"
                                                                },
                                                                slot:
                                                                  "reference"
                                                              },
                                                              [
                                                                _vm._v(
                                                                  "Supprimer la section"
                                                                )
                                                              ]
                                                            )
                                                          ],
                                                          1
                                                        )
                                                      ],
                                                      1
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "transition",
                                                      {
                                                        attrs: { name: "fade" }
                                                      },
                                                      [
                                                        _c(
                                                          "el-popover",
                                                          {
                                                            staticClass:
                                                              "bnt-minus",
                                                            attrs: {
                                                              transition:
                                                                "el-fade-in-linear",
                                                              placement: "top",
                                                              width: "260"
                                                            },
                                                            model: {
                                                              value:
                                                                produit.visible2,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  produit,
                                                                  "visible2",
                                                                  $$v
                                                                )
                                                              },
                                                              expression:
                                                                "produit.visible2"
                                                            }
                                                          },
                                                          [
                                                            _c("p", [
                                                              _vm._v(
                                                                "Voulez-vous vraiment supprimer?"
                                                              )
                                                            ]),
                                                            _vm._v(" "),
                                                            _c(
                                                              "div",
                                                              {
                                                                staticStyle: {
                                                                  "text-align":
                                                                    "right",
                                                                  margin: "0"
                                                                }
                                                              },
                                                              [
                                                                _c(
                                                                  "el-button",
                                                                  {
                                                                    attrs: {
                                                                      size:
                                                                        "mini",
                                                                      type:
                                                                        "text"
                                                                    },
                                                                    on: {
                                                                      click: function(
                                                                        $event
                                                                      ) {
                                                                        produit.visible2 = false
                                                                      }
                                                                    }
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      "Annuler"
                                                                    )
                                                                  ]
                                                                ),
                                                                _vm._v(" "),
                                                                _c(
                                                                  "el-button",
                                                                  {
                                                                    attrs: {
                                                                      type:
                                                                        "primary",
                                                                      size:
                                                                        "mini"
                                                                    },
                                                                    on: {
                                                                      click: function(
                                                                        $event
                                                                      ) {
                                                                        produit.visible2 = false
                                                                        _vm.removeSectionChild(
                                                                          produit.numeroSection
                                                                        )
                                                                      }
                                                                    }
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      "Confirmer"
                                                                    )
                                                                  ]
                                                                )
                                                              ],
                                                              1
                                                            ),
                                                            _vm._v(" "),
                                                            _c(
                                                              "el-dropdown-item",
                                                              {
                                                                attrs: {
                                                                  slot:
                                                                    "reference"
                                                                },
                                                                slot:
                                                                  "reference"
                                                              },
                                                              [
                                                                _vm._v(
                                                                  "Supprimer les sous élements"
                                                                )
                                                              ]
                                                            )
                                                          ],
                                                          1
                                                        )
                                                      ],
                                                      1
                                                    )
                                                  ],
                                                  1
                                                )
                                              ],
                                              1
                                            )
                                          : _vm._e()
                                      ],
                                      1
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      {
                                        staticStyle: {
                                          position: "absolute",
                                          left: "10px"
                                        }
                                      },
                                      [
                                        _c(
                                          "el-dropdown",
                                          { attrs: { size: "small" } },
                                          [
                                            _c(
                                              "button",
                                              {
                                                staticClass:
                                                  "btn btn-sm btn-info",
                                                staticStyle: { padding: "3px" },
                                                attrs: { type: "button" }
                                              },
                                              [
                                                _c("i", {
                                                  staticClass:
                                                    "fas fa-plus-circle"
                                                })
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "el-dropdown-menu",
                                              {
                                                attrs: { slot: "dropdown" },
                                                slot: "dropdown"
                                              },
                                              [
                                                _c(
                                                  "el-dropdown-item",
                                                  {
                                                    nativeOn: {
                                                      click: function($event) {
                                                        return _vm.addInterligneAvant(
                                                          index
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _vm._v(
                                                      "Ajouter une ligne avant"
                                                    )
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "el-dropdown-item",
                                                  {
                                                    nativeOn: {
                                                      click: function($event) {
                                                        return _vm.addInterligneApres(
                                                          index
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _vm._v(
                                                      "Ajouter une ligne après"
                                                    )
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "el-dropdown-item",
                                                  {
                                                    nativeOn: {
                                                      click: function($event) {
                                                        return _vm.addSectionInterligneAvant(
                                                          index
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _vm._v(
                                                      "Ajouter une section avant"
                                                    )
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "el-dropdown-item",
                                                  {
                                                    nativeOn: {
                                                      click: function($event) {
                                                        return _vm.addSectionInterligneApres(
                                                          index
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _vm._v(
                                                      "Ajouter une section après"
                                                    )
                                                  ]
                                                )
                                              ],
                                              1
                                            )
                                          ],
                                          1
                                        )
                                      ],
                                      1
                                    ),
                                    _vm._v(" "),
                                    !produit.numeroSection
                                      ? _c(
                                          "tr",
                                          [
                                            _vm._l(_vm.getNewLibelle, function(
                                              item,
                                              indice
                                            ) {
                                              return _c(
                                                "td",
                                                { key: indice },
                                                [
                                                  item === "quantite"
                                                    ? _c("el-input-number", {
                                                        staticClass:
                                                          "el-input-number",
                                                        attrs: {
                                                          size: "small",
                                                          min: 1
                                                        },
                                                        on: {
                                                          change: function(
                                                            $event
                                                          ) {
                                                            return _vm.calculTotalRow(
                                                              index
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value: produit[item],
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              produit,
                                                              item,
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "produit[item]"
                                                        }
                                                      })
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  item === "numero"
                                                    ? _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value:
                                                              produit[item],
                                                            expression:
                                                              "produit[item]"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "numero-ligne",
                                                        attrs: {
                                                          type: "text",
                                                          readonly: ""
                                                        },
                                                        domProps: {
                                                          value: produit[item]
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              produit,
                                                              item,
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  _vm.defaultsInputs(item)
                                                    ? _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value:
                                                              produit[item],
                                                            expression:
                                                              "produit[item]"
                                                          }
                                                        ],
                                                        attrs: { type: "text" },
                                                        domProps: {
                                                          value: produit[item]
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              produit,
                                                              item,
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  !_vm.showRemise &&
                                                  item === "remise"
                                                    ? _c("vue-numeric", {
                                                        staticClass:
                                                          "remise-col",
                                                        attrs: {
                                                          currency: "%",
                                                          "output-type":
                                                            "Number",
                                                          "currency-symbol-position":
                                                            "suffix",
                                                          max: 200,
                                                          min: 0,
                                                          minus: false,
                                                          "empty-value": "0",
                                                          precision: 0
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            return _vm.calculMontantRemise(
                                                              index
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value: produit[item],
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              produit,
                                                              item,
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "produit[item]"
                                                        }
                                                      })
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  _vm.showRemise &&
                                                  item === "remise"
                                                    ? _c("vue-numeric", {
                                                        staticClass:
                                                          "remise-col",
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "Number",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          readonly: ""
                                                        },
                                                        model: {
                                                          value:
                                                            produit.montantRemise,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              produit,
                                                              "montantRemise",
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "produit.montantRemise"
                                                        }
                                                      })
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  item === "unite"
                                                    ? _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value:
                                                              produit[item],
                                                            expression:
                                                              "produit[item]"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "unite-col",
                                                        attrs: { type: "text" },
                                                        domProps: {
                                                          value: produit[item]
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              produit,
                                                              item,
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  item === "designation"
                                                    ? _c("textarea", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value: produit.nom,
                                                            expression:
                                                              "produit.nom"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          rows: "1",
                                                          placeholder:
                                                            "Saisir la désignation"
                                                        },
                                                        domProps: {
                                                          value: produit.nom
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              produit,
                                                              "nom",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  item === "designation"
                                                    ? _c(
                                                        "el-select",
                                                        {
                                                          staticClass:
                                                            "select-icon",
                                                          on: {
                                                            change: function(
                                                              $event
                                                            ) {
                                                              return _vm.getProduct(
                                                                produit.nom,
                                                                index
                                                              )
                                                            }
                                                          },
                                                          model: {
                                                            value: produit.nom,
                                                            callback: function(
                                                              $$v
                                                            ) {
                                                              _vm.$set(
                                                                produit,
                                                                "nom",
                                                                $$v
                                                              )
                                                            },
                                                            expression:
                                                              "produit.nom"
                                                          }
                                                        },
                                                        _vm._l(
                                                          _vm.mesProduits,
                                                          function(item) {
                                                            return _c(
                                                              "el-option",
                                                              {
                                                                key: item.id,
                                                                attrs: {
                                                                  label:
                                                                    item.nom,
                                                                  value:
                                                                    item.nom
                                                                }
                                                              }
                                                            )
                                                          }
                                                        ),
                                                        1
                                                      )
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  item === "prix"
                                                    ? _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "Number",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ]
                                                        },
                                                        nativeOn: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            return _vm.calculTotalRow(
                                                              index
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value:
                                                            produit.prixVenteUnitaire,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              produit,
                                                              "prixVenteUnitaire",
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "produit.prixVenteUnitaire"
                                                        }
                                                      })
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  item === "total"
                                                    ? _c("vue-numeric", {
                                                        staticClass:
                                                          "total-col",
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "Number",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          readonly: ""
                                                        },
                                                        model: {
                                                          value: produit[item],
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              produit,
                                                              item,
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "produit[item]"
                                                        }
                                                      })
                                                    : _vm._e()
                                                ],
                                                1
                                              )
                                            }),
                                            _vm._v(" "),
                                            _c(
                                              "transition",
                                              { attrs: { name: "fade" } },
                                              [
                                                _c(
                                                  "el-tooltip",
                                                  {
                                                    staticClass: "float-left",
                                                    attrs: {
                                                      content: "Supprimer",
                                                      placement: "top"
                                                    }
                                                  },
                                                  [
                                                    _c(
                                                      "el-popover",
                                                      {
                                                        staticClass:
                                                          "bnt-minus",
                                                        attrs: {
                                                          transition:
                                                            "el-fade-in-linear",
                                                          placement: "top",
                                                          width: "260"
                                                        },
                                                        model: {
                                                          value:
                                                            produit.visible,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              produit,
                                                              "visible",
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "produit.visible"
                                                        }
                                                      },
                                                      [
                                                        _c("p", [
                                                          _vm._v(
                                                            "Voulez-vous vraiment supprimer?"
                                                          )
                                                        ]),
                                                        _vm._v(" "),
                                                        _c(
                                                          "div",
                                                          {
                                                            staticStyle: {
                                                              "text-align":
                                                                "right",
                                                              margin: "0"
                                                            }
                                                          },
                                                          [
                                                            _c(
                                                              "el-button",
                                                              {
                                                                attrs: {
                                                                  size: "mini",
                                                                  type: "text"
                                                                },
                                                                on: {
                                                                  click: function(
                                                                    $event
                                                                  ) {
                                                                    produit.visible = false
                                                                  }
                                                                }
                                                              },
                                                              [
                                                                _vm._v(
                                                                  "Annuler"
                                                                )
                                                              ]
                                                            ),
                                                            _vm._v(" "),
                                                            _c(
                                                              "el-button",
                                                              {
                                                                attrs: {
                                                                  type:
                                                                    "primary",
                                                                  size: "mini"
                                                                },
                                                                on: {
                                                                  click: function(
                                                                    $event
                                                                  ) {
                                                                    produit.visible = false
                                                                    _vm.removeRows(
                                                                      index
                                                                    )
                                                                  }
                                                                }
                                                              },
                                                              [
                                                                _vm._v(
                                                                  "Confirmer"
                                                                )
                                                              ]
                                                            )
                                                          ],
                                                          1
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "button",
                                                          {
                                                            staticClass:
                                                              "btn btn-sm btn-danger btn-minus",
                                                            attrs: {
                                                              slot: "reference",
                                                              type: "button"
                                                            },
                                                            slot: "reference"
                                                          },
                                                          [
                                                            _c("i", {
                                                              staticClass:
                                                                "fas fa-minus-circle"
                                                            })
                                                          ]
                                                        )
                                                      ]
                                                    )
                                                  ],
                                                  1
                                                )
                                              ],
                                              1
                                            )
                                          ],
                                          2
                                        )
                                      : _vm._e()
                                  ])
                                })
                              ],
                              2
                            ),
                            _vm._v(" "),
                            _c(
                              "el-dropdown",
                              { attrs: { size: "small" } },
                              [
                                _c(
                                  "button",
                                  {
                                    staticClass:
                                      "btn btn-sm btn-info ml-5 el-dropdown-link",
                                    attrs: { type: "button" },
                                    on: { click: _vm.addRows }
                                  },
                                  [
                                    _c("i", {
                                      staticClass: "fas fa-plus-circle"
                                    })
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "el-dropdown-menu",
                                  {
                                    attrs: { slot: "dropdown" },
                                    slot: "dropdown"
                                  },
                                  [
                                    _c(
                                      "el-dropdown-item",
                                      {
                                        nativeOn: {
                                          click: function($event) {
                                            return _vm.addRows($event)
                                          }
                                        }
                                      },
                                      [_vm._v("Ligne")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "el-dropdown-item",
                                      {
                                        nativeOn: {
                                          click: function($event) {
                                            return _vm.addSection($event)
                                          }
                                        }
                                      },
                                      [_vm._v("Section")]
                                    )
                                  ],
                                  1
                                )
                              ],
                              1
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          [
                            _c("TotalTable", {
                              staticClass: "float-right total",
                              attrs: {
                                remiseExist: _vm.remiseExist,
                                modelfactureref: _vm.modelfactureref,
                                projectId: _vm.projectId,
                                lesMontants: _vm.lesMontants,
                                idDevis: _vm.idDevis
                              }
                            }),
                            _vm._v(" "),
                            _c("SecondTable", {
                              attrs: { paiementsInfos: _vm.paiementsInfos },
                              on: { getPart: _vm.getPart }
                            })
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "mt-top facture-footer ml-2 pt-2" },
                        [
                          _c(
                            "el-row",
                            { attrs: { gutter: 12 } },
                            [
                              _c(
                                "el-col",
                                { attrs: { span: 8 } },
                                [
                                  _c(
                                    "el-card",
                                    {
                                      staticClass: "el-card border-none",
                                      attrs: { shadow: "never" }
                                    },
                                    [
                                      _c("el-input", {
                                        attrs: {
                                          type: "textarea",
                                          rows: 4,
                                          placeholder:
                                            "Entrez les mentions légales ici"
                                        },
                                        model: {
                                          value: _vm.devisFooter["mentions"],
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.devisFooter,
                                              "mentions",
                                              $$v
                                            )
                                          },
                                          expression: "devisFooter['mentions']"
                                        }
                                      })
                                    ],
                                    1
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "el-col",
                                { attrs: { span: 8 } },
                                [
                                  _c(
                                    "el-card",
                                    {
                                      staticClass: "el-card border-none",
                                      attrs: { shadow: "never" }
                                    },
                                    [
                                      _vm._v(
                                        "\n                  Date : " +
                                          _vm._s(_vm.getCurrentDate()) +
                                          "\n                  \n                  "
                                      ),
                                      _c("br"),
                                      _vm._v(" "),
                                      _c("br"),
                                      _vm._v("A :\n                  "),
                                      _c("el-input", {
                                        staticClass: "place",
                                        attrs: {
                                          type: "textarea",
                                          rows: 2,
                                          placeholder: "Saisir le lieu"
                                        },
                                        model: {
                                          value: _vm.devisFooter["lieu"],
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.devisFooter,
                                              "lieu",
                                              $$v
                                            )
                                          },
                                          expression: "devisFooter['lieu']"
                                        }
                                      })
                                    ],
                                    1
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "el-col",
                                { attrs: { span: 8 } },
                                [
                                  _c(
                                    "el-card",
                                    {
                                      staticClass:
                                        "mr-2 el-card sign border-none pl-5",
                                      attrs: { shadow: "never" }
                                    },
                                    [
                                      _vm.switchState
                                        ? _c("img", {
                                            staticClass: "pr-5",
                                            attrs: {
                                              src:
                                                "data:image/png;base64," +
                                                _vm.signature,
                                              width: "100%",
                                              alt: ""
                                            }
                                          })
                                        : _vm._e(),
                                      _vm._v(" "),
                                      _c(
                                        "el-tooltip",
                                        {
                                          staticClass: "item",
                                          attrs: {
                                            effect: "dark",
                                            content: _vm.stateMessage,
                                            placement: "top"
                                          }
                                        },
                                        [
                                          _c("el-switch", {
                                            staticClass: "switchBtn",
                                            on: { change: _vm.changeState },
                                            model: {
                                              value: _vm.switchState,
                                              callback: function($$v) {
                                                _vm.switchState = $$v
                                              },
                                              expression: "switchState"
                                            }
                                          })
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("hr", { staticClass: "style-eight" }),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "footer-note pl-3 ml-3",
                          domProps: { innerHTML: _vm._s(_vm.footer) }
                        },
                        [
                          _vm._v(
                            "\n          " + _vm._s(_vm.footer) + "\n          "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "mb-5" }, [
                        _c(
                          "button",
                          {
                            staticClass: "save mt-5 mr-4 float-right",
                            on: {
                              click: function($event) {
                                _vm.saveDevis()
                                _vm.active = 2
                              }
                            }
                          },
                          [_vm._v("Enregistrer et continuer")]
                        )
                      ])
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.active === 2
                  ? _c("div", { staticClass: "card" }, [
                      _c(
                        "div",
                        { staticClass: "card-body" },
                        [
                          _c(
                            "el-row",
                            { attrs: { gutter: 30 } },
                            [
                              _c("HeaderInfos", {
                                attrs: {
                                  objDevis: _vm.objDevis,
                                  prestation: _vm.prestation,
                                  informationsClients: _vm.informationsClients,
                                  imageUrl: _vm.imageUrl,
                                  numeroFacture: _vm.numeroFacture
                                }
                              })
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("div", [
                        _c("div", { staticClass: "element mb-5" }, [
                          _c("table", { staticClass: "base-info" }, [
                            _c(
                              "tr",
                              _vm._l(_vm.selectedElement, function(
                                item,
                                index
                              ) {
                                return _c(
                                  "th",
                                  {
                                    key: index,
                                    staticClass: "font-weight-bold"
                                  },
                                  [_vm._v(_vm._s(item.libelle))]
                                )
                              }),
                              0
                            ),
                            _vm._v(" "),
                            _c(
                              "tr",
                              _vm._l(_vm.selectedElement, function(
                                item,
                                index
                              ) {
                                return _c(
                                  "td",
                                  { key: index, class: "styling" + item.id },
                                  [
                                    item.type === "date"
                                      ? _c("el-date-picker", {
                                          attrs: {
                                            format: "dd MMM yyyy",
                                            "value-format": "dd MMM yyyy",
                                            type: "date",
                                            placeholder: "Choisir une date"
                                          },
                                          model: {
                                            value: item["content"],
                                            callback: function($$v) {
                                              _vm.$set(item, "content", $$v)
                                            },
                                            expression: "item['content']"
                                          }
                                        })
                                      : _vm._e(),
                                    _vm._v(" "),
                                    item.type === "code"
                                      ? _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: item.content,
                                              expression: "item.content"
                                            }
                                          ],
                                          staticClass: "code-width",
                                          attrs: { type: "text", readonly: "" },
                                          domProps: { value: item.content },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                item,
                                                "content",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      : _vm._e(),
                                    _vm._v(" "),
                                    item.type === "lieu"
                                      ? _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: item.content,
                                              expression: "item.content"
                                            }
                                          ],
                                          attrs: { type: "text" },
                                          domProps: { value: item.content },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                item,
                                                "content",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      : _vm._e(),
                                    _vm._v(" "),
                                    item.type === "prestation"
                                      ? _c(
                                          "el-select",
                                          {
                                            staticClass: "selection",
                                            attrs: {
                                              placeholder:
                                                "Choisir une prestation"
                                            },
                                            model: {
                                              value: item.content,
                                              callback: function($$v) {
                                                _vm.$set(item, "content", $$v)
                                              },
                                              expression: "item.content"
                                            }
                                          },
                                          _vm._l(_vm.prestations, function(
                                            item
                                          ) {
                                            return _c("el-option", {
                                              key: item.value1,
                                              attrs: {
                                                label: item.label,
                                                value: item.value1,
                                                disabled: item.disabled
                                              }
                                            })
                                          }),
                                          1
                                        )
                                      : _vm._e(),
                                    _vm._v(" "),
                                    item.type === "vendeur"
                                      ? _c(
                                          "el-select",
                                          {
                                            staticClass: "selection",
                                            attrs: {
                                              placeholder: "Choisir un vendeur"
                                            },
                                            model: {
                                              value: item.content,
                                              callback: function($$v) {
                                                _vm.$set(item, "content", $$v)
                                              },
                                              expression: "item.content"
                                            }
                                          },
                                          _vm._l(_vm.vendeurs, function(item) {
                                            return _c("el-option", {
                                              key: item.value2,
                                              attrs: {
                                                label: item.label,
                                                value: item.value2,
                                                disabled: item.disabled
                                              }
                                            })
                                          }),
                                          1
                                        )
                                      : _vm._e(),
                                    _vm._v(" "),
                                    item.type === "payement"
                                      ? _c(
                                          "el-select",
                                          {
                                            staticClass: "selection",
                                            attrs: {
                                              placeholder: "Choisir le payement"
                                            },
                                            model: {
                                              value: item.content,
                                              callback: function($$v) {
                                                _vm.$set(item, "content", $$v)
                                              },
                                              expression: "item.content"
                                            }
                                          },
                                          _vm._l(_vm.payements, function(item) {
                                            return _c("el-option", {
                                              key: item.value3,
                                              attrs: {
                                                label: item.label,
                                                value: item.value3,
                                                disabled: item.disabled
                                              }
                                            })
                                          }),
                                          1
                                        )
                                      : _vm._e()
                                  ],
                                  1
                                )
                              }),
                              0
                            )
                          ])
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "mt-4 element" },
                          [
                            _c(
                              "table",
                              { staticClass: "base-info" },
                              [
                                _c(
                                  "tr",
                                  _vm._l(_vm.selectedElement2, function(
                                    item,
                                    index
                                  ) {
                                    return _c(
                                      "th",
                                      {
                                        key: index,
                                        staticClass: "font-weight-bold"
                                      },
                                      [
                                        _vm._v(
                                          _vm._s(item.libelle) +
                                            "\n                    "
                                        ),
                                        item.libelle === "Remise%"
                                          ? _c("el-button", {
                                              staticClass: "showRemiseButton",
                                              attrs: {
                                                type: "info",
                                                icon: "el-icon-caret-bottom",
                                                plain: ""
                                              },
                                              on: {
                                                click: function($event) {
                                                  return _vm.afficheMontantRemise()
                                                }
                                              }
                                            })
                                          : _vm._e()
                                      ],
                                      1
                                    )
                                  }),
                                  0
                                ),
                                _vm._v(" "),
                                _vm._l(_vm.datas, function(produit, index) {
                                  return _c("tbody", { key: index }, [
                                    _c(
                                      "tr",
                                      [
                                        produit.numeroSection
                                          ? _c(
                                              "td",
                                              {
                                                attrs: {
                                                  colspan:
                                                    _vm.selectedElement2.length
                                                }
                                              },
                                              [
                                                _c(
                                                  "el-input",
                                                  {
                                                    attrs: {
                                                      placeholder:
                                                        "Saisir le titre de la section",
                                                      id: "section-color",
                                                      size: "small",
                                                      type: "text"
                                                    },
                                                    model: {
                                                      value:
                                                        produit.titleSection,
                                                      callback: function($$v) {
                                                        _vm.$set(
                                                          produit,
                                                          "titleSection",
                                                          $$v
                                                        )
                                                      },
                                                      expression:
                                                        "produit.titleSection"
                                                    }
                                                  },
                                                  [
                                                    _c(
                                                      "template",
                                                      { slot: "prepend" },
                                                      [
                                                        _vm._v(
                                                          _vm._s(
                                                            produit.numeroSection
                                                          )
                                                        )
                                                      ]
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "template",
                                                      { slot: "append" },
                                                      [
                                                        produit.subTotalIsVisible
                                                          ? _c("vue-numeric", {
                                                              staticClass:
                                                                "text-right font-weight-bold section-style",
                                                              attrs: {
                                                                currency:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "currency"
                                                                  ],
                                                                "currency-symbol-position":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "currency-symbol-position"
                                                                  ],
                                                                "output-type":
                                                                  "Number",
                                                                "empty-value":
                                                                  "0",
                                                                precision:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "precision"
                                                                  ],
                                                                "decimal-separator":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "decimal-separator"
                                                                  ],
                                                                "thousand-separator":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "thousand-separator"
                                                                  ],
                                                                readonly: ""
                                                              },
                                                              model: {
                                                                value:
                                                                  produit.subTotal,
                                                                callback: function(
                                                                  $$v
                                                                ) {
                                                                  _vm.$set(
                                                                    produit,
                                                                    "subTotal",
                                                                    $$v
                                                                  )
                                                                },
                                                                expression:
                                                                  "produit.subTotal"
                                                              }
                                                            })
                                                          : _vm._e(),
                                                        _vm._v(" "),
                                                        _c(
                                                          "el-button",
                                                          {
                                                            attrs: {
                                                              type: "info",
                                                              round: ""
                                                            },
                                                            nativeOn: {
                                                              click: function(
                                                                $event
                                                              ) {
                                                                return _vm.displaySubtotal(
                                                                  index
                                                                )
                                                              }
                                                            }
                                                          },
                                                          [
                                                            _c("i", {
                                                              staticClass:
                                                                "fa fa-eye"
                                                            })
                                                          ]
                                                        )
                                                      ],
                                                      1
                                                    )
                                                  ],
                                                  2
                                                )
                                              ],
                                              1
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        produit.numeroSection
                                          ? _c(
                                              "el-dropdown",
                                              {
                                                staticClass: "delete-section",
                                                attrs: { size: "small" }
                                              },
                                              [
                                                _c(
                                                  "el-tooltip",
                                                  {
                                                    attrs: {
                                                      content: "Supprimer",
                                                      placement: "top"
                                                    }
                                                  },
                                                  [
                                                    _c(
                                                      "button",
                                                      {
                                                        staticClass:
                                                          "btn btn-sm btn-info btn-minus",
                                                        attrs: {
                                                          type: "button"
                                                        }
                                                      },
                                                      [
                                                        _c("i", {
                                                          staticClass:
                                                            "fas fa-minus-circle"
                                                        })
                                                      ]
                                                    )
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "el-dropdown-menu",
                                                  {
                                                    attrs: { slot: "dropdown" },
                                                    slot: "dropdown"
                                                  },
                                                  [
                                                    _c(
                                                      "transition",
                                                      {
                                                        attrs: { name: "fade" }
                                                      },
                                                      [
                                                        _c(
                                                          "el-popover",
                                                          {
                                                            staticClass:
                                                              "bnt-minus",
                                                            attrs: {
                                                              transition:
                                                                "el-fade-in-linear",
                                                              placement: "top",
                                                              width: "260"
                                                            },
                                                            model: {
                                                              value:
                                                                produit.visible,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  produit,
                                                                  "visible",
                                                                  $$v
                                                                )
                                                              },
                                                              expression:
                                                                "produit.visible"
                                                            }
                                                          },
                                                          [
                                                            _c("p", [
                                                              _vm._v(
                                                                "Voulez-vous vraiment supprimer?"
                                                              )
                                                            ]),
                                                            _vm._v(" "),
                                                            _c(
                                                              "div",
                                                              {
                                                                staticStyle: {
                                                                  "text-align":
                                                                    "right",
                                                                  margin: "0"
                                                                }
                                                              },
                                                              [
                                                                _c(
                                                                  "el-button",
                                                                  {
                                                                    attrs: {
                                                                      size:
                                                                        "mini",
                                                                      type:
                                                                        "text"
                                                                    },
                                                                    on: {
                                                                      click: function(
                                                                        $event
                                                                      ) {
                                                                        produit.visible = false
                                                                      }
                                                                    }
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      "Annuler"
                                                                    )
                                                                  ]
                                                                ),
                                                                _vm._v(" "),
                                                                _c(
                                                                  "el-button",
                                                                  {
                                                                    attrs: {
                                                                      type:
                                                                        "primary",
                                                                      size:
                                                                        "mini"
                                                                    },
                                                                    on: {
                                                                      click: function(
                                                                        $event
                                                                      ) {
                                                                        produit.visible = false
                                                                        _vm.removeSection(
                                                                          index
                                                                        )
                                                                      }
                                                                    }
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      "Confirmer"
                                                                    )
                                                                  ]
                                                                )
                                                              ],
                                                              1
                                                            ),
                                                            _vm._v(" "),
                                                            _c(
                                                              "el-dropdown-item",
                                                              {
                                                                attrs: {
                                                                  slot:
                                                                    "reference"
                                                                },
                                                                slot:
                                                                  "reference"
                                                              },
                                                              [
                                                                _vm._v(
                                                                  "Supprimer la section"
                                                                )
                                                              ]
                                                            )
                                                          ],
                                                          1
                                                        )
                                                      ],
                                                      1
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "transition",
                                                      {
                                                        attrs: { name: "fade" }
                                                      },
                                                      [
                                                        _c(
                                                          "el-popover",
                                                          {
                                                            staticClass:
                                                              "bnt-minus",
                                                            attrs: {
                                                              transition:
                                                                "el-fade-in-linear",
                                                              placement: "top",
                                                              width: "260"
                                                            },
                                                            model: {
                                                              value:
                                                                produit.visible2,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  produit,
                                                                  "visible2",
                                                                  $$v
                                                                )
                                                              },
                                                              expression:
                                                                "produit.visible2"
                                                            }
                                                          },
                                                          [
                                                            _c("p", [
                                                              _vm._v(
                                                                "Voulez-vous vraiment supprimer?"
                                                              )
                                                            ]),
                                                            _vm._v(" "),
                                                            _c(
                                                              "div",
                                                              {
                                                                staticStyle: {
                                                                  "text-align":
                                                                    "right",
                                                                  margin: "0"
                                                                }
                                                              },
                                                              [
                                                                _c(
                                                                  "el-button",
                                                                  {
                                                                    attrs: {
                                                                      size:
                                                                        "mini",
                                                                      type:
                                                                        "text"
                                                                    },
                                                                    on: {
                                                                      click: function(
                                                                        $event
                                                                      ) {
                                                                        produit.visible2 = false
                                                                      }
                                                                    }
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      "Annuler"
                                                                    )
                                                                  ]
                                                                ),
                                                                _vm._v(" "),
                                                                _c(
                                                                  "el-button",
                                                                  {
                                                                    attrs: {
                                                                      type:
                                                                        "primary",
                                                                      size:
                                                                        "mini"
                                                                    },
                                                                    on: {
                                                                      click: function(
                                                                        $event
                                                                      ) {
                                                                        produit.visible2 = false
                                                                        _vm.removeSectionChild(
                                                                          produit.numeroSection
                                                                        )
                                                                      }
                                                                    }
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      "Confirmer"
                                                                    )
                                                                  ]
                                                                )
                                                              ],
                                                              1
                                                            ),
                                                            _vm._v(" "),
                                                            _c(
                                                              "el-dropdown-item",
                                                              {
                                                                attrs: {
                                                                  slot:
                                                                    "reference"
                                                                },
                                                                slot:
                                                                  "reference"
                                                              },
                                                              [
                                                                _vm._v(
                                                                  "Supprimer les sous élements"
                                                                )
                                                              ]
                                                            )
                                                          ],
                                                          1
                                                        )
                                                      ],
                                                      1
                                                    )
                                                  ],
                                                  1
                                                )
                                              ],
                                              1
                                            )
                                          : _vm._e()
                                      ],
                                      1
                                    ),
                                    _vm._v(" "),
                                    !produit.numeroSection
                                      ? _c(
                                          "tr",
                                          [
                                            _vm._l(_vm.getNewLibelle, function(
                                              item,
                                              indice
                                            ) {
                                              return _c(
                                                "td",
                                                { key: indice },
                                                [
                                                  item === "quantite"
                                                    ? _c("el-input-number", {
                                                        staticClass:
                                                          "el-input-number",
                                                        attrs: {
                                                          size: "small",
                                                          min: 1
                                                        },
                                                        on: {
                                                          change: function(
                                                            $event
                                                          ) {
                                                            return _vm.calculTotalRow(
                                                              index
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value: produit[item],
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              produit,
                                                              item,
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "produit[item]"
                                                        }
                                                      })
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  item === "numero"
                                                    ? _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value:
                                                              produit[item],
                                                            expression:
                                                              "produit[item]"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "numero-ligne",
                                                        attrs: { type: "text" },
                                                        domProps: {
                                                          value: produit[item]
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              produit,
                                                              item,
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  _vm.defaultsInputs(item)
                                                    ? _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value:
                                                              produit[item],
                                                            expression:
                                                              "produit[item]"
                                                          }
                                                        ],
                                                        attrs: { type: "text" },
                                                        domProps: {
                                                          value: produit[item]
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              produit,
                                                              item,
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  !_vm.showRemise &&
                                                  item === "remise"
                                                    ? _c("vue-numeric", {
                                                        staticClass:
                                                          "remise-col",
                                                        attrs: {
                                                          currency: "%",
                                                          "output-type":
                                                            "Number",
                                                          "currency-symbol-position":
                                                            "suffix",
                                                          max: 200,
                                                          min: 0,
                                                          minus: false,
                                                          "empty-value": "0",
                                                          precision: 0
                                                        },
                                                        nativeOn: {
                                                          change: function(
                                                            $event
                                                          ) {
                                                            return _vm.calculMontantRemise(
                                                              index
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value: produit[item],
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              produit,
                                                              item,
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "produit[item]"
                                                        }
                                                      })
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  _vm.showRemise &&
                                                  item === "remise"
                                                    ? _c("vue-numeric", {
                                                        staticClass:
                                                          "remise-col",
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "Number",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          readonly: ""
                                                        },
                                                        model: {
                                                          value:
                                                            produit.montantRemise,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              produit,
                                                              "montantRemise",
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "produit.montantRemise"
                                                        }
                                                      })
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  item === "unite"
                                                    ? _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value:
                                                              produit[item],
                                                            expression:
                                                              "produit[item]"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "unite-col",
                                                        attrs: { type: "text" },
                                                        domProps: {
                                                          value: produit[item]
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              produit,
                                                              item,
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  item === "designation"
                                                    ? _c("textarea", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value: produit.nom,
                                                            expression:
                                                              "produit.nom"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          rows: "1",
                                                          placeholder:
                                                            "Saisir la désignation"
                                                        },
                                                        domProps: {
                                                          value: produit.nom
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              produit,
                                                              "nom",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  item === "designation"
                                                    ? _c(
                                                        "el-select",
                                                        {
                                                          staticClass:
                                                            "select-icon",
                                                          on: {
                                                            change: function(
                                                              $event
                                                            ) {
                                                              return _vm.getProduct(
                                                                produit.nom,
                                                                index
                                                              )
                                                            }
                                                          },
                                                          model: {
                                                            value: produit.nom,
                                                            callback: function(
                                                              $$v
                                                            ) {
                                                              _vm.$set(
                                                                produit,
                                                                "nom",
                                                                $$v
                                                              )
                                                            },
                                                            expression:
                                                              "produit.nom"
                                                          }
                                                        },
                                                        _vm._l(
                                                          _vm.mesProduits,
                                                          function(item) {
                                                            return _c(
                                                              "el-option",
                                                              {
                                                                key: item.id,
                                                                attrs: {
                                                                  label:
                                                                    item.nom,
                                                                  value:
                                                                    item.nom
                                                                }
                                                              }
                                                            )
                                                          }
                                                        ),
                                                        1
                                                      )
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  item === "prix"
                                                    ? _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "Number",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ]
                                                        },
                                                        nativeOn: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            return _vm.calculTotalRow(
                                                              index
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value:
                                                            produit.prixVenteUnitaire,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              produit,
                                                              "prixVenteUnitaire",
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "produit.prixVenteUnitaire"
                                                        }
                                                      })
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  item === "total"
                                                    ? _c("vue-numeric", {
                                                        staticClass:
                                                          "total-col",
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "Number",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          readonly: ""
                                                        },
                                                        model: {
                                                          value: produit[item],
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              produit,
                                                              item,
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "produit[item]"
                                                        }
                                                      })
                                                    : _vm._e()
                                                ],
                                                1
                                              )
                                            }),
                                            _vm._v(" "),
                                            _c(
                                              "transition",
                                              { attrs: { name: "fade" } },
                                              [
                                                _c(
                                                  "el-tooltip",
                                                  {
                                                    staticClass: "float-left",
                                                    attrs: {
                                                      content: "Supprimer",
                                                      placement: "top"
                                                    }
                                                  },
                                                  [
                                                    _c(
                                                      "el-popover",
                                                      {
                                                        staticClass:
                                                          "bnt-minus",
                                                        attrs: {
                                                          transition:
                                                            "el-fade-in-linear",
                                                          placement: "top",
                                                          width: "260"
                                                        },
                                                        model: {
                                                          value:
                                                            produit.visible,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              produit,
                                                              "visible",
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "produit.visible"
                                                        }
                                                      },
                                                      [
                                                        _c("p", [
                                                          _vm._v(
                                                            "Voulez-vous vraiment supprimer?"
                                                          )
                                                        ]),
                                                        _vm._v(" "),
                                                        _c(
                                                          "div",
                                                          {
                                                            staticStyle: {
                                                              "text-align":
                                                                "right",
                                                              margin: "0"
                                                            }
                                                          },
                                                          [
                                                            _c(
                                                              "el-button",
                                                              {
                                                                attrs: {
                                                                  size: "mini",
                                                                  type: "text"
                                                                },
                                                                on: {
                                                                  click: function(
                                                                    $event
                                                                  ) {
                                                                    produit.visible = false
                                                                  }
                                                                }
                                                              },
                                                              [
                                                                _vm._v(
                                                                  "Annuler"
                                                                )
                                                              ]
                                                            ),
                                                            _vm._v(" "),
                                                            _c(
                                                              "el-button",
                                                              {
                                                                attrs: {
                                                                  type:
                                                                    "primary",
                                                                  size: "mini"
                                                                },
                                                                on: {
                                                                  click: function(
                                                                    $event
                                                                  ) {
                                                                    produit.visible = false
                                                                    _vm.removeRows(
                                                                      index
                                                                    )
                                                                  }
                                                                }
                                                              },
                                                              [
                                                                _vm._v(
                                                                  "Confirmer"
                                                                )
                                                              ]
                                                            )
                                                          ],
                                                          1
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "button",
                                                          {
                                                            staticClass:
                                                              "btn btn-sm btn-danger btn-minus",
                                                            attrs: {
                                                              slot: "reference",
                                                              type: "button"
                                                            },
                                                            slot: "reference"
                                                          },
                                                          [
                                                            _c("i", {
                                                              staticClass:
                                                                "fas fa-minus-circle"
                                                            })
                                                          ]
                                                        )
                                                      ]
                                                    )
                                                  ],
                                                  1
                                                )
                                              ],
                                              1
                                            )
                                          ],
                                          2
                                        )
                                      : _vm._e()
                                  ])
                                })
                              ],
                              2
                            ),
                            _vm._v(" "),
                            _c(
                              "el-dropdown",
                              { attrs: { size: "small" } },
                              [
                                _c(
                                  "button",
                                  {
                                    staticClass:
                                      "btn btn-sm btn-info ml-5 el-dropdown-link",
                                    attrs: { type: "button" },
                                    on: { click: _vm.addRows }
                                  },
                                  [
                                    _c("i", {
                                      staticClass: "fas fa-plus-circle"
                                    })
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "el-dropdown-menu",
                                  {
                                    attrs: { slot: "dropdown" },
                                    slot: "dropdown"
                                  },
                                  [
                                    _c(
                                      "el-dropdown-item",
                                      {
                                        nativeOn: {
                                          click: function($event) {
                                            return _vm.addRows($event)
                                          }
                                        }
                                      },
                                      [_vm._v("Ligne")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "el-dropdown-item",
                                      {
                                        nativeOn: {
                                          click: function($event) {
                                            return _vm.addSection($event)
                                          }
                                        }
                                      },
                                      [_vm._v("Section")]
                                    )
                                  ],
                                  1
                                )
                              ],
                              1
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          [
                            _c("TotalTable", {
                              staticClass: "float-right total",
                              attrs: {
                                remiseExist: _vm.remiseExist,
                                modelfactureref: _vm.modelfactureref,
                                projectId: _vm.projectId,
                                lesMontants: _vm.lesMontants,
                                idDevis: _vm.idDevis
                              }
                            }),
                            _vm._v(" "),
                            _c("SecondTable", {
                              attrs: { paiementsInfos: _vm.paiementsInfos },
                              on: { getPart: _vm.getPart }
                            })
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "mt-top facture-footer ml-2 pt-2" },
                        [
                          _c(
                            "el-row",
                            { attrs: { gutter: 12 } },
                            [
                              _c(
                                "el-col",
                                { attrs: { span: 8 } },
                                [
                                  _c(
                                    "el-card",
                                    {
                                      staticClass: "el-card border-none",
                                      attrs: { shadow: "never" }
                                    },
                                    [
                                      _c("el-input", {
                                        attrs: {
                                          type: "textarea",
                                          rows: 4,
                                          placeholder:
                                            "Entrez les mentions légales ici"
                                        },
                                        model: {
                                          value: _vm.devisFooter["mentions"],
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.devisFooter,
                                              "mentions",
                                              $$v
                                            )
                                          },
                                          expression: "devisFooter['mentions']"
                                        }
                                      })
                                    ],
                                    1
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "el-col",
                                { attrs: { span: 8 } },
                                [
                                  _c(
                                    "el-card",
                                    {
                                      staticClass: "el-card border-none",
                                      attrs: { shadow: "never" }
                                    },
                                    [
                                      _vm._v(
                                        "\n                    Date : " +
                                          _vm._s(_vm.getCurrentDate()) +
                                          "\n                    \n                    "
                                      ),
                                      _c("br"),
                                      _vm._v(" "),
                                      _c("br"),
                                      _vm._v("A :\n                    "),
                                      _c("el-input", {
                                        staticClass: "place",
                                        attrs: {
                                          type: "textarea",
                                          rows: 2,
                                          placeholder: "Saisir le lieu"
                                        },
                                        model: {
                                          value: _vm.devisFooter["lieu"],
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.devisFooter,
                                              "lieu",
                                              $$v
                                            )
                                          },
                                          expression: "devisFooter['lieu']"
                                        }
                                      })
                                    ],
                                    1
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "el-col",
                                { attrs: { span: 8 } },
                                [
                                  _c(
                                    "el-card",
                                    {
                                      staticClass:
                                        "mr-2 el-card sign border-none pl-5",
                                      attrs: { shadow: "never" }
                                    },
                                    [
                                      _vm.switchState
                                        ? _c("img", {
                                            staticClass: "pr-5",
                                            attrs: {
                                              src:
                                                "data:image/png;base64," +
                                                _vm.signature,
                                              width: "100%"
                                            }
                                          })
                                        : _vm._e(),
                                      _vm._v(" "),
                                      _c("el-switch", {
                                        staticClass: "switchBtn",
                                        model: {
                                          value: _vm.switchState,
                                          callback: function($$v) {
                                            _vm.switchState = $$v
                                          },
                                          expression: "switchState"
                                        }
                                      })
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("hr", { staticClass: "style-eight" }),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "footer-note pl-3 ml-3",
                          domProps: { innerHTML: _vm._s(_vm.footer) }
                        },
                        [
                          _vm._v(
                            "\n            " +
                              _vm._s(_vm.footer) +
                              "\n          "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "mb-5 mt-4" }, [
                        _c(
                          "button",
                          {
                            staticClass: "save ml-4",
                            on: {
                              click: function($event) {
                                return _vm.goback()
                              }
                            }
                          },
                          [_vm._v("Retour")]
                        ),
                        _vm._v(" "),
                        _c(
                          "button",
                          {
                            staticClass: "save mr-5 pr-2 float-right",
                            attrs: {
                              "data-toggle": "modal",
                              "data-target": "#responsive-modal"
                            },
                            on: {
                              click: function($event) {
                                return _vm.saveDevis()
                              }
                            }
                          },
                          [_vm._v("\n                Terminé\n            ")]
                        ),
                        _vm._v(" "),
                        _c(
                          "button",
                          {
                            staticClass: "save float-right mr-2",
                            on: {
                              click: function($event) {
                                _vm.active = 3
                              }
                            }
                          },
                          [_vm._v("Suivant")]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass: "modal fade",
                            staticStyle: { display: "none" },
                            attrs: {
                              id: "responsive-modal",
                              tabindex: "-1",
                              role: "dialog",
                              "aria-labelledby": "myModalLabel",
                              "aria-hidden": "true"
                            }
                          },
                          [
                            _c("div", { staticClass: "modal-dialog" }, [
                              _c("div", { staticClass: "modal-content" }, [
                                _vm._m(0),
                                _vm._v(" "),
                                _c("div", { staticClass: "modal-footer" }, [
                                  _c(
                                    "button",
                                    {
                                      staticClass:
                                        "btn btn-primary waves-effect waves-light mr-5",
                                      attrs: { type: "button" },
                                      on: {
                                        click: function($event) {
                                          return _vm.printPdf("Devis")
                                        }
                                      }
                                    },
                                    [
                                      _c("i", {
                                        staticClass: "fa fa-download pr-2"
                                      }),
                                      _vm._v("Devis PDF")
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "button",
                                    {
                                      staticClass:
                                        "btn btn-primary waves-effect waves-light",
                                      attrs: { type: "button" },
                                      on: {
                                        click: function($event) {
                                          return _vm.printPdf("Facture")
                                        }
                                      }
                                    },
                                    [
                                      _c("i", {
                                        staticClass: "fa fa-download pr-2"
                                      }),
                                      _vm._v("Facture PDF")
                                    ]
                                  )
                                ])
                              ])
                            ])
                          ]
                        )
                      ])
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.active === 3
                  ? _c("div", { staticClass: "card" }, [
                      _c(
                        "div",
                        { staticClass: "card-body" },
                        [
                          _c(
                            "el-row",
                            { attrs: { gutter: 30 } },
                            [
                              _c("HeaderInfos", {
                                attrs: {
                                  objDevis: _vm.objDevis,
                                  prestation: _vm.prestation,
                                  informationsClients: _vm.informationsClients,
                                  imageUrl: _vm.imageUrl,
                                  numeroFacture: _vm.numeroFacture
                                }
                              })
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("div", [
                        _c("div", { staticClass: "element mb-5" }, [
                          _c("table", { staticClass: "base-info" }, [
                            _c(
                              "tr",
                              _vm._l(_vm.selectedElement, function(
                                item,
                                index
                              ) {
                                return _c(
                                  "th",
                                  {
                                    key: index,
                                    staticClass: "font-weight-bold"
                                  },
                                  [_vm._v(_vm._s(item.libelle))]
                                )
                              }),
                              0
                            ),
                            _vm._v(" "),
                            _c(
                              "tr",
                              _vm._l(_vm.selectedElement, function(
                                item,
                                index
                              ) {
                                return _c(
                                  "td",
                                  { key: index, class: "styling" + item.id },
                                  [
                                    item.type === "date"
                                      ? _c("el-date-picker", {
                                          attrs: {
                                            format: "dd MMM yyyy",
                                            "value-format": "dd MMM yyyy",
                                            type: "date",
                                            placeholder: "Choisir une date"
                                          },
                                          model: {
                                            value: item["content"],
                                            callback: function($$v) {
                                              _vm.$set(item, "content", $$v)
                                            },
                                            expression: "item['content']"
                                          }
                                        })
                                      : _vm._e(),
                                    _vm._v(" "),
                                    item.type === "code"
                                      ? _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: item.content,
                                              expression: "item.content"
                                            }
                                          ],
                                          staticClass: "code-width",
                                          attrs: { type: "text", readonly: "" },
                                          domProps: { value: item.content },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                item,
                                                "content",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      : _vm._e(),
                                    _vm._v(" "),
                                    item.type === "lieu"
                                      ? _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: item.content,
                                              expression: "item.content"
                                            }
                                          ],
                                          attrs: { type: "text" },
                                          domProps: { value: item.content },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                item,
                                                "content",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      : _vm._e(),
                                    _vm._v(" "),
                                    item.type === "prestation"
                                      ? _c(
                                          "el-select",
                                          {
                                            staticClass: "selection",
                                            attrs: {
                                              placeholder:
                                                "Choisir une prestation"
                                            },
                                            model: {
                                              value: item.content,
                                              callback: function($$v) {
                                                _vm.$set(item, "content", $$v)
                                              },
                                              expression: "item.content"
                                            }
                                          },
                                          _vm._l(_vm.prestations, function(
                                            item
                                          ) {
                                            return _c("el-option", {
                                              key: item.value1,
                                              attrs: {
                                                label: item.label,
                                                value: item.value1,
                                                disabled: item.disabled
                                              }
                                            })
                                          }),
                                          1
                                        )
                                      : _vm._e(),
                                    _vm._v(" "),
                                    item.type === "vendeur"
                                      ? _c(
                                          "el-select",
                                          {
                                            staticClass: "selection",
                                            attrs: {
                                              placeholder: "Choisir un vendeur"
                                            },
                                            model: {
                                              value: item.content,
                                              callback: function($$v) {
                                                _vm.$set(item, "content", $$v)
                                              },
                                              expression: "item.content"
                                            }
                                          },
                                          _vm._l(_vm.vendeurs, function(item) {
                                            return _c("el-option", {
                                              key: item.value2,
                                              attrs: {
                                                label: item.label,
                                                value: item.value2,
                                                disabled: item.disabled
                                              }
                                            })
                                          }),
                                          1
                                        )
                                      : _vm._e(),
                                    _vm._v(" "),
                                    item.type === "payement"
                                      ? _c(
                                          "el-select",
                                          {
                                            staticClass: "selection",
                                            attrs: {
                                              placeholder: "Choisir le payement"
                                            },
                                            model: {
                                              value: item.content,
                                              callback: function($$v) {
                                                _vm.$set(item, "content", $$v)
                                              },
                                              expression: "item.content"
                                            }
                                          },
                                          _vm._l(_vm.payements, function(item) {
                                            return _c("el-option", {
                                              key: item.value3,
                                              attrs: {
                                                label: item.label,
                                                value: item.value3,
                                                disabled: item.disabled
                                              }
                                            })
                                          }),
                                          1
                                        )
                                      : _vm._e()
                                  ],
                                  1
                                )
                              }),
                              0
                            )
                          ])
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "mt-4 element" },
                          [
                            _c(
                              "table",
                              { staticClass: "base-info" },
                              [
                                _c(
                                  "tr",
                                  _vm._l(_vm.selectedElement2, function(
                                    item,
                                    index
                                  ) {
                                    return _c(
                                      "th",
                                      {
                                        key: index,
                                        staticClass: "font-weight-bold"
                                      },
                                      [
                                        _vm._v(
                                          _vm._s(item.libelle) +
                                            "\n                    "
                                        ),
                                        item.libelle === "Remise%"
                                          ? _c("el-button", {
                                              staticClass: "showRemiseButton",
                                              attrs: {
                                                type: "info",
                                                icon: "el-icon-caret-bottom",
                                                plain: ""
                                              },
                                              on: {
                                                click: function($event) {
                                                  return _vm.afficheMontantRemise()
                                                }
                                              }
                                            })
                                          : _vm._e()
                                      ],
                                      1
                                    )
                                  }),
                                  0
                                ),
                                _vm._v(" "),
                                _vm._l(_vm.datas, function(produit, index) {
                                  return _c("tbody", { key: index }, [
                                    _c(
                                      "tr",
                                      [
                                        produit.numeroSection
                                          ? _c(
                                              "td",
                                              {
                                                attrs: {
                                                  colspan:
                                                    _vm.selectedElement2.length
                                                }
                                              },
                                              [
                                                _c(
                                                  "el-input",
                                                  {
                                                    attrs: {
                                                      placeholder:
                                                        "Saisir le titre de la section",
                                                      id: "section-color",
                                                      size: "small",
                                                      type: "text"
                                                    },
                                                    model: {
                                                      value:
                                                        produit.titleSection,
                                                      callback: function($$v) {
                                                        _vm.$set(
                                                          produit,
                                                          "titleSection",
                                                          $$v
                                                        )
                                                      },
                                                      expression:
                                                        "produit.titleSection"
                                                    }
                                                  },
                                                  [
                                                    _c(
                                                      "template",
                                                      { slot: "prepend" },
                                                      [
                                                        _vm._v(
                                                          _vm._s(
                                                            produit.numeroSection
                                                          )
                                                        )
                                                      ]
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "template",
                                                      { slot: "append" },
                                                      [
                                                        produit.subTotalIsVisible
                                                          ? _c("vue-numeric", {
                                                              staticClass:
                                                                "text-right font-weight-bold section-style",
                                                              attrs: {
                                                                currency:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "currency"
                                                                  ],
                                                                "currency-symbol-position":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "currency-symbol-position"
                                                                  ],
                                                                "output-type":
                                                                  "Number",
                                                                "empty-value":
                                                                  "0",
                                                                precision:
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "precision"
                                                                  ],
                                                                "decimal-separator":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "decimal-separator"
                                                                  ],
                                                                "thousand-separator":
                                                                  _vm
                                                                    .currentCurrencyMask[
                                                                    "thousand-separator"
                                                                  ],
                                                                readonly: ""
                                                              },
                                                              model: {
                                                                value:
                                                                  produit.subTotal,
                                                                callback: function(
                                                                  $$v
                                                                ) {
                                                                  _vm.$set(
                                                                    produit,
                                                                    "subTotal",
                                                                    $$v
                                                                  )
                                                                },
                                                                expression:
                                                                  "produit.subTotal"
                                                              }
                                                            })
                                                          : _vm._e(),
                                                        _vm._v(" "),
                                                        _c(
                                                          "el-button",
                                                          {
                                                            attrs: {
                                                              type: "info",
                                                              round: ""
                                                            },
                                                            nativeOn: {
                                                              click: function(
                                                                $event
                                                              ) {
                                                                return _vm.displaySubtotal(
                                                                  index
                                                                )
                                                              }
                                                            }
                                                          },
                                                          [
                                                            _c("i", {
                                                              staticClass:
                                                                "fa fa-eye"
                                                            })
                                                          ]
                                                        )
                                                      ],
                                                      1
                                                    )
                                                  ],
                                                  2
                                                )
                                              ],
                                              1
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        produit.numeroSection
                                          ? _c(
                                              "el-dropdown",
                                              {
                                                staticClass: "delete-section",
                                                attrs: { size: "small" }
                                              },
                                              [
                                                _c(
                                                  "el-tooltip",
                                                  {
                                                    attrs: {
                                                      content: "Supprimer",
                                                      placement: "top"
                                                    }
                                                  },
                                                  [
                                                    _c(
                                                      "button",
                                                      {
                                                        staticClass:
                                                          "btn btn-sm btn-info btn-minus",
                                                        attrs: {
                                                          type: "button"
                                                        }
                                                      },
                                                      [
                                                        _c("i", {
                                                          staticClass:
                                                            "fas fa-minus-circle"
                                                        })
                                                      ]
                                                    )
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "el-dropdown-menu",
                                                  {
                                                    attrs: { slot: "dropdown" },
                                                    slot: "dropdown"
                                                  },
                                                  [
                                                    _c(
                                                      "transition",
                                                      {
                                                        attrs: { name: "fade" }
                                                      },
                                                      [
                                                        _c(
                                                          "el-popover",
                                                          {
                                                            staticClass:
                                                              "bnt-minus",
                                                            attrs: {
                                                              transition:
                                                                "el-fade-in-linear",
                                                              placement: "top",
                                                              width: "260"
                                                            },
                                                            model: {
                                                              value:
                                                                produit.visible,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  produit,
                                                                  "visible",
                                                                  $$v
                                                                )
                                                              },
                                                              expression:
                                                                "produit.visible"
                                                            }
                                                          },
                                                          [
                                                            _c("p", [
                                                              _vm._v(
                                                                "Voulez-vous vraiment supprimer?"
                                                              )
                                                            ]),
                                                            _vm._v(" "),
                                                            _c(
                                                              "div",
                                                              {
                                                                staticStyle: {
                                                                  "text-align":
                                                                    "right",
                                                                  margin: "0"
                                                                }
                                                              },
                                                              [
                                                                _c(
                                                                  "el-button",
                                                                  {
                                                                    attrs: {
                                                                      size:
                                                                        "mini",
                                                                      type:
                                                                        "text"
                                                                    },
                                                                    on: {
                                                                      click: function(
                                                                        $event
                                                                      ) {
                                                                        produit.visible = false
                                                                      }
                                                                    }
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      "Annuler"
                                                                    )
                                                                  ]
                                                                ),
                                                                _vm._v(" "),
                                                                _c(
                                                                  "el-button",
                                                                  {
                                                                    attrs: {
                                                                      type:
                                                                        "primary",
                                                                      size:
                                                                        "mini"
                                                                    },
                                                                    on: {
                                                                      click: function(
                                                                        $event
                                                                      ) {
                                                                        produit.visible = false
                                                                        _vm.removeSection(
                                                                          index
                                                                        )
                                                                      }
                                                                    }
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      "Confirmer"
                                                                    )
                                                                  ]
                                                                )
                                                              ],
                                                              1
                                                            ),
                                                            _vm._v(" "),
                                                            _c(
                                                              "el-dropdown-item",
                                                              {
                                                                attrs: {
                                                                  slot:
                                                                    "reference"
                                                                },
                                                                slot:
                                                                  "reference"
                                                              },
                                                              [
                                                                _vm._v(
                                                                  "Supprimer la section"
                                                                )
                                                              ]
                                                            )
                                                          ],
                                                          1
                                                        )
                                                      ],
                                                      1
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "transition",
                                                      {
                                                        attrs: { name: "fade" }
                                                      },
                                                      [
                                                        _c(
                                                          "el-popover",
                                                          {
                                                            staticClass:
                                                              "bnt-minus",
                                                            attrs: {
                                                              transition:
                                                                "el-fade-in-linear",
                                                              placement: "top",
                                                              width: "260"
                                                            },
                                                            model: {
                                                              value:
                                                                produit.visible2,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  produit,
                                                                  "visible2",
                                                                  $$v
                                                                )
                                                              },
                                                              expression:
                                                                "produit.visible2"
                                                            }
                                                          },
                                                          [
                                                            _c("p", [
                                                              _vm._v(
                                                                "Voulez-vous vraiment supprimer?"
                                                              )
                                                            ]),
                                                            _vm._v(" "),
                                                            _c(
                                                              "div",
                                                              {
                                                                staticStyle: {
                                                                  "text-align":
                                                                    "right",
                                                                  margin: "0"
                                                                }
                                                              },
                                                              [
                                                                _c(
                                                                  "el-button",
                                                                  {
                                                                    attrs: {
                                                                      size:
                                                                        "mini",
                                                                      type:
                                                                        "text"
                                                                    },
                                                                    on: {
                                                                      click: function(
                                                                        $event
                                                                      ) {
                                                                        produit.visible2 = false
                                                                      }
                                                                    }
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      "Annuler"
                                                                    )
                                                                  ]
                                                                ),
                                                                _vm._v(" "),
                                                                _c(
                                                                  "el-button",
                                                                  {
                                                                    attrs: {
                                                                      type:
                                                                        "primary",
                                                                      size:
                                                                        "mini"
                                                                    },
                                                                    on: {
                                                                      click: function(
                                                                        $event
                                                                      ) {
                                                                        produit.visible2 = false
                                                                        _vm.removeSectionChild(
                                                                          produit.numeroSection
                                                                        )
                                                                      }
                                                                    }
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      "Confirmer"
                                                                    )
                                                                  ]
                                                                )
                                                              ],
                                                              1
                                                            ),
                                                            _vm._v(" "),
                                                            _c(
                                                              "el-dropdown-item",
                                                              {
                                                                attrs: {
                                                                  slot:
                                                                    "reference"
                                                                },
                                                                slot:
                                                                  "reference"
                                                              },
                                                              [
                                                                _vm._v(
                                                                  "Supprimer les sous élements"
                                                                )
                                                              ]
                                                            )
                                                          ],
                                                          1
                                                        )
                                                      ],
                                                      1
                                                    )
                                                  ],
                                                  1
                                                )
                                              ],
                                              1
                                            )
                                          : _vm._e()
                                      ],
                                      1
                                    ),
                                    _vm._v(" "),
                                    !produit.numeroSection
                                      ? _c(
                                          "tr",
                                          [
                                            _vm._l(_vm.getNewLibelle, function(
                                              item,
                                              indice
                                            ) {
                                              return _c(
                                                "td",
                                                { key: indice },
                                                [
                                                  item === "quantite"
                                                    ? _c("el-input-number", {
                                                        staticClass:
                                                          "el-input-number",
                                                        attrs: {
                                                          size: "small",
                                                          min: 1
                                                        },
                                                        on: {
                                                          change: function(
                                                            $event
                                                          ) {
                                                            return _vm.calculTotalRow(
                                                              index
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value: produit[item],
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              produit,
                                                              item,
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "produit[item]"
                                                        }
                                                      })
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  item === "numero"
                                                    ? _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value:
                                                              produit[item],
                                                            expression:
                                                              "produit[item]"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "numero-ligne",
                                                        attrs: { type: "text" },
                                                        domProps: {
                                                          value: produit[item]
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              produit,
                                                              item,
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  _vm.defaultsInputs(item)
                                                    ? _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value:
                                                              produit[item],
                                                            expression:
                                                              "produit[item]"
                                                          }
                                                        ],
                                                        attrs: { type: "text" },
                                                        domProps: {
                                                          value: produit[item]
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              produit,
                                                              item,
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  !_vm.showRemise &&
                                                  item === "remise"
                                                    ? _c("vue-numeric", {
                                                        staticClass:
                                                          "remise-col",
                                                        attrs: {
                                                          currency: "%",
                                                          "output-type":
                                                            "Number",
                                                          "currency-symbol-position":
                                                            "suffix",
                                                          max: 200,
                                                          min: 0,
                                                          minus: false,
                                                          "empty-value": "0",
                                                          precision: 0
                                                        },
                                                        nativeOn: {
                                                          change: function(
                                                            $event
                                                          ) {
                                                            return _vm.calculMontantRemise(
                                                              index
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value: produit[item],
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              produit,
                                                              item,
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "produit[item]"
                                                        }
                                                      })
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  _vm.showRemise &&
                                                  item === "remise"
                                                    ? _c("vue-numeric", {
                                                        staticClass:
                                                          "remise-col",
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "Number",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          readonly: ""
                                                        },
                                                        model: {
                                                          value:
                                                            produit.montantRemise,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              produit,
                                                              "montantRemise",
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "produit.montantRemise"
                                                        }
                                                      })
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  item === "unite"
                                                    ? _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value:
                                                              produit[item],
                                                            expression:
                                                              "produit[item]"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "unite-col",
                                                        attrs: { type: "text" },
                                                        domProps: {
                                                          value: produit[item]
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              produit,
                                                              item,
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  item === "designation"
                                                    ? _c("textarea", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value: produit.nom,
                                                            expression:
                                                              "produit.nom"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control",
                                                        attrs: {
                                                          rows: "1",
                                                          placeholder:
                                                            "Saisir la désignation"
                                                        },
                                                        domProps: {
                                                          value: produit.nom
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              produit,
                                                              "nom",
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  item === "designation"
                                                    ? _c(
                                                        "el-select",
                                                        {
                                                          staticClass:
                                                            "select-icon",
                                                          on: {
                                                            change: function(
                                                              $event
                                                            ) {
                                                              return _vm.getProduct(
                                                                produit.nom,
                                                                index
                                                              )
                                                            }
                                                          },
                                                          model: {
                                                            value: produit.nom,
                                                            callback: function(
                                                              $$v
                                                            ) {
                                                              _vm.$set(
                                                                produit,
                                                                "nom",
                                                                $$v
                                                              )
                                                            },
                                                            expression:
                                                              "produit.nom"
                                                          }
                                                        },
                                                        _vm._l(
                                                          _vm.mesProduits,
                                                          function(item) {
                                                            return _c(
                                                              "el-option",
                                                              {
                                                                key: item.id,
                                                                attrs: {
                                                                  label:
                                                                    item.nom,
                                                                  value:
                                                                    item.nom
                                                                }
                                                              }
                                                            )
                                                          }
                                                        ),
                                                        1
                                                      )
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  item === "prix"
                                                    ? _c("vue-numeric", {
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "Number",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ]
                                                        },
                                                        nativeOn: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            return _vm.calculTotalRow(
                                                              index
                                                            )
                                                          }
                                                        },
                                                        model: {
                                                          value:
                                                            produit.prixVenteUnitaire,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              produit,
                                                              "prixVenteUnitaire",
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "produit.prixVenteUnitaire"
                                                        }
                                                      })
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  item === "total"
                                                    ? _c("vue-numeric", {
                                                        staticClass:
                                                          "total-col",
                                                        attrs: {
                                                          currency:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency"
                                                            ],
                                                          "currency-symbol-position":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "currency-symbol-position"
                                                            ],
                                                          "output-type":
                                                            "Number",
                                                          "empty-value": "0",
                                                          precision:
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "precision"
                                                            ],
                                                          "decimal-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "decimal-separator"
                                                            ],
                                                          "thousand-separator":
                                                            _vm
                                                              .currentCurrencyMask[
                                                              "thousand-separator"
                                                            ],
                                                          readonly: ""
                                                        },
                                                        model: {
                                                          value: produit[item],
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              produit,
                                                              item,
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "produit[item]"
                                                        }
                                                      })
                                                    : _vm._e()
                                                ],
                                                1
                                              )
                                            }),
                                            _vm._v(" "),
                                            _c(
                                              "transition",
                                              { attrs: { name: "fade" } },
                                              [
                                                _c(
                                                  "el-tooltip",
                                                  {
                                                    staticClass: "float-left",
                                                    attrs: {
                                                      content: "Supprimer",
                                                      placement: "top"
                                                    }
                                                  },
                                                  [
                                                    _c(
                                                      "el-popover",
                                                      {
                                                        staticClass:
                                                          "bnt-minus",
                                                        attrs: {
                                                          transition:
                                                            "el-fade-in-linear",
                                                          placement: "top",
                                                          width: "260"
                                                        },
                                                        model: {
                                                          value:
                                                            produit.visible,
                                                          callback: function(
                                                            $$v
                                                          ) {
                                                            _vm.$set(
                                                              produit,
                                                              "visible",
                                                              $$v
                                                            )
                                                          },
                                                          expression:
                                                            "produit.visible"
                                                        }
                                                      },
                                                      [
                                                        _c("p", [
                                                          _vm._v(
                                                            "Voulez-vous vraiment supprimer?"
                                                          )
                                                        ]),
                                                        _vm._v(" "),
                                                        _c(
                                                          "div",
                                                          {
                                                            staticStyle: {
                                                              "text-align":
                                                                "right",
                                                              margin: "0"
                                                            }
                                                          },
                                                          [
                                                            _c(
                                                              "el-button",
                                                              {
                                                                attrs: {
                                                                  size: "mini",
                                                                  type: "text"
                                                                },
                                                                on: {
                                                                  click: function(
                                                                    $event
                                                                  ) {
                                                                    produit.visible = false
                                                                  }
                                                                }
                                                              },
                                                              [
                                                                _vm._v(
                                                                  "Annuler"
                                                                )
                                                              ]
                                                            ),
                                                            _vm._v(" "),
                                                            _c(
                                                              "el-button",
                                                              {
                                                                attrs: {
                                                                  type:
                                                                    "primary",
                                                                  size: "mini"
                                                                },
                                                                on: {
                                                                  click: function(
                                                                    $event
                                                                  ) {
                                                                    produit.visible = false
                                                                    _vm.removeRows(
                                                                      index
                                                                    )
                                                                  }
                                                                }
                                                              },
                                                              [
                                                                _vm._v(
                                                                  "Confirmer"
                                                                )
                                                              ]
                                                            )
                                                          ],
                                                          1
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "button",
                                                          {
                                                            staticClass:
                                                              "btn btn-sm btn-danger btn-minus",
                                                            attrs: {
                                                              slot: "reference",
                                                              type: "button"
                                                            },
                                                            slot: "reference"
                                                          },
                                                          [
                                                            _c("i", {
                                                              staticClass:
                                                                "fas fa-minus-circle"
                                                            })
                                                          ]
                                                        )
                                                      ]
                                                    )
                                                  ],
                                                  1
                                                )
                                              ],
                                              1
                                            )
                                          ],
                                          2
                                        )
                                      : _vm._e()
                                  ])
                                })
                              ],
                              2
                            ),
                            _vm._v(" "),
                            _c(
                              "el-dropdown",
                              { attrs: { size: "small" } },
                              [
                                _c(
                                  "button",
                                  {
                                    staticClass:
                                      "btn btn-sm btn-info ml-5 el-dropdown-link",
                                    attrs: { type: "button" },
                                    on: { click: _vm.addRows }
                                  },
                                  [
                                    _c("i", {
                                      staticClass: "fas fa-plus-circle"
                                    })
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "el-dropdown-menu",
                                  {
                                    attrs: { slot: "dropdown" },
                                    slot: "dropdown"
                                  },
                                  [
                                    _c(
                                      "el-dropdown-item",
                                      {
                                        nativeOn: {
                                          click: function($event) {
                                            return _vm.addRows($event)
                                          }
                                        }
                                      },
                                      [_vm._v("Ligne")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "el-dropdown-item",
                                      {
                                        nativeOn: {
                                          click: function($event) {
                                            return _vm.addSection($event)
                                          }
                                        }
                                      },
                                      [_vm._v("Section")]
                                    )
                                  ],
                                  1
                                )
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "mt-top facture-footer ml-2 pt-2" },
                        [
                          _c(
                            "el-row",
                            { attrs: { gutter: 12 } },
                            [
                              _c(
                                "el-col",
                                { attrs: { span: 8 } },
                                [
                                  _c(
                                    "el-card",
                                    {
                                      staticClass: "el-card border-none",
                                      attrs: { shadow: "never" }
                                    },
                                    [
                                      _c("el-input", {
                                        attrs: {
                                          type: "textarea",
                                          rows: 4,
                                          placeholder:
                                            "Entrez les mentions légales ici"
                                        },
                                        model: {
                                          value: _vm.devisFooter["mentions"],
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.devisFooter,
                                              "mentions",
                                              $$v
                                            )
                                          },
                                          expression: "devisFooter['mentions']"
                                        }
                                      })
                                    ],
                                    1
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "el-col",
                                { attrs: { span: 8 } },
                                [
                                  _c(
                                    "el-card",
                                    {
                                      staticClass: "el-card border-none",
                                      attrs: { shadow: "never" }
                                    },
                                    [
                                      _vm._v(
                                        "\n                    Date : " +
                                          _vm._s(_vm.getCurrentDate()) +
                                          "\n                    \n                    "
                                      ),
                                      _c("br"),
                                      _vm._v(" "),
                                      _c("br"),
                                      _vm._v("A :\n                    "),
                                      _c("el-input", {
                                        staticClass: "place",
                                        attrs: {
                                          type: "textarea",
                                          rows: 2,
                                          placeholder: "Saisir le lieu"
                                        },
                                        model: {
                                          value: _vm.devisFooter["lieu"],
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.devisFooter,
                                              "lieu",
                                              $$v
                                            )
                                          },
                                          expression: "devisFooter['lieu']"
                                        }
                                      })
                                    ],
                                    1
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "el-col",
                                { attrs: { span: 8 } },
                                [
                                  _c(
                                    "el-card",
                                    {
                                      staticClass:
                                        "mr-2 el-card sign border-none pl-5",
                                      attrs: { shadow: "never" }
                                    },
                                    [
                                      _vm.switchState
                                        ? _c("img", {
                                            staticClass: "pr-5",
                                            attrs: {
                                              src:
                                                "data:image/png;base64," +
                                                _vm.signature,
                                              width: "100%",
                                              alt: ""
                                            }
                                          })
                                        : _vm._e(),
                                      _vm._v(" "),
                                      _c("el-switch", {
                                        staticClass: "switchBtn",
                                        model: {
                                          value: _vm.switchState,
                                          callback: function($$v) {
                                            _vm.switchState = $$v
                                          },
                                          expression: "switchState"
                                        }
                                      })
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("hr", { staticClass: "style-eight" }),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "footer-note pl-3 ml-3",
                          domProps: { innerHTML: _vm._s(_vm.footer) }
                        },
                        [
                          _vm._v(
                            "\n            " +
                              _vm._s(_vm.footer) +
                              "\n          "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "mb-5 mt-4" }, [
                        _c(
                          "button",
                          {
                            staticClass: "save ml-4",
                            on: {
                              click: function($event) {
                                _vm.active = 2
                              }
                            }
                          },
                          [_vm._v("Retour")]
                        ),
                        _vm._v(" "),
                        _c(
                          "button",
                          {
                            staticClass: "save mr-5 pr-2 float-right",
                            attrs: {
                              "data-toggle": "modal",
                              "data-target": "#responsive-modal"
                            },
                            on: {
                              click: function($event) {
                                return _vm.saveDevis()
                              }
                            }
                          },
                          [_vm._v("\n                Terminé\n            ")]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass: "modal fade",
                            staticStyle: { display: "none" },
                            attrs: {
                              id: "responsive-modal",
                              tabindex: "-1",
                              role: "dialog",
                              "aria-labelledby": "myModalLabel",
                              "aria-hidden": "true"
                            }
                          },
                          [
                            _c("div", { staticClass: "modal-dialog" }, [
                              _c("div", { staticClass: "modal-content" }, [
                                _vm._m(1),
                                _vm._v(" "),
                                _c("div", { staticClass: "modal-footer" }, [
                                  _c(
                                    "button",
                                    {
                                      staticClass:
                                        "btn btn-primary waves-effect waves-light mr-5",
                                      attrs: { type: "button" },
                                      on: {
                                        click: function($event) {
                                          return _vm.printPdf("Devis")
                                        }
                                      }
                                    },
                                    [
                                      _c("i", {
                                        staticClass: "fa fa-download pr-2"
                                      }),
                                      _vm._v("Devis PDF")
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "button",
                                    {
                                      staticClass:
                                        "btn btn-primary waves-effect waves-light",
                                      attrs: { type: "button" },
                                      on: {
                                        click: function($event) {
                                          return _vm.printPdf("Facture")
                                        }
                                      }
                                    },
                                    [
                                      _c("i", {
                                        staticClass: "fa fa-download pr-2"
                                      }),
                                      _vm._v("Facture PDF")
                                    ]
                                  )
                                ])
                              ])
                            ])
                          ]
                        )
                      ])
                    ])
                  : _vm._e()
              ],
              1
            )
          ])
        ]
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-hidden": "true"
          }
        },
        [_vm._v("×")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-hidden": "true"
          }
        },
        [_vm._v("×")]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/assets/js/views/suivi/devis/components/StepNav.vue":
/*!**********************************************************************!*\
  !*** ./resources/assets/js/views/suivi/devis/components/StepNav.vue ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _StepNav_vue_vue_type_template_id_589ce2d6_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./StepNav.vue?vue&type=template&id=589ce2d6&scoped=true& */ "./resources/assets/js/views/suivi/devis/components/StepNav.vue?vue&type=template&id=589ce2d6&scoped=true&");
/* harmony import */ var _StepNav_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./StepNav.vue?vue&type=script&lang=js& */ "./resources/assets/js/views/suivi/devis/components/StepNav.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _StepNav_vue_vue_type_style_index_0_id_589ce2d6_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./StepNav.vue?vue&type=style&index=0&id=589ce2d6&scoped=true&lang=css& */ "./resources/assets/js/views/suivi/devis/components/StepNav.vue?vue&type=style&index=0&id=589ce2d6&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _StepNav_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _StepNav_vue_vue_type_template_id_589ce2d6_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _StepNav_vue_vue_type_template_id_589ce2d6_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "589ce2d6",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/views/suivi/devis/components/StepNav.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/assets/js/views/suivi/devis/components/StepNav.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************!*\
  !*** ./resources/assets/js/views/suivi/devis/components/StepNav.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StepNav_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./StepNav.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/components/StepNav.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StepNav_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/views/suivi/devis/components/StepNav.vue?vue&type=style&index=0&id=589ce2d6&scoped=true&lang=css&":
/*!*******************************************************************************************************************************!*\
  !*** ./resources/assets/js/views/suivi/devis/components/StepNav.vue?vue&type=style&index=0&id=589ce2d6&scoped=true&lang=css& ***!
  \*******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_StepNav_vue_vue_type_style_index_0_id_589ce2d6_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/style-loader!../../../../../../../node_modules/css-loader??ref--7-1!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./StepNav.vue?vue&type=style&index=0&id=589ce2d6&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/components/StepNav.vue?vue&type=style&index=0&id=589ce2d6&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_StepNav_vue_vue_type_style_index_0_id_589ce2d6_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_StepNav_vue_vue_type_style_index_0_id_589ce2d6_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_StepNav_vue_vue_type_style_index_0_id_589ce2d6_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_StepNav_vue_vue_type_style_index_0_id_589ce2d6_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_StepNav_vue_vue_type_style_index_0_id_589ce2d6_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/assets/js/views/suivi/devis/components/StepNav.vue?vue&type=template&id=589ce2d6&scoped=true&":
/*!*****************************************************************************************************************!*\
  !*** ./resources/assets/js/views/suivi/devis/components/StepNav.vue?vue&type=template&id=589ce2d6&scoped=true& ***!
  \*****************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StepNav_vue_vue_type_template_id_589ce2d6_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./StepNav.vue?vue&type=template&id=589ce2d6&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/components/StepNav.vue?vue&type=template&id=589ce2d6&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StepNav_vue_vue_type_template_id_589ce2d6_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StepNav_vue_vue_type_template_id_589ce2d6_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/assets/js/views/suivi/devis/components/headerInfos.vue":
/*!**************************************************************************!*\
  !*** ./resources/assets/js/views/suivi/devis/components/headerInfos.vue ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _headerInfos_vue_vue_type_template_id_5db00f37_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./headerInfos.vue?vue&type=template&id=5db00f37&scoped=true& */ "./resources/assets/js/views/suivi/devis/components/headerInfos.vue?vue&type=template&id=5db00f37&scoped=true&");
/* harmony import */ var _headerInfos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./headerInfos.vue?vue&type=script&lang=js& */ "./resources/assets/js/views/suivi/devis/components/headerInfos.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _headerInfos_vue_vue_type_style_index_0_id_5db00f37_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./headerInfos.vue?vue&type=style&index=0&id=5db00f37&scoped=true&lang=css& */ "./resources/assets/js/views/suivi/devis/components/headerInfos.vue?vue&type=style&index=0&id=5db00f37&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _headerInfos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _headerInfos_vue_vue_type_template_id_5db00f37_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _headerInfos_vue_vue_type_template_id_5db00f37_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "5db00f37",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/views/suivi/devis/components/headerInfos.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/assets/js/views/suivi/devis/components/headerInfos.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************!*\
  !*** ./resources/assets/js/views/suivi/devis/components/headerInfos.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_headerInfos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./headerInfos.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/components/headerInfos.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_headerInfos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/views/suivi/devis/components/headerInfos.vue?vue&type=style&index=0&id=5db00f37&scoped=true&lang=css&":
/*!***********************************************************************************************************************************!*\
  !*** ./resources/assets/js/views/suivi/devis/components/headerInfos.vue?vue&type=style&index=0&id=5db00f37&scoped=true&lang=css& ***!
  \***********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_headerInfos_vue_vue_type_style_index_0_id_5db00f37_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/style-loader!../../../../../../../node_modules/css-loader??ref--7-1!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./headerInfos.vue?vue&type=style&index=0&id=5db00f37&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/components/headerInfos.vue?vue&type=style&index=0&id=5db00f37&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_headerInfos_vue_vue_type_style_index_0_id_5db00f37_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_headerInfos_vue_vue_type_style_index_0_id_5db00f37_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_headerInfos_vue_vue_type_style_index_0_id_5db00f37_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_headerInfos_vue_vue_type_style_index_0_id_5db00f37_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_headerInfos_vue_vue_type_style_index_0_id_5db00f37_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/assets/js/views/suivi/devis/components/headerInfos.vue?vue&type=template&id=5db00f37&scoped=true&":
/*!*********************************************************************************************************************!*\
  !*** ./resources/assets/js/views/suivi/devis/components/headerInfos.vue?vue&type=template&id=5db00f37&scoped=true& ***!
  \*********************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_headerInfos_vue_vue_type_template_id_5db00f37_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./headerInfos.vue?vue&type=template&id=5db00f37&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/components/headerInfos.vue?vue&type=template&id=5db00f37&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_headerInfos_vue_vue_type_template_id_5db00f37_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_headerInfos_vue_vue_type_template_id_5db00f37_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/assets/js/views/suivi/devis/components/secondTable.vue":
/*!**************************************************************************!*\
  !*** ./resources/assets/js/views/suivi/devis/components/secondTable.vue ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _secondTable_vue_vue_type_template_id_ea5ec20e_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./secondTable.vue?vue&type=template&id=ea5ec20e&scoped=true& */ "./resources/assets/js/views/suivi/devis/components/secondTable.vue?vue&type=template&id=ea5ec20e&scoped=true&");
/* harmony import */ var _secondTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./secondTable.vue?vue&type=script&lang=js& */ "./resources/assets/js/views/suivi/devis/components/secondTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _secondTable_vue_vue_type_style_index_0_id_ea5ec20e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./secondTable.vue?vue&type=style&index=0&id=ea5ec20e&scoped=true&lang=css& */ "./resources/assets/js/views/suivi/devis/components/secondTable.vue?vue&type=style&index=0&id=ea5ec20e&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _secondTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _secondTable_vue_vue_type_template_id_ea5ec20e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _secondTable_vue_vue_type_template_id_ea5ec20e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "ea5ec20e",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/views/suivi/devis/components/secondTable.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/assets/js/views/suivi/devis/components/secondTable.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************!*\
  !*** ./resources/assets/js/views/suivi/devis/components/secondTable.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_secondTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./secondTable.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/components/secondTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_secondTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/views/suivi/devis/components/secondTable.vue?vue&type=style&index=0&id=ea5ec20e&scoped=true&lang=css&":
/*!***********************************************************************************************************************************!*\
  !*** ./resources/assets/js/views/suivi/devis/components/secondTable.vue?vue&type=style&index=0&id=ea5ec20e&scoped=true&lang=css& ***!
  \***********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_secondTable_vue_vue_type_style_index_0_id_ea5ec20e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/style-loader!../../../../../../../node_modules/css-loader??ref--7-1!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./secondTable.vue?vue&type=style&index=0&id=ea5ec20e&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/components/secondTable.vue?vue&type=style&index=0&id=ea5ec20e&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_secondTable_vue_vue_type_style_index_0_id_ea5ec20e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_secondTable_vue_vue_type_style_index_0_id_ea5ec20e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_secondTable_vue_vue_type_style_index_0_id_ea5ec20e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_secondTable_vue_vue_type_style_index_0_id_ea5ec20e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_secondTable_vue_vue_type_style_index_0_id_ea5ec20e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/assets/js/views/suivi/devis/components/secondTable.vue?vue&type=template&id=ea5ec20e&scoped=true&":
/*!*********************************************************************************************************************!*\
  !*** ./resources/assets/js/views/suivi/devis/components/secondTable.vue?vue&type=template&id=ea5ec20e&scoped=true& ***!
  \*********************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_secondTable_vue_vue_type_template_id_ea5ec20e_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./secondTable.vue?vue&type=template&id=ea5ec20e&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/components/secondTable.vue?vue&type=template&id=ea5ec20e&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_secondTable_vue_vue_type_template_id_ea5ec20e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_secondTable_vue_vue_type_template_id_ea5ec20e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/assets/js/views/suivi/devis/formulaire-devis.vue":
/*!********************************************************************!*\
  !*** ./resources/assets/js/views/suivi/devis/formulaire-devis.vue ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _formulaire_devis_vue_vue_type_template_id_7e66b440___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./formulaire-devis.vue?vue&type=template&id=7e66b440& */ "./resources/assets/js/views/suivi/devis/formulaire-devis.vue?vue&type=template&id=7e66b440&");
/* harmony import */ var _formulaire_devis_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./formulaire-devis.vue?vue&type=script&lang=js& */ "./resources/assets/js/views/suivi/devis/formulaire-devis.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _formulaire_devis_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./formulaire-devis.vue?vue&type=style&index=0&lang=css& */ "./resources/assets/js/views/suivi/devis/formulaire-devis.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _formulaire_devis_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _formulaire_devis_vue_vue_type_template_id_7e66b440___WEBPACK_IMPORTED_MODULE_0__["render"],
  _formulaire_devis_vue_vue_type_template_id_7e66b440___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/views/suivi/devis/formulaire-devis.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/assets/js/views/suivi/devis/formulaire-devis.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************!*\
  !*** ./resources/assets/js/views/suivi/devis/formulaire-devis.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_formulaire_devis_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./formulaire-devis.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/formulaire-devis.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_formulaire_devis_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/views/suivi/devis/formulaire-devis.vue?vue&type=style&index=0&lang=css&":
/*!*****************************************************************************************************!*\
  !*** ./resources/assets/js/views/suivi/devis/formulaire-devis.vue?vue&type=style&index=0&lang=css& ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_formulaire_devis_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/style-loader!../../../../../../node_modules/css-loader??ref--7-1!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./formulaire-devis.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/formulaire-devis.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_formulaire_devis_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_formulaire_devis_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_formulaire_devis_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_formulaire_devis_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_formulaire_devis_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/assets/js/views/suivi/devis/formulaire-devis.vue?vue&type=template&id=7e66b440&":
/*!***************************************************************************************************!*\
  !*** ./resources/assets/js/views/suivi/devis/formulaire-devis.vue?vue&type=template&id=7e66b440& ***!
  \***************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_formulaire_devis_vue_vue_type_template_id_7e66b440___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./formulaire-devis.vue?vue&type=template&id=7e66b440& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/views/suivi/devis/formulaire-devis.vue?vue&type=template&id=7e66b440&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_formulaire_devis_vue_vue_type_template_id_7e66b440___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_formulaire_devis_vue_vue_type_template_id_7e66b440___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/assets/js/views/suivi/devis/pdf/pdfBuildBill.js":
/*!*******************************************************************!*\
  !*** ./resources/assets/js/views/suivi/devis/pdf/pdfBuildBill.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Pdf__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Pdf */ "./resources/assets/js/views/suivi/devis/pdf/Pdf.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }



var PdfBuildBill = /*#__PURE__*/function (_Pdf) {
  _inherits(PdfBuildBill, _Pdf);

  var _super = _createSuper(PdfBuildBill);

  function PdfBuildBill() {
    var _this;

    var base = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'Devis';
    var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];

    _classCallCheck(this, PdfBuildBill);

    _this = _super.call(this, base, options);
    _this.type = type;
    return _this;
  }
  /**
   * Titre conventionnel des factures et ds devis
   * @returns {string}
   */


  _createClass(PdfBuildBill, [{
    key: "getTitlePdf",
    value: function getTitlePdf() {
      return _get(_getPrototypeOf(PdfBuildBill.prototype), "getTitlePdf", this).call(this) + '_' + this.data.infosFacture[0].content + '_' + this.data.infosClient[0].inputName;
    }
  }, {
    key: "docDefinition",
    value: function docDefinition() {
      this.getTitlePdf();
      this.setFooter(this.stripTags(this.options.mentions.footer));
      return _get(_getPrototypeOf(PdfBuildBill.prototype), "docDefinition", this).call(this);
    }
    /**
     * En tete du tableau des produits nom Entete, taille et style des colonnes enfants
     * @param elements - elements à retrouver dans le tableau
     * @returns {[][]} - [] tableau des entetes, tableau des taille, tableau des styles
     */

  }, {
    key: "headerProduct",
    value: function headerProduct(elements) {
      var _this2 = this;

      var headerCellElement = [];
      var headerSize = [];
      var styleCellElement = [];
      var i = 0;
      var styles = {
        style: ['montant'],
        borderColor: ['#efefef', '#efefef', '#efefef', '#efefef'],
        border: [true, false, true, true],
        type: {
          name: 'FCFA'
        }
      };
      elements.forEach(function (elt) {
        if (elt.hasOwnProperty('libelle')) {
          switch (elt.libelle) {
            case 'N°':
              headerCellElement.push('numero');
              headerSize.push([i, '5%']);
              styleCellElement.push({
                position: i,
                style: {
                  style: ['montant'],
                  borderColor: ['#efefef', '#efefef', '#efefef', '#efefef'],
                  border: [true, false, true, true]
                }
              });
              break;

            case 'Désignation':
              headerCellElement.push('nom');
              break;

            case 'Unité':
              headerCellElement.push('unite');
              headerSize.push([i, '10%']);
              break;

            case 'Quantité':
              headerCellElement.push('quantite');
              headerSize.push([i, '10%']);
              styleCellElement.push({
                position: i,
                style: {
                  style: ['montant'],
                  borderColor: ['#efefef', '#efefef', '#efefef', '#efefef'],
                  border: [true, false, true, true]
                }
              });
              break;

            case 'Prix de vente Unitaire HT':
              headerCellElement.push('prixVenteUnitaire');
              headerSize.push([i, '30%']);
              styleCellElement.push({
                position: i,
                style: styles
              });
              break;

            case 'Remise%':
              if (_this2.options.discount === '%') {
                headerCellElement.push('remise');
              } else {
                headerCellElement.push('montantRemise');
              }

              headerSize.push([i, '10%']);
              styleCellElement.push({
                position: i,
                style: {
                  style: ['montant'],
                  borderColor: ['#efefef', '#efefef', '#efefef', '#efefef'],
                  border: [true, false, true, true],
                  type: {
                    name: _this2.options.discount
                  }
                }
              });
              break;

            case 'TOTAL HT':
              headerCellElement.push('total');
              styleCellElement.push({
                position: i,
                style: styles
              });
              break;

            default:
              console.warn('Il manque un élément au Header');
          }
        }

        i++;
      });
      return [headerCellElement, headerSize, styleCellElement];
    }
    /**
     * Champ des sections d'un tableau d'objets
     * @param nbr {Number} nombre de colonnes
     * @returns {any[][]} liste des colonnes à afficher
     */

  }, {
    key: "headerSection",
    value: function headerSection(nbr) {
      nbr = nbr - 1;
      var styles = {
        style: ['section'],
        border: [false, false, false, false]
      };
      var styleCellElement = new Array(nbr);
      var sections = new Array(nbr);
      sections[0] = 'numeroSection';
      sections[1] = 'titleSection';
      styleCellElement[0] = styleCellElement[1] = styles;
      var i = 2;

      while (i < nbr) {
        sections[i] = '';
        styleCellElement[i] = styles;
        i++;
      }

      sections[nbr] = 'subTotal';
      styleCellElement[nbr] = {
        style: ['section'],
        border: [false, false, false, false],
        type: {
          name: 'FCFA'
        },
        visible: 'subTotalIsVisible'
      };
      return [sections, styleCellElement];
    }
    /**
     *
     * @param text {String} - Titre du montant
     * @param montant {Number | String}
     * @param bg
     * @returns Array - Une ligne du tableau tslesmonants
     */

  }, {
    key: "montantFacture",
    value: function montantFacture(text, montant) {
      var bg = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'bgColorHeaderTab';
      return [this.getCell(text, {
        style: ['lefted', 'titleMontant'],
        border: [true, true, true, true],
        borderColor: ['#DCDCDC', '#DCDCDC', '#DCDCDC', '#DCDCDC']
      }), this.getCell(montant, {
        style: ['righted', bg, 'montant'],
        border: [true, true, true, true],
        borderColor: ['#DCDCDC', '#DCDCDC', '#DCDCDC', '#DCDCDC'],
        type: {
          name: 'FCFA'
        }
      })];
    }
  }, {
    key: "allMontant",
    value: function allMontant(_allMontant) {
      var results = [];

      for (var montant in _allMontant) {
        switch (montant) {
          case 'remiseTotalMontant':
            if (_allMontant[montant].value) {
              results.push(this.montantFacture('Remise Totale HT', _allMontant[montant].value, 'bgColorLightBlue'));
            }

            break;

          case 'isVisible':
            if (_allMontant[montant] === 'HT') {
              results.push(this.montantFacture('Montant Total Facturé HT', _allMontant['montantTotalHt'].value));
            } else {
              results.push(this.montantFacture('Montant Total Facturé TTC', _allMontant['montantTotalTtc'].value));
            }

            break;

          case 'isVisible2':
            if (_allMontant[montant] === 'HT') {
              results.push(this.montantFacture('Montant Total à payer  HT', _allMontant['montantTotalHt'].value));
            } else {
              results.push(this.montantFacture('Montant Total à payer  TTC', _allMontant['montantTotalTtc'].value));
            }

            break;

          case 'isVisible3':
            if (_allMontant[montant] === 'TVA') {
              results.push(this.montantFacture('Taxes : TVA ' + _allMontant['taxesSelectTaux'].tva.taux + '%', parseInt(_allMontant['tva'].montant), 'bgColorLightBlue'));
            } else {
              results.push(this.montantFacture('Taxes : TPS ' + _allMontant['taxesSelectTaux'].tps.taux + '%', parseInt(_allMontant['tps'].montant), 'bgColorLightBlue'));
            }

            break;

          default:
        }
      }

      return results;
    }
    /*
    * Contenu du PDF
    * @returns {*[]}
    */

  }, {
    key: "content",
    value: function content() {
      return [
      /** Entete **/
      {
        columns: [this.getColumn({
          type: 'image',
          content: this.logo
        }, {
          width: '*',
          margin: [5, -15, 0, 12]
        }, {
          position: 'left',
          height: 60,
          width: 130
        }), this.getColumn({
          content: this.type + ' : ' + this.data.infosFacture[0].content
        }, {
          width: '40%',
          margin: [98, 0, 0, 0]
        }, {
          position: 'right'
        })],
        columnGap: 2
      },
      /** General Info**/
      {
        columns: [{
          width: '40%',
          text: [{
            text: this.stripTags(this.options.mentions.infoEnt),
            alignment: 'left',
            fontSize: 10
          }]
        }, {
          table: _Pdf__WEBPACK_IMPORTED_MODULE_0__["default"].getArray(['*'], this.getBodyArrayByLine(this.data.infosClient, ['inputName'], [{
            style: ['info', 'righted'],
            border: [false, false, false, false]
          }]))
        }],
        columnGap: 2
      }, {
        table: _Pdf__WEBPACK_IMPORTED_MODULE_0__["default"].getArray(['*'], [[{
          text: '',
          border: [false, false, false, false],
          margin: [0, 7]
        }], [{
          text: 'Prestation : ' + this.data.titrePrestation.toUpperCase(),
          style: ['prestation', 'lefted'],
          border: [true, false, false, false],
          borderColor: ['#00c875']
        }], [{
          text: '',
          border: [false, false, false, false],
          margin: [0, 7]
        }]])
      },
      /** Facture Info **/
      {
        table: _Pdf__WEBPACK_IMPORTED_MODULE_0__["default"].getArray(this.getColumns(_Pdf__WEBPACK_IMPORTED_MODULE_0__["default"].getNbrColumns(this.data.infosFacture)), [this.getBodyByColumns(this.data.infosFacture, 'libelle', {
          style: ['bgColorHeaderTab'],
          borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969'],
          border: [true, false, true, false]
        }), this.getBodyByColumns(this.data.infosFacture, 'content', {
          style: ['montant'],
          borderColor: ['#efefef', '#efefef', '#efefef', '#efefef'],
          border: [true, false, true, true]
        })])
      }, {
        text: '',
        margin: [0, 10]
      },
      /** Produits**/
      {
        table: _Pdf__WEBPACK_IMPORTED_MODULE_0__["default"].getArray(this.getColumns(this.options.productsHeader.length, this.headerProduct(this.options.productsHeader)[1]), [this.getBodyArrayByColumns(this.options.productsHeader, 'libelle', {
          style: ['bgColorHeaderTab'],
          borderColor: ['#DCDCDC', '#696969', '#DCDCDC', '#696969'],
          border: [true, false, true, false]
        })].concat(this.getBodyArrayByLine( // Tableau de produits
        this.data.produits, //colonnes du tableau à afficher
        this.headerProduct(this.options.productsHeader)[0], //options de chaque cellule du tableau
        this.getOptionsCell({
          style: ['info'],
          borderColor: ['#efefef', '#efefef', '#efefef', '#efefef'],
          border: [true, false, true, true]
        }, this.headerProduct(this.options.productsHeader)[2], _Pdf__WEBPACK_IMPORTED_MODULE_0__["default"].getNbrColumns(this.options.productsHeader)), //colonnes des sections à afficher
        this.headerSection(_Pdf__WEBPACK_IMPORTED_MODULE_0__["default"].getNbrColumns(this.options.productsHeader)))))
      }, {
        text: '',
        margin: [0, 10]
      }, {
        columns: [
        /** Mode de paiement**/
        {
          width: '60%',
          table: _Pdf__WEBPACK_IMPORTED_MODULE_0__["default"].getArray(['*', '*', '14%', '33%'], [[{
            text: 'Paiement',
            style: ['bgColorHeaderTab'],
            border: [false, false, true, false],
            borderColor: ['#DCDCDC']
          }, {
            text: 'Acompte',
            style: ['bgColorHeaderTab'],
            border: [true, false, true, false],
            borderColor: ['#DCDCDC', '#DCDCDC', '#DCDCDC']
          }, {
            text: 'Part(%)',
            style: ['bgColorHeaderTab'],
            border: [true, false, true, false],
            borderColor: ['#DCDCDC', '#DCDCDC', '#DCDCDC']
          }, {
            text: 'Date de règlement',
            style: ['bgColorHeaderTab'],
            border: [true, false, true, false],
            borderColor: ['#DCDCDC', '#DCDCDC', '#DCDCDC']
          }], [{
            text: this.data.paymentsInfos.paiement.montant ? this.contentFormat(parseInt(this.data.paymentsInfos.paiement.montant), 'FCFA') : '',
            style: ['montant'],
            border: [true, false, true, true],
            borderColor: ['#efefef', '#efefef', '#efefef', '#efefef']
          }, {
            text: this.data.paymentsInfos.accompte.montant ? this.contentFormat(parseInt(this.data.paymentsInfos.accompte.montant), 'FCFA') : '',
            style: ['montant'],
            border: [true, false, true, true],
            borderColor: ['#efefef', '#efefef', '#efefef', '#efefef']
          }, {
            text: this.data.paymentsInfos.part.montant ? this.data.paymentsInfos.part.montant + ' %' : '0 %',
            style: ['montant'],
            border: [true, false, true, true],
            borderColor: ['#efefef', '#efefef', '#efefef', '#efefef']
          }, {
            text: this.data.paymentsInfos.date ? this.data.paymentsInfos.date.value : '',
            style: ['montant'],
            border: [true, false, true, true],
            borderColor: ['#efefef', '#efefef', '#efefef', '#efefef']
          }]])
        },
        /** Montants **/
        {
          table: _Pdf__WEBPACK_IMPORTED_MODULE_0__["default"].getArray(['62%', '*'], this.allMontant(this.options.montants))
        }],
        columnGap: 2
      }, {
        text: '',
        margin: [0, 25]
      },
      /** Footer**/
      {
        columns: [{
          width: '40%',
          text: [{
            text: this.data.footer.mentions,
            margin: [3, 2],
            style: ['info']
          }, {
            text: ' ',
            margin: [0, 6]
          }, {
            text: 'Date : ' + this.data.footer.date + ' à ' + this.data.footer.lieu,
            margin: [3, 2],
            style: ['info']
          }]
        }, this.getColumn({
          type: 'image',
          content: this.signature
        }, {
          width: '*',
          margin: [98, 0, 0, 0]
        }, {
          position: 'right',
          width: 130,
          height: 60
        })],
        columnGap: 2
      }];
    }
  }]);

  return PdfBuildBill;
}(_Pdf__WEBPACK_IMPORTED_MODULE_0__["default"]);

/* harmony default export */ __webpack_exports__["default"] = (PdfBuildBill);

/***/ })

}]);
//# sourceMappingURL=suivi.devis.formulaire-devis.js.map