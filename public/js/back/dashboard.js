
//selection de toutes les évaluateurs
function getEvaluateurs()
{
    var query = firestore.collection("users").where('profile', '==', firestore.collection('profile').doc('2'));
    var evaluateurs = $('#evaluateurs');
    query.get().then(function (docData) {
        var arrObj = [];
        docData.forEach(function (data) {
            var obj = data.data().id;
            arrObj.push(obj);
        });
        // console.log(arrObj);
        evaluateurs.text(arrObj.length);
    });
}

//liste tous les projets
function getData()
{
    var query = firestore.collection("summary");
    var projetList = $('#projet');
    query.get().then(function (docData)
    {
        var arrObj = [];

        docData.forEach(function (data) {
            var obj = data.data();
            var id = data.id;
            obj.id = data.id;
            arrObj.push(obj);
        });
        projetList.text(arrObj.length);
    })
}

//liste tous les projets évalués
function getProjetEvalues()
{
    var query = firestore.collection("summary").where('statut', '==', 'Evalué');
    var projetEvalueList = $('#projetEvalue');
    query.get().then(function (docData)
    {
        var arrObj = [];

        docData.forEach(function (data) {
            var obj = data.data();
            var id = data.id;
            obj.id = data.id;
            arrObj.push(obj);
        });
        projetEvalueList.text(arrObj.length);
    })
}

//liste tous les projets évalués
function getAllUsers()
{
    var query = firestore.collection("users");
    var usersCount = $('#utilisateurs');
    query.get().then(function (docData)
    {
        var arrObj = [];

        docData.forEach(function (data) {
            var obj = data.data();
            var id = data.id;
            obj.id = data.id;
            arrObj.push(obj);
        });
        usersCount.text(arrObj.length);
    })
}

 $(document).ready(function () {
    getData(); //selectionne tous les projets
    getProjetEvalues(); //selectionne tous les projets évalué
    getAllUsers(); //selectionne tous les users
    getEvaluateurs(); //selectionne tous les évaluateurs
    //  console.log(getData());
    initFirebase();
});
