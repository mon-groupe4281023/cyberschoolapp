//ici les operations CRUD pour les activites

//selection de toutes les branches
function getBranches() {
    var select = document.getElementById('branche');
    var query = firestore.collection("branches");
    query.get().then(function (docData) {
        if (docData.size) {
            docData.forEach(function (data) {
                var obj = data.data();
                select.options[select.options.length] = new Option(obj.libelle, data.id);
            });
        }
    });
}

//selectionne la branche d'une activite
function getActiviteBranche() {
    var select = document.getElementById('branche');
    var query = firestore.collection("branches");
    query.get().then(function (docData) {
        if (docData.size) {
            docData.forEach(function (data) {
                var obj = data.data();
                select.options[select.options.length] = new Option(obj.title, data.id);
            });
        }
    });
}

//selection de tous les documents de la collection activites
function getData() {
    var branches = {};
    var activites = {};

    firestore.collection('branches').get().then((results) => {
        results.forEach((doc) => {
            branches[doc.id] = doc.data();
        });
        entr = firestore.collection('activites');
        entr.get().then((docSnaps) => {
            var arrObj = [];
            docSnaps.forEach((doc) => {
                activites = doc.data();
                var id = activites.branche.id;
                activites.branche = branches[id].libelle;
                activites.btnAction = '<button type="button" id="' + doc.id + '"  data-id="' + doc.id + '" data-toggle="modal" data-target="#modalEditEntr" class="btn btn-primary btn_edit" name="btn_edit">Modifier</button>&nbsp<button type="button" id="' + doc.id + '" class="btn btn-danger btn_delete">Supprimer</button>';
                arrObj.push(activites);
                // console.log(arrObj);
            });
            dtTable.clear();
            dtTable.rows.add(arrObj);
            dtTable.order([0, 'desc']).draw();
        });
    });

}

function saveData() {
    var id = $("#inputId").val();
    var branchId = $("#branche").val();
    var activite = $("#activite").val();
    var code = $("#code_activite").val();
    var data = {
        libelle: activite,
        code: code,
    };
    data.branche = firestore.collection('branches').doc(branchId);
    if (id === "") {
        addfirestore(data);
    } else {
        updatefirestore(data, id);
    }

}

// Enregistrement des données
function addfirestore(obj) {
    firestore.collection("activites").
        doc().set(obj)
        .then(function () {
            getData();
            new PNotify({
                title: 'Enregistrement réussi',
                text: "Vous avez enregistré une nouvelle activité",
                type: "success",
                icon: false
            });
        })
        .catch(function (error) {
            new PNotify({
                title: 'Erreur',
                text: "Enregistrement d'une nouvelle activité échoué",
                type: "danger",
                icon: false
            });
        });
}

function updatefirestore(obj, id) {
    firestore.collection("activites").doc(id).update(obj).then(function () {
        getData();
        new PNotify({
            title: 'Modification réussie',
            text: "Vous avez modifié avec succès une activité",
            type: "success",
            animation: "bounceInUp",
            icon: false
        });
    });
    // console.log(id);
}

function deletefirestore(id) {
    firestore.collection("activites").doc(id).delete()
        .then(function () {
            getData();
            new PNotify({
                title: 'Suppression réussie',
                text: "Vous avez supprimé une activité",
                type: "danger",
                animation: "bounceInUp",
                icon: false
            });
            // confirm();
        });
}

function clearInput() {
    $("#activite").val('');
    $("#code_activite").val('');
    $("#inputId").val('');
    $("#branche").text('');
}

$(document).ready(function () {
    dtTable = $('#example').
        DataTable({
            columns:
                [
                    { data: "libelle" },
                    { data: "branche" },
                    { data: "code" },
                    { data: "btnAction" }
                ]
        });

    $("#btnSaveActivite").on('click', function () {
        saveData();
        clearInput();
    });

    initFirebase();
    getBranches();
    getData();
});

$(document).on('click', '.btn_delete', function () {
    var id = $(this).attr("id");
    deletefirestore(id);
    initFirebase();
    getData();
});

$(document).on('click', '.btn_edit', function () {
    var id_edit = $(this).attr("id");
    var activite = firestore.collection("activites").doc(id_edit);
    activite.get().then(function (doc) {
        let act = doc.data();
        $('#inputId').val(id_edit);
        $('#activite').val(act.libelle);
        $('#code_activite').val(act.code);
        $('#branche').val(act.branche.id);
        $('#branche').trigger('change');
    });
    clearInput();
});