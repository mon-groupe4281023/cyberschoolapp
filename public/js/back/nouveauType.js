//ici les operations CRUD pour les types de produits

//selection de tous les documents de la collection restaurant_collection
function getData()
{
    var query = firestore.collection("restaurant_collection");
    query.get().then(function (docData)
    {
        if (docData.size)
        {
            var arrObj = [];

            docData.forEach(function (data) {
                var obj = data.data();
                var id = data.id;
                obj.id = data.id;
                obj.btnAction = '<button type="button" id="'+id+'"  data-id="'+data.id+'" data-toggle="modal" data-target="#modalEditType" class="btn btn-primary btn_edit" name="btn_edit">Modifier</button>&nbsp<button type="button" id="'+id+'" class="btn btn-danger btn_delete">Supprimer</button>';
                arrObj.push(obj);

            });
            dtTable.clear();
            dtTable.rows.add(arrObj);
            dtTable.order([0, 'desc' ] ).draw();
        }
    }, function (error) {
        console.log("error =", error);
    });
}

function saveData()
{
    var ref = storage.ref();
    var designation = $("#designation").val();
    var description = $("#descr").val();
    var imageType = document.querySelector('#imageType').files[0];
    const imageName = (+new Date());
    var message = '';
    const metadata = {
        contentType: imageType.type
    };
    const task = ref.child('categories/'+imageName).put(imageType, metadata);
    task
        .then(snapshot => snapshot.ref.getDownloadURL())
        .then((url) => {
            var data = {
                title: designation,
                description: description,
                imageName: imageName,
                image: url
            };
            addfirestore(data);
        })
        .catch((error) => {
            // A full list of error codes is available at
            switch (error.code) {
                case 'storage/unauthorized':
                    message = 'Vous n\'avez pas les authorisations requises';
                    break;
                case 'storage/canceled':
                    message = 'Le fichier a été supprimé';
                    break;
                case 'storage/unknown':
                    message = 'Erreur inattendue est survenue. Veillez réessayer';
                    break;
            }
        });

}

// Enregistrement des données
function addfirestore(obj)
{
    firestore.collection("restaurant_collection").
    doc().set(obj)
        .then(function () {
            getData();
            new PNotify({
                title: 'Enregistrement réussi',
                text: "Vous avez enregistré un nouveau type de produit",
                type: "success",
                icon: false
            });
        })
        .catch(function (error)
        {
            new PNotify({
                title: 'Erreur',
                text: "L'enregistrement d'un nouveau type de produit a échoué",
                type: "danger",
                icon: false
            });
        });
}

function updatefirestore(obj, id)
{
    firestore.collection("restaurant_collection").doc(id).update(obj).then(function ()
    {
        getData();
        new PNotify({
            title: 'Modification réussie',
            text: "Vous avez modifié avec succès un type de produit",
            type: "success",
            animation: "bounceInUp",
            icon: false
        });
    });
    $("#modalEditType").modal('toggle');
}

function deletefirestore(id) {
    firestore.collection("restaurant_collection").doc(id).delete()
        .then(function () {
            getData();
            /*new PNotify({
                title: 'Suppression réussie',
                text: "Vous avez supprimé un type de produit",
                type: "danger",
                animation: "bounceInUp",
                icon: false
            });*/
            new PNotify({
                title: 'Confirmation Needed',
                text: 'Are you sure?',
                icon: 'fa fa-question-circle',
                hide: false,
                modules: {
                    Confirm: {
                        confirm: true
                    },
                    Buttons: {
                        closer: true,
                        sticker: true
                    },
                    History: {
                        history: false
                    },
                }
            }).on('pnotify.confirm', function() {
                alert('Ok, cool.');
            }).on('pnotify.cancel', function() {
                alert('Oh ok. Chicken, I see.');
            });

        });
}

function clearInput() {
    $("#designation").val('');
    $("#descr").val('');
    $("#imageType").val('');
}

//réinitialiser les champs input de la modal de modification
function clearInputM() {
    $("#idType").val('');
    $("#designationM").val('');
    $("#descrM").val('');
    $("#imgLoaded").attr('src','');
}

$(document).ready(function () {
    dtTable = $('#example').
    DataTable({
        columns:
        [
            { data: "title" },
            { data: "description" },
            { data: "btnAction" }
        ]
    });

    $("#btnSaveType").on('click', function () {
        saveData();
        clearInput();
    });

    initFirebase();
    getData();
});

$(document).on('click', '.btn_delete', function () {
    var id = $(this).attr("id");
    deletefirestore(id);
    initFirebase();
    getData();
});

//A l'affichage du modal de modification des catégories
$(document).on('show.bs.modal', '#modalEditType', function (event) {
    var button = $(event.relatedTarget); // bouton sur lequel a cliqué l'utilisateur
    var id = button.data('id'); // on récupère l'id du type
    clearInputM(); //on réinitialise les champs input
    var categorie = firestore.collection("restaurant_collection").doc(id);
    categorie.get().then(function (docData){
        var donnees = docData.data();
        $("#idType").val(id);
        $("#designationM").val(donnees.title);
        $("#descrM").val(donnees.description);
        $("#name").val(donnees.imageName);
        $("#imgLoaded").attr('src',donnees.image);
    });
});

$(document).on('click', '#BtnModalEditType', function () {
    var ref = storage.ref();//reference to storage cloud
    var id_edit = $("#idType").val();
    var title = $("#designationM").val();
    var descr = $("#descrM").val();
    var nameImg = $("#name").val();
    var data = {};
    var imageTypeM = document.querySelector('#imageTypeM').files[0];

    //on verifie si l'image n'a pas été changée
    if (imageTypeM === undefined)
    {
        var urlM = $("#imgLoaded").attr('src');
        data = {
            title: title,
            description: descr
        };
        updatefirestore(data,id_edit);
    }
    else {
        const imageName = (+new Date()); //new name of the image
        var message = '';
        const metadata = {
            contentType: imageTypeM.type
        };
        var refToDelete = ref.child('restaurant_collection/'+nameImg); //on supprime l'ancienne image
        refToDelete.delete().then(function () {
            const task = ref.child('restaurant_collection/'+imageName).put(imageTypeM, metadata);
            task.then(snapshot => snapshot.ref.getDownloadURL()).then((url) => {
                var urlM = url;
                data = {
                    title: title,
                    imageName: imageName,
                    description: descr,
                    image: urlM
                };
                updatefirestore(data,id_edit);
            }).catch((error) => {
                // A full list of error codes is available at
                switch (error.code) {
                    case 'storage/unauthorized':
                        message = 'Vous n\'avez pas les authorisations requises';
                        break;
                    case 'storage/canceled':
                        message = 'Le fichier a été supprimé';
                        break;
                    case 'storage/unknown':
                        message = 'Erreur inattendue est survenue. Veillez réessayer';
                        break;
                }
            });
        });

    }
});