//ici les operations CRUD pour les produits

//selection de toutes les zones de livraison
function getZonesLivraison()
{
    var select = document.getElementById('zoneLivr');
    var query = firestore.collection("zone_collection");
    query.get().then(function (docData)
    {
        if (docData.size)
        {
            var arrObj = [];

            docData.forEach(function (data) {
                var obj = data.data();
                select.options[select.options.length] = new Option(obj.title, data.id);
            });
        }
    });
}

//selectionne la zone de livraison d'une entreprise
function getEntreZonesLivraison()
{
    var select = document.getElementById('zoneLivrM');
    var query = firestore.collection("zone_collection");
    query.get().then(function (docData)
    {
        if (docData.size)
        {
            var arrObj = [];

            docData.forEach(function (data) {
                var obj = data.data();
                select.options[select.options.length] = new Option(obj.title, data.id);
            });
        }
    });
}

//liste toutes les entreprises enregistrées
function getData()
{
    var zones = {};
    var entreprises = {};

    firestore.collection('zone_collection').get().then((results) => {
        results.forEach((doc) => {
            zones[doc.id] = doc.data();
        });
        entr = firestore.collection('entreprise');
        entr.get().then((docSnaps) => {
            var arrObj = [];
            docSnaps.forEach((doc) => {
                entreprises = doc.data();
                var id = entreprises.collection.id;
                entreprises.libelleZone = zones[id].title;
                entreprises.btnAction = '<button type="button" id="'+doc.id+'"  data-id="'+doc.id+'" data-toggle="modal" data-target="#modalEditEntr" class="btn btn-primary btn_edit" name="btn_edit">Modifier</button>&nbsp<button type="button" id="'+doc.id+'" class="btn btn-danger btn_delete">Supprimer</button>';
                arrObj.push(entreprises);
        });
            dtTable.clear();
            dtTable.rows.add(arrObj);
            dtTable.order([0, 'desc' ] ).draw();
        });
    });

}

function saveData()
{
    var ref = storage.ref();
    var libelle = $("#libelle").val();
    var tel = $("#tel").val();
    var zone_collection = $("#zoneLivr").val();
    var latitude = $("#latitude").val();
    var longitude = $("#longitude").val();
    var description = $("#descr").val();
    var imageEntr = document.querySelector('#imageEntr').files[0];
    const imageName = (+new Date());
    var message = '';
    const metadata = {
        contentType: imageEntr.type
    };
    const task = ref.child('entreprise_collection/'+imageName).put(imageEntr, metadata);
    task
        .then(snapshot => snapshot.ref.getDownloadURL())
        .then((url) => {
            var data = {
                title: libelle,
                tel: tel,
                latitude: latitude,
                longitude: longitude,
                description: description,
                image: {
                    name: imageName,
                    url: url
                }
            };
            data.collection = firestore.collection('zone_collection').doc(zone_collection);
            addfirestore(data);
        })
        .catch((error) => {
            // A full list of error codes is available at
            switch (error.code) {
                case 'storage/unauthorized':
                    message = 'Vous n\'avez pas les authorisations requises';
                    break;
                case 'storage/canceled':
                    message = 'Le fichier a été supprimé';
                    break;
                case 'storage/unknown':
                    message = 'Erreur inattendue est survenue. Veillez réessayer';
                    break;
            }
        });
}

// Enregistrement des données
function addfirestore(obj)
{
    firestore.collection("entreprise").
    doc().set(obj)
        .then(function () {
            getData();
            new PNotify({
                title: 'Enregistrement réussi',
                text: "Vous avez enregistré une nouvelle entreprise",
                type: "success",
                icon: false
            });
        })
        .catch(function (error)
        {
            new PNotify({
                title: 'Erreur',
                text: "L'enregistrement d'une nouvelle entreprise a échoué",
                type: "danger",
                icon: false
            });
        });
}

function updatefirestore(obj, id)
{
    firestore.collection("entreprise").doc(id).update(obj).then(function ()
    {
        getData();
        new PNotify({
            title: 'Modification réussie',
            text: "Vous avez modifié avec succès une entreprise",
            type: "success",
            animation: "bounceInUp",
            icon: false
        });
    });
    $("#modalEditEntr").modal('toggle');
}

function deletefirestore(id) {
    firestore.collection("entreprise").doc(id).delete()
        .then(function () {
            getData();
            new PNotify({
                title: 'Suppression réussie',
                text: "Vous avez supprimé une entreprise",
                type: "danger",
                animation: "bounceInUp",
                icon: false
            });

        });
}

//R2INITIALISER LA VALEUR DES INPUT
function clearInput() {
    $("#libelle").val('');
    $("#tel").val('');
    $("#zoneLivr").val('');
    $("#latitude").val('');
    $("#longitude").val('');
    $("#descr").val('');
    $("#imageEntr").val('');
}

//réinitialiser les champs input de la modal de modification
function clearInputM() {
    $("#idEntr").val('');
    $("#libelleM").val('');
    $("#telM").val('');
    $("#zoneLivrM").val('');
    $("#latitudeM").val('');
    $("#longitudeM").val('');
    $("#descrM").val('');
    $("#imgLoaded").attr('src','');
}

$(document).ready(function () {
    dtTable = $('#example').
    DataTable({
        columns:
        [
            { data: "title" },
            { data: "libelleZone" },
            { data: "tel" },
            { data: "btnAction" }
        ]
    });

    $("#btnSaveEnt").on('click', function () {
        saveData();
        clearInput();
    });

    initFirebase();
    getZonesLivraison();
    getEntreZonesLivraison();
    getData();
});

$(document).on('click', '.btn_delete', function () {
    var id = $(this).attr("id");
    deletefirestore(id);
    initFirebase();
    getData();
});

//Affichage du modal de modification modalBtnEditEntreprise
$(document).on('show.bs.modal', '#modalEditEntr', function (event) {
    var button = $(event.relatedTarget); // bouton sur lequel a cliqué l'utilisateur
    var id = button.data('id'); // on récupère l'id de l'entreprise
    var mySelect = document.getElementById('zoneLivrM');
    clearInputM(); //on réinitialise les champs input
    var entreprise = firestore.collection("entreprise").doc(id);
    entreprise.get().then(function (docData){
        var donnees = docData.data();
        var zoneCollectionId = donnees.collection.id;//recuperation de l'id de la zone
        $("#idEntr").val(id);
        $("#libelleM").val(donnees.title);
        $("#telM").val(donnees.tel);
        $("#zoneLivrM").val(zoneCollectionId);
        $("#latitudeM").val(donnees.latitude);
        $("#longitudeM").val(donnees.longitude);
        $("#descrM").val(donnees.description);
        $("#name").val(donnees.image.name);
        $("#imgLoaded").attr('src',donnees.image.url);
    });
});

$(document).on('click', '#modalBtnEditEntreprise', function () {
    var ref = storage.ref();//reference to storage cloud
    var id_edit = $('#idEntr').val();//id de l'entreprise à modifier
    var title = $("#libelleM").val();
    var tel = $("#telM").val();
    var collection = $("#zoneLivrM").val();
    var latitude = $("#latitudeM").val();
    var longitude = $("#longitudeM").val();
    var descr = $("#descrM").val();
    var nameImg = $("#name").val();
    var data;
    var imageEntr = document.querySelector('#imageEntrM').files[0];

    //on verifie si l'image n'a pas été changée
    if (imageEntr === undefined)
    {
        var urlM = $("#imgLoaded").attr('src');
        data = {
            title: title,
            tel: tel,
            latitude: latitude,
            longitude: longitude,
            description: descr
            // image: urlM
        };
        data.collection = firestore.collection('zone_collection').doc(collection);
        //si l'ancienne image n'a été changé, on enregistre
        updatefirestore(data,id_edit);
    }
    else {
        const imageName = (+new Date()); //new name of the image
        var message = '';
        const metadata = {
            contentType: imageEntr.type
        };
        var refToDelete = ref.child('entreprise_collection/'+nameImg); //on supprime l'ancienne image
        refToDelete.delete().then(function () {
            const task = ref.child('entreprise_collection/'+imageName).put(imageEntr, metadata);
            task.then(snapshot => snapshot.ref.getDownloadURL()).then((url) => {
                var urlM = url;
                data = {
                    title: title,
                    tel: tel,
                    latitude: latitude,
                    longitude: longitude,
                    description: descr,
                    image: {
                        name: imageName,
                        url: urlM
                    }
                };
                data.collection = firestore.collection('zone_collection').doc(collection);
                updatefirestore(data,id_edit);
            }).catch((error) => {
                // A full list of error codes is available at
                switch (error.code) {
                    case 'storage/unauthorized':
                        message = 'Vous n\'avez pas les authorisations requises';
                        break;
                    case 'storage/canceled':
                        message = 'Le fichier a été supprimé';
                        break;
                    case 'storage/unknown':
                        message = 'Erreur inattendue est survenue. Veillez réessayer';
                        break;
                }
            });
        });

    }
});