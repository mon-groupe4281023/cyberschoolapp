
$(document).ready(function () {
    $('#formLogin').submit(function (e) {
        e.preventDefault();
        var email = $('#email').val();
        var password = $('#password').val();
        let passTxt = document.getElementById('passwordError');

        // Sign in with email and pass.
        // [START authwithemail]
        auth.signInWithEmailAndPassword(email, password).then(function (data)
        {
            var uid = data.user.uid;
            var userInfo = firestore.collection('users').doc(uid);
            userInfo.get().then((doc) => {
                var userDB = doc.data();
            var userSession;
            var currentUser = auth.currentUser;
            if (userDB.profile.id == 1) {
                if (userDB.firstTimeConnection === "") {
                    currentUser.updateProfile({
                        displayName: userDB.nom+' '+userDB.prenom,
                        photoURL:    userDB.photo,
                        phoneNumber:    userDB.tel,
                    });
                    userDB.firstTimeConnection = firebase.firestore.FieldValue.serverTimestamp();
                    firestore.collection('users').doc(uid).update(userDB).then(function () {
                        currentUser.updatePhoneNumber({
                            phoneNumber:    userDB.tel,
                        });
                    });
                    userSession = {
                        displayNameAdmin : currentUser.displayName,
                        emailAdmin : currentUser.email,
                        phoneNumberAdmin : currentUser.phoneNumber,
                        photoURLAdmin : currentUser.photoURL,
                        entrepriseAdmin : userDB.entreprise,
                        uidAdmin : currentUser.uid
                    };
                } else {
                    userSession = {
                        displayNameAdmin : currentUser.displayName,
                        emailAdmin : currentUser.email,
                        phoneNumberAdmin : currentUser.phoneNumber,
                        photoURLAdmin : currentUser.photoURL,
                        uidAdmin : currentUser.uid,
                    };
                }
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'POST', // Type of response and matches what we said in the route
                    url: '/admin/login', // This is the url we gave in the route
                    data: {
                        'userSession' : userSession,
                    },
                    success: function(response) {
                        window.location.href = "/admin/accueil";
                    }
                });
            } else {
                window.location.href = "/";
            }
        });
        }).catch(function (error)
        {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = '';
            // [START_EXCLUDE]
            switch (errorCode)
            {
                case 'auth/wrong-password' :
                    errorMessage = 'Mot de passe incorrecte';
                    break;

                case 'auth/invalid-email' :
                    errorMessage = 'Mail invalide';
                    break;

                case 'auth/user-disabled' :
                    errorMessage = 'Compte supprimé ou desactivé. Contactez l\'administrateur';
                    break;

                case 'auth/user-not-found' :
                    errorMessage = 'Aucun compte lié à ce mail';
                    break;
                default:
                    errorMessage = 'Mot de passe ou mail incorrect';

            }
            $('#loginError').text(errorMessage);
            $('#alertLogin').fadeIn().fadeOut(5000);
            // console.log(errorCode);
        });
    });
    initFirebase();
});

$(document).on('click', '.btn_delete', function () {
    initFirebase();
});