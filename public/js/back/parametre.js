//ici les operations CRUD pour les paramètre

//selection de tous les documents de la collection paramètre
function getData()
{
    var query = firestore.collection("parametre");
    query.orderBy("title","desc").get().then(function (docData)
    {
        if (docData.size)
        {
            var arrObj = [];

            docData.forEach(function (data) {
                var obj = data.data();
                var id = data.id;
                obj.id = data.id;
                obj.btnAction = 
                    '<button type="button" id="' + id +'" class="btn btn-primary btn_edit" name="btn_edit"><i class="fa fa-pencil"></i></button>&nbsp&nbsp'+
                '<button type="button" id="' + id +'" class="btn btn-danger btn_delete"><i class="fa fa-trash"></i></button>&nbsp;&nbsp'+
                '<a href="/admin/concours/criteres/' + id +'" id="'+id+'" class="btn btn-success btn_critere"><i class="fa fa-plus"></i></button>';
                arrObj.push(obj);
                console.log(arrObj);
            });
            dtTable.clear();
            dtTable.rows.add(arrObj);
            dtTable.order([0, 'desc' ] ).draw();
        }
    }, function (error) {
        console.log("error =", error);
    });
}

function saveData()
{
    var id = $("#inputId").val();
    var libelle = $("#libelle").val();
    var dateDeb = $("#dateDebut").val();
    var dateFin = $("#dateFin").val();
    var description = $("#descr").val();
    var data = {
        title: libelle,
        dateDebut: dateDeb,
        dateFin: dateFin,
        description: description,
    };
    if (id === "") {
        addfirestore(data);
    } else {
        updatefirestore(data, id);
    }

}

// Enregistrement des données
function addfirestore(obj)
{
    firestore.collection("parametre").
    doc().set(obj)
        .then(function () {
            getData();
            new PNotify({
                title: 'Enregistrement réussi',
                text: "Vous avez paramétré le concours avec succès",
                type: "success",
                icon: false
            });
        })
        .catch(function (error)
        {
            new PNotify({
                title: 'Erreur',
                text: "Le paramétrage du concours a échoué",
                type: "danger",
                icon: false
            });
        });
}

function updatefirestore(obj, id)
{
    firestore.collection("parametre").doc(id).update(obj).then(function ()
    {
        getData();
        new PNotify({
            title: 'Modification réussie',
            text: "Vous avez modifié avec succès un paramètre",
            type: "success",
            animation: "bounceInUp",
            icon: false
        });
    });
    // console.log(id);
}

//function pour ajouter les critères d'un param
function addCriteres(obj, id)
{
    // firestore.collection("parametre").doc(id).update(obj).then(function ()
    // {
    //     getData();
    //     new PNotify({
    //         title: 'Modification réussie',
    //         text: "Vous avez modifié avec succès un paramètre",
    //         type: "success",
    //         animation: "bounceInUp",
    //         icon: false
    //     });
    // });
    console.log(id);
}

function deletefirestore(id) {
    firestore.collection("parametre").doc(id).delete()
        .then(function () {
            getData();
            new PNotify({
                title: 'Suppression réussie',
                text: "Vous avez supprimé un paramètre",
                type: "danger",
                animation: "bounceInUp",
                icon: false
            });
            // confirm();
        });
}

function clearInput() {
    $("#libelle").val('');
    $("#dateDeb").val('');
    $("#DateFin").val('');
    $("#descr").val('');
    $("#inputId").val('');
}

$(document).ready(function () {
    dtTable = $('#example').
    DataTable({
        columns:
            [
                { data: "title" },
                { data: "dateDebut" },
                { data: "dateFin" },
                { data: "btnAction" }
            ]
    });

    $("#btnSaveParam").on('click', function () {
        saveData();
        clearInput();
        // console.log('oooo');
    });

    $("#formCritere").on('submit', function() {

    });

    initFirebase();
    getData();
});

$(document).on('click', '.btn_delete', function () {
    var id = $(this).attr("id");
    deletefirestore(id);
    initFirebase();
    getData();
});

$(document).on('click', '.btn_edit', function () {
    var id_edit = $(this).attr("id");
    var zone_collection = firestore.collection("parametre").doc(id_edit);
    zone_collection.get().then(function (doc)
    {
        var zone = doc.data();
        $('#inputId').val(id_edit);
        $('#libelle').val(zone.title);
        $('#dateDebut').val(zone.dateDebut);
        $('#dateFin').val(zone.dateFin);
        $('#descr').val(zone.description);
    });
});