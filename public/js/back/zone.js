//ici les operations CRUD pour les zones

//selection de tous les documents de la collection zone_collection
function getData()
{
    var query = firestore.collection("zone_collection");
    query.orderBy("title","desc").get().then(function (docData)
    {
        if (docData.size)
        {
            var arrObj = [];

            docData.forEach(function (data) {
                var obj = data.data();
                var id = data.id;
                obj.id = data.id;
                obj.btnAction = '<button type="button" id="'+id+'" class="btn btn-primary btn_edit" name="btn_edit">Modifier</button>&nbsp<button type="button" id="'+id+'" class="btn btn-danger btn_delete">Supprimer</button>';
                arrObj.push(obj);
                console.log(arrObj);
            });
            dtTable.clear();
            dtTable.rows.add(arrObj);
            dtTable.order([0, 'desc' ] ).draw();
        }
    }, function (error) {
        console.log("error =", error);
    });
}

function saveData()
{
    var id = $("#inputId").val();
    var libelle = $("#libelle").val();
    var description = $("#descr").val();
    var data = {
        title: libelle,
        description: description,
    };
    if (id === "") {
        addfirestore(data);
    } else {
        updatefirestore(data, id);
    }

}

// Enregistrement des données
function addfirestore(obj)
{
    firestore.collection("zone_collection").
    doc().set(obj)
        .then(function () {
            getData();
            new PNotify({
                title: 'Enregistrement réussi',
                text: "Vous avez enregistré un nouveau type de produit",
                type: "success",
                icon: false
            });
        })
        .catch(function (error)
        {
            new PNotify({
                title: 'Erreur',
                text: "L'enregistrement d'un nouveau type de produit a échoué",
                type: "danger",
                icon: false
            });
        });
}

function updatefirestore(obj, id)
{
    firestore.collection("zone_collection").doc(id).update(obj).then(function ()
    {
        getData();
        new PNotify({
            title: 'Modification réussie',
            text: "Vous avez modifié avec succès une zone",
            type: "success",
            animation: "bounceInUp",
            icon: false
        });
    });
    // console.log(id);
}

function deletefirestore(id) {
    firestore.collection("zone_collection").doc(id).delete()
        .then(function () {
            getData();
            new PNotify({
                title: 'Suppression réussie',
                text: "Vous avez supprimé un type de produit",
                type: "danger",
                animation: "bounceInUp",
                icon: false
            });
            // confirm();
        });
}

function clearInput() {
    $("#libelle").val('');
    $("#descr").val('');
    $("#inputId").val('');
}

$(document).ready(function () {
    dtTable = $('#example').
    DataTable({
        columns:
            [
                { data: "title" },
                { data: "description" },
                { data: "btnAction" }
            ]
    });

    $("#btnSaveZone").on('click', function () {
        saveData();
        clearInput();
    });

    initFirebase();
    getData();
});

$(document).on('click', '.btn_delete', function () {
    var id = $(this).attr("id");
    deletefirestore(id);
    initFirebase();
    getData();
});

$(document).on('click', '.btn_edit', function () {
    var id_edit = $(this).attr("id");
    var zone_collection = firestore.collection("zone_collection").doc(id_edit);
    zone_collection.get().then(function (doc)
    {
        var zone = doc.data();
        $('#inputId').val(id_edit);
        $('#libelle').val(zone.title);
        $('#descr').val(zone.description);
    });
});