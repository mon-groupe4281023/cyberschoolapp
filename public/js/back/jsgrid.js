
//selection de toutes les évaluateurs
function getEvaluateurs()
{
    var select = document.getElementById('evaluateur');
    var query = firestore.collection("users").where('profile', '==', firebase.firestore().collection('profile').doc('2'));
    query.get().then(function (users)
    {
        if (users.size)
        {
            var arrObj = [];

            users.forEach(function (user) {
                var obj = user.data();
                select.options[select.options.length] = new Option(obj.nom+' '+obj.prenom, user.id);
            });
        }
    });
}

//liste tous les Projets
function getData()
{
    var query = firebase.firestore().collection("summary");
    var arrObj = [];

    query.get().then(function (docData)
    {
        docData.forEach(function (data) {
            var obj = {};
            obj.Projet = data.data().titre;
            obj.Age = data.data().porteur.age;
            obj.Sexe = data.data().porteur.sexe;
            obj.id = data.id;
            arrObj.push(obj);
        });
    }, function (error) {
        console.log("error =", error);
    })
    return arrObj;
}

function getDataJgrid() {
    var summary = {};

    firebase.firestore().collection('summary').get().then((results) => {
        results.forEach((doc) => {
            evaluation[doc.id] = doc.data();
            console.log(evaluation);
        });
        sum = firebase.firestore().collection('summary');
        var arrObj = [];
        sum.get().then((docSnaps) => {
            docSnaps.forEach((doc) => {
                var projet = {};
                var id = doc.id;
                projet.Note = evaluation[summary.id].note_global;
                projet.Projet = doc.data().titre;
                projet.Age = doc.data().porteur.age;
                projet.Sexe = doc.data().porteur.sexe;
                projet.id = id;
                arrObj.push(projet);
                console.log(id);
            });
            console.log(arrObj);
        });
        return arrObj;
    });

}

function updatefirestore(obj, id) {
    firestore.collection("summary").doc(id).update(obj).then(function () {
        getData();
        new PNotify({
            title: 'Assignation réussie',
            text: "Vous avez assigné un évaluateur au Projet",
            type: "success",
            animation: "bounceInUp",
            icon: false
        });
    });
    $("#modalBtnAssignEval").modal('toggle');
}

function deletefirestore(id) {
    firestore.collection("restaurant").doc(id).delete()
        .then(function () {
            getData();
            new PNotify({
                title: 'Suppression réussie',
                text: "Vous avez supprimé une entreprise",
                type: "danger",
                animation: "bounceInUp",
                icon: false
            });

        });
}

//effacer le contenu des input
function clearInput() {
    $("#code").val('');
    $("#designation").val('');
    $("#prix").val('');
    $("#calorie").val('');
    $("#descr").val('');
    $("#imageDefault").attr('src', "<?php echo asset('images/B3.jpg');?>");
}

//effacer le contenu des inputs du modal
function clearInputM() {
    $("#codeM").val('');
    $("#designationM").val('');
    $("#prixM").val('');
    $("#calorieM").val('');
    $("#descrM").val('');
    $("#imageDefault").attr('src', "");
}

//connection de jsgrid à la db
(function () {

    var db = {


        loadData: function (filter) {
            return $.grep(this.clients, function (client) {
                return (!filter.Projet || client.Projet.indexOf(filter.Projet) > -1)
                    && (!filter.Age || client.Age === filter.Age)
                    && (!filter.Note || client.Note === filter.Note)
                    && (!filter.Sexe || client.Sexe === filter.Sexe);
            });
        },

        insertItem: function (insertingClient) {
            this.clients.push(insertingClient);
        },

        updateItem: function (updatingClient) { },

        deleteItem: function (deletingClient) {
            var clientIndex = $.inArray(deletingClient, this.clients);
            this.clients.splice(clientIndex, 1);
        }

    };

    window.db = db;


    db.sexe = [
        { Name: "" },
        { Name: "Masculin"},
        { Name: "Féminin" }
    ];
    db.clients = getData();
    console.log(db.clients);
    console.log(getDataJgrid());

    /* db.clients = [
        {
            "Projet": "Otto Clay",
            "Age": 61,
            "Sexe": 1,
            "Note": "15"
        },

        {
            "Projet": "Carter Clarke",
            "Age": 59,
            "Sexe": 2,
            "Note": "12"
        }
    ]; */

}());

$(document).ready(function () {
    $("#btnSaveType").on('click', function () {
        saveData();
        clearInput();
    });

    initFirebase();
    getData();
});

//paramétrage de jsgrid
! function (document, window, $) {
    "use strict";
    var Site = window.Site;
    $(document).ready(function ($) {

    }), jsGrid.setDefaults({
        tableClass: "jsgrid-table table table-striped table-hover"
    }), jsGrid.setDefaults("text", {
        _createTextBox: function () {
            return $("<input>").attr("type", "text").attr("class", "form-control input-sm")
        }
    }), jsGrid.setDefaults("number", {
        _createTextBox: function () {
            return $("<input>").attr("type", "number").attr("class", "form-control input-sm")
        }
    }), jsGrid.setDefaults("textarea", {
        _createTextBox: function () {
            return $("<input>").attr("type", "textarea").attr("class", "form-control")
        }
    }), jsGrid.setDefaults("control", {
        _createGridButton: function (cls, tooltip, clickHandler) {
            var grid = this._grid;
            return $("<button>").addClass(this.buttonClass).addClass(cls).attr({
                type: "button",
                title: tooltip
            }).on("click", function (e) {
                clickHandler(grid, e)
            })
        }
    }), jsGrid.setDefaults("select", {
        _createSelect: function () {
            var $result = $("<select>").attr("class", "form-control input-sm"),
                valueField = this.valueField,
                textField = this.textField,
                selectedIndex = this.selectedIndex;
            return $.each(this.items, function (index, item) {
                var value = valueField ? item[valueField] : index,
                    text = textField ? item[textField] : item,
                    $option = $("<option>").attr("value", value).text(text).appendTo($result);
                $option.prop("selected", selectedIndex === index)
            }), $result
        }
    }),
        function () {
            $("#basicgrid").jsGrid({
                height: "500px",
                width: "100%",
                filtering: !0,
                editing: !0,
                sorting: !0,
                paging: !0,
                autoload: !0,
                pageSize: 15,
                pageButtonCount: 5,
                deleteConfirm: "Do you really want to delete the client?",
                controller: db,
                fields: [{
                    name: "Projet",
                    type: "text",
                    width: 150
                }, {
                    name: "Age",
                    type: "number",
                    width: 70
                }, {
                    name: "Note",
                    type: "number",
                    width: 70
                }, {
                    name: "Sexe",
                    type: "select",
                    items: db.sexe,
                    valueField: "Name",
                    textField: "Name"
                }, {
                    type: "control"
                }]
            })
        }(),
        
        function () {
            $("#exampleSorting").jsGrid({
                height: "500px",
                width: "100%",
                autoload: !0,
                selecting: !1,
                controller: db,
                fields: [{
                    name: "Name",
                    type: "text",
                    width: 150
                }, {
                    name: "Age porteur",
                    type: "number",
                    width: 50
                }, {
                    name: "Note",
                    type: "text",
                    width: 200
                }, {
                    name: "Sexe",
                    type: "select",
                    items: db.sexe,
                    valueField: "Name",
                    textField: "Name"
                }]
            }), $("#sortingField").on("change", function () {
                var field = $(this).val();
                $("#exampleSorting").jsGrid("sort", field)
            })
        }(),

        function () {
            var MyDateField = function (config) {
                jsGrid.Field.call(this, config)
            };
            MyDateField.prototype = new jsGrid.Field({
                sorter: function (date1, date2) {
                    return new Date(date1) - new Date(date2)
                },
                itemTemplate: function (value) {
                    return new Date(value).toDateString()
                },
                insertTemplate: function () {
                    if (!this.inserting) return "";
                    var $result = this.insertControl = this._createTextBox();
                    return $result
                },
                editTemplate: function (value) {
                    if (!this.editing) return this.itemTemplate(value);
                    var $result = this.editControl = this._createTextBox();
                    return $result.val(value), $result
                },
                insertValue: function () {
                    return this.insertControl.datepicker("getDate")
                },
                editValue: function () {
                    return this.editControl.datepicker("getDate")
                },
                _createTextBox: function () {
                    return $("<input>").attr("type", "text").addClass("form-control input-sm").datepicker({
                        autoclose: !0
                    })
                }
            }), jsGrid.fields.myDateField = MyDateField, $("#exampleCustomGridField").jsGrid({
                height: "500px",
                width: "100%",
                inserting: !0,
                editing: !0,
                sorting: !0,
                paging: !0,
                data: db.users,
                fields: [{
                    name: "Account",
                    width: 150,
                    align: "center"
                }, {
                    name: "Name",
                    type: "text"
                }, {
                    name: "RegisterDate",
                    type: "myDateField",
                    width: 100,
                    align: "center"
                }, {
                    type: "control",
                    editButton: !1,
                    modeSwitchButton: !1
                }]
            })
        }()
}(document, window, jQuery);

$(document).on('show.bs.modal', '#modalEditProjet', function (event) {
    var button = $(event.relatedTarget); // bouton sur lequel a cliqué l'utilisateur
    var id = button.data('id');
    var mySelect = document.getElementById('evaluateur');
    var Projets = firestore.collection("summary").doc(id);
    clearInputM(); //on réinitialise les champs input
    getEvaluateurs();
    Projets.get().then(function (Projet){
        var donnees = Projet.data();
        $("#id").val(id);
        $("#numero").val(donnees.identifiant);
        $("#Projet").val(donnees.Projet);
        $("#formJuridique").val(donnees.formJuridique);
    });
});

$(document).on('click', '#modalBtnAssignEval', function () {
    var id = $('#id').val();
    var identifiant = $('#numero').val();
    var Projet = $("#Projet").val();
    var formJuridique = $("#formjuridique").val();
    var eval = $("#evaluateur").val();
    console.log(eval);
    var data = {
        eval : eval
    };
    updatefirestore(data,id);
});