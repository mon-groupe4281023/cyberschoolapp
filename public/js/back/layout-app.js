
// Initialize Firebase
var config = {
    apiKey: "AIzaSyAtLbJIn6Rk7xXdj81bIyBCc-KoBt3y9Hs",
    authDomain: "cyberschool-ab785.firebaseapp.com",
    databaseURL: "https://cyberschool-ab785.firebaseio.com",
    projectId: "cyberschool-ab785",
    storageBucket: "cyberschool-ab785.appspot.com",
    messagingSenderId: "107562228204"
};
firebase.initializeApp(config);

var firestore;
var auth;
var storage;
var dtTable;

function initFirebase() {
    auth = firebase.auth();
    storage = firebase.storage();
    firestore = firebase.firestore();
}