//ici les operations CRUD pour les branches

//selection de tous les documents de la collection branches
function getData() {
    var query = firestore.collection("branches");
    query.orderBy("libelle", "desc").get().then(function (docData) {
        if (docData.size) {
            var arrObj = [];

            docData.forEach(function (data) {
                var obj = data.data();
                let secteur = '';
                switch(obj.secteur) {
                    case '1':
                    secteur = 'Secteur primaire'
                    break;
                    case '2':
                    secteur = 'Secteur secondaire'
                    break;
                    case '3':
                    secteur = 'Secteur tertiaire'
                    break;
                    case '4':
                    secteur = 'Secteur quaternaire'
                    break;
                }
                var id = data.id;
                obj.id = data.id;
                obj.sect = secteur;
                obj.btnAction = '<button type="button" id="' + id + '" class="btn btn-primary btn_edit" name="btn_edit">Modifier</button>&nbsp<button type="button" id="' + id + '" class="btn btn-danger btn_delete">Supprimer</button>';
                arrObj.push(obj);
            });
            dtTable.clear();
            dtTable.rows.add(arrObj);
            dtTable.order([0, 'desc']).draw();
        }
    }, function (error) {
        console.log("error =", error);
    });
}

function saveData() {
    var id = $("#inputId").val();
    var libelle = $("#branche").val();
    var code = $("#code_branche").val();
    var secteur = $("#secteur").val();
    var data = {
        libelle: libelle,
        code: code,
        secteur: secteur,
    };
    if (id === "") {
        addfirestore(data);
    } else {
        updatefirestore(data, id);
    }

}

// Enregistrement des données
function addfirestore(obj) {
    firestore.collection("branches").
        doc().set(obj)
        .then(function () {
            getData();
            new PNotify({
                title: 'Enregistrement réussi',
                text: "Vous avez enregistré une nouvelle branche",
                type: "success",
                icon: false
            });
        })
        .catch(function (error) {
            new PNotify({
                title: 'Erreur',
                text: "Enregistrement d'une nouvelle branche échoué",
                type: "danger",
                icon: false
            });
        });
}

function updatefirestore(obj, id) {
    firestore.collection("branches").doc(id).update(obj).then(function () {
        getData();
        new PNotify({
            title: 'Modification réussie',
            text: "Vous avez modifié avec succès une branche",
            type: "success",
            animation: "bounceInUp",
            icon: false
        });
    });
    // console.log(id);
}

function deletefirestore(id) {
    firestore.collection("branches").doc(id).delete()
        .then(function () {
            getData();
            new PNotify({
                title: 'Suppression réussie',
                text: "Vous avez supprimé une branche",
                type: "danger",
                animation: "bounceInUp",
                icon: false
            });
            // confirm();
        });
}

function clearInput() {
    $("#libelle").val('');
    $("#branche").val('');
    $("#code_branche").val('');
    $("#inputId").val('');
    $("#secteur").text('');
}

$(document).ready(function () {
    dtTable = $('#example').
        DataTable({
            columns:
                [
                    { data: "sect" },
                    { data: "libelle" },
                    { data: "code" },
                    { data: "btnAction" }
                ]
        });

    $("#btnSaveBranch").on('click', function () {
        saveData();
        clearInput();
    });

    initFirebase();
    getData();
});

$(document).on('click', '.btn_delete', function () {
    var id = $(this).attr("id");
    deletefirestore(id);
    initFirebase();
    getData();
});

$(document).on('click', '.btn_edit', function () {
    var id_edit = $(this).attr("id");
    var branche = firestore.collection("branches").doc(id_edit);
    branche.get().then(function (doc) {
        var brch = doc.data();
        $('#inputId').val(id_edit);
        $('#branche').val(brch.libelle);
        $('#code_branche').val(brch.code);
        $('#secteur').val(brch.secteur);
    });
});