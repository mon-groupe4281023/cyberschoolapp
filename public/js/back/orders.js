//ici les operations CRUD pour les produits

//selection de tous les documents de la collection restaurant_collection
function getTypeProduits()
{
    var select = document.getElementById('typeProduits');
    var query = firestore.collection("restaurant_collection");
    query.get().then(function (docData)
    {
        if (docData.size)
        {
            var arrObj = [];

            docData.forEach(function (data) {
                var obj = data.data();
                select.options[select.options.length] = new Option(obj.title, data.id);
            });
        }
    });
}

//selectionne la catégorie d'un produit
function getProductCateg()
{
    var select = document.getElementById('categorieM');
    var query = firestore.collection("restaurant_collection");
    query.get().then(function (docData)
    {
        if (docData.size)
        {
            var arrObj = [];

            docData.forEach(function (data) {
                var obj = data.data();
                select.options[select.options.length] = new Option(obj.title, data.id);
            });
        }
    });
}

//selection de tous les documents de la collection orders
function getData()
{
    var query = firestore.collection("orders");
    query.get().then(function (docData)
    {
        var arrObj = [];
        docData.forEach(function (data) {
            var obj = data.data();
            var id = data.id;
            obj.id = data.id;
            obj.btnAction = '<button type="button" id="'+id+'"  data-id="'+id+'" data-toggle="modal" data-target="#modalDetail"  class="btn btn-primary btn_edit" name="btn_edit">Détails</button>';
            arrObj.push(obj);
        });
        dtTable.clear();
        dtTable.rows.add(arrObj);
        dtTable.order([0, 'desc' ] ).draw();
    }, function (error) {
        console.log("error =", error);
    });
}

function saveData()
{
    var ref = storage.ref();
    var code = $("#code").val();
    var designation = $("#designation").val();
    var restaurant_collection = $("#typeProduits").val();
    var prix = $("#prix").val();
    var calorie = $("#calorie").val();
    var description = $("#descr").val();
    var imageProduit = document.querySelector('#imageProduit').files[0];
    const imageName = (+new Date());
    var message = '';
    const metadata = {
        contentType: imageProduit.type
    };
    const task = ref.child('produits/'+imageName).put(imageProduit, metadata);
    task
        .then(snapshot => snapshot.ref.getDownloadURL())
        .then((url) => {
            var data = {
                reference: code,
                title: designation,
                price: prix,
                calories: calorie,
                description: description,
                imageName: imageName,
                image: url,
                options: ''
            };
            data.collection = firestore.collection('restaurant_collection').doc(restaurant_collection);
            addfirestore(data);
        })
        .catch((error) => {
            // A full list of error codes is available at
            switch (error.code) {
                case 'storage/unauthorized':
                    message = 'Vous n\'avez pas les authorisations requises';
                    break;
                case 'storage/canceled':
                    message = 'Le fichier a été supprimé';
                    break;
                case 'storage/unknown':
                    message = 'Erreur inattendue est survenue. Veillez réessayer';
                    break;
            }
        });
}

// Enregistrement des données
function addfirestore(obj)
{
    firestore.collection("restaurant").doc().set(obj).then(function ()
    {
        getData();
        new PNotify({
            title: 'Enregistrement réussi',
            text: "Vous avez enregistré un nouveau produit",
            type: "success",
            icon: false
        });
    }).catch(function (error)
    {
        new PNotify({
            title: 'Erreur',
            text: "L'enregistrement d'un nouveau produit a échoué",
            type: "danger",
            icon: false
        });
    });
}

function updatefirestore(obj, id)
{
    firestore.collection("restaurant").doc(id).update(obj).then(function ()
    {
        getData();
        new PNotify({
            title: 'Modification réussie',
            text: "Vous avez modifié avec succès un produit",
            type: "success",
            animation: "bounceInUp",
            icon: false
        });
    });
    $("#modalEditPrdt").modal('toggle');
}

function deletefirestore(id) {
    firestore.collection("restaurant").doc(id).delete()
        .then(function () {
            getData();
            new PNotify({
                title: 'Suppression réussie',
                text: "Vous avez supprimé une entreprise",
                type: "danger",
                animation: "bounceInUp",
                icon: false
            });

        });
}

//effacer le contenu des input
function clearInput() {
    $("#code").val('');
    $("#designation").val('');
    $("#prix").val('');
    $("#calorie").val('');
    $("#descr").val('');
    $("#imageDefault").attr('src',"<?php echo asset('images/B3.jpg');?>");
}

//effacer le contenu des inputs du modal
function clearInputM() {
    $("#codeM").val('');
    $("#designationM").val('');
    $("#prixM").val('');
    $("#calorieM").val('');
    $("#descrM").val('');
    $("#imageDefault").attr('src',"");
}

$(document).ready(function () {
    dtTable = $('#example').
    DataTable({
        columns:
            [
                { data: "code_commande" },
                { data: "total" },
                { data: "livraison.phone" },
                { data: "livraison.zone" },
                { data: "livraison.entreprise" },
                { data: "etatpayement" },
                { data: "etatlivraison" },
                { data: "btnAction" }
            ]
    });

    initFirebase();
    getData();
});


//Affichage du modal de modification de produit
$(document).on('show.bs.modal', '#modalDetail', function (event) {
    var button = $(event.relatedTarget); // bouton sur lequel a cliqué l'utilisateur
    var id = button.data('id'); // on récupère l'id de la commande
    var ref = id.toString(); // on récupère l'id de la commande en string
    // clearInputM(); //on réinitialise les champs input
    var order = firestore.collection("orders").doc(ref);
    order.get().then(function (docData){
        var arrObj = [];
        var plats = docData.data().order;
        // var total = 0;
        plats.forEach(function (data) {
            data.total = data.price * data.quantity;
            console.log(data);
            arrObj.push(data);
        });
        $('#exampled').DataTable( {
            paging: false,
            destroy: true,
            data: arrObj,
            columns: [
                { data: "name" },
                { data: "price" },
                { data: "quantity" },
                { data: "total" }
            ]
        } );
    }, function (error) {
        console.log("error =", error);
    });
});