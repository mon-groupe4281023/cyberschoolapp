//ici les operations CRUD pour les zones

//selection de tous les documents de la collection zone_collection
function getData()
{
    var query = firestore.collection("faqs");
    query.orderBy("question","desc").get().then(function (docData)
    {
        if (docData.size)
        {
            var arrObj = [];

            docData.forEach(function (data) {
                var obj = data.data();
                var id = data.id;
                obj.id = data.id;
                obj.rep = obj.response.substring(0,125)+'...';
                obj.btnAction = '<button type="button" id="'+id+'" class="btn btn-primary btn_edit" name="btn_edit">Modifier</button>&nbsp<button type="button" id="'+id+'" class="btn btn-danger btn_delete">Supprimer</button>';
                arrObj.push(obj);
                console.log(obj);
            });
            dtTable.clear();
            dtTable.rows.add(arrObj);
            dtTable.order([0, 'desc' ] ).draw();
        }
    }, function (error) {
        console.log("error =", error);
    });
}

function saveData()
{
    var id = $("#faqId").val();
    var question = $("#question").val();
    var reponse = $("#rep").val();
    var data = {
        question: question,
        response: reponse,
    };
    if (id === "") {
        addfirestore(data);
    } else {
        updatefirestore(data, id);
    }

}

// Enregistrement des données
function addfirestore(obj)
{
    obj.createdDate = firebase.firestore.FieldValue.serverTimestamp();
    firestore.collection("faqs").
    doc().set(obj)
        .then(function () {
            getData();
            new PNotify({
                title: 'Enregistrement réussi',
                text: "Nouvel élément enregistré dans FAQ",
                type: "success",
                icon: false
            });
        })
        .catch(function (error)
        {
            new PNotify({
                title: 'Erreur',
                text: "Erreur d'enregistrement d'un nouvel élément FAQ",
                type: "danger",
                icon: false
            });
        });
}

function updatefirestore(obj, id)
{
    firestore.collection("faqs").doc(id).update(obj).then(function ()
    {
        getData();
        new PNotify({
            title: 'Modification réussie',
            text: "Vous avez modifié avec succès une zone",
            type: "success",
            animation: "bounceInUp",
            icon: false
        });
    });
    // console.log(id);
}

function deletefirestore(id) {
    firestore.collection("faqs").doc(id).delete()
        .then(function () {
            getData();
            new PNotify({
                title: 'Suppression réussie',
                text: "Un élément de FAQ a été supprimé",
                type: "danger",
                animation: "bounceInUp",
                icon: false
            });
            // confirm();
        });
}

function clearInput() {
    $("#question").val('');
    $("#rep").val('');
    $("#faqId").val('');
}

$(document).ready(function () {
    dtTable = $('#example').
    DataTable({
        columns:
            [
                { data: "question" },
                { data: "rep" },
                { data: "btnAction" }
            ]
    });

    $("#btnSaveFaq").on('click', function () {
        saveData();
        clearInput();
    });

    initFirebase();
    getData();
});

$(document).on('click', '.btn_delete', function () {
    var id = $(this).attr("id");
    deletefirestore(id);
    initFirebase();
    getData();
});

$(document).on('click', '.btn_edit', function () {
    var id_edit = $(this).attr("id");
    var faqs = firestore.collection("faqs").doc(id_edit);
    faqs.get().then(function (doc)
    {
        var faq = doc.data();
        $('#faqId').val(id_edit);
        $('#question').val(faq.question);
        $('#rep').val(faq.response);
    });
});