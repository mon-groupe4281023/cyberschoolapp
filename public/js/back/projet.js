
//selection de toutes les évaluateurs
function getEvaluateurs()
{
    
    var query = firestore.collection("users")
                .where('profile', '==', firebase.firestore().collection('profile')
                .doc('2'));
    query.get().then(function (users)
    {
        var select = document.getElementById('evaluateur');
        if (users.size)
        {
            var arrObj = [];

            users.forEach(function (user) {
                var obj = user.data();
                select.options[select.options.length] = new Option(obj.nom+' '+obj.prenom, user.id);
            });
        }
    });
}

//liste tous les projets
function getData()
{
    var query = firestore.collection("summary").where('eval', '==', '');
    query.get().then(function (docData)
    {
        var arrObj = [];

        docData.forEach(function (data) {
            var obj = data.data();
            var datein= new Date(data.data().createdAt);
            var id = data.id;
            obj.id = data.id;
            obj.dateformated=datein;
            obj.btnAction = '<button type="button" id="'+id+'"  data-id="'+id+'" data-toggle="modal" data-target="#modalEditProjet" class="btn btn-primary btn_edit" name="btn_edit">Attribuer</button>';
            arrObj.push(obj);
        });
        dtTable.clear();
        dtTable.rows.add(arrObj);
        dtTable.order([0, 'desc' ] ).draw();
    }, function (error) {
        console.log("error =", error);
    })
}

function updatefirestore(obj, id) {
    firestore.collection("summary").doc(id).update(obj).then(function () {
        getData();
        new PNotify({
            title: 'Assignation réussie',
            text: "Vous avez assigné un évaluateur au projet",
            type: "success",
            animation: "bounceInUp",
            icon: false
        });
        $("#modalEditProjet").modal('toggle');
    });
}

function deletefirestore(id) {
    firestore.collection("restaurant").doc(id).delete()
        .then(function () {
            getData();
            new PNotify({
                title: 'Suppression réussie',
                text: "Vous avez supprimé une entreprise",
                type: "danger",
                animation: "bounceInUp",
                icon: false
            });

        });
}

//effacer le contenu des input
function clearInput() {
    $("#code").val('');
    $("#designation").val('');
    $("#prix").val('');
    $("#calorie").val('');
    $("#descr").val('');
    $("#imageDefault").attr('src', "<?php echo asset('images/B3.jpg');?>");
}

//effacer le contenu des inputs du modal
function clearInputM() {
    $("#id").val('');
    $("#numero").val('');
    $("#titre").val('');
    $("#formJuridique").val('');
    $("#evaluateur").text('');
}

$(document).ready(function () {
    dtTable = $('#example').DataTable({
        columns:
            [
                {data: "identifiant"},
                {data: "titre"},
                {data: "formJuridique"},
                {data: "dateformated"},
                {data: "btnAction"}
            ]
    });

    $("#btnSaveType").on('click', function () {
        saveData();
        clearInput();
    });

    initFirebase();
    getData();
});

$(document).on('show.bs.modal', '#modalEditProjet', function (event) {
    var button = $(event.relatedTarget); // bouton sur lequel a cliqué l'utilisateur
    var id = button.data('id');
    var mySelect = document.getElementById('evaluateur');
    var projets = firestore.collection("summary").doc(id);
    clearInputM(); //on réinitialise les champs input
    getEvaluateurs();
    projets.get().then(function (projet){
        var donnees = projet.data();
        $("#id").val(id);
        $("#numero").val(donnees.identifiant);
        $("#titre").val(donnees.titre);
        $("#formJuridique").val(donnees.formJuridique);
    });
});

$(document).on('click', '#modalBtnAssignEval', function () {
    var id = $('#id').val();
    var identifiant = $('#numero').val();
    var titre = $("#titre").val();
    var formJuridique = $("#formjuridique").val();
    var eval = $("#evaluateur").val();
    var data = {
        eval: firestore.collection("users").doc(eval)
    };
    updatefirestore(data,id);
});