//ici les operations CRUD pour les users

//selection de tous les documents de la collection restaurant_collection
/*function getTypeProduits()
{
    var select = document.getElementById('typeProduits');
    var query = firestore.collection("restaurant_collection");
    query.get().then(function (docData)
    {
        if (docData.size)
        {
            var arrObj = [];

            docData.forEach(function (data) {
                var obj = data.data();
                select.options[select.options.length] = new Option(obj.title, data.id);
            });
        }
    });
}

//selectionne la catégorie d'un produit
function getProductCateg()
{
    var select = document.getElementById('categorieM');
    var query = firestore.collection("restaurant_collection");
    query.get().then(function (docData)
    {
        if (docData.size)
        {
            var arrObj = [];

            docData.forEach(function (data) {
                var obj = data.data();
                select.options[select.options.length] = new Option(obj.title, data.id);
            });
        }
    });
}*/

//liste tous les produits
function getData()
{
    var profils = {};
    var users = {};

    firestore.collection('profile').get().then((results) => {
        results.forEach((doc) => {
            profils[doc.id] = doc.data();
        });
        var user = firestore.collection('users');
        user.get().then((docSnaps) => {
            var arrObj = [];
            docSnaps.forEach((doc) => {
                users = doc.data();
                var id = users.profile.id;
                users.profil = profils[id].title;
                users.btnAction = '<button type="button" id="'+doc.id+'"  data-id="'+doc.id+'" data-toggle="modal" data-target="#modalEditUser" class="btn btn-primary btn_edit" name="btn_edit">Modifier</button>&nbsp' +
                    '<button type="button" id="'+doc.id+'" class="btn btn-danger btn_delete">Supprimer</button>';
                arrObj.push(users);
            });
            dtTable.clear();
            dtTable.rows.add(arrObj);
            dtTable.order([0, 'desc' ] ).draw();
        });
    });

}

function saveData()
{
    var ref = storage.ref();
    var name = $("#name").val();
    var prenom = $("#prenom").val();
    var sexe = $("#sexe").val();
    var dateNaiss = $("#dateNaiss").val();
    var tel = $("#tel").val();
    var email = $("#email1").val();
    var profile = $("#profil").val();
    var password = $("#password").val();
    var photo_profil = document.querySelector('#photo_profil').files[0];
    const imageName = (+new Date());
    var message = '';
    const metadata = {
        contentType: photo_profil.type
    };
    const task = ref.child('users/'+imageName).put(photo_profil, metadata);
    task
        .then(snapshot => snapshot.ref.getDownloadURL())
        .then((url) => {
            var data = {
                nom: name,
                prenom: prenom,
                sexe: sexe,
                dateNaiss: dateNaiss,
                tel: tel,
                email: email,
                photoName: imageName,
                firstTimeConnection: '',
                profile: firestore.collection('profile').doc(profile),
                photo: url
            };
            auth.createUserWithEmailAndPassword(email, password).then(function(){
                // let authId = ;
                // auth.currentUser.sendEmailVerification();
                auth.currentUser.updateProfile({
                    displayName: name+' '+prenom,
                    photoURL:    url,
                    phoneNumber:    tel,
                    emailVerified : true
                });
                // data.profile = firestore.collection('profile').doc(profile);
                addfirestore(data,auth.currentUser.uid);
            }).catch(function (error) {
                var errorCode = error.code;
                console.log(error);
            });
        })
        .catch((error) => {
            // A full list of error codes is available at
            switch (error.code) {
                case 'storage/unauthorized':
                    message = 'Vous n\'avez pas les authorisations requises';
                    break;
                case 'storage/canceled':
                    message = 'Le fichier a été supprimé';
                    break;
                case 'storage/unknown':
                    message = 'Erreur inattendue est survenue. Veillez réessayer';
                    break;
            }
        });
}

// Enregistrement des données
function addfirestore(obj,doc)
{
    firestore.collection("users").doc(doc).set(obj).then(function ()
    {
        getData();
        new PNotify({
            title: 'Enregistrement réussi',
            text: "Vous avez enregistré un nouvel utilisateur",
            type: "success",
            icon: false
        });
    }).catch(function (error)
    {
        new PNotify({
            title: 'Erreur',
            text: "L'enregistrement d'un nouvel utilisateur a échoué",
            type: "danger",
            icon: false
        });
    });
}

function updatefirestore(obj, id)
{
    firestore.collection("users").doc(id).update(obj).then(function ()
    {
        getData();
        new PNotify({
            title: 'Modification réussie',
            text: "Vous avez modifié avec succès un utilisateur",
            type: "success",
            animation: "bounceInUp",
            icon: false
        });
    });
    $("#modalEditUser").modal('toggle');
}

function deletefirestore(id) {
    firestore.collection("users").doc(id).delete()
        .then(function () {
            getData();
            new PNotify({
                title: 'Suppression réussie',
                text: "Vous avez supprimé un utilisateur",
                type: "danger",
                animation: "bounceInUp",
                icon: false
            });

        });
}

//effacer le contenu des input
function clearInput() {
    $("#name").val('');
    $("#prenom").val('');
    $("#tel").val('');
    $("#email1").val('');
    $("#imageDefault").attr('src',"<?php echo asset('images/B3.jpg');?>");
}

//effacer le contenu des inputs du modal
function clearInputM() {
    $("#codeM").val('');
    $("#designationM").val('');
    $("#prixM").val('');
    $("#calorieM").val('');
    $("#descrM").val('');
    $("#imageDefault").attr('src',"");
}

$(document).ready(function () {
    dtTable = $('#example').
    DataTable({
        columns:
            [
                { data: "nom" },
                { data: "prenom" },
                { data: "profil" },
                { data: "tel" },
                { data: "email" },
                { data: "btnAction" }
            ]
    });

    $("#formUsers").on('submit', function (e) {
        e.preventDefault();
        saveData();
        clearInput();
    });

    initFirebase();
    getData();
});

$(document).on('click', '.btn_delete', function () {
    var id = $(this).attr("id");
    deletefirestore(id);
    initFirebase();
    getData();
});


//Affichage du modal de modification de produit
$(document).on('show.bs.modal', '#modalEditUser', function (event) {
    var button = $(event.relatedTarget); // bouton sur lequel a cliqué l'utilisateur
    var id = button.data('id'); // on récupère l'id de l'utilisateur
    var mySelect = document.getElementById('profilM');
    clearInputM(); //on réinitialise les champs input
    var user = firestore.collection("users").doc(id);
    user.get().then(function (docData){
        var donnees = docData.data();
        var profilId = donnees.profile.id;//recuperation de l'id de la zone
        $("#idUser").val(id);
        $("#nameM").val(donnees.nom);
        $("#prenomM").val(donnees.prenom);
        $("#sexeM").val(donnees.sexe);
        $("#telM").val(donnees.tel);
        $("#profilM").val(profilId);
        $("#emailM").val(donnees.email);
        $("#dateNaissM").val(donnees.dateNaiss);
        $("#name").val(donnees.photoName);
        $("#userPhotoLoaded").attr('src',donnees.photo);
    });
});

$(document).on('click', '#modalBtnEditUser', function () {
    var ref = storage.ref();//reference to storage cloud
    var id_edit = $('#idUser').val();//id du produit à modifier
    var nom = $("#nameM").val();
    var prenom = $("#prenomM").val();
    var tel = $("#telM").val();
    var sexe = $("#sexeM").val();
    var profile = $("#profilM").val();
    var email = $("#emailM").val();
    var password = $("#passwordM").val();
    var dateNaiss = $("#dateNaissM").val();
    var nameImg = $("#name").val();
    var data = {};
    var photoUser = document.querySelector('#photo_profil').files[0];

    //on verifie si l'image n'a pas été changée
    if (photoUser === undefined)
    {
        var urlM = $("#userPhotoLoaded").attr('src');
        data = {
            nom: nom,
            prenom: prenom,
            tel: tel,
            sexe: sexe,
            email: email,
            password: password,
            dateNaiss: dateNaiss
        };
        data.profile = firestore.collection('profile').doc(profile);
        updatefirestore(data,id_edit);
    }
    else {
        const imageName = (+new Date()); //new name of the image
        var message = '';
        const metadata = {
            contentType: photoUser.type
        };
        var refToDelete = ref.child('users/'+nameImg); //on supprime l'ancienne image
        refToDelete.delete().then(function () {
            const task = ref.child('users/'+imageName).put(photoUser, metadata);
            task.then(snapshot => snapshot.ref.getDownloadURL()).then((url) => {
                var urlM = url;
                data = {
                    nom: nom,
                    prenom: prenom,
                    tel: tel,
                    sexe: sexe,
                    email: email,
                    password: password,
                    dateNaiss: dateNaiss,
                    photoName: imageName,
                    photo: urlM
                };
                updatefirestore(data,id_edit);
            }).catch((error) =>
            {
                // A full list of error codes is available at
                switch (error.code) {
                    case 'storage/unauthorized':
                        message = 'Vous n\'avez pas les authorisations requises';
                        break;
                    case 'storage/canceled':
                        message = 'Le fichier a été supprimé';
                        break;
                    case 'storage/unknown':
                        message = 'Erreur inattendue est survenue. Veillez réessayer';
                        break;
                }
            });
        });
    }
});