/* eslint-disable no-undef */
// How to use, run `node SetdefaultProduitsFinancier.js --idproject <projectId>`
// Formations 2023 id => MhfU4PvyQXSdeiwueJPD

if (process.argv[2] && process.argv[2].toLowerCase() === '--idproject') {
    console.info('This script is used to set empty produitsFinanciers field in a bpFinancier.products object');
  } else {
    console.log('Parameter id is absent! Fatal error');
  }

// Checks for --custom and if it has a value
const customIndex = process.argv.indexOf('--idproject');
let customValue;

if (customIndex > -1) {
  // Retrieve the value after --custom
  customValue = process.argv[customIndex + 1];
  const custom = (customValue || null);
  const firebase = require("firebase");

  console.log('= = = = = = = = = = = = = = = = = =');
  console.log('Sélèction du projet ', custom);

  const config = ({
      apiKey: "AIzaSyAtLbJIn6Rk7xXdj81bIyBCc-KoBt3y9Hs",
      authDomain: "cyberschool-ab785.firebaseapp.com",
      databaseURL: "https://cyberschool-ab785.firebaseio.com",
      projectId: "cyberschool-ab785",
      storageBucket: "cyberschool-ab785.appspot.com",
      messagingSenderId: "107562228204",
      appId: "1:107562228204:web:29cb3cd4f35fe30bbcc8c9",
      measurementId: "G-H7E9VRGRNV"

  });
  firebase.initializeApp(config);

  firebase.firestore().settings({
      cacheSizeBytes: firebase.firestore.CACHE_SIZE_UNLIMITED
  });

  firebase.firestore().collection('financier').doc(custom).get().then( doc => {
    // recupere les anciens produits
    console.log(`Produits du projet ${custom}: `,doc.data().produits.keys)
    // rajout de l'objet produitFinancier
    let newProduits = Object.assign(doc.data().produits, {produitsFinanciers: []});
    console.log('Nouveau schema produits: ', newProduits.keys);

    firebase.firestore().collection('financier').doc(custom).update({
        produits: newProduits
    }).then( () => {
        console.log('Default Produits schema successfully updated for project ',custom);
        console.log('= = = = = = = = = = = = = = = = = =');
    });
  });

} else {
console.log("Please be sure to use the --idproject parameter and retry !")
}
