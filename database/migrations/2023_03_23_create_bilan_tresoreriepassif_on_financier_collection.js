/* eslint-disable no-undef */
// How to use, run `node SetdefaultTvaParameter.js --idproject <projectId>`
// Formations 2023 id => MhfU4PvyQXSdeiwueJPD

console.info('This script is used to set default bilan financier data in the whole bpFinancier collection');

const firebase = require("firebase");

const config = ({
    apiKey: "AIzaSyAtLbJIn6Rk7xXdj81bIyBCc-KoBt3y9Hs",
    authDomain: "cyberschool-ab785.firebaseapp.com",
    databaseURL: "https://cyberschool-ab785.firebaseio.com",
    projectId: "cyberschool-ab785",
    storageBucket: "cyberschool-ab785.appspot.com",
    messagingSenderId: "107562228204",
    appId: "1:107562228204:web:29cb3cd4f35fe30bbcc8c9",
    measurementId: "G-H7E9VRGRNV"

});
firebase.initializeApp(config);  

firebase.firestore().settings({
    cacheSizeBytes: firebase.firestore.CACHE_SIZE_UNLIMITED
});

firebase.firestore().collection('financier').get().then((bpfinancier) => {
    let counter = 0;
    
    bpfinancier.forEach((financier) => {
        counter++;
        
        if(financier.data()['projet'] != null) {
            console.log('= = = = = = = = = = = = = = = = = =');
            console.log('Sélèction du projet ', financier.data().projet);
            // console.log(financier.id, " => ", financier.data());
            // recupere les anciennes data
            console.log(`${counter}. Bilan du projet ${financier.data().projet}: `);
            console.log(financier.data().bilan);        
            if(financier.data().bilan){
                // rajout de la dva par defaut
                let newParams = Object.assign(financier.data().bilan, {actif:null, passif: {tresoreriePassif:0}});
                console.log('Nouveau bilan: ', newParams);

                financier.ref.update({
                    'bilan': newParams
                }).then( () => {
                    console.log(`Default bilan financier successfully updated for project ${counter}:`,financier.data().projet);
                    console.log('= = = = = = = = = = = = = = = = = =');
                });
            } else {
                // set with merge
                financier.ref.set({
                        bilan: {actif:null, passif: {tresoreriePassif:0}}
                    }, {merge:true})
                .then(() => {
                    console.log(`Default bilan financier successfully updated for project ${counter}:`,financier.data().projet);
                    console.log('= = = = = = = = = = = = = = = = = =');
                });
            }
        }
    });
});
