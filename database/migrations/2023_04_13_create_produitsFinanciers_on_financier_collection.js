/* eslint-disable no-undef */
// How to use, run `node 2023_03_22_create_impotfiscal_on_financier_collection.js --idproject <projectId>`
// Formations 2023 id => MhfU4PvyQXSdeiwueJPD

console.info('This script is used to set default financial products schema in the whole bpFinancier collection');

const firebase = require("firebase");
const defaultEmptyProduits = require('../../resources/assets/js/views/bp/financier/default.emptyProduits.json');
// console.log(defaultEmptyProduits);

const config = ({
    apiKey: "AIzaSyAtLbJIn6Rk7xXdj81bIyBCc-KoBt3y9Hs",
    authDomain: "cyberschool-ab785.firebaseapp.com",
    databaseURL: "https://cyberschool-ab785.firebaseio.com",
    projectId: "cyberschool-ab785",
    storageBucket: "cyberschool-ab785.appspot.com",
    messagingSenderId: "107562228204",
    appId: "1:107562228204:web:29cb3cd4f35fe30bbcc8c9",
    measurementId: "G-H7E9VRGRNV"

});
firebase.initializeApp(config);

firebase.firestore().settings({
    cacheSizeBytes: firebase.firestore.CACHE_SIZE_UNLIMITED
});

firebase.firestore().collection('financier').get().then((bpfinancier) => {
    let counter = 0;

    bpfinancier.forEach((financier) => {
        counter++;

        if(financier.data()['projet'] != null) {
            console.log('= = = = = = = = = = = = = = = = = =');
            console.log('Sélèction du projet ', financier.data().projet);
            // recupere les anciens parametres
            console.log(`${counter}. Produits du projet ${financier.data().projet} : ${financier.data().produits}: `);
            if(financier.data().produits){
                // rajout de l'objet 'produitsFinanciers' par defaut
                let newProduits = Object.assign(financier.data().produits, {produitsFinanciers:[]} );
                console.log('Nouveau schema produits: ', newProduits);

                financier.ref.update({
                    produits: newProduits
                }).then( () => {
                    console.log(`Default Produits schema successfully updated for project ${counter}:`,financier.data().projet);
                    console.log('= = = = = = = = = = = = = = = = = =');
                });
            } else {
                // get the old bpfinancier
                let oldFinancier = financier.data();
                oldFinancier['produits'] = defaultEmptyProduits;
                financier.ref.update(oldFinancier).then(() => {
                    console.log(`Default Financier Products successfully updated for project ${counter}:`,financier.data().projet);
                    console.log('= = = = = = = = = = = = = = = = = =');
                });
            }
        }
    });
});
