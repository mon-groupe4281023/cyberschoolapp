/* eslint-disable no-undef */
// How to use, run `node SetdefaultTvaParameter.js --idproject <projectId>`
// Formations 2023 id => MhfU4PvyQXSdeiwueJPD

if (process.argv[2] && process.argv[2].toLowerCase() === '--idproject') {
    console.info('This script is used to set default bilan financier data in a bpFinancier project');
  } else {
    console.log('Parameter id is absent! Fatal error');
  }
  
  // Checks for --custom and if it has a value
  const customIndex = process.argv.indexOf('--idproject');
  let customValue;
  
  if (customIndex > -1) {
  // Retrieve the value after --custom
  customValue = process.argv[customIndex + 1];
  const custom = (customValue || null);
  const firebase = require("firebase");
  
  console.log('= = = = = = = = = = = = = = = = = =');
  console.log('Sélèction du projet ', custom);
  
  const config = ({
      apiKey: "AIzaSyAtLbJIn6Rk7xXdj81bIyBCc-KoBt3y9Hs",
      authDomain: "cyberschool-ab785.firebaseapp.com",
      databaseURL: "https://cyberschool-ab785.firebaseio.com",
      projectId: "cyberschool-ab785",
      storageBucket: "cyberschool-ab785.appspot.com",
      messagingSenderId: "107562228204",
      appId: "1:107562228204:web:29cb3cd4f35fe30bbcc8c9",
      measurementId: "G-H7E9VRGRNV"
  
  });
  firebase.initializeApp(config);  
  
  firebase.firestore().settings({
      cacheSizeBytes: firebase.firestore.CACHE_SIZE_UNLIMITED
  });
  
  firebase.firestore().collection('financier').doc(custom).get().then( doc => {
    // recupere les anciennes datas
    console.log(`Bilan financier du projet ${custom}: `,doc.data().bilan)
    if(doc.data().bilan){
      // rajout de la dva par defaut
      let newParams = Object.assign(doc.data().bilan, {actif:null, passif: {tresoreriePassif:0}});
      console.log('Nouveau bilan: ', newParams);
  
      firebase.firestore().collection('financier').doc(custom).update({
          bilan: newParams
      }).then( () => {
          console.log('Default bilan.passif successfully updated for project ',custom);
          console.log('= = = = = = = = = = = = = = = = = =');
      });
    } else {
      // on cree l'objet bilan depuis zero
      console.log('Bilan financier en cours de création . . . . . ');
      // set with merge
      firebase.firestore().collection('financier').doc(custom).set({
            bilan: {actif:null, passif: {tresoreriePassif:0}}
        }, {merge:true})
      .then(() => {
        console.log(`Default bilan financier successfully created for project ${counter}:`,financier.data().projet);
        console.log('= = = = = = = = = = = = = = = = = =');
      });
    }
  });
  
  } else {
    console.log("Please be sure to use the --idproject parameter and retry !")
  }